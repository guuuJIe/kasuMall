//
//  AppDelegate.m
//  MallApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AppDelegate.h"
#import "MainTabbarController.h"
#import "LoginVC.h"
#import "AccountApi.h"
#import <AlipaySDK/AlipaySDK.h>
#import "PayMannagerUtil.h"
#import "WXApi.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AipOcrSdk/AipOcrSdk.h>

// iOS10 注册 APNs 所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
// 如果需要使用 idfa 功能所需要引入的头文件（可选）
#import <AdSupport/AdSupport.h>
#import <UMCommon/UMCommon.h>
#import <UMVerify/UMVerify.h>
#import <CloudPushSDK/CloudPushSDK.h>
#import "NewBindUserVC.h"
@interface AppDelegate ()<UITabBarControllerDelegate,WXApiDelegate,UNUserNotificationCenterDelegate>
@property (nonatomic,strong) MainTabbarController *barContr;
@property (nonatomic,strong) AccountApi *accountApi;
@property (nonatomic,strong) UNUserNotificationCenter *notificationCenter;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //设置窗口对象
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.barContr = [[MainTabbarController alloc]init];
    self.barContr.delegate = self;
    self.window.rootViewController = self.barContr;
    //设置背景色
    self.window.backgroundColor = [UIColor whiteColor];
    
    [[UITabBar appearance] setTranslucent:NO];
    //显示窗口
    [self.window makeKeyAndVisible];
    
    if (accessToken) {
//         [self getRefreshToken];
    }
    
    [self setWX];
    
    [self setMap];
//    JLog(@"%f",Screen_Height);
    [self netWorkChangeEvent];
    
    [self setBaiduSdk];
    
//    [self setJPush:launchOptions];
    [self setupUM];
    
    // APNs注册，获取deviceToken并上报
    [self registerAPNS:application];
    // 初始化SDK
    [self initCloudPush];
    
    // 监听推送通道打开动作
    [self listenerOnChannelOpened];
    // 监听推送消息到达
    [self registerMessageReceive];
    
    // 点击通知将App从关闭状态启动时，将通知打开回执上报
    // [CloudPushSDK handleLaunching:launchOptions];(Deprecated from v1.8.1)
    [CloudPushSDK sendNotificationAck:launchOptions];
    
    //清楚代理ID
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Bid"];//用户注册需要
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clerkid"];//用户扫推广项目二维码需要
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wechatLogin) name:@"callWechatLogin" object:nil];
    return YES;
}

- (void)getRefreshToken{

   
//    NSString *url = [NSString stringWithFormat:@"%@%@",identityServer,@"/v1/login/RefreshToken"];
//    NSString *token = [NSString stringWithFormat:@"%@",refreshToken];
//    [XJUtil PostWithUrlAndBody:url body:[token dataUsingEncoding:NSUTF8StringEncoding] success:^(NSDictionary * _Nonnull response) {
//        
//    } failure:^(NSError * _Nonnull error) {
//        
//    }];
}

- (void)setWX
{
//    [WXApi registerApp:@"wx13518cb074bdb381"];
    [WXApi registerApp:@"wx13518cb074bdb381" universalLink:@"https://setting.ocsawz.com/"];
    
}

- (void)setMap
{
    [AMapServices sharedServices].apiKey = @"243698156d5baa965499a7c30db82779";
}

- (void)setBaiduSdk
{
    [[AipOcrService shardService] authWithAK:@"wD2jgYvO9DhLhFoZXDvv8xmp" andSK:@"PQKDWXxRUnqOXUVILDziW9oO6e4eMLmi"];
}

- (void)setupUM{
    
    [UMConfigure initWithAppkey:@"5e8c47dedbc2ec080a34a026" channel:@"App Store"];
    //设置秘钥
    NSString *info = @"oRDIZGskUdOi9onqLA2kj9EgoO08WWsGXd9nVvX07jLwRWRTK+/P2CMQN3Jal3bWn5wcYdi8WW2lMa2JE91XULvk4kb4+D3NLp7guqrzSP0Ff4haezpj+CaX4tyMF1JpoPin7WeEUKy64tQ+gKtlRqoU42ydKC04Ls1uJ77beOywVcZ5skh2aPae00ny7bChb3TkHIXx21rbdjpomzAI7UoRozRi90G1PZxMU0OeIep4K5e4Bg3lcA==";
    
    [UMCommonHandler setVerifySDKInfo:info complete:^(NSDictionary * _Nonnull resultDic) {
        
    }];
}

//- (void)wechatLogin{
//    
//    
//    [[PayMannagerUtil sharedManager] loginWithWeChatMethodsuccessBlock:^(BaseResp * _Nonnull res) {
//        SendAuthResp *authResp = (SendAuthResp *)res;
//        NSLog(@"微信登录的code%@",authResp.code);
//           NSLog(@"我的Manager ----- %@",[self class]);
//           WeakSelf(self)
//           [self.accountApi AppWxLoginWithparameters:@{@"code":authResp.code} withCompletionHandler:^(NSError *error, MessageBody *result) {
//               StrongSelf(self)
//               NSLog(@"接口返回code%@",result.message);
//               if (result.code == 200) {
//                   NSDictionary *dic = result.resultDic;
//                   NSArray *arr = dic[@"list"];
//                   NSDictionary *userData = arr.firstObject;
//                   [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
//                   [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
//                   [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
//                   [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
//                   [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
//                   [[NSUserDefaults standardUserDefaults] synchronize];
//                   //                                [self dismissViewControllerAnimated:true completion:nil];
//               }else{
//                   NewBindUserVC *vc = [[NewBindUserVC alloc] init];
//                   BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//                   vc2.modalPresentationStyle = 0;
//                   
//                   vc.uninoId = [NSString stringWithFormat:@"%@",result.message];
//                   BaseNavigationController *vc3 = (BaseNavigationController *)self.window.rootViewController;
////                   [self presentViewController:vc2 animated:true completion:nil];
//                   [vc3 presentViewController:vc2 animated:YES completion:nil];
//               }
//               
//           }];
//        
//        
//        
//    } failBlock:^(BOOL fail) {
//        
//    }];
//   
//}

#pragma mark APNs Register
/**
 *    向APNs注册，获取deviceToken用于推送
 *
 *    @brief     application
 */
- (void)registerAPNS:(UIApplication *)application {
    float systemVersionNum = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (systemVersionNum >= 10.0) {
        // iOS 10 notifications
        if (@available(iOS 10.0, *)) {
            _notificationCenter = [UNUserNotificationCenter currentNotificationCenter];
        } else {
            // Fallback on earlier versions
        }
        // 创建category，并注册到通知中心
        [self createCustomNotificationCategory];
        _notificationCenter.delegate = self;
        // 请求推送权限
        if (@available(iOS 10.0, *)) {
            [_notificationCenter requestAuthorizationWithOptions:UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if (granted) {
                    // granted
                    NSLog(@"User authored notification.");
                    // 向APNs注册，获取deviceToken
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [application registerForRemoteNotifications];
                    });
                } else {
                    // not granted
                    NSLog(@"User denied notification.");
                }
            }];
        } else {
            // Fallback on earlier versions
        }
    } else if (systemVersionNum >= 8.0) {
        // iOS 8 Notifications
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        [application registerUserNotificationSettings:
         [UIUserNotificationSettings settingsForTypes:
          (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                                           categories:nil]];
        [application registerForRemoteNotifications];
#pragma clang diagnostic pop
    } else {
        // iOS < 8 Notifications
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
#pragma clang diagnostic pop
    }
}

#pragma mark SDK Init
- (void)initCloudPush {
    // 正式上线建议关闭
#if DEBUG
    [CloudPushSDK turnOnDebug];
#endif
    // SDK初始化，手动输出appKey和appSecret
//    [CloudPushSDK asyncInit:testAppKey appSecret:testAppSecret callback:^(CloudPushCallbackResult *res) {
//        if (res.success) {
//            NSLog(@"Push SDK init success, deviceId: %@.", [CloudPushSDK getDeviceId]);
//        } else {
//            NSLog(@"Push SDK init failed, error: %@", res.error);
//        }
//    }];
    
    // SDK初始化，无需输入配置信息
    // 请从控制台下载AliyunEmasServices-Info.plist配置文件，并正确拖入工程
    [CloudPushSDK autoInit:^(CloudPushCallbackResult *res) {
        if (res.success) {
            NSLog(@"Push SDK init success, deviceId: %@.", [CloudPushSDK getDeviceId]);
        } else {
            NSLog(@"Push SDK init failed, error: %@", res.error);
        }
    }];
}

#pragma mark Channel Opened
/**
 *    注册推送通道打开监听
 */
- (void)listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onChannelOpened:)
                                                 name:@"CCPDidChannelConnectedSuccess"
                                               object:nil];
}


/// 推送通道打开回调
/// @param notification notification description
- (void)onChannelOpened:(NSNotification *)notification {
//    [JMBManager showBriefAlert:@"消息通道建立成功"];
}


#pragma mark Receive Message
/**
 *    @brief    注册推送消息到来监听
 */
- (void)registerMessageReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMessageReceived:)
                                                 name:@"CCPDidReceiveMessageNotification"
                                               object:nil];
}

/**
 *    处理到来推送消息
 *
 *    @brief   notification
 */
- (void)onMessageReceived:(NSNotification *)notification {
    NSLog(@"Receive one message!");
   
    CCPSysMessage *message = [notification object];
    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
    NSLog(@"Receive message title: %@, content: %@.", title, body);
    
  
    if(![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
//            if(tempVO.messageContent != nil) {
//                [self insertPushMessage:tempVO];
//            }
        });
    } else {
//        if(tempVO.messageContent != nil) {
//            [self insertPushMessage:tempVO];
//        }
    }
}

/*
 *  APNs注册成功回调，将返回的deviceToken上传到CloudPush服务器
 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"Upload deviceToken to CloudPush server.");
    [CloudPushSDK registerDevice:deviceToken withCallback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            NSLog(@"Register deviceToken success, deviceToken: %@", [CloudPushSDK getApnsDeviceToken]);
        } else {
            NSLog(@"Register deviceToken failed, error: %@", res.error);
        }
    }];
}

/*
 *  APNs注册失败回调
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
}

/**
 *  创建并注册通知category(iOS 10+)
 */
- (void)createCustomNotificationCategory {
    // 自定义`action1`和`action2`
    if (@available(iOS 10.0, *)) {
        UNNotificationAction *action1 = [UNNotificationAction actionWithIdentifier:@"action1" title:@"test1" options: UNNotificationActionOptionNone];
        UNNotificationAction *action2 = [UNNotificationAction actionWithIdentifier:@"action2" title:@"test2" options: UNNotificationActionOptionNone];
        // 创建id为`test_category`的category，并注册两个action到category
        // UNNotificationCategoryOptionCustomDismissAction表明可以触发通知的dismiss回调
        UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"test_category" actions:@[action1, action2] intentIdentifiers:@[] options:
                                            UNNotificationCategoryOptionCustomDismissAction];
        // 注册category到通知中心
        [_notificationCenter setNotificationCategories:[NSSet setWithObjects:category, nil]];
    } else {
        // Fallback on earlier versions
    }
   
  
}

/**
 *  处理iOS 10通知(iOS 10+)
 */
- (void)handleiOS10Notification:(UNNotification *)notification  API_AVAILABLE(ios(10.0)){
    UNNotificationRequest *request = notification.request;
    UNNotificationContent *content = request.content;
    NSDictionary *userInfo = content.userInfo;
    // 通知时间
    NSDate *noticeDate = notification.date;
    // 标题
    NSString *title = content.title;
    // 副标题
    NSString *subtitle = content.subtitle;
    // 内容
    NSString *body = content.body;
    // 角标
    int badge = [content.badge intValue];
    // 取得通知自定义字段内容，例：获取key为"Extras"的内容
    NSString *extras = [userInfo valueForKey:@"Extras"];
    // 通知角标数清0
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // 同步角标数到服务端
    // [self syncBadgeNum:0];
    // 通知打开回执上报
    [CloudPushSDK sendNotificationAck:userInfo];
    NSLog(@"Notification, date: %@, title: %@, subtitle: %@, body: %@, badge: %d, extras: %@.", noticeDate, title, subtitle, body, badge, extras);
}

/**
 *  App处于前台时收到通知(iOS 10+)
 */
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler  API_AVAILABLE(ios(10.0)){
    NSLog(@"Receive a notification in foregound.");
    // 处理iOS 10通知，并上报通知打开回执
    [self handleiOS10Notification:notification];
    // 通知不弹出
  
    completionHandler(UNNotificationPresentationOptionNone);
  
    // 通知弹出，且带有声音、内容和角标
    //completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
}

/**
 *  触发通知动作时回调，比如点击、删除通知和点击自定义action(iOS 10+)
 */

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler  API_AVAILABLE(ios(10.0)){
    NSString *userAction = response.actionIdentifier;
    // 点击通知打开
    if ([userAction isEqualToString:UNNotificationDefaultActionIdentifier]) {
        NSLog(@"User opened the notification.");
        // 处理iOS 10通知，并上报通知打开回执
        [self handleiOS10Notification:response.notification];
    }
    // 通知dismiss，category创建时传入UNNotificationCategoryOptionCustomDismissAction才可以触发
    if ([userAction isEqualToString:UNNotificationDismissActionIdentifier]) {
        NSLog(@"User dismissed the notification.");
    }
    NSString *customAction1 = @"action1";
    NSString *customAction2 = @"action2";
    // 点击用户自定义Action1
    if ([userAction isEqualToString:customAction1]) {
        NSLog(@"User custom action1.");
    }
    
    // 点击用户自定义Action2
    if ([userAction isEqualToString:customAction2]) {
        NSLog(@"User custom action2.");
    }
     completionHandler();
}
#pragma mark Notification Open
/*
 *  App处于启动状态时，通知打开回调
 */
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    NSLog(@"Receive one notification.");
    // 取得APNS通知内容
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    // 内容
    NSString *content = [aps valueForKey:@"alert"];
    // badge数量
    NSInteger badge = [[aps valueForKey:@"badge"] integerValue];
    // 播放声音
    NSString *sound = [aps valueForKey:@"sound"];
    // 取得通知自定义字段内容，例：获取key为"Extras"的内容
    NSString *Extras = [userInfo valueForKey:@"Extras"]; //服务端中Extras字段，key是自己定义的
    NSLog(@"content = [%@], badge = [%ld], sound = [%@], Extras = [%@]", content, (long)badge, sound, Extras);
    // iOS badge 清0
    application.applicationIconBadgeNumber = 0;
    // 同步通知角标数到服务端
    // [self syncBadgeNum:0];
    // 通知打开回执上报
    // [CloudPushSDK handleReceiveRemoteNotification:userInfo];(Deprecated from v1.8.1)
    [CloudPushSDK sendNotificationAck:userInfo];
}
//- (void)setJPush:(NSDictionary *)launchOptions{
//
//    // 添加下面几行代码，判断是不是点击消息进来的
//    NSDictionary *remoteNotification = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
//    if (remoteNotification) {  // 点击消息进来的
////        BackgoundViewController *bVC = [[BackgoundViewController alloc]initWithRemoteNotification:remoteNotification];
////        UINavigationController *nav = (UINavigationController*) (self.window.rootViewController);
////        [nav pushViewController:bVC animated:NO];
//        // 重置服务器端徽章
//        [JPUSHService resetBadge];
//    }
//
//
//    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
//
//    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
//
//     if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
//       // 可以添加自定义 categories
//       // NSSet<UNNotificationCategory *> *categories for iOS10 or later
//       // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
//     }
//     [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
//#ifdef DEBUG
//     [JPUSHService setupWithOption:launchOptions appKey:@"8f8bc07d55d549f1eba8fc7a" channel:@"APP Store" apsForProduction:false];
//#else
//     [JPUSHService setupWithOption:launchOptions appKey:@"8f8bc07d55d549f1eba8fc7a" channel:@"APP Store" apsForProduction:true];
//#endif
//
//}


-(NSString*)ObjectToJsonString:(id)object{
    
    NSString *jsonString = [[NSString alloc]init];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&error];
    if (! jsonData) {
        NSLog(@"error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if (tabBarController.selectedIndex == 1 || tabBarController.selectedIndex == 2) {
        //判断用户是否登录
//
        if (!accessToken) {
            
            self.barContr.selectedIndex = 0;
            
        }
         
    }
      
}




#pragma mark -----JPUSHRegisterDelegate------
//- (void)application:(UIApplication *)application
//didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//
//    JLog(@"%@",deviceToken);
//  /// Required - 注册 DeviceToken
//  [JPUSHService registerDeviceToken:deviceToken];
//}
//
//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//  //Optional
//  JLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
//}
//
////程序在后台时收到通知，点击通知栏进入app
//// iOS 10 Support
//- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler  API_AVAILABLE(ios(10.0)){
//  // Required
//  NSDictionary * userInfo = response.notification.request.content.userInfo;
//  if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//    [JPUSHService handleRemoteNotification:userInfo];
//  }
//
//  completionHandler();  // 系统要求执行这个方法
//
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//    [JPUSHService setBadge:0];
//}
//
//// iOS 12 Support
//- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center openSettingsForNotification:(UNNotification *)notification API_AVAILABLE(ios(10.0)){
//  if (notification && [notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//    //从通知界面直接进入应用
//  }else{
//    //从通知设置界面进入应用
//  }
//}
////程序在运行时收到通知，点击通知栏进入app
//// iOS 10 Support
//- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler  API_AVAILABLE(ios(10.0)){
//  // Required
//  NSDictionary * userInfo = notification.request.content.userInfo;
//  if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//    [JPUSHService handleRemoteNotification:userInfo];
//  }
//  completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有 Badge、Sound、Alert 三种类型可以选择设置
//}
////监测通知授权状态返回的结果
//- (void)jpushNotificationAuthorization:(JPAuthorizationStatus)status withInfo:(NSDictionary *)info {
//
//}
//
///** 远程通知回调函数，只要点击了远程推送的消息就会走这个方法 */
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//
//    // Required, iOS 7 Support
//    [JPUSHService handleRemoteNotification:userInfo];
//    completionHandler(UIBackgroundFetchResultNewData);
//}



#pragma mark -----UIApplicationDelegate ------
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    BOOL result = [WXApi handleOpenURL:url delegate:[PayMannagerUtil sharedManager]];
    if (!result) {
        if ([url.host isEqualToString:@"safepay"]) {
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"AliPay result%@",resultDic);
                [[PayMannagerUtil sharedManager] handleOpenURL:resultDic];
            }];
        }
    }
    
    
    return result;
}

-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler{
    
    JLog(@"%@",userActivity.webpageURL);
   return [WXApi handleOpenUniversalLink:userActivity delegate:[PayMannagerUtil sharedManager]];
}

#pragma mark - UISceneSession lifecycle


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    JLog(@"111");
   
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}

#pragma mark - 检测网络状态变化
-(NSInteger)netWorkChangeEvent
{
//    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    NSURL *url = [NSURL URLWithString:@"https://www.baidu.com"];
    __block NSInteger Type = 0;
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
//    self.netWorkStatesCode =AFNetworkReachabilityStatusUnknown;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//        self.netWorkStatesCode = status;
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
                JLog(@"当前使用的是流量模式");
                Type = 1;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                JLog(@"当前使用的是wifi模式");
                Type = 2;
                break;
            case AFNetworkReachabilityStatusNotReachable:
                JLog(@"断网了");
                Type = -1;
                break;
            case AFNetworkReachabilityStatusUnknown:
                JLog(@"变成了未知网络状态");
                Type = 0;
                break;
                
            default:
                break;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"netWorkChangeEventNotification" object:@(status)];
    }];
    [manager.reachabilityManager startMonitoring];
    return Type;
}



@end
