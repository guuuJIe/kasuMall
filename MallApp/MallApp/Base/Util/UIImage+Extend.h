//
//  UIImage+Extend.h
//  RebateApp
//
//  Created by Mac on 2019/11/12.
//  Copyright © 2019 Mac. All rights reserved.
//




#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Extend)
+(UIImage *)imageWithColor:(UIColor *)aColor;
@end

NS_ASSUME_NONNULL_END
