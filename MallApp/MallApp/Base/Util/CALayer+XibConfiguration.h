//
//  CALayer+XibConfiguration.h
//  MallApp
//
//  Created by Mac on 2020/2/15.
//  Copyright © 2020 Mac. All rights reserved.
//



#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (XibConfiguration)
@property(nonatomic, assign) UIColor *borderUIColor;
@end

NS_ASSUME_NONNULL_END
