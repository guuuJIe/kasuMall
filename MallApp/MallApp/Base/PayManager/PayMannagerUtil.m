//
//  PayMannagerUtil.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PayMannagerUtil.h"

#import <AlipaySDK/AlipaySDK.h>

@interface PayMannagerUtil ()

@property (nonatomic,strong) paySuccess successBlock;
@property (nonatomic,strong) payFaliure failBlock;
@property (nonatomic,strong) wxPayResult wxPayBlock;
@end

@implementation PayMannagerUtil

+(instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static PayMannagerUtil *instance;
    dispatch_once(&onceToken, ^{
        instance = [[PayMannagerUtil alloc] init];
    });
    return instance;
}

#pragma mark - 微信支付
- (void)payWithWeChatMethod:(NSString*)orderID successBlock:(wxPayResult)success failBlock:(payFaliure)fail{
    NSString *jsonStr = orderID;
    NSDictionary *dic = [self convert2DictionaryWithJSONString:jsonStr];
    NSLog(@"%@",dic);
    _wxPayBlock = success;
    PayReq *req = [[PayReq alloc] init];
    req.partnerId = dic[@"partnerid"];
    req.prepayId = dic[@"prepayid"];
    req.nonceStr = dic[@"noncestr"];
    req.package = dic[@"package"];
    req.sign = dic[@"sign"];
    req.timeStamp = [dic[@"timestamp"] longLongValue];
    
    [WXApi sendReq:req completion:^(BOOL success) {
        
    }];
    
}

#pragma mark - 微信支付
- (void)loginWithWeChatMethodsuccessBlock:(wxPayResult)success failBlock:(payFaliure)fail{
    
    _wxPayBlock = success;
    SendAuthReq *req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"wx_oauth_authorization_state" ;
    req.openID = WX_appID;
    
    [WXApi sendReq:req completion:^(BOOL success) {
        
    }];
    
}

#pragma mark - 支付宝支付
- (void)payWithZhifubaoMethod:(NSString*)orderID successBlock:(paySuccess)success failBlock:(payFaliure)fail{

//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *appScheme = @"XJMY";
    _successBlock = success;
    _failBlock = fail;
    [[AlipaySDK defaultService] payOrder:orderID fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        
    }];

   
}

- (NSDictionary *)convert2DictionaryWithJSONString:(NSString *)jsonString{
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"%@",err);
        return nil;
    }
    return dic;
}

#pragma mark - 微信分享
- (void)wxShareWithDic:(NSDictionary *)dic successBlock:(paySuccess)success failBlock:(payFaliure)fail InSecen:(int)scene{
    if ([WXApi isWXAppInstalled]) {
        _successBlock = success;
        _failBlock = fail;
        //创建多媒体消息结构体
        WXMediaMessage *message = [WXMediaMessage message];
        message.title = dic[@"productInfo"][@"title"];//标题
        message.description =  dic[@"productInfo"][@"sTitle"];//描述
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",URLAddress,dic[@"productInfo"][@"proPic"]]]];
        [message setThumbImage:[UIImage imageWithData:data]];
        //创建网页数据对象
        WXWebpageObject *webObj = [WXWebpageObject object];
        webObj.webpageUrl = dic[@"wapdata"];//链接
        message.mediaObject = webObj;
        
        SendMessageToWXReq *sendReq = [[SendMessageToWXReq alloc] init];
        sendReq.bText = NO;//不使用文本信息
        sendReq.message = message;
        sendReq.scene = scene;//分享到好友会话
        
        [WXApi sendReq:sendReq completion:^(BOOL success) {
            
        }];//发送对象实例
    }else{
        [JMBManager showBriefAlert:@"未安装微信应用或版本过低"];
    }
    
}

- (void)wxShareForInviterWithDic:(NSDictionary *)dic successBlock:(paySuccess)success failBlock:(payFaliure)fail InSecen:(int)scene{
    if ([WXApi isWXAppInstalled]) {
        _successBlock = success;
        _failBlock = fail;
        //创建多媒体消息结构体
        WXMediaMessage *message = [WXMediaMessage message];
        message.title = dic[@"title"];//标题
        message.description = [XJUtil insertStringWithNotNullObject:dic[@"remark"] andDefailtInfo:@""];//描述
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",URLAddress,dic[@"showPic"]]]];
        [message setThumbImage:[UIImage imageWithData:data]];
        //创建网页数据对象
        WXWebpageObject *webObj = [WXWebpageObject object];
        webObj.webpageUrl = dic[@"wapdata"];//链接
        message.mediaObject = webObj;
        
        SendMessageToWXReq *sendReq = [[SendMessageToWXReq alloc] init];
        sendReq.bText = NO;//不使用文本信息
        sendReq.message = message;
        sendReq.scene = scene;//分享到好友会话
        
        [WXApi sendReq:sendReq completion:^(BOOL success) {
            
        }];//发送对象实例
    }else{
        [JMBManager showBriefAlert:@"未安装微信应用或版本过低"];
    }
    
}

#pragma mark - WXApiDelegate
//收到微信回调
- (void)onResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[PayResp class]]) {
        
        if (_wxPayBlock) {
          _wxPayBlock(resp);
        }
    }
    
    
    if ([resp isKindOfClass:[SendMessageToWXResp class]]) {
         SendMessageToWXResp *req = (SendMessageToWXResp *)resp;
        if (_successBlock) {
            _successBlock(req);
        }
    }
    
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *authResp = (SendAuthResp *)resp;
        if (authResp.code) {
            if (_wxPayBlock) {
                _wxPayBlock(authResp);
            }
        }
    }
    
}

///给微信发回调
- (void)onReq:(BaseReq *)req {

}

#pragma mark = 支付宝客户端支付后回调
- (void)handleOpenURL:(NSDictionary *)resultDict {

    NSString *resultStatus = [NSString stringWithFormat:@"%@", [resultDict objectForKey:@"resultStatus"]];
    NSInteger statusCode = [resultStatus integerValue];

    switch (statusCode) {
        case 9000://成功
        {
            if (_successBlock) {
                _successBlock(YES);
            }
        }
            break;
        case 4000://失败
        {
            if (_failBlock) {
                _failBlock(NO);
            }
        }
            break;
        default://其它
        {
            if (_failBlock) {
                _failBlock(NO);
            }
        }
            break;
    }
}

@end
