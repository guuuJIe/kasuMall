//
//  PayMannagerUtil.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"
NS_ASSUME_NONNULL_BEGIN

@protocol PayManagerDelegate <NSObject>

@optional
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response;
@end
typedef void(^paySuccess)(BOOL success);
typedef void(^payFaliure)(BOOL fail);
typedef void(^wxPayResult)(BaseResp *res);
@interface PayMannagerUtil : NSObject<WXApiDelegate>

/**
 代理
 */
@property (nonatomic, assign) id<PayManagerDelegate> delegate;

/**
 单例

 @return 单例
 */
+ (instancetype)sharedManager;

/**
 微信支付

 @param orderID 订单ID
 @param success 成功回调
 @param fail 失败回调
 */
- (void)payWithWeChatMethod:(NSString*)orderID successBlock:(wxPayResult)success failBlock:(payFaliure)fail;
/**
微信登录
@param success 成功回调
@param fail 失败回调
*/
- (void)loginWithWeChatMethodsuccessBlock:(wxPayResult)success failBlock:(payFaliure)fail;
/**
 支付宝支付

 @param orderID 订单ID
 @param success 成功回调
 @param fail 失败回调
 */
- (void)payWithZhifubaoMethod:(NSString*)orderID successBlock:(paySuccess)success failBlock:(payFaliure)fail;

/**
 支付宝支付回调

 @param resultDict 回调字典参数
 */
- (void)handleOpenURL:(NSDictionary *)resultDict;


/**
 微信分享

 @param dic 字典参数
 @param success 成功回调
 @param fail 失败回调
 @param scene 分享场景
 */
- (void)wxShareWithDic:(NSDictionary *)dic successBlock:(paySuccess)success failBlock:(payFaliure)fail InSecen:(int)scene;

/**
微信分享

@param dic 字典参数
@param success 成功回调
@param fail 失败回调
@param scene 分享场景
*/
- (void)wxShareForInviterWithDic:(NSDictionary *)dic successBlock:(paySuccess)success failBlock:(payFaliure)fail InSecen:(int)scene;
@end

NS_ASSUME_NONNULL_END
