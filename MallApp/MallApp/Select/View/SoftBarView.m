//
//  SoftBarView.m
//  jsjhb2c
//
//  Created by qiangchen on 2017/3/28.
//  Copyright © 2017年 jsjh. All rights reserved.
//

#import "SoftBarView.h"
#import "SoftBarIconButton.h"
@interface SoftBarView()
@property (nonatomic,strong) NSMutableArray *btnDatas;
@property (nonatomic,strong) UIButton *defaultBtn;
@property (nonatomic,strong) UIButton *orderCountBtn;
@property (nonatomic,strong) UIButton *hotBtn;
@property (nonatomic,strong) SoftBarIconButton *priceBtn;
@property (nonatomic,strong) UIButton *filterBtin;
@property (nonatomic,strong) UIButton *switchViewBtn;
@property (nonatomic,assign) NSInteger type;
@end

@implementation SoftBarView

-(instancetype)initWithFrame:(CGRect)frame withType:(NSInteger)type
{
    self.type=type;
    return [self initWithFrame:frame];
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setupLayout];
    }
    return self;
}
#pragma  mark -layout
-(void)setupLayout
{
    [self.defaultBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self);
        make.width.mas_equalTo(Screen_Width/5);
    }];
    
    
    [self.hotBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(self.defaultBtn.mas_right);
        make.width.equalTo(self.defaultBtn);
    }];
    
    [self.orderCountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(self.hotBtn.mas_right);
        make.width.equalTo(self.hotBtn);
    }];
    
    if(self.type==2)
    {
        
        [self.filterBtin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self.orderCountBtn.mas_right);
            make.width.equalTo(self.orderCountBtn);
            make.right.equalTo(self);
        }];
//        [self.switchViewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.equalTo(self);
//            make.left.equalTo(self.filterBtin.mas_right);
//            make.right.equalTo(self);
//            make.width.equalTo(self.filterBtin);
//        }];
    }else if(self.type == 1)
    {
        [self.priceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self.orderCountBtn.mas_right);
            make.width.equalTo(self.orderCountBtn);
        }];
        [self.filterBtin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self.priceBtn.mas_right);
            make.width.equalTo(self.priceBtn);
            make.right.equalTo(self);
        }];
    }else{
        [self.priceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self.orderCountBtn.mas_right);
            make.width.equalTo(self.orderCountBtn);
        }];
        [self.filterBtin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self.priceBtn.mas_right);
            make.width.equalTo(self.priceBtn);
        }];
//        [self.switchViewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.equalTo(self);
//            make.left.equalTo(self.filterBtin.mas_right);
//            make.right.equalTo(self);
//            make.width.equalTo(self.filterBtin);
//        }];
    }
    
//    [self addLineWithType:BMLineTypeCustomDefault color:[UIColor colorWithHexString:@"eee"] position:BMLinePostionCustomBottom];
    
    [self.defaultBtn setSelected:YES];
}

#pragma mark -get
-(UIButton *)defaultBtn
{
    if(!_defaultBtn)
    {
        _defaultBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_defaultBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_defaultBtn setTitleColor:APPColor forState:UIControlStateSelected];
        [_defaultBtn setTitleColor:[UIColor colorWithHexString:@"999"] forState:UIControlStateNormal];
        [_defaultBtn setTitle:@"默认" forState:UIControlStateNormal];
        [_defaultBtn setTag:defaultViewEvent];
        [_defaultBtn addTarget:self action:@selector(defaultEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_defaultBtn];
    }
    return _defaultBtn;
}

-(UIButton *)orderCountBtn
{
    if(!_orderCountBtn)
    {
        _orderCountBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_orderCountBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_orderCountBtn setTitleColor:APPColor forState:UIControlStateSelected];
        [_orderCountBtn setTitleColor:[UIColor colorWithHexString:@"999"] forState:UIControlStateNormal];
        [_orderCountBtn setTitle:@"销量" forState:UIControlStateNormal];
        [_orderCountBtn setTag:OrderCountEvent];
        [_orderCountBtn addTarget:self action:@selector(defaultEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_orderCountBtn];
    }
    return _orderCountBtn;
}

-(UIButton *)hotBtn
{
    if(!_hotBtn)
    {
        _hotBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_hotBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_hotBtn setTitleColor:APPColor forState:UIControlStateSelected];
        [_hotBtn setTitleColor:[UIColor colorWithHexString:@"999"] forState:UIControlStateNormal];
        [_hotBtn setTitle:@"人气" forState:UIControlStateNormal];
        [_hotBtn setTag:HotCountEvent];
        [_hotBtn addTarget:self action:@selector(defaultEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_hotBtn];
    }
    return _hotBtn;
}
-(SoftBarIconButton *)priceBtn
{
    if(!_priceBtn)
    {
        _priceBtn=[[SoftBarIconButton alloc]init];
        [_priceBtn setTitle:@"价格" forState:UIControlStateNormal];
        [_priceBtn setTitleColor:APPColor forState:UIControlStateSelected];
        [_priceBtn setTitleColor:[UIColor colorWithHexString:@"999"] forState:UIControlStateNormal];
        [_priceBtn setImage:[UIImage imageNamed:@"soft_icon"] forState:UIControlStateNormal];
        [_priceBtn setImage:[UIImage imageNamed:@"asc_icon"] forState:UIControlStateSelected];
        [_priceBtn setTag:PriceAscEvent];
        [_priceBtn addTarget:self action:@selector(defaultEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_priceBtn];
    }
    return _priceBtn;
}
-(UIButton *)filterBtin
{
    if(!_filterBtin)
    {
        _filterBtin=[UIButton buttonWithType:UIButtonTypeCustom];
        [_filterBtin.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_filterBtin setTitleColor:[UIColor colorWithHexString:@"999"] forState:UIControlStateNormal];
        [_filterBtin setTitle:@"筛选" forState:UIControlStateNormal];
        [_filterBtin setTag:FilterEvent];
        [_filterBtin setImage:[UIImage imageNamed:@"Fliter"] forState:UIControlStateNormal];
        [_filterBtin setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [_filterBtin setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        [_filterBtin addTarget:self action:@selector(defaultEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_filterBtin];
    }
    return _filterBtin;
}
-(UIButton *)switchViewBtn
{
    if(!_switchViewBtn)
    {
        _switchViewBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_switchViewBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_switchViewBtn setImage:[UIImage imageNamed:@"switch_view_table_icon"] forState:UIControlStateNormal];
        [_switchViewBtn setImage:[UIImage imageNamed:@"switch_view_list_icon"] forState:UIControlStateSelected];
        [_switchViewBtn setTag:SwitchViewEvent];
        [_switchViewBtn addTarget:self action:@selector(defaultEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_switchViewBtn];
    }
    return _switchViewBtn;
}

#pragma mark -event
-(void)defaultEvent:(UIButton *)btn
{
    if(btn == self.switchViewBtn){
        [btn setSelected:!btn.selected];
    }else{
        [btn setSelected:YES];
    }
    //            [self.priceBtn setImage:[UIImage imageNamed:@"soft_icon"] forState:UIControlStateNormal];
    NSInteger soft=0;
    if(btn==self.defaultBtn)
    {
        [self.orderCountBtn setSelected:NO];
        [self.priceBtn setSelected:NO];
        [self.hotBtn setSelected:NO];
    }else if(btn==self.orderCountBtn)
    {
        [self.defaultBtn setSelected:NO];
        [self.priceBtn setSelected:NO];
        [self.hotBtn setSelected:NO];
    }else if(btn==self.hotBtn)
    {
        [self.defaultBtn setSelected:NO];
        [self.priceBtn setSelected:NO];
        [self.orderCountBtn setSelected:NO];
    }else if(btn==self.priceBtn)
    {
        UIImage *curImage=[self.priceBtn imageForState:UIControlStateSelected];
        if([UIImagePNGRepresentation(curImage)isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"asc_icon"])])
        {
            [self.priceBtn setImage:[UIImage imageNamed:@"desc_icon"] forState:UIControlStateSelected];
            soft=1;
        }else
        {
            [self.priceBtn setImage:[UIImage imageNamed:@"asc_icon"] forState:UIControlStateSelected];
            soft=2;
            
        }
        [self.defaultBtn setSelected:NO];
        [self.orderCountBtn setSelected:NO];
        [self.hotBtn setSelected:NO];
    }
    self.click(btn.tag,btn.selected,soft);
}

@end
