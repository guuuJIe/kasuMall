//
//  SelectVCCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SelectVCCell.h"

@implementation SelectVCCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"productPic"]];
        [self.goodsImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = dic[@"title"];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
        self.salesLbl.text = @"";
    }
}

@end
