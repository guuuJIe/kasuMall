//
//  SoftBarView.h
//  jsjhb2c
//
//  Created by qiangchen on 2017/3/28.
//  Copyright © 2017年 jsjh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    OrderCountEvent,
    HotCountEvent,
    PriceDescEvent,
    PriceAscEvent,
    FilterEvent,
    SwitchViewEvent,
   defaultViewEvent
}SoftEventType;
typedef void(^clickEvent)(SoftEventType softEventType,BOOL state,NSInteger soft);
@interface SoftBarView : UIView
-(instancetype)initWithFrame:(CGRect)frame withType:(NSInteger)type;//type  0 搜索界面  1 店铺见面
@property (nonatomic,copy) clickEvent click;
@end
