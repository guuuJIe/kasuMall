//
//  SoftBarIconButton.m
//  jsjhb2c
//
//  Created by qiangchen on 2017/3/28.
//  Copyright © 2017年 jsjh. All rights reserved.
//

#import "SoftBarIconButton.h"

@implementation SoftBarIconButton

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        [self.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self.imageView setContentMode:UIViewContentModeLeft];
        [self.titleLabel setTextAlignment:NSTextAlignmentRight];
        
    }
    return self;
}

-(CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 0, self.bounds.size.width*0.7, self.bounds.size.height);
}
-(CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(self.bounds.size.width*0.7, 0, self.bounds.size.width*0.3, self.bounds.size.height);
}

@end
