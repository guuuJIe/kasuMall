//
//  FliterCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FliterCell.h"
@interface FliterCell()

@end

@implementation FliterCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
//        UIView *backgroundView = [UIView new];
//        backgroundView.backgroundColor = APPColor;
//        self.selectedBackgroundView = backgroundView;
        [self setupUI];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
  
}

- (void)setupUI{
    
    UIView *bgView = [UIView new];
    [self addSubview:bgView];
    bgView.backgroundColor = UIColorEF;
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake((Screen_Width-120)/3-10, 40));
        make.center.equalTo(self);
    }];
    self.bgView = bgView;
    
    [bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {

//        make.right.equalTo(bgView);
//        make.size.equalTo(bgView);
        make.edges.mas_equalTo(bgView);
        
    }];
    
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLabel.text = [NSString stringWithFormat:@"%@",dic[@"name"]];
    }
}

- (void)setModel:(CateItemModel *)model{
    self.titleLabel.text = model.name;
    if (model.isSelected) {
        self.titleLabel.textColor = [UIColor whiteColor];
        self.bgView.backgroundColor = APPColor;
    }else{
        self.titleLabel.textColor = UIColor333;
        self.bgView.backgroundColor = UIColorEF;
    }
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
//        _titleLabel = [UILabel commonLabelWithtext:@"" color:UIColor333 font:LabelFont12 textAlignment:NSTextAlignmentCenter];
        _titleLabel.text = @"";
        _titleLabel.font = LabelFont12;
        _titleLabel.textColor = UIColor333;
        _titleLabel.numberOfLines = 2;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}
@end
