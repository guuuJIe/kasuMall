//
//  FliterView.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FliterView.h"
#import "FliterHeadView.h"
#import "FliterCell.h"
#import "SearchGoodsApi.h"
@interface FliterView()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UIView *contentView;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *propertyArray;
@property (nonatomic,strong) NSMutableArray *brandArray;
@property (nonatomic,strong) SearchGoodsApi *goodsApi;
@property (nonatomic,strong) NSMutableString *propStr;
@end
@implementation FliterView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        self.userInteractionEnabled = true;
//        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitAni)] ];
        [self initLayout];
        
    }
    
    return self;
    
}

- (void)initLayout{
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(Screen_Width, 0, self.frame.size.width-120, Screen_Height)];
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.contentView];
    
    [self.contentView addSubview:self.collectionView];
    self.contentView.userInteractionEnabled = true;
   
    
    UIView *bg = [UIView new];
    bg.layer.cornerRadius = 18;
    [self.contentView addSubview:bg];

    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(-10-BottomAreaHeight);
        make.size.mas_equalTo(CGSizeMake(178, 36));
    }];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 89, 36)];
    [button setTitle:@"重置" forState:0];
    [button.titleLabel setFont:LabelFont14];
    button.backgroundColor = UIColorEF;
    [button setTitleColor:UIColor999 forState:0];
    button.tag = 100;
    [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [bg addSubview:button];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:button.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:CGSizeMake(18, 18)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = button.bounds;
    maskLayer.path = maskPath.CGPath;
    button.layer.mask = maskLayer;
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(89, 0, 89, 36)];
    [button2 setTitle:@"确定" forState:0];
    [button2.titleLabel setFont:LabelFont14];
    button2.backgroundColor = APPColor;
    [button2 setTitleColor:[UIColor whiteColor] forState:0];
    button2.tag = 101;
    [button2 addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [bg addSubview:button2];
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:button2.bounds byRoundingCorners:UIRectCornerBottomRight | UIRectCornerTopRight cornerRadii:CGSizeMake(18, 18)];
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = button2.bounds;
    maskLayer2.path = maskPath2.CGPath;
    button2.layer.mask = maskLayer2;
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
          make.left.right.top.equalTo(self.contentView);
          make.bottom.mas_equalTo(bg.mas_top);
      }];
}

- (void)click:(UIButton *)sender{
    if (sender.tag == 101) {
        NSLog(@"%@",self.propStr);
        if (self.clickBlock) {
            self.clickBlock(self.propStr);
        }
    }else if (sender.tag == 100){
        self.propStr = [[NSMutableString alloc] initWithString:@""];
      
        for (int i = 0; i<self.propertyArray.count; i++) {
            CateModel *model = self.propertyArray[i];
            for (int j = 0; j<model.list.count; j++) {
                CateItemModel *iteModel = model.list[j];
                iteModel.isSelected = false;
            }
        }
        [self.collectionView reloadData];
    }
}

- (void)initAnimation{
    

    [UIView animateWithDuration:0.5 animations:^{
        //将view.frame 设置在屏幕上方
        self.contentView.frame = CGRectMake(120, 0, self.frame.size.width-120, Screen_Height);
    }];
}

- (void)exitAni{

    [UIView animateWithDuration:0.5 animations:^{
       self.contentView.frame = CGRectMake(Screen_Width, 0, self.frame.size.width, Screen_Height);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)show{
    
    [_contrc.navigationController.view addSubview:self];
    [self initAnimation];
    
}

- (void)setupData:(NSDictionary *)dic{
    
    [self.dataArray removeAllObjects];
    
    NSArray *data = dic[@"list"];
    self.dataArray = [CateItemModel mj_objectArrayWithKeyValuesArray:data];
    

    [self.collectionView reloadData];
    [self layoutIfNeeded];
}
//设置配件数据
- (void)setSubData:(NSDictionary *)dic{
    NSArray *data = dic[@"list"];
    [self.propertyArray removeAllObjects];
    self.propertyArray = [CateModel mj_objectArrayWithKeyValuesArray:data];
    [self.collectionView reloadData];
    [self layoutIfNeeded];
}
//设置精品数据
- (void)setUpdata:(NSMutableArray *)dataArr{
    JLog(@"%@",dataArr);
  
    self.propertyArray = [CateModel mj_objectArrayWithKeyValuesArray:dataArr];
    [self.collectionView reloadData];
    [self layoutIfNeeded];
}

#pragma mark --UICollectionDataSource--

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1+self.propertyArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return self.dataArray.count;
    }else{
        CateModel *model = self.propertyArray[section-1];
          
        return model.list.count;
    }

     
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeMake(Screen_Width, 60);
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    return CGSizeZero;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
     UICollectionReusableView *reusableview = nil;
    if(kind == UICollectionElementKindSectionHeader){
        FliterHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FliterHeadView" forIndexPath:indexPath];
        if (indexPath.section == 0) {
            headView.titleLabel.text = @"分类";
        }else{
            CateModel *model = self.propertyArray[indexPath.section - 1];
//
            headView.titleLabel.text = model.name;
        }
        
        
            
            
           
        
            reusableview = headView;
    }
    if(kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footView" forIndexPath:indexPath];
        
        footerView.backgroundColor = [UIColor clearColor];
        
        reusableview = footerView;
        
    }
    
    return reusableview;

}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FliterCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FliterCell" forIndexPath:indexPath];
    if (indexPath.section == 0) {
        cell.model = self.dataArray[indexPath.row];
    }else{
       CateModel *model = self.propertyArray[indexPath.section - 1];

       cell.model = model.list[indexPath.row];
       
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        
        CateItemModel *selectModel = self.dataArray[indexPath.item];
        for(CateItemModel  *Itemmodel in self.dataArray) {
            Itemmodel.isSelected = false;
        }
        selectModel.isSelected = true;
        
        [self.goodsApi getProductListWithPropWithparameters:[NSString stringWithFormat:@"%ld",(long)selectModel.proId] withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                if (selectModel.proId == 78) {
                    [self.brandArray removeAllObjects];
                    [self.goodsApi getBrandListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                             NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"name":@"品牌",@"list":result.resultDic[@"list"]}];
                            [self.brandArray addObject:dic];
                            [self setUpdata:self.brandArray];
                        }
                    }];
                }
                NSArray *array = result.result;
                [self.brandArray addObject:array.firstObject];
                [self setSubData:result.resultDic];
            }
        }];
       

    }else{
        CateModel *model = self.propertyArray[indexPath.section - 1];
        
        NSArray *modelArr = model.list;
        CateItemModel *selectModel = modelArr[indexPath.item];
        for(CateItemModel  *Itemmodel in modelArr) {
            Itemmodel.isSelected = false;
        }
        selectModel.isSelected = true;
        self.propStr = [NSMutableString new];
        for(int i = 0 ;i<self.propertyArray.count;i++) {
            CateModel *model = self.propertyArray[i];
            for (int j = 0; j<model.list.count; j++) {
                CateItemModel *item = model.list[j];
                if (item.isSelected) {
                    [self.propStr appendString:[NSString stringWithFormat:@"%ld-",(long)item.proId]];
                }
            }
        }
       
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
    }
   
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
//        FliterCell *cell = (FliterCell *)[collectionView cellForItemAtIndexPath:indexPath];
//
//        [self updateCellStatus:cell selected:NO];
    }
    
}

// 改变cell的背景颜色
-(void)updateCellStatus:(FliterCell *)cell selected:(BOOL)selected
{
    cell.bgView.backgroundColor = selected ? APPColor:UIColorEF;
    cell.titleLabel.textColor = selected ? [UIColor whiteColor]:UIColor333;
//    cell.layer.borderWidth = selected ? 0:1.0;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self exitAni];
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((Screen_Width-120)/3, 50);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[FliterCell class] forCellWithReuseIdentifier:@"FliterCell"];
        [_collectionView registerClass:[FliterHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FliterHeadView"];

    }
    
    return _collectionView;
    
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}
- (NSMutableArray *)propertyArray{
    if (!_propertyArray) {
        _propertyArray = [NSMutableArray new];
    }
    return _propertyArray;
}

- (NSMutableArray *)brandArray{
    if (!_brandArray) {
        _brandArray = [NSMutableArray new];
    }
    return _brandArray;
}

- (SearchGoodsApi *)goodsApi{
    if (!_goodsApi) {
        _goodsApi = [SearchGoodsApi new];
    }
    return _goodsApi;
}
@end
