//
//  FliterView.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FliterView : UIView
@property (nonatomic) UIViewController *contrc;

- (void)show;

- (void)setupData:(NSDictionary *)dic;

@property (nonatomic,copy) void(^clickBlock)(NSMutableString *prop);
@end

NS_ASSUME_NONNULL_END
