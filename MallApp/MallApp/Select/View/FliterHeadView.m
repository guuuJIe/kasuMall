//
//  FliterHeadView.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FliterHeadView.h"

@implementation FliterHeadView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupUI];
       
    }
    return self;
}


- (void)setupUI{
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.mas_equalTo(12);
    }];
    
   
   
}



-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"分类";
        _titleLabel.font = LabelFont12;
        _titleLabel.textColor = UIColor333;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}


@end
