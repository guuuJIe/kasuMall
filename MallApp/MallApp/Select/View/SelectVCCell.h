//
//  SelectVCCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectVCCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *salesLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
