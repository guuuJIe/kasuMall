//
//  FliterCell.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CateModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FliterCell : UICollectionViewCell
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIView *bgView;
- (void)setupData:(NSDictionary *)dic;
@property (nonatomic) CateItemModel *model;
@end

NS_ASSUME_NONNULL_END
