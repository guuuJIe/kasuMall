//
//  SelectVC.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SelectVC.h"
#import "SoftBarView.h"
#import "FliterView.h"
#import "SelectVCCell.h"
#import "SearchGoodsApi.h"
#import "GoodInfoVC.h"
#import "GoodsWithSpecificVC.h"
@interface SelectVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong) UIButton * searchBtn;
@property (nonatomic,strong) UITextField * textField;
@property (nonatomic,strong) SoftBarView *softBarView;
@property (nonatomic,strong) FliterView *fliterView;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) SearchGoodsApi *goodsApi;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,strong) NSString *size;
@property (nonatomic,strong) NSString *order;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSDictionary *dic;
@property (nonatomic,strong) NSMutableString *prop;
@end

@implementation SelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initProperty];
    
    [self initLayout];
    
    
    
    
    [self getProductData:RefreshTypeNormal];
    
    [self getProductTypeData];
    
}

- (void)initProperty{
    self.num = 1;
    self.size = @"20";
    self.order = @"0";
    self.prop = [NSMutableString new];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置navbar
//    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    [self initBar];
    
}


- (void)initBar{

    self.navigationItem.backBarButtonItem = nil;
    UIView *selectView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-80, 30)];
    selectView.backgroundColor=[UIColor whiteColor];
    selectView.layer.cornerRadius=16;
    selectView.layer.masksToBounds=YES;
    
    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 5, selectView.frame.size.height-5*2, selectView.frame.size.height-5*2)];
    selectIcon.image=[UIImage imageNamed:@"搜索"];
    
    self.textField=[[UITextField alloc]initWithFrame:CGRectMake(selectIcon.frame.size.width+selectIcon.frame.origin.x+Default_Space, 0, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
    self.textField.textColor=UIColor333;
//    self.textField.placeholder=@"输入关键字";
    self.textField.font=LabelFont14;
    self.textField.returnKeyType = UIReturnKeySearch;
//    if (@available(iOS 13.0, *)) {
//
//        self.textField.attributedPlaceholder=[[NSAttributedString alloc]initWithString:self.textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];
//
//
//    }else{
//        [self.textField setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
//    }
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(selectIcon.frame.size.width+selectIcon.frame.origin.x+Default_Space, 0, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
    [button addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    //     self.textField.delegate = self;
    self.textField.text = self.keys;
    [selectView addSubview:selectIcon];
    [selectView addSubview:self.textField];
    [selectView addSubview:button];
    button.backgroundColor = [UIColor clearColor];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:selectView];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStylePlain) target:self action:@selector(rightBtnAction)];
    // 字体颜色
    [rightBtn setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)rightBtnAction{
    [self.navigationController popViewControllerAnimated:true];
    
}


- (void)initLayout{
    [self.view addSubview:self.softBarView];
//    [self.view addSubview:self.tableView];
    [self.softBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(40));
        make.top.left.right.equalTo(self.view);
    }];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.softBarView.mas_bottom).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];

}


- (void)getProductData:(RefreshType)type{
    if (type == RefreshTypeUP) {
        self.num = self.num+1;
    }else if (type == RefreshTypeDown || type == RefreshTypeNormal){
        self.num = 1;
        
    }
    NSDictionary *dic = @{@"key":self.keys,@"model":@"",@"order":self.order,@"prop":self.prop};
    [self.goodsApi searchProductListWithparameters:dic andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *array = result.resultDic[@"list"];
//            self.dataArr = result.resultDic[@"list"];
            if (self.num== 1) {
                if (array.count == 0) {
                    [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                    [self.collectionView.mj_header endRefreshing];
                }else{
                    if (array.count <20) {
                        [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                        [self.collectionView.mj_header endRefreshing];
                    }else{
                        [self.collectionView.mj_footer endRefreshing];
                        [self.collectionView.mj_header endRefreshing];
                    }

                }
               
                self.dataArr = [NSMutableArray arrayWithArray:array];
            }else{
                if (array.count == 0) {
                    [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                    [self.collectionView.mj_header endRefreshing];
                }else{
                    if (array.count <20) {
                        [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                        [self.collectionView.mj_header endRefreshing];
                    }else{
                        [self.collectionView.mj_footer endRefreshing];
                        [self.collectionView.mj_header endRefreshing];
                    }
                    
                    [self.dataArr addObjectsFromArray:array];
                }
                
            }
            [self.collectionView reloadData];
//            if (self.num == 1) {
//                 [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:false];
//            }
        }
    }];
}

- (void)getProductTypeData{
    [self.goodsApi getProductTypeWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dic = result.resultDic;
           
        }
    }];

}



//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((Screen_Width-10*3)/2,245);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SelectVCCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SelectVCCell" forIndexPath:indexPath];
    [cell setupData:self.dataArr[indexPath.item]];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.dataArr[indexPath.item];
    NSInteger protype = [dic[@"proType"] integerValue];
    if (protype == 8) {
        GoodsWithSpecificVC *vc = [GoodsWithSpecificVC new];
        vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
        [self.navigationController pushViewController:vc animated:true];
    }else{
        GoodInfoVC *vc = [GoodInfoVC new];
        vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
        [self.navigationController pushViewController:vc animated:true];
    }
    
}

-(SoftBarView *)softBarView
{
    if(!_softBarView)
    {
//        WeakSelf(weakSelf);
        
        NSInteger type = 0 ;
        
      
        WeakSelf(self);
        _softBarView=[[SoftBarView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40) withType:type];
        _softBarView.backgroundColor = [UIColor whiteColor];
        _softBarView.click=^(SoftEventType type,BOOL state,NSInteger soft)
        {
            if(type==PriceAscEvent)
            {
                if (soft == 1) {
                    weakself.order = @"5";
                }else if (soft == 2){
                    weakself.order = @"6";
                }
                
//                weakSelf.supViewModel.order = @"price";
//                weakSelf.supViewModel.order_flag=[NSString stringWithFormat:@"%ld",(long)soft];
//                [weakSelf.supViewModel.refreshDataCommand execute:nil];
            }else if(type==OrderCountEvent)
            {
                weakself.order = @"2";
//                weakSelf.supViewModel.order = @"sales";
//                weakSelf.supViewModel.order_flag=@"1";
//                [weakSelf.supViewModel.refreshDataCommand execute:nil];
            }else if(type==FilterEvent)
            {
//                weakself.fliterView.frame = CGRectMake(0, 0, Screen_Width, Screen_Height);
                [weakself.fliterView setupData:weakself.dic];
                [weakself.fliterView show];
            }else if (type==HotCountEvent){
                weakself.order = @"1";
            }else if (type == defaultViewEvent){
                weakself.order = @"3";
                
            }
            
            [weakself getProductData:RefreshTypeNormal];
        };
        
    }
    return _softBarView;
}


- (FliterView *)fliterView{
    if (!_fliterView) {
        self.fliterView = [[FliterView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
//        self.redView.backgroundColor = [UIColor greenColor];
        self.fliterView.contrc = self;
        WeakSelf(self)
        self.fliterView.clickBlock = ^(NSMutableString * _Nonnull prop) {
            StrongSelf(self)
            self.prop = prop;
            [self getProductData:RefreshTypeNormal];
        };
        [self.view addSubview:self.fliterView];
    }
    return _fliterView;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((Screen_Width-10*3)/2, 245);
        flowLayout.minimumInteritemSpacing =0;
        flowLayout.minimumLineSpacing = 10;
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        _collectionView.backgroundColor = UIColorEF;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
        [_collectionView registerNib:[UINib nibWithNibName:@"SelectVCCell" bundle:nil] forCellWithReuseIdentifier:@"SelectVCCell"];
        WeakSelf(self)
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakself getProductData:RefreshTypeDown];
        }];
        
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakself getProductData:RefreshTypeUP];
        }];
    }
    
    return _collectionView;
    
}

- (SearchGoodsApi *)goodsApi{
    if (!_goodsApi) {
        _goodsApi = [SearchGoodsApi new];
    }
    return _goodsApi;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}
@end
