//
//  HomePageApi.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomePageApi.h"
@interface HomePageApi()

@property (nonatomic , strong) JSHttpClient *client;

@end
@implementation HomePageApi


- (void)getGroupListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:groupListApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)getMallListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:MallListApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)getCarGoodsListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:cargoodsListApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)getshopListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:shopInfoApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getbannerDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:bannerDataApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getGroupListDataWithparameters:(id)parameters withType:(NSString *)type andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@/%@",getGroupListApi,type,num,size];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getAllCarListDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",getAllCarList,parameters];
    [self.client get:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getappVersionWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
//    NSString *api = [NSString stringWithFormat:@"%@%@",getAllCarList,parameters];
    [self.client get:updateAppApi parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)getHotCarTypeListDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",getHotCarTypeList,parameters];
    [self.client get:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getServiceIdDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"https://tsapi.amap.com/v1/track/service/add"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"354702ef1d3727c0b4f9e0bb9077ddfe" forKey:@"key"];
    [dic setValue:@"Kasu_serviceID" forKey:@"name"];
//    [self.client post:api parameters:dic isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
//        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
//        completionHander(error,body);
//    }];
    
    [[AFHTTPSessionManager manager] POST:api parameters:dic progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        //返回响应
        completionHander(nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completionHander(error,nil);
        
    }];
}

- (JSHttpClient *)client
{
    if (!_client) {
        _client = [JSHttpClient shareClient];
    }
    return _client;
}

@end
