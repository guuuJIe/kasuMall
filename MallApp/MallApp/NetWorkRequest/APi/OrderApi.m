//
//  OrderApi.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderApi.h"
@interface OrderApi()

@property (nonatomic , strong) JSHttpClient *client;

@end


@implementation OrderApi
- (void)submitOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:purchaseApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)cancelOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    
    [self.client get:cancelOrder parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)cancelNormalOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    
    [self.client get:cancelNormalOrderApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)confirmOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    
    [self.client get:confirmGroupOrderApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)confirmNormalOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:confirmNormalOrderApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}
- (void)getAddressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:addressListApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)getInvoiceListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:invoiceListApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}
- (void)setDefaultInvoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
   NSString *api = [NSString stringWithFormat:@"%@%@",defaultinVoiceApi,parameters];
    [self.client post:api parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}
- (void)setDefaultAdressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",defaultAddressApi,parameters];
    [self.client post:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}
- (void)delAddressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",delAddressApi,parameters];
    [self.client post:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}

- (void)delInvoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",delInvoiceApi,parameters];
    [self.client post:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}


- (void)createOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:createOrderApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)createSingleOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:createsingleOrderApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)payOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:payOrderApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)getAmountWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:balanceApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}
- (void)groupOrderInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",groupOrderInfoApi,parameters];
    [self.client get:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)groupOrderInfoByOrderNumWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",groupOrderInfoByOrderNumApi,parameters];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)goodsOrderInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",goodsOrderInfoApi,parameters];
    [self.client get:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getFixOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@/%@",getFixOrderListApi,parameters,num,size];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getUserFastServiceOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@/%@",getUserFastServiceOrferListApi,parameters,size,num];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)getSellerFastServiceOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@/%@",getSellerFastServiceOrderListApi,parameters,size,num];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)cancelUserFastSetviceOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:cancelUserFastServiceOrderApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)sellerCancelUserFastSetviceOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:shopConfirmCancelApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)addInvoiceReissuedWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:addReissuedInvoiceApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)checkInvoiceReissuedInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",checkReissuedInvoiceInfoApi,parameters];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)sellerSendInvoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:sellerSendInvoiceApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getCurAccountStatuesWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:getMemberStatuesApi parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getFixOrderDetailDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",repairOrderInfoApi,parameters];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)shopConfirmFixOrderCompleteWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    
    [self.client post:shopConfirmCompleteApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
    
}

- (void)applyNormalSalesReturnWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:userApplyNormalSalesReturnApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (JSHttpClient *)client
{
    if (!_client) {
        _client = [JSHttpClient shareClient];
    }
    return _client;
}
@end
