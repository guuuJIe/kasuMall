//
//  HomePageApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomePageApi : NSObject
/**
 限时团购
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getGroupListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
商城配件
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getMallListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
汽车精品
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getCarGoodsListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
汽车服务
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getshopListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
轮播图
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getbannerDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
团购列表2
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getGroupListDataWithparameters:(id)parameters withType:(NSString *)type andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
所有的汽车列表数据
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getAllCarListDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
一级热门车型品牌
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getHotCarTypeListDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取app版本
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getappVersionWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

- (void)getServiceIdDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
