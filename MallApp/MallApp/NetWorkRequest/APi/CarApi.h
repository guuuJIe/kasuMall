//
//  CarApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CarApi : NSObject
/**
加入购物车
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)addIncarWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
购物车列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopcarListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
购物车数据修改
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopcarDataChangeWithparameters:(id)parameters andNum:(NSString *)num andID:(NSString *)ID withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
购物车删除
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopcarDataDelWithparameters:(id)parameters andID:(NSString *)ID withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
购物车d订单提交
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopcarSubmitOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
