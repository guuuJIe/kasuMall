//
//  GoodsInfoApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsInfoApi : NSObject
/**
商品详情
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getGoodsItemWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
团购商品详情
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getGroupGoodsItemWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
加入购物车
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)AddinCarWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 立即购买
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)purchaseWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 团购商品立即购买
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)groupPurchaseWithparameters:(id)parameters andID:(NSString *)ID andNum:(NSString *)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 获取地址
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getAddressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
