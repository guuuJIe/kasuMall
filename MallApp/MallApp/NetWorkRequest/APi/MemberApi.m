//
//  MemberApi.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MemberApi.h"
@interface MemberApi()

@property (nonatomic , strong) JSHttpClient *client;

@end

@implementation MemberApi


- (void)addInVoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:addinvoiceApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)searchFixCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",searchserviceCardApi,parameters];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)useFixCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:useserviceCardApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getSellerRepairTcOrderListDataWithparameters:(id)parameters withType:(NSString *)Status andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@/%@",getSellerRepairTcOrderListoApi,num,size,Status];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8080" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getRepairTcOrderListDataWithparameters:(id)parameters withType:(NSString *)Status andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@/%@",getRepairTcOrderListoApi,num,size,Status];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8080" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}


- (void)readyUseFixCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:readyUseServiceCardApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)readyReturnCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@/%@",readyReturnCard,parameters];
    [self.client post:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)applyReturnCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:returnServiceCard parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)confirmServicesCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",confirmerSeviceCard,parameters];
    [self.client post:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)sellerEwEmOrderCheckReadyWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",sellerEwEmOrderCheckReadyApi,parameters];
       [self.client post:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
       }];
}

- (void)sellerEwEmOrderCheckWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:sellerEwEmOrderCheckApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getQRCodeDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",getQRCodeData,parameters];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (JSHttpClient *)client
{
    if (!_client) {
        _client = [JSHttpClient shareClient];
    }
    return _client;
}
@end
