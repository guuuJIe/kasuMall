//
//  AccountApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountApi : NSObject
/**
 账号密码登录
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)accountLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 获取验证码
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getverifyCodeWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 验证码登录
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)verifyCodeLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 获取refreshToken
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getRefreshToeknWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
微信登入
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)AppWxLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 获取openid
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getWechatUniod:(id)parameters withCompletionHandler:(NetworkCompletionHandler)completionHander;

/**
扫码登入
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)verifyUserCodeLoginWithparameters:(id)parameters withId:(NSString *)userId withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
设置支付密码
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)setPayPasswordWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
使用旧密码修改登录密码
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)changePayPasswordWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
一键登录
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)quickLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
注册
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)userRegisterWithparameters:(id)parameters withId:(NSString *)userId withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

@end

NS_ASSUME_NONNULL_END
