//
//  SearchGoodsApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchGoodsApi : NSObject
/**
搜索结果
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)searchProductListWithparameters:(id)parameters andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取搜索类型
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getProductTypeWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
根据type 搜索产品类型
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getProductListWithPropWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取汽车品牌
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getBrandListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
