//
//  ShopApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopApi : NSObject
/**
店铺信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
店铺列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopListWithparameters:(id)parameters andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
维修套餐
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)fixgoodsListWithparameters:(id)parameters andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
用户初始询单 （抢修/施救）
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)repairOpentWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
用户初始询单 维修
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)queryRepairOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
用户发布询单 （维修）
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)submitFixOrderDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
维修订单详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getrepairOrderInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取区域列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getRegionWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取门店服务产品信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getServiceProductListWithparameters:(id)parameters andWithShopId:(NSString *)shopId andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
提交救援订单信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)sumitHelpOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
取消救援订单信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)cancelHelpOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
更换维修订单的服务商
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)changefixOrderServicerWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
确认维修订单的服务商
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)confirmfixOrderServicerWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
