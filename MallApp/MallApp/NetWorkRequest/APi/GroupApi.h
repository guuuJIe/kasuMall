//
//  GroupApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupApi : NSObject
/**
获取砍价产品列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getBargainListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**

获取指定砍价订单进行的砍价记录列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getBargainRecordListWithparameters:(id)parameters andNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**

获取砍价产品详细记录
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getBargainGoodInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
对指定订单进行砍价操作
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)bargainCutWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
延保产品列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getEwProdutListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
延保产品详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getEwProdutInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
延保产品生成价格
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)makePriceForEwProdutWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取延保订单列表
@param parameters parameters description
@param size parameters description
@param num parameters description
@param completionHander completionHander description
*/
- (void)getEwOrderListListnWithparameters:(id)parameters withpageSize:(NSInteger)size pageNum:(NSInteger)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**

获取延保订单明细
@param parameters parameters description

@param completionHander completionHander description
*/
- (void)getEwOrderListInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**

获取保修订单报修列表
@param parameters parameters description
@param size parameters description
@param num parameters description
@param completionHander completionHander description
*/
- (void)getEwRecordListWithparameters:(id)parameters withpageSize:(NSInteger)size pageNum:(NSInteger)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
获取保修订单报修明细
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getEwRecordInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
保修订单报修记录创建
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)createEwRecordOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
保修产品协议说明
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)EwProductPactWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取保修方案的保修里程和车龄
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)caluteMileAgeWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
保养产品列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getCarMatainenceProdutListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
保养产品详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getCarMatainenceProdutInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取保养方案的年份
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)caluteMaintainenceOfYearsWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
保养产品生成价格
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)makePriceForCarMaintainenceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 获取保养订单列表
/// @param parameters parameters description
/// @param size size description
/// @param num 页码
/// @param completionHander completionHander description
- (void)getCarMaintainenceOrderListListnWithparameters:(id)parameters withpageSize:(NSInteger)size pageNum:(NSInteger)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取保养订单详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getEmOrderListInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 获取保养订单保养记录列表
/// @param parameters parameters description
/// @param size size description
/// @param num 页码
/// @param completionHander completionHander description
- (void)getEmRecordListWithparameters:(id)parameters withpageSize:(NSInteger)size pageNum:(NSInteger)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
保养订单保养记录创建
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)createEmRecordOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
保养订单保养记录明细
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getEmRecordInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
保养/保修门店列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getShopListDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 卖家保养保修订单列表
/// @param parameters parameters description
/// @param size size description
/// @param num num description
/// @param type type description
/// @param completionHander completionHander description
- (void)getSellerCarMaintainenceOrderListWithparameters:(id)parameters withpageSize:(NSInteger)size pageNum:(NSInteger)num andType:(NSInteger)type withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 卖家保养保修订单详情
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getEmEwOrderInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 卖家保养保修订单详情根据ID
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getSellerEmEwOrderInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 商家维修开始
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)sellerEwEmRepairStartWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 商家维修结束
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)sellerEwEmRepairEndWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 买家保养确认完成
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)emRepairOrderCompleteWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 商家维修完成提交数据
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)sellerSubmitRepiarationCompleteWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

///申请退换货
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)applyGroupsSalesReturnWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
