//
//  MineApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineApi : NSObject
/**
获取团购商品订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getOrderListWithparameters:(id)parameters withPageNum:(NSInteger)num andPageSize:(NSInteger)size andStatues:(NSInteger)statues withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取商品订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getNormalOrderListWithparameters:(id)parameters withPageNum:(NSInteger)num andPageSize:(NSInteger)size andStatues:(NSInteger)statues withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
取消订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)cancelOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
团购确认收货
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)confirmOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
普通商品确认收货
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)confirmNormalOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
取消普通订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)cancelNormalOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
新增地址
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)addAdressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取用户信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getUserInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取余额数量
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getAmountWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
充值余额
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)rechargeBalanceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
交易记录
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)exchangeRecordWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
返还记录
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getBackRecordWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
提现申请
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)withDrawWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
查看批发进度
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)applyWholesaleWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
申请批发
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)applyWholesaleShopWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取买家类型
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getMemberBuyTypeWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取我的银行卡列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getBankListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
删除我的卡包
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)delMyCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
添加卡包 银行卡=1，加油卡=2
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)addMyCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
上传图片
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)uploadPicWithparameters:(id)parameters VisitImagesArr:(NSArray *)imageArr withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取代理商品订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getRecommendGoodsOrderListWithparameters:(id)parameters withPageNum:(NSInteger)num andPageSize:(NSInteger)size andStatues:(NSInteger)statues withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取代理团购商品订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getRecommendGroupGoodsOrderListWithparameters:(id)parameters withPageNum:(NSInteger)num andPageSize:(NSInteger)size andStatues:(NSInteger)statues withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取代理维修订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getRecommendFixOrderListWithparameters:(id)parameters withPageNum:(NSInteger)num andPageSize:(NSInteger)size andStatues:(NSInteger)statues withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取代理信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getDelegateInfoithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取代理授权范围
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getDelegateScopeWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
个人信息修改
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)memberInfoEditWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取指定业务代理列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getBusinessClerkListWithparameters:(id)parameters  withPageNum:(NSInteger)num andPageSize:(NSInteger)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
查看我的服务经理
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)checkMyServiceManagerWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
搜索服务经理
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)searchServiceManagerWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
绑定服务经理
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)bindServiceManagerWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
意见反馈
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)submitSuggestWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
意见反馈列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getFeedBackListWithparameters:(id)parameters  withPageNum:(NSInteger)num andPageSize:(NSInteger)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
提现
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)submitWithdrawInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取必备工具的item
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getMyUtilitiesWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
修改密码
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)resetPasswordWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
我的团队成员
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getMyGroupWithparameters:(id)parameters  withPageNum:(NSInteger)num andPageSize:(NSInteger)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
授权
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)AuthorizeWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
商家入驻
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopSettleInWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 获取服务卡列表
 @param parameters parameters description
 @param size  parameters description
 @param num  parameters description
 @param completionHander completionHander description
 @param type type description
 */
- (void)getServiceCardListnWithparameters:(id)parameters andType:(NSInteger)type withpageSize:(NSInteger)size pageNum:(NSInteger)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 编辑业务代理信息
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)clerkInfoEditWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 获取指定业务代理的推广项目列表
/// @param parameters parameters description
/// @param completionHander completionHander description
/// @param size size description
/// @param num num description
- (void)getBussinessClerkProjectListnWithparameters:(id)parameters withpageSize:(NSInteger)size pageNum:(NSInteger)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 推广项目详情
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getBussinessClerkProjectInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 测试后台派单
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)selectShopWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 服务小类列表
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getRepairTypeListWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 服务分类列表
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getServiceTypeListWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 申请开店
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)applyShopActWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 获取当前申请开店状态
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getApplyShopStatuesWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
