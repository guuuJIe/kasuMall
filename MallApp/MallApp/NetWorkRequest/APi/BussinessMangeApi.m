//
//  BussinessMangeApi.m
//  MallApp
//
//  Created by Mac on 2020/2/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BussinessMangeApi.h"
@interface BussinessMangeApi()
@property (nonatomic , strong) JSHttpClient *client;
@end
@implementation BussinessMangeApi
- (void)getApplyWholesaleListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
  
    [self.client get:getApplyWholesaleListDataApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getApplyWholesaleDetailWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:getApplyWholesaleDetailApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)ApplyPassedWholesaleWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:ApplyPassedWholesaleApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)shopSettleInContentWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:shopSettleInContentApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)ApplyGrouperWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:applyGrouper parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)userProductListWithparameters:(id)parameters andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@",userProjectList,size,num];
    [self.client get:api parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)systemProductListWithparameters:(id)parameters andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@",systemProjectList,size,num];
    [self.client get:api parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)addProductWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:addPromptProject parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)delProductWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@/%@",delPromptProject,parameters];
    [self.client post:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getGrouperInviteListWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:getInviteInfo parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
           MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
           completionHander(error,body);
    }];
}

- (void)getInviteInfoWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:createInviteInfo parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getProjectOrderListWithparameters:(id _Nullable)parameters  andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@/%@",projectOrderList,parameters,size,num];
    [self.client get:api parameters:nil isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (JSHttpClient *)client
{
    if (!_client) {
        _client = [JSHttpClient shareClient];
    }
    return _client;
}
@end
