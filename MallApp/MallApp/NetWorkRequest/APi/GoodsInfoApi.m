//
//  GoodsInfoApi.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoodsInfoApi.h"
@interface GoodsInfoApi()

@property (nonatomic , strong) JSHttpClient *client;

@end
@implementation GoodsInfoApi

- (void)getGoodsItemWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    
    NSString *api = [NSString stringWithFormat:@"%@%@",productInfoApi,parameters];
    [self.client get:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getGroupGoodsItemWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@",groupProductInfoApi,parameters];
    [self.client get:api parameters:@{} isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)AddinCarWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:addIncarApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)purchaseWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:purchaseApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)groupPurchaseWithparameters:(id)parameters andID:(NSString *)ID andNum:(NSString *)num withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@%@/%@",groupProductpurchaseApi,ID,num];
    [self.client get:api parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getAddressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client get:addressListApi parameters:parameters isNeedHeader:true andPort:@"8011" withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (JSHttpClient *)client
{
    if (!_client) {
        _client = [JSHttpClient shareClient];
    }
    return _client;
}
@end
