//
//  BussinessMangeApi.h
//  MallApp
//
//  Created by Mac on 2020/2/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BussinessMangeApi : NSObject
/**
获取申请批发会员的列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getApplyWholesaleListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取申请批发会员的详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getApplyWholesaleDetailWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
通过批发价申请
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)ApplyPassedWholesaleWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
商家入驻内容
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopSettleInContentWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
用户申请成为团长
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)ApplyGrouperWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
用户自己的推广项目库
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)userProductListWithparameters:(id _Nullable)parameters andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
系统推广项目库
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)systemProductListWithparameters:(id _Nullable)parameters andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
用户从项目库里选择项目
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)addProductWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
用户从项目库中删除指定项目
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)delProductWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
成功邀请的列表+奖励信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getGrouperInviteListWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
邀请团长的分享信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getInviteInfoWithparameters:(id _Nullable)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
推广项目订单佣金列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getProjectOrderListWithparameters:(id _Nullable)parameters  andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
