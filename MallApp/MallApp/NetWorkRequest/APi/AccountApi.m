//
//  AccountApi.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AccountApi.h"

@interface AccountApi()

@property (nonatomic , strong) JSHttpClient *client;

@end

@implementation AccountApi

- (void)accountLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander
{
    [self.client post:appLogin parameters:parameters isNeedHeader:NO withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getverifyCodeWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:sendCodeApi parameters:parameters isNeedHeader:NO withCompletionHandler:^(NSError *error, id result) {
         MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
               completionHander(error,body);
    }];
}

- (void)verifyCodeLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:codeLoginApi parameters:parameters isNeedHeader:NO withCompletionHandler:^(NSError *error, id result) {
         MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
               completionHander(error,body);
    }];
}

- (void)verifyUserCodeLoginWithparameters:(id)parameters withId:(NSString *)userId withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    NSString *api = [NSString stringWithFormat:@"%@?id=%@",codeLoginApi,userId];
    [self.client post:api parameters:parameters isNeedHeader:NO withCompletionHandler:^(NSError *error, id result) {
         MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
               completionHander(error,body);
    }];
}
- (void)getRefreshToeknWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    
    [self.client post:refreshTokenApi parameters:parameters isNeedHeader:true withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}
- (void)AppWxLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:AppWxLoginApi parameters:parameters isNeedHeader:true withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)setPayPasswordWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:setPayPasswordApi parameters:parameters isNeedHeader:true withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)changePayPasswordWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:changePayPasswordApi parameters:parameters isNeedHeader:true withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)quickLoginWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:appQuickLogin parameters:parameters isNeedHeader:false withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)userRegisterWithparameters:(id)parameters withId:(NSString *)userId withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander{
    [self.client post:userRegisterApi parameters:parameters isNeedHeader:false withCompletionHandler:^(NSError *error, id result) {
        MessageBody *body = [MessageBody instanceWithDataResponseObject:result withTheDataModelClass:nil withError:error isArray:NO];
        completionHander(error,body);
    }];
}

- (void)getWechatUniod:(id)parameters withCompletionHandler:(NetworkCompletionHandler)completionHander{
    
    
    [[AFHTTPSessionManager manager] GET:@"https://api.weixin.qq.com/sns/oauth2/access_token" parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHander(nil,responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHander(error,nil);
    }];
}

- (JSHttpClient *)client
{
    if (!_client) {
        _client = [JSHttpClient shareClient];
    }
    return _client;
}
@end
