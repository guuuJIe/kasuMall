//
//  MemberApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface MemberApi : NSObject

/**
添加发票
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)addInVoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
查询维修卡
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)searchFixCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 商家使用服务卡准备工作
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)readyUseFixCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
使用维修卡
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)useFixCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
 卖家维修套餐订单列表
 @param parameters parameters description
 @param completionHander completionHander description
 @param Status Status description
 @param num num description
 @param size size description
 */
- (void)getSellerRepairTcOrderListDataWithparameters:(id)parameters withType:(NSString *)Status andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 会员套餐订单列表
/// @param parameters parameters description
/// @param Status Status description
/// @param num num description
/// @param size size description
/// @param completionHander completionHander description
- (void)getRepairTcOrderListDataWithparameters:(id)parameters withType:(NSString *)Status andWithNum:(NSString *)num andSize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 退卡准备
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)readyReturnCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 退卡申请
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)applyReturnCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 服务卡确认完成
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)confirmServicesCardWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 卖家保修保养商家扫码准备
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)sellerEwEmOrderCheckReadyWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/// 卖家保修保养商家扫码检查
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)sellerEwEmOrderCheckWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/// 获取二维码转义数据
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)getQRCodeDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
