//
//  OrderApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderApi : NSObject
/**
 提交订单
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)submitOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
取消订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)cancelOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
取消普通订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)cancelNormalOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
团购确认收货
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)confirmOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
普通商品确认收货
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)confirmNormalOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
 获取地址
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getAddressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 获取发票列表接口
 @param parameters parameters description
 @param completionHander completionHander description
 */
- (void)getInvoiceListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
设置默认地址
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)setDefaultAdressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
设置默认发票
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)setDefaultInvoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
删除地址
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)delAddressWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
删除发票
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)delInvoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
创建团购订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)createOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
创建普通商品订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)createSingleOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
支付订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)payOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取余额数量
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getAmountWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
团购订单详情 根据orderId
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)groupOrderInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
团购订单详情 根据ordernum
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)groupOrderInfoByOrderNumWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
订单详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)goodsOrderInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
维修订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getFixOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
抢修/施救用户订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getUserFastServiceOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
抢修/施救商家订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getSellerFastServiceOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
 申请取消抢修/施救订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)cancelUserFastSetviceOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
 商家确认取消抢修/施救订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)sellerCancelUserFastSetviceOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
订单补开发票
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)addInvoiceReissuedWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
订单补开发票记录
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)checkInvoiceReissuedInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
卖家补开发票发货
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)sellerSendInvoiceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;


/**
获取当前账户状态信息
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getCurAccountStatuesWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
获取维修订单详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getFixOrderDetailDataWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
商家确认完成维修订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)shopConfirmFixOrderCompleteWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

///申请退换货
/// @param parameters parameters description
/// @param completionHander completionHander description
- (void)applyNormalSalesReturnWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
