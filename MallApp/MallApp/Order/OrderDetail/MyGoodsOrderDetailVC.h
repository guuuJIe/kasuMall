//
//  MyGoodsOrderDetailVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyGoodsOrderDetailVC : BaseViewController
@property (nonatomic,strong) NSString *ids;

@property (nonatomic,assign) NSInteger type; //1普通商品 2团购商品

@property (nonatomic,strong) NSString *orderNum;
@end

NS_ASSUME_NONNULL_END
