//
//  SelectPayWayVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//
#define ViewHeight  CGRectGetHeight(self.view.frame)

#import "OrdePayVC.h"
#import "PayWayCell.h"
#import "PurchaseView.h"
#import "OrderApi.h"
#import "PayMannagerUtil.h"
#import "OrderListVC.h"
#import "SubmitOrderVC.h"
#import "MyGoodOrderListVC.h"
#import "SetPwdPopView.h"
#import "SetPayVC.h"
#import "IQKeyboardManager.h"
#import "CustomePwdView.h"
@interface OrdePayVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) UILabel *TitleLabel;
@property (nonatomic) PurchaseView *payBtn;
@property (nonatomic) NSMutableArray *dataArr;
@property (nonatomic) NSIndexPath *selectIndexPath;
@property (nonatomic) NSInteger payway;
@property (nonatomic) OrderApi *api;
@property (nonatomic) NSArray *amountArr;
@property (nonatomic) NSInteger isCallPay;
@property (nonatomic) NSDictionary *statuesDic;
@property (nonatomic) SetPwdPopView *setPwdView;
@property (nonatomic) CustomePwdView *pwdView;
@end

@implementation OrdePayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"选择支付方式";
    [self setupUI];
    
    self.payway = 5;
    

    // 添加通知监听见键盘弹出/退出
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillHideNotification object:nil];
    
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //TODO: 页面appear 禁用
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [self.pwdView.passView.textF resignFirstResponder];
    [self getBalance];
    [self getMemeberStatues];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    //TODO: 页面Disappear 启用
   [[IQKeyboardManager sharedManager] setEnable:YES];
//   [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark UITextFieldDelegate
// 点击非TextField区域取消第一响应者
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
     [self.pwdView.passView.textF resignFirstResponder];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    //方式一
    [self.view endEditing:YES];
   
}


- (void)setupUI{
    [self.view addSubview:self.TitleLabel];
    [self.TitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(44);
        make.centerX.equalTo(self.view);
    }];
    
    [self.view addSubview:self.TabelView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.TitleLabel.mas_bottom).offset(24);
        make.left.equalTo(@12);
        make.right.equalTo(@(-12));
        make.height.mas_equalTo(184);
    }];
    
    [self.view addSubview:self.payBtn];
    [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"PayWayCell";
    PayWayCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[PayWayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
    [cell setAmount:self.amountArr];
    [cell setupData:self.dataArr[indexPath.row]];
    if (_selectIndexPath.row == indexPath.row) {
        cell.selectBtn.selected = true;
    }
    if (indexPath.row != 0) {
        cell.leftAmountLbl.hidden = true;
    }else{
        cell.leftAmountLbl.hidden = false;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectIndexPath = indexPath;
    if (indexPath.row == 0) {
        self.payway = 5;
    }else if (indexPath.row == 1){//支付宝支付
        self.payway = 3;
       
    }else if(indexPath.row == 2){//微信支付
        self.payway = 2;
    }
    [_TabelView reloadData];
}

- (void)getBalance{
    NSDictionary *dic = self.priceArr;
//    if ([self.from isEqualToString:@"1"]) {//订单列表进来哦
        self.TitleLabel.text = [NSString stringWithFormat:@"¥%@元",dic[@"realPrice"]];
//    }else{
//         self.TitleLabel.text = [NSString stringWithFormat:@"¥%@元",_totalPrice];
//    }
    
    [self.api getAmountWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            self.amountArr = dic[@"list"];
            [self.TabelView reloadData];
        }
    }];
}

- (void)getMemeberStatues{
    [self.api getCurAccountStatuesWithparameters:@"" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.statuesDic = datas.firstObject;
           
        }
    }];
}

//- (void)backBtnClick{
////    if (self.isCallPay == 1) {
////        if (![self.from isEqualToString:@"1"]) {
////          [self.navigationController popToRootViewControllerAnimated:true];
////        }else{
////          [self.navigationController popViewControllerAnimated:true];
////        }
////         
////    }else{
////        [self.navigationController popViewControllerAnimated:true];
////    }
//   
//}

- (void)payOrder{
    if (self.payway == 5) {
        if ([self.statuesDic[@"payStatus"] integerValue] == 1) {
            [self.pwdView.passView.textF becomeFirstResponder];
            
            return;
        }else{
            JLog(@"没设置");
            [self.setPwdView show];
            WeakSelf(self)
            self.setPwdView.clickBlock = ^{
                
                SetPayVC *vc = [[SetPayVC alloc] initWithNibName:@"SetPayVC" bundle:nil];
                [weakself.navigationController pushViewController:vc animated:true];
            };
            return;
        }
        
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:@"APP" forKey:@"clientType"];
    [dic setValue:@(self.payway) forKey:@"payment"];
//    if ([self.from isEqualToString:@"1"]) {//订单列表进来
        
    [dic setValue:self.priceArr[@"orderNumber"] forKey:@"outTradeNo"];
//    }else if ([self.from isEqualToString:@"2"]){
//        [dic setValue:self.orderDataArr[1] forKey:@"outTradeNo"];
//    }else{
//        [dic setValue:self.dataArr[1] forKey:@"outTradeNo"];
//    }
    //普通买家
    [self.api payOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arguData = dic[@"list"];
            if (self.payway == 3) {//支付宝
                [[PayMannagerUtil sharedManager] payWithZhifubaoMethod:arguData.firstObject successBlock:^(BOOL success) {
                    if (success) {
                        [self paySuccess];
                    }
                } failBlock:^(BOOL fail) {
                    self.isCallPay = 1;
                    [JMBManager showBriefAlert:@"支付失败"];
                }];
            }else if(self.payway == 2){//微信
                [[PayMannagerUtil sharedManager] payWithWeChatMethod:arguData.firstObject successBlock:^(BaseResp * _Nonnull res) {
                    switch (res.errCode) {
                        case WXSuccess:{
                            [self paySuccess];
                        }
                            
                            break;
                            
                        default:{
                            self.isCallPay = 1;
                            [JMBManager showBriefAlert:@"支付失败"];
                        }
                            break;
                    }
                } failBlock:^(BOOL fail) {
                    self.isCallPay = 1;
                }];
            }else if (self.payway == 5){
                [self paySuccess];
            }
            
        }
       
    }];
}

- (void)paySuccess{
    if ([self.from integerValue] == 1) {
        [self.navigationController pushViewController:[MyGoodOrderListVC new] animated:true];
    }else{
        [self.navigationController pushViewController:[OrderListVC new] animated:true];
       
      
    }
    
}

// 键盘监听事件
- (void)keyboardAction:(NSNotification*)sender{
    // 通过通知对象获取键盘frame: [value CGRectValue]
    NSDictionary *useInfo = [sender userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    //取出键盘弹出需要花费的时间
    double duration = [useInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // <注意>具有约束的控件通过改变约束值进行frame的改变处理
    if([sender.name isEqualToString:UIKeyboardWillShowNotification]){
//        self.toBottom.constant = [value CGRectValue].size.height;
         [UIView animateWithDuration:duration animations:^{
             self.pwdView.frame = CGRectMake(0, self.view.frame.size.height - 200 - [value CGRectValue].size.height, Screen_Width, 200);
         }];
    }else{
        [UIView animateWithDuration:1 animations:^{
            self.pwdView.frame = CGRectMake(0, self.view.frame.size.height, Screen_Width, 200);
        }];
    }
}


#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"PayWayCell" bundle:nil] forCellReuseIdentifier:@"PayWayCell"];
        _TabelView.scrollEnabled = false;
//
//        [_TabelView registerNib:[UINib nibWithNibName:@"OrderItemCell" bundle:nil] forCellReuseIdentifier:@"OrderItemCell"];
//         [_TabelView registerNib:[UINib nibWithNibName:@"OrderfooterCell" bundle:nil] forCellReuseIdentifier:@"OrderfooterCell"];
//         [_TabelView registerNib:[UINib nibWithNibName:@"AddressIsNoneCell" bundle:nil] forCellReuseIdentifier:@"AddressIsNoneCell"];
//        _TabelView.backgroundColor = UIColorEF;
        _TabelView.backgroundColor = [UIColor whiteColor];
        _TabelView.layer.cornerRadius = 8;
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
        NSArray *arr = @[@{@"name":@"余额支付",@"image":@"pay_1"},@{@"name":@"支付宝支付",@"image":@"pay_2"},@{@"name":@"微信支付",@"image":@"pay_3"}];
        [_dataArr addObjectsFromArray:arr];
    }
    
    return _dataArr;
}

-(UILabel *)TitleLabel
{
    if(!_TitleLabel)
    {
        _TitleLabel=[UILabel  new];
        _TitleLabel.font = [UIFont boldSystemFontOfSize:32];
        _TitleLabel.textColor = [UIColor colorWithHexString:@"666666"];
        _TitleLabel.numberOfLines = 1;
        _TitleLabel.text = @"89.00元";
//        _goodsTitleLabel.alpha = 0.2;
    }
    return _TitleLabel;
}

- (PurchaseView *)payBtn{
    if (!_payBtn) {
        _payBtn = [PurchaseView new];
        [_payBtn.purchaseButton setTitle:@"确认支付" forState:0];
        WeakSelf(self);
        _payBtn.addAddressBlock = ^{
            
            [weakself payOrder];

        };
        
    }
    return _payBtn;
}

- (SetPwdPopView *)setPwdView{
    if (!_setPwdView) {
        _setPwdView = [SetPwdPopView new];
    }
    return _setPwdView;
}

- (CustomePwdView *)pwdView{
    if (!_pwdView) {
        _pwdView = [[CustomePwdView alloc] initWithFrame:CGRectMake(0, ViewHeight, Screen_Width, 200)];
        _pwdView.passView.textF.keyboardType = UIKeyboardTypeNumberPad;
        WeakSelf(self)
        _pwdView.finishBlock = ^(NSString * _Nonnull str) {
            StrongSelf(self)
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic setValue:@"APP" forKey:@"clientType"];
            [dic setValue:@(self.payway) forKey:@"payment"];
            [dic setValue:str forKey:@"Password"];
            [dic setValue:self.priceArr[@"orderNumber"] forKey:@"outTradeNo"];
            
            //普通买家
            [self.api payOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    NSDictionary *dic = result.resultDic;
                    [self.pwdView.passView.textF resignFirstResponder];
                    if (self.payway == 5){
                        [self paySuccess];
                    }
                    
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付密码不正确" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *act1 = [UIAlertAction actionWithTitle:@"忘记密码" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                        [self.pwdView.passView.textF resignFirstResponder];
                        [self.pwdView.passView cleanPassword];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            SetPayVC *vc = [[SetPayVC alloc] initWithNibName:@"SetPayVC" bundle:nil];
                                                   [self.navigationController pushViewController:vc animated:true];
                        });
                       
                        
                    }];
                    UIAlertAction *act2 = [UIAlertAction actionWithTitle:@"重新输入" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self.pwdView.passView cleanPassword];
//                        [self.pwdView.passView.textF becomeFirstResponder];
                    }];
                    
                    [alert addAction:act1];
                    [alert addAction:act2];
                    [self presentViewController:alert animated:true completion:nil];
                }
                
            }];
        };
        [self.view addSubview:_pwdView];
    }
    return _pwdView;
}


- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}
@end
