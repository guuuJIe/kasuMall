//
//  WeixiuTaocanOrderDetailVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/22.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WeixiuTaocanOrderDetailVC : BaseViewController
@property (nonatomic,strong) NSString *ids;
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,strong) NSString *orderNum;
@end

NS_ASSUME_NONNULL_END
