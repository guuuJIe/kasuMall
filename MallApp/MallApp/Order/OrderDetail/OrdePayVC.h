//
//  OrdePayVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrdePayVC : BaseViewController
@property (nonatomic) NSDictionary *priceArr;
@property (nonatomic) NSString *from; //1商品 2 团购
@end

NS_ASSUME_NONNULL_END
