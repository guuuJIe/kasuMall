//
//  GoodsOrderDetailSectionSixCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsOrderDetailSectionSixCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *wayLbl;
@property (weak, nonatomic) IBOutlet UILabel *fapiaoLbl;
@property (weak, nonatomic) IBOutlet UILabel *companyLbl;
@property (weak, nonatomic) IBOutlet UILabel *nashuiNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *goodsPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *feeLbl;
@property (weak, nonatomic) IBOutlet UILabel *youhuiLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLbl;
- (void)setupData:(NSDictionary *)dic withType:(NSInteger)type;
- (IBAction)pasteAct:(UIButton *)sender;
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
