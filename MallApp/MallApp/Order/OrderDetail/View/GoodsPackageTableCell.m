//
//  GoodsPackageTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoodsPackageTableCell.h"

@implementation GoodsPackageTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    NSInteger stauts = [dic[@"orderStatus"] integerValue];
    if (stauts == 5 || stauts == 4 || stauts == 6 || stauts == 33) {
        self.stateImage.hidden = false;
        self.stateLbl.hidden = false;
        self.statename.hidden = false;
        self.stateLbl.text = [NSString stringWithFormat:@"%@ 编号:%@",[XJUtil insertStringWithNotNullObject:dic[@"kdType"] andDefailtInfo:@""],[XJUtil insertStringWithNotNullObject:dic[@"kdBh"] andDefailtInfo:@""]];
    }
    if (stauts == 3) {
        self.stateImage.hidden = false;
               self.stateLbl.hidden = false;
               self.statename.hidden = false;
//        self.stateLbl.text = @"";
    }
    
    self.statename.text = [XJUtil insertStringWithNotNullObject:dic[@"kdTime"] andDefailtInfo:@""];
   
}

@end
