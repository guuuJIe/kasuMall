//
//  GoodsOrderDetailSectionSixCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoodsOrderDetailSectionSixCell.h"
@interface GoodsOrderDetailSectionSixCell()
@property (weak, nonatomic) IBOutlet UIButton *clickBtn;

@end
@implementation GoodsOrderDetailSectionSixCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected stateG376219091058
}
- (void)setupData:(NSDictionary *)dic withType:(NSInteger)type{
    if (dic) {
        
        self.numLbl.text = dic[@"orderNumber"];
        self.timeLbl.text = dic[@"createTime"];
        self.wayLbl.text = dic[@"paymant"];
        NSDictionary *invoiceDic = dic[@"invoice"];
        NSInteger statues = [invoiceDic[@"status"] integerValue];
        if (type == 1) {
            if (![invoiceDic[@"type"] isEqual:[NSNull null]]) {
                self.clickBtn.hidden = false;
                self.fapiaoLbl.text = invoiceDic[@"type"];
            }else{
                if (statues == 0) {
                    self.fapiaoLbl.text = @"暂无发票";
                    self.clickBtn.hidden = false;
                }else if (statues == 1){
                    self.fapiaoLbl.text = @"补开发票处理中";
                    self.clickBtn.hidden = false;
                }else if (statues == 2){
                    self.fapiaoLbl.text = @"查看补开发票";
                    self.clickBtn.hidden = false;
                }
            }
            
//            self.fapiaoLbl.text = [XJUtil insertStringWithNotNullObject:invoiceDic[@"type"] andDefailtInfo:@"暂无发票"];
            
        }else{
            self.clickBtn.hidden = true;
            self.fapiaoLbl.text = @"不开发票";
        }
        self.companyLbl.text = [XJUtil insertStringWithNotNullObject:invoiceDic[@"title"] andDefailtInfo:@""];
        self.nashuiNumLbl.text = [XJUtil insertStringWithNotNullObject:invoiceDic[@"sh"] andDefailtInfo:@""];
        self.goodsPriceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
        self.feeLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"freight"]];
        self.totalMoneyLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
        self.youhuiLbl.text = [NSString stringWithFormat:@"¥0"];
    }
}

- (IBAction)pasteAct:(UIButton *)sender {
    UIPasteboard* pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:self.numLbl.text];
}
- (IBAction)applyAct:(id)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}

@end
