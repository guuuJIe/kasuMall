//
//  GoodsPackageTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsPackageTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *stateImage;
@property (weak, nonatomic) IBOutlet UILabel *stateLbl;
@property (weak, nonatomic) IBOutlet UILabel *statename;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
