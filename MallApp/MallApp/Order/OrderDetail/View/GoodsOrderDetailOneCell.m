//
//  GoodsOrderDetailOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoodsOrderDetailOneCell.h"
#import "CountDown.h"
@interface GoodsOrderDetailOneCell()
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (nonatomic, strong) CountDown *countDown;
@property (nonatomic, assign)int nowTime;
@end

@implementation GoodsOrderDetailOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
     //orderStatus 6 交易完成 3是待收货 4是待发货 5//已完成
    NSInteger statues = [dic[@"orderStatus"] integerValue];
    if (statues == 6) {
         self.statusLbl.text = @"交易完成";
        self.timeLbl.hidden = true;
    }else if (statues == 3){
        self.statusLbl.text = @"等待发货";
        self.timeLbl.hidden = true;
    }else if (statues == 2){
        self.statusLbl.text = @"等待买家付款";
        self.timeLbl.hidden = true;
    }else if (statues == 4){
        self.statusLbl.text = @"卖家已发货";
        if (dic[@"orderTime"][@"autoReceivedTime"]) {
            self.timeLbl.hidden = false;
            NSString *limittime = [NSString stringWithFormat:@"%ld",(long)[XJUtil timeSwitchTimestamp:dic[@"orderTime"][@"autoReceivedTime"]]];
            NSString *nowtime = [XJUtil getNowTimeStamp];
            self.nowTime = [limittime intValue] - [nowtime intValue];
            [self.countDown countDownWithStratTimeStamp:self.nowTime  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
                //        NSLog(@"%ld",(long)day);
                self.timeLbl.text=[NSString stringWithFormat:@"还剩:%li天%li时后自动确认收货",(long)day,(long)hour];
                //        [self refreshUIDay:day hour:hour minute:minute second:second];
            }];
        }
        
    }else if (statues == 5){
        self.statusLbl.text = @"等待买家评价";
        self.timeLbl.hidden = true;
    }else if (statues == 33){
        self.statusLbl.text = @"退款、退货已完成";
        self.timeLbl.hidden = true;
    }else if (statues == 31){
        self.statusLbl.text = @"处理中";
        self.timeLbl.hidden = true;
    }else if (statues == 32){
        self.statusLbl.text = @"退款中";
        self.timeLbl.hidden = true;
    }
   
}




- (CountDown *)countDown{
    if (!_countDown) {
        _countDown = [CountDown new];
    }
    return _countDown;
}

@end
