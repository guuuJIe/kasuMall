//
//  SubmitInvoiceVC.m
//  MallApp
//
//  Created by Mac on 2020/2/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitInvoiceVC.h"
#import "PurchaseView.h"
#import "OrderApi.h"
@interface SubmitInvoiceVC ()
@property (nonatomic, strong) PurchaseView *submitView;
@property (nonatomic, strong) OrderApi *api;
@property (weak, nonatomic) IBOutlet UIView *invoiceStatuesView;
@property (weak, nonatomic) IBOutlet UILabel *statuesLbl;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *companyLbl;
@property (weak, nonatomic) IBOutlet UIView *wuLiuCompanyView;
@property (weak, nonatomic) IBOutlet UIView *kdNumView;
@property (weak, nonatomic) IBOutlet UILabel *kdNumLbl;
@end

@implementation SubmitInvoiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"发票详情";
    
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"ApplyforInvoiceVC",@"FapiaoVC"]];
    
    [self setupUI];
    
    [self setupData];
}

- (void)setupData{
    self.typeLbl.text = self.dic[@"type"];
    self.nameLbl.text = self.dic[@"title"];
    self.shibieNumLbl.text = self.dic[@"sh"];
    self.bankTypeLbl.text = self.dic[@"bank"];
    self.bankNumLbl.text = self.dic[@"bankNumber"];
    self.adressLbl.text = self.dic[@"address"];
    self.telLbl.text = self.dic[@"tel"];
    if (self.statues == 1) {
        self.invoiceStatuesView.hidden = false;
        self.statuesLbl.text = @"补开发票处理中";
        self.submitView.hidden = true;
    }else if (self.statues == 2){
        
        [self checkInvoiceInfo];
       
    }

}

- (void)checkInvoiceInfo{
    [self.api checkInvoiceReissuedInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            [self configData:datas.firstObject];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    
    self.invoiceStatuesView.hidden = false;
    self.timeView.hidden = false;
    self.kdNumView.hidden = false;
    self.wuLiuCompanyView.hidden = false;
    self.statuesLbl.text = @"已处理";
    self.submitView.hidden = true;
   
    self.companyLbl.text = dic[@"kd"];
    self.kdNumLbl.text = dic[@"kdbh"];
    self.timeLbl.text = dic[@"kdTime"];
}



- (void)setupUI{
    [self.view addSubview:self.submitView];
    [self.submitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (PurchaseView *)submitView{
    if (!_submitView) {
        _submitView = [PurchaseView new];
        [_submitView.purchaseButton setTitle:@"提交申请" forState:0];
        WeakSelf(self)
        _submitView.addAddressBlock = ^{
            StrongSelf(self)
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否使用当前发票？" andHandler:^(NSInteger index) {
                if (index == 1) {
                    NSDictionary *dic = @{@"ordernumber":self.orderNum,@"fpid":self.dic[@"id"]};
                    [self.api addInvoiceReissuedWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                        StrongSelf(self)
                        if (result.code == 200) {
                            [JMBManager showBriefAlert:@"提交成功"];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshOrderDetail" object:nil];
                            [self.navigationController popViewControllerAnimated:true];
                            
//                            [self checkInvoiceInfo];
                        }
                    }];
                }
            }];
        };
        
    }
    return _submitView;
}

- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}

@end
