//
//  SelectPayWayVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectPayWayVC : BaseViewController
@property (nonatomic) NSArray *orderDataArr;
@property (nonatomic) NSDictionary *priceArr;
@property (nonatomic) NSString *from; //1从列表进
@property (nonatomic) NSString *detailFrom; //详情页
@property (nonatomic) NSString *totalPrice; //总价
@property (nonatomic) NSString *jumpVCName; //要跳转的控制器名字
@end

NS_ASSUME_NONNULL_END
