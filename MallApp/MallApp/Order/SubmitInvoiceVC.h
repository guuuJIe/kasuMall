//
//  SubmitInvoiceVC.h
//  MallApp
//
//  Created by Mac on 2020/2/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubmitInvoiceVC : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *shibieNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *adressLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic,strong) NSString *orderNum;
@property (nonatomic,assign) NSInteger statues;
@end

NS_ASSUME_NONNULL_END
