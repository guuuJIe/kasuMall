//
//  ApplyforInvoiceVC.m
//  MallApp
//
//  Created by Mac on 2020/2/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplyforInvoiceVC.h"
#import "PurchaseView.h"
#import "FapiaoVC.h"
@interface ApplyforInvoiceVC ()
@property (nonatomic, strong) PurchaseView *makeupView;
@property (nonatomic, strong) UILabel *titleLabel;
@end

@implementation ApplyforInvoiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"申请发票";
    
    [self setupUI];
}

- (void)setupUI{
    [self.view addSubview:self.makeupView];
    [self.makeupView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    UIImageView *imageView = [UIImageView new];
    [self.view addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"invoicePic"];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.view).offset(-30);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(imageView.mas_bottom).offset(12);
    }];
    
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.textColor = [UIColor colorWithHexString:@"666666"];
        _titleLabel.numberOfLines = 1;
        _titleLabel.text = @"您还没有申请开具发票";

    }
    return _titleLabel;
}

- (PurchaseView *)makeupView{
    if (!_makeupView) {
        _makeupView = [PurchaseView new];
        [_makeupView.purchaseButton setTitle:@"补开发票" forState:0];
        WeakSelf(self)
        _makeupView.addAddressBlock = ^{
            StrongSelf(self)
            FapiaoVC *vc = [FapiaoVC new];
            vc.tag = 1;
            vc.orderNum = self.orderNum;
            [self.navigationController pushViewController:vc animated:true];
        };
    }
    
    return _makeupView;
}

@end
