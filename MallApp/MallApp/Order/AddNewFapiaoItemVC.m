//
//  AddNewFapiaoItemVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddNewFapiaoItemVC.h"
#import "MemberApi.h"
@interface AddNewFapiaoItemVC ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *taitouText;
@property (weak, nonatomic) IBOutlet UITextField *nashuirenText;
@property (weak, nonatomic) IBOutlet UITextField *yinhangtext;
@property (weak, nonatomic) IBOutlet UITextField *yinghangNumText;
@property (weak, nonatomic) IBOutlet UITextField *addressText;
@property (weak, nonatomic) IBOutlet UITextField *telText;

@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) NSArray *data;
@property (nonatomic, strong) MemberApi *memberApi;
@property (nonatomic, assign) BOOL isDefault;
@end

@implementation AddNewFapiaoItemVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"新建发票";
    self.view.backgroundColor = [UIColor whiteColor];
    self.data = @[@"普通发票",@"增值税发票"];

    self.dataPickerView.delegate = self;
    self.dataPickerView.dataSource = self;
    self.isDefault = true;
}
- (IBAction)isDefault:(UISwitch *)sender {
    self.isDefault = sender.isOn;
    if (self.isDefault) {
        NSLog(@"11");
        self.isDefault = sender.isOn;
        NSLog(@"22");
        self.isDefault = sender.isOn;
    }
}
- (IBAction)confirmAct:(UIButton *)sender {
    NSLog(@"点击了");
    
//        NSMutableDictionary *dic = [NSMutableDictionary new];
//        [dic setValue:self.adress.text forKey:@"address"];
//        [dic setValue:self.bankType.text forKey:@"bank"];
//        [dic setValue:self.bankNum forKey:@"bankNumber"];
//        [dic setValue:self.strCity forKey:@"sh"];
//        [dic setValue:self.strArea forKey:@"strArea"];
//        [dic setValue:self.detailText forKey:@"street"];
//        [dic setValue:@(self.isDefault) forKey:@"tuiJian"];
//        [dic setValue:userId forKey:@"userId"];
    NSDictionary *dic = @{
        @"address":self.adress.text,
        @"bank":self.bankType.text,
        @"bankNumber":self.bankNum.text,
        @"sh":self.shibiehaoNumLbl.text,
        @"tel":self.tel.text,
        @"userId":usersID,
        @"title":self.TaitouLbl.text,
        @"tuiJian":@(self.isDefault),
        @"type":self.fapiaoTypeLbl.titleLabel.text,
        @"id":@0,
    };
    WeakSelf(self)
    [self.memberApi addInVoiceWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [weakself.navigationController popViewControllerAnimated:true];
        }
    }];
}


- (IBAction)setDefault:(UISwitch *)sender {
}

- (IBAction)selectFapiao:(id)sender {
    self.dataPickerView.hidden = false;
//    [self.view addSubview:self.pickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.data.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.data[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self.fapiaoTypeLbl setTitle:self.data[row] forState:0];
    self.dataPickerView.hidden = true;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    self.dataPickerView.hidden = true;
}

- (MemberApi *)memberApi{
    if (!_memberApi) {
        _memberApi = [MemberApi new];
    }
    return _memberApi;
}
@end
