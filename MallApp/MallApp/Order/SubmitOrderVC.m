//
//  SubmitOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitOrderVC.h"
#import "OrderInfoOneCell.h"
#import "OrderHeadView.h"
#import "OrderItemCell.h"
#import "OrderfooterCell.h"
#import "AccountView.h"
#import "OrderFootView.h"
#import "AddressIsNoneCell.h"
#import "SelectPayWayVC.h"
#import "FapiaoView.h"
#import "FapiaoVC.h"
#import "AdressListVC.h"
#import "OrderApi.h"
#import "AdressListVC.h"
#import "GoodsInfoApi.h"
@interface SubmitOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *TabelView;
@property (nonatomic,strong)AccountView *accoutView;
@property (nonatomic,strong)FapiaoView *fapiaoView;
@property (nonatomic,strong)OrderApi *orderApi;
@property (nonatomic,strong)NSArray *adressArr;
@property (nonatomic,strong)NSArray *fapiaoArr;
@property (nonatomic,strong)NSDictionary *feeDic;
@property (nonatomic,strong)NSString *totalPrice;
@property (nonatomic,assign)NSInteger invoiceID;
@property (nonatomic,strong)GoodsInfoApi *goodsApi;
@property (nonatomic,assign)BOOL isfirst;//默认设置一次发票
@property (nonatomic,strong)NSString *fapiaoStr;
@property (nonatomic,strong)UITextField *noteText;
@end

@implementation SubmitOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"提交订单";
    [self setupUI];
    
    [self initProp];
    [self getFreight];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setData];
    
}

- (void)initProp{
    self.invoiceID = 0;
    self.isfirst = true;
    self.fapiaoStr = @"不开发票";
}
- (void)setupUI
{
    
    [self.view addSubview:self.TabelView];
    [self.view addSubview:self.accoutView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.accoutView.mas_top);
    }];
    
    
    [self.accoutView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)getFreight{
    if ([self.from integerValue] == 1) {
        if ([self.adressId integerValue] == 0.0) {
            self.freight = @"0";
            [self.accoutView setUpData:self.dataSourceArr.firstObject andNum:[self.num integerValue]  andFreight:[self.freight floatValue]];
        }else{
            [self.goodsApi groupPurchaseWithparameters:@{@"Address":self.adressId} andID:self.goodsId andNum:self.num withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    NSDictionary *freghtdic = result.resultDic;
                    NSArray *arr = freghtdic[@"list"];
                     
                    self.freight = [NSString stringWithFormat:@"%@",arr.firstObject];
                    [self.accoutView setUpData:self.dataSourceArr.firstObject andNum:[self.num integerValue]  andFreight:[self.freight floatValue]];
                    [self.TabelView reloadData];
                    
                }
            }];
        }
        
    }
    
}

- (void)setData{
    
    [self.orderApi getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            self.adressArr = arr;
            if (arr.count != 0) {
                NSDictionary *dic = arr.firstObject;
                self.adressId = dic[@"id"];
            }else{
                self.adressId = 0;
            }
            if ([self.from integerValue] != 1) {
                NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                [parameters setValue:self.purchaseId forKey:@"Cids"];
                [parameters setValue:self.adressId forKey:@"Address"];
                [self.orderApi submitOrderWithparameters:parameters withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
//                        NSDictionary *dic2 = result.resultDic;
                        NSArray *data = result.result;
                        NSDictionary *shopData = data.firstObject;
                        NSArray *shops = shopData[@"cartShops"];
                        NSDictionary *dic3 = shops.firstObject;
                        self.feeDic = dic3;
                        self.dataSourceArr = dic3[@"cartProducts"];
                        [self.accoutView setUpOtherData:self.feeDic andNum:[self.num integerValue]];
                        [self.TabelView reloadData];
                    }
                    
                    
                }];
                
                [self.orderApi getInvoiceListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        NSDictionary *dic = result.resultDic;
                        NSArray *arr = dic[@"list"];
                        self.fapiaoArr = arr;
                        if (self.isfirst) {
                            [self.fapiaoView setDic:arr.firstObject];
                        }
                       
                    }
                }];
            }else{
               self.feeDic = self.dataSourceArr.firstObject;
              
               [self.TabelView reloadData];
            }
           
            
        }
    }];
    
   
   
}

- (void)createOrder{
    if ([self.from integerValue] == 1) {
        CGFloat price = [self.feeDic[@"price"] floatValue];
        CGFloat goodsPrice = price * [self.num integerValue];
        CGFloat total = goodsPrice +[self.freight floatValue];
        self.totalPrice = [NSString stringWithFormat:@"%.3f",total];
        //先测试团购
        NSDictionary *group = self.dataSourceArr.firstObject;
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setValue:self.adressId forKey:@"addressId"];
        [dic setValue:group[@"id"] forKey:@"groupProId"];
        [dic setValue:@0 forKey:@"cardId"];
        [dic setValue:@0 forKey:@"payment"];//支付方式
        [dic setValue:@0 forKey:@"invoiceId"];//发票ID
        [dic setValue:@0 forKey:@"stageNum"];//发票
        [dic setValue:self.num forKey:@"quautity"];//数量
        [dic setValue:@0 forKey:@"cartClass"];//数量
        [dic setValue:self.noteText.text forKey:@"ShopMessage"];
        [dic setValue:@([self.productInfo[@"type"] integerValue]) forKey:@"GroupType"];
        [dic setValue:@([self.productInfo[@"stageNum"] integerValue]) forKey:@"StageNum"];
        NSInteger ids = [clerkid integerValue];
        if (ids) {
            [dic setValue:@(ids) forKey:@"tgcode"];//代理id
        }
        [self.orderApi createOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSDictionary *dic = result.resultDic;
                SelectPayWayVC *vc = [SelectPayWayVC new];
                vc.from = @"2";
                vc.orderDataArr = dic[@"list"];
                vc.priceArr = self.dataSourceArr[0];
                vc.totalPrice = self.totalPrice;
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clerkid"];
                [self.navigationController pushViewController:vc animated:true];
            }
        }];
    }else{
        //单个商品购买
        CGFloat price = [self.feeDic[@"sPrice"] floatValue];
        CGFloat freight = [self.feeDic[@"freight"] floatValue];
        CGFloat total = price+freight;
        self.totalPrice = [NSString stringWithFormat:@"%.3f",total];
//        NSDictionary *group = self.dataSourceArr.firstObject;
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setValue:self.adressId forKey:@"addressId"];
        [dic setValue:self.purchaseId forKey:@"cartIds"];
        [dic setValue:@0 forKey:@"cardId"];
        [dic setValue:@0 forKey:@"payment"];//支付方式
        [dic setValue:@(self.invoiceID) forKey:@"invoiceId"];//发票ID
//        [dic setValue:@0 forKey:@"stageNum"];//发票
//        [dic setValue:self.num forKey:@"quautity"];//数量
//        [dic setValue:@0 forKey:@"cartClass"];//数量
        [dic setValue:self.noteText.text forKey:@"ShopMessage"];
        NSInteger ids = [clerkid integerValue];
        if (ids) {
            [dic setValue:@(ids) forKey:@"tgcode"];//代理id
        }
        [self.orderApi createSingleOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSDictionary *dic = result.resultDic;
                SelectPayWayVC *vc = [SelectPayWayVC new];
                vc.from = @"2";
                vc.orderDataArr = dic[@"list"];
                vc.priceArr = self.dataSourceArr[0];
                vc.totalPrice = self.totalPrice;
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clerkid"];
                [self.navigationController pushViewController:vc animated:true];
            }
        }];
    }
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else if(section == 1){
        return self.dataSourceArr.count;
    }else{
        return 1;
    }
    
   
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if ([self.adressId integerValue] != 0) {
            static NSString *Identifier = @"OrderInfoOneCell";
            OrderInfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
            if (cell == nil) {
                cell = [[OrderInfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
            }
            cell.adressBlock = ^{
                AdressListVC *vc = [AdressListVC new];
                vc.setAdressBlock = ^(NSDictionary * _Nonnull dic) {
                    self.adressId = [NSString stringWithFormat:@"%@",dic[@"id"]];
                    [self getFreight];
                };
                [self.navigationController pushViewController:vc animated:true];
            };
            [cell setupData:self.adressArr.firstObject];
            return cell;
            
        }else{
            
            static NSString *Identifier = @"AddressIsNoneCell";
            AddressIsNoneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
            if (cell == nil) {
                cell = [[AddressIsNoneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
            }
            
            cell.addBlock = ^{
                AdressListVC *vc = [AdressListVC new];
//                vc.setAdressBlock = ^{
//                    [self getFreight];
//                };
                vc.setAdressBlock = ^(NSDictionary * _Nonnull dic) {
                    self.adressId = [NSString stringWithFormat:@"%@",dic[@"id"]];
                    [self getFreight];
                };
                
                [self.navigationController pushViewController:vc animated:true];
            };
            
            return cell;
        }
  
    }else if(indexPath.section == 1){
        
        
        
        static NSString *Identifier = @"OrderItemCell";
        OrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[OrderItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        [cell setupData:self.dataSourceArr[indexPath.row] withType:[self.from integerValue] andNum:self.num];
        return cell;
    }else{
        static NSString *Identifier = @"OrderfooterCell";
        OrderfooterCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[OrderfooterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        [cell setupData:self.feeDic withType:[self.from integerValue] andPrice:self.feeDic[@"price"] andFee:self.freight andFapiaoData:self.fapiaoArr.firstObject and:[self.num integerValue]];
        if ([self.from integerValue] != 1) {
            [cell.fabiaoLbl setTitle:self.fapiaoStr forState:0];
        }
        self.noteText = cell.noteLbl;
        WeakSelf(cell);
        cell.clickBlock = ^{
            if ([self.from integerValue] != 1) {//团购不需要开发票
                [self.fapiaoView show];
            }else{
//                [self.fapiaoView show];
                [weakcell.fabiaoLbl setTitle:@"团购不需要开发票" forState:0];
            }
            
        };
        
        return cell;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 50.0f;
    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return [OrderHeadView new];
    }
    return [UIView new];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return [OrderFootView new];
    }
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return 10;
    }
    
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [self.navigationController pushViewController:[TuanGouGoodInfoVC new] animated:true];
}

#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"OrderInfoOneCell" bundle:nil] forCellReuseIdentifier:@"OrderInfoOneCell"];
        
        [_TabelView registerNib:[UINib nibWithNibName:@"OrderItemCell" bundle:nil] forCellReuseIdentifier:@"OrderItemCell"];
        [_TabelView registerNib:[UINib nibWithNibName:@"OrderfooterCell" bundle:nil] forCellReuseIdentifier:@"OrderfooterCell"];
        [_TabelView registerNib:[UINib nibWithNibName:@"AddressIsNoneCell" bundle:nil] forCellReuseIdentifier:@"AddressIsNoneCell"];
        _TabelView.backgroundColor = UIColorEF;
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (FapiaoView *)fapiaoView{
    if (!_fapiaoView) {
        _fapiaoView = [FapiaoView new];
        WeakSelf(self);
        _fapiaoView.goclickBlock = ^{
            [weakself.fapiaoView hide];
            FapiaoVC *vc = [FapiaoVC new];
            weakself.isfirst = !weakself.isfirst;
            vc.clickBlock = ^(NSDictionary * _Nonnull dic) {
                [weakself.fapiaoView show];
                [weakself.fapiaoView setDic:dic];
            };

            [weakself.navigationController pushViewController:vc animated:true];
        };
        _fapiaoView.clickBlock = ^(NSDictionary * _Nonnull dic) {
            weakself.invoiceID = [dic[@"id"] integerValue];
            weakself.fapiaoStr = dic[@"type"];
            [weakself.TabelView reloadData];
            [weakself.fapiaoView hide];
            
        };
    }
    return _fapiaoView;
}

- (AccountView *)accoutView{
    if (!_accoutView) {
        _accoutView = [AccountView new];
        WeakSelf(self);
        _accoutView.clickBlock = ^{
            if ([weakself.adressId integerValue] == 0) {
                [JMBManager showBriefAlert:@"请填写地址"];
                return ;
            }
            [weakself createOrder];
//            [weakself.navigationController pushViewController:[SelectPayWayVC new] animated:true];
        };
    }
    return _accoutView;
}

- (GoodsInfoApi *)goodsApi{
    if (!_goodsApi) {
        _goodsApi = [GoodsInfoApi new];
    }
    return _goodsApi;
}


- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
        
    }
    return _orderApi;
}

@end
