//
//  FapiaoDetailVC.m
//  MallApp
//
//  Created by Mac on 2020/2/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FapiaoDetailVC.h"
#import "PurchaseView.h"
@interface FapiaoDetailVC ()
@property (nonatomic, strong) PurchaseView *editBtn;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *shibieNum;
@property (weak, nonatomic) IBOutlet UILabel *bankTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *adressLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@end

@implementation FapiaoDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"发票详情";
    JLog(@"%@",self.dic);
//    [self.view addSubview:self.editBtn];
//    [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.bottom.equalTo(self.view);
//        make.height.mas_equalTo(50+BottomAreaHeight);
//    }];
    [self setupData];
}

- (void)setupData{
    self.typeLbl.text = self.dic[@"type"];
    self.nameLbl.text = self.dic[@"title"];
    self.shibieNum.text = self.dic[@"sh"];
    self.bankTypeLbl.text = self.dic[@"bank"];
    self.bankNumLbl.text = self.dic[@"bankNumber"];
    self.adressLbl.text = self.dic[@"address"];
    self.telLbl.text = self.dic[@"tel"];
}

- (PurchaseView *)editBtn{
    if (!_editBtn) {
        _editBtn = [PurchaseView new];
        [_editBtn.purchaseButton setTitle:@"编辑" forState:0];
//        [_addNewAddress.purchaseButton setTitle:@"申请中" forState:UIControlStateDisabled];
//        [_addNewAddress.purchaseButton setTitle:@"申请中" forState:UIControlStateDisabled];
//        WeakSelf(self);
//        _addNewAddress.addAddressBlock = ^{
//           StrongSelf(self)
//            NSDictionary *dic =
//            @{
//                @"mjType":self.typeLbl.text,
//                @"qyname":self.nameText.text,
//                @"email":self.emailText.text,
//                @"contact":self.conactText.text,
//                @"mobile":self.telText.text,
//                @"Address":self.adressText.text,
//                @"tel":self.telText.text,
//                @"file0":self.imagesArr.firstObject,
//                @"mjPassed":@0,
//                @"verify":@(false)
//            };
//            [self.api applyWholesaleShopWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
//                if (result.code == 200) {
//                    [self.navigationController popViewControllerAnimated:true];
//                }
//            }];
//
//        };
        
    }
    return _editBtn;
}
@end
