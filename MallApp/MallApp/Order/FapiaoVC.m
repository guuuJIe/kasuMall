//
//  FapiaoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FapiaoVC.h"
#import "FapiaoCell.h"
#import "PurchaseView.h"
#import "AddNewFapiaoItemVC.h"
#import "OrderApi.h"
#import "FapiaoDetailVC.h"
#import "SubmitInvoiceVC.h"
@interface FapiaoVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) PurchaseView *addNewAddress;
@property (nonatomic) OrderApi *api;
@property (nonatomic) NSArray *fapiaoArr;
@end

@implementation FapiaoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"发票抬头";
    [self setupUI];
    [self setData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setData];
}

- (void)setupUI{
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.mas_equalTo(-(50+BottomAreaHeight));
    }];
    
    [self.view addSubview:self.addNewAddress];
    [self.addNewAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setData{
    [self.api getInvoiceListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            self.fapiaoArr = arr;
            [self.TabelView reloadData];
        }
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fapiaoArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"FapiaoCell";
    FapiaoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[FapiaoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.fapiaoArr[indexPath.row]];
    WeakSelf(self)
    cell.deleteBlock = ^{
         StrongSelf(self)
         NSDictionary *dic = self.fapiaoArr[indexPath.row];
        [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否删除该发票" andHandler:^(NSInteger index) {
            if (index == 1) {
                [self.api delInvoiceWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                           [self setData];
                       }];
            }
        }];
    };
    
    cell.setDefaultBlock = ^{
        StrongSelf(self)
        NSDictionary *dic = self.fapiaoArr[indexPath.row];
        [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否设为默认发票" andHandler:^(NSInteger index) {
            if (index == 1) {
                [self.api setDefaultInvoiceWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                    [weakself setData];
                }];
            }
        }];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [self.navigationController pushViewController:[TuanGouGoodInfoVC new] animated:true];
    NSDictionary *dic = self.fapiaoArr[indexPath.row];
//    WeakSelf(self)
//    [self.api setDefaultInvoiceWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//            [weakself setData];
//        }
//    }];
    
    if (self.clickBlock) {
        self.clickBlock(dic);
        [self.navigationController popViewControllerAnimated:true];
    }else{
        if (self.tag) {//补发票
            SubmitInvoiceVC *vc = [[SubmitInvoiceVC alloc] initWithNibName:@"SubmitInvoiceVC" bundle:nil];
            vc.dic = dic;
            vc.orderNum = self.orderNum;
            [self.navigationController pushViewController:vc animated:true];
        }else{
            FapiaoDetailVC *vc = [[FapiaoDetailVC alloc] initWithNibName:@"FapiaoDetailVC" bundle:nil];
            vc.dic = dic;
            [self.navigationController pushViewController:vc animated:true];
        }
       
    }
}

#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"FapiaoCell" bundle:nil] forCellReuseIdentifier:@"FapiaoCell"];
        _TabelView.backgroundColor = UIColorEF;
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (PurchaseView *)addNewAddress{
    if (!_addNewAddress) {
        _addNewAddress = [PurchaseView new];
        [_addNewAddress.purchaseButton setTitle:@"新建发票" forState:0];
        WeakSelf(self);
        _addNewAddress.addAddressBlock = ^{
            
            AddNewFapiaoItemVC *vc = [[AddNewFapiaoItemVC alloc] initWithNibName:@"AddNewFapiaoItemVC" bundle:nil];
            [weakself.navigationController pushViewController:vc animated:true];
           
        };
        
    }
    return _addNewAddress;
}

- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    
    return _api;
}

@end
