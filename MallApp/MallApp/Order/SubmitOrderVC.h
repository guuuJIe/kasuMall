//
//  SubmitOrderVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubmitOrderVC : BaseViewController

@property (nonatomic,strong) NSString *adressId;
@property (nonatomic,strong) NSString *purchaseId;
@property (nonatomic,strong) NSString *from;//1是往团购商品进来的 2是普通商品
@property (nonatomic,strong) NSArray *dataSourceArr;
@property (nonatomic,strong) NSString *num;
@property (nonatomic,strong) NSString *freight;
@property (nonatomic,strong) NSDictionary *productInfo;//产品信息  //1=普通团购，2=秒杀团购，3=分期返还  stage分期返还期数
@property (nonatomic,strong) NSString *goodsId;
@end

NS_ASSUME_NONNULL_END
