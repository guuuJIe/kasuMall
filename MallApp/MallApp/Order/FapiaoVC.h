//
//  FapiaoVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FapiaoVC : BaseViewController
@property (nonatomic,copy) void(^clickBlock)(NSDictionary *dic);
@property (nonatomic,assign) NSInteger tag;
@property (nonatomic,strong) NSString *orderNum;

@end

NS_ASSUME_NONNULL_END
