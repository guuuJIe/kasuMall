//
//  OrderPaySucess.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderPaySucess.h"

@interface OrderPaySucess ()

@end

@implementation OrderPaySucess

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"支付成功";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
