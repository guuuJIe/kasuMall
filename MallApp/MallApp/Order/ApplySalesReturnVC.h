//
//  ApplySalesReturnVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplySalesReturnVC : BaseViewController
@property (nonatomic, assign) OrderOperationType type;
@property (nonatomic, strong) NSDictionary *orderDicData;
@end

NS_ASSUME_NONNULL_END
