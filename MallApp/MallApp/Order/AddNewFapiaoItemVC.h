//
//  AddNewFapiaoItemVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddNewFapiaoItemVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *fapiaoTypeLbl;
@property (weak, nonatomic) IBOutlet UITextField *TaitouLbl;
@property (weak, nonatomic) IBOutlet UITextField *shibiehaoNumLbl;
@property (weak, nonatomic) IBOutlet UITextField *bankType;
@property (weak, nonatomic) IBOutlet UITextField *bankNum;
@property (weak, nonatomic) IBOutlet UITextField *adress;
@property (weak, nonatomic) IBOutlet UITextField *tel;
- (IBAction)setDefault:(UISwitch *)sender;
- (IBAction)selectFapiao:(id)sender;
@property (strong, nonatomic) IBOutlet UIPickerView *dataPickerView;


@end

NS_ASSUME_NONNULL_END
