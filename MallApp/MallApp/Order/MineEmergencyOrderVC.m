//
//  MineEmergencyOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineEmergencyOrderVC.h"
#import "MinePromptView.h"
#import "OrderApi.h"
#import "MyFixOrderItemCell.h"
#import "FixOrderHeadView.h"
#import "ServicesOfOrderFootView.h"
#import "HelpDetailAboutKuaixiuVC.h"
@interface MineEmergencyOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) MinePromptView *promptView;
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) OrderApi *orderApi;
@property (nonatomic, strong) NSMutableArray *datas;
@end

@implementation MineEmergencyOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"抢修/施救订单";
    [self initProp];
    
    [self setupUI];
}

- (void)initProp{
    self.num = 1;
    self.size = @"20";
    self.statues = 21;
    self.promptView.orderStatues = 200;
}


- (void)setupUI{
    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.promptView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)getData:(RefreshType)type{
    if (type == RefreshTypeDown) {
         self.num = 1;
     }else if (type == RefreshTypeUP){
         self.num = self.num+1;
     }
    [self.orderApi getUserFastServiceOrderListDataWithparameters:[NSString stringWithFormat:@"%ld",(long)self.statues] andNum:[NSString stringWithFormat:@"%ld",(long)self.num] andsize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
         [self.listTableView.mj_header endRefreshing];
        if (result.code == 200) {
//            self.datas = result.result;
            NSArray *array = result.result;
            self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
            [self.listTableView reloadData];
           
        }
    }];
}

#pragma mark ---UITableViewDelegate----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    OrderListModel *model = self.datas[section];
//    return model.goodsList.count;
//    NSArray *arr = self.datas[section];
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"MyFixOrderItemCell";
    MyFixOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[MyFixOrderItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datas[indexPath.section]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FixOrderHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"FixOrderHeadView"];
    [headView setupSellerFixOrderData:self.datas[section]];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    [footView setupOpeationViewWithDic:self.datas[section] andType:UserEmergencyOrderOpe andIsNeedShow:true];
    NSDictionary *dic = self.datas[section];
    WeakSelf(self)
    footView.clickBlock = ^(NSString * _Nonnull flag) {
        StrongSelf(self)
        if ([flag isEqualToString:@"orderCancel"]) {
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定取消当前订单？" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [self.orderApi cancelUserFastSetviceOrderWithparameters:@{@"OrderNumber":dic[@"orderNumber"]} withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [self getData:RefreshTypeNormal];
                        }
                    }];
                }
            }];
        }else if ([flag isEqualToString:@"connact"]){
            [JSCMMPopupViewTool showMMPopAlertWithTitle:@"请通知对方" message:@"在个人中心/维修订单里取消订单" withCancelButtonTitle:@"取消" withSureButton:@"立即联系" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [XJUtil callTelNoCommAlert:dic[@"shopTel"]];
                }
            }];
        }
    };
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{

    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    [footView setupOpeationViewWithDic:self.datas[section] andType:UserEmergencyOrderOpe andIsNeedShow:true];
    
    return [footView cellHeight];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HelpDetailAboutKuaixiuVC *vc = [HelpDetailAboutKuaixiuVC new];
    NSDictionary *dic = self.datas[indexPath.section];
    vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
    [self.navigationController pushViewController:vc animated:true];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
//        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MyFixOrderItemCell" bundle:nil] forCellReuseIdentifier:@"MyFixOrderItemCell"];
        [_listTableView registerClass:[FixOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"FixOrderHeadView"];
//        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MinePromptView *)promptView{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        _promptView.dataSource = @[@"派单中",@"服务中",@"已完成"];
        WeakSelf(self)
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
                    
                    self.statues = 21;
                    
                    
                    break;
                case 1:
                    
                    self.statues = 22;
                    
                    break;
                case 2:
                    
                    self.statues = 6;
                    
                    break;
                default:
                    break;
            }
            [self.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_promptView];
    }
    
    return _promptView;
}


- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    
    return _orderApi;
}
@end
