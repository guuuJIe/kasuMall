//
//  ApplySalesReturnVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplySalesReturnVC.h"
#import "OrdetItemCell.h"
#import "RefundReasonTableCell.h"
#import "ServicesRepairSectionThreeCell.h"
#import "PurchaseView.h"
#import "TZImagePickerController.h"
#import "MineApi.h"
#import "GroupApi.h"
#import "OrderApi.h"
@interface ApplySalesReturnVC ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) PurchaseView *commView;
@property (nonatomic, strong) UITextField *reasonText;
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, strong) NSString *picStr;
@property (nonatomic, strong) dispatch_semaphore_t lock;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) GroupApi *groupApi;
@property (nonatomic, strong) OrderApi *orderApi;
@end

@implementation ApplySalesReturnVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}


- (void)setupUI{
    self.title = @"申请退货";
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    
    [self.view addSubview:self.commView];
    [self.commView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setOrderDicData:(NSDictionary *)orderDicData{
    _orderDicData = orderDicData;
    [self.listTableView reloadData];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        NSArray *arr = self.orderDicData[@"products"];
        return arr.count;
    }
    
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        static NSString *Identifier = @"OrdetItemCell";
        OrdetItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];//OrdetItemCell 写错了
        if (cell == nil) {
            cell = [[OrdetItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        NSArray *arr = self.orderDicData[@"products"];
        [cell setupData:arr[indexPath.row]];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"RefundReasonTableCell";
        RefundReasonTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[RefundReasonTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        self.reasonText = cell.reasonText;
        return cell;
    }else{
        static NSString *Identifier = @"ServicesRepairSectionThreeCell";
        ServicesRepairSectionThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        WeakSelf(self)
        cell.clickBlock = ^(NSInteger index) {
            if (index == weakself.selectedPhotos.count) {
                [weakself selectPhoto];
            }else{
                [weakself checkPhoto:index];
            }
        };
        [cell setupCollectionDatawith:_selectedPhotos andAssets:_selectedAssets];
        return cell;
    }
    
  
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (void)selectPhoto{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:6 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    imagePickerVc.maxImagesCount = 6;
    imagePickerVc.allowTakePicture = true; // 在内部显示拍照按钮
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    WeakSelf(self)
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        JLog(@"%@ --- %@",assets,photos);
        weakself.selectedAssets = [NSMutableArray arrayWithArray:assets];
        weakself.selectedPhotos = [NSMutableArray arrayWithArray:photos];
        dispatch_async(dispatch_get_main_queue(), ^{
             [self.listTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationNone];
        });
       
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)checkPhoto:(NSInteger)index{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:_selectedAssets selectedPhotos:_selectedPhotos index:index];
    
    
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)submit{
    [JMBManager showLoading];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
      
        NSMutableArray *imagesObj = [NSMutableArray new];
        for (int i = 0; i<self.selectedPhotos.count; i++) {
            NSDictionary *dic = @{@"name":@"files",@"image":self.selectedPhotos[i]};
            [imagesObj addObject:dic];
        }
        
        
        [self.mineApi uploadPicWithparameters:@"17" VisitImagesArr:imagesObj withCompletionHandler:^(NSError *error, MessageBody *result) {
            
            JLog(@"%@",result.result);
            NSArray *datas = result.result;
            self.picStr = [datas componentsJoinedByString:@","];
            
            XJ_UNLOCK(self.lock);
            [JMBManager hideAlert];
            
        }];

        XJ_LOCK(self.lock);
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//        dispatch_async(dispatch_get_main_queue(), ^{
            [dic setValue:[XJUtil insertStringWithNotNullObject:self.reasonText.text andDefailtInfo:@""] forKey:@"reason"];
            [dic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"pic"];
            [dic setValue:self.orderDicData[@"orderNumber"] forKey:@"orderNumber"];
//        });
        
        if (self.type == UserGroupOrder) {
            [self.groupApi applyGroupsSalesReturnWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [JMBManager showBriefAlert:@"提交成功"];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                }
                [JMBManager hideAlert];
            }];
        }else if (self.type == UserNormalOrder){
            
            [self.orderApi applyNormalSalesReturnWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [JMBManager showBriefAlert:@"提交成功"];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                }
                
                [JMBManager hideAlert];
            }];
            
        }
        
    });
}


- (UITableView *)listTableView{
    if (!_listTableView) {
          _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerClass:[RefundReasonTableCell class] forCellReuseIdentifier:@"RefundReasonTableCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesRepairSectionThreeCell" bundle:nil] forCellReuseIdentifier:@"ServicesRepairSectionThreeCell"];
        _listTableView.showsVerticalScrollIndicator= false;
    }

    return _listTableView;
}

- (PurchaseView *)commView{
    if (!_commView) {
        _commView = [PurchaseView new];
        [_commView.purchaseButton setTitle:@"提交" forState:0];
        WeakSelf(self)
        _commView.addAddressBlock = ^{
            [weakself submit];
        };
    }
    
    return _commView;
}

- (dispatch_semaphore_t)lock {
    if (!_lock) {
        _lock = dispatch_semaphore_create(0);
    }
    return _lock;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    
    return _mineApi;
}

- (GroupApi *)groupApi{
    if (!_groupApi) {
        _groupApi = [GroupApi new];
    }
    
    return _groupApi;
}

- (OrderApi *)orderApi {
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    
    return _orderApi;
}
@end
