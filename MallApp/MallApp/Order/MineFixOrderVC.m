//
//  MineFixOrderVC.m
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineFixOrderVC.h"
#import "MyFixOrderTopView.h"
#import "OrdetItemCell.h"
#import "MyOrderListFootView.h"
#import "FixOrderHeadView.h"
#import "MyFixOrderItemCell.h"
#import "OrderApi.h"
#import "HelpDetailVC.h"
#import "SellerFixOrderDetailVC.h"
@interface MineFixOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) MyFixOrderTopView *topView;
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) NSMutableArray *datas;
@property (nonatomic) OrderApi *orderApi;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger statues;
@property (nonatomic,strong) NSString *size;
@end

@implementation MineFixOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"维修订单";
    [self initProp];
    
    [self setupUI];
    
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"HelpDetailVC"]];
    
}

- (void)initProp{
    self.num = 1;
    self.size = @"20";
    self.statues = 3;
}
- (void)setupUI{
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    self.topView.orderStatues = 203;
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)getData:(RefreshType)type{
    if (type == RefreshTypeDown) {
         self.num = 1;
     }else if (type == RefreshTypeUP){
         self.num = self.num+1;
     }
    [self.orderApi getFixOrderListDataWithparameters:[NSString stringWithFormat:@"%ld",(long)self.statues] andNum:[NSString stringWithFormat:@"%ld",(long)self.num] andsize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
         [self.listTableView.mj_header endRefreshing];
        if (result.code == 200) {
//            self.datas = result.result;
            NSArray *array = result.result;
            self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
            [self.listTableView reloadData];
           
        }
    }];
}

#pragma mark ---UITableViewDelegate----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    OrderListModel *model = self.datas[section];
//    return model.goodsList.count;
//    NSArray *arr = self.datas[section];
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"MyFixOrderItemCell";
    MyFixOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[MyFixOrderItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datas[indexPath.section]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FixOrderHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"FixOrderHeadView"];
    [headView setupData:self.datas[section]];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    MyOrderListFootView *footView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderListFootView"];
   [footView setupData:self.datas[section]];
//
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
//    MyOrderListFootView *footView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderListFootView"];
//    footView.orderModel = self.datas[section];
//    if (section != 1) {
//        [footView setData];
//    }
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HelpDetailVC *vc = [HelpDetailVC new];
    NSDictionary *dic = self.datas[indexPath.section];
    vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
    [self.navigationController pushViewController:vc animated:true];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
//        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MyFixOrderItemCell" bundle:nil] forCellReuseIdentifier:@"MyFixOrderItemCell"];
        [_listTableView registerClass:[FixOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"FixOrderHeadView"];
        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];

      
       
    }
    return _datas;
}

- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    return _orderApi;
}

- (MyFixOrderTopView *)topView{
    if (!_topView) {
        _topView = [MyFixOrderTopView new];
        WeakSelf(self);
        _topView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self);
            self.statues = tag - 200;
            [self.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_topView];
    }
    
    return _topView;
}

@end
