//
//  CarMaintainenceOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyWeixiuTaocanOrderVC.h"
#import "EmptyViewForRecord.h"
#import "MyOrderHeadView.h"
#import "OrdetItemCell.h"
#import "MemberApi.h"
#import "ServicesOfOrderFootView.h"
#import "CarMaintainenceOrderDetailVC.h"
#import "SubmitMyCarInfoVC.h"
#import "MinePromptView.h"
#import "WeixiuTaocanOrderDetailVC.h"
#import "OrdePayVC.h"
#import "MineFixcardViewController.h"
#import "OrderApi.h"
@interface MyWeixiuTaocanOrderVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>

@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, strong) MemberApi *api;
@property (nonatomic, strong) MinePromptView *promptView;
@property (nonatomic, strong) OrderApi *orderApi;
@end

@implementation MyWeixiuTaocanOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"维修套餐订单";
    
//    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"CalculateCarMaintenancePriceVC",@"SubmitMyCarInfoVC",@"SelectPayWayVC"]];
    
    [self setupProp];
    
    [self setupUI];
    
//    [self setupData:RefreshTypeDown];
}


- (void)setupProp{
    
    self.num = 1;
    self.size = 20;
    self.statues = 2;
    self.promptView.orderStatues = 200;
}

- (void)setupUI{
    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(45);
    }];
    
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.promptView.mas_bottom).offset(0);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return self.datas.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSDictionary *datas = self.datas[section];
    NSArray *dataArr = datas[@"products"];
    
    return dataArr.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"OrdetItemCell";
    OrdetItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    NSDictionary *datas = self.datas[indexPath.section];
    NSArray *dataArr = datas[@"products"];
    [cell setupData:dataArr[indexPath.row]];
    WeakSelf(self)
    cell.clickBlock = ^{
        WeixiuTaocanOrderDetailVC *VC = [WeixiuTaocanOrderDetailVC new];
    
        VC.ids = [NSString stringWithFormat:@"%@",datas[@"id"]];
        [weakself.navigationController pushViewController:VC animated:YES];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MyOrderHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderHeadView"];

    [headView setupTCData:self.datas[section]];
    return headView;
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    [footView setupTaocanOrderData:self.datas[section]];

    return [footView cellHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    NSDictionary *dic = self.datas[section];
    [footView setupTaocanOrderData:self.datas[section]];
    WeakSelf(self)
    footView.clickBlock = ^(NSString * _Nonnull flag) {
//        StrongSelf(self)
        if ([flag isEqualToString:@"purchaseAgain"]) {
            OrdePayVC *vc = [OrdePayVC new];
            vc.priceArr = dic;
            vc.from = [NSString stringWithFormat:@"1"];
            [weakself.navigationController pushViewController:vc animated:true];
        }else if ([flag isEqualToString:@"checkServiceCard"]){
            MineFixcardViewController *vc = [MineFixcardViewController new];
            vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
            [weakself.navigationController pushViewController:vc animated:true];
        }else if ([flag isEqualToString:@"cancelOrder"]){
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定取消订单？" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [weakself.orderApi cancelNormalOrderWithparameters:@{@"OrderNumber":dic[@"orderNumber"]} withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [weakself setupData:RefreshTypeNormal];
                        }
                    }];
                }
                
            }];
        }
    };
    return footView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
}

#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    EmptyViewForRecord *view = [EmptyViewForRecord initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-115, 150, 150);
    view.desLbl.text = @"暂无数据";
    return view;
}



- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    

    
    [self.api getRepairTcOrderListDataWithparameters:@"" withType:[NSString stringWithFormat:@"%ld",(long)self.statues] andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:[NSString stringWithFormat:@"%ld",(long)self.size] withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
        
            self.datas = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
            [self.listTableView reloadData];
        }
    }];
}


- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];
    }
    return _datas;
}




- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        [_listTableView setEmptyDataSetSource:self];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
//        [_listTableView registerNib:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerClass:[MyOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"MyOrderHeadView"];
//        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);

            [self setupData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MinePromptView *)promptView
{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        WeakSelf(self);
        
        _promptView.dataSource = @[@"待付款",@"待发服务卡",@"已发服务卡",@"已完成"];
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
                    self.statues = 2;
//                    [self configData:self.dataArr withType:0];
                    
                    break;
                case 1:
                    self.statues = 3;
//                    [self configData:self.dataArr withType:1];
                    break;
                case 2:
                    self.statues = 4;
                    //                    [self configData:self.dataArr withType:1];
                    break;
                case 3:
                    self.statues = 6;
                    //                    [self configData:self.dataArr withType:1];
                    break;
                default:
                    break;
            }
             [self.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_promptView];
    }
    return _promptView;
}

- (MemberApi *)api{
    if (!_api) {
        _api =[MemberApi new];
    }
    return _api;
}
-(OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    
    return _orderApi;
}

@end
