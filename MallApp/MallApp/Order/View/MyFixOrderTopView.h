//
//  MyFixOrderTopView.h
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyFixOrderTopView : UIView
@property (nonatomic,copy) void(^clickBlock)(NSInteger tag);
@property (nonatomic , assign) NSInteger orderStatues;
@end

NS_ASSUME_NONNULL_END
