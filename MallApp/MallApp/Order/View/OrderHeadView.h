//
//  OrderHeadView.h
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderHeadView : UITableViewHeaderFooterView
@property (nonatomic) UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
