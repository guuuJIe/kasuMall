//
//  OrderfooterCell.h
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderfooterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgview;
@property (weak, nonatomic) IBOutlet UILabel *payLbl;
@property (weak, nonatomic) IBOutlet UILabel *feeLbl;
@property (weak, nonatomic) IBOutlet UITextField *noteLbl;
- (IBAction)conponLbl:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *fabiaoLbl;
- (IBAction)fapiaoClick:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *bottomview;
@property (nonatomic,copy) void(^clickBlock)(void);
- (void)setupData:(NSDictionary *)dic withType:(NSInteger)type andPrice:(NSString *)price andFee:(NSString *)fee andFapiaoData:(NSDictionary *)fapiaodic and:(NSInteger)num;
@end

NS_ASSUME_NONNULL_END
