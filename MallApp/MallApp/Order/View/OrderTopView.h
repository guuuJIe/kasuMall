//
//  OrderTopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderTopView : UIView
@property (nonatomic,copy) void(^clickBlock)(NSInteger tag);
@property (nonatomic , assign) NSInteger orderStatues;

@end

NS_ASSUME_NONNULL_END
