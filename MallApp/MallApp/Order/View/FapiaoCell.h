//
//  FapiaoCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FapiaoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *numberLbl;
@property (weak, nonatomic) IBOutlet UIButton *defaultBtn;
- (IBAction)defalut:(UIButton *)sender;
- (IBAction)Edit:(UIButton *)sender;
- (IBAction)DeleteBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *bgView;
- (void)setupData:(NSDictionary *)dic;
@property (nonatomic,copy) void(^deleteBlock)(void);
@property (nonatomic,copy) void(^setDefaultBlock)(void);
@end

NS_ASSUME_NONNULL_END
