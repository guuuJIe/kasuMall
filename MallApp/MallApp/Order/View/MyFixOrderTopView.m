//
//  MyFixOrderTopView.m
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyFixOrderTopView.h"
#import "UnderLineButton.h"
@interface MyFixOrderTopView()

@property (nonatomic , strong) UnderLineButton *selectedBtn;
@end

@implementation MyFixOrderTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.userInteractionEnabled = YES;
        [self setupLayout];
    }
    return self;
}
- (void)setupLayout
{
    UIView *lastView;
    NSArray *arr = @[@"服务中",@"待付款",@"已完成"];
    for (int i = 0; i<arr.count; i++) {
        UnderLineButton *button = [UnderLineButton new];
        button.lineView.backgroundColor = APPColor;
        button.l_width = 50;
        [button setTitle:arr[i] forState:0];
        [button setTitleColor:UIColor333 forState:0];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [button setTitleColor:APPColor forState:UIControlStateSelected];
        button.tag = i+203;
        [self addSubview:button];
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.equalTo(self).offset(1);
            }else{
                make.left.equalTo(lastView.mas_right);
            }
            make.top.equalTo(self).offset(0);
            make.width.mas_equalTo(Screen_Width/3);
            make.bottom.equalTo(self).offset(0*AdapterScal);
        }];
        lastView = button;
    }
    
//    UIView *line = [UIView new];
//    line.backgroundColor = UIColore5e5;
//    [self addSubview:line];
//    [line mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).offset(0);
//        make.right.equalTo(self).offset(0);
//        make.height.mas_equalTo(JLineHeight);
//        make.top.equalTo(lastView.mas_bottom).offset(0);
//    }];
    

    
}

-(void)setOrderStatues:(NSInteger)orderStatues
{
    _orderStatues = orderStatues;
    UnderLineButton *button = [self viewWithTag:self.orderStatues];
    [self click:button];
}


- (void)click:(UnderLineButton *)btn
{
    if (btn!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
        self.selectedBtn.selected = YES;
    }
    if (self.clickBlock) {
        self.clickBlock(btn.tag);
    }
}

@end
