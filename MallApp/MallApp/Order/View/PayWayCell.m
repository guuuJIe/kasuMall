//
//  PayWayCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PayWayCell.h"

@implementation PayWayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    _selectBtn.selected = NO;
    _nameLbl.text = dic[@"name"];
    [self.payIcon setImage:[UIImage imageNamed:dic[@"image"]] forState:0];
}
- (void)setAmount:(NSArray *)data{
    CGFloat price = [data.firstObject doubleValue];
    self.leftAmountLbl.text = [NSString stringWithFormat:@"剩余:(%.3f元)",price];
    
}

@end
