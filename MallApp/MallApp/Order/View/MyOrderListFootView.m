//
//  MyOrderListFootView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#define TableViewFooterBottomHeight 50

#import "MyOrderListFootView.h"
#import "GoodsStautesOperationView.h"

@interface MyOrderListFootView()
@property (nonatomic , strong) GoodsStautesOperationView *optionView;
@property (nonatomic , strong) UILabel *priceLabel;
@property (nonatomic , strong) UILabel *numLabel;
@end

@implementation MyOrderListFootView

-(CGFloat)cellHeight{
    CGFloat height = 45;
    if(self.optionView.btnModelArray.count>0){
        height = height + TableViewFooterBottomHeight;
    }
    return height;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setupLayout];
        
    }
    return self;
}

- (void)setupLayout
{
   
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
         make.top.equalTo(self.contentView).offset(13);
        make.right.equalTo(self.contentView).offset(-12);
    }];
    
   
      UILabel *titleLabel = [UILabel new];
      titleLabel.text = @"共1件";
      titleLabel.font = LabelFont14;
      titleLabel.textColor = UIColor333;
      [self.contentView addSubview:titleLabel];
      [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
          make.right.equalTo(self.priceLabel.mas_left).offset(-14);
          make.top.equalTo(self.contentView).offset(13);
      }];
    self.numLabel = titleLabel;
    
    
    [self.contentView addSubview:self.optionView];
    [self.optionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(TableViewFooterBottomHeight);
        make.top.equalTo(titleLabel.mas_bottom).offset(0*AdapterScal);
    }];
    
    
    
}

- (void)setupData:(NSDictionary *)dic withGoodsNum:(NSInteger)num{
    if (!dic) {
        return;
    }
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
    self.numLabel.text = [NSString stringWithFormat:@"共%ld件",(long)num];
    [self.optionView setOrderOption:dic withType:UserGroupOrder];
    if (self.optionView.btnModelArray.count > 0) {
        self.optionView.hidden = false;
    }else{
        self.optionView.hidden = true;
    }
}
- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
           return;
       }
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
    self.numLabel.text = @"";
    self.optionView.hidden = true;
}

- (UILabel *)priceLabel
{
    if (!_priceLabel) {
        _priceLabel = [UILabel new];
        _priceLabel.textColor = UIColorFF3E;
        _priceLabel.font = LabelFont14;
        _priceLabel.text = @"¥606.00";
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}

- (GoodsStautesOperationView *)optionView
{
    if (!_optionView) {
        _optionView = [GoodsStautesOperationView new];
//        _optionView.backgroundColor = APPColor;
        _optionView.userInteractionEnabled = YES;
        WeakSelf(self);
        _optionView.orderHandleBlock = ^(NSString *flag) {
            StrongSelf(self);
            if (self.clickBlock) {
                self.clickBlock(flag);
            }
        };
        [self.contentView addSubview:_optionView];
    }
    return _optionView;
}

@end
