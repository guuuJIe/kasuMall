//
//  MyFixOrderItemCell.m
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyFixOrderItemCell.h"

@implementation MyFixOrderItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.cateLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"分类：%@",dic[@"proTypeString"]] andDefailtInfo:@""];
        self.priceLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"¥%@",dic[@"price"]] andDefailtInfo:@""];
    }
}
@end
