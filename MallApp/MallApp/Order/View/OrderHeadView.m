//
//  OrderHeadView.m
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderHeadView.h"

@implementation OrderHeadView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = UIColorEF;
        [self setupLayout];
    }
    return self;
}


- (void)setupLayout{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(12, 0, Screen_Width - 24, 50)];
    [self.contentView addSubview:view];
    view.backgroundColor = [UIColor whiteColor];
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(@12);
//        make.right.equalTo(@(-12));
//        make.top.bottom.equalTo(self.contentView);
//    }];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:@"店铺"] forState:0];
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(@18);
    }];
    
    [view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.left.equalTo(button.mas_right).offset(15);
    }];
}


-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"卡速自营";
        _titleLabel.font = LabelFont14;
        _titleLabel.textColor = UIColor333;
       
    }
    return _titleLabel;
}

@end
