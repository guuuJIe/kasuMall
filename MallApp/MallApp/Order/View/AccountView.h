//
//  AccountView.h
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountView : UIView
@property (nonatomic, copy) void (^clickBlock)(void);
- (IBAction)clickAction:(UIButton *)sender;
- (void)setUpData:(NSDictionary *)dic andNum:(NSInteger)num andFreight:(CGFloat)freight;
- (void)setUpOtherData:(NSDictionary *)dic andNum:(NSInteger)num;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *priLbl;
@end

NS_ASSUME_NONNULL_END
