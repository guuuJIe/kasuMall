//
//  ShopItemCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopItemCell.h"

@implementation ShopItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgView.layer.shadowColor = shawdowColor.CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.bgView.layer.shadowOpacity = 0.2;
    self.bgView.layer.shadowRadius = 2.0;
    self.bgView.layer.cornerRadius = 8;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"imageUrl"]];
    [self.shopImage yy_setImageWithURL:URL(url) options:0];
    self.nameLbl.text = dic[@"title"];
    self.adressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",dic[@"province"],dic[@"city"],dic[@"area"],dic[@"address"]];
    self.distanceLbl.hidden = true;
}
@end
