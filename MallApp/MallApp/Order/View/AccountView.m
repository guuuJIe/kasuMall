//
//  AccountView.m
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AccountView.h"

@implementation AccountView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        AccountView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"AccountView" owner:self options:nil] lastObject];
        self = lastView;
        [lastView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self);
            make.height.equalTo(@(50+BottomAreaHeight));
        }];
    }
    return self;
}
- (void)setUpData:(NSDictionary *)dic andNum:(NSInteger)num andFreight:(CGFloat)freight{
    CGFloat price = [dic[@"price"] floatValue];
    CGFloat goodsPrice = price * num;
    CGFloat total = goodsPrice + freight;
    self.priLbl.text = [NSString stringWithFormat:@"¥%.3f",total];
}
- (void)setUpOtherData:(NSDictionary *)dic andNum:(NSInteger)num{
    CGFloat freight = [dic[@"freight"] floatValue];
    CGFloat price = [dic[@"sPrice"] floatValue];
//    CGFloat goodTotal = price * num;
    CGFloat total = freight + price;
    self.priLbl.text = [NSString stringWithFormat:@"¥%.3f",total];
}

- (IBAction)clickAction:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}
@end
