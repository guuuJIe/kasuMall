//
//  OrderInfoOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderInfoOneCell.h"

@implementation OrderInfoOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorEF;
    self.bgView.layer.cornerRadius = 8;
    self.contentView.userInteractionEnabled = true;
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
       
        return;
    }
    self.nameLbl.hidden = false;
    self.addressLbl.hidden = false;
    self.telLbl.hidden = false;
    self.ImageBtn.hidden = false;
    self.arrowBtn.hidden = false;
    self.nameLbl.text = dic[@"name"];
    self.addressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",dic[@"strProvince"],dic[@"strCity"],dic[@"strArea"],dic[@"street"]];
    self.telLbl.text = dic[@"mobile"];
}

- (void)click{
    if (self.adressBlock) {
        self.adressBlock();
    }
}

@end
