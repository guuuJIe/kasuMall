//
//  OrderFootView.m
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderFootView.h"

@implementation OrderFootView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorEF;
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(12, 0, Screen_Width - 24, 20)];
    [self addSubview:view];
    view.backgroundColor = [UIColor whiteColor];
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(@12);
//        make.right.equalTo(@(-12));
//        make.top.bottom.equalTo(self.contentView);
//    }];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    
   
}

@end
