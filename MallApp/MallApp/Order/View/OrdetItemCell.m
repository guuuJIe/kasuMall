//
//  OrdetItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrdetItemCell.h"
@interface OrdetItemCell()

@property (nonatomic , strong) UIImageView *goodsImageView;
@property (nonatomic , strong) UILabel *goodTitleLabel;
@property (nonatomic , strong) UILabel *capacityLabel;
@property (nonatomic , strong) UILabel *priceLabel;
@property (nonatomic , strong) UILabel *numLabel;
@property (nonatomic , strong) UILabel *stateLabel;
@end
@implementation OrdetItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.userInteractionEnabled = YES;
        self.backgroundColor = bgColor;
        [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    [self.goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(12);
        make.left.equalTo(self.contentView).offset(12*AdapterScal);
        make.size.mas_equalTo(CGSizeMake(64, 64));
        make.bottom.equalTo(self.contentView).offset(-12*AdapterScal);
    }];
    
    [self.goodTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodsImageView).offset(0);
        make.left.equalTo(self.goodsImageView.mas_right).offset(12);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
        [self.capacityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.goodsImageView.mas_right).offset(12*AdapterScal);
            make.top.equalTo(self.goodTitleLabel.mas_bottom).offset(4*AdapterScal);
        }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodsImageView.mas_right).offset(12*AdapterScal);
        make.top.equalTo(self.goodTitleLabel.mas_bottom).offset(15*AdapterScal);
    }];
    
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.priceLabel);
        make.right.equalTo(self.contentView).offset(-12*AdapterScal);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodsImageView);
        make.right.equalTo(self.numLabel);
        make.height.mas_equalTo(lineHeihgt);
        make.bottom.equalTo(self.contentView);
    }];
}


- (void)click
{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    self.goodTitleLabel.text = dic[@"title"];
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    self.capacityLabel.text = @"";
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    self.numLabel.text = [NSString stringWithFormat:@"x%@",dic[@"quantity"]];
}

- (void)setupTaocanData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    self.goodTitleLabel.text = dic[@"title"];
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    self.capacityLabel.text = @"";
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    self.numLabel.text = @"";
}

-(UIImageView *)goodsImageView{
    if(!_goodsImageView){
        _goodsImageView = [UIImageView new];
        _goodsImageView.layer.cornerRadius = 0;
        _goodsImageView.layer.masksToBounds = YES;
        _goodsImageView.image = [UIImage imageNamed:@"DefaultImage"];
//        _goodsImageView.backgroundColor = UIColorffd700;
        [self.contentView addSubview:_goodsImageView];
    }
    return _goodsImageView;
}

-(UILabel *)goodTitleLabel
{
    if(!_goodTitleLabel){
        _goodTitleLabel = [UILabel new];
        _goodTitleLabel.textColor = UIColor333;
        _goodTitleLabel.font = LabelFont14;
        _goodTitleLabel.numberOfLines = 2;
        _goodTitleLabel.text = @"玲珑轮胎 LMA16 175/70R14 95/93SD...";
        [self.contentView addSubview:_goodTitleLabel];
    }
    return _goodTitleLabel;
}

-(UILabel *)capacityLabel
{
    if(!_capacityLabel){
        _capacityLabel = [UILabel new];
        _capacityLabel.textColor = UIColor999;
        _capacityLabel.font = LabelFont13;
        _capacityLabel.numberOfLines = 0;
        _capacityLabel.text = @"分类：大众(一汽大众)";
        [self.contentView addSubview:_capacityLabel];
    }
    return _capacityLabel;
}
-(UILabel *)priceLabel
{
    if(!_priceLabel){
        _priceLabel = [UILabel new];
        _priceLabel.textColor = [UIColor colorWithHexString:@"333"];
        _priceLabel.font = LabelFont14;
        _priceLabel.text = @"¥ 599.00";
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}
- (UILabel *)stateLabel
{
    if(!_stateLabel)
    {
        _stateLabel=[UILabel  new];
        _stateLabel.font = [UIFont systemFontOfSize:9];
        _stateLabel.textColor = APPColor;
        _stateLabel.numberOfLines = 1;
        _stateLabel.text = @"限时团购";
        _stateLabel.layer.cornerRadius = 7;
        _stateLabel.layer.borderWidth = 1;
        _stateLabel.layer.borderColor = APPColor.CGColor;
        _stateLabel.textAlignment = NSTextAlignmentCenter;
        //        _goodsTitleLabel.alpha = 0.2;
    }
    return _stateLabel;
}


-(UILabel *)numLabel
{
    if(!_numLabel){
        _numLabel = [UILabel new];
        _numLabel.textColor = [UIColor colorWithHexString:@"666666"];
        _numLabel.font = LabelFont12;
        _numLabel.text = @"x3";
        [self.contentView addSubview:_numLabel];
    }
    return _numLabel;
}


@end
