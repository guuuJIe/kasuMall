//
//  MyOrderListFootView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyOrderListFootView : UITableViewHeaderFooterView
@property (nonatomic,copy) void(^clickBlock)(NSString *flag);
-(CGFloat)cellHeight;
- (void)setupData:(NSDictionary *)dic withGoodsNum:(NSInteger)num;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
