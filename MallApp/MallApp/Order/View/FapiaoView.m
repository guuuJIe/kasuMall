//
//  FapiaoView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FapiaoView.h"
#import "PurchaseView.h"
@interface FapiaoView()
@property (nonatomic) PurchaseView *confirmView;
@property (nonatomic) UILabel *labOne;
@property (nonatomic) UILabel *labTwo;
@property (nonatomic) UILabel *labThree;
@property (nonatomic) UISwitch *switchBtn;
@property (nonatomic,assign) BOOL isNeed;
@end

@implementation FapiaoView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        self.isNeed = false;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimiss)]];
        
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(356);
        }];
        
        
        UILabel *type = [UILabel new];
        type.text = @"发票";
        type.textColor = UIColor333;
        type.font = LabelFont14;
        [bgView addSubview:type];
        [type mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@14);
            make.top.equalTo(bgView).offset(16);
        }];
        
        UIView *line = [UIView new];
        [bgView addSubview:line];
        line.backgroundColor = UIColorEF;
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.mas_equalTo(type.mas_bottom).offset(16);
            make.height.mas_equalTo(lineHeihgt);
        }];
        
        
        UILabel *type2 = [UILabel new];
        type2.text = @"开具发票";
        type2.textColor = UIColor333;
        type2.font = LabelFont14;
        [bgView addSubview:type2];
        [type2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@14);
            make.top.equalTo(line.mas_bottom).offset(16);
        }];
        
        
        UIView *line2 = [UIView new];
        [bgView addSubview:line2];
        line2.backgroundColor = UIColorEF;
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.mas_equalTo(type2.mas_bottom).offset(16);
            make.height.mas_equalTo(10);
        }];
        
        UISwitch *swicth = [UISwitch new];
        [bgView addSubview:swicth];
        [swicth mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(bgView).offset(-14);
            make.centerY.equalTo(type2);
//            make.right.equalTo(self).offset(-14)
        }];
        [swicth addTarget:self action:@selector(isOnAct:) forControlEvents:UIControlEventValueChanged];
        self.switchBtn = swicth;
        
        
        UILabel *type3 = [UILabel new];
        type3.text = @"发票抬头";
        type3.textColor = UIColor333;
        type3.font = LabelFont14;
        [bgView addSubview:type3];
        [type3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@14);
            make.top.equalTo(line2.mas_bottom).offset(16);
        }];
        
        
        UILabel *type4 = [UILabel new];
        type4.text = @"请填写发票抬头 >";
        type4.textColor = UIColor999;
        type4.font = LabelFont14;
        [bgView addSubview:type4];
        type4.userInteractionEnabled = true;
        [type4 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
        [type4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-14));
            make.top.equalTo(line2.mas_bottom).offset(16);
        }];
//        self.labOne = type4
            
        [bgView addSubview:self.confirmView];
        [self.confirmView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self);
            make.height.mas_equalTo(50+BottomAreaHeight);
        }];
        
        
        
        UIView *line3 = [UIView new];
        [bgView addSubview:line3];
        line3.backgroundColor = UIColorEF;
        [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.mas_equalTo(type3.mas_bottom).offset(16);
            make.height.mas_equalTo(lineHeihgt);
        }];
        
        
        
        UILabel *type5 = [UILabel new];
        type5.text = @"单位名称";
        type5.textColor = UIColor333;
        type5.font = LabelFont14;
        [bgView addSubview:type5];
        [type5 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@14);
            make.top.equalTo(line3.mas_bottom).offset(16);
        }];
        
        UILabel *type6 = [UILabel new];
        type6.text = @"纳税人识别号";
        type6.textColor = UIColor333;
        type6.font = LabelFont14;
        [bgView addSubview:type6];
        [type6 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@14);
            make.top.equalTo(type5.mas_bottom).offset(16);
        }];
        
        
        UILabel *type5Vaulue = [UILabel new];
        type5Vaulue.text = @"";
        type5Vaulue.textColor = UIColor333;
        type5Vaulue.font = LabelFont14;
        [bgView addSubview:type5Vaulue];
        [type5Vaulue mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-14));
            make.top.equalTo(line3.mas_bottom).offset(16);
        }];
        self.labOne = type5Vaulue;
        
        UILabel *type6Vaulue = [UILabel new];
        type6Vaulue.text = @"";
        type6Vaulue.textColor = UIColor333;
        type6Vaulue.font = LabelFont14;
        [bgView addSubview:type6Vaulue];
        [type6Vaulue mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-14));
            make.centerY.equalTo(type6);
        }];
        
        self.labTwo = type6Vaulue;
        
    }
    return self;
}

- (void)isOnAct:(UISwitch *)sender{
//    sender.isOn = !sender.isOn;
    self.isNeed = sender.isOn;
    
}

-(void)setDic:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    
    _dic = dic;
    self.labOne.text = dic[@"title"];
    self.labTwo.text = dic[@"sh"];
}
- (void)dimiss{
    
    [self hide];
    
}

- (void)click{
//    if (self.isNeed) {
        if (self.goclickBlock) {
            self.goclickBlock();
        }
//    }
    
}

- (PurchaseView *)confirmView{
    if (!_confirmView) {
        _confirmView = [PurchaseView new];
        [_confirmView.purchaseButton setTitle:@"确认" forState:0];
        WeakSelf(self)
        _confirmView.addAddressBlock = ^{
            if (weakself.isNeed) {
                if (weakself.clickBlock) {
                    weakself.clickBlock(weakself.dic);
                }
            }
            
        };
    }
    
    return _confirmView;
}

@end
