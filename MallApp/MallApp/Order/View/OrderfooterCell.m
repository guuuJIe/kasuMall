//
//  OrderfooterCell.m
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderfooterCell.h"
#import "FapiaoView.h"
@interface OrderfooterCell()
@property (nonatomic) FapiaoView *fapiaoView;
@end

@implementation OrderfooterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = UIColorEF;
    self.bottomview.frame = CGRectMake(12, 0, Screen_Width - 24, 10);
//    self.bottomview.backgroundColor = [UIColor redColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottomview.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bottomview.bounds;
    maskLayer.path = maskPath.CGPath;
    self.bottomview.layer.mask = maskLayer;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)conponLbl:(UIButton *)sender {
}
- (IBAction)fapiaoClick:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (void)setupData:(NSDictionary *)dic withType:(NSInteger)type andPrice:(NSString *)price andFee:(NSString *)fee andFapiaoData:(NSDictionary *)fapiaodic and:(NSInteger)num{
    if (!dic) {
        return;
    }
    if (type == 1) {
        CGFloat price = [dic[@"price"] floatValue];
        CGFloat total = price * num;

        self.feeLbl.text = [NSString stringWithFormat:@"运费：¥%@",fee];
        self.payLbl.text = [NSString stringWithFormat:@"实付款：¥%.3f",total];
        [self.fabiaoLbl setTitle:@"团购不支持开发票" forState:0];
    }else{
        CGFloat freight = [dic[@"freight"] floatValue];
        CGFloat price = [dic[@"sPrice"] floatValue];
//           CGFloat goodTotal = price * num;
//           CGFloat total = freight + goodTotal;
        [self.fabiaoLbl setTitle:[XJUtil insertStringWithNotNullObject:dic[@"type"] andDefailtInfo:@""] forState:0];
        self.feeLbl.text = [NSString stringWithFormat:@"运费：¥%@",dic[@"freight"]];
        self.payLbl.text = [NSString stringWithFormat:@"实付款：¥%.3f",price];
    }
    
    
}


@end
