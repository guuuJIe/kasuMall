//
//  OrderItemCell.m
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderItemCell.h"

@implementation OrderItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = UIColorEF;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic withType:(NSInteger)type andNum:(NSString *)num{
    if (!dic) {
        return;
    }
    if (type == 1) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"proPic"]];
        [self.goodsImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = dic[@"title"];
        self.stateLbl.text = @"";
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
        self.numLbl.text = [NSString stringWithFormat:@"x%@",num];
    }else{
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"proPic"]];
        [self.goodsImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = dic[@"proName"];
        self.stateLbl.text = [NSString stringWithFormat:@"适用车型：%@",dic[@"model"]];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"proPrice"]];
        self.numLbl.text = [NSString stringWithFormat:@"x%@",dic[@"proQuantity"]];
    }
    
   
}
@end
