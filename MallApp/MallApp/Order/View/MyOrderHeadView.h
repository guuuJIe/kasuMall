//
//  MyOrderHeadView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyOrderHeadView : UITableViewHeaderFooterView
- (void)setupData:(NSDictionary *)dic;
- (void)setupEwData:(NSDictionary *)dic;
- (void)setupEwEmData:(NSDictionary *)dic;
- (void)setupTCData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
