//
//  AddressIsNoneCell.m
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddressIsNoneCell.h"

@implementation AddressIsNoneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorEF;
    self.bg.layer.cornerRadius = 8;
    self.addAddressLbl.userInteractionEnabled = true;
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)click{
    if (self.addBlock) {
        self.addBlock();
    }
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
}

@end
