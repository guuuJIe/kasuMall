//
//  MyFixOrderItemCell.h
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyFixOrderItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *dataImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *cateLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
