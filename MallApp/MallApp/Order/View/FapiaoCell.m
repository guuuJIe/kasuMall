//
//  FapiaoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FapiaoCell.h"

@implementation FapiaoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorEF;
    self.bgView.layer.cornerRadius = 8;
    self.selectionStyle = UITableViewCellSeparatorStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)defalut:(id)sender {
    if (self.setDefaultBlock) {
        self.setDefaultBlock();
    }
}

- (IBAction)Edit:(UIButton *)sender {
}

- (IBAction)DeleteBtn:(UIButton *)sender {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@"暂无"];
    self.numberLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"address"] andDefailtInfo:@"暂无"];
    NSInteger defaultAddress = [dic[@"tuiJian"] integerValue];
    if (defaultAddress == 1) {
        self.defaultBtn.selected = true;
    }else{
        
    }
}
@end
