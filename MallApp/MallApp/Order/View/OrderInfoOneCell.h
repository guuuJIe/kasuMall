//
//  OrderInfoOneCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderInfoOneCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *ImageBtn;
@property (nonatomic,copy) void(^adressBlock)(void);
@property (weak, nonatomic) IBOutlet UIButton *arrowBtn;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
