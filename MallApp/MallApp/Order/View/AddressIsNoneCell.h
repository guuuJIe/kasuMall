//
//  AddressIsNoneCell.h
//  MallApp
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddressIsNoneCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bg;
@property (nonatomic, copy) void (^addBlock)(void);
@property (weak, nonatomic) IBOutlet UILabel *addAddressLbl;


@end

NS_ASSUME_NONNULL_END
