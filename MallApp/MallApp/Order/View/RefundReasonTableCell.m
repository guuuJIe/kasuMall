//
//  RefundReasonTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "RefundReasonTableCell.h"

@implementation RefundReasonTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
        
    }
    
    return self;
}

- (void)setupUI{
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    UILabel *title = [UILabel new];
    title.text = @"退款说明：";
    title.textColor = UIColor333;
    title.font = LabelFont14;
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(22);
        make.bottom.mas_equalTo(-22);
    }];
    
    UITextField *text= [UITextField new];
    text.placeholder = @"请输入退款原因";
    text.textColor = UIColor333;
    text.font = LabelFont14;
    [self.contentView addSubview:text];
    [text mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title.mas_right).offset(3);
        make.centerY.mas_equalTo(title);
    }];
    
    self.reasonText = text;
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
}

@end
