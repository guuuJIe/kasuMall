//
//  FapiaoView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FapiaoView : MMPopupView
@property (nonatomic,copy) void(^clickBlock)(NSDictionary *dic);
@property (nonatomic,copy) void(^goclickBlock)(void);
@property (nonatomic) NSDictionary *dic;
@end

NS_ASSUME_NONNULL_END
