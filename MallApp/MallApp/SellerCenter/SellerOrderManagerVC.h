//
//  SellerOrderManagerVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SellerOrderManagerVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *orderView;
@property (weak, nonatomic) IBOutlet UIView *fixOrderView;
@property (weak, nonatomic) IBOutlet UIView *refundView;

@end

NS_ASSUME_NONNULL_END
