//
//  FixOrderEditViewController.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FixOrderEditViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *moneyView;
@property (nonatomic,strong) NSString *orderNum;
@property (nonatomic,strong) NSDictionary *dic;
@property (nonatomic,assign) NSInteger tag;
@property (nonatomic,copy) void(^backBlock)(void);
@end

NS_ASSUME_NONNULL_END
