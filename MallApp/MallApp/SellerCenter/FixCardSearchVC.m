//
//  FixCardSearchVC.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixCardSearchVC.h"
#import "FixCardSearchResultVC.h"
#import "MemberApi.h"
#import "ScanVC.h"
@interface FixCardSearchVC ()
@property (weak, nonatomic) IBOutlet UITextField *accountText;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (nonatomic, strong) MemberApi *api;
@end

@implementation FixCardSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
#if DEBUG
    self.searchBtn.layer.cornerRadius = 4;
    self.accountText.text = @"P144879064346-152"; 
//    self.accountText.text = @"073335035221";
#endif
    self.title = @"服务卡查询";
}
- (IBAction)searchAct:(id)sender {
    if (!self.accountText.text) {
        
        [JMBManager showBriefAlert:@"请输入查询卡号"];
         return;
    }
//    [self.api searchFixCardWithparameters:self.accountText.text withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//            NSArray *datas = result.result;
            FixCardSearchResultVC *vc = [[FixCardSearchResultVC alloc] initWithNibName:@"FixCardSearchResultVC" bundle:nil];
            vc.accountText = self.accountText.text;
            [self.navigationController pushViewController:vc animated:true];
//        }
//    }];
   
}
- (IBAction)scanAct:(UIButton *)sender {
    
    ScanVC *vc = [ScanVC new];
    [self.navigationController pushViewController:vc animated:true];
    
}

- (MemberApi *)api{
    if (!_api) {
        _api = [MemberApi new];
    }
    return _api;
}

@end
