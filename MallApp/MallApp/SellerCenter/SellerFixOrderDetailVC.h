//
//  SellerFixOrderDetailVC.h
//  MallApp
//
//  Created by Mac on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SellerFixOrderDetailVC : BaseViewController
@property (nonatomic,strong) NSString *orderNum;
@property (nonatomic,assign) NSInteger type;//1抢修/施救
@end

NS_ASSUME_NONNULL_END
