//
//  CarMaintainenceOrderDetailVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCarMaintainenceOrderDetailVC.h"

#import "SellerCarMaintainenceOrderDetailSectionOneCell.h"
#import "ServicesOrderItemCell.h"
#import "OrderCarInfoCell.h"
#import "SellerCarMaintainenceOrderDetailSectionFourCell.h"
#import "PurchaseView.h"
#import "CarMaintainenceListVC.h"
#import "ApplyRepairVC.h"
#import "GroupApi.h"
#import "WebVC.h"
#import "SubmitMyCarInfoVC.h"
#import "UploadCarMaintainenceListVC.h"
#import "CarMaintainceProInfoVC.h"
#import "SellerCarMaintainenceTableCell.h"
#import "SellerCarMaintainenceSectionTwoCell.h"
#import "CommonPicPreviewCell.h"
#import "ServicesOfYearDetails.h"
@interface SellerCarMaintainenceOrderDetailVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) PurchaseView *purchaseView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSDictionary *dic;

@property (nonatomic, assign) NSInteger isCheckOut;
@end

@implementation SellerCarMaintainenceOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"ScanVC",@"SellerVerifyOrderVC"]];

    [self setupUI];
    
    [self getData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setupUI{
    self.title = @"订单详情";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).offset(-50-BottomAreaHeight);
    }];
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)getData{
    
    [self.api getSellerEmEwOrderInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dic = datas.firstObject;
            NSInteger statues = [self.dic[@"status"] integerValue];
            if (statues == 2) {
                self.purchaseView.hidden = false;
                [self.purchaseView.purchaseButton setTitle:@"开始维修" forState:0];
                
            }else if (statues == 3){
                
                self.purchaseView.hidden = false;
                [self.purchaseView.purchaseButton setTitle:@"完成维修" forState:0];
                [self.purchaseView.purchaseButton setBackgroundColor:FF3E3E];
            }else if (statues == 5){
                self.isCheckOut = [self.dic[@"orderInfo"][@"isCheckOut"] integerValue];
                if (self.type == 2) {
                    
                    if (self.isCheckOut > 0) {
                        self.purchaseView.hidden = true;
                        //                    [self.purchaseView.purchaseButton setTitle:@"上传资料" forState:0];
                    }else{
                        self.purchaseView.hidden = false;
                        [self.purchaseView.purchaseButton setTitle:@"上传资料" forState:0];
                    }
                }else if (self.type == 1){
                    self.purchaseView.hidden = true;
                }
               
                
            }
            
            [self.listTableView reloadData];
        }
    }];
    
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.isCheckOut > 0 ? 7 : 5;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        static NSString *Identifier = @"SellerCarMaintainenceOrderDetailSectionOneCell";
        SellerCarMaintainenceOrderDetailSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
       
      
        [cell setupData:self.dic];
       if (self.type == 1) {
                  cell.titleLbl.text = @"保修项目";
              }
        return cell;
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"ServicesOrderItemCell";
        ServicesOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dic[@"product"]];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        return cell;
    }else if (indexPath.row == 2){
        static NSString *Identifier = @"OrderCarInfoCell";
        OrderCarInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
      
        [cell setupData:self.dic];
        return cell;
    }else if (indexPath.row == 3){
        static NSString *Identifier = @"SellerCarMaintainenceOrderDetailSectionFourCell";
        SellerCarMaintainenceOrderDetailSectionFourCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dic];
        return cell;
    }else if (indexPath.row == 4){
        static NSString *Identifier = @"SellerCarMaintainenceTableCell";
        SellerCarMaintainenceTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        if (self.type == 1) {
            cell.titleLbl.text = @"保修清单";
            cell.titleLbl2.text = @"保修次数";
        }
        [cell setupData:self.dic[@"orderInfo"]];
        return cell;
    }else if (indexPath.row == 5){
        static NSString *Identifier = @"SellerCarMaintainenceSectionTwoCell";
        SellerCarMaintainenceSectionTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        if (self.type == 1) {
            cell.titleLbl.text = @"保修价格";
        }
        [cell setupData:self.dic[@"orderInfo"]];
        return cell;
    }else if (indexPath.row == 6){
        static NSString *Identifier = @"CommonPicPreviewCell";
        CommonPicPreviewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        cell.titleLbl.textColor = UIColor66;
        [cell setupData:self.dic[@"orderInfo"]];
        return cell;
    }
   

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
         NSDictionary *dic = self.dic[@"product"];
        if (self.type == 1) {
            ServicesOfYearDetails *vc = [ServicesOfYearDetails new];
            vc.ids = [dic[@"id"] integerValue];
            [self.navigationController pushViewController:vc animated:true];
        }else{
            CarMaintainceProInfoVC *vc = [CarMaintainceProInfoVC new];
            vc.ids = [dic[@"id"] integerValue];
            [self.navigationController pushViewController:vc animated:true];
        }
       
    }
}



- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.tableHeaderView = [UIView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.estimatedSectionHeaderHeight = 0;
        _listTableView.estimatedSectionFooterHeight = 0;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[SellerCarMaintainenceOrderDetailSectionOneCell class] forCellReuseIdentifier:@"SellerCarMaintainenceOrderDetailSectionOneCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderItemCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderItemCell"];
         [_listTableView registerNib:[UINib nibWithNibName:@"OrderCarInfoCell" bundle:nil] forCellReuseIdentifier:@"OrderCarInfoCell"];
        [_listTableView registerClass:[SellerCarMaintainenceTableCell class] forCellReuseIdentifier:@"SellerCarMaintainenceTableCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderDetailSectionThreeCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderDetailSectionThreeCell"];
        [_listTableView registerClass:[SellerCarMaintainenceOrderDetailSectionFourCell class] forCellReuseIdentifier:@"SellerCarMaintainenceOrderDetailSectionFourCell"];
        [_listTableView registerClass:[SellerCarMaintainenceSectionTwoCell class] forCellReuseIdentifier:@"SellerCarMaintainenceSectionTwoCell"];
        [_listTableView registerClass:[CommonPicPreviewCell class] forCellReuseIdentifier:@"CommonPicPreviewCell"];
        _listTableView.showsVerticalScrollIndicator= false;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
//        [_purchaseView.purchaseButton setTitle:@"开始维修" forState:0];
        _purchaseView.hidden = true;
        WeakSelf(self);
        _purchaseView.addAddressBlock = ^{
            StrongSelf(self)
            if ([self.purchaseView.purchaseButton.currentTitle isEqualToString:@"开始维修"]) {
                [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认开始维修" andHandler:^(NSInteger index) {
                    if (index == 1) {
                        [self.api sellerEwEmRepairStartWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
                            if (result.code == 200) {
                                [self getData];
                            }
                        }];
                    }
                }];
            }else if ([self.purchaseView.purchaseButton.currentTitle isEqualToString:@"完成维修"]){
                [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认完成" andHandler:^(NSInteger index) {
                    if (index == 1) {
                       [self.api sellerEwEmRepairEndWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
                            if (result.code == 200) {
                                  [self getData];
                            }
                        }];
                    }
                }];
                
            }else if ([self.purchaseView.purchaseButton.currentTitle isEqualToString:@"上传资料"]){
                UploadCarMaintainenceListVC *vc = [UploadCarMaintainenceListVC new];
                vc.refreshBlock = ^{
                    [self getData];
                };
                vc.dic = self.dic;
                [self.navigationController pushViewController:vc animated:true];
            }
           
        };
    }
    return _purchaseView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}
@end
