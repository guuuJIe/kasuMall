//
//  UploadCarMaintainenceListVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UploadCarMaintainenceListVC : BaseViewController
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic, copy) void(^refreshBlock)(void);
@end

NS_ASSUME_NONNULL_END
