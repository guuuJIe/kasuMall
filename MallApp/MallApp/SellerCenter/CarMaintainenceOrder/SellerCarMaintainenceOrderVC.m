//
//  CarMaintainenceOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCarMaintainenceOrderVC.h"
#import "EmptyViewForRecord.h"
#import "MyOrderHeadView.h"
#import "ServicesOrderItemCell.h"
#import "GroupApi.h"
#import "SellerCarMaintainenceOperationView.h"
#import "SellerCarMaintainenceOrderDetailVC.h"
#import "SubmitMyCarInfoVC.h"
#import "ScanVC.h"
#import "SellerVerifyOrderVC.h"
@interface SellerCarMaintainenceOrderVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>

@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) UIButton *scanBtn;

@end

@implementation SellerCarMaintainenceOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"CalculateCarMaintenancePriceVC",@"SubmitMyCarInfoVC",@"SelectPayWayVC"]];
    
    [self setupProp];
    
    [self setupUI];
    
    [self setupData:RefreshTypeDown];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.scanBtn];
}


- (void)setupProp{
    
    self.num = 1;
    self.size = 20;
    self.statues = 1;
    
    if (self.type == 1) {
        self.title = @"保修订单";
    }else if (self.type == 2){
        self.title = @"保养订单";
    }
}

- (void)setupUI{

    
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)scan{
    ScanVC *vc = [ScanVC new];
    [self.navigationController pushViewController:vc animated:true];

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"ServicesOrderItemCell";
    ServicesOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    NSDictionary *dic = self.datas[indexPath.section];
    [cell setupData:dic[@"product"]];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MyOrderHeadView *HeadView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderHeadView"];
    [HeadView setupEwEmData:self.datas[section]];
    return HeadView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    SellerCarMaintainenceOperationView *footView = [SellerCarMaintainenceOperationView new];
    [footView setupSellerData:self.datas[section] withOpType:SellerCarMaintainceOpe];
    
    return [footView cellHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    SellerCarMaintainenceOperationView *footView = [SellerCarMaintainenceOperationView new];
    NSDictionary *dic = self.datas[section];
    [footView setupSellerData:self.datas[section] withOpType:SellerCarMaintainceOpe];
    WeakSelf(self)
    footView.clickBlock = ^(NSString * _Nonnull flag) {
        StrongSelf(self)
        if ([flag isEqualToString:@"startRepair"]) {
            
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认开始维修" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [self.api sellerEwEmRepairStartWithparameters:dic[@"orderNumber"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [self setupData:RefreshTypeNormal];
                        }
                    }];
                }
            }];
            
        }else if ([flag isEqualToString:@"achieveRepair"]){
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认完成" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [self.api sellerEwEmRepairEndWithparameters:dic[@"orderNumber"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [self setupData:RefreshTypeNormal];
                        }
                    }];
                }
            }];
            
        }
    };
    return footView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SellerCarMaintainenceOrderDetailVC *vc = [SellerCarMaintainenceOrderDetailVC new];
    NSDictionary *dic = self.datas[indexPath.section];
    vc.orderNum = dic[@"recordId"];
    vc.type = self.type;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    EmptyViewForRecord *view = [EmptyViewForRecord initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-115, 150, 150);
    view.desLbl.text = @"暂无数据";
    return view;
}



- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown || type == RefreshTypeNormal) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    

    [self.api getSellerCarMaintainenceOrderListWithparameters:@"0" withpageSize:self.size pageNum:self.num andType:self.type withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            
            NSArray *array = result.result;
            self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
            [self.listTableView reloadData];
            [self.listTableView reloadEmptyDataSet];
            
        }
    }];
}


- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];
    }
    return _datas;
}




- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        [_listTableView setEmptyDataSetSource:self];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderItemCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderItemCell"];
        [_listTableView registerClass:[MyOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"MyOrderHeadView"];
//        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);

            [self setupData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (GroupApi *)api{
    if (!_api) {
        _api =[GroupApi new];
    }
    return _api;
}

- (UIButton *)scanBtn
{
    if (!_scanBtn) {

        UIButton *_backBtn ;
        
        _backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"scan"] forState:UIControlStateNormal];
        _backBtn.contentMode = UIViewContentModeCenter;
        [_backBtn.titleLabel setHidden:YES];
        
        _backBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        _backBtn.frame=CGRectMake(-10, 0, 20, 40);

        _backBtn.contentMode = UIViewContentModeScaleAspectFit;
        [_backBtn addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
    
        _scanBtn = _backBtn;
        
    }
    return _scanBtn;
}


@end
