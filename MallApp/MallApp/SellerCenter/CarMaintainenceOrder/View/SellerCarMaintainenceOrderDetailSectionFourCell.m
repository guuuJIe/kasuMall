//
//  SellerCarMaintainenceOrderDetailSectionFourCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCarMaintainenceOrderDetailSectionFourCell.h"
@interface SellerCarMaintainenceOrderDetailSectionFourCell()
@property (nonatomic, strong) UILabel *stauesLbl;
@property (nonatomic, strong) UILabel *timeLbl;
@end
@implementation SellerCarMaintainenceOrderDetailSectionFourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UILabel *title = [UILabel new];
    title.text = @"状态信息";
    title.textColor = UIColor333;
    title.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(14);
    }];
    UIView *line = [UIView new];
    line.backgroundColor = UIColorED;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title.mas_bottom).offset(14);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"订单状态";
    title2.textColor = UIColor66;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(14);
    }];
    [self.contentView addSubview:self.stauesLbl];
    [self.stauesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title2);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorED;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title2);
        make.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title2.mas_bottom).offset(14);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"创建时间";
    title3.textColor = UIColor66;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line2.mas_bottom).offset(14);
        make.bottom.mas_equalTo(self.contentView).offset(-14);
    }];
    [self.contentView addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title3);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSInteger stautes = [dic[@"status"] integerValue];
        if (stautes == 1) {
            self.stauesLbl.text = @"审核中";
            self.stauesLbl.textColor = UIColor1F85;
        }else if(stautes == 0){
            self.stauesLbl.text = @"已失效";
        }else if(stautes == 2){
            self.stauesLbl.text = @"待服务";
            self.stauesLbl.textColor = APPColor;
        }else if(stautes == 3){
            self.stauesLbl.text = @"服务中";
            self.stauesLbl.textColor = UIColorFFA1;
        }else if(stautes == 4){
            self.stauesLbl.text = @"维修完成";
            self.stauesLbl.textColor = UIColor1F85;
        }else if(stautes == 5){
            self.stauesLbl.text = @"已完成";
            self.stauesLbl.textColor = UIColor1F85;
        }else if(stautes == 6){
            self.stauesLbl.text = @"已失效";
            self.stauesLbl.textColor = UIColor999;
        }

        self.timeLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"createTime"] andDefailtInfo:@""];
    }
}


- (UILabel *)stauesLbl{
    if (!_stauesLbl) {
        _stauesLbl = [UILabel new];
        _stauesLbl.textColor = UIColor333;
        _stauesLbl.font = LabelFont14;
        _stauesLbl.text = @"";
    }
    
    return _stauesLbl;
}

- (UILabel *)timeLbl{
    if (!_timeLbl) {
        _timeLbl = [UILabel new];
        _timeLbl.textColor = UIColor333;
        _timeLbl.font = LabelFont14;
        _timeLbl.text = @"";
    }
    
    return _timeLbl;
}

@end
