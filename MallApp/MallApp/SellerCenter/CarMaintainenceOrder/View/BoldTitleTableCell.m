//
//  BoldTitleTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BoldTitleTableCell.h"

@implementation BoldTitleTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
        
    }];
    
    [self.contentView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(14);
       
    }];
    
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorEF;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(self.nameLbl.mas_bottom).offset(14);
    }];
}

- (UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [UILabel new];
        _nameLbl.textColor = UIColor333;
        _nameLbl.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
        _nameLbl.text = @"保养项目";
    }
    
    return _nameLbl;
}
@end
