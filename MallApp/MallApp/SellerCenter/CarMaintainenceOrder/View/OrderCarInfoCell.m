//
//  OrderCarInfoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OrderCarInfoCell.h"
@interface OrderCarInfoCell()

@property (weak, nonatomic) IBOutlet UILabel *mileAgeLbl;
@property (weak, nonatomic) IBOutlet UILabel *carNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *carVinLbl;
@property (weak, nonatomic) IBOutlet UILabel *carTypeLbl;

@end

@implementation OrderCarInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.mileAgeLbl.text = [NSString stringWithFormat:@"%@公里",[XJUtil insertStringWithNotNullObject:dic[@"userOrderInfo"][@"mileage"] andDefailtInfo:@""]];
        self.carNumLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"carInfo"][@"numberPlate"] andDefailtInfo:@""];
        self.carVinLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"carInfo"][@"vin"] andDefailtInfo:@""];
        self.carTypeLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"carInfo"][@"ocsaModel"] andDefailtInfo:@""];
    }
}

@end
