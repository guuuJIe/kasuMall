//
//  SellerCarMaintainenceOperationView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCarMaintainenceOperationView.h"
#import "CarMaintainenceOperationView.h"
@interface SellerCarMaintainenceOperationView()
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, strong) CarMaintainenceOperationView *optionView;
@end

@implementation SellerCarMaintainenceOperationView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
//        [self addSubview:self.priceLbl];
//        [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(12);
//            make.right.mas_equalTo(-12);
//        }];
        
        [self addSubview:self.optionView];
        [self.optionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.height.mas_equalTo(50);
            make.top.equalTo(self).offset(12);
        }];
        
    }
    
    return self;
}

- (void)setupSellerData:(NSDictionary *)dic withOpType:(OperationType)type{
    if (dic) {
        self.dataDic = dic;
//        NSString *sumStr = [NSString stringWithFormat:@"实付款:¥%@",dic[@"realPrice"]];
//        NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
//        [aStr addAttribute:NSForegroundColorAttributeName value:UIColor333 range:[sumStr rangeOfString:@"实付款:"]];
//        self.priceLbl.attributedText = aStr;
        [self.optionView setupOperationViewWithDic:dic andType:type];
        if (self.optionView.btnModelArray.count > 0) {
            self.optionView.hidden = false;
        }else{
            self.optionView.hidden = true;
        }
    }
}


-(CGFloat)cellHeight{
    CGFloat height = 0;

    if(self.optionView.btnModelArray.count > 0){
        height = height + 60;
    }
    return height;
}

-(UILabel *)priceLbl
{
    if(!_priceLbl)
    {
        _priceLbl=[UILabel  new];
        _priceLbl.font = LabelFont13;
        _priceLbl.textColor = FF3E3E;
        _priceLbl.text = @"实付款:";
    }
    return _priceLbl;
}

- (CarMaintainenceOperationView *)optionView
{
    if (!_optionView) {
        _optionView = [CarMaintainenceOperationView new];
//        _optionView.backgroundColor = APPColor;
        _optionView.userInteractionEnabled = YES;
        WeakSelf(self);
        _optionView.orderHandleBlock = ^(NSString *flag) {
            StrongSelf(self);
            if (self.clickBlock) {
                self.clickBlock(flag);
            }
        };
        [self addSubview:_optionView];
    }
    return _optionView;
}

@end
