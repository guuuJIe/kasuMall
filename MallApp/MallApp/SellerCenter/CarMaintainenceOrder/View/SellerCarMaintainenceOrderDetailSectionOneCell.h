//
//  SellerCarMaintainenceOrderDetailSectionOneCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SellerCarMaintainenceOrderDetailSectionOneCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLbl;
- (void)setupData:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
