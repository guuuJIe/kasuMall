//
//  SellerCarMaintainenceSectionTwoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCarMaintainenceSectionTwoCell.h"
@interface SellerCarMaintainenceSectionTwoCell()
@property (nonatomic, strong) UILabel *totalPriceLbl;
@property (nonatomic, strong) UILabel *workFeeLbl;
@property (nonatomic, strong) UILabel *materFeeLbl;
@end

@implementation SellerCarMaintainenceSectionTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UILabel *title = [UILabel new];
    title.text = @"保养价格";
    title.textColor = UIColor66;
    title.font = LabelFont14;
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(14);
    }];
    
    self.titleLbl = title;
//    UIView *line = [UIView new];
//    line.backgroundColor = UIColorED;
//    [self.contentView addSubview:line];
//    [line mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.mas_equalTo(self.contentView);
//        make.top.mas_equalTo(title.mas_bottom).offset(14);
//        make.height.mas_equalTo(lineHeihgt);
//    }];
    
    [self.contentView addSubview:self.totalPriceLbl];
    [self.totalPriceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title);
    }];
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"工时费";
    title2.textColor = UIColor66;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(title.mas_bottom).offset(14);
    }];
    
    [self.contentView addSubview:self.workFeeLbl];
    [self.workFeeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title2);
    }];
   
    
//    UIView *line2 = [UIView new];
//    line2.backgroundColor = UIColorED;
//    [self.contentView addSubview:line2];
//    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(title2);
//        make.right.mas_equalTo(self.contentView);
//        make.top.mas_equalTo(title2.mas_bottom).offset(14);
//        make.height.mas_equalTo(lineHeihgt);
//    }];
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"材料费";
    title3.textColor = UIColor66;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(title2.mas_bottom).offset(14);
    }];
    
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title3.mas_bottom).offset(14);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(title3);
    }];
    [self.contentView addSubview:self.materFeeLbl];
    [self.materFeeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title3);
    }];
    
    
}


- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.workFeeLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"workingCosts"]];
        self.totalPriceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"repairPrice"]];
        self.materFeeLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"materialCosts"]];
    }
}


- (UILabel *)totalPriceLbl{
    if (!_totalPriceLbl) {
        _totalPriceLbl = [UILabel new];
        _totalPriceLbl.textColor = UIColorFF3E;
        _totalPriceLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
        _totalPriceLbl.text = @"";
    }
    
    return _totalPriceLbl;
}


- (UILabel *)workFeeLbl{
    if (!_workFeeLbl) {
        _workFeeLbl = [UILabel new];
        _workFeeLbl.textColor = UIColor66;
        _workFeeLbl.font = LabelFont14;
        _workFeeLbl.text = @"¥";
    }
    
    return _workFeeLbl;
}

- (UILabel *)materFeeLbl{
    if (!_materFeeLbl) {
        _materFeeLbl = [UILabel new];
        _materFeeLbl.textColor = UIColor66;
        _materFeeLbl.font = LabelFont14;
        _materFeeLbl.text = @"¥";
    }
    
    return _materFeeLbl;
}

@end
