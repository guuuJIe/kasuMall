//
//  UploadFixFeeTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UploadFixFeeTableCell.h"
@interface UploadFixFeeTableCell()
@property (nonatomic, strong) UITextField *textField;

@end

@implementation UploadFixFeeTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIView *view = [UIView new];
    view.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
//    UILabel *title = [UILabel new];
//    title.text = @"总费用";
//    title.textColor = UIColor333;
//    title.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
//    [self.contentView addSubview:title];
//    [title mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(12);
//        make.top.mas_equalTo(view.mas_bottom).offset(14);
//    }];
//    UIView *line = [UIView new];
//    line.backgroundColor = UIColorED;
//    [self.contentView addSubview:line];
//    [line mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.mas_equalTo(self.contentView);
//        make.top.mas_equalTo(title.mas_bottom).offset(14);
//        make.height.mas_equalTo(lineHeihgt);
//    }];
//    [self.contentView addSubview:self.textField];
//    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(title.mas_right).offset(26);
//        make.centerY.mas_equalTo(title);
//        make.height.mas_equalTo(20);
//        make.right.mas_equalTo(-12);
//    }];
//
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"工时费用";
    title2.textColor = UIColor333;
    title2.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(view.mas_bottom).offset(14);
        make.size.mas_equalTo(CGSizeMake(80, 20));
    }];
    [self.contentView addSubview:self.textField1];
    [self.textField1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title2.mas_right).offset(0);
        make.centerY.mas_equalTo(title2);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(-12);
    }];

    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorED;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title2);
        make.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title2.mas_bottom).offset(14);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"材料费用";
    title3.textColor = UIColor333;
    title3.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line2.mas_bottom).offset(14);
        make.size.mas_equalTo(CGSizeMake(80, 20));
    }];
    
    [self.contentView addSubview:self.textField2];
    [self.textField2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title3.mas_right).offset(0);
        make.centerY.mas_equalTo(title3);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(-12);
    }];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title3.mas_bottom).offset(14);
        make.height.mas_equalTo(10);
        make.bottom.mas_equalTo(self.contentView);
    }];
}


- (UITextField *)textField{
    if (!_textField) {
        _textField = [UITextField new];
        _textField.placeholder = @"请输入保养费用";
        _textField.textColor = UIColor333;
        _textField.font = LabelFont14;
        _textField.textAlignment = NSTextAlignmentLeft;
        _textField.backgroundColor = UIColor.redColor;
    }
    
    return _textField;
}

- (UITextField *)textField1{
    if (!_textField1) {
        _textField1 = [UITextField new];
        _textField1.placeholder = @"请输入保养工时费用";
        _textField1.textColor = UIColor333;
        _textField1.font = LabelFont14;
        _textField1.textAlignment = NSTextAlignmentLeft;
//        _textField1.backgroundColor = UIColor.purpleColor;
    }
    
    return _textField1;
}


- (UITextField *)textField2{
    if (!_textField2) {
        _textField2 = [UITextField new];
        _textField2.placeholder = @"请输入保养材料费用";
        _textField2.textColor = UIColor333;
        _textField2.font = LabelFont14;
        _textField2.textAlignment = NSTextAlignmentLeft;
//        _textField2.backgroundColor = UIColor.redColor;
    }
    
    return _textField2;
}
@end
