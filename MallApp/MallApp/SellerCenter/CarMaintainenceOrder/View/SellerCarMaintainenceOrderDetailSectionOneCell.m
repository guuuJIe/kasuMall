//
//  SellerCarMaintainenceOrderDetailSectionOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCarMaintainenceOrderDetailSectionOneCell.h"
@interface SellerCarMaintainenceOrderDetailSectionOneCell()
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *telLbl;
@end

@implementation SellerCarMaintainenceOrderDetailSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    UIView *view = [UIView new];
    view.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
    UILabel *title = [UILabel new];
    title.text = @"联系资料";
    title.textColor = UIColor333;
    title.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(view.mas_bottom).offset(14);
    }];
    UIView *line = [UIView new];
    line.backgroundColor = UIColorED;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title.mas_bottom).offset(14);
         make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"姓名";
    title2.textColor = UIColor66;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(14);
    }];
    
    [self.contentView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title2);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorED;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title2);
        make.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title2.mas_bottom).offset(14);
        make.height.mas_equalTo(lineHeihgt);
    }];
       
    UILabel *title3 = [UILabel new];
    title3.text = @"联系方式";
    title3.textColor = UIColor66;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line2.mas_bottom).offset(14);
    }];
    [self.contentView addSubview:self.telLbl];
    [self.telLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title3);
    }];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title3.mas_bottom).offset(14);
        make.height.mas_equalTo(10);
    }];
    
    
    UILabel *title4 = [UILabel new];
    title4.text = @"保养项目";
    title4.textColor = UIColor333;
    title4.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [self.contentView addSubview:title4];
    [title4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line3.mas_bottom).offset(14);
    }];
    self.titleLbl = title4;
    UIView *line4 = [UIView new];
    line4.backgroundColor = UIColorED;
    [self.contentView addSubview:line4];
    [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title4.mas_bottom).offset(14);
        make.height.mas_equalTo(lineHeihgt);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contact"][@"name"] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contact"][@"phone"] andDefailtInfo:@""];
    }
}

- (UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [UILabel new];
        _nameLbl.textColor = UIColor333;
        _nameLbl.font = LabelFont14;
        _nameLbl.text = @"姓名";
    }
    
    return _nameLbl;
}

- (UILabel *)telLbl{
    if (!_telLbl) {
        _telLbl = [UILabel new];
        _telLbl.textColor = UIColor333;
        _telLbl.font = LabelFont14;
        _telLbl.text = @"联系方式";
    }
    
    return _telLbl;
}

@end
