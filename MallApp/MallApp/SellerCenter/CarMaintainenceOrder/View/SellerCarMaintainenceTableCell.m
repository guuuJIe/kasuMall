//
//  SellerCarMaintainenceTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCarMaintainenceTableCell.h"
@interface SellerCarMaintainenceTableCell()
@property (nonatomic, strong) UILabel *timesLbl;
@property (nonatomic, strong) UILabel *stautesLbl;
@end

@implementation SellerCarMaintainenceTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    return self;
}


- (void)setupUI{
    UIView *view = [UIView new];
    view.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
    UILabel *title = [UILabel new];
    title.text = @"保养清单";
    title.textColor = UIColor333;
    title.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(view.mas_bottom).offset(14);
    }];
    self.titleLbl = title;
    UIView *line = [UIView new];
    line.backgroundColor = UIColorED;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title.mas_bottom).offset(14);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"保养次数";
    title2.textColor = UIColor66;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(14);
    }];
    self.titleLbl2 = title2;
    
    [self.contentView addSubview:self.timesLbl];
    [self.timesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title2);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorED;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title2);
        make.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title2.mas_bottom).offset(14);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"结算状态";
    title3.textColor = UIColor66;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line2.mas_bottom).offset(14);
    }];
    [self.contentView addSubview:self.stautesLbl];
    [self.stautesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(title3);
    }];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title3.mas_bottom).offset(14);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(title3);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.timesLbl.text = [NSString stringWithFormat:@"%@",dic[@"repairNumber"]];
        NSInteger statues = [dic[@"isCheckOut"] integerValue];
        if (statues == 0) {
            self.stautesLbl.text = @"未上传";
        }else if (statues == 1){
            self.stautesLbl.text = @"未结算";
        }else if (statues == 2){
            self.stautesLbl.text = @"已结算";
        }
        
    }
}


- (UILabel *)timesLbl{
    if (!_timesLbl) {
        _timesLbl = [UILabel new];
        _timesLbl.textColor = UIColor66;
        _timesLbl.font = LabelFont14;
        _timesLbl.text = @"";
    }
    
    return _timesLbl;
}

- (UILabel *)stautesLbl{
    if (!_stautesLbl) {
        _stautesLbl = [UILabel new];
        _stautesLbl.textColor = APPColor;
        _stautesLbl.font = LabelFont14;
        _stautesLbl.text = @"";
    }
    
    return _stautesLbl;
}


@end
