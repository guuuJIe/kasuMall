//
//  SellerVerifyOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerVerifyOrderVC.h"
#import "MemberApi.h"
#import "SellerCarMaintainenceOrderDetailVC.h"
@interface SellerVerifyOrderVC ()

@property (nonatomic, strong) UIButton *actBtn;
@property (nonatomic, strong) UITextField *mileAgeText;
@property (nonatomic, strong) UITextField *carNumText;
@property (nonatomic, strong) UITextField *vinText;
@property (nonatomic, strong) UITextField *carTypeText;
@property (nonatomic, strong) MemberApi *memberApi;
@end

@implementation SellerVerifyOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self setupUI];
    
    [self setupData];
}

- (void)setupUI{
    self.title = @"车辆验证";
    
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(45);
    }];
    UILabel *title = [UILabel new];
    title.text = @"里程数";
    title.textColor = UIColor333;
    title.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [view addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(view);
    }];
    UITextField *text = [UITextField new];
    text.placeholder = @"请输入里程数(公里)";
    text.textColor = UIColor333;
    text.font = LabelFont14;
    text.textAlignment = NSTextAlignmentRight;
    text.keyboardType = UIKeyboardTypeNumberPad;
    [view addSubview:text];
    [text mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(view);
        make.size.mas_equalTo(CGSizeMake(140, 20));
    }];
    self.mileAgeText = text;
//    UIView *line = [UIView new];
//    line.backgroundColor = UIColorEF;
//    [view addSubview:line];
//    [line mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.mas_equalTo(view);
//        make.height.mas_equalTo(lineHeihgt);
//        make.bottom.mas_equalTo(view);
//    }];
    
    
    
    UIView *view2 = [UIView new];
    view2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view2];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_bottom).offset(1);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(45);
    }];
    UILabel *title2 = [UILabel new];
    title2.text = @"车牌号";
    title2.textColor = UIColor333;
    title2.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [view2 addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(view2);
    }];
    UITextField *text2 = [UITextField new];
    text2.placeholder = @"请输入车牌号";
    text2.textColor = UIColor333;
    text2.font = LabelFont14;
    text2.userInteractionEnabled = false;
    text2.textAlignment = NSTextAlignmentRight;
    [view2 addSubview:text2];
    [text2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(view2);
        make.size.mas_equalTo(CGSizeMake(170, 20));
    }];
    self.carNumText = text2;
   
       
    
    UIView *view3 = [UIView new];
    view3.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view3];
    [view3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view2.mas_bottom).offset(1);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(45);
    }];
    UILabel *title3 = [UILabel new];
    title3.text = @"VIN码";
    title3.textColor = UIColor333;
    title3.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [view3 addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(view3);
    }];
    UITextField *text3 = [UITextField new];
    text3.placeholder = @"请输VIN码";
    text3.textColor = UIColor333;
    text3.font = LabelFont14;
    text3.userInteractionEnabled = false;
    text3.textAlignment = NSTextAlignmentRight;
    [view3 addSubview:text3];
    [text3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(view3);
        make.size.mas_equalTo(CGSizeMake(170, 20));
    }];
    self.vinText = text3;
    
    
    UIView *view4 = [UIView new];
    view4.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view4];
    [view4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view3.mas_bottom).offset(1);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(45);
    }];
    UILabel *title4 = [UILabel new];
    title4.text = @"车型";
    title4.textColor = UIColor333;
    title4.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [view4 addSubview:title4];
    [title4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(view4);
    }];
    UITextField *text4 = [UITextField new];
    text4.placeholder = @"车型";
    text4.textColor = UIColor333;
    text4.font = LabelFont14;
    text4.userInteractionEnabled = false;
    text4.textAlignment = NSTextAlignmentRight;
    [view4 addSubview:text4];
    [text4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(view4);
        make.size.mas_equalTo(CGSizeMake(170, 20));
    }];
    self.carTypeText = text4;
    
    
    [self.view addSubview:self.actBtn];
    [self.actBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(view4.mas_bottom).offset(118*AdapterScal);
    }];
    
}


- (void)setupData{
    self.carNumText.text = [XJUtil insertStringWithNotNullObject:self.dic[@"carInfo"][@"numberPlate"] andDefailtInfo:@""];
    self.carTypeText.text = [XJUtil insertStringWithNotNullObject:self.dic[@"carInfo"][@"ocsaModel"] andDefailtInfo:@""];
    self.vinText.text = [XJUtil insertStringWithNotNullObject:self.dic[@"carInfo"][@"vin"] andDefailtInfo:@""];
}

- (void)click{
    if (!self.mileAgeText.text) {
        [JMBManager showBriefAlert:@"请输入里程数"];
        
        return;
    }
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:self.orderNum forKey:@"orderNumber"];
    [dic setValue:@([self.mileAgeText.text integerValue]) forKey:@"mileage"];
    
    [self.memberApi sellerEwEmOrderCheckWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            SellerCarMaintainenceOrderDetailVC *vc = [SellerCarMaintainenceOrderDetailVC new];
            vc.orderNum = self.orderNum ;
            [self.navigationController pushViewController:vc animated:true];
        }
    }];
    
}

- (MemberApi *)memberApi{
    if (!_memberApi) {
        _memberApi = [MemberApi new];
    }
    
    return _memberApi;
}

-(UIButton *)actBtn
{
    if(!_actBtn)
    {
        _actBtn=[UIButton  new];
        [_actBtn setTitle:@"立即验证" forState:UIControlStateNormal];
        _actBtn.layer.cornerRadius = 4;
        _actBtn.titleLabel.font = LabelFont14;
        [_actBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_actBtn setBackgroundColor:UIColor276];
        [_actBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [_actBtn setBackgroundImage:[UIImage imageWithColor:UIColorCC] forState:UIControlStateDisabled];
        
    }
    return _actBtn;
}
@end
