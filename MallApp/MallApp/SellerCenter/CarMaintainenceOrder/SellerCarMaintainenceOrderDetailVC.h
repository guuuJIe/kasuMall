//
//  SellerCarMaintainenceOrderDetailVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SellerCarMaintainenceOrderDetailVC : BaseViewController
@property (nonatomic, strong) NSString *orderNum;
@property (nonatomic, assign) NSInteger ids;
@property (nonatomic, assign) NSInteger type;//1保修 2保养
@end

NS_ASSUME_NONNULL_END
