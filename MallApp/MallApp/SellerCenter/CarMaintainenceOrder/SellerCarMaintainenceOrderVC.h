//
//  SellerCarMaintainenceOrderVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SellerCarMaintainenceOrderVC : BaseViewController
@property (nonatomic, assign) NSInteger type;//1=保修，2=保养
@end

NS_ASSUME_NONNULL_END
