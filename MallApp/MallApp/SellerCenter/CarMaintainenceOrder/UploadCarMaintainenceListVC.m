//
//  UploadCarMaintainenceListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UploadCarMaintainenceListVC.h"
#import "TZImagePickerController.h"
#import "PurchaseView.h"
#import "ServicesOrderItemCell.h"
#import "BoldTitleTableCell.h"
#import "ServicesRepairSectionThreeCell.h"
#import "UploadFixFeeTableCell.h"
#import "MineApi.h"
#import "GroupApi.h"
@interface UploadCarMaintainenceListVC ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate>

@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, strong) dispatch_semaphore_t lock;
@property (nonatomic, strong) PurchaseView *purchaseView;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) NSString *picStr;
@property (nonatomic, strong) UITextField *feeText;
@property (nonatomic, strong) UITextField *materialText;
@property (nonatomic, strong) GroupApi *groupApi;
@end

@implementation UploadCarMaintainenceListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)setupUI{
    self.title = @"上传清单";
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-50-BottomAreaHeight);
    }];
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        
        static NSString *Identifier = @"BoldTitleTableCell";
        BoldTitleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        return cell;
        
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"ServicesOrderItemCell";
        ServicesOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dic[@"product"]];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        return cell;
    }else if (indexPath.row == 2){
        static NSString *Identifier = @"UploadFixFeeTableCell";
        UploadFixFeeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        self.feeText = cell.textField1;
        self.materialText = cell.textField2;
        return cell;
    }else if (indexPath.row == 3){
        static NSString *Identifier = @"ServicesRepairSectionThreeCell";
        ServicesRepairSectionThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        WeakSelf(self)
        cell.clickBlock = ^(NSInteger index) {
            if (index == weakself.selectedPhotos.count) {
                [weakself selectPhoto];
            }else{
                [weakself checkPhoto:index];
            }
        };
        [cell setupCollectionDatawith:_selectedPhotos andAssets:_selectedAssets];
        return cell;
    }
    
    
    return nil;
}

- (void)selectPhoto{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:6 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    imagePickerVc.maxImagesCount = 6;
    imagePickerVc.allowTakePicture = true; // 在内部显示拍照按钮
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    WeakSelf(self)
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        JLog(@"%@ --- %@",assets,photos);
        weakself.selectedAssets = [NSMutableArray arrayWithArray:assets];
        weakself.selectedPhotos = [NSMutableArray arrayWithArray:photos];
        dispatch_async(dispatch_get_main_queue(), ^{
             [self.listTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        });
       
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)checkPhoto:(NSInteger)index{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:_selectedAssets selectedPhotos:_selectedPhotos index:index];
    
    
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)submitInfoAct{
     dispatch_async(dispatch_get_global_queue(0, 0), ^{
    //        if (self.selectedPhotos.count >0) {
            NSMutableArray *imagesObj = [NSMutableArray new];
            if (self.selectedPhotos.count != 0) {
                for (int i = 0; i<self.selectedPhotos.count; i++) {
                    NSDictionary *dic = @{@"name":@"files",@"image":self.selectedPhotos[i]};
                    [imagesObj addObject:dic];
                }
                
                [JMBManager showLoading];
                
                [self.mineApi uploadPicWithparameters:@"5" VisitImagesArr:imagesObj withCompletionHandler:^(NSError *error, MessageBody *result) {
                    
                    JLog(@"%@",result.result);
                    NSArray *datas = result.result;
                    self.picStr = [datas componentsJoinedByString:@","];
                    [JMBManager hideAlert];
                    
                    XJ_UNLOCK(self.lock);
                }];
                
                //        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER); // 在A请求未结束前 线程阻赛在这。直接A请求结束调用semaphore_signal信号量+1。不阻塞
                XJ_LOCK(self.lock);
            }
        
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (!self.feeText.text || !self.materialText.text) {
                 [JMBManager showBriefAlert:@"请输入相关费用"];
                 return ;
             }
             NSMutableDictionary *dic = [NSMutableDictionary new];
//             [dic setValue:self.dic[@"orderNumber"] forKey:@"orderNumber"];
             [dic setValue:@([self.feeText.text doubleValue]) forKey:@"workingCosts"];
             [dic setValue:@([self.materialText.text doubleValue]) forKey:@"materialCosts"];
             [dic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"pic"];
             [dic setValue:self.dic[@"recordId"] forKey:@"recordId"];
             
             [self.groupApi sellerSubmitRepiarationCompleteWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                 if (result.code == 200) {
                     if (self.refreshBlock) {
                         self.refreshBlock();
                         [self.navigationController popViewControllerAnimated:true];
                     }
                 }
             }];
             
         });
         
         
         
         
         
         
        });
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.showsVerticalScrollIndicator = false;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderItemCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderItemCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesRepairSectionTwoCell" bundle:nil] forCellReuseIdentifier:@"ServicesRepairSectionTwoCell"];
        [_listTableView registerClass:[BoldTitleTableCell class] forCellReuseIdentifier:@"BoldTitleTableCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesRepairSectionThreeCell" bundle:nil] forCellReuseIdentifier:@"ServicesRepairSectionThreeCell"];
        [_listTableView registerClass:[UploadFixFeeTableCell class] forCellReuseIdentifier:@"UploadFixFeeTableCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"提交" forState:0];
        WeakSelf(self);
        _purchaseView.addAddressBlock = ^{

            [weakself submitInfoAct];
            
        };
    }
    return _purchaseView;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    
    return _mineApi;
}

- (dispatch_semaphore_t)lock {
    if (!_lock) {
        _lock = dispatch_semaphore_create(0);
    }
    return _lock;
}

- (GroupApi *)groupApi{
    if (!_groupApi) {
        _groupApi = [GroupApi new];
    }
    
    return _groupApi;
}

@end
