//
//  MyGoodsOrderDetailVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerOrderDetailVC.h"
#import "GoodsOrderDetailOneCell.h"
#import "GoodsPackageTableCell.h"
#import "GoodsAddressTableCell.h"
#import "OrdetItemCell.h"
#import "GoodsOrderDetailStateCell.h"
#import "GoodsOrderDetailSectionSixCell.h"
#import "SellerOrderDetailOpView.h"
#import "OrderApi.h"
#import "OrdePayVC.h"
#import "SellerDelieverGoodsVC.h"
#import "SellerSendInvoiceVC.h"
#import "FapiaoDetailVC.h"
@interface SellerOrderDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) SellerOrderDetailOpView *opView;
@property (nonatomic) OrderApi *orderApi;
@property (nonatomic) NSDictionary *dataDic;

@end

@implementation SellerOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单详情";
    
    [self setupUI];
    
    
    [self setupData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-50-BottomAreaHeight);
    }];
    
    [self.view addSubview:self.opView];
    [self.opView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
//    [self.opView setOrderOption];
}

- (void)setupData{
//    if (self.type == 2) {
//
//        [self.orderApi groupOrderInfoWithparameters:self.ids withCompletionHandler:^(NSError *error, MessageBody *result) {
//            if (result.code == 200) {
//                NSArray *dataArr= result.resultDic[@"list"];
//                self.dataDic = dataArr.firstObject;
//                [self.opView setOrderOption:self.dataDic];
//                [self.listTableView reloadData];
//            }
//        }];
//    }else if (self.type == 1){
       [self.orderApi goodsOrderInfoWithparameters:self.ids withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *dataArr= result.resultDic[@"list"];
                self.dataDic = dataArr.firstObject;
                [self.opView setOrderOption:self.dataDic];
                [self.listTableView reloadData];
            }
        }];
//    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 6;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 3) {
        NSArray *data = self.dataDic[@"products"];
        return data.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"GoodsOrderDetailOneCell";
        GoodsOrderDetailOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[GoodsOrderDetailOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic];
        return cell;
    }else if(indexPath.section == 1){
        static NSString *Identifier = @"GoodsPackageTableCell";
        GoodsPackageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[GoodsPackageTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"GoodsAddressTableCell";
        GoodsAddressTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[GoodsAddressTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic[@"address"]];
        return cell;
    }else if (indexPath.section == 3){
        static NSString *Identifier = @"OrdetItemCell";
        OrdetItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[OrdetItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        NSArray *data = self.dataDic[@"products"];
        cell.backgroundColor = [UIColor whiteColor];
        [cell setupData:data[indexPath.row]];
        return cell;
    }else if (indexPath.section == 4){
        static NSString *Identifier = @"GoodsOrderDetailStateCell";
        GoodsOrderDetailStateCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[GoodsOrderDetailStateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        cell.backgroundColor = [UIColor whiteColor];
         [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.section == 5){
        static NSString *Identifier = @"GoodsOrderDetailSectionSixCell";
        GoodsOrderDetailSectionSixCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[GoodsOrderDetailSectionSixCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        cell.clickBlock = ^{
            NSDictionary *invoiceDic = self.dataDic[@"invoice"];
            NSInteger statues = [invoiceDic[@"status"] integerValue];
            if (![invoiceDic[@"type"] isEqual:[NSNull null]] && statues == 0) {
                FapiaoDetailVC *vc = [[FapiaoDetailVC alloc] initWithNibName:@"FapiaoDetailVC" bundle:nil];
                vc.dic = invoiceDic;
                [self.navigationController pushViewController:vc animated:true];
            }else{
               
                if (statues == 1){
                    SellerSendInvoiceVC *vc = [[SellerSendInvoiceVC alloc] initWithNibName:@"SellerSendInvoiceVC" bundle:nil];
                    vc.statues = statues;
                    vc.orderNum = self.dataDic[@"orderNumber"];
                    vc.dic = invoiceDic;
                    vc.clickBlock = ^{
                        [self setupData];
                    };
                    [self.navigationController pushViewController:vc animated:true];
                }else if (statues == 2){
                    SellerSendInvoiceVC *vc = [[SellerSendInvoiceVC alloc] initWithNibName:@"SellerSendInvoiceVC" bundle:nil];
                    vc.statues = statues;
                    vc.orderNum = self.dataDic[@"orderNumber"];
                    vc.dic = invoiceDic;
                    [self.navigationController pushViewController:vc animated:true];
                }
            }
            
        };
        
        [cell setupData:self.dataDic withType:1];
        
        return cell;
    }
    
    
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //orderStatus 7 交易完成 3是待收货 4是待发货 5//已完成
    NSInteger statues = [self.dataDic[@"orderStatus"] integerValue];
    if (indexPath.section == 1) {
        if (statues == 3 || statues == 4 || statues == 5) {
            return UITableViewAutomaticDimension;
        }else{
            return CGFLOAT_MIN;
        }
    }else{
        return UITableViewAutomaticDimension;
    }

}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
//        _listTableView.rowHeight = UITableViewAutomaticDimension;
//        _listTableView.estimatedRowHeight = 40;

        _listTableView.showsVerticalScrollIndicator = false;
     
        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];

        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_listTableView registerNib:[UINib nibWithNibName:@"GoodsOrderDetailOneCell" bundle:nil] forCellReuseIdentifier:@"GoodsOrderDetailOneCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"GoodsPackageTableCell" bundle:nil] forCellReuseIdentifier:@"GoodsPackageTableCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"GoodsAddressTableCell" bundle:nil] forCellReuseIdentifier:@"GoodsAddressTableCell"];
         [_listTableView registerNib:[UINib nibWithNibName:@"GoodsOrderDetailStateCell" bundle:nil] forCellReuseIdentifier:@"GoodsOrderDetailStateCell"];
         [_listTableView registerNib:[UINib nibWithNibName:@"GoodsOrderDetailSectionSixCell" bundle:nil] forCellReuseIdentifier:@"GoodsOrderDetailSectionSixCell"];
//        [_listTableView registerClass:[GoodsOrderDetailOneCell class] forCellReuseIdentifier:@"InfoFourCell"];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}



- (SellerOrderDetailOpView *)opView{
    if (!_opView) {
        _opView = [SellerOrderDetailOpView new];
        WeakSelf(self)
        _opView.orderHandleBlock = ^(NSString * _Nonnull flag) {
            StrongSelf(self);
            if ([flag isEqualToString:@"orderStart"]) {
                SellerDelieverGoodsVC *vc = [[SellerDelieverGoodsVC alloc] initWithNibName:@"SellerDelieverGoodsVC" bundle:nil];
                vc.orderNum = self.dataDic[@"orderNumber"];
                vc.clickBlock = ^{
                    [self setupData];
                };
                [self.navigationController pushViewController:vc animated:true];
            }
            
        };
    }
    return _opView;
}

-(OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    
    return _orderApi;
}

@end
