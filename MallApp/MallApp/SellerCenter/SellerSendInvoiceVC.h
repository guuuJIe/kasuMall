//
//  SellerSendInvoiceVC.h
//  MallApp
//
//  Created by Mac on 2020/2/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SellerSendInvoiceVC : BaseViewController
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic,strong) NSString *orderNum;
@property (nonatomic,assign) NSInteger statues;
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
