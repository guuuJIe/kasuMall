//
//  SellerDelieverGoodsVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SellerDelieverGoodsVC : BaseViewController
@property (nonatomic, strong) NSString *orderNum;
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
