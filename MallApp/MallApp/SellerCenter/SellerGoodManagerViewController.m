//
//  SellerGoodManagerViewController.m
//  MallApp
//
//  Created by Mac on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerGoodManagerViewController.h"
#import "MinePromptView.h"
#import "SellerGoodsCell.h"
#import "GoodsOperationView.h"
#import "JJOptionView.h"
#import "SellerCenterApi.h"
#import "FixCardRecordVC.h"
@interface SellerGoodManagerViewController ()<UITableViewDelegate,UITableViewDataSource,JJOptionViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong) MinePromptView *promptView;
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) UILabel *typeLabel;
@property (nonatomic,strong) SellerCenterApi *centerApi;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,strong) NSString *statues;
@property (nonatomic,strong) UITextField *textF;
@property (nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation SellerGoodManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"商品管理";
    [self setupLayout];
    [self setupProp];
    [self.listTableView.mj_header beginRefreshing];
}

- (void)setupLayout{
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(8, 8, Screen_Width - 16, 28)];
    [self.view addSubview:bg];
    bg.backgroundColor = [UIColor whiteColor];
    bg.layer.cornerRadius = 4;
    
    
    JJOptionView *view = [[JJOptionView alloc] initWithFrame:CGRectMake(5, 5, 100, 20) dataSource:@[@"全部类型",@"汽车配件",@"汽车精品",@"维修项目"]];
    view.delegate = self;
    [bg addSubview:view];
//    UILabel *label = [[UILabel alloc] init];
//    label.text = @"全部类型";
//    label.numberOfLines = 0;
//    label.textColor = UIColor66;
//    label.font = LabelFont14;
//    label.userInteractionEnabled = true;
//    [bg addSubview:label];
//    self.typeLabel = label;
//    [label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(bg);
//        make.left.mas_equalTo(8);
//    }];
//
//    [self.typeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
//
//    UIButton *button = [UIButton new];
//    [button setImage:[UIImage imageNamed:@"downArrow"] forState:0];
//    [bg addSubview:button];
//    [button mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(label.mas_right).offset(2);
//        make.centerY.mas_equalTo(label);
//        make.size.mas_equalTo(CGSizeMake(12, 12));
//    }];
    
    UITextField *text = [UITextField new];
    text.placeholder = @"请输入搜索内容";
    text.font = LabelFont14;
    [bg addSubview:text];
    text.delegate = self;
    text.returnKeyType = UIReturnKeySearch;
    [text mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(view.mas_right).offset(5);
        make.centerY.equalTo(view);
        make.width.mas_equalTo(@200);
    }];
    self.textF = text;
    [self.view addSubview:self.promptView];
    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(bg.mas_bottom).offset(10);
    }];
    self.promptView.orderStatues = 200;
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.promptView.mas_bottom).offset(1);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
   
}

- (void)setupProp{
    self.promptView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = @"2";
    self.type = 0;
}

- (void)getData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    NSDictionary *dic = @{@"proType":@(self.type),@"proName":self.textF.text,@"Passed":self.statues};
    [self.centerApi sellProductListWithparameters:dic andNum:self.num andsize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        [self.listTableView.mj_header endRefreshing];
        if (result.code == 200) {
          
            NSArray *array = result.result;
            [self configData:array withRefreshType:type];
            
        }
    }];
}

- (void)configData:(NSArray *)array withRefreshType:(RefreshType)type{
   
    if (type == RefreshTypeDown) {
        if (array.count <20) {
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
        self.dataArray = [NSMutableArray arrayWithArray:array];
    }else if (type == RefreshTypeUP){
        if (array.count == 0) {
            self.num --;
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            for (int i = 0; i<array.count; i++) {
                NSDictionary *dic = array[i];
                [self.dataArray addObject:dic];
            }
            //            [self.datas addObjectsFromArray:array];
            [self.listTableView.mj_footer endRefreshing];
        }
    }else if (type == RefreshTypeNormal){
         self.dataArray = [NSMutableArray arrayWithArray:array];
    }

    
    [self.listTableView reloadData];
    
}

#pragma mark ---JJOptionViewDelegate---
- (void)optionView:(JJOptionView *)optionView selectedIndex:(NSInteger)selectedIndex{
    if (selectedIndex == 0) {
        self.type = 0;
    }else if (selectedIndex == 1){
        self.type = 3;
    }else if (selectedIndex == 2){
        self.type = 4;
    }else if (selectedIndex == 3){
        self.type = 8;
    }//
    
    [self getData:RefreshTypeNormal];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self getData:RefreshTypeNormal];
    return YES;
}


#pragma mark ---UITableViewDelegate---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"SellerGoodsCell";
    SellerGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[SellerGoodsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupdata:self.dataArray[indexPath.section]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    GoodsOperationView *opView = [GoodsOperationView new];
    [opView setOperationWithStatues:[self.statues integerValue] andProType:self.dataArray[section]];
    NSDictionary *dic = self.dataArray[section];
    WeakSelf(self)
    opView.orderHandleBlock = ^(NSString * _Nonnull flag) {
        if ([flag isEqualToString:@"shangjia"]) {
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定上架该产品？" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [weakself.centerApi sellProductUpLoadWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [weakself getData:RefreshTypeNormal];
                        }
                    }];
                }
            }];
        }else if ([flag isEqualToString:@"xiajia"]){
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定下架该产品？" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [weakself.centerApi sellProductDownWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [weakself getData:RefreshTypeNormal];
                        }
                    }];
                }
            }];
        }else if ([flag isEqualToString:@"services"]){
//            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定下架该产品？" andHandler:^(NSInteger index) {
//                if (index == 1) {
//                    [self.centerApi sellProductDownWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
//                        if (result.code == 200) {
//                            [self getData:RefreshTypeNormal];
//                        }
//                    }];
//                }
//            }];
            
            FixCardRecordVC *vc = [FixCardRecordVC new];
            vc.proId = [NSString stringWithFormat:@"%@",dic[@"id"]];
            [weakself.navigationController pushViewController:vc animated:true];
        }
    };
    return opView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 64.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (MinePromptView *)promptView{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        _promptView.dataSource = @[@"未上架",@"已上架"];
        WeakSelf(self)
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
                    self.statues = @"2";
                    break;
                case 1:
                    self.statues = @"1";
                    break;
                
                default:
                    break;
            }
             [self.listTableView.mj_header beginRefreshing];
        };
    }
    return _promptView;
}

-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
//        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
//        _listTableView.tableHeaderView = [UIView new];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        _listTableView.estimatedSectionFooterHeight = 0;
        _listTableView.estimatedSectionHeaderHeight = 0;
        [_listTableView registerNib:[UINib nibWithNibName:@"SellerGoodsCell" bundle:nil] forCellReuseIdentifier:@"SellerGoodsCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeDown];
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (SellerCenterApi *)centerApi{
    if (!_centerApi) {
        _centerApi = [SellerCenterApi new];
    }
    return _centerApi;
}
@end
