//
//  SellerOrderManagerVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerOrderManagerVC.h"
#import "SellerCenterGoodsOrderVC.h"
#import "SellerFixOrderListVC.h"
#import "SellerOrderRefundVC.h"
#import "SellerCenteWeixiuTaocanOrderVC.h"
#import "SellerCarMaintainenceOrderVC.h"
#import "SellerRepairOrderVC.h"
@interface SellerOrderManagerVC ()
@property (weak, nonatomic) IBOutlet UIView *checkFixOrderView;
@property (weak, nonatomic) IBOutlet UIView *carMaintainenceView;
@property (weak, nonatomic) IBOutlet UIView *carFixOrderView;
@property (weak, nonatomic) IBOutlet UIView *userRepairView;
@property (weak, nonatomic) IBOutlet UIView *userfixView;

@end

@implementation SellerOrderManagerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"订单管理";
    [self.orderView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.fixOrderView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.refundView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.checkFixOrderView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.carMaintainenceView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.carFixOrderView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.userRepairView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.userfixView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            SellerCenterGoodsOrderVC *vc = [SellerCenterGoodsOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 101:
        {
            SellerFixOrderListVC *vc = [SellerFixOrderListVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 102:
        {
            SellerOrderRefundVC *vc = [SellerOrderRefundVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
            
        case 103:
        {
            SellerCenteWeixiuTaocanOrderVC *vc = [SellerCenteWeixiuTaocanOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 104:{
            SellerCarMaintainenceOrderVC *vc = [SellerCarMaintainenceOrderVC new];
            vc.type = 2;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 105:{
            SellerCarMaintainenceOrderVC *vc = [SellerCarMaintainenceOrderVC new];
            vc.type = 1;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 106:{
            SellerRepairOrderVC *vc = [SellerRepairOrderVC new];

            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        default:
            break;
    }
}

@end
