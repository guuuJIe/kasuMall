//
//  SellerDelieverGoodsVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerDelieverGoodsVC.h"
#import "LogisticListVC.h"
#import "SellerCenterApi.h"
@interface SellerDelieverGoodsVC ()
@property (weak, nonatomic) IBOutlet UIView *logsticisView;
@property (weak, nonatomic) IBOutlet UITextField *companyText;
@property (weak, nonatomic) IBOutlet UITextField *orderNumText;
@property (nonatomic,strong) SellerCenterApi *centerApi;
@end

@implementation SellerDelieverGoodsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"发货";
    [self.logsticisView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
}
- (IBAction)confirm:(id)sender {
    if (self.companyText.text.length<0 || self.orderNumText.text<0) {
        [JMBManager showBriefAlert:@"请填写完整信息"];
        return;
    }
    
    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确认信息无误？" andHandler:^(NSInteger index) {
        if (index == 1) {
            NSDictionary *dic = @{@"OrderNumber":self.orderNum,@"KdType":self.companyText.text,@"KdNo":self.orderNumText.text};
            
            [self.centerApi orderCompltetSendWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    if (self.clickBlock) {
                        self.clickBlock();
                    }
                    [self.navigationController popViewControllerAnimated:true];
                }
            }];
        }
    }];
    
    
}

- (void)click{
    LogisticListVC *vc = [LogisticListVC new];
    vc.clickBlock = ^(NSDictionary * _Nonnull dic) {
        self.companyText.text = dic[@"kdType"];
    };
    [self.navigationController pushViewController:vc animated:true];
}

- (SellerCenterApi *)centerApi{
    if (!_centerApi) {
        _centerApi = [SellerCenterApi new];
    }
    return _centerApi;
}

@end
