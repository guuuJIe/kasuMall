//
//  FixCardSearchResultVC.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixCardSearchResultVC.h"
#import "PurchaseView.h"
#import "YN_PassWordView.h"
#import "CustomePwdView.h"
#import "IQKeyboardManager.h"
#import "MemberApi.h"
@interface FixCardSearchResultVC ()<UIScrollViewDelegate>
@property (nonatomic, strong) PurchaseView *submitView;
@property (nonatomic, strong) CustomePwdView *pwdView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *accountLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UILabel *createLbl;
@property (weak, nonatomic) IBOutlet UILabel *sellerNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *belongsLbl;
@property (weak, nonatomic) IBOutlet UILabel *isUsedLbl;
@property (weak, nonatomic) IBOutlet UILabel *usernameLbl;
@property (weak, nonatomic) IBOutlet UILabel *useTimeLbl;
@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet UIView *useTimeView;
@property (nonatomic, strong) MemberApi *api;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@end

@implementation FixCardSearchResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     // 添加通知监听见键盘弹出/退出
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillHideNotification object:nil];
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"ScanVC"]];
    [self configUI];
    [self configData];
   
}


- (void)configData{
    [self.api searchFixCardWithparameters:self.accountText withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
//            FixCardSearchResultVC *vc = [[FixCardSearchResultVC alloc] initWithNibName:@"FixCardSearchResultVC" bundle:nil];
//            vc.dic = datas.firstObject;
//            [self.navigationController pushViewController:vc animated:true];
            [self setupDataWithDic:datas.firstObject];
        }else{
            self.bgView.hidden = true;
        }
    }];
}

- (void)setupDataWithDic:(NSDictionary *)dic{
    self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@""];
    self.accountLbl.text = dic[@"account"];
//    if ([userLevelGrade integerValue] >= 3) {
        self.moneyLbl.text = [NSString stringWithFormat:@"%@",dic[@"amount"]];
//        self.statesLbl.text = @"金额";
//    }else{
//        self.moneyLbl.text = [NSString stringWithFormat:@"%@",self.dic[@"password"]];
////        self.statesLbl.text = @"密码";
//    }
   
    self.createLbl.text = dic[@"created"];
    self.sellerNameLbl.text = dic[@"userShopName"];
    self.belongsLbl.text = dic[@"userShopUserName"];
    NSInteger isuesd = [dic[@"isUse"] integerValue];
    if (isuesd == 0) {
        self.isUsedLbl.text = @"未使用";
        self.userView.hidden = true;
        self.useTimeView.hidden = true;
        self.isUsedLbl.textColor = [UIColor colorWithHexString:@"23CC0C"];
        self.submitView.hidden = false;
    }else{
        self.isUsedLbl.text = @"已使用";
        self.userView.hidden = false;
        self.useTimeView.hidden = false;
        self.submitView.hidden = true;
        self.usernameLbl.text = dic[@"userUseShopUserName"];
        self.useTimeLbl.text = dic[@"useTime"];
        self.isUsedLbl.textColor = [UIColor colorWithHexString:@"FFA127"];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    //TODO: 页面appear 禁用
   [[IQKeyboardManager sharedManager] setEnable:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    //TODO: 页面Disappear 启用
   [[IQKeyboardManager sharedManager] setEnable:YES];
   [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)configUI{
    self.title = @"查询结果";
    [self.view addSubview:self.submitView];
    [self.submitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)userCard{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:self.accountText forKey:@"account"];
    [self.api readyUseFixCardWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [JMBManager showBriefAlert:@"使用成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:true];
            });
        }else if (result.code == 3011){
            [self.pwdView.passView.textF becomeFirstResponder];
        }
    }];
}
#pragma mark UITextFieldDelegate
// 点击非TextField区域取消第一响应者
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
     [self.pwdView.passView.textF resignFirstResponder];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    //方式一
    [self.view endEditing:YES];
   
}

// 键盘监听事件
- (void)keyboardAction:(NSNotification*)sender{
    // 通过通知对象获取键盘frame: [value CGRectValue]
    NSDictionary *useInfo = [sender userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    //取出键盘弹出需要花费的时间
    double duration = [useInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // <注意>具有约束的控件通过改变约束值进行frame的改变处理
    if([sender.name isEqualToString:UIKeyboardWillShowNotification]){
//        self.toBottom.constant = [value CGRectValue].size.height;
         [UIView animateWithDuration:duration animations:^{
             self.pwdView.frame = CGRectMake(0, self.view.frame.size.height - 200 - [value CGRectValue].size.height, Screen_Width, 200);
         }];
    }else{
        [UIView animateWithDuration:1 animations:^{
            self.pwdView.frame = CGRectMake(0, self.view.frame.size.height, Screen_Width, 200);
        }];
    }
}

- (PurchaseView *)submitView{
    if (!_submitView) {
        _submitView = [PurchaseView new];
        [_submitView.purchaseButton setTitle:@"使用此服务卡" forState:0];
        WeakSelf(self)
        _submitView.addAddressBlock = ^{
            StrongSelf(self)
            
            [self userCard];
            
        };
    }
    return _submitView;
}

- (CustomePwdView *)pwdView{
    if (!_pwdView) {
        _pwdView = [[CustomePwdView alloc] initWithFrame:CGRectMake(0,  self.view.frame.size.height, Screen_Width, 200)];
        _pwdView.passView.textF.keyboardType = UIKeyboardTypeASCIICapable;
        WeakSelf(self)
        _pwdView.finishBlock = ^(NSString * _Nonnull str) {
            StrongSelf(self)
            NSDictionary *dic = @{@"account":self.accountText,@"password":str};
            [self.api useFixCardWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [self.view endEditing:true];
                    [JMBManager showBriefAlert:@"使用成功"];
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        [self.navigationController popViewControllerAnimated:true];
//                    });
                    [self configData];
                }
            }];
           
        };
        [self.view addSubview:_pwdView];
    }
    return _pwdView;
}

- (MemberApi *)api{
    if (!_api) {
        _api = [MemberApi new];
    }
    return _api;
}
@end
