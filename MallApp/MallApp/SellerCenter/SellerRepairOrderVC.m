//
//  MineEmergencyOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerRepairOrderVC.h"
#import "MinePromptView.h"
#import "OrderApi.h"
#import "MyFixOrderItemCell.h"
#import "FixOrderHeadView.h"
#import "ServicesOfOrderFootView.h"
#import "SellerFixOrderDetailVC.h"
#import "CustomePwdView.h"
#import "IQKeyboardManager.h"
@interface SellerRepairOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) MinePromptView *promptView;
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) OrderApi *orderApi;
@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, strong) CustomePwdView *pwdView;
@end

@implementation SellerRepairOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"抢修/施救订单";
    [self initProp];
    
    [self setupUI];
    
    
    // 添加通知监听见键盘弹出/退出
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //TODO: 页面appear 禁用
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [self.pwdView.passView.textF resignFirstResponder];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //TODO: 页面Disappear 启用
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
}

- (void)initProp{
    self.num = 1;
    self.size = @"20";
    self.statues = 21;
    self.promptView.orderStatues = 200;
}


- (void)setupUI{
    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.promptView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)getData:(RefreshType)type{
    if (type == RefreshTypeDown) {
         self.num = 1;
     }else if (type == RefreshTypeUP){
         self.num = self.num+1;
     }
    [self.orderApi getSellerFastServiceOrderListDataWithparameters:[NSString stringWithFormat:@"%ld",(long)self.statues] andNum:[NSString stringWithFormat:@"%ld",(long)self.num] andsize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
         [self.listTableView.mj_header endRefreshing];
        if (result.code == 200) {
//            self.datas = result.result;
            NSArray *array = result.result;
            self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
            [self.listTableView reloadData];
           
        }
    }];
}

#pragma mark ---UITableViewDelegate----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    OrderListModel *model = self.datas[section];
//    return model.goodsList.count;
//    NSArray *arr = self.datas[section];
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"MyFixOrderItemCell";
    MyFixOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[MyFixOrderItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datas[indexPath.section]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FixOrderHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"FixOrderHeadView"];
    [headView setupSellerFixOrderData:self.datas[section]];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    NSDictionary *dic = self.datas[section];
    [footView setupOpeationViewWithDic:self.datas[section] andType:SellerEmergencyOrderOpe andIsNeedShow:true];
    WeakSelf(self)
    footView.clickBlock = ^(NSString * _Nonnull flag) {
        StrongSelf(self)
        if ([flag isEqualToString:@"orderCancel"]) {
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认取消订单" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [self.orderApi sellerCancelUserFastSetviceOrderWithparameters:@{@"orderNumber":dic[@"orderNumber"]} withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [self getData:RefreshTypeNormal];
                        }
                    }];
                }
            }];
        }else if ([flag isEqualToString:@"confirm"]){
            
            [self.pwdView.passView.textF becomeFirstResponder];
            self.pwdView.finishBlock = ^(NSString * _Nonnull str) {
                StrongSelf(self)
                [self.orderApi shopConfirmFixOrderCompleteWithparameters:@{@"OrderNumber":dic[@"orderNumber"],@"Code":str} withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        [self.pwdView.passView.textF resignFirstResponder];
                        [self getData:RefreshTypeNormal];
                    }
                }];
                
                
            };
          
        }
    };
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{

    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    [footView setupOpeationViewWithDic:self.datas[section] andType:SellerEmergencyOrderOpe andIsNeedShow:true];
    
    return [footView cellHeight];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    SellerFixOrderDetailVC *vc = [SellerFixOrderDetailVC new];
    NSDictionary *dic = self.datas[indexPath.section];
    vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:true];
}


// 键盘监听事件
- (void)keyboardAction:(NSNotification*)sender{
    // 通过通知对象获取键盘frame: [value CGRectValue]
    NSDictionary *useInfo = [sender userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    //取出键盘弹出需要花费的时间
    double duration = [useInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // <注意>具有约束的控件通过改变约束值进行frame的改变处理
    if([sender.name isEqualToString:UIKeyboardWillShowNotification]){
//        self.toBottom.constant = [value CGRectValue].size.height;
         [UIView animateWithDuration:duration animations:^{
             self.pwdView.frame = CGRectMake(0, self.view.frame.size.height - 200 - [value CGRectValue].size.height, Screen_Width, 200);
         }];
    }else{
        [UIView animateWithDuration:1 animations:^{
            self.pwdView.frame = CGRectMake(0, self.view.frame.size.height, Screen_Width, 200);
        }];
    }
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
//        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MyFixOrderItemCell" bundle:nil] forCellReuseIdentifier:@"MyFixOrderItemCell"];
        [_listTableView registerClass:[FixOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"FixOrderHeadView"];
//        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MinePromptView *)promptView{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        _promptView.dataSource = @[@"派单中",@"服务中",@"已完成"];
        WeakSelf(self)
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
                    
                    self.statues = 21;
                    
                    
                    break;
                case 1:
                    
                    self.statues = 22;
                    
                    break;
                case 2:
                    
                     self.statues = 6;
                    
                    break;
                case 3:
                    
                   
                    
                    break;
                default:
                    break;
            }
            [self.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_promptView];
    }
    
    return _promptView;
}


- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    
    return _orderApi;
}

- (CustomePwdView *)pwdView{
    if (!_pwdView) {
        _pwdView = [[CustomePwdView alloc] initWithFrame:CGRectMake(0, ViewHeight, Screen_Width, 200) withPwdType:3 andNum:4];
        _pwdView.passView.textF.keyboardType = UIKeyboardTypeNumberPad;
       
        [self.view addSubview:_pwdView];
    }
    return _pwdView;
}
@end
