//
//  FixCardRecordVC.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixCardRecordVC.h"
#import "MinePromptView.h"
#import "MineApi.h"
#import "CardRecordCell.h"
#import "FixCardDetailVC.h"
#import "MineServicesCardDetailVC.h"
@interface FixCardRecordVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) MinePromptView *topView;
@property (nonatomic, strong) UITableView  *listTableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) MineApi *api;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, strong) NSString *statues;
@property (nonatomic, assign) NSInteger type;
@end

@implementation FixCardRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"服务卡统计";
    [self setupUI];
    [self setupProp];
}

- (void)setupProp{
//    self.topView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = @"2";
    self.topView.orderStatues = 200;
}

- (void)setupUI{
    [self.view addSubview:self.topView];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
//    self.promptView.orderStatues = 200;
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.view);
    }];
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.statues forKey:@"use"];
    if (self.proId) {
         [dic setValue:self.proId forKey:@"ProId"];
         self.type = 1;
    }
    
    if ([userLevelGrade integerValue] > 3) {
       
    }else{
        self.type = 3;
    }
    
    
    [self.api getServiceCardListnWithparameters:dic andType:self.type withpageSize:self.size pageNum:self.num withCompletionHandler:^(NSError *error, MessageBody *result) {
        NSArray *datas = result.result;
        
        self.dataArray = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.dataArray withNum:self.num];
       
    }];
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"CardRecordCell";
    CardRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[CardRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArray[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    FixCardDetailVC *vc = [[FixCardDetailVC alloc] initWithNibName:@"FixCardDetailVC" bundle:nil];
//    [self.navigationController pushViewController:vc animated:true];
    MineServicesCardDetailVC *vc = [[MineServicesCardDetailVC alloc] initWithNibName:@"MineServicesCardDetailVC" bundle:nil];
    vc.dic = self.dataArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:true];
}

-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerNib:[UINib nibWithNibName:@"CardRecordCell" bundle:nil] forCellReuseIdentifier:@"CardRecordCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}
- (MinePromptView *)topView
{
    if (!_topView) {
        _topView = [MinePromptView new];
        WeakSelf(self)
        _topView.dataSource = @[@"已使用",@"未使用"];
        _topView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
                    self.statues = @"2";
                    break;
                case 1:
                    self.statues = @"1";
                    break;
                
                default:
                    break;
            }
             [weakself.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_topView];
    }
    return _topView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
