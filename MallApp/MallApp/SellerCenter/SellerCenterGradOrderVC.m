//
//  SellerCenterGradOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCenterGradOrderVC.h"
#import "MinePromptView.h"
#import "SellerCenterApi.h"
#import "GradOrderCell.h"
#import "GrapOrderHeadView.h"
#import "GrapOrderFootView.h"
#import "GrapOrderFootViewTypeTwo.h"
@interface SellerCenterGradOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic , strong) MinePromptView *promptView;
@property (nonatomic , strong) UITableView  *listTableView;
@property (nonatomic , strong) NSMutableArray *dataArray;
@property (nonatomic , strong) SellerCenterApi *api;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,strong) NSString *statues;
@end

@implementation SellerCenterGradOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"抢单中心";
    [self setupUI];
    [self setupProp];
//    [self.listTableView.mj_header beginRefreshing];
}


- (void)setupProp{
    self.promptView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = @"1";
}

- (void)setupUI{
    [self.view addSubview:self.promptView];
    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
//    self.promptView.orderStatues = 200;
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.promptView.mas_bottom).offset(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(self.view);
    }];
}


- (void)getData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    [self.api getGradOrderListDataWithparameters:self.statues andNum:self.num andsize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *array = dic[@"list"];
            [self configData:array withRefreshType:type];
            
        }else{
             [self.listTableView.mj_header endRefreshing];
        }
       
    }];
}


- (void)configData:(NSArray *)array withRefreshType:(RefreshType)type{
   
    if (type == RefreshTypeDown) {
        if (array.count <20) {
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
        self.dataArray = [NSMutableArray arrayWithArray:array];
    }else if (type == RefreshTypeUP){
        if (array.count == 0) {
            self.num --;
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            for (int i = 0; i<array.count; i++) {
                NSDictionary *dic = array[i];
                [self.dataArray addObject:dic];
            }
            //            [self.datas addObjectsFromArray:array];
            [self.listTableView.mj_footer endRefreshing];
        }
    }else if (type == RefreshTypeNormal){
         self.dataArray = [NSMutableArray arrayWithArray:array];
    }

    
    [self.listTableView reloadData];
    
}






- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"GradOrderCell";
    GradOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[GradOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArray[indexPath.section]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    GrapOrderHeadView *headView = [GrapOrderHeadView new];
    [headView setupData:self.dataArray[section]];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if ([self.statues intValue] == 1) {
        return 84;
    }else{
        return 55;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if ([self.statues intValue] == 1) {
        GrapOrderFootView *footView = [GrapOrderFootView new];
          footView.clickBlock = ^{
              [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确认接受此订单？" andHandler:^(NSInteger index) {
                  if (index == 1) {
                      NSDictionary *dic = self.dataArray[section];
                      [self.api grabOrderWithparameters:dic[@"cartId"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                          if (result.code == 200) {
                              [self getData:RefreshTypeNormal];
                          }
                      }];
                  }
              }];
          };
          return footView;
    }else{
        GrapOrderFootViewTypeTwo *footView = [GrapOrderFootViewTypeTwo new];
        [footView setupData:self.dataArray[section]];
        return footView;
    }
}



- (MinePromptView *)promptView
{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        WeakSelf(self)
        _promptView.dataSource = @[@"待抢单",@"已抢单"];
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
                    self.statues = @"1";
                    break;
                case 1:
                    self.statues = @"2";
                    break;
                case 2:

                    break;
                    
                case 3:

                    break;
                case 4:

                    break;
                default:
                    break;
            }
             [weakself.listTableView.mj_header beginRefreshing];
        };
//        [self.view addSubview:_promptView];
    }
    return _promptView;
}

-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
//        _listTableView.tableFooterView = [UIView new];
//         _listTableView.tableHeaderView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.estimatedSectionHeaderHeight = 0.0;
        _listTableView.showsVerticalScrollIndicator = false;
        _listTableView.estimatedSectionFooterHeight = 0;
        _listTableView.estimatedSectionHeaderHeight = 0;
        [_listTableView registerNib:[UINib nibWithNibName:@"GradOrderCell" bundle:nil] forCellReuseIdentifier:@"GradOrderCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeDown];
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}



- (SellerCenterApi *)api{
    if (!_api) {
        _api = [SellerCenterApi new];
    }
    return _api;
}

@end
