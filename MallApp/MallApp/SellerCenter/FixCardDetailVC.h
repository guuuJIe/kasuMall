//
//  FixCardDetailVC.h
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FixCardDetailVC : BaseViewController
@property (nonatomic, strong) NSDictionary *dic;
@end

NS_ASSUME_NONNULL_END
