//
//  SellerFixCardManageVC.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerFixCardManageVC.h"
#import "FixCardRecordVC.h"
#import "FixCardSearchVC.h"
@interface SellerFixCardManageVC ()
@property (weak, nonatomic) IBOutlet UIView *recordView;
@property (weak, nonatomic) IBOutlet UIView *searchView;

@end

@implementation SellerFixCardManageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"维修卡管理";
    [self.recordView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.searchView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
//            SellerFixCardManageVC *vc = [[SellerFixCardManageVC alloc] initWithNibName:@"SellerFixCardManageVC" bundle:nil];
//            [self.navigationController pushViewController:vc animated:true];
            FixCardRecordVC *vc = [FixCardRecordVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
           
            break;
        case 101:
        {
            FixCardSearchVC *vc = [[FixCardSearchVC alloc] initWithNibName:@"FixCardSearchVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
        }
            
            break;
        default:
            break;
    }
}

@end
