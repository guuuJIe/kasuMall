//
//  LogisticListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "LogisticListVC.h"
#import "TypeCell.h"
#import "SellerCenterApi.h"
@interface LogisticListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSource;
@property (nonatomic,strong) SellerCenterApi *centerApi;
@end

@implementation LogisticListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"物流公司";
    [self setupUI];
    [self getdata];
    
}

- (void)setupUI{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)getdata{
    [self.centerApi getKdListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataSource = [NSMutableArray arrayWithArray:result.result];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"TypeCell";
    TypeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[TypeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
//    TypeCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TypeCell class])];
//    cell.typeLbl.text = self.dataSource[indexPath.row];
    [cell setupData:self.dataSource[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.clickBlock) {
        self.clickBlock(self.dataSource[indexPath.row]);
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
   
        _tableView.separatorInset = UIEdgeInsetsMake(0, 5, 0, 5);
//        [_tableView registerClass:[TypeCell class] forCellReuseIdentifier:NSStringFromClass([TypeCell class])];
        [_tableView registerNib:[UINib nibWithNibName:@"TypeCell" bundle:nil] forCellReuseIdentifier:@"TypeCell"];
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIApplicationBackgroundFetchIntervalNever;
        }
    }
    return _tableView;
}

- (SellerCenterApi *)centerApi{
    if (!_centerApi) {
        _centerApi = [SellerCenterApi new];
    }
    
    return _centerApi;
}

@end
