//
//  GrapOrderFootView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GrapOrderFootView.h"
@interface GrapOrderFootView()
@property (nonatomic,strong) UIButton *grabButton;
@end

@implementation GrapOrderFootView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width-20, 74)];
    [self addSubview:bg];
    bg.backgroundColor = [UIColor whiteColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bg.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bg.bounds;
    maskLayer.path = maskPath.CGPath;
    bg.layer.mask = maskLayer;
    
   
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(lineHeihgt);
    }];
    [self.grabButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self);
        make.left.equalTo(@10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(40);
    }];
}

- (void)click{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

-(UIButton *)grabButton
{
    if(!_grabButton)
    {
        _grabButton=[UIButton  new];
        [_grabButton setTitle:@"抢单" forState:UIControlStateNormal];
        _grabButton.titleLabel.font = LabelFont14;
        [_grabButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _grabButton.backgroundColor = UIColor276;
        [_grabButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        _grabButton.layer.cornerRadius = 5;
        [self addSubview:_grabButton];
        
    }
    return _grabButton;
}




@end
