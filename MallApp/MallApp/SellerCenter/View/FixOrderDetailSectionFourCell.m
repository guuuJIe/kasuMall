//
//  FixOrderDetailSectionFourCell.m
//  MallApp
//
//  Created by Mac on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixOrderDetailSectionFourCell.h"
@interface FixOrderDetailSectionFourCell()
@property (weak, nonatomic) IBOutlet UILabel *orderNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *stastLbl;
@property (weak, nonatomic) IBOutlet UILabel *endPlaceLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@property (weak, nonatomic) IBOutlet UILabel *stateLbl;
@end

@implementation FixOrderDetailSectionFourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.priceLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"¥%@",dic[@"realPrice"]] andDefailtInfo:@""];
        self.orderNumLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"%@",dic[@"orderNumber"]] andDefailtInfo:@""];
        self.timeLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"%@",dic[@"orderTime"]] andDefailtInfo:@""];
        self.stastLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"%@",dic[@"locationString"]] andDefailtInfo:@""];
        self.endPlaceLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"locationToString"] andDefailtInfo:@""];
        if (self.endPlaceLbl.text.length == 0) {
            self.stateLbl.hidden = true;
        }
//        self.stateLbl.text
        
    }
}

@end
