//
//  FixOrderDetailSectionFourCell.h
//  MallApp
//
//  Created by Mac on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FixOrderDetailSectionFourCell : UITableViewCell
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
