//
//  GoodsOperationView.h
//  MallApp
//
//  Created by Mac on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsOperationView : UIView
@property (nonatomic , strong) NSMutableArray *btnModelArray;
@property (nonatomic,copy) void(^orderHandleBlock)(NSString *flag);
- (void)setOperationWithStatues:(NSInteger)statues andProType:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
