//
//  GrapOrderHeadView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GrapOrderHeadView.h"
@interface GrapOrderHeadView()
@property (nonatomic) UILabel *goodTitleLabel;
@end

@implementation GrapOrderHeadView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width-20, 40)];
    [self addSubview:bg];
    bg.backgroundColor = [UIColor whiteColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bg.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bg.bounds;
    maskLayer.path = maskPath.CGPath;
    bg.layer.mask = maskLayer;
    
    [self addSubview:self.goodTitleLabel];
    [self.goodTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@12);
        make.centerY.mas_equalTo(self);
    }];
    
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(lineHeihgt);
    }];
}


- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.goodTitleLabel.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"车型：%@",dic[@"carModel"]] andDefailtInfo:@""];
        
    }
}


-(UILabel *)goodTitleLabel
{
    if(!_goodTitleLabel){
        _goodTitleLabel = [UILabel new];
        _goodTitleLabel.textColor = UIColor333;
        _goodTitleLabel.font = LabelFont14;
        _goodTitleLabel.numberOfLines = 1;
        _goodTitleLabel.text = @"车型:奥迪A4L";
        [self addSubview:_goodTitleLabel];
    }
    return _goodTitleLabel;
}

@end
