//
//  GoodsOperationView.m
//  MallApp
//
//  Created by Mac on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoodsOperationView.h"

@implementation GoodsOperationView

- (instancetype)init{
    self = [super init];
    if (self) {

        self.btnModelArray = [NSMutableArray array];
        self.userInteractionEnabled = true;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setOperationWithStatues:(NSInteger)statues andProType:(NSDictionary *)dic{
    UIView *lastView;
    
    
    for (UIView *view in self.subviews) {
        if ([view isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    NSInteger proType = [dic[@"proType"] integerValue];
    
    if (statues == 2) {
        
        NSDictionary *dic = @{@"title":@"上架",@"flag":@"shangjia"};
        [self.btnModelArray addObject:dic];
        
        if (proType == 8) {
            NSDictionary *dic = @{@"title":@"服务卡统计",@"flag":@"services"};
            [self.btnModelArray addObject:dic];
        }
        
    }else if (statues == 1){
        NSDictionary *dic = @{@"title":@"下架",@"flag":@"xiajia"};
        [self.btnModelArray addObject:dic];
        
        if (proType == 8) {
            NSDictionary *dic = @{@"title":@"服务卡统计",@"flag":@"services"};
            [self.btnModelArray addObject:dic];
        }
    }
    
    for (int i = 0; i < self.btnModelArray.count; i++) {
        NSDictionary *dic = self.btnModelArray[i];
        
        NSString *title = dic[@"title"];
        
        
        UIButton *button = [UIButton new];

        if (![title isEqualToString:@"服务卡统计"]) {
            button.layer.borderColor = UIColorB6.CGColor;
            button.layer.borderWidth = 1;
            button.layer.cornerRadius = 2;
        }
        
        
        [button setTitle:dic[@"title"] forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont14;
        [button setTitleColor:UIColor333 forState:UIControlStateNormal];
        [button setTitle:dic[@"flag"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if ([title isEqualToString:@"服务卡统计"]) {
                make.size.mas_equalTo(CGSizeMake(80, 60/2));
            }else{
                make.size.mas_equalTo(CGSizeMake(60, 60/2));
            }
           
            make.top.equalTo(self).offset(12);
            if(i == 0){
                make.right.equalTo(self).offset(-12);
            }else{
                make.right.equalTo(lastView.mas_left).offset(-12);
            }
        }];
        lastView = button;
    }
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(0);
        make.right.equalTo(self).offset(0);
        make.height.mas_equalTo(10);
        make.bottom.equalTo(self);
    }];
}

- (NSMutableArray *)btnModelArray{
    if (!_btnModelArray) {
        _btnModelArray = [NSMutableArray new];
    }
    return _btnModelArray;
}
- (void)buttonClick:(UIButton *)sender{
    if (self.orderHandleBlock) {
        self.orderHandleBlock([sender titleForState:UIControlStateSelected]);
    }
}
@end
