//
//  GoodsStautesOperationView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerFixOrderOpView.h"

@implementation SellerFixOrderOpView
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    self.userInteractionEnabled = YES;
}


- (NSDictionary *)createPurchasingButton:(NSDictionary *)data
{
    NSDictionary *dic;
    
    
    NSInteger type = [data[@"status"] integerValue];
    if (type == 3) {
        dic = @{
            @"title":@"设置价格",
            @"flag":@"OrderPriceEdit"
        };
        
        [self.btnModelArray addObject:dic];
        
    }
    
    if (type == 4) {
        dic = @{
            @"title":@"修改价格",
            @"flag":@"OrderPriceChange"
        };
        [self.btnModelArray addObject:dic];
        
    }
    
    if (type == 5) {
        
        
    }
    
    
    return dic;
}


- (void)setOrderOption:(NSDictionary *)dic2
{
    
    _btnModelArray = [NSMutableArray new];
    NSDictionary *dic;
    dic = [self createPurchasingButton:dic2];
    UIView *lastView;
    
    
    for (UIView *view in self.subviews) {
        if ([view isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < self.btnModelArray.count; i++) {
        NSDictionary *dic = self.btnModelArray[i];
        UIButton *button = [UIButton new];
        button.backgroundColor = [UIColor whiteColor];
        if (i == 0) {
            
            [button setTitleColor:APPColor forState:UIControlStateNormal];
            button.layer.borderColor = APPColor.CGColor;
            
        }else{
            [button setTitleColor:UIColorB6 forState:UIControlStateNormal];
            button.layer.borderColor = UIColorB6.CGColor;
        }
        [button setTitle:dic[@"title"] forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont14;
        button.layer.borderWidth = 1;
        button.layer.cornerRadius = 15;
        [button setTitle:dic[@"flag"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(160/2, 60/2));
            make.centerY.equalTo(self);
            if(i == 0){
                make.right.equalTo(self).offset(-12);
            }else{
                make.right.equalTo(lastView.mas_left).offset(-15);
            }
        }];
        lastView = button;
    }
}

-(void)buttonClick:(UIButton *)button{
    self.orderHandleBlock([button titleForState:UIControlStateSelected]);
}

@end
