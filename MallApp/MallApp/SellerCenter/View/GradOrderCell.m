//
//  GradOrderCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GradOrderCell.h"
@interface GradOrderCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *subNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *quesLbl;

@end
@implementation GradOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"locationString"] andDefailtInfo:@""];
        self.subNameLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"%@ %@",dic[@"carContact"],dic[@"carPhone"]] andDefailtInfo:@""];
        self.quesLbl.text = [NSString stringWithFormat:@"问题描述：%@",dic[@"remark"]];
    }
}
@end
