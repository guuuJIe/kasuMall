//
//  MyOrderListFootView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#define TableViewFooterBottomHeight 40

#import "SellerFixOrderFootView.h"
#import "SellerFixOrderOpView.h"

@interface SellerFixOrderFootView()
@property (nonatomic , strong) SellerFixOrderOpView *optionView;
@property (nonatomic , strong) UILabel *priceLabel;
@property (nonatomic , strong) UILabel *numLabel;
@end

@implementation SellerFixOrderFootView

-(CGFloat)cellHeight{
    CGFloat height = 45;
    if(self.optionView.btnModelArray.count>0){
        height = height + TableViewFooterBottomHeight;
    }
    return height;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setupLayout];
        
    }
    return self;
}

- (void)setupLayout
{
   
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
         make.top.equalTo(self.contentView).offset(13);
        make.right.equalTo(self.contentView).offset(-12);
    }];
    
    
    [self.contentView addSubview:self.optionView];
    [self.optionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(TableViewFooterBottomHeight);
        make.top.equalTo(self.priceLabel.mas_bottom).offset(0*AdapterScal);
    }];
    
    
    
}

//- (void)setupData:(NSDictionary *)dic withGoodsNum:(NSInteger)num{
//    if (!dic) {
//        return;
//    }
//    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
//    self.numLabel.text = [NSString stringWithFormat:@"共%ld件",(long)num];
//    [self.optionView setOrderOption:dic];
//    if (self.optionView.btnModelArray.count > 0) {
//        self.optionView.hidden = false;
//    }else{
//        self.optionView.hidden = true;
//    }
//}
- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
    self.numLabel.text = @"";
    [self.optionView setOrderOption:dic];
    if (self.optionView.btnModelArray.count > 0) {
        self.optionView.hidden = false;
    }else{
        self.optionView.hidden = true;
    }
}

- (UILabel *)priceLabel
{
    if (!_priceLabel) {
        _priceLabel = [UILabel new];
        _priceLabel.textColor = UIColorFF3E;
        _priceLabel.font = LabelFont14;
        _priceLabel.text = @"¥606.00";
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}

- (SellerFixOrderOpView *)optionView
{
    if (!_optionView) {
        _optionView = [SellerFixOrderOpView new];
//        _optionView.backgroundColor = APPColor;
        _optionView.userInteractionEnabled = YES;
        WeakSelf(self);
        _optionView.orderHandleBlock = ^(NSString *flag) {
            StrongSelf(self);
            if (self.clickBlock) {
                self.clickBlock(flag);
            }
        };
        [self.contentView addSubview:_optionView];
    }
    return _optionView;
}

@end
