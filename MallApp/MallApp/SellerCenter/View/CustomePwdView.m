//
//  CustomePwdView.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CustomePwdView.h"

@interface CustomePwdView()

@end
@implementation CustomePwdView

- (instancetype)initWithFrame:(CGRect)frame withPwdType:(NSInteger)type andNum:(NSInteger)num{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
     
        [self setupUIWithPwdType:type andNum:num];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
     
        [self setupUI];
    }
    return self;
}


- (void)setupUI{
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(16, 19, 22, 22)];
    [btn setImage:[UIImage imageNamed:@"关闭"] forState:0];
    [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请输入密码";
    label.textColor = UIColor333;
    label.font = LabelFont16;
    label.numberOfLines = 0;
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(14);
    }];
    
    UIView *line = [UIView new];
    [self addSubview:line];
    line.backgroundColor = UIColorEF;
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.mas_equalTo(btn.mas_bottom).offset(16);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    self.passView = [[YN_PassWordView alloc] init];
    self.passView.frame = CGRectMake(25, CGRectGetMaxY(btn.frame)+32, Screen_Width-25*2, 50);
    self.passView.backgroundColor= [UIColor whiteColor];
    self.passView.showType = 1;//五种样式
   
    self.passView.textF.keyboardType = UIKeyboardTypeDefault;
    self.passView.num = 6;//框框个数
    self.passView.tintColor = UIColorE9;
    [self.passView show];
    [self addSubview:self.passView];
    WeakSelf(self)
    self.passView.textBlock = ^(NSString *str) {
        StrongSelf(self)
        if (self.finishBlock) {
            self.finishBlock(str);
        }
    };

}
- (void)setupUIWithPwdType:(NSInteger)type andNum:(NSInteger)num{
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(16, 19, 22, 22)];
    [btn setImage:[UIImage imageNamed:@"关闭"] forState:0];
    [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请输入确认码";
    label.textColor = UIColor333;
    label.font = LabelFont16;
    label.numberOfLines = 0;
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(14);
    }];
    
    UIView *line = [UIView new];
    [self addSubview:line];
    line.backgroundColor = UIColorEF;
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.mas_equalTo(btn.mas_bottom).offset(16);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    self.passView = [[YN_PassWordView alloc] init];
    self.passView.frame = CGRectMake(25, CGRectGetMaxY(btn.frame)+32, Screen_Width-25*2, 50);
    self.passView.backgroundColor= [UIColor whiteColor];
    self.passView.showType = type;//五种样式
    self.passView.textF.keyboardType = UIKeyboardTypeDefault;
    self.passView.num = num;//框框个数
    self.passView.tintColor = UIColorE9;
    [self.passView show];
    [self addSubview:self.passView];
    WeakSelf(self)
    self.passView.textBlock = ^(NSString *str) {
        StrongSelf(self)
        if (self.finishBlock) {
            self.finishBlock(str);
        }
    };
//       [self.passView.textF becomeFirstResponder];
}
- (void)click{
    [self.passView.textF resignFirstResponder];
}
@end
