//
//  SellerGoodsCell.m
//  MallApp
//
//  Created by Mac on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerGoodsCell.h"

@implementation SellerGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupdata:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
        [self.goodsImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = dic[@"title"];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
        self.sellsLbl.text = [NSString stringWithFormat:@"销量 %@",dic[@"sell"]];
        self.stockLbl.text = [NSString stringWithFormat:@"库存 %@",dic[@"stock"]];
    }
}

@end
