//
//  CustomePwdView.h
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YN_PassWordView.h"
NS_ASSUME_NONNULL_BEGIN

@interface CustomePwdView : UIView
@property(nonatomic,assign) NSInteger pwdType;
@property(nonatomic,assign) NSInteger pwdCount;
@property(nonatomic,strong) YN_PassWordView *passView;
@property(nonatomic,copy) void(^finishBlock)(NSString *str);
- (instancetype)initWithFrame:(CGRect)frame withPwdType:(NSInteger)type andNum:(NSInteger)num;
@end

NS_ASSUME_NONNULL_END
