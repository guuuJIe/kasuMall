//
//  TypeCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
