//
//  FixOrderDetailInfoSectionOneCell.m
//  MallApp
//
//  Created by Mac on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixOrderDetailInfoSectionOneCell.h"
@interface FixOrderDetailInfoSectionOneCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;

@end
@implementation FixOrderDetailInfoSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = [NSString stringWithFormat:@"%@  %@",dic[@"carContact"],dic[@"carPhone"]];
        self.subTitleLbl.text = [NSString stringWithFormat:@"%@",dic[@"carModel"]];
    }
}
@end
