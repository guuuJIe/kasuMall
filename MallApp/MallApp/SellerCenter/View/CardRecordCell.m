//
//  CardRecordCell.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CardRecordCell.h"
@interface CardRecordCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@property (weak, nonatomic) IBOutlet UILabel *statesLbl;
@end
@implementation CardRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSInteger isused = [dic[@"isUse"] integerValue];
        if (isused == 0) {
            self.statesLbl.text = @"未使用";
            self.statesLbl.textColor = [UIColor greenColor];
        }else{
            self.statesLbl.text = @"已使用";
            self.statesLbl.textColor = UIColor999;
        }
        self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@"暂无标题"];
        self.subTitleLbl.text = dic[@"created"];
        self.priceLbl.text = [NSString stringWithFormat:@"%@元",dic[@"amount"]];
        
    }
}
@end
