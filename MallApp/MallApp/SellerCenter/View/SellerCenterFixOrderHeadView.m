//
//  MyOrderHeadView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCenterFixOrderHeadView.h"
@interface SellerCenterFixOrderHeadView()
@property (nonatomic , strong) UILabel *namelabel;
@property (nonatomic , strong) UIView *statuesView;
@property (nonatomic , strong) UILabel *titlelabel;
@property (nonatomic , strong) UIImageView *headImage;
@end
@implementation SellerCenterFixOrderHeadView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    UIView *bgView = [UIView new];
    [self.contentView addSubview:bgView];
    bgView.backgroundColor = UIColorF5F7;
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
    [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView).offset(5);
        make.left.equalTo(self.contentView).offset(13);
    }];
   
    
    [self.namelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.headImage);
        make.left.equalTo(self.headImage.mas_right).offset(6);
    }];
    

    
    
    
    [self.titlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.namelabel);
        make.right.mas_equalTo(-12);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    // 3 4 5
    NSInteger stautes = [dic[@"status"] integerValue];
    if (stautes == 3) {
        self.titlelabel.text = @"服务中";
    }else if (stautes == 4){
        self.titlelabel.text = @"待付款";
    }else if (stautes == 5){
        self.titlelabel.text = @"已完成";
    }
    self.namelabel.text = dic[@"orderNumber"];
}
-(UIImageView *)headImage
{
    if(!_headImage)
    {
        _headImage=[UIImageView  new];
        _headImage.image = [UIImage imageNamed:@"order"];
//        _headImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_headImage];
    }
    return _headImage;
}

- (UILabel *)namelabel
{
    if (!_namelabel) {
        _namelabel = [UILabel new];
        _namelabel.text = @"卡速自营";
        _namelabel.textColor = UIColor333;
        _namelabel.font = LabelFont14;
        [self.contentView addSubview:_namelabel];
    }
    return _namelabel;
}

- (UILabel *)titlelabel
{
    if (!_titlelabel) {
        _titlelabel = [UILabel new];
        _titlelabel.textColor = APPColor;
        _titlelabel.text = @"待发货";
        _titlelabel.font = LabelFont14;
        [self.contentView addSubview:_titlelabel];
    }
    return _titlelabel;
}




@end
