//
//  TypeCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "TypeCell.h"

@implementation TypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.typeLbl.text = dic[@"kdType"];
    }
}

@end
