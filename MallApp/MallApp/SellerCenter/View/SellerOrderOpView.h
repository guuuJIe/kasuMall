//
//  SellerOrderOpView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SellerOrderOpView : UIView
@property (nonatomic , strong) NSMutableArray *btnModelArray;
@property (nonatomic,copy) void(^orderHandleBlock)(NSString *flag);
- (void)setOrderOption:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
