//
//  SellerGoodsCell.h
//  MallApp
//
//  Created by Mac on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SellerGoodsCell : UITableViewCell
- (void)setupdata:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *sellsLbl;
@property (weak, nonatomic) IBOutlet UILabel *stockLbl;

@end

NS_ASSUME_NONNULL_END
