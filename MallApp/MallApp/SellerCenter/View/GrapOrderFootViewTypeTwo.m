//
//  GrapOrderFootViewTypeTwo.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GrapOrderFootViewTypeTwo.h"
@interface GrapOrderFootViewTypeTwo()
@property (nonatomic) UILabel *goodTitleLabel;
@end
@implementation GrapOrderFootViewTypeTwo

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width-20, 40)];
    [self addSubview:bg];
    bg.backgroundColor = [UIColor whiteColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bg.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bg.bounds;
    maskLayer.path = maskPath.CGPath;
    bg.layer.mask = maskLayer;
    
    [self addSubview:self.goodTitleLabel];
    [self.goodTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.centerY.mas_equalTo(bg);
    }];
    
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(lineHeihgt);
    }];
}


- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.goodTitleLabel.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"%@",dic[@"orderTime"]] andDefailtInfo:@""];
        
    }
}


-(UILabel *)goodTitleLabel
{
    if(!_goodTitleLabel){
        _goodTitleLabel = [UILabel new];
        _goodTitleLabel.textColor = UIColor999;
        _goodTitleLabel.font = LabelFont14;
        _goodTitleLabel.numberOfLines = 1;
        _goodTitleLabel.text = @"2019-01-01";
        [self addSubview:_goodTitleLabel];
    }
    return _goodTitleLabel;
}
@end
