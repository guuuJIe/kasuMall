//
//  FixCardDetailVC.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixCardDetailVC.h"
#import "MyServiceCarQRCodeVC.h"
#import "ServiceCardOperationView.h"
#import "ReturnServiceCardVC.h"
#import "MemberApi.h"
@interface FixCardDetailVC ()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *accountLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *belongsLbl;
@property (weak, nonatomic) IBOutlet UILabel *statesLbl;

@property (weak, nonatomic) IBOutlet UILabel *isUsedLbl;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet UIView *useTimeView;
@property (weak, nonatomic) IBOutlet UILabel *usedTimeLbl;
@property (weak, nonatomic) IBOutlet UIView *checkQRCodeView;
@property (nonatomic, strong) ServiceCardOperationView *operationView;
@property (nonatomic, strong) MemberApi *memberApi;
@end

@implementation FixCardDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"服务卡详情";
    [self setupUI];
    [self setupData];
    [self.checkQRCodeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkAct)]];
   
}

- (void)setupUI{
    [self.view addSubview:self.operationView];
    [self.operationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setupData{
    self.titleLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"title"] andDefailtInfo:@""];
    self.accountLbl.text = self.dic[@"account"];
//    if ([userLevelGrade integerValue] >= 3) {
//        self.moneLbl.text = [NSString stringWithFormat:@"%@",self.dic[@"amount"]];
//        self.statesLbl.text = @"金额";
//    }else{
        self.moneLbl.text = [NSString stringWithFormat:@"%@",self.dic[@"password"]];
        self.statesLbl.text = @"密码";
//    }
   
    self.timeLbl.text = self.dic[@"created"];
    self.shopNameLbl.text = self.dic[@"userShopName"];
    self.belongsLbl.text = self.dic[@"userShopUserName"];
    NSInteger isuesd = [self.dic[@"isUse"] integerValue];
    NSInteger iscomp = [self.dic[@"isComplete"] integerValue];
    if (isuesd == 0) {
        self.operationView.Type = 1;
        self.isUsedLbl.text = @"未使用";
        self.userView.hidden = true;
        self.useTimeView.hidden = true;
        self.isUsedLbl.textColor = [UIColor colorWithHexString:@"23CC0C"];
    }else{
        self.operationView.Type = iscomp;
        
        self.isUsedLbl.text = @"已使用";
        self.userView.hidden = false;
        self.useTimeView.hidden = false;
        self.userNameLbl.text = self.dic[@"userUseShopUserName"];
        self.usedTimeLbl.text = self.dic[@"useTime"];
        self.isUsedLbl.textColor = [UIColor colorWithHexString:@"FFA127"];
    }
}

- (void)checkAct{
    MyServiceCarQRCodeVC *vc = [MyServiceCarQRCodeVC new];
    vc.dic = self.dic;
    [self.navigationController pushViewController:vc animated:true];
}


- (ServiceCardOperationView *)operationView{
    if (!_operationView) {
        _operationView = [ServiceCardOperationView new];
        
        WeakSelf(self)
        _operationView.clickBlock = ^(NSString * _Nonnull prop) {

            if ([prop isEqualToString:@"apply"]) {
//                NSMutableDictionary *dic = [NSMutableDictionary new];
//                [dic setValue:weakself.dic[@"account"] forKey:@"Account"];
                [weakself.memberApi readyReturnCardWithparameters:weakself.dic[@"account"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        NSArray *array = result.result;
                        ReturnServiceCardVC *vc = [ReturnServiceCardVC new];
                        vc.dic = array.firstObject;
                        vc.serviceDic = weakself.dic;
                        [weakself.navigationController pushViewController:vc animated:true];
                    }
                    
                }];

            }else if ([prop isEqualToString:@"achieve"]){
                [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认完成" andHandler:^(NSInteger index) {
                    if (index == 1) {
                        [weakself.memberApi confirmServicesCardWithparameters:weakself.dic[@"account"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                            if (result.code == 200) {
                                [JMBManager showBriefAlert:@"操作成功"];
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    [weakself.navigationController popViewControllerAnimated:true];
                                });
                            }
                        }];
                    }
                }];
            }
            
            
            
        };
    }
    
    return _operationView;
}

- (MemberApi *)memberApi{
    if (!_memberApi) {
        _memberApi = [MemberApi new];
    }
    
    return _memberApi;
}

@end
