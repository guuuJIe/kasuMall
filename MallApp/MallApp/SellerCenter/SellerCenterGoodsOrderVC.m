//
//  MyGoodOrderListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerCenterGoodsOrderVC.h"
#import "OrdetItemCell.h"
#import "MyOrderHeadView.h"
#import "SellerCenterGoodFootView.h"
#import "OrderTopView.h"
#import "SellerOrderDetailVC.h"
#import "SellerCenterApi.h"
#import "SelectPayWayVC.h"
#import "SellerDelieverGoodsVC.h"
@interface SellerCenterGoodsOrderVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) NSMutableArray *datas;
@property (nonatomic) OrderTopView *orderView;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,assign) NSInteger statues;
@property (nonatomic,strong) SellerCenterApi *api;
@end

@implementation SellerCenterGoodsOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)setupUI{
    self.title = @"订单管理";
    self.view.backgroundColor =UIColorF5F7;
    [self.orderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    self.orderView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = 0;
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.orderView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    [self.api getGoodsOrderListWithparameters:@{} withPageNum:self.num andPageSize:self.size andStatues:self.statues withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *array = dic[@"list"];
            [self configData:array withRefreshType:type];
            
        }
    }];
}

- (void)configData:(NSArray *)array withRefreshType:(RefreshType)type{
    
    if (type == RefreshTypeDown) {
        if (array.count <20) {
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
        self.datas = [NSMutableArray arrayWithArray:array];
    }else if (type == RefreshTypeUP){
        if (array.count == 0) {
            self.num --;
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            for (int i = 0; i<array.count; i++) {
                NSDictionary *dic = array[i];
                [self.datas addObject:dic];
            }
            //            [self.datas addObjectsFromArray:array];
            [self.listTableView.mj_footer endRefreshing];
        }
    }else if (type == RefreshTypeNormal){
        self.datas = [NSMutableArray arrayWithArray:array];
    }
    
    
    [self.listTableView reloadData];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    OrderListModel *model = self.datas[section];
//    return model.goodsList.count;
    NSDictionary *dic = self.datas[section];
    NSArray *arr = dic[@"products"];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"OrdetItemCell";
    OrdetItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];//OrdetItemCell 写错了
    if (cell == nil) {
        cell = [[OrdetItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    NSDictionary *dic = self.datas[indexPath.section];
    NSArray *arr = dic[@"products"];
    [cell setupData:arr[indexPath.row]];
    cell.clickBlock = ^{
        SellerOrderDetailVC *VC = [SellerOrderDetailVC new];
//        VC.type = 1;
        VC.ids = [NSString stringWithFormat:@"%@",dic[@"id"]];
        [self.navigationController pushViewController:VC animated:YES];
    };
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MyOrderHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderHeadView"];
    //    headView.ordermodel = self.datas[section];
    [headView setupData:self.datas[section]];
    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    SellerCenterGoodFootView *footView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SellerCenterGoodFootView"];
    NSDictionary *dic = self.datas[section];
    NSArray *arr = dic[@"products"];
    [footView setupData:self.datas[section] withGoodsNum:arr.count];
    
    footView.clickBlock = ^(NSString * _Nonnull flag) {
        if ([flag isEqualToString:@"orderStart"]) {//去付款
            SellerDelieverGoodsVC *vc = [[SellerDelieverGoodsVC alloc] initWithNibName:@"SellerDelieverGoodsVC" bundle:nil];
            vc.orderNum = dic[@"orderNumber"];
            vc.clickBlock = ^{
                [self setupData:RefreshTypeNormal];
            };
            [self.navigationController pushViewController:vc animated:true];
        }else if ([flag isEqualToString:@"orderDelete"]){
   
        }else if ([flag isEqualToString:@"orderReceive"]){
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认收货？" andHandler:^(NSInteger index) {
                if (index == 1) {

                }
            }];
        }
    };
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    SellerCenterGoodFootView *footView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SellerCenterGoodFootView"];
    NSDictionary *dic = self.datas[section];
    NSArray *arr = dic[@"products"];
    [footView setupData:self.datas[section] withGoodsNum:arr.count];
   
    return [footView cellHeight];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerClass:[MyOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"MyOrderHeadView"];
        [_listTableView registerClass:[SellerCenterGoodFootView class] forHeaderFooterViewReuseIdentifier:@"SellerCenterGoodFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);

            [self setupData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)datas{
    if (!_datas) {
        
        _datas = [NSMutableArray array];

    }
    return _datas;
}

- (OrderTopView *)orderView
{
    if (!_orderView) {
        _orderView = [OrderTopView new];
        WeakSelf(self);
        _orderView.clickBlock = ^(NSInteger tag) {
            switch (tag - 200) {
                case 0:
                    weakself.statues = 0;
                    break;
                case 1:
                    weakself.statues = 2;
                    break;
                case 2:
                    weakself.statues = 3;
                    break;
                    
                case 3:
                    weakself.statues = 4;
                    break;
                case 4:
                    weakself.statues = 5;
                    break;
                default:
                    break;
            }
             [weakself.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_orderView];
    }
    return _orderView;
}

- (SellerCenterApi *)api{
    if (!_api) {
        _api =[SellerCenterApi new];
    }
    return _api;
}
@end
