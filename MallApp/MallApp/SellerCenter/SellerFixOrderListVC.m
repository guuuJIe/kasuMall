//
//  MineFixOrderVC.m
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerFixOrderListVC.h"
#import "MyFixOrderTopView.h"
#import "OrdetItemCell.h"
#import "MyOrderListFootView.h"
#import "SellerCenterFixOrderHeadView.h"
#import "MyFixOrderItemCell.h"
#import "SellerCenterApi.h"
#import "HelpDetailVC.h"
#import "SellerFixOrderFootView.h"
#import "FixOrderEditViewController.h"
#import "SellerFixOrderDetailVC.h"
@interface SellerFixOrderListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) MyFixOrderTopView *topView;
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) NSMutableArray *datas;
@property (nonatomic) SellerCenterApi *sellApi;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger statues;
@property (nonatomic,strong) NSString *size;
@end

@implementation SellerFixOrderListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"维修订单";
    [self initProp];
    [self setupUI];
    
   
//    [self.listTableView.mj_header beginRefreshing];
}

- (void)initProp{
    self.num = 1;
    self.size = @"20";
    self.statues = 3;
}
- (void)setupUI{
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    self.topView.orderStatues = 203;
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)getData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    [self.sellApi getSellerFixOrderListDataWithparameters:[NSString stringWithFormat:@"%ld",(long)self.statues] andNum:[NSString stringWithFormat:@"%ld",(long)self.num] andsize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        [self.listTableView.mj_header endRefreshing];
        if (result.code == 200) {
            NSArray *array = result.result;
            [self configData:array withRefreshType:type];
            
        }
    }];
}

- (void)configData:(NSArray *)array withRefreshType:(RefreshType)type{
   
    if (type == RefreshTypeDown) {
        if (array.count <20) {
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
        self.datas = [NSMutableArray arrayWithArray:array];
    }else if (type == RefreshTypeUP){
        if (array.count == 0) {
            self.num --;
            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            for (int i = 0; i<array.count; i++) {
                NSDictionary *dic = array[i];
                [self.datas addObject:dic];
            }
            //            [self.datas addObjectsFromArray:array];
            [self.listTableView.mj_footer endRefreshing];
        }
    }else if (type == RefreshTypeNormal){
         self.datas = [NSMutableArray arrayWithArray:array];
    }

    [self.listTableView reloadData];
    
}



#pragma mark ---UITableViewDelegate----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    OrderListModel *model = self.datas[section];
//    return model.goodsList.count;
//    NSArray *arr = self.datas[section];
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"MyFixOrderItemCell";
    MyFixOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[MyFixOrderItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datas[indexPath.section]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SellerCenterFixOrderHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SellerCenterFixOrderHeadView"];
    [headView setupData:self.datas[section]];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    SellerFixOrderFootView *footView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SellerFixOrderFootView"];
    [footView setupData:self.datas[section]];
    NSDictionary *dic = self.datas[section];
    footView.clickBlock = ^(NSString * _Nonnull flag) {
        if ([flag isEqualToString:@"OrderPriceChange"]) {
            FixOrderEditViewController *vc = [[FixOrderEditViewController alloc] initWithNibName:@"FixOrderEditViewController" bundle:nil];
            vc.backBlock = ^{
                [self getData:RefreshTypeNormal];
            };
            vc.dic = dic;
            vc.tag = 201;
            [self.navigationController pushViewController:vc animated:true];
        }else if ([flag isEqualToString:@"OrderPriceEdit"]){
            FixOrderEditViewController *vc = [[FixOrderEditViewController alloc] initWithNibName:@"FixOrderEditViewController" bundle:nil];
            vc.backBlock = ^{
                [self getData:RefreshTypeNormal];
            };
            vc.dic = dic;
            vc.tag = 200;
            [self.navigationController pushViewController:vc animated:true];
        }
    };
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    SellerFixOrderFootView *footView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"SellerFixOrderFootView"];
    [footView setupData:self.datas[section]];
    return [footView cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    HelpDetailVC *vc = [HelpDetailVC new];
//    NSDictionary *dic = self.datas[indexPath.section];
//    vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
//    [self.navigationController pushViewController:vc animated:true];
    SellerFixOrderDetailVC *vc = [SellerFixOrderDetailVC new];
    NSDictionary *dic = self.datas[indexPath.section];
    vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
    [self.navigationController pushViewController:vc animated:true];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
//        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MyFixOrderItemCell" bundle:nil] forCellReuseIdentifier:@"MyFixOrderItemCell"];
        [_listTableView registerClass:[SellerCenterFixOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"SellerCenterFixOrderHeadView"];
        [_listTableView registerClass:[SellerFixOrderFootView class] forHeaderFooterViewReuseIdentifier:@"SellerFixOrderFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];

      
       
    }
    return _datas;
}

- (SellerCenterApi *)sellApi{
    if (!_sellApi) {
        _sellApi = [SellerCenterApi new];
    }
    return _sellApi;
}

- (MyFixOrderTopView *)topView{
    if (!_topView) {
        _topView = [MyFixOrderTopView new];
        WeakSelf(self);
        _topView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self);
            self.statues = tag - 200;
            [self.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_topView];
    }
    
    return _topView;
}

@end
