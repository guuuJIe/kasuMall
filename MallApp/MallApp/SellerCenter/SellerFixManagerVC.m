//
//  SellerFixManagerVC.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerFixManagerVC.h"
#import "SellerFixCardManageVC.h"
@interface SellerFixManagerVC ()
@property (weak, nonatomic) IBOutlet UIView *cardManageView;

@end

@implementation SellerFixManagerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"维修管理";
    [self.cardManageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            SellerFixCardManageVC *vc = [[SellerFixCardManageVC alloc] initWithNibName:@"SellerFixCardManageVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
        }
           
            break;
            
        default:
            break;
    }
}
@end
