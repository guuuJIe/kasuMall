//
//  RefundOrderListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerOrderRefundVC.h"
#import "MinePromptView.h"
#import "OrdetItemCell.h"
#import "MyOrderHeadView.h"
#import "SellerCenterApi.h"
#import "MyGoodsOrderDetailVC.h"
@interface SellerOrderRefundVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) MinePromptView *promptView;
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,assign) NSInteger statues;
@property (nonatomic,strong) NSMutableArray *datas;
@property (nonatomic,strong) SellerCenterApi *api;
@end

@implementation SellerOrderRefundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initProp];
    [self setupUI];
}
- (void)initProp{
    self.promptView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = 31;
}

- (void)setupUI{
    self.title = @"退款售后";
    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
   
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.promptView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)backBtnClick{
    [self.navigationController popToRootViewControllerAnimated:true];
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    [self.api getSellerRefundOrderListDataWithparameters:[NSString stringWithFormat:@"%ld",(long)self.statues] andNum:[NSString stringWithFormat:@"%ld",(long)self.num] andsize:[NSString stringWithFormat:@"%ld",(long)self.size] withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
           
            NSArray *array = result.result;

            self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
            [self.listTableView reloadData];
        }
    }];
}



#pragma mark ---UITableViewDelegate---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    OrderListModel *model = self.datas[section];
//    return model.goodsList.count;
    NSDictionary *dic = self.datas[section];
    NSArray *arr = dic[@"products"];
    return arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"OrdetItemCell";
    OrdetItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];//OrdetItemCell写错了
    if (cell == nil) {
        cell = [[OrdetItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    NSDictionary *dic = self.datas[indexPath.section];
    NSArray *arr = dic[@"products"];

    [cell setupData:arr[indexPath.row]];
    cell.clickBlock = ^{
        MyGoodsOrderDetailVC *VC = [MyGoodsOrderDetailVC new];
        VC.type = 1;
        VC.ids = [NSString stringWithFormat:@"%@",dic[@"id"]];
        [self.navigationController pushViewController:VC animated:YES];
    };
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MyOrderHeadView *headView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderHeadView"];

    [headView setupData:self.datas[section]];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}



-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerClass:[MyOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"MyOrderHeadView"];
//        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
//            [self getData:RefreshTypeDown];
            [self setupData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];

       
    }
    return _datas;
}

- (SellerCenterApi *)api{
    if (!_api) {
        _api =[SellerCenterApi new];
    }
    return _api;
}

- (MinePromptView *)promptView{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        _promptView.dataSource = @[@"待处理",@"服务中",@"已完成"];
        WeakSelf(self)
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            if (tag == 200) {
                self.statues = 31;
            }else if (tag == 201){
                self.statues = 32;
            }else if (tag == 202){
                self.statues = 33;
            }
            [self.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_promptView];
    }
    return _promptView;
}

@end
