//
//  SellerSendInvoiceVC.m
//  MallApp
//
//  Created by Mac on 2020/2/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerSendInvoiceVC.h"
#import "PurchaseView.h"
#import "OrderApi.h"
#import "LogisticListVC.h"
@interface SellerSendInvoiceVC ()
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *shibieNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankTypeLbl;

@property (weak, nonatomic) IBOutlet UILabel *bankNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *adressLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *statuesLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *companyLbl;
@property (weak, nonatomic) IBOutlet UITextField *kdNumText;

@property (nonatomic, strong) PurchaseView *submitView;
@property (nonatomic, strong) OrderApi *api;

@end

@implementation SellerSendInvoiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"发票抬头";
    [self setupUI];
    [self setupData];
}

- (void)setupUI{
    [self.view addSubview:self.submitView];
    [self.submitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setupData{
    self.typeLbl.text = self.dic[@"type"];
    self.nameLbl.text = self.dic[@"title"];
    self.shibieNumLbl.text = self.dic[@"sh"];
    self.bankTypeLbl.text = self.dic[@"bank"];
    self.bankNumLbl.text = self.dic[@"bankNumber"];
    self.adressLbl.text = self.dic[@"address"];
    self.telLbl.text = self.dic[@"tel"];
    if (self.statues == 1) {
//        self.invoiceStatuesView.hidden = false;
        self.statuesLbl.text = @"补开发票处理中";
//        self.submitView.hidden = true;
    }else if (self.statues == 2){
        [self checkInvoiceInfo];
        self.submitView.hidden = true;
        self.statuesLbl.text = @"已处理";
    }
    self.timeLbl.text = [XJUtil getNowTimeStamp];
    [self.companyLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
}

- (void)click{
    LogisticListVC *vc = [LogisticListVC new];
    vc.clickBlock = ^(NSDictionary * _Nonnull dic) {
        self.companyLbl.text = dic[@"kdType"];
    };
    [self.navigationController pushViewController:vc animated:true];
}

- (void)checkInvoiceInfo{
    [self.api checkInvoiceReissuedInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            [self configData:datas.firstObject];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    self.companyLbl.text = dic[@"kd"];
    self.kdNumText.text = dic[@"kdbh"];
    self.kdNumText.userInteractionEnabled = false;
}


- (PurchaseView *)submitView{
    if (!_submitView) {
        _submitView = [PurchaseView new];
        [_submitView.purchaseButton setTitle:@"发货" forState:0];
        WeakSelf(self)
        _submitView.addAddressBlock = ^{
            StrongSelf(self)
            if (self.kdNumText.text.length == 0) {
                [JMBManager showBriefAlert:@"请填写物流单号"];
                return ;
            }
            
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确认信息是否填写完整？" andHandler:^(NSInteger index) {
                if (index == 1) {
                    NSDictionary *dic = @{@"ordernumber":self.orderNum,@"KD":self.companyLbl.text,@"KDBH":self.kdNumText.text};
                    [self.api sellerSendInvoiceWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            if (self.clickBlock) {
                                self.clickBlock();
                                [self.navigationController popViewControllerAnimated:true];
                            }
                            
                        }
                    }];
                }
            }];
        };
        
    }
    return _submitView;
}

- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}


@end
