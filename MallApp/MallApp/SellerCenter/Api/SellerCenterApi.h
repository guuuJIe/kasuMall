//
//  SellerCenterApi.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SellerCenterApi : NSObject
/**
商家抢单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getGradOrderListDataWithparameters:(id)parameters andNum:(NSInteger)num andsize:(NSInteger)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
卖家中心订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getGoodsOrderListWithparameters:(id)parameters withPageNum:(NSInteger)num andPageSize:(NSInteger)size andStatues:(NSInteger)statues withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
抢单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)grabOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
卖家产品管理-列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)sellProductListWithparameters:(id)parameters andNum:(NSInteger)num andsize:(NSInteger)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
上架产品
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)sellProductUpLoadWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
下架产品
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)sellProductDownWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
维修订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getSellerFixOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
退款订单列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getSellerRefundOrderListDataWithparameters:(id)parameters andNum:(NSString *)num andsize:(NSString *)size withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
上传图片
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)uploadPicWithparameters:(id)parameters VisitImagesArr:(NSArray *)imageArr withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
编辑价格
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)editPriceWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
/**
提交w完成的维修订单
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)submitFinsinFixOrderWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
获取快递列表
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getKdListWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
立即发货
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)orderCompltetSendWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;

/**
维修订单详情
@param parameters parameters description
@param completionHander completionHander description
*/
- (void)getsellerFixOrderDetailInfoWithparameters:(id)parameters withCompletionHandler:(MessageBodyNetworkCompletionHandler)completionHander;
@end

NS_ASSUME_NONNULL_END
