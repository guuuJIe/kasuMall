//
//  SellerFixOrderDetailVC.m
//  MallApp
//
//  Created by Mac on 2020/1/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerFixOrderDetailVC.h"
#import "FixOrderDetailInfoSectionOneCell.h"
#import "MyFixOrderItemCell.h"
#import "FixOrderDetailSectionFourCell.h"
#import "HelpDetailSectionThreeCell.h"
#import "SellerCenterApi.h"
#import "ShopApi.h"
@interface SellerFixOrderDetailVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *listTableView;
@property (nonatomic,strong)SellerCenterApi *api;
@property (nonatomic,strong)NSDictionary *dic;
@property (nonatomic,strong)ShopApi *shopApi;
@end

@implementation SellerFixOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单详情";
    [self setupUI];
    [self getData];
}

- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)getData{

    
    
    if (self.type == 1) {
        [self.shopApi getrepairOrderInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *arr = result.result;
                self.dic = arr.firstObject;
                //               [self.oprationView setOrderOptionWithDic:self.dataDic withType:self.opType];
                //               self.value = [self.dataDic[@"status"] integerValue];
                [self.listTableView reloadData];
            }
        }];
    }else{
        [self.api getsellerFixOrderDetailInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *array = result.result;
                self.dic = array.firstObject;
                [self.listTableView reloadData];
            }
        }];
    }
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.type == 1 ? 3 : 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"FixOrderDetailInfoSectionOneCell";
        FixOrderDetailInfoSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[FixOrderDetailInfoSectionOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dic];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"MyFixOrderItemCell";
        MyFixOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[MyFixOrderItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        cell.contentView.backgroundColor = [UIColor whiteColor];
        [cell setupData:self.dic];
        return cell;
    }else if (indexPath.section == 2){
            static NSString *Identifier = @"FixOrderDetailSectionFourCell";
        FixOrderDetailSectionFourCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[FixOrderDetailSectionFourCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        //        cell.contentView.backgroundColor = [UIColor whiteColor];
        [cell setupData:self.dic];
        return cell;
    }else if (indexPath.section == 3){
        static NSString *Identifier = @"HelpDetailSectionThreeCell";
        HelpDetailSectionThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[HelpDetailSectionThreeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupListData:self.dic];
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3) {
        return 180;
    }
    
    return UITableViewAutomaticDimension;
}




- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 40;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerClass:[HelpDetailSectionThreeCell class] forCellReuseIdentifier:@"HelpDetailSectionThreeCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_listTableView registerNib:[UINib nibWithNibName:@"FixOrderDetailInfoSectionOneCell" bundle:nil] forCellReuseIdentifier:@"FixOrderDetailInfoSectionOneCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MyFixOrderItemCell" bundle:nil] forCellReuseIdentifier:@"MyFixOrderItemCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"FixOrderDetailSectionFourCell" bundle:nil] forCellReuseIdentifier:@"FixOrderDetailSectionFourCell"];


        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (SellerCenterApi *)api{
    if (!_api) {
        _api = [SellerCenterApi new];
    }
    return _api;
}

- (ShopApi *)shopApi{
    if (!_shopApi) {
        _shopApi = [ShopApi new];
    }
    
    return _shopApi;
}

@end
