//
//  DelegateShowView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DelegateShowView : UIView
+(instancetype)initViewClass;

@property (nonatomic, copy) void (^actBlock)(void);
@property (nonatomic, copy) void (^readBlock)(BOOL isread);
@end

NS_ASSUME_NONNULL_END
