//
//  CalculateCarMaintenancePriceVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CalculateCarMaintenancePriceVC.h"
#import "CustomeTextField.h"
#import "ServicesDetailOfBottomView.h"
#import "MyAddressPopView.h"
#import "CarListVC.h"
#import "GroupApi.h"
#import "MineApi.h"
#import "CustomeTextField.h"
#import "OrderApi.h"
#import "AdressListVC.h"
#import "SelectPayWayVC.h"
#import "MainWebVC.h"
#import "WebVC.h"
#import "CusPickerView.h"
#import "ShuKeTimerPickerView.h"
#import "DelegateShowView.h"
#import "NSString+Extend.h"
@interface CalculateCarMaintenancePriceVC ()<UITextFieldDelegate,ShuKeTimerPickerViewDelegate>
@property (weak, nonatomic) IBOutlet CustomeTextField *mileAgeText;
@property (weak, nonatomic) IBOutlet CustomeTextField *carNumText;
@property (weak, nonatomic) IBOutlet CustomeTextField *carIdentifierText;
@property (weak, nonatomic) IBOutlet CustomeTextField *carTypeText;
@property (weak, nonatomic) IBOutlet CustomeTextField *useNatureText;
@property (weak, nonatomic) IBOutlet CustomeTextField *registerDateText;

@property (weak, nonatomic) IBOutlet UIView *selectSchemeView;
@property (weak, nonatomic) IBOutlet UILabel *schemeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;


@property (weak, nonatomic) IBOutlet UIView *selectUserNatureView;
@property (weak, nonatomic) IBOutlet UIView *selectRegisterDateView;
@property (weak, nonatomic) IBOutlet UIView *selectCarTypeView;

@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userNatureTextTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schemeWidthConstant;


@property (nonatomic, strong) ServicesDetailOfBottomView *bottomView;
@property (nonatomic, strong) MyAddressPopView *popView;
@property (nonatomic, strong) UIViewController *vc;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) NSDictionary *addressDic;
@property (nonatomic, strong) NSDictionary *countDic;
@property (nonatomic, strong) CusPickerView *pickerView;
@property (nonatomic, strong) OrderApi *orderApi;
@property (nonatomic, strong) NSString *EwCarAge;
@property (nonatomic, strong) ShuKeTimerPickerView *datepickerView;
@property (nonatomic, strong) DelegateShowView *delegateView;
@property (nonatomic, assign) BOOL isread;
@property (nonatomic, strong) NSString *carModel;
@end

@implementation CalculateCarMaintenancePriceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCarType:) name:@"getCarModel" object:nil];
    [self setupUI];
    
    [self filldata];
}

- (void)setupUI{
     self.title = @"选择方案";
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    [self.view addSubview:self.delegateView];
    [self.delegateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(27);
        make.bottom.mas_equalTo(self.bottomView.mas_top);
    }];
    
    [self.selectSchemeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAct)]];
    self.selectSchemeView.layer.borderWidth = 1;
    self.selectSchemeView.layer.borderColor = APPColor.CGColor;
    self.selectSchemeView.layer.cornerRadius = 4;
    
    self.mileAgeText.delegate = self;
    
    [self.selectUserNatureView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectAct:)]];
    [self.selectRegisterDateView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectAct:)]];
    [self.selectCarTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectAct:)]];
    
}

- (void)filldata{
    self.mileAgeText.text = [NSString stringWithFormat:@"%@公里",self.datas[@"Mileage"]];
    self.carNumText.text = self.datas[@"carNum"];
    self.carIdentifierText.text = self.datas[@"carIdentifie"];
    self.carTypeText.text = self.datas[@"carType"];
    self.useNatureText.text = self.datas[@"usernature"];
    self.registerDateText.text = self.datas[@"RegistrationDate"];
}

- (void)getCarType:(NSNotification *)noti{
    JLog(@"%@",noti.userInfo);
    NSDictionary *dic = noti.userInfo;
    NSString *carModel = dic[@"model"];
    NSArray * array = [carModel componentsSeparatedByString:@"-"];

    self.carModel = [array componentsJoinedByString:@","];
//    [self.datas setValue:[array componentsJoinedByString:@","] forKey:@"carModel"];
    [self.datas setValue:[self.carModel substringToIndex:self.carModel.length - 1] forKey:@"carModel"];
    self.carTypeText.text = dic[@"carname"];
    [self.datas setValue:dic[@"carname"] forKey:@"carType"];
}


- (void)makePrice{
   
    
    if ([self.bottomView.submitOrder.currentTitle isEqualToString:@"计算价格"]) {
        [self caluteSchemePrice];
    }else if ([self.bottomView.submitOrder.currentTitle isEqualToString:@"提交订单"]){
        [self submitOrderAct];
    }
    
    
}



- (void)clickAct{
    
    self.pickerView.dataArr = self.ewCarageArr;
    WeakSelf(self)
    self.pickerView.selectBlock = ^(id  _Nonnull result, id  _Nonnull result2, NSString * _Nonnull value) {
        StrongSelf(self);
        self.schemeLbl.text = result2;
        self.EwCarAge = value;
        [self caluteSchemePrice];
    };
    
    [self.pickerView popPickerView];
}

- (void)selectAct:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 1000:
        {
            
            MMPopupItemHandler mblock = ^(NSInteger index){
               
                JLog(@"%ld",(long)index);
                if (index == 0) {
                    self.useNatureText.text = @"营运";
                }else if (index == 1){
                    self.useNatureText.text = @"非营运";
                }
               
            };
            
            NSArray *items =
            @[MMItemMake(@"营运", MMItemTypeNormal, mblock),
              MMItemMake(@"非营运", MMItemTypeNormal, mblock),
             ];
            [JSCMMPopupViewTool showMMPopSheetWithTitle:@"" withItemsArray:items];
            
        }
            break;
        case 1001:
        {

            
            CarListVC *vc = [CarListVC new];
            vc.type = 1;
            vc.vcname = @"CalculateCarMaintenancePriceVC";
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        default:
            break;
    }
}

#pragma mark - ShuKeTimerPickerView Delegate
- (void)toobarDonBtnHaveClick:(ShuKeTimerPickerView *)pickView resultString:(NSString *)resultString atIndexof:(NSInteger)indexRow
{
    NSLog(@" ==== delegate => toobarDonBtnHaveClick 【%@】", resultString);
//    _timeStr = resultString;
//    [_timeBtn setTitle:_timeStr forState:UIControlStateNormal];
    NSArray * array = [resultString componentsSeparatedByString:@"-"];

    self.registerDateText.text = [array componentsJoinedByString:@""];
//    self.registerDateText.text = resultString;
}

- (void)toobarCancelBtn {
//    [_datepickerView remove];
}


#pragma mark - Logic
// 获取当前手机时间
- (NSString *)getNowDate:(NSString*)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSDate *datenow = [NSDate date];
    return [formatter stringFromDate:datenow];
}



- (IBAction)resetInfo:(id)sender {
    
    if (self.clickBlock) {
        self.clickBlock();
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (void)caluteSchemePrice{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:self.carIdentifierText.text forKey:@"vin"];
    [dic setValue:self.carNumText.text forKey:@"numberPlate"];
    [dic setValue:self.datas[@"ProductId"] forKey:@"productId"];
    [dic setValue:[self.mileAgeText.text substringToIndex:self.mileAgeText.text.length - 2] forKey:@"mileage"];
    [dic setValue:self.datas[@"pic"] forKey:@"pic"];
    [dic setValue:self.datas[@"engineNumber"] forKey:@"engineNumber"];
    [dic setValue:self.registerDateText.text forKey:@"registrationDate"];
    [dic setValue:self.datas[@"issueDate"] forKey:@"issueDate"];
    [dic setValue:self.useNatureText.text forKey:@"useNature"];
    [dic setValue:self.datas[@"address"] forKey:@"address"];
    [dic setValue:self.datas[@"owner"] forKey:@"owner"];
    [dic setValue:self.datas[@"vehicleType"] forKey:@"vehicleType"];
    [dic setValue:self.datas[@"carModel"] forKey:@"carModel"];
    [dic setValue:self.datas[@"model"] forKey:@"model"];
    //        [dic setValue:self.ewMileAge forKey:@"EwMileage"];
    [dic setValue:self.EwCarAge forKey:@"EwCarAge"];
//    [dic setValue:self.EwCarAge forKey:@"EwCarAge"];
    [JMBManager showLoading];
    [self.api makePriceForCarMaintainenceWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            NSDictionary *dic = datas.firstObject;
            NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",[dic[@"price"] floatValue]]];
            self.bottomView.priceLbl.text = [NSString stringWithFormat:@"¥%@",number];
            self.countDic = dic;
            [self changeUI];
        }
        JLog(@"%@",result.result);
        [JMBManager hideAlert];
    }];
}

- (void)changeUI{
    [self.bottomView.submitOrder setTitle:@"提交订单" forState:0];
    self.mileAgeText.userInteractionEnabled = false;
    self.carNumText.userInteractionEnabled = false;
    self.carIdentifierText.userInteractionEnabled = false;
    self.delegateView.hidden = false;
    self.btn1.hidden = true;
    self.btn2.hidden = true;
    self.textTrailing.constant = -25;
    self.userNatureTextTrailing.constant = -25;
//    [self.carTypeText addConstraint:[NSLayoutConstraint constraintWithItem:self.selectCarTypeView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:<#(nullable id)#> attribute:<#(NSLayoutAttribute)#> multiplier:1 constant:0]]
    self.selectCarTypeView.userInteractionEnabled = false;
    self.mileAgeText.userInteractionEnabled = false;
    self.carNumText.userInteractionEnabled = false;
    self.carIdentifierText.userInteractionEnabled = false;
    self.selectUserNatureView.userInteractionEnabled = false;
    self.schemeWidthConstant.constant =  [NSString sizeWithText:self.schemeLbl.text font:LabelFont14 maxSize:Max_Size].width+Default_Space;
    
}
- (void)submitOrderAct{
    
    
    if(!self.isread){
        [JMBManager showBriefAlert:@"您还未勾选协议"];
        
        return;
    }
    
    WeakSelf(self)
    [self.popView setData:^(NSDictionary * _Nonnull dic) {
        if (dic) {
            weakself.addressDic = dic;
        }
        
        [weakself.popView show];
    }];
    
    
    self.popView.clickBlock = ^{
        [weakself.popView hide];
        [weakself.navigationController pushViewController:[AdressListVC new] animated:true];
    };
    
    self.popView.purchaseBlock = ^{
        [JMBManager showLoading];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setValue:weakself.addressDic[@"id"] forKey:@"addressId"];
        [dic setValue:weakself.datas[@"ProductId"] forKey:@"groupProId"];
        [dic setValue:@0 forKey:@"cardId"];
        [dic setValue:@0 forKey:@"payment"];//支付方式
        [dic setValue:@0 forKey:@"invoiceId"];//发票ID
        [dic setValue:@0 forKey:@"stageNum"];//发票
        [dic setValue:@1 forKey:@"quautity"];//数量
        [dic setValue:@6 forKey:@"GroupType"];//保养
        NSInteger ids = [clerkid integerValue];
        if (ids) {
            [dic setValue:@(ids) forKey:@"tgcode"];//代理id
        }
        [weakself.orderApi createOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                [weakself.popView hide];
                SelectPayWayVC *vc = [SelectPayWayVC new];
                vc.from = @"3";
                vc.orderDataArr = result.result;
                vc.jumpVCName = @"CarMaintainenceOrderVC";
                vc.totalPrice = [NSString stringWithFormat:@"%@",weakself.countDic[@"price"]];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clerkid"];
                [weakself.navigationController pushViewController:vc animated:true];
                
            }
            JLog(@"%@",result.result);
            [JMBManager hideAlert];
            
        }];
    };
}

#pragma UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.mileAgeText.text = [NSString stringWithFormat:@"%@公里",textField.text];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.text = @"";
}


- (MyAddressPopView *)popView{
    if (!_popView) {
        _popView = [MyAddressPopView new];
        [_popView.purchaseBtn setTitle:@"确定" forState:0];
    }
    return _popView;
}

- (ServicesDetailOfBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [ServicesDetailOfBottomView initViewClass];
        WeakSelf(self)
        self.bottomView.ActBlock = ^(NSInteger index) {
        
            if (index == 1) {
                [weakself makePrice];
            }else if (index == 2){
                WebVC *vc = [WebVC new];
                vc.ids = [weakself.datas[@"ProductId"] integerValue];
                [weakself.navigationController pushViewController:vc animated:true];
               
            }

        };
    }
    return _bottomView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    return _mineApi;
}

- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    return _orderApi;
}

- (CusPickerView *)pickerView{
    if (!_pickerView) {
        _pickerView = [[CusPickerView alloc] initWithFrame:CGRectMake(0, Screen_Height, Screen_Width, Screen_Height)];
        _pickerView.btnTitleColor = UIColor333;
        _pickerView.baseViewColor = [UIColor whiteColor];

        [self.navigationController.view addSubview:_pickerView];
    }
    return _pickerView;
}

- (DelegateShowView *)delegateView{
    if (!_delegateView) {
        _delegateView = [DelegateShowView initViewClass];
        _delegateView.hidden = true;
        WeakSelf(self)
        _delegateView.actBlock = ^{
            StrongSelf(self)
            WebVC *vc = [WebVC new];
            vc.ids = [self.datas[@"ProductId"] integerValue];
            [self.navigationController pushViewController:vc animated:true];
            
        };
        
        _delegateView.readBlock = ^(BOOL isread) {
            StrongSelf(self)
            self.isread = isread;
        };
    }
    
    return _delegateView;
}
@end
