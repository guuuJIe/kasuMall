//
//  CalculateCarMaintenancePriceVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CalculateCarMaintenancePriceVC : BaseViewController
@property (nonatomic, assign) NSInteger ids;
@property (nonatomic, strong) NSDictionary *datas;
@property (nonatomic,copy) void(^clickBlock)(void);
@property (nonatomic, strong) NSArray *ewCarageArr;

@end

NS_ASSUME_NONNULL_END
