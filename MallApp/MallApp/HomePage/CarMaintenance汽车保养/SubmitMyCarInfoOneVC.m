//
//  SubmitMyCarInfoOneVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitMyCarInfoOneVC.h"
#import "CustomeTextField.h"
#import "CarListVC.h"
#import "SubmitCarInfoBottomView.h"
#import "CalculateCarMaintenancePriceVC.h"
#import "GroupApi.h"
@interface SubmitMyCarInfoOneVC ()
@property (weak, nonatomic) IBOutlet CustomeTextField *mileAgeText;
@property (weak, nonatomic) IBOutlet UIView *carTypeView;
@property (weak, nonatomic) IBOutlet CustomeTextField *carTypeText;
@property (nonatomic, strong) SubmitCarInfoBottomView *bottomView;
@property (nonatomic, strong) NSString *carModel;//卡速车型
@property (nonatomic, strong) GroupApi *api;
@end

@implementation SubmitMyCarInfoOneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"填写资料";
    [self.carTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectType)]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCarType:) name:@"getCarModel" object:nil];
    self.bottomView = [SubmitCarInfoBottomView initViewClass];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    WeakSelf(self)
    self.bottomView.ActBlock = ^{
        [weakself caluteAct];
    };
}


- (void)getCarType:(NSNotification *)noti{
    JLog(@"%@",noti.userInfo);
    NSDictionary *dic = noti.userInfo;
    NSString *carModel = dic[@"model"];
    NSArray * array = [carModel componentsSeparatedByString:@"-"];

    self.carModel = [array componentsJoinedByString:@","];
    self.carTypeText.text = dic[@"carname"];
}



- (void)selectType{
    CarListVC *vc = [CarListVC new];
    vc.type = 1;
    vc.vcname = @"SubmitMyCarInfoOneVC";
    [self.navigationController pushViewController:vc animated:true];
}

- (void)caluteAct{
    if (!self.carModel || !self.mileAgeText.text) {
        
        [JMBManager showBriefAlert:@"请填写完整信息"];
        
        return;
    }
    
    [self.datas setValue:self.carTypeText.text forKey:@"carType"];
//    [self.datas setValue:self.carModel forKey:@"carModel"];
    [self.datas setValue:[self.carModel substringToIndex:self.carModel.length - 1] forKey:@"carModel"];
    [self.datas setValue:self.mileAgeText.text forKey:@"Mileage"];
    

    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:self.datas[@"ProductId"] forKey:@"ProductId"];
    [dic setValue:self.mileAgeText.text forKey:@"Mileage"];
    
    [JMBManager showLoading];
    WeakSelf(self)
    [self.api caluteMaintainenceOfYearsWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            [weakself pushToServiceInfo:datas.firstObject];
        }
        [JMBManager hideAlert];
//        JLog(@"%@",result.result);
    }];
    
}


- (void)pushToServiceInfo:(NSDictionary *)dic{

    CalculateCarMaintenancePriceVC *vc = [[CalculateCarMaintenancePriceVC alloc] initWithNibName:@"CalculateCarMaintenancePriceVC" bundle:nil];
    vc.datas = self.datas;
    vc.ewCarageArr = dic[@"ewProject"];
    [self.navigationController pushViewController:vc animated:true];
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

- (NSMutableDictionary *)datas{
    if (!_datas) {
        _datas = [NSMutableDictionary new];
    }
    return _datas;
}

@end
