//
//  SubmitMyCarInfoVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"
#import "LBXScanView.h"
NS_ASSUME_NONNULL_BEGIN

@interface SubmitMyCarInfoVC : BaseViewController
@property (nonatomic, assign) NSInteger ids;
#pragma mark - 扫码界面效果及提示等
/**
 @brief  扫码区域视图,二维码一般都是框
 */
@property (nonatomic,strong) LBXScanView* qRScanView;

@end

NS_ASSUME_NONNULL_END
