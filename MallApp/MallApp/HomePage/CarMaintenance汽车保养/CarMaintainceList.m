//
//  CarMaintainceList.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarMaintainceList.h"
#import "ServicesOfYearSectionsCell.h"
#import "ServicesOfItemCell.h"
#import "CarMaintainceProInfoVC.h"
#import "GroupApi.h"
#import "SearchVC.h"
@interface CarMaintainceList ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tabelView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSMutableArray *datasArray;
@property (nonatomic, strong) UIView *titleView;
@end

@implementation CarMaintainceList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    
    [self setupData];
    
  
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置navbar
//    self.navigationItem.leftBarButtonItem = nil;
    //    [self initBar];
//    self.navigationItem.backBarButtonItem = nil;
    
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.titleView];
}

- (void)setupUI{
    self.title = @"一年保养";
    [self.tabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(8);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)setupData{
    WeakSelf(self)
    [self.api getCarMatainenceProdutListWithparameters:@"" withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.datasArray = [XJUtil dealData:datas withRefreshType:RefreshTypeDown withListView:self.tabelView AndCurDataArray:self.datasArray withNum:1];
            [self.tabelView reloadData];
        }
    }];
}

#pragma mark -----UITableViewDelegate、UITableViewDataSource----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if (section == 1) {
        return self.datasArray.count;
//    }
//    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == 0) {
//        static NSString *Identifier = @"ServicesOfYearSectionsCell";
//        ServicesOfYearSectionsCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
//        if (cell == nil) {
//            cell = [[ServicesOfYearSectionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
//        }
//        [cell setupData:@{@"banner":@"carMaintainenceBanner",@"titlePic":@"carMaintainenceTitle"}];
//        return cell;
//    }else if (indexPath.section == 1){
        static NSString *Identifier = @"ServicesOfItemCell";
        ServicesOfItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[ServicesOfItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.datasArray[indexPath.row]];
        return cell;
//    }
//
//
//    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == 1) {
        CarMaintainceProInfoVC *vc = [CarMaintainceProInfoVC new];
        NSDictionary *dic = self.datasArray[indexPath.row];
        vc.ids = [dic[@"productId"] integerValue];
        [self.navigationController pushViewController:vc animated:true];
//    }
}

-(UITableView *)tabelView
{
    if(_tabelView==nil)
    {
        _tabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tabelView setDelegate:self];
        [_tabelView setDataSource:self];
        [_tabelView setTableFooterView:[UIView new]];
        [_tabelView setTableHeaderView:[UIView new]];
        _tabelView.rowHeight = UITableViewAutomaticDimension;
        _tabelView.estimatedRowHeight = 44;
        _tabelView.showsVerticalScrollIndicator = false;
        [_tabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tabelView.sectionIndexColor = UIColor70;
        [_tabelView setSectionIndexBackgroundColor:[UIColor clearColor]];
        _tabelView.backgroundColor = [UIColor clearColor];
        [_tabelView registerNib:[UINib nibWithNibName:@"ServicesOfYearSectionsCell" bundle:nil] forCellReuseIdentifier:@"ServicesOfYearSectionsCell"];
//        [_tabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
        [_tabelView registerNib:[UINib nibWithNibName:@"ServicesOfItemCell" bundle:nil] forCellReuseIdentifier:@"ServicesOfItemCell"];
        WeakSelf(self);
        _tabelView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            [weakself setupData];
        }];
        [self.view addSubview:_tabelView];
    }
    return _tabelView;
}


- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

- (UIView *)titleView{
    if (!_titleView) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 30)];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [button setImage:[UIImage imageNamed:@"back"] forState:0];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
        [button addTarget:self action:@selector(clickPop) forControlEvents:UIControlEventTouchUpInside];
        [_titleView addSubview:button];

        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(button.frame), 0, _titleView.frame.size.width - 70, 30)];
        titleView.backgroundColor = [UIColor whiteColor];
        titleView.layer.cornerRadius = 15;
        [titleView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onClickSearchBtn)]];
        
        UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 20, 20)];
        selectIcon.image=[UIImage imageNamed:@"搜索"];
        
        UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(selectIcon.frame), 2, _titleView.frame.size.width-130-selectIcon.frame.size.width, _titleView.frame.size.height - 3)];
        textField.textColor=UIColor333;
        textField.placeholder=@"请输入套餐名称";
        
        if (@available(iOS 13.0, *)) {
            
            textField.attributedPlaceholder=[[NSAttributedString alloc]initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];

        }else{
            [textField setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
        }
        textField.font=LabelFont14;
        textField.returnKeyType = UIReturnKeySearch;
        textField.userInteractionEnabled = false;

        [titleView addSubview:selectIcon];
        [titleView addSubview:textField];
        [_titleView addSubview:titleView];
        
    }
    
    return _titleView;
}

- (void)clickPop{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)onClickSearchBtn{
    SearchVC *vc = [SearchVC new];
    [self.navigationController pushViewController:vc animated:true];
}

@end
