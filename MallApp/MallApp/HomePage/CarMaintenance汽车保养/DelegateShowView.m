//
//  DelegateShowView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "DelegateShowView.h"

@interface DelegateShowView()
@property (weak, nonatomic) IBOutlet UILabel *delegateLbl;

@end

@implementation DelegateShowView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.delegateLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
}


+(instancetype)initViewClass
{
    return [[[NSBundle mainBundle] loadNibNamed:@"DelegateShowView" owner:self options:nil] firstObject];
}
- (IBAction)readAct:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.readBlock) {
        self.readBlock(sender.selected);
    }
}

- (void)click{
    if (self.actBlock) {
        self.actBlock();
    }
}

@end
