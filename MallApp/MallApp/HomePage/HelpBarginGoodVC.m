//
//  HelpBarginGoodVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpBarginGoodVC.h"
#import "BargainGoodView.h"
#import "UIButton+Gradient.h"
#import "RuleView.h"
#import "GroupApi.h"
#import "CountDown.h"
#import "CutRecordCell.h"
#import "NoDataView.h"
#import "OrderApi.h"
#import "SelectPayWayVC.h"
#import "ShareView.h"
#import "PayMannagerUtil.h"
#import "MyAddressPopView.h"
#import "AdressListVC.h"
#import "ReturnCashVC.h"
#import "BargainVC.h"
#import "BargainSuccessPopView.h"
#import "PayMannagerUtil.h"
#import "BarginingGoodsDetailView.h"
@interface HelpBarginGoodVC ()
@property (nonatomic, strong) UIImageView *headImage;
//@property (nonatomic, strong) BargainGoodView *goodsView;
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, strong) UILabel *userNameLbl;
@property (nonatomic, strong) UILabel *countLbl;
@property (nonatomic, strong) CountDown *countDown;
@property (nonatomic, assign) int nowTime;
@property (nonatomic, strong) UIButton *purchaseBtn;
@property (nonatomic, strong) UIButton *cutBtn;
@property (nonatomic, assign) NSInteger num;

@property (nonatomic, strong) UITableView *listTable;

@property (strong, nonatomic) NSMutableArray *datasArray;
@property (strong, nonatomic) UILabel *emptyLbl;
@property (strong, nonatomic) OrderApi *orderApi;

@property (nonatomic, strong) NSDictionary *addressDic;
@property (nonatomic, strong) MyAddressPopView *popView;
@property (nonatomic, strong) ShareView *shareView;

@property (strong, nonatomic) BarginingGoodsDetailView *goodsView;
@end

@implementation HelpBarginGoodVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self getGoodInfo];
    
}

- (void)setupUI{
    
    [UIPasteboard generalPasteboard].string = @"";
    self.title = @"砍价返现";
    UIView *bg = [UIView new];
    bg.backgroundColor = [UIColor colorWithHexString:@"FF5F28"];
    [self.view addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    UIImageView *headImage = [[UIImageView alloc] init];
    headImage.image = [UIImage imageNamed:@"B_top"];
    [bg addSubview:headImage];
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.mas_equalTo(0);
        
    }];
    
    UIButton *ruleBtn = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - 54, 18, 54, 28)];
    [ruleBtn setTitle:@"活动规则" forState:0];
    [ruleBtn setTitleColor:[UIColor colorWithHexString:@"CB4026"] forState:0];
    [ruleBtn.titleLabel setFont:LabelFont11];
    [ruleBtn setBackgroundColor:[UIColor whiteColor]];
    ruleBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [ruleBtn addTarget:self action:@selector(clickAct) forControlEvents:UIControlEventTouchUpInside];
    [bg addSubview:ruleBtn];
    
    
    ruleBtn.layer.mask = [XJUtil cutCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft ForView:ruleBtn withSize:CGSizeMake(10, 10)];
    
    
//
//    UIView *whiteBg = [UIView new];
//    whiteBg.backgroundColor = [UIColor whiteColor];
//    [bg addSubview:whiteBg];
//    whiteBg.layer.cornerRadius = 8;
//    [whiteBg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(headImage.mas_bottom);
//        make.left.mas_equalTo(12);
//        make.right.mas_equalTo(-12);
//        make.height.mas_equalTo(330);
//    }];
//
//    self.headImage = [UIImageView new];
//    [whiteBg addSubview:self.headImage];
//    self.headImage.image = [UIImage imageNamed:@"defaultHead"];
//    [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(20);
//        make.left.mas_equalTo(12);
//        make.size.mas_equalTo(CGSizeMake(37, 37));
//    }];
    
//    [whiteBg addSubview:self.userNameLbl];
//    [self.userNameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.headImage);
//        make.left.mas_equalTo(self.headImage.mas_right).offset(10);
//    }];
//
//
//    UILabel *sub  = [UILabel new];
//    sub.text = @"“我发现一件好货，能免费拿哦~”";
//    sub.textColor = [UIColor colorWithHexString:@"141414"];
//    sub.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
//    [whiteBg addSubview:sub];
//    [sub mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.userNameLbl);
//        make.top.mas_equalTo(self.userNameLbl.mas_bottom).offset(5);
//    }];
//
//    self.goodsView = [BargainGoodView initViewClass];
//    [whiteBg addSubview:self.goodsView];//89223406
//    [self.goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(12);
//        make.right.mas_equalTo(-12);
//        make.top.mas_equalTo(self.headImage.mas_bottom).offset(12);
//        make.height.mas_equalTo(112);
//    }];
//
//    [whiteBg addSubview:self.priceLbl];
//    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.goodsView.mas_bottom).offset(18);
//        make.centerX.mas_equalTo(whiteBg);
//    }];
//
//
//
//    UIButton *four = [UIButton buttonWithType:UIButtonTypeCustom];
//    [four setTitle:@"我也要免费拿" forState:UIControlStateNormal];
//    [four setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [four setImage:[UIImage imageNamed:@"right"] forState:UIControlStateNormal];
//    four.layer.cornerRadius = 25;
//    four.layer.masksToBounds = true;
//    [four gradientButtonWithSize:CGSizeMake(138, 55) colorArray:@[(id)[UIColor colorWithHexString:@"97C1FF"],(id)[UIColor colorWithHexString:@"5497FB"]] percentageArray:@[@(0.18),@(1)] gradientType:GradientFromTopToBottom];
//    [whiteBg addSubview:four];
//    [four.titleLabel setFont:LabelFont14];
//    [four addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [four mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(@12);
//        make.size.mas_equalTo(CGSizeMake(Screen_Width/2 - 24 - 13, 55));
//        make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(12);
//    }];
//    four.tag = 200;
//    self.purchaseBtn = four;
//
//    UIButton *five = [UIButton buttonWithType:UIButtonTypeCustom];
//    [five setTitle:@"帮好友砍一刀" forState:UIControlStateNormal];
//    [five setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [five setImage:[UIImage imageNamed:@"right"] forState:UIControlStateNormal];
//    five.layer.cornerRadius = 25;
//    five.layer.masksToBounds = true;
//    [five setTitleColor:[UIColor colorWithHexString:@"F9472A"] forState:0];
//    [five.titleLabel setFont:LabelFont14];
//    [five gradientButtonWithSize:CGSizeMake(138, 55) colorArray:@[(id)[UIColor colorWithHexString:@"FFF000"],(id)[UIColor colorWithHexString:@"FFAC28"]] percentageArray:@[@(0.18),@(1)] gradientType:GradientFromTopToBottom];
//    [five addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//    five.tag = 201;
//    [whiteBg addSubview:five];
//    [five mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-12);
//        make.size.mas_equalTo(CGSizeMake(Screen_Width/2 - 24 - 13, 55));
//        make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(12);
//    }];
//    self.cutBtn = five;
//
//    [whiteBg addSubview:self.countLbl];
//    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(whiteBg);
//        make.top.mas_equalTo(five.mas_bottom).offset(12);
//    }];
    
    [self.view addSubview:self.goodsView];
    [self.goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(headImage.mas_bottom);
        make.right.mas_equalTo(-12);
    }];
    
    
}


- (void)btnClick:(NSInteger)index{
    if (index == 200) {
        BargainVC *vc = [BargainVC new];
        [self.navigationController pushViewController:vc animated:true];
    }else if (index == 201){
        WeakSelf(self)
        [self.api bargainCutWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
            StrongSelf(self)
            if (result.code == 200) {
                NSArray *array = result.result;
                BargainSuccessPopView *popView = [BargainSuccessPopView new];
                [popView setupData:array.firstObject];
                [popView show];
                WeakSelf(popView)
                popView.clickBlock = ^{
                    [weakpopView hideWithBlock:^(MMPopupView *view, BOOL isHide) {
                        BargainVC *vc = [BargainVC new];
                        [self.navigationController pushViewController:vc animated:true];
//                        ShareView *shareview = [ShareView new];
//                        [shareview show];
//                        WeakSelf(shareview)
//                        shareview.clickBlock = ^(NSInteger index) {
//                            [[PayMannagerUtil sharedManager] wxShareWithDic:self.dataDic successBlock:^(BOOL success) {
//                                [weakshareview hide];
//                            } failBlock:^(BOOL fail) {
//
//                            } InSecen:index];
//                        };
                    }];
                   
                };
               [self getGoodInfo];
            }
        }];
    }
}

- (void)getGoodInfo{
    WeakSelf(self)
    [self.api getBargainGoodInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dataDic = datas.firstObject;
            [self configData:self.dataDic];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    [self.goodsView setupData:dic withtype:1];
    self.userNameLbl.text = dic[@"orderUserName"];
    NSString *limittime = [NSString stringWithFormat:@"%ld",(long)[XJUtil timeSwitchTimestamp:dic[@"expireTime"]]];
    NSString *nowtime = [XJUtil getNowTimeStamp];
    self.nowTime = [limittime intValue] - [nowtime intValue];
    WeakSelf(self)
    [self.countDown countDownWithStratTimeStamp:self.nowTime  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        
        weakself.countLbl.text=[NSString stringWithFormat:@"还剩%li天%li时%li分后砍价过期",(long)day,(long)hour,(long)minute];
        
    }];
    self.priceLbl.text = [NSString stringWithFormat:@"商品价格%@元，已砍%@元",dic[@"price"],dic[@"bargainPrice"]];
}



- (void)clickAct{
    RuleView *ruleview = [RuleView new];
    [ruleview setupdata:self.dataDic];
    [ruleview show];

//    BargainSuccessPopView *popView = [BargainSuccessPopView new];
//    [popView setupData:@{@"amount":@"12"}];
//    [popView show];
}

- (void)getAdress{
    WeakSelf(self)
    [self.popView setData:^(NSDictionary * _Nonnull dic) {
        if (dic) {
            weakself.addressDic = dic;
        }
        
        [weakself.popView show];
    }];
    
    
    self.popView.clickBlock = ^{
        [weakself.popView hide];
        [weakself.navigationController pushViewController:[AdressListVC new] animated:true];
    };
    
    self.popView.purchaseBlock = ^{
        [JMBManager showLoading];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setValue:weakself.addressDic[@"id"] forKey:@"addressId"];
        [dic setValue:weakself.dataDic[@"productId"] forKey:@"groupProId"];
        [dic setValue:@0 forKey:@"cardId"];
        [dic setValue:@0 forKey:@"payment"];//支付方式
        [dic setValue:@0 forKey:@"invoiceId"];//发票ID
        [dic setValue:@0 forKey:@"stageNum"];//发票
        [dic setValue:@1 forKey:@"quautity"];//数量
        [dic setValue:@4 forKey:@"GroupType"];//团购类型
        NSInteger ids = [clerkid integerValue];
        if (ids) {
            [dic setValue:@(ids) forKey:@"tgcode"];//代理id
        }
        [weakself.orderApi createOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                [weakself.popView hide];
                NSArray *datas = result.result;
                ReturnCashVC *vc = [[ReturnCashVC alloc] init];
                
                vc.orderNum = [NSString stringWithFormat:@"%@",datas[1]];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clerkid"];
                [weakself.navigationController pushViewController:vc animated:true];
                
            }
            
            [JMBManager hideAlert];
            
        }];
    };
    
}

- (MyAddressPopView *)popView{
    if (!_popView) {
        _popView = [MyAddressPopView new];
    }
    return _popView;
}

- (UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.text = @"";
        _priceLbl.textColor = [UIColor colorWithHexString:@"5C330E"];
        _priceLbl.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    }
    return _priceLbl;
}

- (UILabel *)emptyLbl{
    if (!_emptyLbl) {
        _emptyLbl = [UILabel new];
        _emptyLbl.text = @"暂无砍价记录，立即喊朋友来砍价吧~";
        _emptyLbl.textColor = [UIColor colorWithHexString:@"7A4901"];
        _emptyLbl.font = [UIFont systemFontOfSize:14];
        _emptyLbl.hidden = true;
    }
    return _emptyLbl;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    
    return _api;
}

- (UILabel *)userNameLbl{
    if (!_userNameLbl) {
        _userNameLbl = [UILabel new];
        _userNameLbl.text = @"";
        _userNameLbl.textColor = UIColor66;
        _userNameLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    }
    return _userNameLbl;
}

- (UILabel *)countLbl{
    if (!_countLbl) {
        _countLbl = [UILabel new];
        _countLbl.text = @"";
        _countLbl.textColor = [UIColor colorWithHexString:@"58585A"];
        _countLbl.font = [UIFont systemFontOfSize:14];
    }
    return _countLbl;
}

- (CountDown *)countDown{
    if (!_countDown) {
        _countDown = [CountDown new];
    }
    
    return _countDown;
}

- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    return _orderApi;
}

- (BarginingGoodsDetailView *)goodsView{
    if (!_goodsView) {
        _goodsView = [BarginingGoodsDetailView new];
         WeakSelf(self)
        _goodsView.clickBlock = ^(NSInteger index) {
//            if (weakself.clickBlock) {
//                weakself.clickBlock(index);
//            }
            [weakself btnClick:index];
        };
    }
    return _goodsView;
}

@end
