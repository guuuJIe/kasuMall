//
//  UserApplySettleInVC.m
//  MallApp
//
//  Created by Mac on 2020/2/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UserApplySettleInVC.h"
#import "UIViewController+XHPhoto.h"
#import "MineApi.h"
#import "ImageZoomUtil.h"
@interface UserApplySettleInVC ()
@property (weak, nonatomic) IBOutlet UIView *bussinessPic;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *telText;
@property (weak, nonatomic) IBOutlet UITextField *companyNameText;
@property (weak, nonatomic) IBOutlet UIImageView *bussinssImageView;
@property (nonatomic, strong) MineApi *uploadApi;
@property (nonatomic, assign) BOOL isupload;
@property (nonatomic, strong) NSString *uploadPicStr;
@end

@implementation UserApplySettleInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(self.type == 1){
         self.title = @"申请批发";
    }else{
         self.title = @"申请开店";
    }
   
    [self.bussinessPic addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(uploadUserPic)]];
}

- (void)uploadUserPic{
    if (self.isupload) {
        [ImageZoomUtil ImageZoomWithImageView:self.bussinssImageView];
    }else{
        [self showCanEdit:true photo:^(UIImage *photo) {
            NSArray *arr =@[@{@"name":@"files",@"image":photo}];
            [JMBManager showLoading];
            [self.uploadApi uploadPicWithparameters:@"7" VisitImagesArr:arr withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    NSArray *datas = result.result;
                    self.bussinssImageView.image = photo;
                    self.isupload = true;
                    self.uploadPicStr = datas.firstObject;
                }
                
                [JMBManager hideAlert];
            }];
        }];
    }
    
}

- (IBAction)uploadInfo:(id)sender {
    NSDictionary *dic = @{
        @"type":@(self.type),
        @"name":[XJUtil insertStringWithNotNullObject:self.nameText.text andDefailtInfo:@""],
        @"phone":[XJUtil insertStringWithNotNullObject:self.telText.text andDefailtInfo:@""],
        @"CompanyName":[XJUtil insertStringWithNotNullObject:self.companyNameText.text andDefailtInfo:@""],
        @"pic1":[XJUtil insertStringWithNotNullObject:self.uploadPicStr andDefailtInfo:@""],
    };
    [self.uploadApi shopSettleInWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [JMBManager showBriefAlert:@"提交成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:true];
            });
        }
    }];

}



- (MineApi *)uploadApi{
    if (!_uploadApi) {
        _uploadApi = [MineApi new];
    }
    return _uploadApi;
}
@end
