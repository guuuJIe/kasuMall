//
//  BPHScanVC.m
//  519
//
//  Created by Macmini on 2018/12/13.
//  Copyright © 2018年 519. All rights reserved.
//
#define JSCGlobalTheme0f70044 [UIColor colorWithHexString:@"f70044"]
#import "ScanVC.h"
#import "LBXScanResult.h"
#import "LBXScanWrapper.h"
#import "LBXScanVideoZoomView.h"
#import "LBXAlertAction.h"
#import "LoginVC.h"
#import "FixCardSearchResultVC.h"
#import "SellerVerifyOrderVC.h"
#import "MemberApi.h"
#import "ServicesOfYearDetails.h"
#import "CarMaintainceProInfoVC.h"
#import "WeixiuTaoCanVC.h"
#import "GoodsWithSpecificVC.h"
#import "TuanGouGoodInfoVC.h"
#import "InviteFriendVC.h"
@interface ScanVC ()
@property (nonatomic, strong) UILabel *promptLabel;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) MemberApi *api;
@end

@implementation ScanVC
-(void)initViewController{
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 3;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
//    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_part_net"];;
    
    //码框周围4个角的颜色
    style.colorAngle = APPColor;
    //矩形框颜色
    style.colorRetangleLine = APPColor;
    
    self.style = style;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initViewController];
    [self setupNavigationBar];
    [self.view addSubview:self.promptLabel];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
//    self.view.backgroundColor = [UIColor blackColor];
    
    //设置扫码后需要扫码图像
    self.isNeedScanImage = YES;
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupNavigationBar];
}

- (void)dealloc{

}

- (void)setupNavigationBar {
    self.navigationItem.title = @"扫一扫";
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"相册" style:(UIBarButtonItemStyleDone) target:self action:@selector(openPhoto)];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    
    [self.navigationController setNavigationBarHidden:false animated:true];
    // 设置navbar
    self.navigationItem.backBarButtonItem = nil;
    //    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    //    self.navigationController.navigationBar.barTintColor=JSCGlobalThemeColor;
    
    [self.backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    self.backBtn.frame = CGRectMake(0, 0, 50, 50);
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:self.backBtn];
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"相册" style:(UIBarButtonItemStylePlain) target:self action:@selector(openPhoto)];
    // 字体颜色
    [rightBtn setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)showError:(NSString*)str
{
    
    [LBXAlertAction showAlertWithTitle:@"提示" msg:str buttonsStatement:@[@"知道了"] chooseBlock:nil];
    
}


- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    
    if (array.count < 1)
    {
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
    //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
    for (LBXScanResult *result in array) {
        NSLog(@"scanResult:%@",result.strScanned);
    }
    
    LBXScanResult *scanResult = array[0];
    
    NSString*strResult = scanResult.strScanned;
    
    self.scanImage = scanResult.imgScanned;
    
    if (!strResult) {
        
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
    //震动提醒
    // [LBXScanWrapper systemVibrate];
    //声音提醒
    //[LBXScanWrapper systemSound];
    
    [self showNextVCWithScanResult:scanResult];
    
}

- (void)showNextVCWithScanResult:(LBXScanResult*)strResult{
    NSString *scanRes = strResult.strScanned;
    JLog(@"scanRes---%@",scanRes);
//
    [self reStartDevice];
    if ([scanRes hasPrefix:@"https://wap.ocsawz.com/login"]) {
       
        NSArray * arr = [scanRes componentsSeparatedByString:@"?"];
        NSArray * urlArr = [scanRes componentsSeparatedByString:@"="];
        if (urlArr.count != 0) {
            if ([urlArr.firstObject hasPrefix:@"https://wap.ocsawz.com/login"]) {
                
//                LoginVC *vc = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//                vc.scanId = [NSString stringWithFormat:@"%@",urlArr[1]];
//                BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//                vc2.modalPresentationStyle = UIModalPresentationFullScreen;
//                [self presentViewController:vc2 animated:true completion:nil];
                
//                AuthoritaionLoginVC *vc = [[AuthoritaionLoginVC alloc] initWithNibName:@"AuthoritaionLoginVC" bundle:nil];
//                vc.scanId = [NSString stringWithFormat:@"%@",urlArr[1]];
//                [self.navigationController pushViewController:vc animated:true];
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",urlArr[1]] forKey:@"Bid"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                if (!accessToken) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                    
                }else{
                    [JMBManager showBriefAlert:@"您已登录"];
                    [self.navigationController popViewControllerAnimated:true];
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                         self.tabBarController.selectedIndex = 2;
//                    });
                   
                }
                
            }
            
        }
        NSArray *lastArr = [arr.lastObject componentsSeparatedByString:@"&"];
        NSLog(@"%@  %@ %@",arr,urlArr,lastArr);
    }else if([scanRes hasPrefix:@"P"]){
        WeakSelf(self)
//        [self stopDevice];
//        [self.api searchFixCardWithparameters:scanRes withCompletionHandler:^(NSError *error, MessageBody *result) {
//            if (result.code == 200) {
//
//                NSArray *datas = result.result;
//                FixCardSearchResultVC *vc = [[FixCardSearchResultVC alloc] initWithNibName:@"FixCardSearchResultVC" bundle:nil];
//                vc.dic = datas.firstObject;
//                [weakself.navigationController pushViewController:vc animated:true];
//            }
//            [self reStartDevice];
//        }];
        FixCardSearchResultVC *vc = [[FixCardSearchResultVC alloc] initWithNibName:@"FixCardSearchResultVC" bundle:nil];
        vc.accountText = scanRes;
        [self.navigationController pushViewController:vc animated:true];
    }else if ([scanRes hasPrefix:@"G"]){
//        WeakSelf(self)
        [self stopDevice];
        [self.api sellerEwEmOrderCheckReadyWithparameters:scanRes withCompletionHandler:^(NSError *error, MessageBody *result) {
             
            if (result.code == 200) {
            
                NSArray *datas = result.result;

                SellerVerifyOrderVC *vc = [SellerVerifyOrderVC new];
                vc.orderNum = [scanRes substringFromIndex:4];
                vc.dic = datas.firstObject;
                [self.navigationController pushViewController:vc animated:true];
            }
           [self reStartDevice];
        }];
    }else if ([scanRes hasPrefix:@"https://wap.ocsawz.com/qrcode"]){
        NSArray * resArr = [scanRes componentsSeparatedByString:@"/"];
        JLog(@"%@",resArr);
        [self stopDevice];
        
        [self.api getQRCodeDataWithparameters:resArr.lastObject withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *datas = result.result;
                NSDictionary *dic = datas.firstObject;
                NSInteger isPassed = [dic[@"passed"] integerValue];
                
                if (isPassed == 1) {
                    NSInteger code = [dic[@"code"] integerValue];
                    if (code == 1002) {//1002推广项目 1001登录注册 1004首页活动
                        NSInteger clerkId = [dic[@"cherkId"] integerValue];
                        NSInteger type = [dic[@"type"] integerValue]; //5保养 4保修 6维修套餐
                        if (clerkId > 0) {
                            [[NSUserDefaults standardUserDefaults] setObject:@(clerkId) forKey:@"clerkid"];
                            [[NSUserDefaults standardUserDefaults] setObject:@(clerkId) forKey:@"Bid"];
                        }
                        
                        if (type == 4) {
                            ServicesOfYearDetails *vc = [ServicesOfYearDetails new];
                            vc.ids = [dic[@"number"] integerValue];
                            [self.navigationController pushViewController:vc animated:true];
                        }else if (type == 5){
                            CarMaintainceProInfoVC *vc = [CarMaintainceProInfoVC new];
                            vc.ids = [dic[@"number"] integerValue];
                            [self.navigationController pushViewController:vc animated:true];
                        }else if (type == 6){
                            GoodsWithSpecificVC *vc = [GoodsWithSpecificVC new];
                            vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"number"]];
                            [self.navigationController pushViewController:vc animated:true];
                        }else if (type == 1){
                            TuanGouGoodInfoVC *vc = [TuanGouGoodInfoVC new];
                            vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"number"]];
                            [self.navigationController pushViewController:vc animated:true];
                        }else if (type == 2){
                            TuanGouGoodInfoVC *vc = [TuanGouGoodInfoVC new];
                            vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"number"]];
                            [self.navigationController pushViewController:vc animated:true];
                        }
                    }else if (code == 1003){
                        NSInteger clerkId = [dic[@"cherkId"] integerValue];
                        NSInteger type = [dic[@"type"] integerValue]; //5保养 4保修 6维修套餐
                        if (clerkId > 0) {
                            [[NSUserDefaults standardUserDefaults] setObject:@(clerkId) forKey:@"clerkid"];
                            [[NSUserDefaults standardUserDefaults] setObject:@(clerkId) forKey:@"Bid"];
                        }
                        InviteFriendVC *vc = [InviteFriendVC new];
                        [self.navigationController pushViewController:vc animated:true];
                    }
                }else{
                    [JMBManager showBriefAlert:@"二维码已失效"];
                }
              
            }
            
            [self reStartDevice];
        }];
        
    }
}

- (void)popAlertMsgWithScanResult:(NSString*)strResult
{
    if (!strResult) {
        
        strResult = @"识别失败";
    }
    
    __weak __typeof(self) weakSelf = self;
    [LBXAlertAction showAlertWithTitle:@"扫码内容" msg:strResult buttonsStatement:@[@"知道了"] chooseBlock:^(NSInteger buttonIdx) {
        [weakSelf reStartDevice];
    }];
    
    
    
}
-(void)backBtnClick
{
//    if (self.navigationController.childViewControllers.count>1) {
        [self.navigationController popViewControllerAnimated:YES];
//    }else{
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
    
}


- (UILabel *)promptLabel {
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc] init];
        _promptLabel.backgroundColor = [UIColor clearColor];
        CGFloat promptLabelX = 0;
        CGFloat promptLabelY = 0.73 * self.view.frame.size.height;
        CGFloat promptLabelW = self.view.frame.size.width;
        CGFloat promptLabelH = 25;
        _promptLabel.frame = CGRectMake(promptLabelX, promptLabelY, promptLabelW, promptLabelH);
        _promptLabel.textAlignment = NSTextAlignmentCenter;
        _promptLabel.font = [UIFont boldSystemFontOfSize:13.0];
        _promptLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _promptLabel.text = @"将二维码/条码放入框内, 即可自动扫描";
    }
    return _promptLabel;
}

#pragma mark -底部功能项
//打开相册
- (void)openPhoto
{
    if ([LBXScanWrapper isGetPhotoPermission])
        [self openLocalPhoto];
    else
    {
        [self showError:@"      请到设置->隐私中开启本程序相册权限     "];
    }
}
-(UIButton *)backBtn
{
    if (_backBtn==nil) {
        _backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        _backBtn.contentMode = UIViewContentModeCenter;
        [_backBtn.titleLabel setHidden:YES];
        [_backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _backBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        _backBtn.frame=CGRectMake(-20, 0, 40, 40);

        
        _backBtn.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _backBtn;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (MemberApi *)api{
    if (!_api) {
        _api = [MemberApi new];
    }
    
    return _api;
}

@end
