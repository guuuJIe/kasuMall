//
//  MerchantSettledVC.m
//  MallApp
//
//  Created by Mac on 2020/2/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MerchantSettledVC.h"
#import "BussinessMangeApi.h"
#import "SettleInView.h"
#import "UserApplySettleInVC.h"
#import <WebKit/WebKit.h>
@interface MerchantSettledVC ()<WKUIDelegate,WKNavigationDelegate>
@property (nonatomic, strong) BussinessMangeApi *api;
@property (nonatomic, strong) SettleInView *settleView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) WKWebView *wkwebView;
@end

@implementation MerchantSettledVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"商家入驻";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupUI];
    [self setupData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setupUI{
    
    [self.wkwebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-BottomAreaHeight-60);
    }];
    
    [self.view addSubview:self.settleView];
    [self.settleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(BottomAreaHeight+60);
    }];
}

- (void)setupData{
    [self.api shopSettleInContentWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            NSString *htmlStr = datas.firstObject;
            [self.wkwebView loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",htmlStr] baseURL:nil];
        }
    }];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
//    [webView evaluateJavaScript:@"document.body.offsetHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
//        // 计算webView高度
//        if (!self.webViewCellHeight && self.isFirstLoad) {
//            self.webViewCellHeight = [result doubleValue];
//            // 刷新tableView
//            self.isFirstLoad = !self.isFirstLoad;
//            [UIView performWithoutAnimation:^{
//                [self.listTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:3 inSection:0],nil] withRowAnimation:UITableViewRowAnimationNone];
//            }];
////            [self.listTableView reloadData];
//            [JMBManager hideAlert];
//        }
//        
//    }];
    
    
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    [JMBManager hideAlert];
}
- (BussinessMangeApi *)api{
    if (!_api) {
        _api = [BussinessMangeApi new];
    }
    return _api;
}

- (SettleInView *)settleView{
    if (!_settleView) {
        _settleView = [SettleInView new];
        WeakSelf(self)
        _settleView.itemClikcBlock = ^(NSInteger tag) {
            StrongSelf(self)
            UserApplySettleInVC *vc = [[UserApplySettleInVC alloc] initWithNibName:@"UserApplySettleInVC" bundle:nil];
            vc.type = tag;
            [self.navigationController pushViewController:vc animated:true];
        };
    }
    return _settleView;
}

- (WKWebView *)wkwebView {
    if (!_wkwebView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 偏好设置
        config.preferences = [[WKPreferences alloc] init];
        config.preferences.minimumFontSize = 10;
        config.preferences.javaScriptEnabled = YES;
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;


        WKUserContentController *userContentController = [[WKUserContentController alloc] init];
//        WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:cookie injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
//        [userContentController addUserScript:cookieScript];

//        [userContentController addScriptMessageHandler:self name:@"share"];
//        [userContentController addScriptMessageHandler:self name:@"login"];

        config.userContentController = userContentController;
        config.selectionGranularity = WKSelectionGranularityDynamic;
        config.allowsInlineMediaPlayback = YES;
//        config.mediaTypesRequiringUserActionForPlayback = false;
        
      
        _wkwebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        
        _wkwebView.navigationDelegate = self;//导航代理
        _wkwebView.UIDelegate = self;//UI代理
        _wkwebView.backgroundColor = [UIColor whiteColor];
        _wkwebView.allowsBackForwardNavigationGestures = YES;
//        _wkwebView.scrollView.scrollEnabled = false;
        _wkwebView.scrollView.showsVerticalScrollIndicator = false;
        [self.view addSubview:_wkwebView];
       
    }
    return _wkwebView;
}

@end
