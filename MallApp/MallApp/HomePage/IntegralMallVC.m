//
//  IntegralMallVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "IntegralMallVC.h"
#import "HomePageApi.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
//#import <AMapTrackKit/AMapTrackKit.h>
@interface IntegralMallVC ()
@property (nonatomic, strong) HomePageApi *homeApi;
@end

@implementation IntegralMallVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    data": {
//        "name": "Kasu_serviceID",
//        "sid": 132635
//    },
    
    [self getData];
}

- (void)getData{
   
}

- (HomePageApi *)homeApi{
    if (!_homeApi) {
        _homeApi = [HomePageApi new];
    }
    
    return _homeApi;
}
@end
