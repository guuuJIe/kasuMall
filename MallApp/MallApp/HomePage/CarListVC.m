//
//  CarListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarListVC.h"
#import "HomePageApi.h"
#import "CarItemDataCell.h"
#import "CarItemDataHeadView.h"
#import "CarSubListVC.h"
#import "HotCarTypeCell.h"
#import "HotCarTypeHeadView.h"
@interface CarListVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) HomePageApi *homeApi;
@property (nonatomic) NSMutableDictionary *dataDic;
@property (nonatomic) NSArray *groupTitles;
@property (nonatomic) UIView *view_section;
@property (nonatomic) NSArray *hotCarTypeArr;
@property (nonatomic) NSMutableArray *searchArr;
@property (nonatomic,assign) BOOL isSearch;
@end

@implementation CarListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self setupUI];
    
    [self setupData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
     self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)setupUI{
     self.title = @"请选择品牌";
    [self.view addSubview:self.TabelView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(1);
        make.left.bottom.mas_equalTo(self.view);
        make.right.mas_equalTo(0);
    }];

}

- (void)setupData{
//    dispatch_semaphore_t signal = dispatch_semaphore_create(1);
//    dispatch_queue_t quene = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    __block long x = 0;
//    //任务1
//    dispatch_async(quene, ^{
////        x = dispatch_semaphore_signal(signal);
//
//        x= dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER);
//        NSLog(@"1----%ld",x);
//        sleep(2);
//        x = dispatch_semaphore_signal(signal);
//        NSLog(@"2----%ld",x);
//
//    });
//    //任务2
//    dispatch_async(quene, ^{
//        dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER);
//        NSLog(@"run task 2");
//        sleep(2);
//        NSLog(@"任务全部执行完成 complete task 2");
//        dispatch_semaphore_signal(signal);
//
//    });
    
    dispatch_group_t  group = dispatch_group_create();
    
    dispatch_group_enter(group);
    [self.homeApi getAllCarListDataWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {

            NSArray *arr = result.resultDic[@"list"];
            [self configData:arr];

        }else{

        }
        dispatch_group_leave(group);
    }];
    dispatch_group_enter(group);
    [self.homeApi getHotCarTypeListDataWithparameters:@"8" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.hotCarTypeArr = result.result;
        }
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.TabelView reloadData];
    });
}

- (void)configData:(NSArray *)array{
    
    for (NSDictionary *dic in array) {
        NSString *string = [NSString stringWithFormat:@"%@",dic[@"value"]];
        if (self.dataDic[string]) {
            NSArray *data = self.dataDic[string];
            NSMutableArray *datas = [NSMutableArray arrayWithArray:data];
//            [datas addObject:dic[@"name"]];
            [datas addObject:@{@"name":dic[@"name"],@"value":dic[@"id"],@"img":dic[@"sPicUrl"]}];
            self.dataDic[string] = datas;
        }else{
//            self.dataDic[string] = @[dic[@"name"]];
            self.dataDic[string] = @[@{@"name":dic[@"name"],@"value":dic[@"id"],@"img":dic[@"sPicUrl"]}];
        }
        
    }
    self.groupTitles = [[self.dataDic allKeys] sortedArrayUsingSelector:@selector(compare:)];
//    for (NSString *keyStr in self.groupTitles) {
//        
//        [self.groupTitles addObject:[NSString stringWithFormat:@"%@  ",keyStr]];//每个字母后面添加空格
//        
//    }
    [self.TabelView reloadData];
    [self setsectionIndexTitles];
    
}



#pragma mark ---CustomesectionIndex--
- (void)setsectionIndexTitles{

    CGRect frame = self.TabelView.bounds;

    self.view_section = [[UIView alloc]initWithFrame:CGRectMake(Screen_Width-20, 50, 20, frame.size.height - 100)];

//    self.view_section.backgroundColor = [UIColor whiteColor];

//    self.view_section .alpha = 0.5;

    CGFloat height = self.view_section.frame.size.height/self.groupTitles.count;//button平均高度

    for (int i = 0; i<self.groupTitles.count; i++) {  //往自定义view上添加button

        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, i*height, 20, height/2)];

        [btn setTitle:self.groupTitles[i] forState:UIControlStateNormal];

        [btn setTitleColor:UIColor70 forState:UIControlStateNormal];

        btn.titleLabel.font = [UIFont systemFontOfSize:12];

        btn.tag = i;

        [btn addTarget:self action:@selector(click_btn:) forControlEvents:UIControlEventTouchUpInside];//添加点击事件

        [self.view_section addSubview:btn];

    }

    [self.view addSubview:self.view_section];

}

- (void)click_btn:(UIButton *)btn{//点击自定义索引button跳到指定section

    CGRect section_frame = [self.TabelView rectForSection:btn.tag+1];//得到第几个section的frame

    self.TabelView.contentOffset = CGPointMake(0, section_frame.origin.y);//修改tableview的偏移量到指定section

}

-(void)changedTextField:(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    if (theTextField.text.length == 0) {
        self.isSearch = NO;
        [self.TabelView reloadData];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
   [self.searchArr removeAllObjects];
    if (textField.text.length == 0) {
        self.isSearch = NO;
    }else{
        self.isSearch = YES;
        for (int i = 0;i<self.dataDic.count;i++) {
            NSString *titleIndex = self.groupTitles[i];
            NSArray *datas = self.dataDic[titleIndex];
            for (int j = 0; j<datas.count; j++) {
                NSString *str = datas[j][@"name"];
                NSRange  letters = [str rangeOfString:textField.text options:NSCaseInsensitiveSearch];
                if (letters.location != NSNotFound) {
                    [self.searchArr addObject:datas[j]];
                }
            }
        }
       
    }
     [self.TabelView reloadData];
    [self.view endEditing:true];
    return true;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
//    self.isSearch = NO;
//    textField.text = @"";
//    [self.TabelView reloadData];
    return YES;
}


#pragma mark ---UITableViewDelegate---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //搜索出来只显示一块
    if (self.isSearch) {
        return 1;
    }
    return self.dataDic.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.isSearch) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//        GYZCity *city =  [self.searchCities objectAtIndex:indexPath.row];
        NSDictionary *dic = self.searchArr[indexPath.row];
        [cell.textLabel setText:dic[@"name"]];
        return cell;
    }
    if (indexPath.section == 0) {

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
        return cell;
        
    }else{
        static NSString *Identifier = @"CarItemDataCell";
        CarItemDataCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[CarItemDataCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        NSString *titleIndex = self.groupTitles[indexPath.section - 1];
        NSArray *allData = self.dataDic[titleIndex];
        //    cell.nameLbl.text = allData[indexPath.row];
        [cell setupData:allData[indexPath.row]];
        //    NSLog(@"%@",cell.carTypeImage);
        return cell;
        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isSearch) {
        return 50;
    }
    if (indexPath.section == 0) {
        return 0;
    }else{
        return 50;
    }
}



- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearch) {
        return self.searchArr.count;
    }
    if (section == 0) {
        return 1;
    }else{
        NSString *titleIndex = self.groupTitles[section - 1];
        
        NSArray *allData = self.dataDic[titleIndex];
        return allData.count;
    }
}


//- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
//    return self.groupTitles;
//}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return 76+Screen_Width/4*2;
    }
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section != 0) {
        NSString *titleIndex = self.groupTitles[section - 1];
        CarItemDataHeadView *headView = [CarItemDataHeadView new];
        
        headView.titleLabel.text = titleIndex;
        return headView;
    }else{
        WeakSelf(self)
        HotCarTypeHeadView *headView = [[HotCarTypeHeadView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 76+Screen_Width/4*2)];
        headView.datasArr = self.hotCarTypeArr;
        headView.clickBlock = ^(NSDictionary * _Nonnull dic) {
            StrongSelf(self)
            CarSubListVC *vc = [CarSubListVC new];
            vc.type = self.type;
            vc.carname = [NSString stringWithFormat:@"%@",dic[@"name"]];
            if (self.type == 1) {
                
            }else{
               [[NSUserDefaults standardUserDefaults] setObject:dic[@"name"] forKey:@"mycarName"];
            }
            vc.carTypeIds = [NSString stringWithFormat:@"%@",dic[@"id"]];
            if (self.vcname) {
                       vc.vcname = self.vcname;
                   }
            [self.navigationController pushViewController:vc animated:true];
        };
       
//        [headView.textField addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
        headView.textField.delegate = self;
        headView.textField.inputAccessoryView = [UIView new];
        return headView;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isSearch) {
        CarSubListVC *vc = [CarSubListVC new];

        NSDictionary *dic = self.searchArr[indexPath.row];
        vc.type = self.type;
        vc.carname = [NSString stringWithFormat:@"%@",dic[@"name"]];
        if (self.clickBlock) {
            self.clickBlock([NSString stringWithFormat:@"%@",dic[@"name"]]);
            [self.navigationController popViewControllerAnimated:true];
            return;
        }
        
        if (self.type == 1) {
            
        }else{
            
            [[NSUserDefaults standardUserDefaults] setObject:dic[@"name"] forKey:@"mycarName"];
        }
               vc.carTypeIds = [NSString stringWithFormat:@"%@",dic[@"value"]];
        if (self.vcname) {
            vc.vcname = self.vcname;
        }
        [self.navigationController pushViewController:vc animated:true];
    }
    
    if (indexPath.section == 0) {
        
    }else{
        JLog(@"%ld",(long)indexPath.section);
        NSString *titleIndex = self.groupTitles[indexPath.section-1];
        NSArray *allData = self.dataDic[titleIndex];
        NSDictionary *dic = allData[indexPath.row];
        
        CarSubListVC *vc = [CarSubListVC new];
        vc.type = self.type;
        vc.carname = [NSString stringWithFormat:@"%@",dic[@"name"]];
        if (self.clickBlock) {
            self.clickBlock([NSString stringWithFormat:@"%@",dic[@"name"]]);
            [self.navigationController popViewControllerAnimated:true];
            return;
        }
        
        if (self.type == 1) {
            
        }else{
            
            [[NSUserDefaults standardUserDefaults] setObject:dic[@"name"] forKey:@"mycarName"];
        }
        if (self.vcname) {
                   vc.vcname = self.vcname;
               }
        vc.carTypeIds = [NSString stringWithFormat:@"%@",dic[@"value"]];
        [self.navigationController pushViewController:vc animated:true];
        
    }
    
   
    
}

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        [_TabelView setTableHeaderView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _TabelView.sectionIndexColor = UIColor70;
        [_TabelView setSectionIndexBackgroundColor:[UIColor clearColor]];
        [_TabelView registerNib:[UINib nibWithNibName:@"CarItemDataCell" bundle:nil] forCellReuseIdentifier:@"CarItemDataCell"];
        [_TabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
        [_TabelView registerNib:[UINib nibWithNibName:@"HotCarTypeCell" bundle:nil] forCellReuseIdentifier:@"HotCarTypeCell"];
        [_TabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (NSMutableDictionary *)dataDic{
    if (!_dataDic) {
        _dataDic = [NSMutableDictionary new];
    }
    
    return _dataDic;
}

- (HomePageApi *)homeApi{
    if (!_homeApi) {
        _homeApi = [HomePageApi new];
    }
    
    return _homeApi;
}

- (NSMutableArray *)searchArr{
    if (!_searchArr) {
        _searchArr = [NSMutableArray array];
    }
    return _searchArr;
}


@end
