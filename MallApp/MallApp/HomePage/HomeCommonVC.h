//
//  HomeCommonVC.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCommonVC : BaseViewController
@property (nonatomic,strong) NSString *type; //77商城配件 78 汽车精品
@end

NS_ASSUME_NONNULL_END
