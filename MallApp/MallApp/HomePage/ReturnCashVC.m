//
//  ReturnCashVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ReturnCashVC.h"
#import "BargainGoodView.h"

#import "RuleView.h"
#import "GroupApi.h"
#import "CountDown.h"
#import "CutRecordCell.h"
#import "NoDataView.h"
#import "OrderApi.h"
#import "SelectPayWayVC.h"
#import "ShareView.h"
#import "PayMannagerUtil.h"
#import "SharePopView.h"
#import "BarginingGoodsDetailView.h"
#import "BagainTopCell.h"
#import "BargainCenterCell.h"
#import "BarginRecordHeadView.h"
#import "FootView.h"
#import "MyGoodsOrderDetailVC.h"
@interface ReturnCashVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSDictionary *dataDic;


@property (nonatomic, assign) NSInteger num;

@property (nonatomic, strong) UITableView *listTable;

@property (strong, nonatomic) NSMutableArray *datasArray;
@property (strong, nonatomic) UILabel *emptyLbl;
@property (strong, nonatomic) OrderApi *orderApi;


@end

@implementation ReturnCashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"砍价返现";
    
    [self initProp];
    
    [self setupUI];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self getGoodInfo];
    
    [self getBargainRecordList:RefreshTypeDown];
    
}

- (void)initProp{
    self.num = 1;
}

- (void)getGoodInfo{
    WeakSelf(self)
    [self.api getBargainGoodInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dataDic = datas.firstObject;
            [self configData:self.dataDic];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
//    [self.goodsView setupData:dic];
    
    [self.listTable reloadData];
}

- (void)getBargainRecordList:(RefreshType)type{
    if (type == RefreshTypeUP) {
        self.num = self.num+1;
    }else if (type == RefreshTypeDown){
        self.num = 1;
    }
    WeakSelf(self)
    [self.api getBargainRecordListWithparameters:self.orderNum andNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:@"20" withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        NSArray *datas = result.result;
        self.datasArray = [XJUtil dealData:datas withRefreshType:type withListView:self.listTable AndCurDataArray:self.datasArray withNum:self.num];
        if (self.datasArray.count == 0) {
            self.emptyLbl.hidden = false;
        }else{
             self.emptyLbl.hidden = true;
            [self.listTable reloadData];
        }
       
    }];
}

- (void)setupUI{
//    UIView *bg = [UIView new];
//    bg.backgroundColor = [UIColor colorWithHexString:@"FF5F28"];
//    [self.view addSubview:bg];
//    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(self.view);
//    }];
    

    [self.view addSubview:self.listTable];
    [self.listTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
//    WeakSelf(self)
//    self.goodsView.clickBlock = ^(NSInteger index) {
//
//    };
    
}

- (void)btnClick:(UIButton *)button{
    if (button.tag == 200) {
        [self getAdress];
    }else if (button.tag == 201){

        
        SharePopView *popView = [SharePopView new];
        [popView show];
        popView.clickBlock = ^{
            [self openWechat];
        };
    }
}


-(void)openWechat{
    
    [UIPasteboard generalPasteboard].string = self.dataDic[@"shortURL"];
    NSURL * url = [NSURL URLWithString:@"weixin://"];
    BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:url];
    //先判断是否能打开该url
    if (canOpen)
    {   //打开微信
        [[UIApplication sharedApplication] openURL:url];
    }else {
//        [MBProgressHUD showMoreLine:@"您的设备未安装微信APP" view:nil];
        [JMBManager showBriefAlert:@"您的设备未安装微信APP"];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        static NSString *Identifier = @"BagainTopCell";
        BagainTopCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[BagainTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        
        cell.clickBlock = ^{
            RuleView *ruleview = [RuleView new];
            [ruleview setupdata:self.dataDic];
            [ruleview show];
        };
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"BargainCenterCell";
        BargainCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[BargainCenterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        WeakSelf(self)
        cell.clickBlock = ^(NSInteger index) {
            if (index == 200) {
                [weakself getAdress];
            }else if (index == 201){
                
                SharePopView *popView = [SharePopView new];
                [popView show];
                popView.clickBlock = ^{
                    [weakself openWechat];
                };
            }
        };
        
        [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.section == 2){
       static NSString *Identifier = @"CutRecordCell";
        CutRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[CutRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.datasArray[indexPath.row]];
        return cell;
    }
    

    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section != 2) {
        return 1;
    }
    
    return self.datasArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 2) {
        return 45.0f;
    }
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 2) {
        BarginRecordHeadView *headView = [[BarginRecordHeadView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 45)];
        return headView;
    }
    
    return [UIView new];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return self.datasArray.count > 0 ? 10.0f : 40.0f;
    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return self.datasArray.count > 0 ? [[FootView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)] : [[FootView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
    }
    return [UIView new];
}



- (void)getAdress{
    WeakSelf(self)
//    [self.orderApi getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
//        NSDictionary *dic = result.resultDic;
//        NSArray *arr = dic[@"list"];
        
    [weakself purchaseGoods:@{}];
        
//    }];
    
}

- (void)purchaseGoods:(NSDictionary *)addressDic{
//    NSMutableDictionary *dic = [NSMutableDictionary new];
//    [dic setValue:addressDic[@"id"] forKey:@"addressId"];
//    [dic setValue:self.dataDic[@"productId"] forKey:@"groupProId"];
//    [dic setValue:@0 forKey:@"cardId"];
//    [dic setValue:@0 forKey:@"payment"];//支付方式
//    [dic setValue:@0 forKey:@"invoiceId"];//发票ID
//    [dic setValue:@0 forKey:@"stageNum"];//发票
//    [dic setValue:@1 forKey:@"quautity"];//数量
//    [dic setValue:@4 forKey:@"GroupType"];//团购类型
//    CGFloat totalPrice = [self.dataDic[@"price"] floatValue] - [self.dataDic[@"bargainPrice"] floatValue];
////    WeakSelf(self)
////    [self.orderApi createOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
////        StrongSelf(self)
////        if (result.code == 200) {
////            NSDictionary *dic = result.resultDic;
//            SelectPayWayVC *vc = [SelectPayWayVC new];
//            vc.from = @"2";
//            vc.orderDataArr = @[@"0",self.orderNum];
////            vc.priceArr = self.dataSourceArr[0];
//            vc.totalPrice = [NSString stringWithFormat:@"%.2f",totalPrice];
//            [self.navigationController pushViewController:vc animated:true];
////        }
////    }];
    
    MyGoodsOrderDetailVC *VC = [MyGoodsOrderDetailVC new];
    VC.type = 2;
    VC.orderNum = self.orderNum;
    [self.navigationController pushViewController:VC animated:YES];
}





- (UILabel *)emptyLbl{
    if (!_emptyLbl) {
        _emptyLbl = [UILabel new];
        _emptyLbl.text = @"暂无砍价记录，立即喊朋友来砍价吧~";
        _emptyLbl.textColor = [UIColor colorWithHexString:@"7A4901"];
        _emptyLbl.font = [UIFont systemFontOfSize:14];
        _emptyLbl.hidden = true;
    }
    return _emptyLbl;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    
    return _api;
}



-(UITableView *)listTable
{
    if(_listTable==nil)
    {
        _listTable=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_listTable setDelegate:self];
        [_listTable setDataSource:self];
//        [_listTable setEmptyDataSetSource:self];
        [_listTable setTableFooterView:[UIView new]];
        _listTable.rowHeight = UITableViewAutomaticDimension;
        _listTable.estimatedRowHeight = 44;
        _listTable.showsVerticalScrollIndicator = false;
        [_listTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_listTable registerNib:[UINib nibWithNibName:@"CutRecordCell" bundle:nil] forCellReuseIdentifier:@"CutRecordCell"];
        [_listTable registerClass:[BagainTopCell class] forCellReuseIdentifier:@"BagainTopCell"];
        [_listTable registerClass:[BargainCenterCell class] forCellReuseIdentifier:@"BargainCenterCell"];
        _listTable.backgroundColor = [UIColor colorWithHexString:@"FF5C28"];
        WeakSelf(self)
//        _listTable.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
//            [weakself getBargainRecordList:RefreshTypeDown];
//        }];
//        
//        _listTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            [weakself getBargainRecordList:RefreshTypeUP];
//        }];
//        
        
        [self.view addSubview:_listTable];
    }
    return _listTable;
}

- (NSMutableArray *)datasArray{
    if (!_datasArray) {
        _datasArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _datasArray;
}

- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    return _orderApi;
}




@end
