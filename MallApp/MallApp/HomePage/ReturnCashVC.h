//
//  ReturnCashVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReturnCashVC : BaseViewController
@property (nonatomic, copy) NSString *orderNum;
@end

NS_ASSUME_NONNULL_END
