//
//  CateModel.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface CateItemModel : NSObject

@property (nonatomic , assign) NSInteger proId;
@property (nonatomic , copy) NSString *name;

@property (nonatomic , assign) BOOL isSelected;
@end

@interface CateModel : NSObject
@property (nonatomic,copy) NSString *name;
@property (nonatomic,assign) NSInteger proId;
@property (nonatomic,strong) NSMutableArray *list;
@end

NS_ASSUME_NONNULL_END
