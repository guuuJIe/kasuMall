//
//  SelectVC.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeCommonVC.h"
#import "SoftBarView.h"
#import "FliterView.h"
#import "SelectVCCell.h"
#import "SearchGoodsApi.h"
#import "GoodInfoVC.h"
#import "HomeFilterView.h"
#import "CateModel.h"
#import "UIScrollView+EmptyDataSet.h"
#import "NoDataView.h"
@interface HomeCommonVC ()<UICollectionViewDelegate,UICollectionViewDataSource,DZNEmptyDataSetSource>
@property (nonatomic,strong) UIButton * searchBtn;
@property (nonatomic,strong) UITextField * textField;
@property (nonatomic,strong) SoftBarView *softBarView;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) SearchGoodsApi *goodsApi;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,strong) NSString *size;
@property (nonatomic,strong) NSString *order;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSMutableDictionary *dic;
@property (nonatomic,strong) HomeFilterView *fliterView;
@property (nonatomic,strong) NSMutableString *propStr;
@property (nonatomic,strong) NSMutableArray *propertyArr;
@property (nonatomic,strong) NSString *brand;
@property (nonatomic,strong) NSString *prop;
@end

@implementation HomeCommonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self initProperty];
    
    [self initLayout];
    
   
    
   
    
    [self getProductData:RefreshTypeNormal];
//
    [self getProductTypeData];

    
}

- (void)initProperty{
    self.num = 1;
    self.size = @"20";
    self.order = @"3";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置navbar
//    self.navigationItem.leftBarButtonItem = nil;
//    self.navigationItem.backBarButtonItem = nil;
//    self.navigationItem.hidesBackButton = YES;
    
   
    
}




- (void)initLayout{
    if ([self.type integerValue] == 77) {
        self.title = @"汽车配件";
    }else if ([self.type integerValue] == 78){
        self.title = @"汽车精品";
    }
    
    [self.view addSubview:self.softBarView];
//    [self.view addSubview:self.tableView];
    [self.softBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(40));
        make.top.left.right.equalTo(self.view);
    }];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.softBarView.mas_bottom).offset(0);
        make.left.right.bottom.offset(0);
    }];

}


- (void)getProductData:(RefreshType)type{
    //    if ([self.type integerValue] == 77) {
    if (type == RefreshTypeUP) {
        self.num = self.num+1;
    }else if (type == RefreshTypeDown || type == RefreshTypeNormal){
        self.num = 1;
//        [self.collectionView scrollRectToVisible:CGRectZero animated:true];
    }
    NSDictionary *dic;
    if ([self.type integerValue] == 77) {
        if (mycarModel) {
           dic = @{@"type":self.type,@"order":self.order,@"prop":self.propStr};
        }else{
           dic = @{@"type":self.type,@"order":self.order,@"prop":self.propStr};
        }
        
    }else if ([self.type integerValue] == 78){
        if (self.prop) {
          dic = @{@"type":self.type,@"order":self.order,@"brand":[XJUtil insertStringWithNotNullObject:self.brand andDefailtInfo:@0],@"prop":[XJUtil insertStringWithNotNullObject:self.prop andDefailtInfo:@0]};
        }else{
           dic = @{@"type":self.type,@"order":self.order};
        }
        
    }
    
    [self.goodsApi searchProductListWithparameters:dic andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *array = result.resultDic[@"list"];
            
            if (self.num== 1) {
                if (array.count == 0) {
                    [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                    [self.collectionView.mj_header endRefreshing];
                }else{
                    if (array.count <20) {
                        [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                        [self.collectionView.mj_header endRefreshing];
                    }else{
                        [self.collectionView.mj_footer endRefreshing];
                        [self.collectionView.mj_header endRefreshing];
                    }

                }
                self.dataArr = [NSMutableArray arrayWithArray:array];
            }else{
                if (array.count == 0) {
                    [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                    [self.collectionView.mj_header endRefreshing];
                }else{
                    if (array.count <20) {
                        [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                        [self.collectionView.mj_header endRefreshing];
                    }else{
                        [self.collectionView.mj_footer endRefreshing];
                        [self.collectionView.mj_header endRefreshing];
                    }
                    
                    [self.dataArr addObjectsFromArray:array];
                }
                
            }
            [self.collectionView reloadData];
//            [self.collectionView reloadEmptyDataSet];
//            if (self.num == 1) {
//                 [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:false];
//            }
        }
    }];
    //    }
    
}


- (void)getProductTypeData{
    [self.goodsApi getProductListWithPropWithparameters:self.type withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            if ([self.type integerValue] == 78) {
                NSArray *arr = result.resultDic[@"list"];
                NSDictionary *dic = arr.firstObject;
                NSMutableDictionary *dicData = [NSMutableDictionary dictionaryWithDictionary:@{@"name":dic[@"name"],@"list":dic[@"list"]}];
                [self.propertyArr addObject:dicData];

                [self getBrandType];
            }else if ([self.type integerValue] == 77){
                 self.dic = [[NSMutableDictionary alloc] initWithDictionary:result.resultDic];
//                self.dic = result.resultDic;
            }
           
           
        }
    }];

}
//78调用
- (void)getBrandType{
    [self.goodsApi getBrandListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"name":@"品牌",@"list":result.resultDic[@"list"]}];
            [self.propertyArr addObject:dic];
            
            self.dic = [[NSMutableDictionary alloc] initWithDictionary:@{@"list":self.propertyArr}];
        }
    }];
}



//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((Screen_Width-10*3)/2,245);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SelectVCCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SelectVCCell" forIndexPath:indexPath];
    [cell setupData:self.dataArr[indexPath.item]];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GoodInfoVC *vc = [GoodInfoVC new];
    NSDictionary *dic = self.dataArr[indexPath.item];
    vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
    [self.navigationController pushViewController:vc animated:true];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NoDataView *view = [NoDataView initView];
//    view.backgroundColor = [UIColor redColor];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 50, CGRectGetHeight(self.view.frame)/2-50, 100, 100);
    return view;
}

- (void)emptyDataSet:(nonnull UIScrollView *)scrollView didTapView:(nonnull UIView *)view {
    
}


-(SoftBarView *)softBarView
{
    if(!_softBarView)
    {
        //        WeakSelf(weakSelf);
        
        NSInteger type = 0 ;
        
        
        WeakSelf(self);
        _softBarView=[[SoftBarView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40) withType:type];
        _softBarView.backgroundColor = [UIColor whiteColor];
        _softBarView.click=^(SoftEventType type,BOOL state,NSInteger soft)
        {
            if(type==PriceAscEvent)
            {
                if (soft == 1) {
                    weakself.order = @"5";
                }else if (soft == 2){
                    weakself.order = @"6";
                }
                
                
            }else if(type==OrderCountEvent)
            {
                weakself.order = @"2";
                
            }else if(type==FilterEvent)
            {
                
                weakself.fliterView.type = [weakself.type integerValue];
                [weakself.fliterView setupData:weakself.dic];
                
                [weakself.fliterView show];
                weakself.fliterView.clickBlock = ^(NSMutableString * _Nonnull prop) {
                    weakself.propStr = prop;
                    [weakself getProductData:RefreshTypeNormal];
                    [weakself.fliterView exitAni];
                };
                
                weakself.fliterView.carClickBlock = ^(NSString * _Nonnull prop, NSString * _Nonnull brand) {
                    weakself.prop = prop;
                    weakself.brand = brand;
                    [weakself getProductData:RefreshTypeNormal];
                    [weakself.fliterView exitAni];
                };
            }else if (type==HotCountEvent){
                weakself.order = @"1";
            }else if (type == defaultViewEvent){
                weakself.order = @"3";
                
            }
            
            [weakself getProductData:RefreshTypeNormal];
        };
        
    }
    return _softBarView;
}




- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((Screen_Width-10*3)/2, 245);
        flowLayout.minimumInteritemSpacing =0;
        flowLayout.minimumLineSpacing = 10;
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        _collectionView.backgroundColor = UIColorEF;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
//        _collectionView.emptyDataSetSource = self;
//        [_collectionView setEmptyDataSetSource:self];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        
        [_collectionView registerNib:[UINib nibWithNibName:@"SelectVCCell" bundle:nil] forCellWithReuseIdentifier:@"SelectVCCell"];
        WeakSelf(self)
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakself getProductData:RefreshTypeDown];
        }];
        
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakself getProductData:RefreshTypeUP];
        }];
       
        
    }
    
    return _collectionView;
    
}

- (HomeFilterView *)fliterView{
    if (!_fliterView) {
        self.fliterView = [[HomeFilterView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        
        self.fliterView.contrc = self;
        
        [self.view addSubview:self.fliterView];
    }
    return _fliterView;
}

- (SearchGoodsApi *)goodsApi{
    if (!_goodsApi) {
        _goodsApi = [SearchGoodsApi new];
    }
    return _goodsApi;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}
- (NSMutableString *)propStr{
    if (!_propStr) {
        _propStr = [NSMutableString new];
    }
    return _propStr;
}

- (NSMutableArray *)propertyArr{
    if (!_propertyArr) {
        _propertyArr = [NSMutableArray new];
    }
    return _propertyArr;
}

- (void)dealloc{
    JLog(@"销毁了");
}

@end
