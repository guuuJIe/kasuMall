//
//  CarListVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarListVC : BaseViewController
@property (nonatomic,copy) void(^clickBlock)(NSString *name);
@property (nonatomic,assign) NSInteger type;//1是从维修服务调过来 0是从首页跳进
@property (nonatomic,strong) NSString *vcname;
@end

NS_ASSUME_NONNULL_END
