//
//  HotCarTypeHeadView.h
//  MallApp
//
//  Created by Mac on 2020/2/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HotCarTypeHeadView : UIView
@property (nonatomic,copy) void(^clickBlock)(NSDictionary *dic);
//- (void)setupData:(NSDictionary *)dic;
@property (nonatomic,strong) NSArray *datasArr;
/**
 *  搜索框
 */
@property (nonatomic, strong) UITextField *textField;
@end

NS_ASSUME_NONNULL_END
