//
//  HomeServiceCell.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeServiceCell : UICollectionViewCell
@property (nonatomic,copy) void(^clickBlock)(NSInteger index);
@property (nonatomic)NSArray *dataArr;
@end

NS_ASSUME_NONNULL_END
