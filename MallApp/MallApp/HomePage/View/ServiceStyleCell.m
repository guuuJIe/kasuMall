//
//  ServiceStyleCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServiceStyleCell.h"

@implementation ServiceStyleCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         self.backgroundColor = [UIColor whiteColor];
         [self setupLayout];
//         [self setupData];
    }
    return self;
}

- (void)setupLayout
{
    [self addSubview:self.goodsImageView];
//    [self addSubview:self.priceLbl];
    [self addSubview:self.goodsTitleLabel];
  
    [self.goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(0);
        make.left.equalTo(self).offset(0);
        make.size.mas_equalTo(CGSizeMake((self.frame.size.width-10), self.frame.size.width-10));
    }];
   
    [self.goodsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodsImageView.mas_bottom).offset(8);
        make.left.mas_equalTo(self.goodsImageView);
        make.right.mas_equalTo(self.goodsImageView);
        
    }];

}

- (void)setupData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"imageUrl"]];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    self.goodsTitleLabel.text = dic[@"title"];
    
    
}


-(UILabel *)goodsTitleLabel
{
    if(!_goodsTitleLabel)
    {
        _goodsTitleLabel=[UILabel  new];
        _goodsTitleLabel.font = LabelFont13;
        _goodsTitleLabel.textColor = UIColor333;
        _goodsTitleLabel.numberOfLines = 1;
         _goodsTitleLabel.text = @"卡速自营";
//        _goodsTitleLabel.alpha = 0.2;
    }
    return _goodsTitleLabel;
}
-(UIImageView *)goodsImageView
{
    if(!_goodsImageView)
    {
        _goodsImageView=[UIImageView  new];
        _goodsImageView.image = [UIImage imageNamed:@"DefaultImage"];
//        _goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _goodsImageView;
}
@end
