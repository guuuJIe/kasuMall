//
//  CarItemDataHeadView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarItemDataHeadView.h"
@interface CarItemDataHeadView()

@end
@implementation CarItemDataHeadView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = UIColorF5F7;
        [self setuplayout];
    }
    return self;
}

- (void)setuplayout{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(23);
        make.centerY.equalTo(self);
        make.top.mas_equalTo(5);
        make.bottom.mas_equalTo(-5);
    }];
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"限时抢购";
        _titleLabel.font = LabelFont18;
        _titleLabel.textColor = LabelTextColor191919;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}
@end
