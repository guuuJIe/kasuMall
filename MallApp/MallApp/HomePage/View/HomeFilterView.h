//
//  HomeFilterView.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeFilterView : UIView
@property (nonatomic) UIViewController *contrc;
@property (nonatomic,copy) void(^clickBlock)(NSMutableString *prop);
@property (nonatomic,copy) void(^carClickBlock)(NSString *prop,NSString *brand);
- (void)show;
- (void)setupData:(NSDictionary *)dic;

- (void)exitAni;

@property (nonatomic,assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
