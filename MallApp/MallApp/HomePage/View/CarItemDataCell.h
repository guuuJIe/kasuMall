//
//  CarItemCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CarItemDataCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *carTypeImage;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
