//
//  CarItemCollectionCell.m
//  MallApp
//
//  Created by Mac on 2020/2/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarItemCollectionCell.h"
@interface CarItemCollectionCell()
@property (weak, nonatomic) IBOutlet UIImageView *typeImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end

@implementation CarItemCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setupData:(NSDictionary *)dic{
    self.nameLbl.text = dic[@"name"];
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"sPicUrl"]];
    [self.typeImg yy_setImageWithURL:URL(url) options:0];
}

@end
