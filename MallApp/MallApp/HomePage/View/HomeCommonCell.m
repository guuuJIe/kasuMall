//
//  HomeCommonCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeCommonCell.h"

@implementation HomeCommonCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         self.backgroundColor = [UIColor whiteColor];
         [self setupLayout];
//         [self setupData];
    }
    return self;
}

- (void)setupLayout
{
    [self addSubview:self.goodsImageView];
    [self addSubview:self.priceLbl];
    [self addSubview:self.goodsTitleLabel];
  
    [self.goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(100, 100));
//        make.size.mas_equalTo(CGSizeMake(109, 109));
    }];
   
    [self.goodsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodsImageView.mas_bottom).offset(8);
        make.left.mas_equalTo(self.goodsImageView);
        make.right.equalTo(self);
        
    }];
    
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(self.goodsTitleLabel.mas_bottom).offset(10);
           make.left.mas_equalTo(self.goodsImageView);
        
          
       }];
    
   
//    [self.purchaseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.goodsTitleLabel.mas_bottom).offset(10);
//        make.centerX.equalTo(self);
//        make.size.mas_equalTo(CGSizeMake(78, 22));
//    }];
//    [self.soldOutImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.center.equalTo(self.goodsImageView);
//    }];
}

- (void)setupData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"productPic"]];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    self.goodsTitleLabel.text = dic[@"title"];
    
//    self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    NSString *sumStr = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
    [aStr addAttribute:NSFontAttributeName value:LabelFont12 range:[sumStr rangeOfString:@"¥"]];

    self.priceLbl.attributedText = aStr;
}
-(UILabel *)priceLbl
{
    if(!_priceLbl)
    {
        _priceLbl=[UILabel  new];
        _priceLbl.font = LabelFont16;
        _priceLbl.textColor = FF3E3E;
        _priceLbl.text = @"¥122";
    }
    return _priceLbl;
}


-(UILabel *)goodsTitleLabel
{
    if(!_goodsTitleLabel)
    {
        _goodsTitleLabel=[UILabel  new];
        _goodsTitleLabel.font = LabelFont12;
        _goodsTitleLabel.textColor = [UIColor colorWithHexString:@"999999"];
        _goodsTitleLabel.numberOfLines = 1;
         _goodsTitleLabel.text = @"壳牌洗礼发动机机油";
//        _goodsTitleLabel.alpha = 0.2;
    }
    return _goodsTitleLabel;
}
-(UIImageView *)goodsImageView
{
    if(!_goodsImageView)
    {
        _goodsImageView=[UIImageView  new];
        _goodsImageView.image = [UIImage imageNamed:@"DefaultImage"];
//        _goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _goodsImageView;
}

@end
