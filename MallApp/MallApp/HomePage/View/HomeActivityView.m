//
//  HomeActivityView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeActivityView.h"
@interface HomeActivityView()
@property (nonatomic, strong)UIButton *chaButton;
@end

@implementation HomeActivityView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
       
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];

        
        UIImageView *headImage = [[UIImageView alloc] init];
        headImage.image = [UIImage imageNamed:@"H_pop"];
        [self addSubview:headImage];
        [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self);
        }];
        
        [self addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(headImage).offset(10);
            make.top.equalTo(headImage).offset(-26);
        }];
               
    }
    
    return self;
}


- (void)dismissView{
    [self hide];
}

-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
      
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}

@end
