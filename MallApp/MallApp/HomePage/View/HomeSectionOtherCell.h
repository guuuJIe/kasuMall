//
//  HomeSectionOtherCell.h
//  RebateApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeSectionOtherCell : UICollectionViewCell
@property (nonatomic,copy) void(^clickBlock)(NSInteger index);
@property (nonatomic,strong) NSArray *dataArr;
@end

NS_ASSUME_NONNULL_END
