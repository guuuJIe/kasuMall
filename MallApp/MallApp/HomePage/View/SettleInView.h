//
//  SettleInView.h
//  MallApp
//
//  Created by Mac on 2020/2/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettleInView : UIView
@property (nonatomic,copy) void(^itemClikcBlock)(NSInteger tag);
@end

NS_ASSUME_NONNULL_END
