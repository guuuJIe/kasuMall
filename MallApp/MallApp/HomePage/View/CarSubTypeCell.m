//
//  CarSubTypeCell.m
//  MallApp
//
//  Created by Mac on 2020/2/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarSubTypeCell.h"

@implementation CarSubTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
       self.cartypeLbl.text = dic[@"name"];
      
    }
    
}


@end
