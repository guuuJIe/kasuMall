//
//  NoDataView.m
//  MallApp
//
//  Created by Mac on 2020/2/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NoDataView.h"

@implementation NoDataView

+(instancetype)initView{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"NoDataView" owner:self options:nil] lastObject];
     // lastObject 可改为 firstObject，该数组只有一个元素，写哪个都行
}


@end
