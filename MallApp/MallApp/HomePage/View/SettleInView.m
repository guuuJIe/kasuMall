//
//  SettleInView.m
//  MallApp
//
//  Created by Mac on 2020/2/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SettleInView.h"

@implementation SettleInView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    UIView *lastView;
    NSArray *arr = @[@"申请批发",@"申请开店"];
    for (int i = 0; i<arr.count; i++) {
        UIButton *button = [UIButton new];
       
        [button setTitle:arr[i] forState:0];
        [button setTitleColor:[UIColor whiteColor] forState:0];
        if (i == 0) {
            [button setBackgroundColor:UIColorFDC4];
        }else{
            [button setBackgroundColor:UIColorFDC8];
        }
        [button.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        button.tag = i;
        button.layer.cornerRadius = 4;
        [self addSubview:button];
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.equalTo(self).offset(12);
            }else{
                make.right.equalTo(self).offset(-12);
            }
            make.top.equalTo(self).offset(11);
            make.width.mas_equalTo(Screen_Width/2 - 18);
            make.height.mas_equalTo(38);
           
        }];
        lastView = button;
    }
}

- (void)click:(UIButton *)sender{
    if (self.itemClikcBlock) {
        self.itemClikcBlock(sender.tag+1);
    }
}
@end
