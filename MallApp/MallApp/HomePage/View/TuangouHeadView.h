//
//  TuangouHeadView.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TuangouHeadView : UICollectionReusableView
@property (nonatomic,copy) void(^moreClickBlock)(void);
@end

NS_ASSUME_NONNULL_END
