//
//  HoriztalDisplayCollectionCell.m
//  RebateApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HoriztalDisplayCollectionCell.h"
@interface HoriztalDisplayCollectionCell()

@property (nonatomic , strong) UIImageView *goodsImageView;
@property (nonatomic , strong) UILabel *priceLbl;
@property (nonatomic , strong) UILabel *goodsTitleLabel;
@property (nonatomic , strong) UIButton *purchaseBtn;
@property (nonatomic , strong) UILabel *orgpriceLbl;
@property (nonatomic , strong) UIImageView *soldOutImgView;
@end
@implementation HoriztalDisplayCollectionCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         self.backgroundColor = [UIColor whiteColor];
         [self setupLayout];
//         [self setupData];
    }
    return self;
}

- (void)setupLayout
{
    [self addSubview:self.goodsImageView];
    [self addSubview:self.priceLbl];
    [self addSubview:self.goodsTitleLabel];
    [self addSubview:self.orgpriceLbl];
    [self addSubview:self.purchaseBtn];
    [self addSubview:self.soldOutImgView];
    [self.goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(0);
        make.left.right.equalTo(self);
//        make.size.mas_equalTo(CGSizeMake(140, 140));
        make.height.mas_equalTo(140);
    }];
   
    [self.goodsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodsImageView.mas_bottom).offset(8);
        make.left.mas_equalTo(8);
        make.right.equalTo(self);
        
        
    }];
    
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(self.goodsTitleLabel.mas_bottom).offset(10);
           make.left.mas_equalTo(8);
        
          
       }];
    
    [self.orgpriceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLbl).offset(-1);
        make.left.mas_equalTo(self.priceLbl.mas_right).offset(6);
    }];
//    [self.purchaseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.goodsTitleLabel.mas_bottom).offset(10);
//        make.centerX.equalTo(self);
//        make.size.mas_equalTo(CGSizeMake(78, 22));
//    }];
//    [self.soldOutImgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.center.equalTo(self.goodsImageView);
//    }];
}

- (void)setupData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"imageUrl"]];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    self.goodsTitleLabel.text = dic[@"title"];
    
//    self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"sPrice"]];
    NSString *sumStr = [NSString stringWithFormat:@"¥%@",dic[@"sPrice"]];
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
    [aStr addAttribute:NSFontAttributeName value:LabelFont12 range:[sumStr rangeOfString:@"¥"]];
    
    self.priceLbl.attributedText = aStr;
//    self.orgpriceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    
    NSString *orgpriceStr = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    NSMutableAttributedString *orgStr = [[NSMutableAttributedString alloc] initWithString:orgpriceStr];
    [orgStr addAttribute:NSFontAttributeName value:LabelFont9 range:[orgpriceStr rangeOfString:@"¥"]];
    self.orgpriceLbl.attributedText = orgStr;
}
-(UILabel *)priceLbl
{
    if(!_priceLbl)
    {
        _priceLbl=[UILabel  new];
        _priceLbl.font = LabelFont16;
        _priceLbl.textColor = FF3E3E;
        _priceLbl.text = @"¥122";
    }
    return _priceLbl;
}

-(UILabel *)orgpriceLbl
{
    if(!_orgpriceLbl)
    {
        _orgpriceLbl=[UILabel  new];
        _orgpriceLbl.font = LabelFont12;
        _orgpriceLbl.textColor = UIColorBF;
        _orgpriceLbl.text = @"¥";
    }
    return _orgpriceLbl;
}

-(UILabel *)goodsTitleLabel
{
    if(!_goodsTitleLabel)
    {
        _goodsTitleLabel=[UILabel  new];
        _goodsTitleLabel.font = LabelFont12;
        _goodsTitleLabel.textColor = [UIColor colorWithHexString:@"999999"];
        _goodsTitleLabel.numberOfLines = 1;
         _goodsTitleLabel.text = @"壳牌洗礼发动机机油";

    }
    return _goodsTitleLabel;
}
-(UIImageView *)goodsImageView
{
    if(!_goodsImageView)
    {
        _goodsImageView=[UIImageView  new];
        _goodsImageView.image = [UIImage imageNamed:@"DefaultImage"];
//        _goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _goodsImageView;
}

- (UIButton *)purchaseBtn
{
    if (!_purchaseBtn) {
        _purchaseBtn = [[UIButton alloc] init];
        [_purchaseBtn setTitle:@"立即购买" forState:0];
        [_purchaseBtn setTitleColor:LabelTextColorff4c4c forState:0];
        [_purchaseBtn.titleLabel setFont:LabelFont14];

        _purchaseBtn.layer.cornerRadius = 12;
        _purchaseBtn.layer.borderWidth = lineHeihgt;
        _purchaseBtn.layer.borderColor = LabelTextColorff4c4c.CGColor;
        _purchaseBtn.layer.masksToBounds = YES;
        _purchaseBtn.userInteractionEnabled=NO;
    }
    return _purchaseBtn;
}
-(UIImageView *)soldOutImgView
{
    if(!_soldOutImgView)
    {
//        _soldOutImgView=[UIImageView  new];
//        _soldOutImgView.image = [UIImage imageNamed:@"home_sold_out"];
    }
    return _soldOutImgView;
}
@end
