//
//  FliterView.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeFilterView.h"
#import "FliterHeadView.h"
#import "FliterCell.h"
#import "SearchGoodsApi.h"
#import "CateModel.h"
@interface HomeFilterView()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UIView *contentView;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *propertyArray;
@property (nonatomic,strong) SearchGoodsApi *goodsApi;
@property (nonatomic,strong) NSMutableString *propStr;
@property (nonatomic,strong) NSString *brand;
@property (nonatomic,strong) NSString *prop;
@end
@implementation HomeFilterView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        self.userInteractionEnabled = true;
//        self.propStr = [NSMutableString new];
        [self initLayout];
        
    }
    
    return self;
    
}

- (void)initLayout{
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(Screen_Width, 0, self.frame.size.width-120, Screen_Height)];
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.contentView];
    
    [self.contentView addSubview:self.collectionView];
    self.contentView.userInteractionEnabled = true;
    
    
    UIView *bg = [UIView new];
    bg.layer.cornerRadius = 18;
    [self.contentView addSubview:bg];
//    bg.backgroundColor = APPColor;
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(-10-BottomAreaHeight);
        make.size.mas_equalTo(CGSizeMake(178, 36));
    }];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 89, 36)];
    [button setTitle:@"重置" forState:0];
    [button.titleLabel setFont:LabelFont14];
    button.backgroundColor = UIColorEF;
    [button setTitleColor:UIColor999 forState:0];
    button.tag = 100;
    [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [bg addSubview:button];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:button.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:CGSizeMake(18, 18)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = button.bounds;
    maskLayer.path = maskPath.CGPath;
    button.layer.mask = maskLayer;
    
    UIButton *button2 = [[UIButton alloc] initWithFrame:CGRectMake(89, 0, 89, 36)];
    [button2 setTitle:@"确定" forState:0];
    [button2.titleLabel setFont:LabelFont14];
    button2.backgroundColor = APPColor;
    [button2 setTitleColor:[UIColor whiteColor] forState:0];
    button2.tag = 101;
    [button2 addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [bg addSubview:button2];
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:button2.bounds byRoundingCorners:UIRectCornerBottomRight | UIRectCornerTopRight cornerRadii:CGSizeMake(18, 18)];
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = button2.bounds;
    maskLayer2.path = maskPath2.CGPath;
    button2.layer.mask = maskLayer2;
    
    
   
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.bottom.mas_equalTo(bg.mas_top);
    }];
    
    
    
    [self layoutIfNeeded];
//
}

- (void)click:(UIButton *)sender{
    NSLog(@"%ld",(long)sender.tag);
    if (sender.tag == 101) {
        if (self.type == 77) {
            if (self.clickBlock) {
                self.clickBlock(self.propStr);
            }
            self.propStr = [NSMutableString stringWithCapacity:0];
        }else if (self.type == 78){
            if (self.carClickBlock) {
                self.carClickBlock(self.prop, self.brand);
                self.brand = @"";
                self.prop = @"";
            }
        }
       
        
    }else if (sender.tag == 100){
        self.propStr = [[NSMutableString alloc] initWithString:@""];
        self.brand = @"";
        self.prop = @"";
        for (int i = 0; i<self.dataArray.count; i++) {
            CateModel *model = self.dataArray[i];
            for (int j = 0; j<model.list.count; j++) {
                CateItemModel *iteModel = model.list[j];
                iteModel.isSelected = false;
            }
        }
        [self.collectionView reloadData];
    }
    
    
}

- (void)initAnimation{
    

    [UIView animateWithDuration:0.5 animations:^{
        //将view.frame 设置在屏幕上方
        self.contentView.frame = CGRectMake(120, 0, self.frame.size.width-120, Screen_Height);
    }];
}

- (void)exitAni{

    [UIView animateWithDuration:0.5 animations:^{
       self.contentView.frame = CGRectMake(Screen_Width, 0, self.frame.size.width, Screen_Height);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)show{
    
    [_contrc.navigationController.view addSubview:self];
    [self initAnimation];
    
}


- (void)setupData:(NSDictionary *)dic{
//    self.dataArray = dic[@"list"];
//    NSArray *data = @[@{@"name":@"精品"},@{@"name":@"精品"},@{@"name":@"精品"}];
//    if (type == 77) {
        [self.dataArray removeAllObjects];
        self.propStr = [NSMutableString new];
        NSArray *data = dic[@"list"];
        self.dataArray = [CateModel mj_objectArrayWithKeyValuesArray:data];
//    }
   


    [self.collectionView reloadData];
}




//  NSArray *data = @[
//    @{
//        @"cartProducts":@[
//                @{
//                    @"proId":@222,
//                    @"proName":@"精品"
//                },
//                @{
//                    @"proId":@223,
//                    @"proName":@"精品"
//                },
//                @{
//                    @"proId":@232,
//                    @"proName":@"精品"
//                },
//        ],
//        @"name":@"分类",
//    },
//    @{
//        @"cartProducts":@[
//                @{
//                    @"proId":@121,
//                    @"proName":@"精品"
//                },
//                @{
//                    @"proId":@122,
//                    @"proName":@"精品"
//                },
//                @{
//                    @"proId":@123,
//                    @"proName":@"精品"
//                },
//        ],
//        @"name":@"分类",
//    }
//    ];

#pragma mark --UICollectionDataSource--

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.dataArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    CateModel *model = self.dataArray[section];
   
    return model.list.count;
   
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeMake(Screen_Width, 60);
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    return CGSizeZero;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
     UICollectionReusableView *reusableview = nil;
    CateModel *model = self.dataArray[indexPath.section];
    if(kind == UICollectionElementKindSectionHeader){
        FliterHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FliterHeadView" forIndexPath:indexPath];
        
            headView.titleLabel.text = model.name;
       
            reusableview = headView;
    }
    if(kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footView" forIndexPath:indexPath];
        
        footerView.backgroundColor = [UIColor clearColor];
        
        reusableview = footerView;
        
    }
    
    return reusableview;

}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FliterCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FliterCell" forIndexPath:indexPath];
    
//    [cell setupData:self.dataArray[indexPath.item]];
    CateModel *model = self.dataArray[indexPath.section];
    
    cell.model = model.list[indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CateModel *model = self.dataArray[indexPath.section];
    
    NSArray *modelArr = model.list;
    CateItemModel *selectModel = modelArr[indexPath.item];
    for(CateItemModel  *Itemmodel in modelArr) {
        Itemmodel.isSelected = false;
    }
    selectModel.isSelected = true;
    NSLog(@"%ld",(long)selectModel.proId);
    if (self.type == 77) {
         [self.propStr appendString:[NSString stringWithFormat:@"%ld-",(long)selectModel.proId]];
    }else if (self.type == 78){
        if (indexPath.section == 0) {
            self.prop = [NSString stringWithFormat:@"%ld",(long)selectModel.proId];
        }else if (indexPath.section == 1){
            self.brand = [NSString stringWithFormat:@"%ld",(long)selectModel.proId];
        }
    }
   
    if (indexPath.section == 0) {
   
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }else if(indexPath.section == 1){
        
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:1]];
    }else if(indexPath.section == 2){
        
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:2]];
    }
   
    
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self exitAni];
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((Screen_Width-120)/3, 50);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[FliterCell class] forCellWithReuseIdentifier:@"FliterCell"];
        [_collectionView registerClass:[FliterHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FliterHeadView"];

    }
    
    return _collectionView;
    
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
//        NSArray *data = @[@{@"name":@"精品"},@{@"name":@"精品"},@{@"name":@"精品"}];
        _dataArray = [[NSMutableArray alloc] init];
//        _dataArray = [[NSMutableArray alloc] initWithArray:data];
    }
    return _dataArray;
}
- (NSMutableArray *)propertyArray{
    if (!_propertyArray) {
        _propertyArray = [NSMutableArray new];
    }
    return _propertyArray;
}

- (SearchGoodsApi *)goodsApi{
    if (!_goodsApi) {
        _goodsApi = [SearchGoodsApi new];
    }
    return _goodsApi;
}
@end
