//
//  HomeReusableView.h
//  RebateApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeReusableView : UICollectionReusableView
@property (nonatomic , strong) UILabel *titleLabel;
@property (nonatomic , strong) UIImageView *bgImg;
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
