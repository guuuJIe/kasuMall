//
//  HomeReusableView.m
//  RebateApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeReusableView.h"
@interface HomeReusableView()


@property (nonatomic , strong) UIImageView *arrowImg;

@end
@implementation HomeReusableView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = bgColor;
        self.userInteractionEnabled = true;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
        [self setupUI];
       
    }
    return self;
}


- (void)setupUI{
    
    [self.bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.left.right.bottom.equalTo(self);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.bgImg);
        make.left.mas_equalTo(15);
    }];
    
    [self.arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).offset(0);
        make.centerY.equalTo(self.titleLabel);
    }];
   
}
- (void)click{
    if (self.clickBlock) {
        self.clickBlock();
    }
}


-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        
        _titleLabel = [UILabel commonLabelWithtext:@"" color:LabelTextColor191919 font:LabelFont18 textAlignment:NSTextAlignmentLeft];
//        _titleLabel=[UILabel  new];
//        _titleLabel.text = @"限时抢购";
//        _titleLabel.font = LabelFont18;
//        _titleLabel.textColor = LabelTextColor191919;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UIImageView *)arrowImg{
    if (!_arrowImg) {
        _arrowImg = [UIImageView new];
        _arrowImg.image = [UIImage imageNamed:@"blackArrow"];
        [self addSubview:_arrowImg];
    }
    return _arrowImg;
}

- (UIImageView *)bgImg{
    if (!_bgImg) {
        _bgImg = [UIImageView new];
        _bgImg.image = [UIImage imageNamed:@"HomeHeadPic"];
        _bgImg.backgroundColor = APPColor;
        [self addSubview:_bgImg];
    }
    return _bgImg;
}

@end
