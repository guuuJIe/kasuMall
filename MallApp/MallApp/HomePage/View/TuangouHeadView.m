//
//  TuangouHeadView.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "TuangouHeadView.h"
@interface TuangouHeadView()

@property (nonatomic , strong) UILabel *titleLabel;
@property (nonatomic , strong) UIImageView *arrowImg;
@property (nonatomic , strong) UILabel *moreLabel;
@end
@implementation TuangouHeadView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = bgColor;
        [self setupUI];
       
    }
    return self;
}


- (void)setupUI{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.mas_equalTo(15);
    }];
    
  
    [self.moreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(@(-15));
    }];
    
    [self.moreLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
   
}

- (void)click{
    if (self.moreClickBlock) {
        self.moreClickBlock();
    }
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"优惠团购";
        _titleLabel.font = LabelFont18;
        _titleLabel.textColor = LabelTextColor191919;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(UILabel *)moreLabel
{
    if(!_moreLabel)
    {
        _moreLabel=[UILabel  new];
        _moreLabel.text = @"更多";
        _moreLabel.font = LabelFont14;
        _moreLabel.textColor = UIColor999;
        _moreLabel.userInteractionEnabled = true;
        [self addSubview:_moreLabel];
    }
    return _moreLabel;
}


@end
