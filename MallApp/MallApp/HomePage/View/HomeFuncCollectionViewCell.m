//
//  HomeFuncCollectionViewCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeFuncCollectionViewCell.h"
#import "FuncItemCollectionViewCell.h"
@interface HomeFuncCollectionViewCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) UICollectionView *colletionView;
@property (nonatomic) NSMutableArray *dataArr;
@end

@implementation HomeFuncCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         self.backgroundColor = [UIColor whiteColor];
         [self setupLayout];
//         [self setupData];
    }
    return self;
}
- (void)setupLayout
{
    [self addSubview:self.colletionView];
    [self.colletionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FuncItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FuncItemCollectionViewCell" forIndexPath:indexPath];
//    cell.model = self.old_wine[indexPath.row];
//    cell.backgroundColor = RandomColor;
    [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.dataArr[indexPath.row];
    if (self.clickBlock) {
        
        self.clickBlock(dic[@"name"]);
    }
}


- (UICollectionView *)colletionView
{
    if (!_colletionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(Screen_Width/4, Screen_Width/4);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _colletionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _colletionView.showsVerticalScrollIndicator = NO;
        _colletionView.showsHorizontalScrollIndicator = NO;
        _colletionView.backgroundColor = [UIColor clearColor];
        _colletionView.delegate = self;
        _colletionView.dataSource = self;
        [_colletionView registerClass:[FuncItemCollectionViewCell class] forCellWithReuseIdentifier:@"FuncItemCollectionViewCell"];
    }
    return _colletionView;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        
        _dataArr = [NSMutableArray array];
        NSArray *arr = [NSArray arrayWithObjects:
  @{@"name":@"优惠团购",@"image":@"item1"},@{@"name":@"汽车维修",@"image":@"item4"},@{@"name":@"汽车配件",@"image":@"item3"},@{@"name":@"汽车精品",@"image":@"item2"},@{@"name":@"全车保修",@"image":@"item5"},@{@"name":@"一年保养",@"image":@"item6"},@{@"name":@"随心砍",@"image":@"item7"}, nil];
        [_dataArr addObjectsFromArray:arr];
    }
    return _dataArr;
}

@end
