//
//  CarItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarItemDataCell.h"

@implementation CarItemDataCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
       self.nameLbl.text = dic[@"name"];
       NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"img"]];
       [self.carTypeImage yy_setImageWithURL:URL(url) options:0];
//        NSLog(@"%@",url);
    }
    
}

@end
