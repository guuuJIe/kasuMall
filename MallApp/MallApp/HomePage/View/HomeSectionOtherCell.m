//
//  HomeSectionOtherCell.m
//  RebateApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeSectionOtherCell.h"
#import "HoriztalDisplayCollectionCell.h"
@interface HomeSectionOtherCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic , strong) UICollectionView *colletionView;

@end

@implementation HomeSectionOtherCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = UIColorF5F7;
        [self addSubview:self.colletionView];
        [self.colletionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self).offset(0);
            //        make.bottom.equalTo(self).offset(-60);
            make.bottom.equalTo(self);
        }];
    }
    return self;
}

- (void)setDataArr:(NSArray *)dataArr{
    _dataArr = dataArr;
    [self.colletionView reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HoriztalDisplayCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HoriztalDisplayCollectionCell" forIndexPath:indexPath];
//    cell.model = self.old_wine[indexPath.row];
//    cell.backgroundColor = RandomColor;
    [cell setupData:self.dataArr[indexPath.row]];
   
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.clickBlock) {
        self.clickBlock(indexPath.row);
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 10, 0, 0);//分别为上、左、下、右
}

- (UICollectionView *)colletionView
{
    if (!_colletionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(140, 204);
        flowLayout.minimumLineSpacing = 10.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _colletionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _colletionView.showsVerticalScrollIndicator = NO;
        _colletionView.showsHorizontalScrollIndicator = NO;
        _colletionView.backgroundColor = UIColorF5F7;
        _colletionView.delegate = self;
        _colletionView.dataSource = self;
        [_colletionView registerClass:[HoriztalDisplayCollectionCell class] forCellWithReuseIdentifier:@"HoriztalDisplayCollectionCell"];
    }
    return _colletionView;
}

@end
