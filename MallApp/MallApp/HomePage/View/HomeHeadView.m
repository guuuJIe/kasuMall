//
//  HomeHeadView.m
//  RebateApp
//
//  Created by Mac on 2019/11/14.
//  Copyright © 2019 Mac. All rights reserved.
//

#import "HomeHeadView.h"

typedef NS_ENUM(NSInteger, FMRefreshState) {
    FMRefreshStateNormal = 0,     /** 普通状态 */
    FMRefreshStatePulling,        /** 释放刷新状态 */
    FMRefreshStateRefreshing,     /** 正在刷新 */
};

static NSString * _Nullable FM_Refresh_normal_title  = @"正常状态";
static NSString * _Nullable FM_Refresh_pulling_title  = @"释放刷新状态";
static NSString * _Nonnull FM_Refresh_Refreshing_title  = @"正在刷新";
@interface HomeHeadView()
@property (assign, nonatomic) id refreshTarget;
@property (nonatomic, assign) SEL refreshAction;
@property (nonatomic, strong) UIImageView  *backgroundView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIScrollView *superScrollView;
@property (nonatomic, strong) UIView  *bgView;
@property (nonatomic) UILabel *stateLbl;
@property (nonatomic, assign) FMRefreshState currentStatus;
@property (nonatomic, assign) CGFloat originalOffsetY;
@end
@implementation HomeHeadView

- (instancetype)initWithTargrt:(id)target refreshAction:(SEL)refreshAction
{
    self = [super initWithFrame:CGRectMake(0, -StatusBarAndNavigationBarHeight-50, Screen_Width, StatusBarAndNavigationBarHeight+50)];
    if (self) {
        self.refreshTarget = target;
        self.refreshAction = refreshAction;
        [self createUI];
    }
    return self;
}


- (void)createUI{
    self.backgroundColor = [UIColor redColor];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];

    imageView.image = [UIImage imageNamed:@"homebg"];
//    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:imageView];
    self.stateLbl = [[UILabel alloc] init];
    self.stateLbl.textColor = [UIColor darkGrayColor];
    self.stateLbl.font = [UIFont systemFontOfSize:16];
    self.stateLbl.text = NSLocalizedString(FM_Refresh_normal_title, nil);
    [self.stateLbl sizeToFit];
    [self addSubview:self.stateLbl];
    [self.stateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:@"contentOffset"];
}

#pragma mark - KVO
- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    if ([newSuperview isKindOfClass:[UIScrollView class]]) {
        self.superScrollView = (UIScrollView *)newSuperview;
        
        [self.superScrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    CGFloat pullDistance = -self.frame.origin.y;
//    CGFloat resfrehingDistance = -k_FMRefresh_Height + pullDistance;
    NSLog(@"释放%f",self.superScrollView.contentOffset.y);
    if (self.superScrollView.isDragging) {
        
        if (!self.originalOffsetY) {
            
            self.originalOffsetY = -self.superScrollView.contentInset.top;
            //             NSLog(@"%f",self.originalOffsetY);
        }
        CGFloat normalPullingOffset =  self.originalOffsetY-20;
        if (self.currentStatus == FMRefreshStatePulling && self.superScrollView.contentOffset.y > normalPullingOffset) {
            
            self.currentStatus = FMRefreshStateNormal;
        } else if (self.currentStatus == FMRefreshStateNormal && self.superScrollView.contentOffset.y < normalPullingOffset) {
            self.currentStatus = FMRefreshStatePulling;
        }
    }else if(!self.superScrollView.isDragging){
        if (self.currentStatus == FMRefreshStatePulling) {
            
            
            self.currentStatus = FMRefreshStateRefreshing;
        }
    }

}

- (void)setCurrentStatus:(FMRefreshState)currentStatus {
    _currentStatus = currentStatus;
    switch (_currentStatus) {
        case FMRefreshStateNormal:
            NSLog(@"切换到Normal");
           
            self.stateLbl.text = FM_Refresh_normal_title;
            [self.stateLbl sizeToFit];
            
            
            break;
        case FMRefreshStatePulling:
            NSLog(@"切换到Pulling");
            self.stateLbl.text = FM_Refresh_pulling_title;
            [self.stateLbl sizeToFit];
           
            
            break;
        case FMRefreshStateRefreshing:
            NSLog(@"切换到Refreshing");
            self.stateLbl.text = FM_Refresh_Refreshing_title;
            [self.stateLbl sizeToFit];
//            [self beginRefreshing];
           
            self.superScrollView.contentInset = UIEdgeInsetsMake(88+60, 0,0, 0);
//            self.superScrollView.contentOffset = CGPointMake(0, -self.originalOffsetY);
//            [self doRefreshAction];
            
            break;
    }
}

- (void)doRefreshAction
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    if (self.refreshTarget && [self.refreshTarget respondsToSelector:self.refreshAction])
        [self.refreshTarget performSelector:self.refreshAction];
#pragma clang diagnostic pop
    
}


- (void)endRefreshing {
    
    if (self.currentStatus != FMRefreshStateRefreshing) {
        return;
    }
    
    self.currentStatus = FMRefreshStateNormal;
//    [super endRefreshing];
    
    //在执行刷新的状态中，用户手动拖动到 nornal 状态的 offset，[super endRefreshing] 无法回到初始位置，所以手动设置
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(self.superScrollView.contentOffset.y >= self.originalOffsetY - StatusBarAndNavigationBarHeight && self.superScrollView.contentOffset.y <= self.originalOffsetY) {
            CGPoint offset = self.superScrollView.contentOffset;
            offset.y = self.originalOffsetY;
            [self.superScrollView setContentOffset:offset animated:YES];
        }
    });
    
}
@end
