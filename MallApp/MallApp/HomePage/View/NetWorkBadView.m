//
//  NetWorkBadView.m
//  MallApp
//
//  Created by Mac on 2020/2/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NetWorkBadView.h"

@implementation NetWorkBadView

+(instancetype)ViewClass{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"NetWorkBadView" owner:self options:nil] lastObject];
     // lastObject 可改为 firstObject，该数组只有一个元素，写哪个都行
}

@end
