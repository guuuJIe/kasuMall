//
//  HomeSectionOneCell.m
//  RebateApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HomeSectionOneCell.h"
#import "SDCycleScrollView.h"
@interface HomeSectionOneCell()<SDCycleScrollViewDelegate>
@property (nonatomic) SDCycleScrollView *focusHeadView;
@property (nonatomic) UILabel *carLbl;
@property (nonatomic) UILabel *subLbl;
@end

@implementation HomeSectionOneCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
//    UIView *view = [UIView new];
//    view.backgroundColor = UIColor.clearColor;
//    [self addSubview:view];
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(-200);
//        make.left.right.equalTo(self);
//        make.height.mas_equalTo(305*AdapterScal);
//    }];
//
    [self.focusHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(207*AdapterScal);
        make.top.mas_equalTo(0);
    }];
    
   
    
    UIView *carview = [UIView new];
    carview.backgroundColor = [UIColor whiteColor];
    [self addSubview:carview];
    carview.layer.shadowColor = shawdowColor.CGColor;
    carview.layer.shadowOffset = CGSizeMake(0, 0);
    carview.layer.shadowOpacity = 0.2;
    carview.layer.shadowRadius = 2.0;
    carview.layer.cornerRadius = 8;
    carview.userInteractionEnabled = true;
    [carview addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(carClick)]];
    [carview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(96*AdapterScal);
        make.top.mas_equalTo(self.focusHeadView.mas_bottom).offset(15);
        
    }];
    
    UIImageView *image = [UIImageView new];
    image.image = [UIImage imageNamed:@"addCar"];
    [carview addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(21*AdapterScal);
        make.centerY.equalTo(carview);
//        make.size.mas_equalTo(CGSizeMake(100, 78));
        make.size.mas_equalTo(CGSizeMake(45, 45));
    }];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"添加爱车";
    label.textColor = [UIColor blackColor];
    label.font = LabelFont16;
    [carview addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(27*AdapterScal);
        make.left.mas_equalTo(image.mas_right).offset(15);
        make.right.mas_equalTo(-2);
    }];
    self.carLbl = label;
    UILabel *cheaplabel = [[UILabel alloc] init];
    cheaplabel.text = @"";
    cheaplabel.textColor = [UIColor blackColor];
    cheaplabel.font = LabelFont12;
    cheaplabel.textColor = UIColor999;
    [carview addSubview:cheaplabel];
    [cheaplabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label.mas_bottom).offset(10*AdapterScal);
        make.left.mas_equalTo(label);
        make.right.mas_equalTo(-2);
    }];
    self.subLbl = cheaplabel;
    
    UIImageView *carimage = [UIImageView new];
    carimage.image = [UIImage imageNamed:@"car"];
    [carview addSubview:carimage];
    [carimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10*AdapterScal);
        make.centerY.equalTo(carview);
        //        make.size.mas_equalTo(CGSizeMake(100, 78));
        make.size.mas_equalTo(CGSizeMake(107, 78));
    }];
    
}

- (void)setCarData{
    self.carLbl.text = [XJUtil insertStringWithNotNullObject:mycarName andDefailtInfo:@"添加爱车"];
    self.subLbl.text = [XJUtil insertStringWithNotNullObject:mycarmodelName andDefailtInfo:@"选择车型"];
}

- (void)carClick{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (void)setBannerArr:(NSArray *)bannerArr{
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i<bannerArr.count; i++) {
        NSDictionary *dic = bannerArr[i];
        NSString *pic = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"picUrl"]];
        [data addObject:pic];
        
    }
    [self.focusHeadView setImageURLStringsGroup:data];
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (self.bannerClickBlock) {
        self.bannerClickBlock(index);
    }
}


-(SDCycleScrollView *)focusHeadView{
    if (!_focusHeadView) {
        _focusHeadView = [SDCycleScrollView new];
        _focusHeadView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
//        _focusHeadView.currentPageDotColor = JSCGlobalThemeColor;
        _focusHeadView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
//        _focusHeadView.layer.cornerRadius = 8;
//        _focusHeadView.mainView.layer.cornerRadius = 8;
//        _focusHeadView.placeholderImage = PlaceholderImageWithSquare;
        
        _focusHeadView.tag = 200;
        _focusHeadView.delegate = self;
//        _focusHeadView.delegate = self;
#ifdef DEBUG
        
//                [_focusHeadView setImageURLStringsGroup:@[@"http://cdnimg.jsojs.com/goods_item1/300085/icon.jpg@!414",@"http://cdnimg.jsojs.com/goods_item1/300085/icon06.jpg@!414"]];
        //#else
        
#endif
        
        [self addSubview:_focusHeadView];
    }
    return _focusHeadView;
}
@end
