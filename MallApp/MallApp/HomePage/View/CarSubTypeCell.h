//
//  CarSubTypeCell.h
//  MallApp
//
//  Created by Mac on 2020/2/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CarSubTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cartypeLbl;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
