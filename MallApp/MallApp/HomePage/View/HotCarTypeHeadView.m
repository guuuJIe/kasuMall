//
//  HotCarTypeHeadView.m
//  MallApp
//
//  Created by Mac on 2020/2/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HotCarTypeHeadView.h"
#import "CarItemCollectionCell.h"
@interface HotCarTypeHeadView()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>
@property (nonatomic,strong) UICollectionView *colletionView;

@end
@implementation HotCarTypeHeadView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIView *selectView=[[UIView alloc]initWithFrame:CGRectMake(12, 12, Screen_Width-24, 30)];
    selectView.backgroundColor=UIColorEE;
    selectView.layer.cornerRadius=4;
    selectView.layer.masksToBounds=YES;
    
    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 5, selectView.frame.size.height-5*2, selectView.frame.size.height-5*2)];
    selectIcon.image=[UIImage imageNamed:@"搜索"];
    
    self.textField=[[UITextField alloc]initWithFrame:CGRectMake(selectIcon.frame.size.width+selectIcon.frame.origin.x+5, 1, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
    self.textField.textColor=UIColor333;
    self.textField.placeholder=@"搜索车型";
    self.textField.font=LabelFont14;
    self.textField.returnKeyType = UIReturnKeySearch;
    self.textField.delegate = self;
    [selectView addSubview:selectIcon];
    [selectView addSubview:self.textField];
    [self addSubview:selectView];
    
    UIView *hotView = [UIView new];
    [self addSubview:hotView];
    hotView.backgroundColor = UIColorEF;
    [hotView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(selectView.mas_bottom).offset(9);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(30);
    }];
    
    UILabel *type = [UILabel new];
    type.text = @"热门品牌";
    type.textColor = UIColor333;
    type.font = LabelFont12;
    type.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
    [hotView addSubview:type];
    [type mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@12);
        make.centerY.equalTo(hotView);
    }];
    
    [self addSubview:self.colletionView];
    [self.colletionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(hotView.mas_bottom);
        make.left.right.bottom.equalTo(self);
//        make.height.mas_equalTo(Screen_Width/5*2);
    }];
    
    
}

- (void)setDatasArr:(NSArray *)datasArr{
    _datasArr = datasArr;
  
    [self.colletionView reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.datasArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CarItemCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CarItemCollectionCell" forIndexPath:indexPath];

    [cell setupData:self.datasArr[indexPath.item]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.clickBlock) {
        self.clickBlock(self.datasArr[indexPath.item]);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);//分别为上、左、下、右
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/4,Screen_Width/4);
}



- (UICollectionView *)colletionView
{
    if (!_colletionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((Screen_Width)/4, (Screen_Width)/4);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
//        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _colletionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _colletionView.showsVerticalScrollIndicator = NO;
        _colletionView.showsHorizontalScrollIndicator = NO;
        _colletionView.backgroundColor = [UIColor clearColor];
        _colletionView.delegate = self;
        _colletionView.dataSource = self;
        _colletionView.scrollEnabled = false;
        [_colletionView registerNib:[UINib nibWithNibName:@"CarItemCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"CarItemCollectionCell"];
    }
    return _colletionView;
}
@end
