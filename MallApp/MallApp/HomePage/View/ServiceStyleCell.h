//
//  ServiceStyleCell.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceStyleCell : UICollectionViewCell
@property (nonatomic , strong) UIImageView *goodsImageView;
@property (nonatomic , strong) UILabel *priceLbl;
@property (nonatomic , strong) UILabel *goodsTitleLabel;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
