//
//  SearchVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SearchVC.h"
#import "SelectVC.h"
#import "NSString+Extend.h"

@interface SearchVC ()<UITextFieldDelegate>
@property(nonatomic,strong)NSMutableArray * selectMuArr;
@property(nonatomic,strong)NSArray * historyStrArray;
@property(nonatomic,strong)UITextField * textField;

@property(nonatomic,strong)UIView *historyView;
@property(nonatomic,strong)UIImageView *historyImg;
@property(nonatomic,strong)UILabel *historyLabel;
@property(nonatomic,strong)UIImageView *historDelImg;
@property(nonatomic,strong)UIView *historyTagView;

@property(nonatomic,strong)UIView *hotView;
@property(nonatomic,strong)UIImageView *hotImg;
@property(nonatomic,strong)UILabel *hotLabel;
@property(nonatomic,strong)UIView *hotTagView;

@property(nonatomic,strong)UIScrollView *scrollView;
@end

@implementation SearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _selectMuArr  = [NSMutableArray new];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _historyStrArray = [[NSUserDefaults standardUserDefaults]objectForKey:@"arrayOfSelect"];
    if (_historyStrArray.count >15) {
        _historyStrArray = @[];
    }
    [self showData];
    [self initBar];
}

-(void)initBar{
    self.navigationItem.hidesBackButton = YES;
  
   
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [backBtn addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(-7, -30, 0, 0)];
    
    UIView *bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-80, 30)];
    bgView.backgroundColor=[UIColor clearColor];
    bgView.layer.cornerRadius=16;
    bgView.layer.masksToBounds=YES;
    
    
    UIView *selectView=[[UIView alloc]initWithFrame:CGRectMake(25, 0, Screen_Width-80, 30)];
    selectView.backgroundColor=[UIColor whiteColor];
    selectView.layer.cornerRadius=16;
    selectView.layer.masksToBounds=YES;
    

    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 5, selectView.frame.size.height-5*2, selectView.frame.size.height-5*2)];
    selectIcon.image=[UIImage imageNamed:@"搜索"];
    
    self.textField=[[UITextField alloc]initWithFrame:CGRectMake(selectIcon.frame.size.width+selectIcon.frame.origin.x+Default_Space-5, 0, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
    self.textField.textColor=UIColor333;
    self.textField.placeholder=@"输入关键字";
    
    if (@available(iOS 13.0, *)) {
        
        self.textField.attributedPlaceholder=[[NSAttributedString alloc]initWithString:self.textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];

      
    }else{
        
        [self.textField setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
        
    }
    
    self.textField.font=LabelFont14;
    self.textField.returnKeyType = UIReturnKeySearch;
    self.textField.delegate = self;
    [bgView addSubview:backBtn];
    
    [selectView addSubview:selectIcon];
    [selectView addSubview:self.textField];
    [bgView addSubview:selectView];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:bgView];
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"搜索" style:(UIBarButtonItemStylePlain) target:self action:@selector(selectOnClick)];
    // 字体颜色
    [rightBtn setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)setupUI{
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-20)];
    self.scrollView.bounces=NO;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=UIColorF5F7;
    self.historyView=[[UIView alloc]init];
    self.historyView.backgroundColor=UIColorF5F7;
    self.historyImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 20, 20)];
    self.historyImg.image=[UIImage imageNamed:@"select_black_icon"];
    self.historyLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, Default_Space, Screen_Width-Default_Space-20-Default_Space, 20)];
    self.historyLabel.text=@"历史搜索";
    self.historyLabel.textColor=UIColor999;
    self.historyLabel.font=LabelFont14;
    self.historDelImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.historyLabel.frame.size.width+self.historyLabel.frame.origin.x  , Default_Space, 20, 20)];
    self.historDelImg.image=[UIImage imageNamed:@"delete"];
    self.historDelImg.userInteractionEnabled = true;
    UITapGestureRecognizer * imgTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgTap)];
    [self.historDelImg addGestureRecognizer:imgTap];
    
    self.historyTagView=[[UIView alloc]init];
    
    [self.historyView addSubview:self.historyImg];
    [self.historyView addSubview:self.historyLabel];
    [self.historyView addSubview:self.historDelImg];
    [self.historyView addSubview:self.historyTagView];
    [self.scrollView addSubview:self.historyView];
    [self.view addSubview:self.scrollView];
       
    [self showData];
}

- (void)popVC{
    [self.navigationController popViewControllerAnimated:true];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self selectOnClick];
    return YES;
}

-(void)selectOnClick{
    [_selectMuArr removeAllObjects];
    if (self.textField.text.length>0) {
        for (NSInteger i = 0; i<_historyStrArray.count; i++) {
           
            NSString *str = _historyStrArray[i];
           
            if ([str isEqualToString:self.textField.text]) {
                
            }else{
                 [_selectMuArr insertObject:_historyStrArray[i] atIndex:0];
            }
        }
        if ([_selectMuArr containsObject:self.textField.text]) {
            [_selectMuArr removeObject:self.textField.text];
        }
        [_selectMuArr insertObject:self.textField.text atIndex:0];
        [[NSUserDefaults standardUserDefaults]setObject:_selectMuArr forKey:@"arrayOfSelect"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        SelectVC * vc = [SelectVC new];
        vc.keys = self.textField.text;
        
        [self.navigationController pushViewController:vc animated:true];
    }
    
   
}

-(void)imgTap{
    _historyStrArray = @[];
    
    [_selectMuArr removeAllObjects];
    [[NSUserDefaults standardUserDefaults]setObject:_selectMuArr forKey:@"arrayOfSelect"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self showData];
}

-(void)showData{
    self.historyTagView.frame=CGRectMake(0, 40, Screen_Width, [self getTagViewHeight:self.historyStrArray]);
    self.historyView.frame=CGRectMake(0, Default_Space, Screen_Width, self.historyTagView.frame.size.height+40);
    
    [self showTagData:self.historyTagView array:self.historyStrArray];
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.historyView.frame.size.height+self.historyView.frame.origin.y+Default_Space);
}


-(void)showTagData:(UIView *)tagView array:(NSArray *)array{
    if (!array) {
        return;
    }
    
    for (UIView *view in tagView.subviews){
        [view removeFromSuperview];
    }
    
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=36;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        UIFont *titleFont=LabelFont14;
        NSString *titleStr=[array objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(tagNowX, tagNowLine*tagItemHeight, itemWidth, tagItemHeight)];
        view.backgroundColor=[UIColor clearColor];
        
        UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, 5, itemWidth-Default_Space*2, tagItemHeight-Default_Space)];
        title.textColor=[UIColor colorWithHexString:@"666666"];
        title.textAlignment=NSTextAlignmentCenter;
        title.text=titleStr;
        title.font=titleFont;
        title.layer.cornerRadius=5;
//        title.layer.borderColor=APPFourColor.CGColor;
//        title.layer.borderWidth=Default_Line;
        title.layer.masksToBounds=YES;
        title.backgroundColor=[UIColor whiteColor];
        
        [view addSubview:title];
        
        view.tag=i;
        UITapGestureRecognizer *itemTap;
        if(array==self.historyStrArray){
            itemTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hisItemClick:)];
        }
        [view addGestureRecognizer:itemTap];
        
        [tagView addSubview:view];
        
        tagNowX+=itemWidth;
    }
}


-(void)hisItemClick:(id)sender{
    UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
    self.textField.text=[self.historyStrArray objectAtIndex:[tap view].tag];
    SelectVC * vc = [SelectVC new];
    vc.keys = self.textField.text;
    [self.navigationController pushViewController:vc animated:true];
}



-(CGFloat)getTagViewHeight:(NSArray *)array{
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=36;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        UIFont *titleFont=LabelFont14;
        NSString *titleStr=[array objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        tagNowX+=itemWidth;
    }
    
    return (tagNowLine+1)*tagItemHeight+Default_Space;
}



@end
