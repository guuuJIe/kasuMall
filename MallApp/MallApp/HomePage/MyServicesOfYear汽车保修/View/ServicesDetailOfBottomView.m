//
//  ServicesDetailOfBottomView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesDetailOfBottomView.h"
#import "CusPickerView.h"
@interface ServicesDetailOfBottomView()
@property (assign,nonatomic) CGRect myframe;
@end

@implementation ServicesDetailOfBottomView

//- (instancetype)initWithFrame:(CGRect)frame {
//    self = [super initWithFrame: frame];
//    if (self) {
//        ServicesDetailOfBottomView *vi = [[[NSBundle mainBundle] loadNibNamed:@"ServicesDetailOfBottomView" owner:self options:nil] firstObject];
//        
//        //        self = vi;
//        //
//        //        _myframe = frame;
//        
//        [self addSubview:vi];
//        self = vi;
//        
//        [vi mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.top.equalTo(self);
//            make.height.mas_equalTo(80+BottomAreaHeight);
//        }];
//    }
//    return self;
//}

//-(void)drawRect:(CGRect)rect
//
//{
//
//    self.frame=_myframe;//关键点在这里
//
//
//
//}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.serviceDelegate addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
}



+(instancetype)initViewClass
{
    return [[[NSBundle mainBundle] loadNibNamed:@"ServicesDetailOfBottomView" owner:self options:nil] firstObject];
}

- (void)click{
    if (self.ActBlock) {
        self.ActBlock(2);
    }
}


- (IBAction)submitAct:(id)sender {
    if (self.ActBlock) {
        self.ActBlock(1);
    }
}

- (IBAction)readAct:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
}
@end
