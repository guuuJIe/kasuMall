//
//  ServicesDetailOfBottomView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServicesDetailOfBottomView : UIView
+(instancetype)initViewClass;
@property (weak, nonatomic) IBOutlet UILabel *serviceDelegate;
@property (weak, nonatomic) IBOutlet UIButton *submitOrder;
- (IBAction)submitAct:(id)sender;
- (IBAction)readAct:(id)sender;
@property (nonatomic, copy) void (^ActBlock)(NSInteger index);
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@end

NS_ASSUME_NONNULL_END
