//
//  SchemeWayCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SchemeWayCell.h"
@interface SchemeWayCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end

@implementation SchemeWayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setupData:(NSDictionary *)dic{
    _selectBtn.selected = NO;
    _nameLbl.text = dic[@"name"];
//    [self.payIcon setImage:[UIImage imageNamed:dic[@"image"]] forState:0];
}
@end
