//
//  SubmitCarInfoBottomView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubmitCarInfoBottomView : UIView
@property (nonatomic, copy) void (^ActBlock)(void);
+(instancetype)initViewClass;
@end

NS_ASSUME_NONNULL_END
