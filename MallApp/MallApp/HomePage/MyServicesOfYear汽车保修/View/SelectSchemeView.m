//
//  SelectSchemeView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SelectSchemeView.h"
#import "PurchaseView.h"
#import "SchemeWayCell.h"
@interface SelectSchemeView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) UILabel *TitleLabel;
@property (nonatomic) UIButton *payBtn;
@property (nonatomic) UIView *bgView;
@property (nonatomic) NSIndexPath *selectIndexPath;
@end
@implementation SelectSchemeView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        self.userInteractionEnabled = true;
//        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
        [self initLayout];
        
    }
    
    return self;
    
}



- (void)initLayout{
    
    self.bgView = [[UIView alloc] initWithFrame:CGRectMake(Screen_Width, 0, self.frame.size.width-135, self.frame.size.height)];
    self.bgView.backgroundColor = [UIColor colorWithHexString:@"F0F0F0"];
    
    [self addSubview:self.bgView];
    
    self.bgView.userInteractionEnabled = true;
    [self.bgView addSubview:self.TitleLabel];
    [self.TitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView).offset(16);

        make.left.equalTo(self.bgView).offset(12);
    }];



    [self.bgView addSubview:self.TabelView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.TitleLabel.mas_bottom).offset(8);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-50-BottomAreaHeight - 20);
    }];

    [self.bgView addSubview:self.payBtn];
    [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(50);
        make.bottom.mas_equalTo(-10-BottomAreaHeight);
    }];
    
//    [self layoutIfNeeded];
  
}

- (void)setDatas:(NSArray *)datas{
    _datas = datas;
    
    [self.TabelView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"SchemeWayCell";
    SchemeWayCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[SchemeWayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
    [cell setupData:self.datas[indexPath.row]];
    
    if (_selectIndexPath.row == indexPath.row) {
        cell.selectBtn.selected = true;
    }
    
    
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectIndexPath = indexPath;
//    if (indexPath.row == 0) {
//        self.payway = 5;
//    }else if (indexPath.row == 1){//支付宝支付
//        self.payway = 3;
//       
//    }else if(indexPath.row == 2){//微信支付
//        self.payway = 2;
//    }
    [_TabelView reloadData];
}


- (void)initAnimation{
    

    [UIView animateWithDuration:0.5 animations:^{
        //将view.frame 设置在屏幕上方
        self.bgView.frame = CGRectMake(120, 0, self.frame.size.width-120, self.frame.size.height);
    }];
}

- (void)exitAni{

    [UIView animateWithDuration:0.5 animations:^{
       self.bgView.frame = CGRectMake(Screen_Width, 0, self.frame.size.width, self.frame.size.height);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)show{
    
    [_contrc.view addSubview:self];
    [self initAnimation];
    
}

- (void)clickAct{
    if (self.confirmClickBlock) {
        self.confirmClickBlock(self.datas[self.selectIndexPath.row]);
    }
    [self exitAni];
}

- (void)click{
    [self exitAni];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self exitAni];
}


#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"SchemeWayCell" bundle:nil] forCellReuseIdentifier:@"SchemeWayCell"];


        _TabelView.backgroundColor = [UIColor clearColor];
        
       
    }
    return _TabelView;
}

-(UILabel *)TitleLabel
{
    if(!_TitleLabel)
    {
        _TitleLabel=[UILabel  new];
        _TitleLabel.font = [UIFont boldSystemFontOfSize:14];
        _TitleLabel.textColor = UIColor333;
        _TitleLabel.numberOfLines = 1;
        _TitleLabel.text = @"保障车龄/里程数";

    }
    return _TitleLabel;
}

- (UIButton *)payBtn{
    if (!_payBtn) {
        _payBtn = [UIButton new];
        [_payBtn setTitle:@"确定" forState:0];
        [_payBtn setBackgroundColor:APPColor];
        [_payBtn.titleLabel setFont:LabelFont14];
        _payBtn.layer.cornerRadius = 4;
        [_payBtn addTarget:self action:@selector(clickAct) forControlEvents:UIControlEventTouchUpInside];
        WeakSelf(self);
//        _payBtn.addAddressBlock = ^{
//            StrongSelf(self)
//
//            [self exitAni];
//
//        };
        
    }
    return _payBtn;
}


@end
