//
//  SubmitCarInfoBottomView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitCarInfoBottomView.h"

@implementation SubmitCarInfoBottomView


+(instancetype)initViewClass
{
    return [[[NSBundle mainBundle] loadNibNamed:@"SubmitCarInfoBottomView" owner:self options:nil] firstObject];
}

- (IBAction)nextAct:(UIButton *)sender {
    if (self.ActBlock) {
        self.ActBlock();
    }
}

@end
