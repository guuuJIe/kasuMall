//
//  ServicesOfCenterCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOfCenterCell.h"
@interface ServicesOfCenterCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@end

@implementation ServicesOfCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = dic[@"title"];
        self.subTitleLbl.text = dic[@"sTitle"];
//        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"minPrice"]];
        NSString *sumStr = [NSString stringWithFormat:@"¥%@起",dic[@"minPrice"]];
        NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
        [aStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12 weight:UIFontWeightMedium] range:[sumStr rangeOfString:@"¥"]];
        [aStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12 weight:UIFontWeightMedium] range:[sumStr rangeOfString:@"起"]];
        self.priceLbl.attributedText = aStr;
    }
}

@end
