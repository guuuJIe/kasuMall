//
//  SchemeWayCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SchemeWayCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
