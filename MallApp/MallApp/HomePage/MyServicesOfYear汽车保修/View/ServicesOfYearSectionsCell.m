//
//  ServicesOfYearSectionsCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOfYearSectionsCell.h"
@interface ServicesOfYearSectionsCell()
@property (weak, nonatomic) IBOutlet UIImageView *bannerPic;
@property (weak, nonatomic) IBOutlet UIImageView *titlePic;

@end

@implementation ServicesOfYearSectionsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.bannerPic.image = [UIImage imageNamed:dic[@"banner"]];
        self.titlePic.image = [UIImage imageNamed:dic[@"titlePic"]];
    }
}
@end
