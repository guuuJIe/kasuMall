//
//  SelectSchemeView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectSchemeView : UIView
@property (nonatomic,copy) void(^confirmClickBlock)(NSDictionary *dic);
@property (nonatomic,strong) UIViewController *contrc;
@property (nonatomic,strong) NSArray *datas;


- (void)exitAni;
- (void)show;

@end

NS_ASSUME_NONNULL_END
