//
//  MainWebVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainWebVC : UIViewController
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSDictionary *dic;

@end

NS_ASSUME_NONNULL_END
