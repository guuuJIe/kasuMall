//
//  ServicesOfYearDetails.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOfYearDetails.h"
#import "InfoOneCell.h"
#import "ContentTableCell.h"
#import "FSScrollContentView.h"
#import "ServicesOfCenterCell.h"
#import "MainWebVC.h"
#import "PurchaseView.h"
#import "SubmitPersonCarInfo.h"
#import "GroupApi.h"
#import "navView.h"
#import "BaseTableView.h"
@interface ServicesOfYearDetails ()<UITableViewDataSource,UITableViewDelegate,FSPageContentViewDelegate,FSPageContentViewDelegate,FSSegmentTitleViewDelegate>
@property (nonatomic) BaseTableView *listTableView;
@property (nonatomic) ContentTableCell *contentCell;
@property (nonatomic) FSSegmentTitleView *titleView;
@property (nonatomic) PurchaseView *purchaseView;
@property (nonatomic) GroupApi *api;
@property (nonatomic) NSDictionary *dataDic;
@property (nonatomic) NSInteger rows;
@property (nonatomic) navView *navbar;
@property (nonatomic, assign) BOOL canScroll;
@property (nonatomic, assign) BOOL cellScroll;
@property (nonatomic, assign) CGFloat curContentoffset;
@end

@implementation ServicesOfYearDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeScrollStatus) name:@"leaveTop" object:nil];
    [self initProp];
    
    [self setupUI];
    
    [self setupData];
}

- (void)initProp{
    self.rows = 0;
}

- (void)setupUI{
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-50-BottomAreaHeight);
    }];
    
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    [self.view addSubview:self.navbar];
    [self.navbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(44+StatusBarHeight);
    }];
}

- (void)setupData{
    
    [self.api getEwProdutInfoWithparameters:@(self.ids) withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.rows = 3;
            NSArray *datas = result.result;
            self.dataDic = datas.firstObject;
           
            [self.listTableView reloadData];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"InfoOneCell";
        InfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"ContentTableCell";
        _contentCell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (!_contentCell) {
            _contentCell = [[ContentTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];

            NSMutableArray *contentVCs = [NSMutableArray array];

            for (int i = 0; i< 2; i++) {
                MainWebVC *vc = [[MainWebVC alloc] init];
                
//                vc.dic = self.dataDic;
                
                
                [contentVCs addObject:vc];
            }

            _contentCell.viewControllers = contentVCs;
            _contentCell.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, CGRectGetHeight(self.view.bounds)) childVCs:contentVCs parentVC:self delegate:self];
            [_contentCell.contentView addSubview:_contentCell.pageContentView];
        }else{
            MainWebVC *vc = (MainWebVC *)_contentCell.viewControllers.firstObject;
            vc.dic = self.dataDic;
        }
        

        return _contentCell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"ServicesOfCenterCell";
        ServicesOfCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[ServicesOfCenterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic];
        return cell;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return self.titleView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return Screen_Width*0.8;
    }else if(indexPath.section == 2){
        return CGRectGetHeight(self.view.bounds);
    }else if(indexPath.section == 1){
        return UITableViewAutomaticDimension;
    }
    
    
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 2) {
        return 60.0f;
    }
    
    return CGFLOAT_MIN;
}

- (void)purchaseAct{
    SubmitPersonCarInfo *vc = [[SubmitPersonCarInfo alloc] initWithNibName:@"SubmitPersonCarInfo" bundle:nil];
    vc.ids = self.ids;
    [self.navigationController pushViewController:vc animated:true];
}



#pragma mark FSSegmentTitleViewDelegate
- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
   
    

}

- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{

    self.contentCell.pageContentView.contentViewCurrentIndex = endIndex;
//    if (endIndex == 1) {
        MainWebVC *vc = (MainWebVC *)_contentCell.viewControllers[1];
        vc.index = endIndex;
        vc.dic = self.dataDic;
        
//    }
    
}

- (void)FSContentViewDidScroll:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex progress:(CGFloat)progress
{

    self.titleView.selectIndex = endIndex;
//    if (endIndex == 1) {
        MainWebVC *vc = (MainWebVC *)_contentCell.viewControllers[1];
        vc.index = endIndex;
        vc.dic = self.dataDic;
//    }
    _listTableView.scrollEnabled = YES;//此处其实是监测scrollview滚动，pageView滚动结束主tableview可以滑动，或者通过手势监听或者kvo，这里只是提供一种实现方式
}

- (void)changeScrollStatus//改变主视图的状态
{
    self.canScroll = YES;
    
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    CGRect rectsize = [self.listTableView rectForSection:1];
        JLog(@"%f   ---   %f ---   %f",scrollView.contentOffset.y, rectsize.size.height,rectsize.origin.y);
    //
        CGFloat value = scrollView.contentOffset.y;
        CGFloat limit = rectsize.size.height+rectsize.origin.y - StatusBarAndNavigationBarHeight;
        if (scrollView.contentOffset.y >= rectsize.size.height+rectsize.origin.y - StatusBarAndNavigationBarHeight) {
            scrollView.contentOffset = CGPointMake(0, rectsize.size.height+rectsize.origin.y - StatusBarAndNavigationBarHeight);
            if (self.cellScroll) {
                _curContentoffset = limit;
                _canScroll = false;
                _cellScroll = false;
            }
                      
        }else{
             if (value<=0) {
                 scrollView.contentOffset = CGPointMake(0, 0);
              
             }
             
             if (!self.canScroll && _curContentoffset) {
                 scrollView.contentOffset = CGPointMake(0, limit);

                 JLog(@"执行了");
             }
        }

        if (scrollView.contentOffset.y >StatusBarAndNavigationBarHeight) {
            
            [UIView animateWithDuration:0.5 animations:^{
                self.navbar.backgroundColor = APPColor;
                [self.navbar.backButton setImage:[UIImage imageNamed:@"back"] forState:0];
            }];
            
        }else{
            [UIView animateWithDuration:0.5 animations:^{
                self.navbar.backgroundColor = [UIColor clearColor];
                [self.navbar.backButton setImage:[UIImage imageNamed:@"返回"] forState:0];
            }];
        }
    
}

- (BaseTableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [BaseTableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.showsVerticalScrollIndicator = false;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOfCenterCell" bundle:nil] forCellReuseIdentifier:@"ServicesOfCenterCell"];
        [_listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (FSSegmentTitleView *)titleView{
    if (!_titleView) {
        self.titleView = [[FSSegmentTitleView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 60) titles:@[@"产品参数",@"服务说明"] delegate:self indicatorType:FSIndicatorTypeCustom];
        self.titleView.backgroundColor = [UIColor whiteColor];
        self.titleView.titleNormalColor = [UIColor colorWithHexString:@"BFC2CC"];
        self.titleView.titleSelectColor = APPColor;
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
        [self.titleView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.titleView);
            make.height.mas_equalTo(12);
            make.width.mas_equalTo(1);
        }];
        self.titleView.indicatorColor = [UIColor clearColor];
    }
    
    return _titleView;
}


- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"立即购买" forState:0];
        WeakSelf(self)
        _purchaseView.addAddressBlock = ^{
            
            if (!accessToken) {
//                [XJUtil callUserLogin:weakself];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
                return ;
            }
            
            [weakself purchaseAct];
        };
    }
    
    return _purchaseView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

- (navView *)navbar{
    if (!_navbar) {
        _navbar = [navView new];
        
        WeakSelf(self);
        _navbar.clickBlock = ^{
            [weakself.navigationController popViewControllerAnimated:true];
        };
    }
    return _navbar;
}

@end
