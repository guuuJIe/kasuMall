//
//  MainWebVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/18.
//  Copyright © 2020 Mac. All rights reserved.
//


#import "MainWebVC.h"
#import <WebKit/WebKit.h>
#import "GroupApi.h"
@interface MainWebVC ()<WKUIDelegate,WKNavigationDelegate,UIScrollViewDelegate>
@property (nonatomic, strong) WKWebView *wkwebView;
@property (nonatomic, strong) GroupApi *api;
@end

@implementation MainWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.view.backgroundColor = RandomColor;
    [self setupUI];
    
    

}
- (void)setupUI{
    self.view.backgroundColor = UIColorF5F7;
    [self.view addSubview:self.wkwebView];
    [self.wkwebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(1);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-StatusBarAndNavigationBarHeight - 50-BottomAreaHeight - 60);
    }];
    
   
}





-(void)setDic:(NSDictionary *)dic{
    if (dic) {
        NSString *htmlStr = @"";
        if (self.index == 0) {
            htmlStr = dic[@"details"];
        }else if (self.index == 1){
            htmlStr = [XJUtil insertStringWithNotNullObject:dic[@"agreementContent"] andDefailtInfo:@""];
        }
        
        [self.wkwebView loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",htmlStr] baseURL:nil];
//        [self.wkwebView loadRequest:[[NSURLRequest alloc]initWithURL:[NSURL URLWithString:@"https://www.jianshu.com/p/f4ebac24367c"]]];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
  
    JLog(@"子视图cell的偏移量%f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y <= 0) {

        scrollView.contentOffset = CGPointMake(0, 0);
       [[NSNotificationCenter defaultCenter] postNotificationName:@"leaveTop" object:nil];//到顶通知父视图改变状态
    }else{
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTop" object:nil userInfo:@{@"yValue":@(scrollView.contentOffset.y)}];
    }

   
}


- (WKWebView *)wkwebView {
    if (!_wkwebView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 偏好设置
        config.preferences = [[WKPreferences alloc] init];
        config.preferences.minimumFontSize = 10;
        config.preferences.javaScriptEnabled = YES;
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;


        WKUserContentController *userContentController = [[WKUserContentController alloc] init];
//        WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:cookie injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
//        [userContentController addUserScript:cookieScript];

//        [userContentController addScriptMessageHandler:self name:@"share"];
//        [userContentController addScriptMessageHandler:self name:@"login"];

        config.userContentController = userContentController;
        config.selectionGranularity = WKSelectionGranularityDynamic;
        config.allowsInlineMediaPlayback = YES;
//        config.mediaTypesRequiringUserActionForPlayback = false;
        
      
        _wkwebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        
        _wkwebView.navigationDelegate = self;//导航代理
        _wkwebView.UIDelegate = self;//UI代理
        _wkwebView.backgroundColor = [UIColor whiteColor];
        _wkwebView.allowsBackForwardNavigationGestures = YES;
        _wkwebView.scrollView.scrollEnabled = true;
        _wkwebView.scrollView.delegate = self;
        _wkwebView.scrollView.showsVerticalScrollIndicator = false;
        
       
    }
    return _wkwebView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    
    return _api;
}


@end
