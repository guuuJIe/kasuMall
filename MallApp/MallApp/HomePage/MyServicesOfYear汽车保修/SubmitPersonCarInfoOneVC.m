//
//  SubmitPersonCarInfoOneVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitPersonCarInfoOneVC.h"
#import "CustomeTextField.h"
#import "CarListVC.h"
#import "SubmitCarInfoBottomView.h"
#import "GroupApi.h"
#import "SubmitPersonCarInfoTwoVC.h"
@interface SubmitPersonCarInfoOneVC ()
@property (weak, nonatomic) IBOutlet CustomeTextField *mileAgeText;
@property (weak, nonatomic) IBOutlet UIView *selectTypeView;
@property (weak, nonatomic) IBOutlet CustomeTextField *catTypeText;
@property (nonatomic, strong) SubmitCarInfoBottomView *bottomView;
@property (nonatomic, strong) NSString *carModel;//卡速车型
@property (nonatomic, strong) GroupApi *api;
@end

@implementation SubmitPersonCarInfoOneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"填写资料";
    [self.selectTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectType)]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCarType:) name:@"getCarModel" object:nil];
    self.bottomView = [SubmitCarInfoBottomView initViewClass];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    WeakSelf(self)
    self.bottomView.ActBlock = ^{
        [weakself caluteAct];
    };
}

- (void)caluteAct{
    
    if (!self.carModel || !self.mileAgeText.text) {
        
        [JMBManager showBriefAlert:@"请填写完整信息"];
        
        return;
    }
    
    [self.datas setValue:self.catTypeText.text forKey:@"carType"];
    [self.datas setValue:[self.carModel substringToIndex:self.carModel.length - 1] forKey:@"carModel"];
    [self.datas setValue:self.mileAgeText.text forKey:@"Mileage"];
    
    SubmitPersonCarInfoTwoVC *vc = [[SubmitPersonCarInfoTwoVC alloc] initWithNibName:@"SubmitPersonCarInfoTwoVC" bundle:nil];
    
    vc.datas = self.datas;
    
    [self.navigationController pushViewController:vc animated:true];
    
}


- (void)getCarType:(NSNotification *)noti{
    JLog(@"%@",noti.userInfo);
    NSDictionary *dic = noti.userInfo;
    NSString *carModel = dic[@"model"];
    NSArray * array = [carModel componentsSeparatedByString:@"-"];

    self.carModel = [array componentsJoinedByString:@","];
    self.catTypeText.text = dic[@"carname"];
}

- (void)selectType{
    CarListVC *vc = [CarListVC new];
    vc.type = 1;
    vc.vcname = @"SubmitPersonCarInfoOneVC";
    [self.navigationController pushViewController:vc animated:true];
}

- (NSMutableDictionary *)datas{
    if (!_datas) {
        _datas = [NSMutableDictionary new];
    }
    
    return _datas;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
