//
//  ServicesOfYearDetails.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseHideNavViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServicesOfYearDetails : BaseHideNavViewController
@property (nonatomic, assign) NSInteger ids;
@end

NS_ASSUME_NONNULL_END
