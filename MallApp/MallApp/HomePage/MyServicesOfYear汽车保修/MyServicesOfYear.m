//
//  MyServicesOfYear.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/17.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyServicesOfYear.h"
#import "ServicesOfYearSectionsCell.h"
#import "ServicesOfItemCell.h"
#import "ServicesOfYearDetails.h"
#import "GroupApi.h"
@interface MyServicesOfYear ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tabelView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSMutableArray *datasArray;
@end

@implementation MyServicesOfYear

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"保修服务";
    
     self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"SubmitPersonCarInfo",@"SubmitServicesInfoVC",@"SelectPayWayVC"]];
    
    [self setupUI];
    
    [self setupData];
    
}

- (void)setupUI{
    [self.tabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)setupData{
    [self.api getEwProdutListWithparameters:@"" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.datasArray = [XJUtil dealData:datas withRefreshType:RefreshTypeDown withListView:self.tabelView AndCurDataArray:self.datasArray withNum:1];
            [self.tabelView reloadData];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return self.datasArray.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"ServicesOfYearSectionsCell";
        ServicesOfYearSectionsCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[ServicesOfYearSectionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:@{@"banner":@"serviceBanner",@"titlePic":@"servicesHeadPic"}];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"ServicesOfItemCell";
        ServicesOfItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[ServicesOfItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.datasArray[indexPath.row]];
//        cell.priceLbl.hidden = true;
        return cell;
    }


    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        ServicesOfYearDetails *vc = [ServicesOfYearDetails new];
        NSDictionary *dic = self.datasArray[indexPath.row];
        vc.ids = [dic[@"productId"] integerValue];
        [self.navigationController pushViewController:vc animated:true];
    }
}





-(UITableView *)tabelView
{
    if(_tabelView==nil)
    {
        _tabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tabelView setDelegate:self];
        [_tabelView setDataSource:self];
        [_tabelView setTableFooterView:[UIView new]];
        [_tabelView setTableHeaderView:[UIView new]];
        _tabelView.rowHeight = UITableViewAutomaticDimension;
        _tabelView.estimatedRowHeight = 44;
        _tabelView.showsVerticalScrollIndicator = false;
        [_tabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tabelView.sectionIndexColor = UIColor70;
        [_tabelView setSectionIndexBackgroundColor:[UIColor clearColor]];
        _tabelView.backgroundColor = [UIColor clearColor];
        [_tabelView registerNib:[UINib nibWithNibName:@"ServicesOfYearSectionsCell" bundle:nil] forCellReuseIdentifier:@"ServicesOfYearSectionsCell"];
//        [_tabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
        [_tabelView registerNib:[UINib nibWithNibName:@"ServicesOfItemCell" bundle:nil] forCellReuseIdentifier:@"ServicesOfItemCell"];
        WeakSelf(self);
        _tabelView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            [weakself setupData];
        }];
        [self.view addSubview:_tabelView];
    }
    return _tabelView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

@end
