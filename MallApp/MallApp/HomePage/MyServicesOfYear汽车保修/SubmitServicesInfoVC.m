//
//  SubmitServicesInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitServicesInfoVC.h"
#import "ServicesDetailOfBottomView.h"
#import "MyAddressPopView.h"
#import <AipOcrSdk/AipOcrSdk.h>
#import "CarListVC.h"
#import "GroupApi.h"
#import "MineApi.h"
#import "CustomeTextField.h"
#import "OrderApi.h"
#import "AdressListVC.h"
#import "SelectPayWayVC.h"
#import "MainWebVC.h"
#import "WebVC.h"
#import "CusPickerView.h"
#import "SelectSchemeView.h"
#import "DelegateShowView.h"
@interface SubmitServicesInfoVC ()
@property (weak, nonatomic) IBOutlet UITextField *meterText;
@property (weak, nonatomic) IBOutlet UITextField *carIdentifierNum;
@property (weak, nonatomic) IBOutlet UITextField *carNumText;
@property (weak, nonatomic) IBOutlet UIButton *scanBtn;
@property (weak, nonatomic) IBOutlet CustomeTextField *useNatureText;//使用性质
@property (weak, nonatomic) IBOutlet CustomeTextField *carnameText;
@property (weak, nonatomic) IBOutlet CustomeTextField *registerDateText;
@property (weak, nonatomic) IBOutlet UILabel *schemeLbl;
@property (weak, nonatomic) IBOutlet UIView *selectTypeView;

@property (nonatomic, strong) MyAddressPopView *popView;
@property (nonatomic, strong) UIViewController *vc;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) ServicesDetailOfBottomView *bottomView;

@property (nonatomic, strong) NSString *vehicleType;//车辆类型
@property (nonatomic, strong) NSString *owner;//所有人
@property (nonatomic, strong) NSString *address;//住址
@property (nonatomic, strong) NSString *useNature;//使用性质
@property (nonatomic, strong) NSString *model;//品牌型号
@property (nonatomic, strong) NSString *engineNumber;//发动机号码
@property (nonatomic, strong) NSString *registrationDate;//注册日期
@property (nonatomic, strong) NSString *issueDate;//发证日期
@property (nonatomic, strong) NSString *pic;//驾驶证图片
@property (nonatomic, assign) NSInteger mileage;//已行驶里程数
@property (nonatomic, strong) NSString *carModel;//卡速车型
@property (nonatomic, strong) NSString *carModelStr;
@property (nonatomic, strong) OrderApi *orderApi;
@property (nonatomic, strong) NSDictionary *addressDic;
@property (nonatomic, strong) NSDictionary *countDic;;
@property (nonatomic, strong) NSString *ewMileAge;
@property (nonatomic, strong) NSString *ewCarAge;

@property (nonatomic, strong) CusPickerView *pickerView;
@property (nonatomic, strong) SelectSchemeView *schemeView;
@property (nonatomic, strong) DelegateShowView *delegateView;
@property (nonatomic, assign) BOOL isread;
@end

@implementation SubmitServicesInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"选择方案";
    
   
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    [self.view addSubview:self.delegateView];
    [self.delegateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(27);
        make.bottom.mas_equalTo(self.bottomView.mas_top);
    }];
    
  
    
////   NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
//    [self.selectCarTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectType)]];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCarType:) name:@"getCarModel" object:nil];
    
    [self filldata];
    self.selectTypeView.userInteractionEnabled = true;
    [self.selectTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAct)]];
//    self.selectTypeView.layer.borderWidth = 1;
//    self.selectTypeView.layer.borderColor = UIColor66.CGColor;
//    self.selectTypeView.layer.cornerRadius = 4;
    
}

//- (void)changeSMSTime{
//    JLog(@"11");
//}

//- (void)getCarType:(NSNotification *)noti{
//    JLog(@"%@",noti.userInfo);
//    NSDictionary *dic = noti.userInfo;
//    NSString *carModel = dic[@"model"];
//    NSArray * array = [carModel componentsSeparatedByString:@"-"];
////    self.carModelStr = [NSMutableString string];
////    for (int i = 0; i<array.count; i++) {
////        NSString *str = [NSString stringWithFormat:@"%@,",array[i]];
////        [self.carModelStr appendString:str];
////    }
//    self.carModel = [array componentsJoinedByString:@","];
//    self.carnameText.text = dic[@"carname"];
//}

- (void)filldata{
    self.meterText.text = [NSString stringWithFormat:@"%@",self.datas[@"Mileage"]];
    self.carNumText.text = self.datas[@"carNum"];
    self.carIdentifierNum.text = self.datas[@"carIdentifie"];
    self.carnameText.text = self.datas[@"carType"];
    self.useNatureText.text = self.datas[@"usernature"];
    self.registerDateText.text = self.datas[@"RegistrationDate"];
}

- (IBAction)scanAct:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock();
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (void)uploadCarPic:(UIImage *)image{
    NSArray *arr =@[@{@"name":@"files",@"image":image}];
    
    [self.mineApi uploadPicWithparameters:@"9" VisitImagesArr:arr withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.pic = datas.firstObject;
            JLog(@"%@",result.result);
        }
        
    }];
}


- (void)selectType{
    CarListVC *vc = [CarListVC new];
    vc.type = 1;
 
    [self.navigationController pushViewController:vc animated:true];
}

- (void)dealloc{
    JLog(@"%s",__func__);
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupAddress{
    WeakSelf(self)
    [self.popView setData:^(NSDictionary * _Nonnull dic) {
    
        
        [weakself.popView show];
    }];
           
    
    self.popView.clickBlock = ^{
        [weakself.popView hide];

    };
    
}

- (void)makePrice{
   
    
    if ([self.bottomView.submitOrder.currentTitle isEqualToString:@"计算价格"]) {
        
      
        
        [self calutePrice];
    }else if ([self.bottomView.submitOrder.currentTitle isEqualToString:@"提交订单"]){
        [self submitOrderAct];
    }
    
    
}

- (void)changeUI{
    [self.bottomView.submitOrder setTitle:@"提交订单" forState:0];
//    self.selectSchemeView.userInteractionEnabled = false;
//    self.selectSchemeView.layer.borderColor = UIColorEF.CGColor;
//    self.schemeLbl.textColor = UIColor333;
//    self.arrowImage.image = [UIImage imageNamed:@"G_downArrow"];
    self.delegateView.hidden = false;
}

- (void)clickAct{
    
//    self.pickerView.dataArr = self.ewMileageArr;
//    WeakSelf(self)
//    self.pickerView.selectBlock = ^(id  _Nonnull result, id  _Nonnull result2, NSString * _Nonnull value) {
//        StrongSelf(self);
//        self.schemeLbl.text = result2;
//        self.ewCarAge = value;
//
//    };
//
//    [self.pickerView popPickerView];
    JLog(@"点击");
    self.schemeView.datas = self.ewMileageArr;
    [self.schemeView show];
}


- (void)submitOrderAct{
    if(!self.isread){
        [JMBManager showBriefAlert:@"您还未勾选协议"];
        
        return;
    }
    WeakSelf(self)
    [self.popView setData:^(NSDictionary * _Nonnull dic) {
        if (dic) {
            weakself.addressDic = dic;
        }
        
        [weakself.popView show];
    }];
    
    
    self.popView.clickBlock = ^{
        [weakself.popView hide];
        [weakself.navigationController pushViewController:[AdressListVC new] animated:true];
    };
    
    self.popView.purchaseBlock = ^{
        [JMBManager showLoading];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setValue:weakself.addressDic[@"id"] forKey:@"addressId"];
        [dic setValue:weakself.datas[@"ProductId"] forKey:@"groupProId"];
        [dic setValue:@0 forKey:@"cardId"];
        [dic setValue:@0 forKey:@"payment"];//支付方式
        [dic setValue:@0 forKey:@"invoiceId"];//发票ID
        [dic setValue:@0 forKey:@"stageNum"];//发票
        [dic setValue:@1 forKey:@"quautity"];//数量
        [dic setValue:@5 forKey:@"GroupType"];//团购类型
        NSInteger ids = [clerkid integerValue];
        if (ids) {
            [dic setValue:@(ids) forKey:@"tgcode"];//代理id
        }
        [weakself.orderApi createOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                [weakself.popView hide];
                SelectPayWayVC *vc = [SelectPayWayVC new];
                vc.from = @"3";
                vc.orderDataArr = result.result;
                vc.jumpVCName = @"ServicesOfYearOrderVC";
                vc.totalPrice = [NSString stringWithFormat:@"%@",weakself.countDic[@"price"]];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clerkid"];
                [weakself.navigationController pushViewController:vc animated:true];
                
            }
            JLog(@"%@",result.result);
            [JMBManager hideAlert];
            
        }];
    };
}

- (void)calutePrice{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:self.datas[@"carIdentifie"] forKey:@"vin"];
    [dic setValue:self.datas[@"carNum"] forKey:@"numberPlate"];
    [dic setValue:self.datas[@"ProductId"] forKey:@"productId"];
    [dic setValue:[self.meterText.text substringToIndex:self.meterText.text.length - 2] forKey:@"mileage"];
    [dic setValue:self.datas[@"pic"] forKey:@"pic"];
    [dic setValue:self.datas[@"engineNumber"] forKey:@"engineNumber"];
    [dic setValue:self.datas[@"RegistrationDate"] forKey:@"registrationDate"];
    [dic setValue:self.datas[@"issueDate"] forKey:@"issueDate"];
    [dic setValue:self.datas[@"usernature"] forKey:@"useNature"];
    [dic setValue:self.datas[@"address"] forKey:@"address"];
    [dic setValue:self.datas[@"owner"] forKey:@"owner"];
    [dic setValue:self.datas[@"vehicleType"] forKey:@"vehicleType"];
    [dic setValue:self.datas[@"carModel"] forKey:@"carModel"];
    [dic setValue:self.datas[@"model"] forKey:@"model"];
    //        [dic setValue:self.ewMileAge forKey:@"EwMileage"];
    [dic setValue:self.ewCarAge forKey:@"EwCarAge"];
    [JMBManager showLoading];
    [self.api makePriceForEwProdutWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            NSDictionary *dic = datas.firstObject;
            NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",[dic[@"price"] floatValue]]];
                       self.bottomView.priceLbl.text = [NSString stringWithFormat:@"¥%@",number];
            self.countDic = dic;
            [self changeUI];
        }
        JLog(@"%@",result.result);
        [JMBManager hideAlert];
    }];
}

- (MyAddressPopView *)popView{
    if (!_popView) {
        _popView = [MyAddressPopView new];
        [_popView.purchaseBtn setTitle:@"确定" forState:0];
    }
    return _popView;
}

- (ServicesDetailOfBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [ServicesDetailOfBottomView initViewClass];
        WeakSelf(self)
        self.bottomView.ActBlock = ^(NSInteger index) {
        
            if (index == 1) {
                [weakself makePrice];
            }else if (index == 2){
                WebVC *vc = [WebVC new];
                vc.ids = [weakself.datas[@"ProductId"] integerValue];
                [weakself.navigationController pushViewController:vc animated:true];
               
            }
            
            
        };
    }
    return _bottomView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    return _mineApi;
}

- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    return _orderApi;
}

- (CusPickerView *)pickerView{
    if (!_pickerView) {
        _pickerView = [[CusPickerView alloc] initWithFrame:CGRectMake(0, Screen_Height, Screen_Width, Screen_Height)];
      
        [self.navigationController.view addSubview:_pickerView];
    }
    return _pickerView;
}

- (SelectSchemeView *)schemeView{
    if (!_schemeView) {
        self.schemeView = [[SelectSchemeView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, self.view.frame.size.height)];
        
        
        
        [self.view addSubview:self.schemeView];
        WeakSelf(self)
        _schemeView.confirmClickBlock = ^(NSDictionary * _Nonnull dic) {
            StrongSelf(self)
            self.schemeLbl.text = [NSString stringWithFormat:@"%@",dic[@"name"]];
            self.ewCarAge = [NSString stringWithFormat:@"%@",dic[@"value"]];
            [self calutePrice];
        };
        self.schemeView.contrc = weakself;
    }
    return _schemeView;
}

- (DelegateShowView *)delegateView{
    if (!_delegateView) {
        _delegateView = [DelegateShowView initViewClass];
        _delegateView.hidden = true;
        WeakSelf(self)
        _delegateView.actBlock = ^{
            StrongSelf(self)
            WebVC *vc = [WebVC new];
            vc.ids = [self.datas[@"ProductId"] integerValue];
            [self.navigationController pushViewController:vc animated:true];
            
        };
        
        _delegateView.readBlock = ^(BOOL isread) {
            StrongSelf(self)
            self.isread = isread;
        };
    }
    
    return _delegateView;
}

@end
