//
//  SubmitPersonCarInfoTwoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitPersonCarInfoTwoVC.h"
#import "CustomeTextField.h"
#import "ServicesDetailOfBottomView.h"
#import "MyAddressPopView.h"
#import "CarListVC.h"
#import "GroupApi.h"
#import "SubmitServicesInfoVC.h"
#import "SubmitCarInfoBottomView.h"
@interface SubmitPersonCarInfoTwoVC ()<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet CustomeTextField *carTypeText;
@property (weak, nonatomic) IBOutlet CustomeTextField *userNatureText;
@property (weak, nonatomic) IBOutlet CustomeTextField *registerDateText;
@property (weak, nonatomic) IBOutlet CustomeTextField *mileAgeText;
@property (weak, nonatomic) IBOutlet CustomeTextField *carNumText;
@property (weak, nonatomic) IBOutlet CustomeTextField *carIdentifierText;

@property (weak, nonatomic) IBOutlet UIView *userNatureView;
@property (weak, nonatomic) IBOutlet UIView *selectCarTypeView;
@property (nonatomic, strong) SubmitCarInfoBottomView *bottomView;
@property (strong, nonatomic) GroupApi *api;
@property (strong, nonatomic) NSString *carModel;//卡速车型
@end

@implementation SubmitPersonCarInfoTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCarType:) name:@"getCarModel" object:nil];
    
    [self setupUI];
    
    [self filldata];
    
}

- (void)setupUI{
    self.title = @"选择方案";
    self.bottomView = [SubmitCarInfoBottomView initViewClass];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    WeakSelf(self)
    self.bottomView.ActBlock = ^{
        [weakself caluteAct];
    };
    
    
    self.mileAgeText.delegate = self;
    

    [self.userNatureView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectAct:)]];
    [self.selectCarTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectAct:)]];
    
}




- (void)caluteAct{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:self.datas[@"ProductId"] forKey:@"ProductId"];
    [dic setValue:[self.mileAgeText.text substringToIndex:self.mileAgeText.text.length - 2] forKey:@"Mileage"];
    [dic setValue:self.datas[@"RegistrationDate"] forKey:@"RegistrationDate"];
    [JMBManager showLoading];
    WeakSelf(self)
    [self.api caluteMileAgeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            [weakself pushToServiceInfo:datas.firstObject];
        }
        [JMBManager hideAlert];
        //        JLog(@"%@",result.result);
    }];
    
//    [self pushToServiceInfo:@{}];
}

- (void)pushToServiceInfo:(NSDictionary *)dic{
    NSArray *datas = dic[@"ewProject"];

    if (datas.count == 0) {

        [JMBManager showBriefAlert:@"您的车辆超出保修方案范围"];

        return;
    }

    [self.datas setValue:self.mileAgeText.text forKey:@"Mileage"];
    [self.datas setValue:self.registerDateText.text forKey:@"RegistrationDate"];
    [self.datas setValue:self.carNumText.text forKey:@"carNum"];
    [self.datas setValue:self.carIdentifierText.text forKey:@"carIdentifie"];
    [self.datas setValue:self.userNatureText.text forKey:@"usernature"];
    
  
    SubmitServicesInfoVC *vc = [[SubmitServicesInfoVC alloc] initWithNibName:@"SubmitServicesInfoVC" bundle:nil];
    vc.datas = self.datas;

    vc.ewMileageArr = dic[@"ewProject"];

    [self.navigationController pushViewController:vc animated:true];
}

- (void)filldata{
    self.mileAgeText.text = [NSString stringWithFormat:@"%@公里",self.datas[@"Mileage"]];
    self.carNumText.text = self.datas[@"carNum"];
    self.carIdentifierText.text = self.datas[@"carIdentifie"];
    self.carTypeText.text = self.datas[@"carType"];
    self.userNatureText.text = self.datas[@"usernature"];
    self.registerDateText.text = self.datas[@"RegistrationDate"];
}


- (void)getCarType:(NSNotification *)noti{
    JLog(@"%@",noti.userInfo);
    NSDictionary *dic = noti.userInfo;
    NSString *carModel = dic[@"model"];
    NSArray * array = [carModel componentsSeparatedByString:@"-"];

    self.carModel = [array componentsJoinedByString:@","];
    self.carTypeText.text = dic[@"carname"];
    
    [self.datas setValue:self.carTypeText.text forKey:@"carType"];
    [self.datas setValue:[self.carModel substringToIndex:self.carModel.length - 1] forKey:@"carModel"];
}

#pragma mark --- UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.text = @"";
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.mileAgeText.text = [NSString stringWithFormat:@"%@公里",textField.text];
}


- (void)selectAct:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 10000:
        {
            MMPopupItemHandler mblock = ^(NSInteger index){
                   
                   JLog(@"%ld",(long)index);
                   if (index == 0) {
                       self.userNatureText.text = @"营运";
                   }else if (index == 1){
                       self.userNatureText.text = @"非营运";
                   }
                   
               };
               
               NSArray *items =
               @[MMItemMake(@"营运", MMItemTypeNormal, mblock),
                 MMItemMake(@"非营运", MMItemTypeNormal, mblock),
               ];
               [JSCMMPopupViewTool showMMPopSheetWithTitle:@"" withItemsArray:items];
        }
            break;
            
        case 10001:
            
        {
            CarListVC *vc = [CarListVC new];
            vc.type = 1;
            vc.vcname = @"SubmitPersonCarInfoTwoVC";
            [self.navigationController pushViewController:vc animated:true];
            
        }
            break;
            
        default:
            break;
    }
}

- (NSMutableDictionary *)datas{
    if (!_datas) {
        _datas = [NSMutableDictionary new];
    }
    
    return _datas;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

@end
