//
//  SubmitPersonCarInfo.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitPersonCarInfo.h"
#import "CustomeTextField.h"
#import "CarListVC.h"
#import <AipOcrSdk/AipOcrSdk.h>
#import "SubmitCarInfoBottomView.h"
#import "GroupApi.h"
#import "SubmitPersonCarInfoOneVC.h"
#import "MineApi.h"
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>
@interface SubmitPersonCarInfo ()
@property (weak, nonatomic) IBOutlet UIView *scanView;

@property (weak, nonatomic) IBOutlet CustomeTextField *carNumText;
@property (weak, nonatomic) IBOutlet CustomeTextField *carIdentifieText;
@property (weak, nonatomic) IBOutlet CustomeTextField *useNatureText;
@property (weak, nonatomic) IBOutlet CustomeTextField *registerDateText;

@property (weak, nonatomic) IBOutlet CustomeTextField *mileAgeText;
@property (weak, nonatomic) IBOutlet CustomeTextField *carTypeText;
@property (weak, nonatomic) IBOutlet UIView *carTypeView;

@property (nonatomic, strong) UIViewController *vc;
@property (nonatomic, strong) SubmitCarInfoBottomView *bottomView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSString *carModel;//卡速车型
@property (nonatomic, strong) NSString *pic;//驾照图片
@property (nonatomic, strong) NSString *model;//品牌型号
@property (nonatomic, strong) NSString *engineNumber;//发动机号码
@property (nonatomic, strong) NSString *issueDate;//发证日期
@property (nonatomic, strong) NSString *vehicleType;//车辆类型
@property (nonatomic, strong) NSString *owner;//所有人
@property (nonatomic, strong) NSString *address;//住址
@property (nonatomic, strong) NSMutableDictionary *datas;
@property (nonatomic, strong) MineApi *mineApi;
@end

@implementation SubmitPersonCarInfo

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"填写资料";
    [self.scanView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.carTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCarType:) name:@"getCarModel" object:nil];
    self.bottomView = [SubmitCarInfoBottomView initViewClass];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    WeakSelf(self)
    self.bottomView.ActBlock = ^{
        [weakself caluteAct];
    };
    
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        
    }];
    
    
}


- (void)getCarType:(NSNotification *)noti{
    JLog(@"%@",noti.userInfo);
    NSDictionary *dic = noti.userInfo;
    NSString *carModel = dic[@"model"];
    NSArray * array = [carModel componentsSeparatedByString:@"-"];

    self.carModel = [array componentsJoinedByString:@","];
    self.carTypeText.text = dic[@"carname"];
}

- (void)caluteAct{
    
    if (self.carNumText.text.length == 0 || self.carIdentifieText.text.length == 0 || self.useNatureText.text == 0 || self.address.length == 0 || self.vehicleType.length == 0 || self.owner.length == 0 || self.model.length == 0 || self.engineNumber.length == 0 || self.issueDate.length == 0) {
        [JMBManager showBriefAlert:@"信息不完整，请尝试重新填写"];
        return;
    }

//    NSMutableDictionary *dic = [NSMutableDictionary new];
//    [dic setValue:@(self.ids) forKey:@"ProductId"];
//    [dic setValue:self.mileAgeText.text forKey:@"Mileage"];
//    [dic setValue:self.registerDateText.text forKey:@"RegistrationDate"];
//     WeakSelf(self)
//    [self.api caluteMileAgeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//            NSArray *datas = result.result;
//            [weakself pushToServiceInfo:datas.firstObject];
//        }
//
//        JLog(@"%@",result.result);
//    }];
//
    [self.datas setValue:@(self.ids) forKey:@"ProductId"];
    [self.datas setValue:self.registerDateText.text forKey:@"RegistrationDate"];
    [self.datas setValue:self.carNumText.text forKey:@"carNum"];
    [self.datas setValue:self.carIdentifieText.text forKey:@"carIdentifie"];
    [self.datas setValue:self.useNatureText.text forKey:@"usernature"];
//    [self.datas setValue:self.carModel forKey:@"carModel"];
    [self.datas setValue:self.pic forKey:@"pic"];
    [self.datas setValue:self.address forKey:@"address"];
    [self.datas setValue:self.vehicleType forKey:@"vehicleType"];
    [self.datas setValue:self.owner forKey:@"owner"];
    [self.datas setValue:self.model forKey:@"model"];
    [self.datas setValue:self.engineNumber forKey:@"engineNumber"];
    [self.datas setValue:self.issueDate forKey:@"issueDate"];
    SubmitPersonCarInfoOneVC *vc = [[SubmitPersonCarInfoOneVC alloc] initWithNibName:@"SubmitPersonCarInfoOneVC" bundle:nil];
    vc.datas = self.datas;
    [self.navigationController pushViewController:vc animated:true];
}

- (void)pushToServiceInfo:(NSDictionary *)dic{
    NSArray *datas = dic[@"ewProject"];
    
    if (datas.count == 0) {
        
        [JMBManager showBriefAlert:@"您的车辆超出保修方案范围"];
        
        return;
    }
    [self.datas setValue:@(self.ids) forKey:@"ProductId"];
    [self.datas setValue:self.mileAgeText.text forKey:@"Mileage"];
    [self.datas setValue:self.registerDateText.text forKey:@"RegistrationDate"];
    
    [self.datas setValue:self.carTypeText.text forKey:@"carType"];
    [self.datas setValue:self.carNumText.text forKey:@"carNum"];
    [self.datas setValue:self.carIdentifieText.text forKey:@"carIdentifie"];
    [self.datas setValue:self.useNatureText.text forKey:@"usernature"];
//    [self.datas setValue:self.carModel forKey:@"carModel"];
    [self.datas setValue:self.pic forKey:@"pic"];
    [self.datas setValue:self.address forKey:@"address"];
    [self.datas setValue:self.vehicleType forKey:@"vehicleType"];
    [self.datas setValue:self.owner forKey:@"owner"];
    [self.datas setValue:self.model forKey:@"model"];
    [self.datas setValue:self.engineNumber forKey:@"engineNumber"];
    [self.datas setValue:self.issueDate forKey:@"issueDate"];
//    SubmitServicesInfoVC *vc = [[SubmitServicesInfoVC alloc] initWithNibName:@"SubmitServicesInfoVC" bundle:nil];
//    vc.datas = self.datas;
////    vc.clickBlock = ^{
////        [self resetData];
////    };
//    vc.ewMileageArr = dic[@"ewProject"];
//
//    [self.navigationController pushViewController:vc animated:true];
}



- (void)resetData{
    self.carIdentifieText.text = @"";
    self.mileAgeText.text = @"";
    self.carNumText.text = @"";
    self.carTypeText.text = @"";
    self.useNatureText.text = @"";
    self.registerDateText.text = @"";
    self.pic = @"";
    self.carModel = @"";
}



- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            [self scanPic];
        }
            break;
        case 101:
        {
            [self selectType];
        }
            break;
        default:
            break;
    }
   
}

- (void)selectType{
//    CarListVC *vc = [CarListVC new];
//    vc.type = 1;
//    vc.vcname = @"SubmitPersonCarInfo";
//    [self.navigationController pushViewController:vc animated:true];
}

- (void)scanPic{
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请在IOS“设置”-“隐私”-“相机”中打开" preferredStyle:(UIAlertControllerStyleAlert)];
        UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"去设置" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if([[UIApplication sharedApplication] canOpenURL:url]) {
                NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                
                [[UIApplication sharedApplication] openURL:url];
            }
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    WeakSelf(self)
    self.vc = [AipGeneralVC ViewControllerWithHandler:^(UIImage *image) {
        
        //并发队列
        dispatch_queue_t queue = dispatch_queue_create("kk", DISPATCH_QUEUE_CONCURRENT);
        
        dispatch_async(queue, ^{
            [weakself uploadCarPic:image];
        });
        
        
      
        [JMBManager showLoading];
        [[AipOcrService shardService] detectVehicleLicenseFromImage:image withOptions:nil successHandler:^(id result) {
            NSDictionary *dic = result[@"words_result"];
            JLog(@"result --- %@",dic);
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakself setupInfo:dic];
                [weakself.vc dismissViewControllerAnimated:true completion:nil];
                
            });
            
            [JMBManager hideAlert];
        } failHandler:^(NSError *err) {
            [JMBManager showBriefAlert:@"识别失败"];
            [JMBManager hideAlert];
        }];
    }];
    
//    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, 100, 25)];
//    [self.vc.view addSubview:btn];
//    btn.backgroundColor = [UIColor redColor];
//    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(-100);
//        make.size.mas_equalTo(CGSizeMake(100, 25));
//        make.centerX.mas_equalTo(self.vc);
//    }];
//
    self.vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:self.vc animated:YES completion:nil];
}


- (void)uploadCarPic:(UIImage *)image{
    NSArray *arr =@[@{@"name":@"files",@"image":image}];
    
    [self.mineApi uploadPicWithparameters:@"9" VisitImagesArr:arr withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.pic = datas.firstObject;
            JLog(@"%@",result.result);
        }
    }];
}



- (void)setupInfo:(NSDictionary *)dic{
    JLog(@"%@",dic);
    self.carNumText.text = [XJUtil insertStringWithNotNullObject:dic[@"号牌号码"][@"words"] andDefailtInfo:@""];
    self.carIdentifieText.text = [XJUtil insertStringWithNotNullObject:dic[@"车辆识别代号"][@"words"] andDefailtInfo:@""];
    self.useNatureText.text = [XJUtil insertStringWithNotNullObject:dic[@"使用性质"][@"words"] andDefailtInfo:@""];
    self.registerDateText.text = [XJUtil insertStringWithNotNullObject:dic[@"注册日期"][@"words"] andDefailtInfo:@""];
    self.address = [XJUtil insertStringWithNotNullObject:dic[@"住址"][@"words"] andDefailtInfo:@""];
    self.vehicleType = [XJUtil insertStringWithNotNullObject:dic[@"车辆类型"][@"words"] andDefailtInfo:@""];
    self.owner = [XJUtil insertStringWithNotNullObject:dic[@"所有人"][@"words"] andDefailtInfo:@""];
    self.model = [XJUtil insertStringWithNotNullObject:dic[@"品牌型号"][@"words"] andDefailtInfo:@""];
    self.engineNumber = [XJUtil insertStringWithNotNullObject:dic[@"发动机号码"][@"words"] andDefailtInfo:@""];
    self.issueDate = [XJUtil insertStringWithNotNullObject:dic[@"发证日期"][@"words"] andDefailtInfo:@""];
 
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

- (NSMutableDictionary *)datas{
    if (!_datas) {
        _datas = [NSMutableDictionary new];
    }
    return _datas;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    
    return _mineApi;
}

@end
