//
//  NewHomeVC.m
//  RebateApp
//
//  Created by Mac on 2020/1/2.
//  Copyright © 2020 Mac. All rights reserved.
//
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "NetWorkBadView.h"
#import "GYZChooseCityController.h"
#import "GYZCity.h"

#import "NewHomeVC.h"
#import "HomeSectionOneCell.h"
#import "HomeSectionOtherCell.h"
#import "HomeReusableView.h"
#import "HomeFuncCollectionViewCell.h"
#import "XianShiTuanGouVC.h"
#import "GoodInfoVC.h"
#import "AddAdressVC.h"
#import "BaseNavigationController.h"
#import "AdressListVC.h"
#import "TuangouHeadView.h"
#import "MallPeijianCell.h"
#import "HomeServiceCell.h"
#import "TuanGouGoodInfoVC.h"
#import "SelectVC.h"
#import "ShopInfoVC.h"
#import "SearchVC.h"
#import "HomePageApi.h"
#import "CarListVC.h"
#import "CarSubListVC.h"
#import "HomeCommonVC.h"
#import "WeiXiuShop.h"
#import "ScanVC.h"
#import "HomeMarketingCollectionCell.h"
#import "XianShiTuanGouVC.h"
#import "ShopSettleInCollectionCell.h"
#import "MerchantSettledVC.h"
#import "UIScrollView+EmptyDataSet.h"
#import "BargainVC.h"
#import "HelpBarginGoodVC.h"
#import "MyServicesOfYear.h"
#import "CarMaintainceList.h"
#import "ShopVC.h"
#import "AccountApi.h"
#import "PayMannagerUtil.h"
#import "NewBindUserVC.h"
#import "NewOtherLoginVC.h"
//#import <UMCommon/UMCommon.h>
//#import <UMVerify/UMVerify.h>
#import "TasksDownloaderOperation.h"
@interface NewHomeVC ()<UICollectionViewDelegate,UICollectionViewDataSource,GYZChooseCityDelegate,AMapLocationManagerDelegate,DZNEmptyDataSetSource>
@property (nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic,strong)UIButton * searchBtn;
@property (nonatomic,strong)UIButton * scanBtn;
@property (nonatomic,strong)UIBarButtonItem * msgBtn;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)HomePageApi *homeApi;
@property (nonatomic,strong)NSArray *groupArr;
@property (nonatomic,strong)NSArray *peijianArr;
@property (nonatomic,strong)NSArray *cargoodsArr;
@property (nonatomic,strong)NSArray *carServiceArr;
@property (nonatomic,strong)NSArray *bannerArr;
@property (nonatomic,strong)AMapLocationManager *locationManager;
@property (nonatomic,strong)AccountApi *accountApi;
@end

@implementation NewHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    [self initBar];
//    [self.collectionView.mj_header beginRefreshing];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshHomeData) name:@"refreshHomeData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHomeData) name:refreshAppHome object:nil];
    //网络状态发生变化的时候的通知方法
    [[NSNotificationCenter defaultCenter]addObserver:self
                                               selector:@selector(NetWorkStatesChange:) name:@"netWorkChangeEventNotification"
                                                 object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(quickLogin) name:@"quickLogin" object:nil];
    [self getLocation];
    [self getVersion];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置navbar
    self.navigationItem.leftBarButtonItem = nil;
//    [self initBar];
    self.navigationItem.backBarButtonItem = nil;
   
   
    
    [self getPasteBoardStr];
}


- (void)initBar{
    

    UIView * searview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width,30)];
    searview.backgroundColor = [UIColor clearColor];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searview];
    self.navigationItem.titleView = searview;
    self.titleLabel.frame = CGRectMake(10, 0, 50, 30);
    [searview addSubview:self.titleLabel];
    

    
//    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.titleLabel.frame)+5, 0, searview.frame.size.width - 130, 30)];
//    [self.searchBtn setTitle:@"请输入商品名称" forState:UIControlStateNormal];
//    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:14];
//    [self.searchBtn setTitleColor:UIColorB6 forState:UIControlStateNormal];
//    [self.searchBtn setBackgroundColor:[UIColor whiteColor]];
//    [self.searchBtn setImageEdgeInsets:UIEdgeInsetsMake(5, -95*AdapterScal, 5, 0)];
//    [self.searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -85*AdapterScal, 0, 0)];
////    self.searchBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
////    [self.searchBtn.titleLabel setTextAlignment:NSTextAlignmentLeft];
//    [self.searchBtn setImage:[UIImage imageNamed:@"搜索"] forState:(UIControlStateNormal)];
//    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
//
//    self.searchBtn.layer.cornerRadius = 15;
//    self.searchBtn.layer.masksToBounds = YES;
//    [searview addSubview:self.searchBtn];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.titleLabel.frame)+5, 0, searview.frame.size.width - 130, 30)];
    titleView.backgroundColor = [UIColor whiteColor];
    titleView.layer.cornerRadius = 15;
    [titleView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onClickSearchBtn)]];
    
    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 20, 20)];
    selectIcon.image=[UIImage imageNamed:@"搜索"];
    
    UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(selectIcon.frame), 1, searview.frame.size.width-130-selectIcon.frame.size.width, searview.frame.size.height-1)];
    textField.textColor=UIColor333;
    textField.placeholder=@"输入关键字";
    
    if (@available(iOS 13.0, *)) {
        
        textField.attributedPlaceholder=[[NSAttributedString alloc]initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];

    }else{
        [textField setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
    }
    textField.font=LabelFont14;
    textField.returnKeyType = UIReturnKeySearch;
    textField.userInteractionEnabled = false;

    [titleView addSubview:selectIcon];
    [titleView addSubview:textField];
    [searview addSubview:titleView];
    
    [searview addSubview:self.scanBtn];
    self.scanBtn.frame = CGRectMake(CGRectGetMaxX(titleView.frame)+14, 2, 60, 30);
    

    
    
}

- (void)refreshHomeData{
    //刷新车型
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
}

- (void)onClickSearchBtn{
    
    [self.navigationController pushViewController:[SearchVC new] animated:true];
    
}
- (void)NetWorkStatesChange:(NSNotification *)sender{
    int networkState = [[sender object] intValue];
    switch (networkState) {
        case -1:
            //未知网络状态
            [self.collectionView reloadNetUnworkDataSet];
            break;
            
        case 0:
            //没有网络
            [self.collectionView reloadNetUnworkDataSet];
            break;
            
        case 1:
            //3G或者4G，反正用的是流量
            [self.collectionView reloadNetworkDataSet];
            [self getHomeData];
            break;
            
        case 2:
            //WIFI网络
            [self.collectionView reloadNetworkDataSet];
            [self getHomeData];
            break;
            
        default:
            break;
    }
}

- (void)scan{
    ScanVC *vc = [ScanVC new];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)msg{
    
}

- (void)selectCity{
    NSMutableArray *hotcity = [NSMutableArray new];
    GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
    [cityPickerVC setDelegate:self];
    NSArray *array = @[
          @{@"city_key":@"200010000",@"city_name":@"上海市",@"initials":@"sh",@"pinyin":@"shanghai",@"short_name":@"上海"},
          @{@"city_key":@"100010000",@"city_name":@"北京市",@"initials":@"bj",@"pinyin":@"beijing",@"short_name":@"北京"},
          @{@"city_key":@"300210000",@"city_name":@"深圳市",@"initials":@"sz",@"pinyin":@"shenzhen",@"short_name":@"深圳"},
          @{@"city_key":@"300110000",@"city_name":@"广州市",@"initials":@"gz",@"pinyin":@"guangzhou",@"short_name":@"广州"},
          @{@"city_key":@"600010000",@"city_name":@"杭州市",@"initials":@"hz",@"pinyin":@"hangzhou",@"short_name":@"杭州"},
          @{@"city_key":@"700010000",@"city_name":@"南京市",@"initials":@"nj",@"pinyin":@"nanjing",@"short_name":@"南京"},
          @{@"city_key":@"800010000",@"city_name":@"成都市",@"initials":@"cd",@"pinyin":@"chengdu",@"short_name":@"成都"},
          @{@"city_key":@"400010000",@"city_name":@"武汉市",@"initials":@"wh",@"pinyin":@"wuhan",@"short_name":@"武汉"},
    ];
    for (int i = 0; i<array.count; i++) {
        GYZCity *city = [GYZCity new];
        NSDictionary *dic = array[i];
        city.cityID = dic[@"city_key"];
        city.cityName = dic[@"city_name"];
        city.initials = dic[@"initials"];
        city.pinyin = dic[@"pinyin"];
        city.shortName = dic[@"short_name"];
        [hotcity addObject:city];
    }
    cityPickerVC.hotCitys =hotcity;
    
    BaseNavigationController *vc = [[BaseNavigationController alloc] initWithRootViewController:cityPickerVC];
    vc.modalPresentationStyle = 0;
    [self presentViewController:vc animated:true completion:nil];
    
}

- (void)getLocation{
    if([CLLocationManager locationServicesEnabled]){
        //查询天气
//        AMapWeatherSearchRequest *request = [[AMapWeatherSearchRequest alloc] init];
        AMapLocationManager *locationManager = [[AMapLocationManager alloc]init];
        locationManager.delegate = self;
        
        // 设定定位的最小更新距离。单位米，默认为 kCLDistanceFilterNone，表示只要检测到设备位置发生变化就会更新位置信息
        locationManager.distanceFilter = 5;
        
        // 设定期望的定位精度。单位米，默认为 kCLLocationAccuracyBest
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
        //        //是否允许后台定位。默认为NO。只在iOS 9.0及之后起作用。
        //        [locationManager setAllowsBackgroundLocationUpdates:NO];
        //
        // 开始定位服务
        
        _locationManager = locationManager;
        
        [_locationManager requestLocationWithReGeocode:true completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
            self.titleLabel.text = regeocode.city;
        }];
    ;
    }
}

- (void)setupUI{
   
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - GYZCityPickerDelegate
- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
{
    self.titleLabel.text = city.cityName;
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController{
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 7;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            
            return 1;
            
            break;
        case 1:
            
            return 1;
            
            break;
        case 2:
                       
            return 1;
                       
            break;
        case 3:
            
            return self.groupArr.count == 0 ? 0 : 1;
            
            break;
//        case 4:
//
//            return self.carServiceArr.count == 0 ? 0 : 1;
//
//            break;
        case 4:
            
            return self.peijianArr.count == 0 ? 0 : 1;
            
            break;
        case 5:
            
            return self.cargoodsArr.count == 0 ? 0 : 1;
            
            break;
        case 6:
            
            return self.cargoodsArr.count == 0 ? 0 : 1;
            
            break;
        default:
            return 0;
            break;
    }
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize zeroSize = CGSizeMake(Screen_Width, 0);
    CGSize headSize = CGSizeMake(Screen_Width, 70*AdapterScal);
 
    if (section == 4){
        return self.peijianArr.count == 0 ? zeroSize : headSize;
    }else if (section == 5){
         return self.cargoodsArr.count == 0 ? zeroSize : headSize;
//        return headSize;
    }else if (section == 3){
        return self.groupArr.count == 0 ? zeroSize : CGSizeMake(Screen_Width, 50*AdapterScal);
    }else if (section == 2){
        return CGSizeMake(Screen_Width, 15);
    }else if (section == 6){
        return CGSizeMake(Screen_Width, 15);
    }else{
        return CGSizeZero;
    }
    
   
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    
    
    return CGSizeZero;
}



-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableview = nil;
    if(kind == UICollectionElementKindSectionHeader){
//         if(indexPath.section == 4){
//                    HomeReusableView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeadView" forIndexPath:indexPath];
//
//        //            headView.clickBlock = ^{
//        //                WeiXiuShop *vc = [WeiXiuShop new];
//        //                [self.navigationController pushViewController:vc animated:true];
//        //            };
//
//                    headView.titleLabel.text = @"汽车配件";
//
//                    reusableview = headView;
//                }else
        if (indexPath.section == 3){
            TuangouHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"TuangouHeadView" forIndexPath:indexPath];
            
            
            headView.moreClickBlock = ^{
                [self.navigationController pushViewController:[XianShiTuanGouVC new] animated:true];
            };
            
            
            reusableview = headView;
        }else if (indexPath.section == 4){
            HomeReusableView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeadView" forIndexPath:indexPath];
            
            headView.clickBlock = ^{
                HomeCommonVC *vc = [HomeCommonVC new];
                vc.type = @"77";//汽车配件
                
                [self.navigationController pushViewController:vc animated:true];
            };
            
            headView.titleLabel.text = @"汽车配件";
            headView.bgImg.image = [UIImage imageNamed:@"H_Pic-2"];
            reusableview = headView;
        }else if (indexPath.section == 5){
            HomeReusableView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeadView" forIndexPath:indexPath];
            headView.clickBlock = ^{
                HomeCommonVC *vc = [HomeCommonVC new];
                vc.type = @"78";//汽车精品
                
                [self.navigationController pushViewController:vc animated:true];
            };
            
            
            headView.titleLabel.text = @"汽车精品";
            headView.bgImg.image = [UIImage imageNamed:@"H_Pic-1"];
            reusableview = headView;
        }else if (indexPath.section == 2){
           UICollectionReusableView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeadView2" forIndexPath:indexPath];
           
            headView.backgroundColor = UIColorF5F7;
            
            reusableview = headView;
        }else if (indexPath.section == 6){
           UICollectionReusableView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeadView2" forIndexPath:indexPath];
           
            headView.backgroundColor = UIColorF5F7;
            
            reusableview = headView;
        }
    }
    if(kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footView" forIndexPath:indexPath];
        
        footerView.backgroundColor = [UIColor clearColor];
        
        reusableview = footerView;
        
    }
    
    return reusableview;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            HomeSectionOneCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeSectionOneCell" forIndexPath:indexPath];
            cell.bannerArr = self.bannerArr;
            cell.clickBlock = ^{

                [self.navigationController pushViewController:[CarListVC new] animated:true];
            };
            WeakSelf(self)
            cell.bannerClickBlock = ^(NSInteger index) {
                StrongSelf(self)
                NSDictionary *dic = self.bannerArr[index];
                NSString *str = [NSString stringWithFormat:@"%@",dic[@"url"]];
                
                if ([str hasPrefix:@"https"]) {
                    XianShiTuanGouVC *vc = [XianShiTuanGouVC new];
                    [self.navigationController pushViewController:vc animated:true];
                }
                
                
            };
            [cell setCarData];
            return cell;
        }
            break;
        case 1:
        {
            HomeFuncCollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeFuncCollectionViewCell" forIndexPath:indexPath];

            
            WeakSelf(self)
            cell.clickBlock = ^(NSString * _Nonnull name) {
                StrongSelf(self)
                if ([name isEqualToString:@"优惠团购"]) {
                    [self.navigationController pushViewController:[XianShiTuanGouVC new] animated:true];
                }else if ([name isEqualToString:@"汽车维修"]){
                    ShopVC *vc = [ShopVC new];
                    [self.navigationController pushViewController:vc animated:true];
                }else if ([name isEqualToString:@"汽车配件"]){
                    HomeCommonVC *vc = [HomeCommonVC new];
                    vc.type = @"77";
                    [self.navigationController pushViewController:vc animated:true];
                }else if ([name isEqualToString:@"汽车精品"]){
                    HomeCommonVC *vc = [HomeCommonVC new];
                    vc.type = @"78";
                    [self.navigationController pushViewController:vc animated:true];
                }else if ([name isEqualToString:@"全车保修"]){
                    MyServicesOfYear *vc = [MyServicesOfYear new];
                    [self.navigationController pushViewController:vc animated:true];
                }else if ([name isEqualToString:@"一年保养"]){
                    CarMaintainceList *vc = [CarMaintainceList new];
                    [self.navigationController pushViewController:vc animated:true];
                }else if ([name isEqualToString:@"随心砍"]){
//                    CarMaintainceList *vc = [CarMaintainceList new];
//                    [self.navigationController pushViewController:vc animated:true];
                    BargainVC *vc = [BargainVC new];
                    [self.navigationController pushViewController:vc animated:true];
                }
            };
            return cell;
        }
            break;
            
        case 2:
        {
            HomeMarketingCollectionCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeMarketingCollectionCell" forIndexPath:indexPath];
//            cell.clickBlock = ^(NSInteger index) {
//
//
//            };
            return cell;
        }
            break;
        case 3:
        {
            HomeSectionOtherCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeSectionOtherCell" forIndexPath:indexPath];
            cell.clickBlock = ^(NSInteger index) {
                TuanGouGoodInfoVC *VC = [TuanGouGoodInfoVC new];
                NSDictionary *dic = self.groupArr[index];
                VC.goodsId = dic[@"id"];
                
                [self.navigationController pushViewController:VC animated:true];
           
              
            };
            cell.dataArr = self.groupArr;
            return cell;
        }
            break;
//        case 4:
//        {
//            HomeServiceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeServiceCell" forIndexPath:indexPath];
//            cell.clickBlock = ^(NSInteger index) {
//                ShopInfoVC *vc = [ShopInfoVC new];
//                NSDictionary *dic = self.carServiceArr[index];
//                vc.shopsId = dic[@"id"];
//                [self.navigationController pushViewController:vc animated:true];
//            };
//            cell.dataArr = self.carServiceArr;
//            return cell;
//        }
                break;
        case 4:
        {
            MallPeijianCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MallPeijianCell" forIndexPath:indexPath];
            cell.clickBlock = ^(NSInteger index) {
           
                GoodInfoVC *VC = [GoodInfoVC new];
                NSDictionary *dic = self.peijianArr[index];
                VC.goodsId = dic[@"id"];
                [self.navigationController pushViewController:VC animated:true];
                
            };
            
            cell.dataArr = self.peijianArr;
            return cell;
        }
            break;
        case 5:
        {
            MallPeijianCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MallPeijianCell" forIndexPath:indexPath];
            cell.clickBlock = ^(NSInteger index) {
                GoodInfoVC *VC = [GoodInfoVC new];
                NSDictionary *dic = self.cargoodsArr[index];
                VC.goodsId = dic[@"id"];
                [self.navigationController pushViewController:VC animated:true];
            };
            cell.dataArr = self.cargoodsArr;
            return cell;
        }
            break;
        case 6:
        {
            ShopSettleInCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShopSettleInCollectionCell" forIndexPath:indexPath];
            return cell;
        }
            break;
        default:
        {
            UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor blackColor];
            return cell;
        }
            break;
    }
    
    
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return CGSizeMake(Screen_Width, 270*AdapterScal);
            break;
        case 1:
            return CGSizeMake(Screen_Width, (Screen_Width/4)*2);
            break;
        case 2:
            return CGSizeMake(Screen_Width, 110);
        case 3:
            return CGSizeMake(Screen_Width,204);
            break;
//        case 4:
//            return CGSizeMake(Screen_Width,170*AdapterScal);
//            break;
        case 4:
            return CGSizeMake(Screen_Width, 180);
            break;
        case 6:
            return CGSizeMake(Screen_Width, 78*AdapterScal);
            break;
        default:
            return CGSizeMake(Screen_Width, 180);
            break;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    JLog(@"%ld",(long)indexPath.section);
    if (indexPath.section == 2) {
//        XianShiTuanGouVC *vc = [XianShiTuanGouVC new];
//        [self.navigationController pushViewController:vc animated:true];
        JLog(@"%ld-----",(long)index);
        BargainVC *vc = [BargainVC new];
        [self.navigationController pushViewController:vc animated:true];
    }else if (indexPath.section == 6){
        MerchantSettledVC *vc = [MerchantSettledVC new];
        [self.navigationController pushViewController:vc animated:true];
    }
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NetWorkBadView *view = [NetWorkBadView ViewClass];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 50, CGRectGetHeight(self.view.frame)/2-50, 100, 100);

    return view;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{
//    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
//    [manager stopMonitoring];
//    [manager startMonitoring];
    [self.collectionView reloadNetworkDataSet];
    [self getHomeData];
}
#pragma mark ----ApiEvent----
- (void)getHomeData{
    dispatch_group_t  group = dispatch_group_create();
    dispatch_queue_t que = dispatch_queue_create("gcd_global", DISPATCH_QUEUE_CONCURRENT);


    dispatch_group_async(group, que, ^{
        dispatch_group_enter(group);
      [self.homeApi getCarGoodsListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {

          if (result.code == 200) {
              self.cargoodsArr =result.result;
          }
          dispatch_group_leave(group);

      }];


    });

    dispatch_group_async(group, que, ^{
        dispatch_group_enter(group);
        [self.homeApi getMallListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {

            if (result.code == 200) {
                self.peijianArr =result.result;
            }
            dispatch_group_leave(group);
        }];

    });
    dispatch_group_async(group, que, ^{
        dispatch_group_enter(group);
        [self.homeApi getshopListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
            //        NSLog(@"%@",result.resultDic);
            NSDictionary *dic = result.resultDic;
            if (result.code == 200) {
                self.carServiceArr =dic[@"list"];
            }
            dispatch_group_leave(group);
        }];

    });
    dispatch_group_async(group, que, ^{
        dispatch_group_enter(group);
        [self.homeApi getbannerDataWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) { //        NSLog(@"轮播图数据%@",result.resultDic);
            NSDictionary *dic = result.resultDic;
            if (result.code == 200) {
                self.bannerArr =dic[@"list"];

            }
            dispatch_group_leave(group);
        }];
    });
    dispatch_group_async(group, que, ^{
        dispatch_group_enter(group);
        [self.homeApi getGroupListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {

            if (result.code == 200) {
                self.groupArr =result.result;
            }
            dispatch_group_leave(group);
        }];
    });
    dispatch_group_notify(group, que, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];

            [self.collectionView.mj_header endRefreshing];
        });

    });
    
//    NSOperationQueue *queue = [NSOperationQueue new];
//    queue.maxConcurrentOperationCount = 3;
//
//    __block TasksDownloaderOperation *op1 = [[TasksDownloaderOperation alloc] initWithTask:^{
//        [self.homeApi getbannerDataWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) { //        NSLog(@"轮播图数据%@",result.resultDic);
//
//            if (result.code == 200) {
//                self.bannerArr =result.result;
//
//            }
//
//            [op1 done];
//
//            JLog(@"任务1完成 --%lu",(unsigned long)self.bannerArr.count);
//
//        }];
//    }];
//
//
//   __block TasksDownloaderOperation *op2 = [[TasksDownloaderOperation alloc] initWithTask:^{
//        [self.homeApi getGroupListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
//
//
//            if (result.code == 200) {
//                self.groupArr =result.result;
//
//            }
//
//
//            [op2 done];
//           JLog(@"任务2完成 --%lu",(unsigned long)self.groupArr.count);
//        }];
//
//    } ];
//
//    __block TasksDownloaderOperation *op3 = [[TasksDownloaderOperation alloc] initWithTask:^{
//        [self.homeApi getCarGoodsListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
//
//
//            if (result.code == 200) {
//                self.cargoodsArr =result.result;
//
//            }
//
//
//            [op3 done];
//
//             JLog(@"任务3完成 --%lu",(unsigned long)self.cargoodsArr.count);
//        }];
//
//    }];
//
//    __block TasksDownloaderOperation *op4 = [[TasksDownloaderOperation alloc] initWithTask:^{
//        [self.homeApi getMallListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
//
//            if (result.code == 200) {
//                self.peijianArr =result.result;
//            }
//
//            [op4 done];
//        }];
//
//    }];
//
//
//   __block TasksDownloaderOperation *op5 = [[TasksDownloaderOperation alloc] initWithTask:^{
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            [self.collectionView reloadData];
//            [self.collectionView.mj_header endRefreshing];
//        }];
//
//        [op5 done];
//    }];
//
//    [op2 addDependency:op1];
//    [op3 addDependency:op2];
//    [op4 addDependency:op3];
//    [op5 addDependency:op4];
//
//    [queue addOperation:op1];
//    [queue addOperation:op2];
//    [queue addOperation:op3];
//    [queue addOperation:op4];
//    [queue addOperation:op5];





    
    
//    dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
//    dispatch_queue_t queue = dispatch_queue_create(0, DISPATCH_QUEUE_SERIAL);
//    dispatch_async(queue, ^{
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        NSLog(@"执行任务一");
//        JLog(@"任务一当前线程----%@",[NSThread currentThread]);
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            NSLog(@"网络任务一");
//            dispatch_semaphore_signal(semaphore);
//        });
//    });
//
//    dispatch_async(queue, ^{
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        NSLog(@"执行任务二");
//        JLog(@"任务二当前线程----%@",[NSThread currentThread]);
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            NSLog(@"网络任务二");
//            dispatch_semaphore_signal(semaphore);
//        });
//    });
//
//    dispatch_async(queue, ^{
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        NSLog(@"执行任务三");
//        JLog(@"任务三当前线程----%@",[NSThread currentThread]);
//        dispatch_semaphore_signal(semaphore);
//    });
//
//    dispatch_async(queue, ^{
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        NSLog(@"执行完成");
//        dispatch_semaphore_signal(semaphore);
//    });
}

- (void)getPasteBoardStr{
    NSString *shareStr = [UIPasteboard generalPasteboard].string;
    JLog(@"剪贴板字符串%@",shareStr);
    if (shareStr.length > 0) {
        NSDictionary *dic = [XJUtil convert2DictionaryWithJSONString:shareStr];
        NSInteger code = [dic[@"code"] integerValue];
        if (code == 1101) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"有一件待砍价商品等你去查看" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                if (!accessToken) {
//                    [XJUtil callUserLogin:self];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
                    return ;
                }
                
                HelpBarginGoodVC *secv = [[HelpBarginGoodVC alloc]init];
                
                secv.orderNum = dic[@"data"];
             
                [self.navigationController pushViewController:secv animated:YES];
                
            }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [UIPasteboard generalPasteboard].string = @"";
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
       
    }
    
}

- (void)getVersion{
    [self.homeApi getappVersionWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *array = result.result;
            NSDictionary *dateDic = array.firstObject;
            NSInteger statues = [dateDic[@"status"] integerValue];
            NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
            //如果手机当前版本小于服务器的版本号 提示更新
            if ([ nowSysVer compare:dateDic[@"versionCode"]] == NSOrderedAscending) {
                
                switch (statues) {
                    case 0:
                    {
//                         [self showUpdateAlert];
                    }
                        break;
                    case 1:
                    {
                        [self showUpdateAlert];
                    }
                        break;
                    case 2:
                    {
                        [self showUpdateAlert];
                    }
                        break;
                    default:
                        break;
                }
                
            }
            
        }
    }];
}

- (void)showUpdateAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"更新提示" message:@"有新版本了，请您更新APP后使用" preferredStyle:UIAlertControllerStyleAlert];
   
    UIAlertAction *act = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *name = @"https://apps.apple.com/cn/app/%E5%8D%A1%E9%80%9F%E8%BD%A6%E5%93%81%E5%95%86%E5%9F%8E/id1495565576";
        NSURL *url = [NSURL URLWithString:name];
       
        [[UIApplication sharedApplication]openURL:url];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }];
    [alert addAction:act];
    [alert addAction:cancel];
    [self presentViewController:alert animated:true completion:nil];
}

#pragma mark ---Lazy-----
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(340/2*AdapterScal, 140/2*AdapterScal);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        _collectionView.backgroundColor = UIColorF5F7;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView setEmptyDataSetSource:self];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        
        [_collectionView registerClass:[HomeReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeadView"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeadView2"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footView"];
        [_collectionView registerClass:[TuangouHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"TuangouHeadView"];
        
        [_collectionView registerClass:[HomeSectionOneCell class] forCellWithReuseIdentifier:@"HomeSectionOneCell"];
        [_collectionView registerClass:[HomeFuncCollectionViewCell class] forCellWithReuseIdentifier:@"HomeFuncCollectionViewCell"];
        [_collectionView registerClass:[HomeSectionOtherCell class] forCellWithReuseIdentifier:@"HomeSectionOtherCell"];
        [_collectionView registerClass:[MallPeijianCell class] forCellWithReuseIdentifier:@"MallPeijianCell"];
        [_collectionView registerClass:[HomeServiceCell class] forCellWithReuseIdentifier:@"HomeServiceCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"HomeMarketingCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"HomeMarketingCollectionCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"ShopSettleInCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"ShopSettleInCollectionCell"];
        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(getHomeData)];
        header.lastUpdatedTimeLabel.textColor = [UIColor whiteColor];
        header.stateLabel.textColor = [UIColor whiteColor];
        
        
//        WeakSelf(self);
        _collectionView.mj_header = header;
        
        
    }
    
    return _collectionView;
    
}



- (HomePageApi *)homeApi{
    if (!_homeApi) {
        _homeApi = [HomePageApi new];
    }
    
    return _homeApi;
}



- (UIButton *)scanBtn
{
    if (!_scanBtn) {
//        _scanBtn = [[UIBarButtonItem alloc] init];
        UIButton *_backBtn ;
        
        _backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"scan"] forState:UIControlStateNormal];
        _backBtn.contentMode = UIViewContentModeCenter;
        [_backBtn.titleLabel setHidden:YES];
        
        _backBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        _backBtn.frame=CGRectMake(-10, 0, 20, 40);
        
        
        _backBtn.contentMode = UIViewContentModeScaleAspectFit;
        [_backBtn addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
        
        
        _scanBtn = _backBtn;
        
//        _scanBtn.customView = _backBtn;
    }
    return _scanBtn;
}

- (UIBarButtonItem *)msgBtn
{
    if (!_msgBtn) {
        _msgBtn = [[UIBarButtonItem alloc] init];
        UIButton *_backBtn ;
        
        _backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"msg"] forState:UIControlStateNormal];
        _backBtn.contentMode = UIViewContentModeCenter;
        [_backBtn.titleLabel setHidden:YES];
        
        _backBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
//        _backBtn.frame=CGRectMake(-30, 0, 20, 40);
        
        
//        _backBtn.contentMode = UIViewContentModeScaleAspectFit;
        [_backBtn addTarget:self action:@selector(msg) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        _msgBtn.customView = _backBtn;
    }
    return _msgBtn;
}


-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[[UILabel  alloc] init];
        _titleLabel.text = @"温州市";
        _titleLabel.font = LabelFont14;
        _titleLabel.adjustsFontSizeToFitWidth = true;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.userInteractionEnabled = true;
        [_titleLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectCity)]];
        
    }
    return _titleLabel;
}


@end
