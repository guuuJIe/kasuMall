//
//  CarSubListVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarSubListVC : BaseViewController
@property (nonatomic) NSString *carTypeIds;
@property (nonatomic,copy) void(^clickBlock)(void);
@property (nonatomic,assign) NSInteger type;//1是从维修服务调过来 0是从首页跳进
@property (nonatomic,strong) NSString *carname;
@property (nonatomic,strong) NSString *vcname;
@end

NS_ASSUME_NONNULL_END
