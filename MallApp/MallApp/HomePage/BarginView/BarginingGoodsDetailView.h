//
//  BarginingGoodsDetailView.h
//  MallApp
//
//  Created by Mac on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BarginingGoodsDetailView : UIView
- (void)setupData:(NSDictionary *)dic withtype:(NSInteger)type;
@property (nonatomic,copy) void(^clickBlock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END
