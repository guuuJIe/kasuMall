//
//  RuleView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface RuleView : MMPopupView
- (void)setupdata:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
