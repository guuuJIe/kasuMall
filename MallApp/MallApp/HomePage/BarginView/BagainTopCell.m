//
//  BagainTopCell.m
//  MallApp
//
//  Created by Mac on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BagainTopCell.h"

@implementation BagainTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    UIImageView *headImage = [[UIImageView alloc] init];
    headImage.image = [UIImage imageNamed:@"B_top"];
    [self.contentView addSubview:headImage];
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    UIButton *ruleBtn = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - 54, 18, 54, 28)];
    [ruleBtn setTitle:@"活动规则" forState:0];
    [ruleBtn setTitleColor:[UIColor colorWithHexString:@"CB4026"] forState:0];
    [ruleBtn.titleLabel setFont:LabelFont11];
    [ruleBtn setBackgroundColor:[UIColor whiteColor]];
    ruleBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [ruleBtn addTarget:self action:@selector(clickAct) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:ruleBtn];

    ruleBtn.layer.mask = [XJUtil cutCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft ForView:ruleBtn withSize:CGSizeMake(10, 10)];
    
}


- (void)clickAct{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

@end
