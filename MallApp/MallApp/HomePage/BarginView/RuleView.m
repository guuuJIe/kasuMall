//
//  RuleView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "RuleView.h"
@interface RuleView()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *chaButton;
@property (nonatomic, strong) UILabel *ruleLabel;
@property (nonatomic, strong) UITextView *textview;
@end

@implementation RuleView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];

        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(29, Screen_Height/2 - 307/2, Screen_Width - 58, 289*AdapterScal)];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.cornerRadius = 5;

        
        UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, -1, Screen_Width - 58, 45)];
        [bgView addSubview:topView];
        topView.backgroundColor = [UIColor colorWithHexString:@"FFAB09"];
        topView.layer.mask = [XJUtil cutCorners:UIRectCornerTopLeft | UIRectCornerTopRight ForView:topView withSize:CGSizeMake(5, 5)];
        
        
        
        [bgView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(bgView);
            make.top.mas_equalTo(16);
        }];
        
        [self addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(bgView);
            make.bottom.equalTo(bgView.mas_top).offset(-15);
        }];
        
        [bgView addSubview:self.textview];
        [self.textview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(15);
            make.left.mas_equalTo(bgView).offset(15);
            make.right.mas_equalTo(-15);
            make.bottom.mas_equalTo(bgView).offset(-15);
        }];
               
    }
    return self;
}

- (void)dismissView{
    [self hide];
}

- (void)setupdata:(NSDictionary *)dic{
    if (dic) {
        self.textview.text = dic[@"activityRules"];
    }
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"活动规则";
        _titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
        _titleLabel.textColor = [UIColor whiteColor];
        
    }
    return _titleLabel;
}

-(UILabel *)ruleLabel
{
    if(!_ruleLabel)
    {
        _ruleLabel=[UILabel  new];
        
        _ruleLabel.font = LabelFont12;
        _ruleLabel.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        _ruleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _ruleLabel.numberOfLines = 0;
        _ruleLabel.text = @"1.请选择心仪的商品，邀请好友一起砍价，在规定时间内，砍掉多少就能优惠多少付款。\n2.必须在砍价过期前提供收货地址，没有收货地址砍到0元也是砍价失败，将无法为您发货。\n3.砍价活动需要好友共同参与，每次砍价以页面实际金额为主，参与好友越多，优惠越多。\n4.砍价活动最终解释权归卡速商城所有";
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.ruleLabel.text];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        [paragraphStyle setLineSpacing:10];
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.ruleLabel.text length])];
        
        self.ruleLabel.attributedText = attributedString;
      
        
    }
    return _ruleLabel;
}


-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
      
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}

- (UITextView *)textview{
    if(!_textview){
        _textview = [UITextView new];
    }
    return _textview;
}

@end
