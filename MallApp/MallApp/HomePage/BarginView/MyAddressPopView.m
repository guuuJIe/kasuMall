//
//  MyAddressPopView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyAddressPopView.h"
#import "AdresslistTableCell.h"
#import "OrderApi.h"
#import "AddressFootView.h"
@interface MyAddressPopView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIButton *chaButton;
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) OrderApi *api;
@property (nonatomic) NSArray *adressArr;

@end

@implementation MyAddressPopView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];

        UIView *bgView = [[UIView alloc] init];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(355+BottomAreaHeight);
        }];
        
        [bgView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.top.mas_equalTo(14);
        }];
        
        [bgView addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.titleLabel);
            make.right.mas_equalTo(-12);
        }];
        

        [bgView addSubview:self.purchaseBtn];
        [self.purchaseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(bgView);
            make.height.mas_equalTo(45);
            make.bottom.mas_equalTo(-BottomAreaHeight);
        }];
        
        [bgView addSubview:self.TabelView];
        [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(14);
            make.left.right.mas_equalTo(0);
            make.bottom.mas_equalTo(self.purchaseBtn.mas_top).offset(1);
        }];
     
//        [self setData];
    }
    return self;
}

- (void)dismissView{
    [self hide];
}

- (void)createOrder{
    if (self.purchaseBlock) {
        self.purchaseBlock();
    }
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"确认收货地址";
        _titleLabel.font = LabelFont16;
        _titleLabel.textColor = UIColor333;
        
    }
    return _titleLabel;
}

-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}

- (void)setData:(void (^)(NSDictionary *dic))finishBlock{
    [self.api getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        NSDictionary *dic = result.resultDic;
        NSArray *arr = dic[@"list"];
        self.adressArr = arr;
//        if (arr.count != 0) {
        if (finishBlock) {
            finishBlock(arr.firstObject);
        }
        //        }
       
        
        [self.TabelView reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.adressArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"AdresslistTableCell";
    AdresslistTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[AdresslistTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
    [cell setupData:self.adressArr[indexPath.row]];
     WeakSelf(self)
    cell.deleteBlock = ^{
        NSDictionary *dic = self.adressArr[indexPath.row];
        [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否删除该地址" andHandler:^(NSInteger index) {
            if (index == 1) {
                [self.api delAddressWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                    [weakself setData:^(NSDictionary * _Nonnull dic) {
                        
                    }];
                }];
            }
        }];
    };
    
    cell.setDefalutBlock = ^{
        NSDictionary *dic = self.adressArr[indexPath.row];
       
        
        [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否默认地址" andHandler:^(NSInteger index) {
            if (index == 1) {
                [self.api setDefaultAdressWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                    [weakself setData:^(NSDictionary * _Nonnull dic) {
                        
                    }];
                }];
            }
              }];
       
    };
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    AddressFootView *footview = [[AddressFootView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 45)];
    footview.clickBlock = ^{
        if (self.clickBlock) {
            self.clickBlock();
        }
    };
    return footview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 45;
}

#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"AdresslistTableCell" bundle:nil] forCellReuseIdentifier:@"AdresslistTableCell"];
        _TabelView.backgroundColor = UIColorEF;
        
    }
    return _TabelView;
}


- (UIButton *)purchaseBtn
{
    if (!_purchaseBtn) {
        _purchaseBtn = [[UIButton alloc] init];
        [_purchaseBtn setTitle:@"已选该产品，开始免费拿" forState:0];
        [_purchaseBtn setTitleColor:[UIColor whiteColor] forState:0];
        [_purchaseBtn.titleLabel setFont:LabelFont14];
        [_purchaseBtn setBackgroundColor:APPColor];
        [_purchaseBtn addTarget:self action:@selector(createOrder) forControlEvents:UIControlEventTouchUpInside];
    }
    return _purchaseBtn;
}

- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}


@end
