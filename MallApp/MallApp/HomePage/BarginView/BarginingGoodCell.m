//
//  BarginingGoodCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BarginingGoodCell.h"
#import "ProgressBarView.h"
#import "UIButton+Gradient.h"
#import "CountDown.h"
@interface BarginingGoodCell()

@property (nonatomic, strong) UIImageView *goodsImage;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) ProgressBarView *barView;
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) UILabel *timeLbl;
@property (nonatomic, strong) UILabel *dayLbl;
@property (nonatomic, strong) UILabel *hourLbl;
@property (nonatomic, strong) UILabel *minLbl;
@property (nonatomic, strong) CountDown *countDown;
@property (nonatomic, assign)int nowTime;
@property (nonatomic, strong) UILabel *endLbl;
@end

@implementation BarginingGoodCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = UIColorF5F7;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bgView];
    bgView.layer.cornerRadius = 5;
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(134);
//        make.bottom.mas_equalTo(self.contentView);
    }];
    
    [bgView addSubview:self.goodsImage];
    [self.goodsImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(125*AdapterScal, 125*AdapterScal));
        make.left.mas_equalTo(14);
        make.centerY.mas_equalTo(bgView);
    }];
    
    
    
    [bgView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.goodsImage);
        make.right.mas_equalTo(self.goodsImage);
        make.width.mas_equalTo(self.goodsImage);
        make.bottom.mas_equalTo(self.goodsImage).offset(-5);
    }];
    
    [bgView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.goodsImage.mas_right).offset(10);
        make.right.mas_equalTo(bgView).offset(-10);
        make.top.mas_equalTo(self.goodsImage);
       
    }];
    
    [bgView addSubview:self.barView];
    [self.barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(12);
        make.size.mas_equalTo(CGSizeMake(185, 10));
    }];
    
    UIButton *four = [UIButton new];
    [four setTitle:@"优惠购买" forState:UIControlStateNormal];
    [four setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    four.layer.cornerRadius = 3;
    four.layer.masksToBounds = true;
    four.backgroundColor = [UIColor colorWithHexString:@"5497FB"];
    [self addSubview:four];
    [four.titleLabel setFont:LabelFont14];
    four.userInteractionEnabled = false;
    [four mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(94*AdapterScal, 30*AdapterScal));
        make.left.mas_equalTo(self.titleLabel.mas_left).offset(0);
        make.bottom.mas_equalTo(self.goodsImage);
    }];
    

    UIButton *five = [UIButton new];
    [five setTitle:@"继续砍价" forState:UIControlStateNormal];
    [five setTitleColor:[UIColor colorWithHexString:@"F9472A"] forState:UIControlStateNormal];
    five.layer.cornerRadius = 3;
    five.layer.masksToBounds = true;
    [bgView addSubview:five];
    five.userInteractionEnabled = false;
    [five.titleLabel setFont:LabelFont14];
    [five gradientButtonWithSize:CGSizeMake(94*AdapterScal, 30*AdapterScal) colorArray:@[(id)[UIColor colorWithHexString:@"FFEA00"],(id)[UIColor colorWithHexString:@"FFAC28"]] percentageArray:@[@(0.18),@(1)] gradientType:GradientFromTopToBottom];
    [five mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(94*AdapterScal, 30*AdapterScal));
        make.left.mas_equalTo(four.mas_right).offset(7);
        make.bottom.mas_equalTo(self.goodsImage);
    }];
    
    
   
    
    [bgView addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(bgView);
        make.bottom.mas_equalTo(four.mas_top).offset(-5);
    }];
    
    [bgView addSubview:self.dayLbl];
    [self.dayLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.size.mas_equalTo(CGSizeMake(20*AdapterScal, 17*AdapterScal));
         make.bottom.mas_equalTo(four.mas_top).offset(-5);
    }];
    
    UILabel *dot = [UILabel new];
    dot.text = @"天";
    dot.font = LabelFont12;
    [bgView addSubview:dot];
    [dot mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(self.dayLbl.mas_right).offset(4);
        make.centerY.mas_equalTo(self.dayLbl);
    }];
    
    
    [bgView addSubview:self.hourLbl];
    [self.hourLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dot.mas_right).offset(4);
        make.size.mas_equalTo(CGSizeMake(20*AdapterScal, 17*AdapterScal));
        make.bottom.mas_equalTo(four.mas_top).offset(-5);
    }];
    
    UILabel *dot2 = [UILabel new];
    dot2.text = @"时";
    dot2.font = LabelFont12;
    [bgView addSubview:dot2];
    [dot2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.hourLbl.mas_right).offset(4);
        make.centerY.mas_equalTo(self.hourLbl);
    }];
    
    [bgView addSubview:self.minLbl];
    [self.minLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dot2.mas_right).offset(4);
        make.size.mas_equalTo(CGSizeMake(20*AdapterScal, 17*AdapterScal));
        make.bottom.mas_equalTo(four.mas_top).offset(-5);
    }];
    
    [bgView addSubview:self.endLbl];
    [self.endLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.minLbl.mas_right).offset(4);
//        make.size.mas_equalTo(CGSizeMake(20*AdapterScal, 17*AdapterScal));
        make.bottom.mas_equalTo(four.mas_top).offset(-5);
    }];
    
    UIView *bottomView = [UIView new];
    bottomView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:bottomView];
    
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
        make.top.mas_equalTo(bgView.mas_bottom);
    }];
}


- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
        [self.goodsImage yy_setImageWithURL:URL(url) options:0];
        self.nameLbl.text = dic[@"title"];
        NSString *price = [NSString stringWithFormat:@"%.2f",[dic[@"price"] floatValue]];
        NSString *bargainPrice = [NSString stringWithFormat:@"%.2f",[dic[@"bargainPrice"] floatValue]];
        self.titleLabel.text = [NSString stringWithFormat:@"商品价格%.2f元,已砍%.2f元",[dic[@"price"] floatValue],[dic[@"bargainPrice"] floatValue]];
        NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];
        [aStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FC3018"] range:NSMakeRange(4, price.length)];
        [aStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FC3018"] range:NSMakeRange(8+price.length, bargainPrice.length)];
        self.titleLabel.attributedText = aStr;
        
        CGFloat value = ([dic[@"bargainPrice"] floatValue])/[dic[@"price"] floatValue];
        NSString *title = @"%";
        self.barView.numLabel.text = [NSString stringWithFormat:@"%.2f%@",value*100,title];
        self.barView.gradientView.frame = CGRectMake(0, 0, 185*AdapterScal*value, 10*AdapterScal);
        
        
        NSString *limittime = [NSString stringWithFormat:@"%ld",(long)[XJUtil timeSwitchTimestamp:dic[@"expireTime"]]];
        NSString *nowtime = [XJUtil getNowTimeStamp];
        self.nowTime = [limittime intValue] - [nowtime intValue];
        WeakSelf(self)
        [self.countDown countDownWithStratTimeStamp:self.nowTime  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
           
            weakself.dayLbl.text = [NSString stringWithFormat:@"%li",(long)day];
            weakself.hourLbl.text = [NSString stringWithFormat:@"%li",(long)hour];
            weakself.minLbl.text = [NSString stringWithFormat:@"%li",(long)minute];
            
            //15021912016  83319882
        }];
    }
}

- (CountDown *)countDown{
    if (!_countDown) {
        _countDown = [CountDown new];
    }
    
    return _countDown;
}


- (UIImageView *)goodsImage{
    if (!_goodsImage) {
        _goodsImage = [UIImageView new];
        _goodsImage.image = [UIImage imageNamed:@""];
        _goodsImage.backgroundColor = APPColor;
    }
    
    return _goodsImage;
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"价格";
        _titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightBold];
        _titleLabel.textColor = UIColor333;
        _titleLabel.numberOfLines = 1;
        
    }
    return _titleLabel;
}

-(UILabel *)dayLbl
{
    if(!_dayLbl)
    {
        _dayLbl=[[UILabel  alloc] initWithFrame:CGRectMake(0, 0, 20*AdapterScal, 17*AdapterScal)];
       
        _dayLbl.font = [UIFont systemFontOfSize:13 weight:UIFontWeightBold];
        _dayLbl.textColor = [UIColor whiteColor];
        _dayLbl.backgroundColor = [UIColor colorWithHexString:@"58595B"];
        _dayLbl.textAlignment = NSTextAlignmentCenter;
        _dayLbl.layer.cornerRadius = 3;
        _dayLbl.layer.masksToBounds = true;
        
    }
    return _dayLbl;
}

-(UILabel *)hourLbl
{
    if(!_hourLbl)
    {
        _hourLbl=[[UILabel  alloc] initWithFrame:CGRectMake(0, 0, 20*AdapterScal, 17*AdapterScal)];
       
        _hourLbl.font = [UIFont systemFontOfSize:13 weight:UIFontWeightBold];
        _hourLbl.textColor = [UIColor whiteColor];
        _hourLbl.backgroundColor = [UIColor colorWithHexString:@"58595B"];
        _hourLbl.textAlignment = NSTextAlignmentCenter;
        _hourLbl.layer.cornerRadius = 3;
        _hourLbl.layer.masksToBounds = true;
        
    }
    return _hourLbl;
}

-(UILabel *)endLbl
{
    if(!_endLbl)
    {
        _endLbl=[[UILabel  alloc] init];
       
        _endLbl.font = [UIFont systemFontOfSize:12];
        _endLbl.textColor = [UIColor colorWithHexString:@"353537"];
        _endLbl.text = @"分后结束";
        _endLbl.textAlignment = NSTextAlignmentCenter;
         _endLbl.layer.masksToBounds = true;
        
    }
    return _endLbl;
}

-(UILabel *)minLbl
{
    if(!_minLbl)
    {
        _minLbl=[[UILabel  alloc] initWithFrame:CGRectMake(0, 0, 20*AdapterScal, 17*AdapterScal)];
       
        _minLbl.font = [UIFont systemFontOfSize:13 weight:UIFontWeightBold];
        _minLbl.textColor = [UIColor whiteColor];
        _minLbl.backgroundColor = [UIColor colorWithHexString:@"58595B"];
        _minLbl.textAlignment = NSTextAlignmentCenter;
        _minLbl.layer.cornerRadius = 3;
        _minLbl.layer.masksToBounds = true;
        
    }
    return _minLbl;
}

-(UILabel *)nameLbl
{
    if(!_nameLbl)
    {
        _nameLbl=[UILabel  new];
        _nameLbl.text = @"商品价格";
        _nameLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
        _nameLbl.textColor = UIColor333;
        _nameLbl.numberOfLines = 1;
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _nameLbl;
}

-(UILabel *)timeLbl
{
    if(!_timeLbl)
    {
        _timeLbl=[UILabel  new];
        _timeLbl.text = @"";
        _timeLbl.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
        _timeLbl.textColor = UIColor333;
        _timeLbl.numberOfLines = 1;
        _timeLbl.textAlignment = NSTextAlignmentLeft;
        
    }
    return _timeLbl;
}




- (ProgressBarView *)barView{
    if (!_barView) {
        _barView = [[ProgressBarView alloc] initWithFrame:CGRectMake(0, 0, 185*AdapterScal, 10*AdapterScal)];
        _barView.layer.cornerRadius = 5;
        _barView.bgView.backgroundColor = [UIColor colorWithHexString:@"E7E7E7"];
        _barView.bgView.layer.cornerRadius = 5;
        _barView.gradientView.backgroundColor = [UIColor colorWithHexString:@"FFD926"];
        _barView.gradientView.layer.cornerRadius = 5;
        [self.contentView addSubview:_barView];
    }
    return _barView;
}

@end
