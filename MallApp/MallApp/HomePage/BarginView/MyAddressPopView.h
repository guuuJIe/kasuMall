//
//  MyAddressPopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN
//typedef void(^finishHandler)(void);
@interface MyAddressPopView : MMPopupView
@property (nonatomic,copy) void(^clickBlock)(void);
@property (nonatomic,copy) void(^purchaseBlock)(void);
- (void)setData:(void (^)(NSDictionary *dic))finishBlock;
@property (nonatomic) UIButton *purchaseBtn;
@end

NS_ASSUME_NONNULL_END
