//
//  BarginSectionOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BarginSectionOneCell.h"
@interface BarginSectionOneCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end

@implementation BarginSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithCoder:(NSCoder *)coder{
    self = [super initWithCoder:coder];
    if (self) {
        [self changeUI];
    }
    
    return self;
}

- (void)changeUI{
//    UILabel *label = [[UILabel alloc] init];
//    label.frame = CGRectMake(79,97,223.5,44);
//    label.numberOfLines = 0;
//    [self.view addSubview:label];

    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowBlurRadius = 14;
    shadow.shadowColor = [UIColor colorWithRed:236/255.0 green:48/255.0 blue:26/255.0 alpha:0.55];
    shadow.shadowOffset = CGSizeMake(0,1);

    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"砍价免费拿" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"AmericanTypewriter-Bold" size: 47.3],NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0], NSShadowAttributeName: shadow}];

    self.titleLbl.attributedText = string;
}

@end
