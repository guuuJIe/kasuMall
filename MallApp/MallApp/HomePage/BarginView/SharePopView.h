//
//  SharePopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SharePopView : MMPopupView
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
