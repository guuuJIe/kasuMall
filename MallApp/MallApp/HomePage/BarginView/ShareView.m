//
//  ShareView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShareView.h"
#import "FuncItemCollectionViewCell.h"
#import "WXApi.h"
@interface ShareView()<UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate>
@property (nonatomic) UICollectionView *colletionView;
@property (nonatomic) NSMutableArray *dataArr;
@end

@implementation ShareView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        self.userInteractionEnabled = true;
//
        
        UIView *bgView = [[UIView alloc] init];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.userInteractionEnabled = true;
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(90+BottomAreaHeight);
        }];
        
        [bgView addSubview:self.colletionView];
        [self.colletionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(bgView);
            make.height.mas_equalTo(90);
        }];
        
        if ([WXApi isWXAppInstalled]) {
            self.colletionView.hidden = false;
        }else{
            self.colletionView.hidden = true;
        }
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideBg)];
        tapGes.delegate = self;
        
        [self addGestureRecognizer:tapGes];
    }
    
    return self;
}

- (void)hideBg{
    [self hide];
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FuncItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FuncItemCollectionViewCell" forIndexPath:indexPath];
//    cell.model = self.old_wine[indexPath.row];
//    cell.backgroundColor = RandomColor;
    [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.clickBlock) {
        self.clickBlock(indexPath.row);
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
 
    if ([touch.view isDescendantOfView:self.colletionView]) {
        return NO;
    }
    
    return YES;

}



- (UICollectionView *)colletionView
{
    if (!_colletionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(Screen_Width/4, 90);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _colletionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _colletionView.showsVerticalScrollIndicator = NO;
        _colletionView.showsHorizontalScrollIndicator = NO;
        _colletionView.backgroundColor = [UIColor whiteColor];
        _colletionView.delegate = self;
        _colletionView.dataSource = self;
//        _colletionView.userInteractionEnabled = true;
        [_colletionView registerClass:[FuncItemCollectionViewCell class] forCellWithReuseIdentifier:@"FuncItemCollectionViewCell"];
    }
    return _colletionView;
}



- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        
        _dataArr = [NSMutableArray array];
        NSArray *arr = [NSArray arrayWithObjects:
  @{@"name":@"微信好友",@"image":@"weixin"},@{@"name":@"朋友圈",@"image":@"Moment"}, nil];
        [_dataArr addObjectsFromArray:arr];
    }
    return _dataArr;
}
@end
