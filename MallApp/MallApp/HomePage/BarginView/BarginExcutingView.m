//
//  BarginExcutingView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BarginExcutingView.h"

@implementation BarginExcutingView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorF5F7;
        [self createUI];
    }
    
    return self;
}

- (void)createUI{
    UIImageView *head = [UIImageView new];
    head.image = [UIImage imageNamed:@"B_icon_1"];
    [self addSubview:head];
    self.headImage = head;
    [head mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self);
    }];
}

@end
