//
//  BarginRecordHeadView.m
//  MallApp
//
//  Created by Mac on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BarginRecordHeadView.h"

@implementation BarginRecordHeadView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"FF5F28"];
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIView *recordWhiteBg = [UIView new];
    recordWhiteBg.backgroundColor = [UIColor whiteColor];
    [self addSubview:recordWhiteBg];
    recordWhiteBg.layer.cornerRadius = 8;
    [recordWhiteBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(10);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(45);
    }];
    
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 24, 45)];
    [recordWhiteBg addSubview:topView];
    topView.backgroundColor = [UIColor colorWithHexString:@"FEEBC3"];
    topView.layer.mask = [XJUtil cutCorners:UIRectCornerTopLeft | UIRectCornerTopRight ForView:topView withSize:CGSizeMake(8, 8)];
    
    UILabel *title = [UILabel new];
    title.text = @"砍价记录";
    [topView addSubview:title];
    title.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
    title.textColor = [UIColor colorWithHexString:@"E02A21"];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(topView);
    }];
}

@end
