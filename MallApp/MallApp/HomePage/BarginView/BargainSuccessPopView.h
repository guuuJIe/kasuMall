//
//  BargainSuccessPopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BargainSuccessPopView : MMPopupView
@property (nonatomic,copy) void(^clickBlock)(void);
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
