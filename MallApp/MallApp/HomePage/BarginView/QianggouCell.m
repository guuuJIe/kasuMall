//
//  QianggouCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "QianggouCell.h"
#import "ProgressBarView.h"
@interface QianggouCell()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *purchaseBtn;

@property (weak, nonatomic) IBOutlet ProgressBarView *progressView;
@property (nonatomic, strong) ProgressBarView *barView;
@property (weak, nonatomic) IBOutlet UILabel *goodsnameLbl;
@property (weak, nonatomic) IBOutlet UIButton *cutBtn;
@property (weak, nonatomic) IBOutlet UIView *yellowView;
@end

@implementation QianggouCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    self.progressView.frame = CGRectMake(CGRectGetMaxX(self.goodsImage.frame)+10, CGRectGetMaxY(self.priceLbl.frame)+15, 200 , 22);
//    self.barView.bgView.backgroundColor = [UIColor colorWithHexString:@"E7E7E7"];
//    [self.barView.gradientView setBackgroundColor:[UIColor colorWithHexString:@"FFD926"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
//        self.barView.frame = CGRectMake(0, 0, 100, 20);
       
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
        [self.goodsImage yy_setImageWithURL:URL(url) options:0];
        self.goodsnameLbl.text = dic[@"title"];
        self.titleLbl.text = [NSString stringWithFormat:@"商品价格%.2f元,已砍%.2f元",[dic[@"price"] floatValue],[dic[@"bargainPrice"] floatValue]];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
        self.yellowView.ag_width = 20;
    }
}

- (ProgressBarView *)barView{
    if (!_barView) {
        _barView = [[ProgressBarView alloc] initWithFrame:CGRectMake(0, 0, 100, 13)];
        _barView.layer.cornerRadius = 8;
        [self.contentView addSubview:_barView];
    }
    return _barView;
}

@end
