//
//  FootView.m
//  MallApp
//
//  Created by Mac on 2020/3/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FootView.h"

@implementation FootView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(12, 0, Screen_Width - 24, self.frame.size.height)];
    [self addSubview:view];
    view.backgroundColor = [UIColor whiteColor];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

@end
