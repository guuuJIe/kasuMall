//
//  BargainGoodView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BargainGoodView.h"
#import "CountDown.h"
@interface BargainGoodView()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (nonatomic, strong) CountDown *countDown;
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;
@property (weak, nonatomic) IBOutlet UILabel *hourLbl;
@property (weak, nonatomic) IBOutlet UILabel *minLbl;
@property(nonatomic,assign)int nowTime;
@end

@implementation BargainGoodView

+(instancetype)initViewClass{
    return [[[NSBundle mainBundle] loadNibNamed:@"BargainGoodView" owner:self options:nil] lastObject];
}

- (void)setupData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
    [self.goodsImage yy_setImageWithURL:URL(url) options:0];
    self.titleLbl.text = dic[@"title"];
    self.subTitleLbl.text = [NSString stringWithFormat:@"%@",dic[@"sTitle"]];
    NSString *limittime = [NSString stringWithFormat:@"%ld",(long)[XJUtil timeSwitchTimestamp:dic[@"expireTime"]]];
    NSString *nowtime = [XJUtil getNowTimeStamp];
    self.nowTime = [limittime intValue] - [nowtime intValue];
    WeakSelf(self)
    [self.countDown countDownWithStratTimeStamp:self.nowTime  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        //        NSLog(@"%ld",(long)day);
        weakself.timeLbl.text=[NSString stringWithFormat:@"%li天%li时%li分后砍价结束",(long)day,(long)hour,(long)minute];
        //        [self refreshUIDay:day hour:hour minute:minute second:second];
        self.dayLbl.text = [NSString stringWithFormat:@"%li",day];
        self.hourLbl.text = [NSString stringWithFormat:@"%li",hour];
        self.minLbl.text = [NSString stringWithFormat:@"%li",minute];
    }];
//    self.timeLbl.text = [NSString stringWithFormat:@"已送出%@件",[NSString stringWithFormat:@"%@",dic[@"saleQuantity"]]];
}

- (CountDown *)countDown{
    if (!_countDown) {
        _countDown = [CountDown new];
    }
    
    return _countDown;
}

@end
