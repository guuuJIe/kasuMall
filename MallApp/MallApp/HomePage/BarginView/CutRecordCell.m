//
//  CutRecordCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CutRecordCell.h"
@interface CutRecordCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@end

@implementation CutRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = dic[@"userName"];
        CGFloat amount = [dic[@"amount"] floatValue];
        self.priceLbl.text = [NSString stringWithFormat:@"砍掉%.2f元",amount];
    }
}

@end
