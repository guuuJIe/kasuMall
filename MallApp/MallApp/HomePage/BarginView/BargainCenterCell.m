//
//  BargainCenterCell.m
//  MallApp
//
//  Created by Mac on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BargainCenterCell.h"
#import "BarginingGoodsDetailView.h"
@interface BargainCenterCell()
@property (strong, nonatomic) BarginingGoodsDetailView *goodsView;
@end
@implementation BargainCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"FF5F28"];
        [self createUI];
    }
    
    return self;
}

- (void)createUI{
    [self.contentView addSubview:self.goodsView];

    [self.goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(self.contentView).offset(0);
    }];
    
   
 
    
//    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(self.goodsView);
//    }];
    

}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        [self.goodsView setupData:dic withtype:0];
        
//        [self.goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.mas_equalTo(self);
//        }];
    }
    
}


- (BarginingGoodsDetailView *)goodsView{
    if (!_goodsView) {
        _goodsView = [BarginingGoodsDetailView new];
         WeakSelf(self)
        _goodsView.clickBlock = ^(NSInteger index) {
            if (weakself.clickBlock) {
                weakself.clickBlock(index);
            }
        };
    }
    return _goodsView;
}
@end
