//
//  AddressFootView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddressFootView.h"

@implementation AddressFootView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorEF;
        UIButton *btn = [UIButton new];
        [btn setTitle:@"添加新地址" forState:0];
        [btn setTitleColor:APPColor forState:0];
        [btn.titleLabel setFont:LabelFont12];
        [self addSubview:btn];
        [btn addTarget:self action:@selector(addAddress) forControlEvents:UIControlEventTouchUpInside];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.right.mas_equalTo(-12);
        }];
    }
    return self;
}

- (void)addAddress{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

@end
