//
//  BarginingGoodsDetailView.m
//  MallApp
//
//  Created by Mac on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BarginingGoodsDetailView.h"
#import "BargainGoodView.h"
#import "CountDown.h"
#import "UIButton+Gradient.h"
#import "ProgressBarView.h"
@interface BarginingGoodsDetailView()
@property (nonatomic, strong) UIImageView *headImage;
@property (nonatomic, strong) BargainGoodView *goodsView;
@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) CountDown *countDown;
@property (nonatomic, assign) int nowTime;
@property (nonatomic, strong) UIButton *purchaseBtn;
@property (nonatomic, strong) UIButton *cutBtn;
@property (nonatomic, strong) UILabel *userNameLbl;
@property (nonatomic, strong) UILabel *countLbl;
@property (nonatomic, strong) ProgressBarView *barView;

@property (nonatomic, strong) UILabel *dayLbl;
@property (nonatomic, strong) UILabel *hourLbl;
@property (nonatomic, strong) UILabel *minLbl;
@property (nonatomic, strong) UILabel *endLbl;
@end

@implementation BarginingGoodsDetailView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 8;
        self.userInteractionEnabled = true;
        [self setupUI];
        
    }
    
    return self;
}

- (void)setupUI{
    
    self.headImage = [UIImageView new];
    [self addSubview:self.headImage];
    self.headImage.image = [UIImage imageNamed:@"defaultHead"];
    [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20*AdapterScal);
        make.left.mas_equalTo(12);
        make.size.mas_equalTo(CGSizeMake(37, 37));
    }];
    
    [self addSubview:self.userNameLbl];
    [self.userNameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImage);
        make.left.mas_equalTo(self.headImage.mas_right).offset(10);
    }];
    
        
    UILabel *sub  = [UILabel new];
    sub.text = @"“我发现一件好货，能免费拿哦~”";
    sub.textColor = [UIColor colorWithHexString:@"141414"];
    sub.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    [self addSubview:sub];
    [sub mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userNameLbl);
        make.top.mas_equalTo(self.userNameLbl.mas_bottom).offset(5*AdapterScal);
    }];
        
    self.goodsView = [BargainGoodView initViewClass];
    [self addSubview:self.goodsView];//89223406
    [self.goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.headImage.mas_bottom).offset(12*AdapterScal);
        
    }];
    
    [self addSubview:self.priceLbl];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsView.mas_bottom).offset(16*AdapterScal);
        make.centerX.mas_equalTo(self);
    }];
    
    [self addSubview:self.barView];
    [self.barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(12*AdapterScal);
        make.size.mas_equalTo(CGSizeMake(Screen_Width - 60, 10));
    }];
    

    
    UIButton *four = [UIButton new];
    [four setTitle:@"立即购买" forState:UIControlStateNormal];
    [four setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [four setImage:[UIImage imageNamed:@"right"] forState:UIControlStateNormal];
    four.layer.cornerRadius = 28*AdapterScal;
    four.layer.masksToBounds = true;
//    four.backgroundColor = [UIColor redColor];
    four.userInteractionEnabled = true;
    
    [four gradientButtonWithSize:CGSizeMake(Screen_Width/2 - 24 - 13, 55*AdapterScal) colorArray:@[(id)[UIColor colorWithHexString:@"97C1FF"],(id)[UIColor colorWithHexString:@"5497FB"]] percentageArray:@[@(0.18),@(1)] gradientType:GradientFromTopToBottom];
    [self addSubview:four];
    [four.titleLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightBold]];
    [four addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [four mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@12);
        make.size.mas_equalTo(CGSizeMake(Screen_Width/2 - 24 - 13, 55*AdapterScal));
        make.top.mas_equalTo(self.barView.mas_bottom).offset(12*AdapterScal);
    }];
    four.tag = 200;
    self.purchaseBtn = four;
    
    UIButton *five = [UIButton buttonWithType:UIButtonTypeCustom];
    [five setTitle:@"找人砍价" forState:UIControlStateNormal];
    [five setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [five setImage:[UIImage imageNamed:@"right"] forState:UIControlStateNormal];
    five.layer.cornerRadius = 28*AdapterScal;
    five.layer.masksToBounds = true;
    [five setTitleColor:[UIColor colorWithHexString:@"F9472A"] forState:0];
    [five.titleLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightBold]];
    [five gradientButtonWithSize:CGSizeMake(Screen_Width/2 - 24 - 13, 55*AdapterScal) colorArray:@[(id)[UIColor colorWithHexString:@"FFF000"],(id)[UIColor colorWithHexString:@"FFAC28"]] percentageArray:@[@(0.18),@(1)] gradientType:GradientFromTopToBottom];
    [five addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    five.tag = 201;
    [self addSubview:five];
    [five mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.size.mas_equalTo(CGSizeMake(Screen_Width/2 - 24 - 13, 55*AdapterScal));
        make.top.mas_equalTo(self.barView.mas_bottom).offset(12*AdapterScal);
    }];
    self.cutBtn = five;
    
    [self addSubview:self.countLbl];
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(five.mas_bottom).offset(12*AdapterScal);
        make.bottom.mas_equalTo(self).offset(-15*AdapterScal);
    }];
    
    
}

- (void)setupData:(NSDictionary *)dic withtype:(NSInteger)type{
    self.userNameLbl.text = dic[@"orderUserName"];
    [self.goodsView setupData:dic];
    NSString *limittime = [NSString stringWithFormat:@"%ld",(long)[XJUtil timeSwitchTimestamp:dic[@"expireTime"]]];
    NSString *nowtime = [XJUtil getNowTimeStamp];
    self.nowTime = [limittime intValue] - [nowtime intValue];
    WeakSelf(self)
    [self.countDown countDownWithStratTimeStamp:self.nowTime  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        
        weakself.countLbl.text=[NSString stringWithFormat:@"还剩%li天%li时%li分后砍价结束",(long)day,(long)hour,(long)minute];
        
    }];
    self.priceLbl.text = [NSString stringWithFormat:@"商品价格%@元，已砍%@元",dic[@"price"],dic[@"bargainPrice"]];
    
    CGFloat value = ([dic[@"bargainPrice"] floatValue])/[dic[@"price"] floatValue];
    NSString *title = @"%";
    self.barView.numLabel.text = [NSString stringWithFormat:@"%.2f%@",value*100,title];
    self.barView.gradientView.frame = CGRectMake(0, 0, (Screen_Width - 60)*value, 10*AdapterScal);
    if (type == 1) {
        [self.purchaseBtn setTitle:@"我也要免费拿" forState:0];
        [self.cutBtn setTitle:@"帮好友砍一刀" forState:0];
    }
}

- (void)btnClick:(UIButton *)button{
    if (self.clickBlock) {
        self.clickBlock(button.tag);
    }
}


- (UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.text = @"";
        _priceLbl.textColor = [UIColor colorWithHexString:@"5C330E"];
        _priceLbl.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    }
    return _priceLbl;
}

- (UILabel *)userNameLbl{
    if (!_userNameLbl) {
        _userNameLbl = [UILabel new];
        _userNameLbl.text = @"";
        _userNameLbl.textColor = UIColor66;
        _userNameLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    }
    return _userNameLbl;
}

- (UILabel *)countLbl{
    if (!_countLbl) {
        _countLbl = [UILabel new];
        _countLbl.text = @"还剩";
        _countLbl.textColor = [UIColor colorWithHexString:@"58585A"];
        _countLbl.font = [UIFont systemFontOfSize:14];
    }
    return _countLbl;
}

- (CountDown *)countDown{
    if (!_countDown) {
        _countDown = [CountDown new];
    }
    
    return _countDown;
}


- (ProgressBarView *)barView{
    if (!_barView) {
        _barView = [[ProgressBarView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 60, 10*AdapterScal)];
        _barView.layer.cornerRadius = 5;
        _barView.bgView.backgroundColor = [UIColor colorWithHexString:@"E7E7E7"];
        _barView.bgView.layer.cornerRadius = 5;
        _barView.gradientView.backgroundColor = [UIColor colorWithHexString:@"FFD926"];
        _barView.gradientView.layer.cornerRadius = 5;
        [self addSubview:_barView];
    }
    return _barView;
}




@end
