//
//  SharePopView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SharePopView.h"
#import "UIButton+Gradient.h"
@interface SharePopView()
@property (nonatomic) UIButton *chaButton;
@end

@implementation SharePopView
- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        
        UIView *bgView = [[UIView alloc] init];
        [self addSubview:bgView];
        bgView.layer.cornerRadius = 5;
        bgView.layer.masksToBounds = true;
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(40);
            make.right.mas_equalTo(-40);
            make.center.mas_equalTo(self);
            make.height.mas_equalTo(274);
        }];
        
        
        [self addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(bgView);
            make.bottom.equalTo(bgView.mas_top).offset(-15);
        }];
        
        
        UIImageView *codeimage = [UIImageView new];
        codeimage.image = [UIImage imageNamed:@"B_code"];
        [bgView addSubview:codeimage];
        [codeimage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.top.mas_equalTo(24);
        }];
        
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"去微信粘贴给好友";
        label.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
        [bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(codeimage.mas_bottom).offset(18);
            make.centerX.mas_equalTo(codeimage);
        }];
        
        UILabel *label2 = [[UILabel alloc] init];
        label2.text = @"好友帮砍后可砍大金额";
        label2.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
        [bgView addSubview:label2];
        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(label.mas_bottom).offset(5);
            make.centerX.mas_equalTo(codeimage);
        }];
        
        NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:label2.text];
        [aStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FC3018"] range:NSMakeRange(6, 4)];
        label2.attributedText = aStr;
        
        
        UIButton *four = [UIButton new];
        [four setTitle:@"去微信粘贴" forState:UIControlStateNormal];
        [four setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [four setImage:[UIImage imageNamed:@"B_wechat"] forState:UIControlStateNormal];
                four.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
        four.layer.cornerRadius = 5;
        four.layer.masksToBounds = true;
        [four gradientButtonWithSize:CGSizeMake(257*AdapterScal, 45*AdapterScal) colorArray:@[(id)[UIColor colorWithHexString:@"6FE433"],(id)[UIColor colorWithHexString:@"4CC80E"]] percentageArray:@[@(0.18),@(1)] gradientType:GradientFromTopToBottom];
        [self addSubview:four];
        [four.titleLabel setFont:LabelFont14];
        [four addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
        [four mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(bgView);
            make.size.mas_equalTo(CGSizeMake(257*AdapterScal, 45*AdapterScal));
            make.bottom.mas_equalTo(bgView.mas_bottom).offset(-18);
        }];
    }
    
    return self;
}

- (void)dismissView{
    [self hide];
}

- (void)btnClick{
    if (self.clickBlock) {
        self.clickBlock();
    }
}


-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}

@end
