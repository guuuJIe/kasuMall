//
//  BargainSuccessPopView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BargainSuccessPopView.h"
#import "UIButton+Gradient.h"
#import "BargainVC.h"
@interface BargainSuccessPopView()
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic) UIButton *chaButton;
@property (nonatomic, strong) UILabel *stateLbl;
@end

@implementation BargainSuccessPopView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        
        
        
        UIImageView *imageView = [UIImageView new];
        imageView.image = [UIImage imageNamed:@"B_pop"];
        [self addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self);
            make.height.mas_equalTo(267);
            make.left.mas_equalTo(32);
            make.right.mas_equalTo(-32);
        }];
        
//        UILabel *label2 = [UILabel new];
//        label2.text = @"多砍了";
//        label2.textColor = [UIColor colorWithHexString:@"7E5F23"];
//        label2.font = LabelFont16;
//
//        [imageView addSubview:label2];
//        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
//           make.centerX.mas_equalTo(imageView).offset(-24);
//           make.top.mas_equalTo(65);
//        }];
        
        self.priceLbl = [UILabel new];
        self.priceLbl.font = LabelFont16;
        self.priceLbl.textColor = [UIColor colorWithHexString:@"7E5F23"];
        self.priceLbl.text = @"";
       
        [imageView addSubview:self.priceLbl];
        [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(imageView);
            make.top.mas_equalTo(65);
        }];
        
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"送你一件免费商品,快来挑选吧";
        label.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
        [imageView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(13);
            make.centerX.mas_equalTo(imageView);
        }];
        self.stateLbl = label;
        
        [self addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(imageView.mas_top).offset(0);
            make.right.mas_equalTo(imageView);
        }];
        
        UIButton *four = [UIButton new];
        [four setTitle:@"马上去选" forState:UIControlStateNormal];
        [four setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [four setImage:[UIImage imageNamed:@"B_wechat"] forState:UIControlStateNormal];
//        four.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
        four.layer.cornerRadius = 5;
        four.layer.masksToBounds = true;
        [four gradientButtonWithSize:CGSizeMake(257*AdapterScal, 45*AdapterScal) colorArray:@[(id)[UIColor colorWithHexString:@"FD6F31"],(id)[UIColor colorWithHexString:@"FC2C17"]] percentageArray:@[@(0.18),@(1)] gradientType:GradientFromTopToBottom];
        [self addSubview:four];
        [four.titleLabel setFont:LabelFont14];
        [four addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
        [four mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(imageView);
            make.size.mas_equalTo(CGSizeMake(257*AdapterScal, 45*AdapterScal));
            make.bottom.mas_equalTo(imageView.mas_bottom).offset(-18*AdapterScal);
        }];
        
        
    }
    
    return self;
}

- (void)btnClick{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (void)dismissView{
    [self hide];
}

- (void)setupData:(NSDictionary *)dic{
    CGFloat amount = [dic[@"amount"] floatValue];
//    self.priceLbl.text = [NSString stringWithFormat:@"%.2f",amount];
    NSString *price = [NSString stringWithFormat:@"%.2f",amount];
    NSString *sumStr = [NSString stringWithFormat:@"谢谢你帮我砍了%.2f元",amount];
    self.priceLbl.text = sumStr;
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
    [aStr addAttribute:NSFontAttributeName value:LabelFont30 range:NSMakeRange(7, price.length)];
    [aStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FC2C17"] range:NSMakeRange(7, price.length)];
    self.priceLbl.attributedText = aStr;
    
//    NSMutableAttributedString *aStr2 = [[NSMutableAttributedString alloc] initWithString:self.stateLbl.text];
//    [aStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FC2C17"] range:[self.stateLbl.text rangeOfString:@"再砍一刀"]];
//    self.stateLbl.attributedText = aStr2;
}

-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}

@end
