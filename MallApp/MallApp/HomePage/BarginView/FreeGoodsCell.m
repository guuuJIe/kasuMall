//
//  FreeGoodsCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FreeGoodsCell.h"
@interface FreeGoodsCell()
@property (weak, nonatomic) IBOutlet UIImageView *goodImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *salesLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@end

@implementation FreeGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
        [self.goodImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = dic[@"title"];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
        self.salesLbl.text = [NSString stringWithFormat:@"还剩%@件",[NSString stringWithFormat:@"%@",dic[@"saleQuantity"]]];
    }
}

@end
