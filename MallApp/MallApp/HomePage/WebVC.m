//
//  WebVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WebVC.h"
#import <WebKit/WebKit.h>
#import "GroupApi.h"
@interface WebVC ()<WKUIDelegate,WKNavigationDelegate>
@property (nonatomic, strong) WKWebView *wkwebView;
@property (nonatomic, strong) GroupApi *api;
@end

@implementation WebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    [self setupdata];
}

- (void)setupUI{
    self.title = @"服务条款";
    [self.view addSubview:self.wkwebView];
    [self.wkwebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
   
    
    
}

- (void)setupdata{
   
    WeakSelf(self)
    [self.api EwProductPactWithparameters:@(self.ids) withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
           
            [weakself.wkwebView loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",datas.firstObject] baseURL:nil];
        }
    }];
}


- (WKWebView *)wkwebView {
    if (!_wkwebView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 偏好设置
        config.preferences = [[WKPreferences alloc] init];
        config.preferences.minimumFontSize = 10;
        config.preferences.javaScriptEnabled = YES;
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;


        WKUserContentController *userContentController = [[WKUserContentController alloc] init];
//        WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:cookie injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
//        [userContentController addUserScript:cookieScript];

//        [userContentController addScriptMessageHandler:self name:@"share"];
//        [userContentController addScriptMessageHandler:self name:@"login"];

        config.userContentController = userContentController;
        config.selectionGranularity = WKSelectionGranularityDynamic;
        config.allowsInlineMediaPlayback = YES;
//        config.mediaTypesRequiringUserActionForPlayback = false;
        
      
        _wkwebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        
        _wkwebView.navigationDelegate = self;//导航代理
        _wkwebView.UIDelegate = self;//UI代理
        _wkwebView.backgroundColor = [UIColor whiteColor];
        _wkwebView.allowsBackForwardNavigationGestures = YES;
        _wkwebView.scrollView.scrollEnabled = false;
        _wkwebView.scrollView.showsVerticalScrollIndicator = false;
        
       
    }
    return _wkwebView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    
    return _api;
}

@end
