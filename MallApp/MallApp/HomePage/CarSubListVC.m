//
//  CarSubListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarSubListVC.h"
#import "CarSubTypeCell.h"
#import "HomePageApi.h"
#import "UnderLineButton.h"
@interface CarSubListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) NSArray *dataArr;
@property (nonatomic) HomePageApi *homeApi;
@property (nonatomic) NSMutableString *model;
@property (nonatomic) NSString *modelname;
@property (nonatomic) NSMutableArray *btnModelArray;
@property (nonatomic) NSMutableArray *btnArray;
@property (nonatomic) NSArray *defaultValueArr;
@property (nonatomic) UnderLineButton *selectedBtn;
@property (nonatomic,assign) NSInteger indexLength;
@end

@implementation CarSubListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initTopBtn];
    [self setupData];
    [self getCarData];
//    [self.btnModelArray addObject:_defaultValueArr[_indexLength]];
//    [self setcarType:_indexLength];
    
}

- (void)setupData{
    self.title = @"请选择车型";
//    [self.model appendString:self.carTypeIds];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(50);
    }];
}

- (void)getCarData{
//    NSLog(@"%@",self.carTypeIds);
    
    [self.model appendString:[NSString stringWithFormat:@"%@-",self.carTypeIds]];
//    NSLog(@"%@",self.model);
    [self.homeApi getAllCarListDataWithparameters:self.carTypeIds withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *arr = result.resultDic[@"list"];
            if (arr.count != 0) {
                self.dataArr = arr;
                [self.TabelView reloadData];
            }else{
                
                if (self.type == 1) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"getCarModel" object:nil userInfo:@{@"model":self.model,@"modelname":self.modelname,@"carname":self.carname}];
                    [XJUtil popToVC:self.vcname inViewControllers:self.navigationController.viewControllers InCurNav:self.navigationController];
                }else if (self.type == 0){
                    [[NSUserDefaults standardUserDefaults] setObject:self.model forKey:@"mycarModel"];
                    [[NSUserDefaults standardUserDefaults] setObject:self.modelname forKey:@"mycarmodelName"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshHomeData" object:nil];
                    [self.navigationController popToRootViewControllerAnimated:true];
                }
                
               
                
            }
        }else{
            [JMBManager showBriefAlert:@"加载失败"];
        }
    }];
    
}

- (void)initTopBtn{
    UIView *lastView;
    for (UIView *view in self.view.subviews) {
        if ([view isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    self.defaultValueArr = [NSArray arrayWithObjects:@{@"name":@"选择产地"},@{@"name":@"选择车型"},@{@"name":@"选择年份"},@{@"name":@"选择车款"}, nil];
    _indexLength = 0;
    for (int i = 0; i < self.defaultValueArr.count; i++) {
        NSDictionary *dic = self.defaultValueArr[i];
        UnderLineButton *button = [UnderLineButton new];
        [button setTitleColor:UIColor333 forState:UIControlStateNormal];
        button.layer.borderColor = UIColorB6.CGColor;
        button.l_width = Screen_Width/4;
        [button setTitle:dic[@"name"] forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont14;
        
        button.tag = i+200;
        [self.view addSubview:button];
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        if (i != 0) {
            button.hidden = true;
        }
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width/4, 50));
            make.top.mas_equalTo(0);
            if(i == 0){
                make.left.equalTo(self.view).offset(0);
            }else{
                make.left.equalTo(lastView.mas_right).offset(2);
            }
        }];
        lastView = button;
        [self.btnArray addObject:button];
    }
    UnderLineButton *defaultButton = [self.view viewWithTag:200];
    [self click:defaultButton];
}


- (void)setcarType:(NSInteger)i
{

    UnderLineButton *button = self.btnArray[i];
    if (i != 3) {
        UnderLineButton *showbutton = self.btnArray[i+1];
        showbutton.hidden = false;
        [self click:showbutton];
    }
    
    NSDictionary *dic = self.btnModelArray[i];
    
    [button setTitle:dic[@"name"] forState:UIControlStateNormal];
    
  
}

- (void)click:(UnderLineButton *)btn{
    
    if (btn!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
        self.selectedBtn.selected = YES;
    }
    
    
    
}

#pragma mark ---UITableViewDelegate----
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *Identifier = @"CarSubTypeCell";
    CarSubTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[CarSubTypeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
 
    [cell setupData:self.dataArr[indexPath.row]];
    
    return cell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSDictionary *dic = self.dataArr[indexPath.row];
    self.carTypeIds = dic[@"id"];
    self.modelname = [NSString stringWithFormat:@"%@",dic[@"name"]];
    if (_indexLength <=3) {
        [self.btnModelArray replaceObjectAtIndex:_indexLength withObject:dic];
        [self.btnModelArray addObject:_defaultValueArr[_indexLength]];
        
        [self setcarType:_indexLength];
    }
   
    [self getCarData];
    _indexLength++;
    JLog(@"%ld",(long)_indexLength);
}


-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        //        [_TabelView registerNib:[UINib nibWithNibName:@"CarHeadCell" bundle:nil] forCellReuseIdentifier:@"CarHeadCell"];
        
        [_TabelView registerNib:[UINib nibWithNibName:@"CarSubTypeCell" bundle:nil] forCellReuseIdentifier:@"CarSubTypeCell"];
        
        _TabelView.backgroundColor = UIColorEF;
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (NSMutableString *)model{
    if (!_model) {
        _model = [NSMutableString new];
    }
    return _model;
}

- (HomePageApi *)homeApi{
    if (!_homeApi) {
        _homeApi = [HomePageApi new];
    }
    
    return _homeApi;
}

- (NSMutableArray *)btnModelArray{
    if (!_btnModelArray) {
        //@{@"name":@"选择车型"},@{@"name":@"选择年份"},@{@"name":@"选择车款"},
        _btnModelArray = [NSMutableArray arrayWithObjects:_defaultValueArr[0],nil];
    }
    return _btnModelArray;
}

- (NSMutableArray *)btnArray{
    if (!_btnArray) {
        _btnArray = [NSMutableArray new];
    }
    return _btnArray;
}
@end
