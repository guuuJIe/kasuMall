//
//  BargainVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BargainVC.h"
#import "BarginSectionOneCell.h"
#import "QianggouCell.h"
#import "BarginExcutingView.h"
#import "FreeGoodsCell.h"
#import "ReturnCashVC.h"
#import "GroupApi.h"
#import "MyAddressPopView.h"
#import "AdressListVC.h"
#import "OrderApi.h"
#import "BarginingGoodCell.h"
@interface BargainVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tabelView;
@property (nonatomic, strong) GroupApi *api;
@property (assign, nonatomic) NSInteger type;
@property (nonatomic, strong) NSArray *freedataArray;
@property (nonatomic, strong) NSArray *myrushArray;
@property (nonatomic, strong) MyAddressPopView *popView;
@property (nonatomic, strong) OrderApi *orderApi;
@property (nonatomic, strong) NSDictionary *addressDic;
@end

@implementation BargainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"砍价免费拿";
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"HelpBarginGoodVC"]];
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    if (accessToken) {
        self.type = 1;
        [self getallData];
    }else{
        self.type = 0;
        [self getListData];
        
    }
    
    
}

- (void)setupUI{
    [self.tabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)getListData{

    [self.api getBargainListWithparameters:@0 withCompletionHandler:^(NSError *error, MessageBody *result) {
        NSArray *datas = result.result;
        self.freedataArray = datas;
        [self.tabelView reloadData];
    }];
}

- (void)getListData2{

    [self.api getBargainListWithparameters:@1 withCompletionHandler:^(NSError *error, MessageBody *result) {
        NSArray *datas = result.result;
        self.myrushArray = datas;
        
    }];
}

- (void)getallData{
    dispatch_group_t  group = dispatch_group_create();
    dispatch_queue_t que = dispatch_queue_create("gcd_global", DISPATCH_QUEUE_CONCURRENT);
    dispatch_group_async(group, que, ^{
        dispatch_group_enter(group);
        [self.api getBargainListWithparameters:@0 withCompletionHandler:^(NSError *error, MessageBody *result) {
            
            if (result.code == 200) {
                self.freedataArray =result.result;
            }
            dispatch_group_leave(group);
            
        }];
        
        
    });
    
   
    
  dispatch_group_async(group, que, ^{
        dispatch_group_enter(group);
        [self.api getBargainListWithparameters:@1 withCompletionHandler:^(NSError *error, MessageBody *result) {
            
            if (result.code == 200) {
                self.myrushArray =result.result;
            }
            dispatch_group_leave(group);
            
        }];
        
        
    });
    
    
    dispatch_group_notify(group, que, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tabelView reloadData];
            
        });
    });
       
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return self.myrushArray.count;
    }else{
        return self.freedataArray.count;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"BarginSectionOneCell";
        BarginSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[BarginSectionOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
       
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"BarginingGoodCell";
        BarginingGoodCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[BarginingGoodCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.myrushArray[indexPath.row]];
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"FreeGoodsCell";
        FreeGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[FreeGoodsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.freedataArray[indexPath.row]];
        return cell;
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGFLOAT_MIN;
    }else if (section == 1){
        return self.myrushArray.count > 0 ? 45 : CGFLOAT_MIN;
    }else if (section == 2){
        return self.freedataArray.count > 0 ? 45 : CGFLOAT_MIN;
    }
    
    return CGFLOAT_MIN;
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        BarginExcutingView *headview = [[BarginExcutingView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 45)];
        headview.headImage.image = [UIImage imageNamed:@"B_icon_1"];
        return headview;
    }else{
        BarginExcutingView *headview = [[BarginExcutingView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 45)];
        headview.headImage.image = [UIImage imageNamed:@"B_icon_2"];
        return headview;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        
        if (!accessToken) {
//            [XJUtil callUserLogin:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
            return;
        }
        
        NSDictionary *itemdic = self.freedataArray[indexPath.row];
        WeakSelf(self)
        [self.popView setData:^(NSDictionary * _Nonnull dic) {
            if (dic) {
                weakself.addressDic = dic;
            }
            
            [weakself.popView show];
        }];
        
        
        self.popView.clickBlock = ^{
            [weakself.popView hide];
            [weakself.navigationController pushViewController:[AdressListVC new] animated:true];
        };
        
        self.popView.purchaseBlock = ^{
            [JMBManager showLoading];
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic setValue:weakself.addressDic[@"id"] forKey:@"addressId"];
            [dic setValue:itemdic[@"productId"] forKey:@"groupProId"];
            [dic setValue:@0 forKey:@"cardId"];
            [dic setValue:@0 forKey:@"payment"];//支付方式
            [dic setValue:@0 forKey:@"invoiceId"];//发票ID
            [dic setValue:@0 forKey:@"stageNum"];//发票
            [dic setValue:@1 forKey:@"quautity"];//数量
            [dic setValue:@4 forKey:@"GroupType"];//团购类型
            NSInteger ids = [clerkid integerValue];
            if (ids) {
                [dic setValue:@(ids) forKey:@"tgcode"];//代理id
            }
            [weakself.orderApi createOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [weakself.popView hide];
                    NSArray *datas = result.result;
                    ReturnCashVC *vc = [[ReturnCashVC alloc] init];
                    vc.orderNum = [NSString stringWithFormat:@"%@",datas[1]];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clerkid"];
                    [weakself.navigationController pushViewController:vc animated:true];
                   
                }
                
                [JMBManager hideAlert];
              
            }];
        };
    }else if (indexPath.section == 1){
        ReturnCashVC *vc = [[ReturnCashVC alloc] init];
        NSDictionary *dic = self.myrushArray[indexPath.row];
        vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
        [self.navigationController pushViewController:vc animated:true];
    }
}


-(UITableView *)tabelView
{
    if(_tabelView==nil)
    {
        _tabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tabelView setDelegate:self];
        [_tabelView setDataSource:self];
        [_tabelView setTableFooterView:[UIView new]];
        [_tabelView setTableHeaderView:[UIView new]];
        _tabelView.rowHeight = UITableViewAutomaticDimension;
        _tabelView.estimatedRowHeight = 44;
        _tabelView.showsVerticalScrollIndicator = false;
        [_tabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tabelView.sectionIndexColor = UIColor70;
        [_tabelView setSectionIndexBackgroundColor:[UIColor clearColor]];
        _tabelView.backgroundColor = [UIColor clearColor];
        [_tabelView registerNib:[UINib nibWithNibName:@"BarginSectionOneCell" bundle:nil] forCellReuseIdentifier:@"BarginSectionOneCell"];
        [_tabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
        [_tabelView registerNib:[UINib nibWithNibName:@"QianggouCell" bundle:nil] forCellReuseIdentifier:@"QianggouCell"];
        [_tabelView registerNib:[UINib nibWithNibName:@"FreeGoodsCell" bundle:nil] forCellReuseIdentifier:@"FreeGoodsCell"];
        [_tabelView registerClass:[BarginingGoodCell class] forCellReuseIdentifier:@"BarginingGoodCell"];
        [_tabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];//89223406
        [self.view addSubview:_tabelView];
    }
    return _tabelView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    
    return _api;
}

- (MyAddressPopView *)popView{
    if (!_popView) {
        _popView = [MyAddressPopView new];
    }
    return _popView;
}

- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
    }
    return _orderApi;
}
@end
