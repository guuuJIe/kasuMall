//
//  TuanGouGoodInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "TuanGouGoodInfoVC.h"
#import "OperationView.h"
#import "navView.h"
#import "TuangouInfoOneCell.h"
#import "TuangouInfoTwoCell.h"
#import "TuangouInfoThreeCell.h"
#import "InfoFourCell.h"
#import "TuangouInfoTwoCell.h"
#import "PurchaseView.h"
#import "SubmitOrderVC.h"
#import "GoodsInfoApi.h"
#import "PurchaseNumPopView.h"
#import "LoginVC.h"
@interface TuanGouGoodInfoVC ()<UITableViewDelegate,UITableViewDataSource,WKUIDelegate,WKNavigationDelegate>
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) OperationView *OpView;
@property (nonatomic) navView *navbar;
@property (nonatomic,assign) CGFloat webViewCellHeight;
@property (nonatomic) PurchaseView *purchaseView;
@property (nonatomic) GoodsInfoApi *goodApi;
@property (nonatomic) NSArray *dataArr;
@property (nonatomic) NSDictionary *dataDic;
@property (nonatomic) NSString *num;
@property (nonatomic) NSString *addressId;
@property (nonatomic) PurchaseNumPopView *purchaseNumView;
@property (nonatomic,strong) NSMutableString *htmStr;
@property (nonatomic,assign) BOOL isFirstLoad;//只加载一次
@property (nonatomic,assign) NSInteger rows;
@end

@implementation TuanGouGoodInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setData];
    
}

- (void)setupUI{
    self.view.backgroundColor = UIColorEF;
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-(50+BottomAreaHeight));
    }];
    AdjustsScrollViewInsetNever(self, self.listTableView);
    
    [self.view addSubview:self.OpView];
    [self.OpView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    [self.view addSubview:self.navbar];
    [self.navbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(44+StatusBarHeight);
    }];
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setData{
    self.num = @"1";
    self.isFirstLoad = true;
    self.addressId = @"0";
    self.rows = 0;
    [JMBManager showWaitingWithTitle:@"加载中" inView:self.view];
    WeakSelf(self)
    [self.goodApi getGroupGoodsItemWithparameters:self.goodsId withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            self.dataArr = arr;
//            [self.listTableView reloadData];
            
            self.rows = 4;
            [self configData:arr];
        }
    }];
}

-(void)configData:(NSArray *)data{
    self.dataDic = data.firstObject;
    
    NSInteger statues = [self.dataDic[@"status"] integerValue];
    
    if (statues == 2) {
        self.purchaseView.purchaseButton.enabled = false;
        [self.purchaseView.purchaseButton setTitle:@"已结束" forState:UIControlStateDisabled];
    }else{
        self.purchaseView.purchaseButton.enabled = true;
    }
    
    

    
//    NSString *imgstr = [XJUtil filterHtmlGoodsImageArr:[XJUtil insertStringWithNotNullObject:self.dataDic[@"introduction"] andDefailtInfo:@""]];
//    NSArray *imgarray2 = [XJUtil filterImage:[XJUtil insertStringWithNotNullObject:self.dataDic[@"parameter"] andDefailtInfo:@""]];
//    NSArray *imgarray3 = [XJUtil filterImage:[XJUtil insertStringWithNotNullObject:self.dataDic[@"aftersale"] andDefailtInfo:@""]];
//    NSArray *imgarray4 = [XJUtil filterImage:[XJUtil insertStringWithNotNullObject:self.dataDic[@"packing"] andDefailtInfo:@""]];
    
    self.htmStr = [NSMutableString new];
    
    NSString *content = [XJUtil filterHtmlGoodsImageArr:[XJUtil insertStringWithNotNullObject:self.dataDic[@"introduction"] andDefailtInfo:@""]];
    NSLog(@"content \n%@",content);
    NSString *parameter = [XJUtil filterHtmlGoodsImageArr:[XJUtil insertStringWithNotNullObject:self.dataDic[@"parameter"] andDefailtInfo:@""]];
     NSLog(@"parameter \n%@",parameter);
    NSString *aftersale = [XJUtil filterHtmlGoodsImageArr:[XJUtil insertStringWithNotNullObject:self.dataDic[@"aftersale"] andDefailtInfo:@""]];
    NSString *packing = [XJUtil filterHtmlGoodsImageArr:[XJUtil insertStringWithNotNullObject:self.dataDic[@"packing"] andDefailtInfo:@""]];
      
    [self.htmStr appendString:content];
    [self.htmStr appendString:parameter];
    [self.htmStr appendString:aftersale];
    [self.htmStr appendString:packing];

//    for (int i = 0; i<imgarray.count; i++) {
//        NSString *title = imgarray[i];
//        if ([title hasPrefix:@"http"]) {
//            title = [NSString stringWithFormat:@"<img src='%@'/>",title];
//        }else{
//           title = [NSString stringWithFormat:@"<img src='%@%@'/>",URLAddress,title];
//        }
//
//
//        [self.htmStr appendString:title];
//
//    }
//
//    for (int i = 0; i<imgarray2.count; i++) {
//          NSString *title = imgarray2[i];
//          if ([title hasPrefix:@"http"]) {
//              title = [NSString stringWithFormat:@"<img src='%@'/>",title];
//          }else{
//             title = [NSString stringWithFormat:@"<img src='%@%@'/>",URLAddress,title];
//          }
//
//
//          [self.htmStr appendString:title];
//
//      }
//
//    for (int i = 0; i<imgarray3.count; i++) {
//        NSString *title = imgarray3[i];
//        if ([title hasPrefix:@"http"]) {
//            title = [NSString stringWithFormat:@"<img src='%@'/>",title];
//        }else{
//           title = [NSString stringWithFormat:@"<img src='%@%@'/>",URLAddress,title];
//        }
//
//
//        [self.htmStr appendString:title];
//
//    }
//
//    for (int i = 0; i<imgarray4.count; i++) {
//         NSString *title = imgarray4[i];
//         if ([title hasPrefix:@"http"]) {
//             title = [NSString stringWithFormat:@"<img src='%@'/>",title];
//         }else{
//            title = [NSString stringWithFormat:@"<img src='%@%@'/>",URLAddress,title];
//         }
//
//
//         [self.htmStr appendString:title];
//
//     }
//
    NSLog(@"htmStr--%@",self.htmStr);
    
    [self.listTableView reloadData];
  
}

- (void)purchase{
    if (![self.purchaseNumView isHidden]) {
        [self.purchaseNumView hide];
    }
    WeakSelf(self)
    [self.goodApi getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        NSDictionary *dic = result.resultDic;
        NSArray *arr = dic[@"list"];
        //        self.adressArr = arr;
        if (arr.count != 0) {
            NSDictionary *dic = arr.firstObject;
            self.addressId = dic[@"id"];
            [self jumpToOrderVC];
        }else{
            [self jumpToOrderVC];
        }
    }];
    
}

- (void)jumpToOrderVC{
// 
   WeakSelf(self)
    [self.goodApi getGroupGoodsItemWithparameters:self.goodsId withCompletionHandler:^(NSError *error, MessageBody *result) {
       StrongSelf(self)
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            self.dataArr = arr;
            SubmitOrderVC *order = [SubmitOrderVC new];
            order.adressId = self.addressId;
            order.from = @"1";
            order.dataSourceArr = self.dataArr;
            order.num = self.num;
            order.goodsId = self.goodsId;
            order.productInfo = self.dataDic;
//            order.freight = [NSString stringWithFormat:@"%@",arr.firstObject];
            [self.navigationController pushViewController:order animated:true];
            
        }
    }];
    
}
#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *Identifier = @"TuangouInfoOneCell";
        TuangouInfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[TuangouInfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
//        [cell setData];
        [cell setData:self.dataDic];
        return cell;
    }else if(indexPath.row == 1){
        static NSString *Identifier = @"TuangouInfoTwoCell";
        TuangouInfoTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[TuangouInfoTwoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        [cell setupData:self.dataDic];
        return cell;
    }else if(indexPath.row == 2){
        static NSString *Identifier = @"TuangouInfoThreeCell";
        TuangouInfoThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[TuangouInfoThreeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic];
        return cell;
    }else if(indexPath.row == 3){
        static NSString *Identifier = @"InfoFourCell";
        InfoFourCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InfoFourCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
//        cell.backgroundColor = [UIColor redColor];
        cell.wkwebView.navigationDelegate = self;//加载webview记得要开启网络
        cell.wkwebView.UIDelegate = self;
        if (self.htmStr) {
           [cell.wkwebView loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",self.htmStr] baseURL:nil];
        }
       
        return cell;
        
    }
    else{
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        if (self.dataDic) {
          NSInteger type = [self.dataDic[@"type"] integerValue];
            if (type != 3) {
                return Screen_Width+50;
               
            }else{
                 return Screen_Width+50+20;
            }
        }
        return 0;
    }else if (indexPath.row == 3){
        return self.webViewCellHeight;
    }else{
        return UITableViewAutomaticDimension;
    }
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 判断webView所在的cell是否可见，如果可见就layout
//    NSArray *cells = self.listTableView.visibleCells;
//    for (UITableViewCell *cell in cells) {
//        if ([cell isKindOfClass:[InfoFourCell class]]) {
//            InfoFourCell *webCell = (InfoFourCell *)cell;
//
//            [webCell.wkwebView setNeedsLayout];
//        }
//    }
    if (scrollView.contentOffset.y >64) {
        [UIView animateWithDuration:0.5 animations:^{
            self.navbar.backgroundColor = APPColor;
            [self.navbar.backButton setImage:[UIImage imageNamed:@"back"] forState:0];
        }];
        
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            self.navbar.backgroundColor = [UIColor clearColor];
            [self.navbar.backButton setImage:[UIImage imageNamed:@"返回"] forState:0];
        }];
        
    }
    
}
#pragma mark - WkwebViewDelegate--
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    if (!self.webViewCellHeight && self.isFirstLoad) {
        WeakSelf(self)
        [webView evaluateJavaScript:@"document.body.offsetHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            StrongSelf(self)
            [JMBManager hideAlert];
            // 计算webView高度
            //            if (!self.webViewCellHeight) {
            self.isFirstLoad = !self.isFirstLoad;
            self.webViewCellHeight = [result doubleValue];
            // 刷新tableView
            [self.listTableView reloadData];
            //            }
            
        }];
    }
    


}
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    [JMBManager hideAlert];
}
- (navView *)navbar{
    if (!_navbar) {
        _navbar = [navView new];
        
        WeakSelf(self);
        _navbar.clickBlock = ^{
            [weakself.navigationController popViewControllerAnimated:true];
        };
    }
    return _navbar;
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 40;
        _listTableView.backgroundColor = [UIColor whiteColor];
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerClass:[TuangouInfoOneCell class] forCellReuseIdentifier:@"TuangouInfoOneCell"];
//        [_listTableView registerClass:[InfoTwoCell class] forCellReuseIdentifier:@"InfoTwoCell"];
//        _listTableView.mj_footer = [JGIifFooter footerWithRefreshingBlock:^{
//
//        }];
       
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_listTableView registerNib:[UINib nibWithNibName:@"TuangouInfoTwoCell" bundle:nil] forCellReuseIdentifier:@"TuangouInfoTwoCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"TuangouInfoThreeCell" bundle:nil] forCellReuseIdentifier:@"TuangouInfoThreeCell"];
        [_listTableView registerClass:[InfoFourCell class] forCellReuseIdentifier:@"InfoFourCell"];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        WeakSelf(self);
        _purchaseView.addAddressBlock = ^{
//            [weakself purchase];
            if (!accessToken) {
                //                [JMBManager showBriefAlert:@"未登入"];
//                LoginVC *vc = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//                BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//                vc2.modalPresentationStyle = 0;
//                [weakself presentViewController:vc2 animated:true completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
                //普通买家 delafqm
                return ;
            }
            [weakself.purchaseNumView show];
        };
    }
    return _purchaseView;
}


- (GoodsInfoApi *)goodApi{
    if (!_goodApi) {
        _goodApi = [GoodsInfoApi new];
    }
    return _goodApi;
}

- (PurchaseNumPopView *)purchaseNumView{
    if (!_purchaseNumView) {
        _purchaseNumView = [PurchaseNumPopView new];
        WeakSelf(self);
        _purchaseNumView.purchaseBlock = ^(NSInteger tag, NSInteger num) {
           
            weakself.num = [NSString stringWithFormat:@"%ld",(long)num];
            [weakself purchase];
            
        };
    }
    return _purchaseNumView;
}

-(NSArray*)getImgTags:(NSString *)htmlText
{
    if (htmlText == nil) {
        return nil;
    }

    
    NSError *error;
    NSString *regulaStr = @"<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *arrayOfAllMatches = [regex matchesInString:htmlText options:0 range:NSMakeRange(0, [htmlText length])];

    return arrayOfAllMatches;
}


@end
