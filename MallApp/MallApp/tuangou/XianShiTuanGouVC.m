//
//  XianShiTuanGouVC.m
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "XianShiTuanGouVC.h"
#import "TuanGouCell.h"
#import "UnderLineButton.h"
#import "TuanGouGoodInfoVC.h"
#import "HomePageApi.h"
@interface XianShiTuanGouVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *TabelView;
@property(nonatomic,strong) UnderLineButton *lineBtn;
@property(nonatomic,strong) UnderLineButton *selectedBtn;
@property(nonatomic,strong) HomePageApi *homeApi;
@property(nonatomic,strong) NSMutableArray *dataArr;

@property(nonatomic,assign) NSInteger num;
@property(nonatomic,strong) NSString *size;
@property(nonatomic,strong) NSString *type;
@end

@implementation XianShiTuanGouVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    
    
    [self getData:RefreshTypeNormal];
}

-(void)setupUI{
    self.title = @"优惠团购";
//    self.view.backgroundColor = UIColorEF;
//    UIView *lastView;
//    NSArray *arr = @[@"抢购中",@"即将开抢"];
//    for (int i = 0; i<arr.count; i++) {
//        UnderLineButton *button = [UnderLineButton new];
////        button.lineView.backgroundColor = UIColor333;
////        button.l_width = 50;
//        [button setTitle:arr[i] forState:0];
//        [button setTitleColor:UIColor333 forState:0];
//        [button setBackgroundColor:[UIColor whiteColor]];
//        [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
//        [button setTitleColor:APPColor forState:UIControlStateSelected];
//        button.tag = i+200;
//        [self.view addSubview:button];
//        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
//        [button mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (i == 0) {
//                make.left.equalTo(self.view).offset(1);
//            }else{
//                make.left.equalTo(lastView.mas_right);
//            }
//            make.top.equalTo(self.view).offset(0);
//            make.width.mas_equalTo(Screen_Width/2);
//            make.height.mas_equalTo(45);
//        }];
//        lastView = button;
//    }
//
//    UnderLineButton *defaultButton = [self.view viewWithTag:200];
//    [self click:defaultButton];
    self.num = 1;
    self.size = @"20";
    self.type = @"3";
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(15);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (void)click:(UnderLineButton *)btn{
    if (btn!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
        self.selectedBtn.selected = YES;
    }
    if (btn.tag == 200) {
        self.type = @"0";
    }else{
        self.type = @"1";
    }
   
}

- (void)getData:(RefreshType)type{
    if (type == RefreshTypeNormal || type == RefreshTypeDown) {
        self.num = 1;
    }else{
        self.num = self.num + 1;
    }
    [self.homeApi getGroupListDataWithparameters:@{} withType:self.type andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
//            self.dataArr = result.resultDic[@"list"];
            self.dataArr = [XJUtil dealData:datas withRefreshType:type withListView:self.TabelView AndCurDataArray:self.dataArr withNum:self.num];
            [self.TabelView reloadData];
        }
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"TuanGouCell";
    TuanGouCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[TuanGouCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArr[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TuanGouGoodInfoVC *vc = [TuanGouGoodInfoVC new];
    NSDictionary *dic = self.dataArr[indexPath.row];
    vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
    [self.navigationController pushViewController:vc animated:true];
}
#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerClass:[TuanGouCell class] forCellReuseIdentifier:@"TuanGouCell"];
        _TabelView.backgroundColor = [UIColor clearColor];
        WeakSelf(self)
        _TabelView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            [weakself getData:RefreshTypeDown];
        }];
        
        _TabelView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakself getData:RefreshTypeUP];
        }];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (HomePageApi *)homeApi{
    if (!_homeApi) {
        _homeApi = [HomePageApi new];
    }
    return _homeApi;
}

@end
