//
//  TuangouInfoThreeCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "TuangouInfoThreeCell.h"

@implementation TuangouInfoThreeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    self.numLbl.text = [NSString stringWithFormat:@"%@件",dic[@"minNumber"]];
    self.limitLbl.text = [NSString stringWithFormat:@"%@件",dic[@"limits"]];
    self.timeLbl.text = [NSString stringWithFormat:@"%@",dic[@"endTime"]];
    
    self.stateLbl.text = [NSString stringWithFormat:@"%@",dic[@"tip"]];
    
}

@end
