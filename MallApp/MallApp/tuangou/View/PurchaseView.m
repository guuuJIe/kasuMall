//
//  PurchaseView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PurchaseView.h"
@interface PurchaseView()




@end

@implementation PurchaseView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
   
    [self addSubview:self.purchaseButton];
    
   
    
    [self.purchaseButton mas_makeConstraints:^(MASConstraintMaker *make) {
          make.right.top.equalTo(self);
          make.height.equalTo(@50);
         
          make.width.mas_equalTo(Screen_Width);
      }];
}

- (void)click{
    if (self.addAddressBlock) {
        self.addAddressBlock();
    }
}


-(UIButton *)purchaseButton
{
    if(!_purchaseButton)
    {
        _purchaseButton=[UIButton  new];
        [_purchaseButton setTitle:@"立即购买" forState:UIControlStateNormal];
//        [_purchaseButton setTitle:@"" forState:UIControlStateNormal];
        _purchaseButton.titleLabel.font = LabelFont14;
        [_purchaseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        _purchaseButton.backgroundColor = UIColor276;
        [_purchaseButton setBackgroundColor:UIColor276];
        [_purchaseButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [_purchaseButton setBackgroundImage:[UIImage imageWithColor:UIColorCC] forState:UIControlStateDisabled];
        
    }
    return _purchaseButton;
}

@end
