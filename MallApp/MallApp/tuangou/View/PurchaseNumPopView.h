//
//  PurchaseNumPopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PurchaseNumPopView : MMPopupView
@property (nonatomic,copy) void(^purchaseBlock)(NSInteger tag,NSInteger num);
@property (nonatomic,strong) UILabel *titleLbl;
@end

NS_ASSUME_NONNULL_END
