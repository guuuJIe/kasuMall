//
//  ProgressBarView.m
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ProgressBarView.h"

@interface ProgressBarView()



@end

@implementation ProgressBarView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    
    [self addSubview:self.bgView];
    self.bgView.frame = self.frame;
    self.bgView.layer.cornerRadius = 7;
    
    [self addSubview:self.gradientView];
    self.gradientView.frame = CGRectMake(0, 0, 0, self.frame.size.height);
//    self.gradientView.layer.cornerRadius = 7;
    
    [self addSubview:self.numLabel];
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.bgView);
    }];
}


- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = UIColorFFD1;
    }
    return _bgView;
}

- (UIView *)gradientView{
    if (!_gradientView) {
        _gradientView = [UIView new];
        _gradientView.backgroundColor = UIColorFF3E;
    }
    return _gradientView;
}

- (UILabel *)numLabel
{
    if(!_numLabel)
    {
        _numLabel=[UILabel  new];
        _numLabel.font = [UIFont systemFontOfSize:9];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.numberOfLines = 1;
        _numLabel.text = @"已抢购30件";
        //        _goodsTitleLabel.alpha = 0.2;
    }
    return _numLabel;
}

@end
