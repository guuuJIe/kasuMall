//
//  ProgressBarView.h
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgressBarView : UIView
@property (nonatomic) UIView *gradientView;
@property (nonatomic) UILabel *numLabel;
@property (nonatomic) UIView *bgView;
@end

NS_ASSUME_NONNULL_END
