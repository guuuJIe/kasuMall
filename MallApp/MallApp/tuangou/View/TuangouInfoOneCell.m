//
//  TuangouInfoOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "TuangouInfoOneCell.h"
#import "SDCycleScrollView.h"
#import "CountDown.h"
@interface TuangouInfoOneCell()
@property (nonatomic , strong) SDCycleScrollView *focusHeadView;
@property (nonatomic , strong) UIView *stateView;
@property (nonatomic , strong) UIView *fenqiView;
@property (nonatomic , strong) UILabel *stateLabel;
@property (nonatomic , strong) NSMutableArray *bannerData;
@property (nonatomic , strong) UILabel *fenqiLbl;
@property (nonatomic , strong) UILabel *limitLbl;
@property (nonatomic , strong) UILabel *countLbl;
@property (nonatomic , strong) CountDown *countDown;
@property(nonatomic,assign)int nowTime;
@end

@implementation TuangouInfoOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
        
    }
    return self;
}

- (void)setupLayout{
    [self.focusHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(Screen_Width);
    }];
    [self.contentView addSubview:self.stateView];
    [self.stateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(50);
        make.top.equalTo(self.focusHeadView.mas_bottom).offset(0);
    }];
    
    [self.stateView addSubview:self.fenqiLbl];
    [self.fenqiLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(12);
    }];
    
    [self.stateView addSubview:self.limitLbl];
    [self.limitLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.fenqiLbl.mas_bottom).offset(3);
    }];
    
    [self.contentView addSubview:self.fenqiView];
    [self.fenqiView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.stateView.mas_bottom).offset(0);
    }];
    
    [self.fenqiView addSubview:self.stateLabel];
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fenqiView);
        make.left.equalTo(@10);
    }];
    
    [self.stateView addSubview:self.countLbl];
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.stateView);
        make.right.equalTo(self.stateView).offset(-12);
    }];
}

- (void)setData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    NSString *morePic = dic[@"morePic"];
    NSString *imageUrl = dic[@"imageUrl"];
    NSArray *array = [morePic componentsSeparatedByString:@"|"];//|为分隔符
    
    if (imageUrl.length > 0) {
        if (array.count > 1) {
            for (int i = 0; i<array.count; i++) {
                NSString *url = array[i];
                [self.bannerData addObject:[NSString stringWithFormat:@"%@%@",URLAddress,url]];
            }
            [self.bannerData addObject:[NSString stringWithFormat:@"%@%@",URLAddress,imageUrl]];
        }else{
            [self.bannerData addObject:[NSString stringWithFormat:@"%@%@",URLAddress,imageUrl]];
        }
        
    }
    
    if (morePic.length > 0) {
        
        [self.bannerData addObject:[NSString stringWithFormat:@"%@Uploadfiles/MorePic/%@",URLAddress,morePic]];
        
    }
    
  
    
   
    
    NSLog(@"bannerData--%@",self.bannerData);
   
    NSInteger type = [dic[@"type"] integerValue];
    NSInteger status = [dic[@"status"] integerValue];
    if (type == 3) {//分期团购
        self.stateLabel.text = [NSString stringWithFormat:@"购买金额%@天为1期分%@期返还,每期返还%@元",dic[@"stageSize"],dic[@"stageNum"],dic[@"stagePrice"]];
        self.stateView.backgroundColor = APPColor;
        self.fenqiLbl.text = @"分期团购";
//        self.limitLbl.text = [NSString stringWithFormat:@"%@"]
         self.fenqiView.hidden = false;
    }else if (type == 1){//限时团购
        self.stateView.backgroundColor = [UIColor colorWithHexString:@"FF546C"];
        self.fenqiLbl.text = @"限时团购";
        self.fenqiView.hidden = true;
    }
    NSInteger total = [dic[@"number"] integerValue];
    NSInteger num = [dic[@"groupNumber"] integerValue];
    
    self.limitLbl.text = [NSString stringWithFormat:@"仅剩%ld件",total-num];
    
    [self.focusHeadView setImageURLStringsGroup:self.bannerData];
    
    if (status != 2) {
        NSString *limittime = [NSString stringWithFormat:@"%ld",(long)[XJUtil timeSwitchTimestamp:dic[@"endTime"]]];
        NSString *nowtime = [XJUtil getNowTimeStamp];
        self.nowTime = [limittime intValue] - [nowtime intValue];
        WeakSelf(self)
        [self.countDown countDownWithStratTimeStamp:self.nowTime  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
            //        NSLog(@"%ld",(long)day);
            weakself.countLbl.text=[NSString stringWithFormat:@"距结束：%li天%li时%li分%li秒",(long)day,(long)hour,(long)minute,(long)second];
            //        [self refreshUIDay:day hour:hour minute:minute second:second];
        }];
    }else{
        self.countLbl.text = @"已结束";
    }
   
}


- (UIView *)stateView{
    if (!_stateView) {
        _stateView = [UIView new];
        _stateView.backgroundColor = APPColor;
    }
    
    return _stateView;
}

- (UIView *)fenqiView{
    if (!_fenqiView) {
        _fenqiView = [UIView new];
        _fenqiView.backgroundColor = UIColorEF;
    }
    
    return _fenqiView;
}

- (NSMutableArray *)bannerData{
    if (!_bannerData) {
        _bannerData = [NSMutableArray array];
    }
    return _bannerData;
}

#pragma mark ----UI---
-(SDCycleScrollView *)focusHeadView{
    if (!_focusHeadView) {
        _focusHeadView = [SDCycleScrollView new];
        _focusHeadView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        //        _focusHeadView.currentPageDotColor = ThemeColor;
        _focusHeadView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
//        _focusHeadView.placeholderImage = PlaceholderImage;
        _focusHeadView.tag = 200;
//        _focusHeadView.delegate = self;
        _focusHeadView.backgroundColor = [UIColor whiteColor];
#ifdef DEBUG
        
        [_focusHeadView setImageURLStringsGroup:@[@"http://cdnimg.jsojs.com/goods_item1/300085/icon.jpg@!414",@"http://cdnimg.jsojs.com/goods_item1/300085/icon06.jpg@!414"]];
#else
        
#endif
        [self.contentView addSubview:_focusHeadView];
        
    }
    return _focusHeadView;
}
- (UILabel *)fenqiLbl
{
    if(!_fenqiLbl)
    {
        _fenqiLbl=[UILabel  new];
        _fenqiLbl.font = [UIFont systemFontOfSize:14];
        _fenqiLbl.textColor = [UIColor whiteColor];
        _fenqiLbl.numberOfLines = 1;
        _fenqiLbl.text = @"限时团购";
     
        _fenqiLbl.textAlignment = NSTextAlignmentLeft;
        //        _goodsTitleLabel.alpha = 0.2;
    }
    return _fenqiLbl;
}

- (UILabel *)limitLbl
{
    if(!_limitLbl)
    {
        _limitLbl=[UILabel  new];
        _limitLbl.font = [UIFont systemFontOfSize:10];
        _limitLbl.textColor = [UIColor whiteColor];
        _limitLbl.numberOfLines = 1;
        _limitLbl.text = @"仅剩3件";
     
        _limitLbl.textAlignment = NSTextAlignmentLeft;
       
    }
    return _limitLbl;
}

- (UILabel *)stateLabel
{
    if(!_stateLabel)
    {
        _stateLabel=[UILabel  new];
        _stateLabel.font = [UIFont systemFontOfSize:13];
        _stateLabel.textColor = APPColor;
        _stateLabel.numberOfLines = 1;
        _stateLabel.text = @"购买金额分12期返还，每期返还24.9元";
        _stateLabel.textAlignment = NSTextAlignmentLeft;
        //        _goodsTitleLabel.alpha = 0.2;
    }
    return _stateLabel;
}

- (UILabel *)countLbl
{
    if(!_countLbl)
    {
        _countLbl=[UILabel  new];
        _countLbl.font = [UIFont systemFontOfSize:13];
        _countLbl.textColor = [UIColor whiteColor];
        _countLbl.numberOfLines = 1;
        _countLbl.text = @"距结束";
     
        _countLbl.textAlignment = NSTextAlignmentLeft;
        //        _goodsTitleLabel.alpha = 0.2;
    }
    return _countLbl;
}

- (CountDown *)countDown{
    if (!_countDown) {
        _countDown = [CountDown new];
    }
    return _countDown;
}
@end
