//
//  TuangouInfoTwoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "TuangouInfoTwoCell.h"

@implementation TuangouInfoTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    
//    self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    self.orgPriceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"sPrice"]];
//    self.fenqiLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"stagePrice"]];
    self.fenqiNumLbl.text = [NSString stringWithFormat:@"x%@期",dic[@"stageNum"]];
    self.titleLbl.text = [NSString stringWithFormat:@"%@",dic[@"title"]];
    NSString *titleString = dic[@"title"];
     //创建  NSMutableAttributedString 富文本对象
     NSMutableAttributedString *maTitleString = [[NSMutableAttributedString alloc] initWithString:titleString];
     //创建一个小标签的Label
     NSString *aa = @"自营";
     CGFloat aaW = 12*aa.length +6;
     UILabel *aaL = [UILabel new];
     aaL.frame = CGRectMake(0, 0, aaW*3, 16*3);
     aaL.text = aa;
     aaL.font = [UIFont boldSystemFontOfSize:12*3];
     aaL.textColor = [UIColor whiteColor];
     aaL.backgroundColor = APPColor;
     aaL.clipsToBounds = YES;
     aaL.layer.cornerRadius = 3*3;
     aaL.textAlignment = NSTextAlignmentCenter;
     //调用方法，转化成Image
     UIImage *image = [self imageWithUIView:aaL];
     //创建Image的富文本格式
     NSTextAttachment *attach = [[NSTextAttachment alloc] init];
     attach.bounds = CGRectMake(0, -2.5, aaW, 16); //这个-2.5是为了调整下标签跟文字的位置
     attach.image = image;
     //添加到富文本对象里
     NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
     [maTitleString insertAttributedString:imageStr atIndex:0];//加入文字前面
     //[maTitleString appendAttributedString:imageStr];//加入文字后面
     //[maTitleString insertAttributedString:imageStr atIndex:4];//加入文字第4的位置
     
     
     self.titleLbl.attributedText = maTitleString;
    self.stateLbl.text = [NSString stringWithFormat:@"%@",dic[@"sTitle"]];
    NSInteger type = [dic[@"type"] integerValue];
    if (type == 3) {//分期团购
        self.fenqiNumLbl.hidden = false;
        self.fenqiLbl.hidden = false;
    }else if (type == 1){//显示团购
        self.fenqiNumLbl.hidden = true;
        self.fenqiLbl.hidden = true;
    }
    NSString *sumStr = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
    [aStr addAttribute:NSFontAttributeName value:LabelFont15 range:[sumStr rangeOfString:@"¥"]];
    
    self.priceLbl.attributedText = aStr;
    
    NSString *sumStr2 = [NSString stringWithFormat:@"¥%@",dic[@"stagePrice"]];
    NSMutableAttributedString *aStr2 = [[NSMutableAttributedString alloc] initWithString:sumStr2];
    [aStr2 addAttribute:NSFontAttributeName value:LabelFont15 range:[sumStr2 rangeOfString:@"¥"]];
    self.fenqiLbl.attributedText = aStr2;
    
}

//view转成image
- (UIImage*) imageWithUIView:(UIView*) view{
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage* tImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tImage;
}
@end
