//
//  TuangouInfoTwoCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TuangouInfoTwoCell : UITableViewCell
- (void)setupData:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *orgPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *fenqiLbl;
@property (weak, nonatomic) IBOutlet UILabel *fenqiNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *stateLbl;
@end

NS_ASSUME_NONNULL_END
