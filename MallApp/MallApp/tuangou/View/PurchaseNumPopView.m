//
//  PurchaseNumPopView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PurchaseNumPopView.h"
#import "NumView.h"
@interface PurchaseNumPopView()
@property (nonatomic) NumView *numView;
@property (nonatomic) NSInteger num;
@property (nonatomic) UIButton *confirm;
@end

@implementation PurchaseNumPopView
- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        self.num = 1;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimiss)]];
        
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(130);
        }];
        
        UILabel *type = [UILabel new];
        type.text = @"购买数量";
        type.textColor = UIColor333;
        type.font = LabelFont16;
        [bgView addSubview:type];
        [type mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@12);
            make.top.equalTo(bgView).offset(16);
        }];
        
        self.titleLbl = type;
        
        [bgView addSubview:self.numView];
        [self.numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(type);
            make.right.equalTo(@-16);
            make.size.mas_equalTo(CGSizeMake(100, 26));
        }];
        
        [bgView addSubview:self.confirm];
        [self.confirm mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(bgView);
//            make.top.equalTo(type.mas_bottom).offset(16);
            make.height.mas_equalTo(45);
            make.bottom.equalTo(bgView);
        }];
    }
    
    return self;
}

- (void)click{
    if (self.purchaseBlock) {
        self.purchaseBlock(0, self.num);
        [self hide];
    }
}



- (void)dimiss{
    
    [self hide];
}

- (NumView *)numView{
    if (!_numView) {
        _numView = [NumView new];
        WeakSelf(self);
        _numView.AddBlock = ^(UILabel * _Nonnull countLabel) {
//            NSInteger value = [countLabel.text integerValue];
//            NSLog(@"%ld",(long)value);
            weakself.num = [countLabel.text integerValue];
        };
        _numView.CutBlock = ^(UILabel * _Nonnull countLabel) {
            weakself.num = [countLabel.text integerValue];
        };
//        _numView.backgroundColor = [UIColor redColor];
    }
    return _numView;
}




-(UIButton *)confirm
{
    if(!_confirm)
    {
        _confirm=[UIButton  new];
        [_confirm setTitle:@"确定" forState:UIControlStateNormal];
        _confirm.titleLabel.font = LabelFont14;
        [_confirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _confirm.backgroundColor = UIColor276;
        [_confirm addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _confirm;
}

@end
