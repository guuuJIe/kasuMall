//
//  TuanGouCell.m
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "TuanGouCell.h"
#import "ProgressBarView.h"
@interface TuanGouCell()
@property (nonatomic) ProgressBarView *barView;
@property (nonatomic , strong) UIImageView *goodsImageView;
@property (nonatomic , strong) UILabel *priceLbl;
@property (nonatomic , strong) UILabel *goodsTitleLabel;
@property (nonatomic , strong) UILabel *stateLabel;
@property (nonatomic , strong) UILabel *orgpriceLbl;
@property (nonatomic , strong) UILabel *qianggouLbl;
@end

@implementation TuanGouCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = UIColorF5F7;
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    
    
    UIView *bg = [UIView new];
    bg.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bg];
    bg.layer.cornerRadius = 10;
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(136);
        make.top.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    [bg addSubview:self.goodsImageView];
    [self.goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(12));
        make.centerY.equalTo(bg);
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    [bg addSubview:self.goodsTitleLabel];
    [self.goodsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodsImageView);
        make.left.equalTo(self.goodsImageView.mas_right).offset(12);
        make.right.equalTo(@(-12));
    }];
    
    [bg addSubview:self.stateLabel];
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodsTitleLabel.mas_bottom).offset(7);
        make.left.equalTo(self.goodsTitleLabel);
        make.size.mas_equalTo(CGSizeMake(46, 15));
    }];
    
    [bg addSubview:self.priceLbl];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodsTitleLabel);
        make.top.equalTo(self.stateLabel.mas_bottom).mas_equalTo(7);
    }];
    
    [bg addSubview:self.orgpriceLbl];
    [self.orgpriceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceLbl.mas_right).offset(15);
        make.bottom.equalTo(self.priceLbl);
    }];
    
    [bg addSubview:self.barView];
    [self.barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodsTitleLabel);
        make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(7);
        make.size.mas_equalTo(CGSizeMake(100, 13));
    }];
    
    [bg addSubview:self.qianggouLbl];
    [self.qianggouLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bg).offset(-12);
        make.centerY.equalTo(self.barView).offset(0);
        make.size.mas_equalTo(CGSizeMake(70*AdapterScal, 24*AdapterScal));
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"imageUrl"]];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"sPrice"]];
    self.goodsTitleLabel.text = [NSString stringWithFormat:@"%@",dic[@"title"]];
    self.orgpriceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    CGFloat salls = [dic[@"groupNumber"] floatValue];
    CGFloat total = [dic[@"number"] floatValue];
    CGFloat radio = salls/total;
    CGFloat width = 0;
    if (radio >= 1) {
        width = 100;
        radio = 1;
    }else{
        width = (salls/total)*100;
    }
    
//    self.barView.frame = CGRectMake(124, 40, 100, 13);
    [UIView animateWithDuration:1 animations:^{
          self.barView.gradientView.frame = CGRectMake(0, 0, width, 13);
    }];
  

    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.barView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(7, 7)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.barView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.barView.layer.mask = maskLayer;
    NSString *str = @"%";
    self.barView.numLabel.text = [NSString stringWithFormat:@"已售%.f%@",radio*100,str];
    NSInteger type = [dic[@"type"] integerValue];
    if (type == 1) {
        self.stateLabel.text = @"限时团购";
        self.stateLabel.textColor = UIColorFF3E;
        self.stateLabel.layer.borderColor = UIColorFF3E.CGColor;
    }else if (type == 3){
        self.stateLabel.text = @"分期返还";
        self.stateLabel.textColor =UIColorFFA1;
        self.stateLabel.layer.borderColor = UIColorFFA1.CGColor;
    }else if (type == 2){
        self.stateLabel.text = @"限时秒杀";
    }
    
    NSInteger statues = [dic[@"status"] integerValue];
    if (statues == 1) {
        self.qianggouLbl.text = @"即将开抢";
        self.barView.gradientView.backgroundColor = UIColor276;
        self.barView.bgView.backgroundColor = UIColorAE;
        self.qianggouLbl.backgroundColor = UIColor276;
    }else if (statues == 2){
        self.qianggouLbl.text = @"团购结束";
        self.qianggouLbl.backgroundColor = UIColorCC;
        self.barView.gradientView.backgroundColor = UIColorFF3E;
        self.barView.bgView.backgroundColor = UIColorFFD1;
    }else if (statues == 0){
        self.qianggouLbl.text = @"去抢购";
        self.qianggouLbl.backgroundColor = UIColorFF3E;
        self.barView.gradientView.backgroundColor = UIColorFF3E;
        self.barView.bgView.backgroundColor = UIColorFFD1;
    }
}



- (ProgressBarView *)barView{
    if (!_barView) {
        _barView = [[ProgressBarView alloc] initWithFrame:CGRectMake(0, 0, 100, 13)];
        _barView.layer.cornerRadius = 8;
        [self.contentView addSubview:_barView];
    }
    return _barView;
}

-(UIImageView *)goodsImageView
{
    if(!_goodsImageView)
    {
        _goodsImageView=[UIImageView  new];
        _goodsImageView.image = [UIImage imageNamed:@"DefaultImage"];
//        _goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _goodsImageView;
}

- (UILabel *)goodsTitleLabel
{
    if(!_goodsTitleLabel)
    {
        _goodsTitleLabel=[UILabel  new];
        _goodsTitleLabel.font = LabelFont14;
        _goodsTitleLabel.textColor = [UIColor colorWithHexString:@"333333"];
        _goodsTitleLabel.numberOfLines = 2;
        _goodsTitleLabel.text = @"";
        //        _goodsTitleLabel.alpha = 0.2;
    }
    return _goodsTitleLabel;
}

- (UILabel *)stateLabel
{
    if(!_stateLabel)
    {
        _stateLabel=[UILabel  new];
        _stateLabel.font = [UIFont systemFontOfSize:9];
        _stateLabel.textColor = APPColor;
        _stateLabel.numberOfLines = 1;
        _stateLabel.text = @"限时团购";
        _stateLabel.layer.cornerRadius = 7;
        _stateLabel.layer.borderWidth = 1;
        _stateLabel.layer.borderColor = APPColor.CGColor;
        _stateLabel.textAlignment = NSTextAlignmentCenter;
       
    }
    return _stateLabel;
}


- (UILabel *)qianggouLbl
{
    if(!_qianggouLbl)
    {
        _qianggouLbl=[UILabel  new];
        _qianggouLbl.font = [UIFont systemFontOfSize:13];
        _qianggouLbl.textColor = [UIColor whiteColor];
        _qianggouLbl.numberOfLines = 1;
        _qianggouLbl.text = @"去抢购";
        _qianggouLbl.layer.cornerRadius = 12;
//        _qianggouLbl.layer.borderWidth = 1;
//        _qianggouLbl.layer.borderColor = APPColor.CGColor;
        _qianggouLbl.layer.masksToBounds = true;
        _qianggouLbl.textAlignment = NSTextAlignmentCenter;
        _qianggouLbl.backgroundColor = FF3E3E;
        
    }
    return _qianggouLbl;
}

-(UILabel *)priceLbl
{
    if(!_priceLbl)
    {
        _priceLbl=[UILabel  new];
        _priceLbl.font = LabelFont15;
        _priceLbl.textColor = FF3E3E;
        _priceLbl.text = @"¥122";
    }
    return _priceLbl;
}

-(UILabel *)orgpriceLbl
{
    if(!_orgpriceLbl)
    {
        _orgpriceLbl=[UILabel  new];
        _orgpriceLbl.font = LabelFont10;
        _orgpriceLbl.textColor = UIColorBF;
        _orgpriceLbl.text = @"¥152";
    }
    return _orgpriceLbl;
}

@end
