//
//  ShopcarGoodsModel.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ItemModel : NSObject

@property (nonatomic , assign) NSInteger proId;
@property (nonatomic , copy) NSString *proName;
@property (nonatomic , copy) NSString *proSpecification;
@property (nonatomic , copy) NSString *model;
@property (nonatomic , assign) NSInteger proQuantity;
@property (nonatomic , assign) NSInteger proMinQuantity;
@property (nonatomic , assign) CGFloat proPrice;
@property (nonatomic , copy) NSString *proPic;
@property (nonatomic , assign) NSInteger cartId;
@property (nonatomic , assign) BOOL isSelected;
@end

@interface ShopcarGoodsModel : NSObject

@property (nonatomic,assign) NSInteger shopId;
@property (nonatomic,strong) NSString *shopName;
@property (nonatomic,assign) CGFloat sPrice;
@property (nonatomic,strong) NSMutableArray *cartProducts;

@property (nonatomic,assign) CGFloat freight;
@end

NS_ASSUME_NONNULL_END
