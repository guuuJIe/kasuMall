//
//  ShopcarOrderSubmitVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShopcarOrderSubmitVC : BaseViewController
@property (nonatomic,strong) NSDictionary *data;
@property (nonatomic,strong) NSString *cartsID;
@property (nonatomic,strong) NSString *addressId;
@property (nonatomic,strong) NSString *totalPrice;
@end

NS_ASSUME_NONNULL_END
