//
//  ShopcarVC.m
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopcarVC.h"
#import "CarHeadCell.h"
#import "CarItemCell.h"
#import "CarFootView.h"
#import "CarPayView.h"
#import "ShopcarGoodsModel.h"
#import "SubmitOrderVC.h"
#import "CarApi.h"
#import "CarHeadView.h"
#import "ShopcarGoodsModel.h"
#import "ShopcarOrderSubmitVC.h"
#import "OrderApi.h"
#import "NoDataView.h"
@interface ShopcarVC ()<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) CarPayView *payView;
@property (nonatomic)OrderApi *orderApi;
/**
 总存储数组
 */
@property (nonatomic, strong) NSMutableArray *storeArray;
/**
 选中的数组
 */
@property (nonatomic, strong) NSMutableArray *selectArray;
@property (nonatomic, strong) NSMutableArray *itemArray;
@property (nonatomic, strong) CarApi *carApi;
//总价
@property (nonatomic,strong) NSString *totalPrice;

@property (nonatomic,strong) NSString *cids;
@end

@implementation ShopcarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"购物车";
    
    [self setupUI];
    
//    [self setData];
    [self getCarData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(OtherVCNotifyCarData) name:refreshShopCart object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置navbar
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = nil;
    self.view.backgroundColor = UIColorEF;
}



- (void)setupUI{
    self.view.backgroundColor = UIColorEF;
    [self.view addSubview:self.TabelView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(12);
        make.bottom.mas_equalTo(-50);
        make.left.equalTo(@12);
        make.right.mas_equalTo(-12);
    }];
    
    [self.payView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    
//    self.payView.submitBlock = ^{
//        NSLog(@"111");
//    };
}

- (void)getCarData{
//    NSOperationQueue
    [self countPrice];
    [self.carApi shopcarListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
//            NSDictionary *dic = result.resultDic;
            NSArray *array = result.result; //nil
            if (array.count == 0) {
                [self setData:@[]];
            }else{
                NSDictionary *shop = array.firstObject;
                NSArray *shops = shop[@"cartShops"];
                [self setData:shops];
            }
           
            
        }else{
            [self.TabelView reloadEmptyDataSet];
        }
    }];
}
- (void)OtherVCNotifyCarData{
    [self.selectArray removeAllObjects];
    [self countPrice];
    [self.carApi shopcarListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *array = result.result; //nil
            if (array.count == 0) {
                [self setData:@[]];
            }else{
                NSDictionary *shop = array.firstObject;
                NSArray *shops = shop[@"cartShops"];
                [self setData:shops];
            }
            
        }else{
            NSArray *array = result.result; //nil
            if (array.count == 0) {
                [self setData:@[]];
            }
//            if ([array.firstObject isEqual:[NSNull null]]) {
//
//            }
          
        }
    }];
}

- (void)setData:(NSArray *)data{
//    for (int i = 0; i<data.count; i++) {
//        NSDictionary *dic = data[i];
//        [self.storeArray addObject:dic];
//    }
    
    self.storeArray = [ShopcarGoodsModel mj_objectArrayWithKeyValuesArray:data];
//
    [self.TabelView reloadData];
    
    [self.TabelView reloadEmptyDataSet];
    
//    [self.TabelView.mj_header endRefreshing];
}

#pragma mark ---UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.storeArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSDictionary *dic = self.storeArray[section];
    //    NSArray *array = dic[@"cartProducts"];
    //    if (self.itemArray.count != 0) {
    ////        ShopcarGoodsModel *model = self.storeArray[section];
    ////           self.itemArray = model.cartProducts;
    //           return self.itemArray.count;
    //    }else{
    ShopcarGoodsModel *model = self.storeArray[section];
//    self.itemArray = model.cartProducts;
    return model.cartProducts.count;
    //    }
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *Identifier = @"CarItemCell";
    CarItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[CarItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
//    if (self.itemArray) {
//        ItemModel *item = self.itemArray[indexPath.row];
//        cell.itemModel = item;
//        [self shoppingCartCellClickAction:cell goodsModel:item indexPath:indexPath];
//    }else{
        ShopcarGoodsModel *model = self.storeArray[indexPath.section];
        ItemModel *item = model.cartProducts[indexPath.row];
        cell.itemModel = item;
        [self shoppingCartCellClickAction:cell goodsModel:item indexPath:indexPath];
//    }
    

   
    return cell;

    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CarHeadView *view = [CarHeadView new];
    view.goodsModel = self.storeArray[section];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (section == 1) {
        return 30.0f;
//    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    if (section == 1) {
        return [CarFootView new];
//    }
//    return [UIView new];
}

//左滑删除
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return true;
}
// 定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ShopcarGoodsModel *model = self.storeArray[indexPath.section];
        ItemModel *item = model.cartProducts[indexPath.row];
        
//        [self deleteData:[NSString stringWithFormat:@"%ld",(long)item.cartId]];
        
        [self deleteData:item andNSIndexPath:indexPath];
    }
}

// 修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

-(void)deleteData:(ItemModel *)model andNSIndexPath:(NSIndexPath *)indexpath{
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该商品" preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [self delGoods:model and:indexpath];
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)delGoods:(ItemModel *)model and:(NSIndexPath *)path{
    [self.carApi shopcarDataDelWithparameters:@{} andID:[NSString stringWithFormat:@"%ld",(long)model.cartId] withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
         
            if ([self.selectArray containsObject:model]) {
                [self.selectArray removeObject:model];
                [self countPrice];
            }
            
            ShopcarGoodsModel *Carmodel = self.storeArray[path.section];
            self.itemArray = Carmodel.cartProducts;
            [self.itemArray replaceObjectAtIndex:path.row withObject:model];
            Carmodel.cartProducts = self.itemArray;
            [self.storeArray replaceObjectAtIndex:path.section withObject:Carmodel];
            
            [self.itemArray removeObject:model];
            if (self.itemArray.count == 0) {
                [self.storeArray removeObjectAtIndex:path.section];
            }
            
            [self.TabelView reloadData];
            [self.TabelView reloadEmptyDataSet];
        }
    }];
}

- (void)submitOrder{
    NSMutableArray *dataArr = [NSMutableArray new];
    for (int i = 0; i<self.selectArray.count; i++) {
        ItemModel *model = self.selectArray[i];
        [dataArr addObject:[NSString stringWithFormat:@"%ld",(long)model.cartId]];
    }
    NSString *cids = [dataArr componentsJoinedByString:@","];//#为分隔符
    self.cids = cids;
    [self.orderApi getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            
            if (arr.count != 0) {
                NSDictionary *dic = arr.firstObject;
                [self jumpToOrderVC:dic WithCids:cids];
                
                
            }else{
                [self jumpToOrderVC:@{@"id":@0} WithCids:cids];
            }
        }
        
        
    }];
    
}

- (void)jumpToOrderVC:(NSDictionary *)dic WithCids:(NSString *)cids{
    [self.carApi shopcarSubmitOrderWithparameters:@{@"Cids":cids,@"Address":dic[@"id"]} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            ShopcarOrderSubmitVC *vc = [ShopcarOrderSubmitVC new];
            NSArray *data = result.result;
            NSDictionary *dicData =data.firstObject;
            NSArray *array = dicData[@"cartShops"];
            NSDictionary *priceDic = array.firstObject;
            vc.data = data.firstObject;
            vc.cartsID = cids;
            vc.addressId = dic[@"id"];
            CGFloat freight = [priceDic[@"freight"] floatValue];
            CGFloat price = [priceDic[@"sPrice"] floatValue];
            
            CGFloat total = freight + price;
            self.totalPrice = [NSString stringWithFormat:@"%.2f",total];
            vc.totalPrice = self.totalPrice;
            [self.navigationController pushViewController:vc animated:true];
        }
        
    }];
}

#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NoDataView *view = [NoDataView initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-115, 150, 150);
    view.desLbl.text = @"购物车空空如也～";
    return view;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{

//    [self.collectionView reloadNetworkDataSet];
//    [self getHomeData];
}

#pragma mark  ----------- Action 点击事件 --------------------
-(void)shoppingCartCellClickAction:(CarItemCell *)cell goodsModel:(ItemModel *)goodsModel indexPath:(NSIndexPath *)indexPath{
    
    cell.ClickRowBlock = ^(BOOL isClick) {
        goodsModel.isSelected = !goodsModel.isSelected;
        if (goodsModel.isSelected) {
           
           
            
            ShopcarGoodsModel *model = self.storeArray[indexPath.section];
            self.itemArray = model.cartProducts;
            [self.itemArray replaceObjectAtIndex:indexPath.row withObject:goodsModel];
            model.cartProducts = self.itemArray;
            [self.storeArray replaceObjectAtIndex:indexPath.section withObject:model];
           
            if ([self.selectArray containsObject:goodsModel]) {
                for (int i = 0; i<self.selectArray.count; i++) {
                    ItemModel *selectmodel = self.selectArray[i];
                    if (goodsModel.cartId == selectmodel.cartId) {
                        [self.selectArray replaceObjectAtIndex:i withObject:goodsModel];
                    }
                }
               
            }else{
                [self.selectArray addObject:goodsModel];
            }
            
            

        }else{
            if ([self.selectArray containsObject:goodsModel]) {
                [self.selectArray removeObject:goodsModel];
            }
            
        }
        
        [self countPrice];
        
    };
    
    cell.AddBlock = ^(UILabel *countLabel) {
        [self updateGoodsNumOperation:goodsModel withNum:countLabel.text andNSIndexpath:indexPath];
        
    };
    
    cell.CutBlock = ^(UILabel *countLabel) {
        [self updateGoodsNumOperation:goodsModel withNum:countLabel.text andNSIndexpath:indexPath];
    };
    
}

- (void)countPrice{
     CGFloat totalPrice = 0.0;
    for (int i = 0; i<self.selectArray.count; i++) {
        ItemModel *model = self.selectArray[i];
//        CGFloat totalPrice = 0.0;
        CGFloat itemPirce = model.proPrice * model.proQuantity;
        totalPrice += itemPirce;
    }
    self.payView.numLbl.text = [NSString stringWithFormat:@"已选(%lu)",(unsigned long)self.selectArray.count];
    self.payView.priceLbl.text = [NSString stringWithFormat:@"%.2f",totalPrice];
    
}

/**
 购物车修改
 */
- (void)updateGoodsNumOperation:(ItemModel *)model withNum:(NSString *)num andNSIndexpath:(NSIndexPath *)indexpath
{
    
    [JMBManager showLoading];
    [self.carApi shopcarDataChangeWithparameters:@{@"num":num} andNum:num andID:[NSString stringWithFormat:@"%ld",(long)model.cartId] withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [JMBManager hideAlert];
            ItemModel *tempModel = model;
            tempModel.proQuantity = [num integerValue];
            ShopcarGoodsModel *carmodel = self.storeArray[indexpath.section];
            self.itemArray = carmodel.cartProducts;
            [self.itemArray replaceObjectAtIndex:indexpath.row withObject:model];
            carmodel.cartProducts = self.itemArray;
            [self.storeArray replaceObjectAtIndex:indexpath.section withObject:carmodel];
            for (int i = 0; i<self.selectArray.count; i++) {
                ItemModel *selectmodel = self.selectArray[i];
                if (model.cartId == selectmodel.cartId) {
                    
                    if ([self.selectArray containsObject:tempModel]) {
                        tempModel.isSelected = true;
                        [self.selectArray replaceObjectAtIndex:i withObject:tempModel];
                        [self countPrice];
                    }
                    
                }
            }
            [self.TabelView reloadData];
            [self.TabelView reloadEmptyDataSet];
        }
    }];
}

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setEmptyDataSetSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        //        [_TabelView registerClass:[RepairFuncCell class] forCellReuseIdentifier:@"RepairFuncCell"];
        [_TabelView registerNib:[UINib nibWithNibName:@"CarHeadCell" bundle:nil] forCellReuseIdentifier:@"CarHeadCell"];
        ////
        [_TabelView registerNib:[UINib nibWithNibName:@"CarItemCell" bundle:nil] forCellReuseIdentifier:@"CarItemCell"];
//        WeakSelf(self)
//        _TabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            [weakself getCarData];
//        }];
        _TabelView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

-(CarPayView *)payView{
    if (!_payView) {
        _payView = [CarPayView new];
        WeakSelf(self);
        _payView.submitBlock = ^{
            if (weakself.selectArray.count == 0) {
                [JMBManager showBriefAlert:@"请选择商品"];
                return ;
            }
            
            
            [weakself submitOrder];
            
        };
        [self.view addSubview:_payView];
    }
    return _payView;
}

- (NSMutableArray *)storeArray{
    if (!_storeArray) {
        _storeArray = [NSMutableArray array];
    }
    return _storeArray;
}

- (NSMutableArray *)selectArray{
    if (!_selectArray) {
        _selectArray = [NSMutableArray new];
    }
    return _selectArray;
}

- (NSMutableArray *)itemArray{
    if (!_itemArray) {
        _itemArray = [NSMutableArray new];
    }
    return _itemArray;
}

- (CarApi *)carApi{
    if (!_carApi) {
        _carApi =[CarApi new];
    }
    return _carApi;
}

- (OrderApi *)orderApi{
    if (!_orderApi) {
        _orderApi = [OrderApi new];
        
    }
    return _orderApi;
}

@end
