//
//  ShopCarSubmitOrderCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopcarGoodsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ShopCarSubmitOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;

@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (nonatomic,strong) ItemModel *itemModel;
@end

NS_ASSUME_NONNULL_END
