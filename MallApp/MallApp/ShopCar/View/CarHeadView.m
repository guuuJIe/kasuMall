//
//  CarHeadView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarHeadView.h"
@interface CarHeadView()
@property (nonatomic) UILabel *goodTitleLabel;
@end
@implementation CarHeadView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width-24, 40)];
    [self addSubview:bg];
    bg.backgroundColor = [UIColor whiteColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:bg.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bg.bounds;
    maskLayer.path = maskPath.CGPath;
    bg.layer.mask = maskLayer;
    
    [self addSubview:self.goodTitleLabel];
    [self.goodTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@12);
        make.centerY.mas_equalTo(self);
    }];
    
}

- (void)setGoodsModel:(ShopcarGoodsModel *)goodsModel{
    _goodsModel = goodsModel;
    self.goodTitleLabel.text = goodsModel.shopName;
}




-(UILabel *)goodTitleLabel
{
    if(!_goodTitleLabel){
        _goodTitleLabel = [UILabel new];
        _goodTitleLabel.textColor = UIColor333;
        _goodTitleLabel.font = LabelFont14;
        _goodTitleLabel.numberOfLines = 1;
        _goodTitleLabel.text = @"卡速自营";
        
    }
    return _goodTitleLabel;
}

@end
