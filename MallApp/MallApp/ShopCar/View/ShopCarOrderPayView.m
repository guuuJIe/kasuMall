//
//  ShopCarOrderPayView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopCarOrderPayView.h"

@implementation ShopCarOrderPayView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        ShopCarOrderPayView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"ShopCarOrderPayView" owner:self options:nil] lastObject];
        self = lastView;
        [lastView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self);
            make.height.equalTo(@(50+BottomAreaHeight));
        }];
    }
    return self;
}
- (IBAction)orderPayAct:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}
@end
