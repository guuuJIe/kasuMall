//
//  ShopcarOrderFootView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopcarOrderFootView : UIView
@property (weak, nonatomic) IBOutlet UIView *bottom;
@property (weak, nonatomic) IBOutlet UILabel *freightLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *fapiaoBtn;
- (IBAction)fabiaoAct:(UIButton *)sender;

@property (nonatomic, copy) void (^cilckBlock)(void);
@property (weak, nonatomic) IBOutlet UILabel *fapiaoLbl;
@end

NS_ASSUME_NONNULL_END
