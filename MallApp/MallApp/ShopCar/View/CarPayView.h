//
//  CarPayView.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CarPayView : UIView

- (IBAction)Submit:(UIButton *)sender;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, copy) void (^submitBlock)(void);
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;

@end

NS_ASSUME_NONNULL_END
