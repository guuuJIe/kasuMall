//
//  ShopCarSubmitOrderCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopCarSubmitOrderCell.h"

@implementation ShopCarSubmitOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setItemModel:(ItemModel *)itemModel{
    if (!itemModel) {
        return;
    }
//    if (!_itemModel) {
//        return;
    //    }
    _itemModel = itemModel;
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,itemModel.proPic];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    self.titleLbl.text = itemModel.proName;
    self.subTitleLbl.text = itemModel.model;
    self.priceLbl.text = [NSString stringWithFormat:@"¥%.2f",itemModel.proPrice];
    
    self.numLbl.text = [NSString stringWithFormat:@"x%ld",(long)itemModel.proQuantity];
    
}
@end
