//
//  ShopCarOrderPayView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopCarOrderPayView : UIView
- (IBAction)orderPayAct:(UIButton *)sender;
@property (nonatomic, copy) void (^clickBlock)(void);
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@end

NS_ASSUME_NONNULL_END
