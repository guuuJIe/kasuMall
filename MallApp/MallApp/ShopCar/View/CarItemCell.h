//
//  CarItemCell.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopcarGoodsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CarItemCell : UITableViewCell
- (IBAction)selectAct:(UIButton *)sender;
- (IBAction)AddAct:(UIButton *)sender;
- (IBAction)SubAct:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *goodImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *subLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *subBtn;
@property (weak, nonatomic) IBOutlet UILabel *goodNumLbl;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
/**
 选中
 */
@property (nonatomic, copy) void (^ClickRowBlock)(BOOL isClick);
/**
 加
 */
@property (nonatomic, copy) void (^AddBlock)(UILabel *countLabel);

/**
 减
 */
@property (nonatomic, copy) void (^CutBlock)(UILabel *countLabel);
@property (weak, nonatomic) IBOutlet UIButton *selectbtn;

@property (nonatomic,strong) ItemModel *itemModel;
@end

NS_ASSUME_NONNULL_END
