//
//  ShopCarAddressView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopCarAddressView : UIView
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *addAdress;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *adressLbl;
@property (nonatomic,copy) void(^clickBlock)(NSString *flag);
@property (weak, nonatomic) IBOutlet UIImageView *locImage;
@property (weak, nonatomic) IBOutlet UILabel *usernameLbl;

- (void)setupData:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
