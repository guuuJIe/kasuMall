//
//  CarItemCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarItemCell.h"

@implementation CarItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectAct:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    if (self.ClickRowBlock) {
        self.ClickRowBlock(sender.selected);
    }
}

- (IBAction)AddAct:(UIButton *)sender {
    NSInteger count = [self.numLbl.text integerValue];
    count++;
    self.numLbl.text = [NSString stringWithFormat:@"%ld", (long)count];
    if (self.AddBlock) {
        self.AddBlock(self.numLbl);
    }
    
}

- (IBAction)SubAct:(UIButton *)sender {
    NSInteger count = [self.numLbl.text integerValue];
    count--;
    if (count <= 0) {
        return;
    }
    self.numLbl.text = [NSString stringWithFormat:@"%ld", (long)count];
    if (self.CutBlock) {
        self.CutBlock(self.numLbl);
    }
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"proPic"]];
    [self.goodImg yy_setImageWithURL:URL(url) options:0];
    self.nameLbl.text = dic[@"proName"];
    self.subLbl.text = dic[@"model"];
    self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"proPrice"]];
    self.numLbl.text = [NSString stringWithFormat:@"%@",dic[@"proQuantity"]];
    self.goodNumLbl.text = [NSString stringWithFormat:@"x%@",dic[@"proQuantity"]];
}

- (void)setItemModel:(ItemModel *)itemModel{
    if (!itemModel) {
        return;
    }
//    if (!_itemModel) {
//        return;
//    }
    _itemModel = itemModel;
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,itemModel.proPic];
       [self.goodImg yy_setImageWithURL:URL(url) options:0];
       self.nameLbl.text = itemModel.proName;
       self.subLbl.text = itemModel.model;
    self.priceLbl.text = [NSString stringWithFormat:@"¥%.2f",itemModel.proPrice];
    self.goodNumLbl.text = [NSString stringWithFormat:@"x%ld",(long)itemModel.proQuantity];
    self.numLbl.text = [NSString stringWithFormat:@"%ld",(long)itemModel.proQuantity];
    if (itemModel.isSelected) {
        self.selectbtn.selected = true;
    }else{
        self.selectbtn.selected = false;
    }
}
@end
