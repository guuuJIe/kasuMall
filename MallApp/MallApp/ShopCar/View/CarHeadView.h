//
//  CarHeadView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopcarGoodsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CarHeadView : UIView

@property (nonatomic) ShopcarGoodsModel *goodsModel;

@end

NS_ASSUME_NONNULL_END
