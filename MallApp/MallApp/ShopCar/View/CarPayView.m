//
//  CarPayView.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarPayView.h"

@implementation CarPayView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        CarPayView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"CarPayView" owner:self options:nil] lastObject];
        [self addSubview:lastView];
        self = lastView;
//        self.submitBlock()
        [lastView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self);
            make.height.equalTo(@50);
        }];
    }
    return self;
}

- (IBAction)Submit:(UIButton *)sender {
    
    if (self.submitBlock) {
        self.submitBlock();
    }
}
@end
