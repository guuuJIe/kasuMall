//
//  CarHeadCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarHeadCell.h"

@implementation CarHeadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorEF;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.bgView.frame = CGRectMake(0, 0, Screen_Width - 24, 50);
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 24, 50)];
//    [self.contentView addSubview:view];
//    view.backgroundColor = [UIColor clearColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bgView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.bgView.layer.mask = maskLayer;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)allSelect:(UIButton *)sender {
    NSLog(@"点击了");
}

@end
