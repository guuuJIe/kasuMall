//
//  ShopcarOrderFootView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopcarOrderFootView.h"

@implementation ShopcarOrderFootView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        ShopcarOrderFootView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"ShopcarOrderFootView" owner:self options:nil] lastObject];
        [self addSubview:lastView];
        lastView.bounds = self.bounds;
        self = lastView;
        

        self.userInteractionEnabled = true;
//        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
//
//        self.bottom.frame = CGRectMake(0, 0, Screen_Width, 10);
//
//        self.bottom.backgroundColor = [UIColor redColor];
       UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii:CGSizeMake(8, 8)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
        [self.fapiaoLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];

    }
    return self;
}

- (void)click{
    if (self.cilckBlock) {
           self.cilckBlock();
       }
}

- (IBAction)fabiaoAct:(UIButton *)sender {
    if (self.cilckBlock) {
        self.cilckBlock();
    }
}
@end
