//
//  ShopCarAddressView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopCarAddressView.h"

@implementation ShopCarAddressView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        ShopCarAddressView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"ShopCarAddressView" owner:self options:nil] lastObject];
        [self addSubview:lastView];
        self = lastView;
        self.layer.cornerRadius = 8;
        self.userInteractionEnabled = true;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
//        [lastView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.top.equalTo(self);
//            make.height.equalTo(@50);
//        }];
    }
    return self;
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    self.usernameLbl.hidden = false;
    self.adressLbl.hidden = false;
    self.telLbl.hidden = false;
    self.addAdress.hidden = true;
    self.locImage.hidden = false;
    
    self.usernameLbl.text = dic[@"name"];
    self.adressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",dic[@"strProvince"],dic[@"strCity"],dic[@"strArea"],dic[@"street"]];
    self.telLbl.text = dic[@"mobile"];
}

- (void)click{
    if (self.clickBlock) {
        self.clickBlock(@"");
    }
}

@end
