//
//  ShopcarOrderSubmitVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopcarOrderSubmitVC.h"
#import "ShopCarAddressView.h"
#import "ShopcarOrderHeadView.h"
#import "ShopcarOrderFootView.h"
#import "CarItemCell.h"
#import "OrderApi.h"
#import "ShopcarGoodsModel.h"
#import "ShopCarOrderPayView.h"
#import "SelectPayWayVC.h"
#import "AdressListVC.h"
#import "CarApi.h"
#import "FapiaoView.h"
#import "FapiaoVC.h"
#import "ShopCarSubmitOrderCell.h"
@interface ShopcarOrderSubmitVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *TabelView;
@property (nonatomic,strong) ShopCarAddressView *adressView;
@property (nonatomic,strong) OrderApi *api;
@property (nonatomic,strong) NSMutableArray *storeArray;
@property (nonatomic,strong) ShopCarOrderPayView *payView;
@property (nonatomic,assign) NSInteger invoiceID;
@property (nonatomic,strong) CarApi *carApi;
@property (nonatomic,strong) FapiaoView *fapiaoView;
@property (nonatomic,assign) BOOL isfirst;//默认设置一次发票
@property (nonatomic,strong) NSString *piaoType;
@end

@implementation ShopcarOrderSubmitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initProp];
    
    [self setupUI];
   
    [self setData];
   
}

- (void)initProp{
    self.invoiceID = 0;
    self.addressId = @"0";
    self.piaoType = @"";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.api getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            
            if (arr.count != 0) {
                NSDictionary *dic = arr.firstObject;
                self.addressId = dic[@"id"];
                [self.adressView setupData:dic];
            }
            
             [self getFreight];
        }
    }];
    
   
}

- (void)setupUI{
     self.title = @"提交订单";
    self.view.backgroundColor = UIColorEF;
    [self.view addSubview:self.adressView];
    [self.adressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@12);
        make.height.mas_equalTo(80);
        make.right.mas_equalTo(-12);
    }];
    
    [self.view addSubview:self.TabelView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.adressView.mas_bottom).offset(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.equalTo(self.view).offset(-50-BottomAreaHeight);
    }];
    
    [self.view addSubview:self.payView];
    [self.payView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    WeakSelf(self)
    self.adressView.clickBlock = ^(NSString * _Nonnull flag) {
        StrongSelf(self)
        AdressListVC *vc = [AdressListVC new];
        vc.setAdressBlock = ^(NSDictionary * _Nonnull dic) {
            self.addressId = [NSString stringWithFormat:@"%@",dic[@"id"]];
            [self getFreight];
        };
        [weakself.navigationController pushViewController:vc animated:true];
    };
}

- (void)setData{
    
//    [self.api getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//            NSDictionary *dic = result.resultDic;
//            NSArray *arr = dic[@"list"];
//
//            if (arr.count != 0) {
//                NSDictionary *dic = arr.firstObject;
//                self.addressId = dic[@"id"];
//                [self.adressView setupData:dic];
//            }
//        }
//    }];
    self.storeArray = [NSMutableArray array];
    
    NSArray *data = self.data[@"cartShops"];
    self.payView.priceLbl.text = [NSString stringWithFormat:@"¥%@",self.totalPrice];
    self.storeArray = [ShopcarGoodsModel mj_objectArrayWithKeyValuesArray:data];
    [self.TabelView reloadData];
    WeakSelf(self)
    self.payView.clickBlock = ^{
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setValue:weakself.addressId forKey:@"addressId"];
        [dic setValue:weakself.cartsID forKey:@"cartIds"];
        [dic setValue:@0 forKey:@"cardId"];
        [dic setValue:@0 forKey:@"payment"];//支付方式
        [dic setValue:@(weakself.invoiceID) forKey:@"invoiceId"];//发票ID
        [weakself.api createSingleOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                SelectPayWayVC *vc = [SelectPayWayVC new];
                vc.orderDataArr = result.resultDic[@"list"];
                vc.from = @"2";
                vc.totalPrice = weakself.totalPrice;
//                [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
                [weakself.navigationController pushViewController:vc animated:true];
            }
        }];
    };
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.storeArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ShopcarGoodsModel *model = self.storeArray[section];
   
    return model.cartProducts.count;
 
    
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"ShopCarSubmitOrderCell";
    ShopCarSubmitOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[ShopCarSubmitOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
 
    ShopcarGoodsModel *model = self.storeArray[indexPath.section];
    ItemModel *item = model.cartProducts[indexPath.row];
    cell.itemModel = item;
    
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ShopcarOrderHeadView *view = [ShopcarOrderHeadView new];
//    view.goodsModel = self.storeArray[section];
    view.goodsModel = self.storeArray[section];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    return 150;

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    ShopcarOrderFootView *footView = [[ShopcarOrderFootView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width-24, 150)];
//    footView.layer.cornerRadius = 8;
   

    ShopcarGoodsModel *model = self.storeArray[section];
    footView.freightLbl.text = [NSString stringWithFormat:@"¥%.2f",model.freight];
    footView.priceLbl.text = [NSString stringWithFormat:@"¥%.2f",model.sPrice];
    footView.cilckBlock = ^{
        [self.fapiaoView show];
    };
//    [footView.fapiaoBtn setTitle:[XJUtil insertStringWithNotNullObject:self.piaoType andDefailtInfo:@""] forState:0];
    footView.fapiaoLbl.text = [XJUtil insertStringWithNotNullObject:self.piaoType andDefailtInfo:@"不开发票"];
    return footView;
    
}


- (void)getFreight{
    [self.carApi shopcarSubmitOrderWithparameters:@{@"Cids":_cartsID,@"Address":self.addressId } withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
//            ShopcarOrderSubmitVC *vc = [ShopcarOrderSubmitVC new];
            NSArray *data = result.resultDic[@"list"];
            NSDictionary *dicData =data.firstObject;
            NSArray *array = dicData[@"cartShops"];
//            NSDictionary *priceDic = array.firstObject;
            __block CGFloat total = 0.0;
            self.storeArray = [ShopcarGoodsModel mj_objectArrayWithKeyValuesArray:array];
            [self.storeArray enumerateObjectsUsingBlock:^(ShopcarGoodsModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CGFloat freight = obj.freight;
                CGFloat price = obj.sPrice;
                total += (price+freight);
            }];
           
            
            self.totalPrice = [NSString stringWithFormat:@"%.2f",total];
            self.payView.priceLbl.text = [NSString stringWithFormat:@"¥%@",self.totalPrice];
            [self.TabelView reloadData];

        }
        
    }];
}

//左滑删除

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
      
        
        [_TabelView registerNib:[UINib nibWithNibName:@"ShopCarSubmitOrderCell" bundle:nil] forCellReuseIdentifier:@"ShopCarSubmitOrderCell"];
        
        _TabelView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (ShopCarAddressView *)adressView{
    if (!_adressView) {
        _adressView = [ShopCarAddressView new];
        WeakSelf(self);
//        _adressView.clickBlock = ^(NSString * _Nonnull flag) {
//
//        };
    }
    return _adressView;
}

- (FapiaoView *)fapiaoView{
    if (!_fapiaoView) {
        _fapiaoView = [FapiaoView new];
        WeakSelf(self);
        _fapiaoView.goclickBlock = ^{
            [weakself.fapiaoView hide];
            FapiaoVC *vc = [FapiaoVC new];
            weakself.isfirst = !weakself.isfirst;
            vc.clickBlock = ^(NSDictionary * _Nonnull dic) {
                [weakself.fapiaoView show];
                [weakself.fapiaoView setDic:dic];
            };

            [weakself.navigationController pushViewController:vc animated:true];
        };
        _fapiaoView.clickBlock = ^(NSDictionary * _Nonnull dic) {
            weakself.invoiceID = [dic[@"id"] integerValue];
            weakself.piaoType = dic[@"type"];
            [weakself.TabelView reloadData];
            [weakself.fapiaoView hide];
            
        };
    }
    return _fapiaoView;
}


- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}

- (CarApi *)carApi{
    if (!_carApi) {
        _carApi =[CarApi new];
    }
    return _carApi;
}

- (ShopCarOrderPayView *)payView{
    if (!_payView) {
        _payView = [ShopCarOrderPayView new];
    }
    return _payView;
}

@end
