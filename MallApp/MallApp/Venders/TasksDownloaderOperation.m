//
//  TasksDownloaderOperation.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/6/3.
//  Copyright © 2020 Mac. All rights reserved.
//



#import "TasksDownloaderOperation.h"
#import "HomePageApi.h"

@interface TasksDownloaderOperation()

@property (copy, nonatomic) ExcuteBlock excutedBlock;
@property (assign, nonatomic, getter = isFinished) BOOL finished;
//@property (readonly, getter=isFinished) BOOL finished;
@property (strong, nonatomic) HomePageApi *managers;
@end

@implementation TasksDownloaderOperation

@synthesize finished = _finished;

- (instancetype)initWithTask:(ExcuteBlock)block{
    if (self = [super init]) {
        _finished = NO;
//        _completedBlock = completionHander;
        _excutedBlock = block;
    }
    
    return self;
}


- (void)start{
    @synchronized (self) {
        if (self.isCancelled) {
            self.finished = true;
            [self reset];
            return;
        }
    }
    
    
    self.excutedBlock();
}

- (void)done {
    
    self.finished = YES;
    self.excutedBlock = nil;

}

- (void)reset{
    self.excutedBlock = nil;
}

- (void)setFinished:(BOOL)finished{
    [self willChangeValueForKey:@"isFinished"];
    _finished = finished;
    [self didChangeValueForKey:@"isFinished"];
}



@end
