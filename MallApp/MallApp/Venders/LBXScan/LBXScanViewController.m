//
//
//  
//
//  Created by lbxia on 15/10/21.
//  Copyright © 2015年 lbxia. All rights reserved.
//

#import "LBXScanViewController.h"
#import <ZBarSDK.h>
#import "LBXScanResult.h"
@interface LBXScanViewController ()


@end

@implementation LBXScanViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.view.backgroundColor = [UIColor blackColor];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self drawScanView];
    
    
    [self performSelector:@selector(startScan) withObject:nil afterDelay:0.2];
}

//绘制扫描区域
- (void)drawScanView
{
    if (!_qRScanView)
    {
        CGRect rect = self.view.frame;
        rect.origin = CGPointMake(0, 0);
        
        self.qRScanView = [[LBXScanView alloc]initWithFrame:rect style:_style];
        
        [self.view addSubview:_qRScanView];
    
    }
    
//      [_qRScanView startDeviceReadyingWithText:@"相机启动中"];
    
    
}

- (void)reStartDevice
{
    [_scanObj startScan];
}

- (void)stopDevice
{
    [_scanObj stopScan];
}

//启动设备
- (void)startScan
{
    if ( ![LBXScanWrapper isGetCameraPermission] )
    {
        [_qRScanView stopDeviceReadying];
        
        [self showError:@"   请到设置隐私中开启本程序相机权限   "];
        return;
    }
    
  
    
    if (!_scanObj )
    {
        __weak __typeof(self) weakSelf = self;
         // AVMetadataObjectTypeQRCode   AVMetadataObjectTypeEAN13Code
        
        CGRect cropRect = CGRectZero;
        
        if (_isOpenInterestRect) {
            
            cropRect = [LBXScanView getScanRectWithPreView:self.view style:_style];
        }
        
        UIView *videoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
        videoView.backgroundColor = [UIColor clearColor];
        [self.view insertSubview:videoView atIndex:0];

//        self.scanObj = [[LBXScanWrapper alloc]initWithPreView:videoView
//                                              ArrayObjectType:nil
//                                                     cropRect:cropRect
//                                                      success:^(NSArray<LBXScanResult *> *array){
//                                                          [weakSelf scanResultWithArray:array];
//                                                      }];
        if (!_scanObj) {
            self.scanObj = [[LBXScanWrapper alloc] initZXingWithPreView:videoView success:^(NSArray<LBXScanResult *> *array) {
                [weakSelf scanResultWithArray:array];
            }];
        }
        
//        if (!_zbarObj) {
//            self.zbarObj = [[LBXZBarWrapper alloc] initWithPreView:videoView barCodeType:ZBAR_I25 block:^(NSArray<LBXZbarResult *> *result) {
//                //测试，只使用扫码结果第一项
//                LBXZbarResult *firstObj = result[0];
//                LBXScanResult *scanResult = [[LBXScanResult alloc]init];
//                scanResult.strScanned = firstObj.strScanned;
//                scanResult.imgScanned = firstObj.imgScanned;
//                scanResult.strBarCodeType = [LBXZBarWrapper convertFormat2String:firstObj.format];
//
//                [weakSelf scanResultWithArray:@[scanResult]];
//            }];
//        }
        [_scanObj setNeedCaptureImage:_isNeedScanImage];
        
        [self cameraInitOver];
      
    }
    [_scanObj startScan];
//    [_zbarObj start];

    [_qRScanView stopDeviceReadying];
    
    [_qRScanView startScanAnimation];
    
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)cameraInitOver
{
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [_scanObj stopScan];
    [_qRScanView stopScanAnimation];
}



#pragma mark -实现类继承该方法，作出对应处理
- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    
}





//开关闪光灯
- (void)openOrCloseFlash
{
    [_scanObj openOrCloseFlash];
    
    self.isOpenFlash =!self.isOpenFlash;
    
}


#pragma mark --打开相册并识别图片

/*!
 *  打开本地照片，选择图片识别
 */
- (void)openLocalPhoto
{
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//
//    picker.delegate = self;
//
    
//    picker.allowsEditing = YES;
    
    
//    [self presentViewController:picker animated:YES completion:nil];
    
    ZBarReaderController *imagePicker = [ZBarReaderController new];
    imagePicker.showsHelpOnFail = NO; // 禁止显示读取失败页面
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
   
}



//当选择一张图片后进入这里

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    
    for(symbol in results) {
        
        break;
    }
   
    __weak __typeof(self) weakSelf = self;
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //二维码字符串
        NSString *QRCodeString =  symbol.data;
        LBXScanResult *result = [[LBXScanResult alloc] init];
        result.strScanned = QRCodeString;
        [weakSelf scanResultWithArray:@[result]];
       
        
    }];
  
    
//    [picker dismissViewControllerAnimated:YES completion:nil];
//
//    __block UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
//
//    if (!image){
//        image = [info objectForKey:UIImagePickerControllerOriginalImage];
//    }
//
//    __weak __typeof(self) weakSelf = self;
//    [LBXScanWrapper recognizeImage:image success:^(NSArray<LBXScanResult *> *array) {
//
//        [weakSelf scanResultWithArray:array];
//    }];
    
   
    
    //系统自带识别方法
    /*
     CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{ CIDetectorAccuracy : CIDetectorAccuracyHigh }];
     NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
     if (features.count >=1)
     {
     CIQRCodeFeature *feature = [features objectAtIndex:0];
     NSString *scanResult = feature.messageString;
     
     NSLog(@"%@",scanResult);
     }
     */
    
    
}

/**
*  读取二维码/条码失败的回调
*/
-(void)readerControllerDidFailToRead:(ZBarReaderController *)reader withRetry:(BOOL)retry{
    
    if (retry) { //retry == 1 选择图片为非二维码。
        [self dismissViewControllerAnimated:YES completion:^{
            
//            [MBProgressHUD showMessag:@"识别失败" toView:self.view];
        }];
        
    }
    return;
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"cancel");
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//子类继承必须实现的提示
- (void)showError:(NSString*)str
{
    
}

@end
