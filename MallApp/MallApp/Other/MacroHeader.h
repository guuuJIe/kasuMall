//
//  MacroHeader.h
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#ifndef MacroHeader_h
#define MacroHeader_h


//第三方头文件
#import "AFNetworking.h"
#import "MJRefresh.h"
#import "Masonry.h"
#import "MJExtension.h"
#import "UIView+AG_Frame.h"
#import "UIColor+HexString.h"
#import "UIImage+Extend.h"
#import "UIColor+Extend.h"
#import "MessageBody.h"
#import "JMBManager.h"
#import "XJUtil.h"
#import "MBProgressHUD.h"
#import "JSHttpClient.h"
#import "YYWebImage.h"
#import "JSCMMPopupViewTool.h"
#import "BaseNavigationController.h"
#import "UIView+CreateEle.h"
#import  "UIFont+SettingSize.h"
#import "UIScrollView+EmptyDataSet.h"
//@import Lottie;
//#import <Lottie/Lottie-Swift.h>

#endif /* MacroHeader_h */
