//
//  Api.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#ifndef Api_h
#define Api_h


#define appLogin @"/v1/login/UserPassLogin"
#define sendCodeApi @"/v1/Sms/SendSms"
#define codeLoginApi @"/v1/login/PhoneSmsLogin"
#define setPayPasswordApi @"/v1/login/Pay/RePasswrod"//忘记/设置支付密码
#define changePayPasswordApi @"/v1/login/Pay/RePasswrod/Old" //使用旧密码修改支付密码
#define appQuickLogin @"/v1/login/OneClickLogin" //一键登录
#define userRegisterApi @"/v1/login/UserPassRegister" //用户名密码注册
#define AppWxLoginApi @"/v1/login/AppWxLogin"
#define groupListApi @"/v1/Groups/List/0/0/1/6"
#define MallListApi @"/v1/Products/AutoParts/top/6"
#define cargoodsListApi @"/v1/Products/AutoAccessories/Top/6"
#define shopInfoApi @"/v1/Repairs/top/6"
#define productInfoApi @"/v1/Products/info/"
#define addIncarApi @"/v1/ShoppingCart/add"
#define purchaseApi @"/v1/ShoppingCart/Order"
#define addressListApi @"/v1/Member/AddressList"
#define refreshTokenApi @"/v1/login/RefreshToken"
#define bannerDataApi @"/v1/systems/homepic/6"
#define groupProductInfoApi @"/v1/Groups/Info/"
#define groupProductpurchaseApi @"/v1/Groups/Freight/"
#define invoiceListApi @"/v1/Member/InvoiceList"
#define addinvoiceApi @"/v1/Member/Invoice/Add"
#define defaultAddressApi @"/v1/Member/Address/SetDefault/"
#define defaultinVoiceApi @"/v1/Member/Invoice/SetDefault/"
#define createOrderApi @"/v1/Groups/CreateOrder"
#define createsingleOrderApi @"/v1/Orders/Create"
#define payOrderApi @"/v1/Pay/ReadyPay"
#define balanceApi @"/v1/Amount/Balance"
#define orderListApi @"/v1/Orders/GroupOrderList/"
#define normalOrderListApi @"/v1/Orders/OrderList/"
#define delAddressApi @"/v1/Member/Address/Del/"
#define delInvoiceApi @"/v1/Member/Invoice/Del/"
#define cancelOrder @"/v1/Groups/CancelOrder"
#define confirmGroupOrderApi @"/v1/Groups/CompleteDelivery"
#define confirmNormalOrderApi @"/v1/Orders/CompleteDelivery"
#define cancelNormalOrderApi @"/v1/Orders/CancelOrder"
#define addAddressApi @"/v1/Member/Address/Add"
#define getUserInfoApi @"/v1/Member/Info"
#define addIncarApi @"/v1/ShoppingCart/add"
#define carListApi @"/v1/ShoppingCart/list"
#define ChangeNum @"/v1/ShoppingCart/ChangeNum/"
#define delcarDataApi @"/v1/ShoppingCart/del/"
#define shopCarSumitOrder @"/v1/ShoppingCart/Order"
#define groupOrderInfoApi @"/v1/Orders/GroupOrderInfo/"
#define groupOrderInfoByOrderNumApi @"/v1/Orders/GroupOrderInfoByOrderNumber/"
#define goodsOrderInfoApi @"/v1/Orders/OrderInfo/"
#define storeInfoApi @"/v1/Shop/Index/"
#define storeListApi @"/v1/Repairs/List/"
#define getGroupListApi @"/v1/Groups/List/0/"
#define getProduceListApi @"/v1/Search/Product/"
#define getHotCarTypeList  @"/v1/Search/NewModel/Top/"
#define getAllCarList @"/v1/Search/NewModel/"
#define getCarTypeList @"/v1/Search/Type"
#define searchPropertytypeList @"/v1/Search/Property/"


#define rechargeBalancehApi @"/v1/Member/Cash/In"
#define getBrandListApi  @"/v1/Search/Brand"
#define getRegionListApi @"/v1/Member/Address/Region/"
#define getServiceProductListApi @"/v1/Shop/ServiceProductList/"
#define getExchangeRecordListApi @"/v1/Amount/List/30/"
#define getBackRecordListApi @"/v1/Groups/Stage/List/"
#define getCashApplyApi @"/v1/Member/Cash/Notout"


#define memberApplyWholesale @"/v1/Member/ApplyWholesale"
#define memberBuyertypeApi  @"/v1/Member/BuyerType"
#define MemberCardListApi @"/v1/Member/Card/List/"
#define delMemberCardApi @"/v1/Member/Card/Del/"
#define addMemberCardApi @"/v1/Member/Card/Edit"
#define uploadPicApi @"v1/Images/UpFile/IFormFiles/"//上传图片接口
#define newUploadPicApi @"v1/Images/UpFile/IFormFilesPath/"//新上传图片接口
/*
 Business业务代理中心
 */
#define getRecommendGoodsOrderListApi @"/v1/Business/Clerk/ProductOrder/"
#define getRecommendGroupGoodsOrderListApi @"/v1/Business/Clerk/GroupOrder/"
#define getRecommendFixOrderListApi @"/v1/Business/Clerk/RepairOrder/"
#define getDelegateInfoApi @"/v1/Business/Clerk/Info/"
#define getDelegateAuthorizeScope @"/v1/Business/Clerk/Authorize/Scope" //获取代理授权范围
#define memberInfoEditApi @"/v1/Member/Info/Edit"
#define clerkInfoEditApi @"/v1/Business/Clerk/Info/Edit" //编辑业务代理信息
#define delegateListApi @"/v1/Business/Clerk/"
#define bussinessClerkProjectListApi @"/v1/Business/Clerk/Project/List/" //获取指定业务代理的推广项目列表
#define bussinessClerkProjectInfoApi @"/v1/Business/Clerk/Project/Info/" //获取指定业务代理的推广项目详情
#define customerAuthorizeApi @"/v1/Business/Clerk/Authorize"
#define checkmyServiceManagerApi @"/v1/Member/ServiceManager/Info"
#define bindmyServiceManagerApi @"/v1/Member/ServiceManager/Set"
#define applyGrouper @"/v1/Business/Clerk/Project/Groupor/Apply"//用户申请成为团长
#define userProjectList @"/v1/Business/Clerk/Project/User/" //用户自己的推广项目库
#define systemProjectList @"/v1/Business/Clerk/Project/System/" //系统推广项目库
#define projectOrderList @"/v1/Business/Clerk/Project/Order/List/" //推广项目订单佣金列表
#define addPromptProject @"/v1/Business/Clerk/Project/Add" //用户从项目库里选择项目
#define delPromptProject @"/v1/Business/Clerk/Project/Delete" //用户从项目库中删除指定项目
#define getInviteInfo @"/v1/Business/Clerk/Project/Groupor/Inviters/List" //成功邀请的列表+奖励信息
#define createInviteInfo @"/v1/Business/Clerk/Project/Groupor/Invite"// 邀请团长的分享信息
#define sellerProductListApi @"/v1/Products/Sell/List/"
#define sellerProductUpApi @"/v1/Products/Sell/Up/"
#define sellerProductDownApi @"/v1/Products/Sell/Down/"
#define getSellerFixOrderListApi @"/v1/Repairs/OrderList/2/"
#define uploadMorePicApi @"v1/Images/UpFile/IFormFiles/5"//上传图片接口
#define uploadfixorderPicApi @"v1/Images/UpFile/IFormFiles/17"//上传维修明细图片接口
#define editFixOrderPriceApi @"/v1/Repairs/EditPrice"
#define submitFixOrderPriceApi @"/v1/Repairs/Submit"
#define getKDListApi @"/v1/Orders/KdList"
#define sendOrderCompelter @"/v1/Orders/CompleteSend"
#define repairOrderInfoApi @"/v1/Repairs/OrderInfo/"
#define feedBackSubmitApi @"/v1/Member/Message/Add"
#define feedBackListApi @"/v1/Member/Message/List/"
#define submitWithdrawInfoApi @"/v1/Member/Cash/Out"
#define getMyUtilMenuApi @"/v1/systems/Apple/Menu/List/"
#define resetPwdApi @"/v1/Manage/Member/Repassword"
#define sellerRefundOrderListApi @"/v1/Orders/SellerOrderList/"
#define updateAppApi @"/v1/systems/version/apply"
/**
 Member
*/
#define getApplyWholesaleListDataApi @"/v1/Member/ApplyWholesaleList"//获取申请批发会员的列表
#define getApplyWholesaleDetailApi @"/v1/Member/ApplyWholesale"//获取申请批发会员的列表
#define ApplyPassedWholesaleApi @"/v1/Member/ApplyPassedWholesale"//通过批发价申请
#define shopSettleInContentApi @"/v1/Shop/Settlein/Content"//入驻通道内容
#define shopSettleInApi @"/v1/Shop/Settlein"//商家入驻通道
#define addReissuedInvoiceApi @"/v1/Orders/Invoice/Reissued/Add"//订单补开发票
#define checkReissuedInvoiceInfoApi @"/v1/Orders/Invoice/Reissued/Info/"//获取指定订单补开发票记录
#define sellerSendInvoiceApi @"/v1/Orders/Invoice/Reissued/Send"//卖家补开发票发货
#define serviceCardListApi @"/v1/Member/Card/Service/List/"//获取服务卡列表
#define searchserviceCardApi @"/v1/Member/Card/Service/Info/"//查询服务卡是否已使用
#define useserviceCardApi @"/v1/Member/Card/Service/Use"//使用服务卡
#define readyUseServiceCardApi @"/v1/Member/Card/Service/UseReady"//使用服务卡准备
#define getMemberStatuesApi @"/v1/Member/Status"//获取当前账户状态信息
#define applyforShopApi @"/v1/Member/ApplyShop"//提交申请开店
#define getServiceTypeList @"/v1/Member/ServiceType"//服务分类
/**
  Repair
 */
#define queryRepairOrder  @"/v1/Repairs/Open/0"//用户初始询单
#define repairsOrderRequeryApi @"/v1/Repairs/ReQuery"//用户重新发布询单-换一家
#define userApplyHelpApi @"/v1/Repairs/Query"//用户发布询单
#define repairOrderCancelApi @"/v1/Repairs/Cancel"//用户取消订单
#define confirmFixOrderSeriviceApi @"/v1/Repairs/Select"//用户确认维修厂生成订单-进行服务中状态
#define repairOrderInfoApi @"/v1/Repairs/OrderInfo/"//获取维修订单明细
#define getFixOrderListApi @"/v1/Repairs/OrderList/1/"
#define getGrapOrderListApi @"/v1/Repairs/GrabList/"
#define getSellerGoodsOrderListApi @"/v1/Orders/SellerOrderList/"
#define grabOrderApi @"/v1/Repairs/Grab/"//商家抢单
#define getRepiarTypeList @"/v1/Repairs/TypeList"//服务小类

/**
   groups订单中心
 */
#define getBargainListApi @"/v1/Groups/Bargain/List"//获取砍价产品列表
#define getBargainRecordListApi @"/v1/Groups/Bargain/RecordList"//获取指定砍价订单进行的砍价记录列表
#define getBargainGoodInfoApi @"/v1/Groups/Bargain/Info"//获取砍价产品详细记录
#define BargainCutApi @"/v1/Groups/Bargain/Cut"//对指定订单进行砍价操作
#define getEwProductListApi @"/v1/Groups/Ew/Product/List"//延保产品列表
#define getEwProductInfoApi @"/v1/Groups/Ew/Product/Info/"//延保产品明细
#define makePrcieForEwProductApi @"/v1/Groups/Ew/Product/MakePrice"//延保产品生成价格
#define getEwProductOrderListApi @"/v1/Groups/Ew/Order/List/"//获取延保订单列表
#define getEwProductOrderInfoApi @"/v1/Groups/Ew/Order/Info/"//获取延保订单明细
#define getEwRecordListApi @"/v1/Groups/Ew/Record/List/"//获取保修订单报修列表
#define getEwRecordInfoApi @"/v1/Groups/Ew/Record/Info/"//获取保修订单报修明细
#define createEwOrderRecordApi @"/v1/Groups/Ew/Record/Add"//保修订单报修记录创建
#define getProducntPactApi @"/v1/Groups/Ew/Product/Pact/"//保修产品协议说明
#define getEwRequireApi @"/v1/Groups/Ew/Require"//获取保修方案的保修里程和车龄
#define getCarMaintainceProListApi @"/v1/Groups/Em/Product/List"//保养产品列表
#define getCarMaintainceProInfoApi @"/v1/Groups/Em/Product/Info/"//保养产品详情
#define getCarMaintainceOfYearsApi @"/v1/Groups/Em/Require"//获取保养方案的年份
#define MakePriceForCarMaintainceApi @"/v1/Groups/Em/Product/MakePrice"//保养产品生成价格
#define getCarMaintainceOrderListApi @"/v1/Groups/Em/Order/List/"//获取保养订单列表
#define getEmProductOrderInfoApi @"/v1/Groups/Em/Order/Info/"//获取保养订单详情
#define getEmRecordListApi @"/v1/Groups/Em/Record/List/"//获取保养订单保养列表
#define createEmOrderRecordApi @"/v1/Groups/Em/Record/Add"//保养订单保养记录创建
#define getEmRecordInfoApi @"/v1/Groups/Em/Record/Info/"//获取保养订单保养明细
#define getSellerRepairTcOrderListoApi @"/v1/Orders/SellerRepairTcOrderList/"//卖家维修套餐订单列表
//#define getRepairTcOrderListApi       @"/v1/Manage/Repair/OrderPackage/List/" //维修套餐订单列表
#define getRepairTcOrderListoApi @"/v1/Orders/OrderRepairTcList/" //会员维修套餐订单列表
#define readyReturnCard @"/v1/Member/Card/Service/ReturnReady" //退卡准备
#define returnServiceCard @"/v1/Member/Card/Service/Return/Add" //退卡申请
#define confirmerSeviceCard @"/v1/Member/Card/Service/Complete/" //服务卡确认完成
#define getRepairShopList @"/v1/Groups/Ew/Repair/List/"//保修保养门店列表
#define getSellerCarMaintainceOrderListApi @"/v1/Orders/SellerEwEmOrderList/"//获取卖家保修保养订单列表
#define getSellerEwEmOrderInfoApi @"/v1/Orders/SellerEwEmOrderInfo/"//获取卖家保修保养订单详情
#define getSellerEwEmOrderInfoByIdApi @"/v1/Orders/SellerEwEmOrderInfo/ById/"//获取卖家保修保养订单详情根据ID
#define sellerEwEmOrderCheckReadyApi @"/v1/Orders/SellerEwEmOrderCheckReady/"//卖家保修保养商家扫码检查
#define sellerEwEmOrderCheckApi @"/v1/Orders/SellerEwEmOrderCheck"//卖家保修保养商家扫码检查
#define sellerEwEmRepairStartApi @"/v1/Orders/SellerEwEmRepairStart/"//商家维修开始
#define sellerEwEmRepairEndApi @"/v1/Orders/SellerEwEmRepairEnd/"//商家维修结束
#define userEwEmRepairCompleteApi @"/v1/Groups/Em/Record/Complete/"//维修保养确认完成
#define sellerSubmitRepiarationCompleteApi @"/v1/Orders/SellerEwEmRepairComplete"//商家维修完成提交数据
#define userApplySalesReturnApi @"/v1/Groups/SaledApply"//申请团购退货
#define userApplyNormalSalesReturnApi @"/v1/Orders/SaledApply"//申请普通商品退货
/**
  QRCOde
 */
#define getQRCodeData    @"/v1/Qrcode/GetData/" //获取二维码转义数据

/**
 FastService 快修
*/
#define getRepairOrderInfo @"/v1/FastService/OrderInfo/"//维修订单详情
#define repairOrderSearchApi @"/v1/FastService/Init"//查询有无维修订单进行
#define fastServiceSelectShop @"/v1/manage/FastService/SelectShop"//派单
#define getUserFastServiceOrferListApi @"/v1/FastService/OrderList/User/"//用户订单列表
#define getSellerFastServiceOrderListApi @"/v1/FastService/OrderList/Shop/"//商家订单列表
#define cancelUserFastServiceOrderApi @"/v1/FastService/UserCancel"//用户申请取消
#define shopConfirmCancelApi @"/v1/FastService/ShopConfirmCancel" //商家确认取消
#define applyHelpApi @"/v1/FastService/Create" //创建订单
#define shopConfirmCompleteApi @"/v1/FastService/ShopConfirmComplete"//商家确认订单完成

#ifdef DEBUG

#define identityServer @"http://192.168.1.3:8012"
#define ServerAddress @"http://192.168.1.3:8011"
#define URLAddress @"http://192.168.1.3:8015/"


#else

#define identityServer @"https://identity.ocsawz.com"
#define ServerAddress @"https://gatewayapi.ocsawz.com"
#define URLAddress @"https://resources.ocsawz.com/"

//#define identityServer @"http://192.168.1.3:8012"
//#define ServerAddress @"http://192.168.1.3:8011"
//#define URLAddress @"http://192.168.1.3:8015/"

#endif

#endif /* Api_h */
