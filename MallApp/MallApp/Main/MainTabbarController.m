//
//  MainTabbarController.m
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MainTabbarController.h"

#import "MineVC.h"
#import "BaseNavigationController.h"
#import "NewHomeVC.h"
#import "ShopVC.h"
#import "ShopcarVC.h"
#import "NewLoginVC.h"
#import "AccountApi.h"
#import "PayMannagerUtil.h"
#import "NewBindUserVC.h"
#import "NewOtherLoginVC.h"
#import <UMCommon/UMCommon.h>
#import <UMVerify/UMVerify.h>
#import <CloudPushSDK/CloudPushSDK.h>
@interface MainTabbarController ()
@property (nonatomic,strong) AccountApi *accountApi;
@end

@implementation MainTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self SetupAllControllers];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callMyLogin) name:@"quickLogin" object:nil];
    
    self.tabBar.backgroundColor = [UIColor whiteColor];
}

- (void)SetupAllControllers{
    NSArray *titles = @[@"首页",  @"购物车", @"我的",];
    NSArray *images = @[@"首页-未选中", @"购物车-未选中", @"我的-未选中"];
    NSArray *selectedImages = @[@"首页-选中", @"购物车-选中", @"我的-选中"];
    NewHomeVC *main1 = [NewHomeVC new];
//    ShopVC *main2 = [ShopVC new];
    ShopcarVC *main3 = [ShopcarVC new];
    MineVC *main4 = [MineVC new];
    NSArray *viewControllers = @[main1,main3,main4];
    
    for (int i = 0; i < viewControllers.count; i++) {
        UIViewController *childVc = viewControllers[i];
        [self SetupChildVc:childVc title:titles[i] image:images[i] selectedImage:selectedImages[i]];
    }
}

- (void)SetupChildVc:(UIViewController *)VC title:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName{
//    [VC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} forState:UIControlStateNormal];
    VC.tabBarItem.title = title;
    [VC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName: APPColor} forState:UIControlStateSelected];
    VC.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    VC.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:VC];
    [self addChildViewController:nav];
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
//     self.tabBarController.selectedIndex = 1;
    if ([item.title isEqualToString:@"购物车"] || [item.title isEqualToString:@"我的"]) {
        if (!accessToken) {
//            LoginVC *vc = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//            NewLoginVC *vc = [NewLoginVC new];
//            BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//            vc2.modalPresentationStyle = 0;
//            [self presentViewController:vc2 animated:true completion:nil];
//            [XJUtil callUMLogin:^(NSDictionary * _Nullable resultDic) {
//
//            } inVC:self];
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callMyLogin) name:@"quickLogin" object:nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
            [self callMyLogin];
            
        }
        

    }
    
}

- (void)dealloc{
    
}

- (void)callMyLogin{
   
    [XJUtil callUMLogin:^(NSDictionary * _Nullable resultDic) {
        NSString *delegateId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Bid"];
        if (!delegateId) {
            delegateId = @"0";
        }
        NSString *token = [resultDic objectForKey:@"token"];
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setValue:token forKey:@"token"];
        [dic setValue:@"apple" forKey:@"appType"];
        [dic setValue:@([delegateId integerValue]) forKey:@"clerkId"];
        [dic setValue:@"" forKey:@"unionID"];
        [JMBManager showLoading];
        [self.accountApi quickLoginWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSDictionary *dic = result.resultDic;
                NSArray *arr = dic[@"list"];
                NSDictionary *userData = arr.firstObject;
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Bid"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshUserVC object:nil];
                JLog(@"%@",userData[@"userId"]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UMCommonHandler cancelLoginVCAnimated:YES complete:nil];
                });
               
                [CloudPushSDK addAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] withCallback:^(CloudPushCallbackResult *res) {
                    if (res.success) {
                        JLog(@"设置成功");
                    }
                    
                }];
            }
            
            [JMBManager hideAlert];
            
        }];
               
    } with:^(NSInteger type) {
        if (type == 1000) {
            [[PayMannagerUtil sharedManager] loginWithWeChatMethodsuccessBlock:^(BaseResp * _Nonnull res) {
                SendAuthResp *authResp = (SendAuthResp *)res;
                
                NSLog(@"微信登录的code%@",authResp.code);
                NSLog(@"我的Manager ----- %@",[self class]);
                WeakSelf(self)
                [self.accountApi AppWxLoginWithparameters:@{@"code":authResp.code} withCompletionHandler:^(NSError *error, MessageBody *result) {
                    StrongSelf(self)
                    NSLog(@"接口返回code%@",result.message);
                    if (result.code == 200) {
                        NSDictionary *dic = result.resultDic;
                        NSArray *arr = dic[@"list"];
                        NSDictionary *userData = arr.firstObject;
                        [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
                        [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
                        [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
                        [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
                        [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
                                   [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
                                   [[NSNotificationCenter defaultCenter] postNotificationName:refreshUserVC object:nil];
                        [CloudPushSDK addAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] withCallback:^(CloudPushCallbackResult *res) {
                            if (res.success) {
                                JLog(@"设置成功");
                            }

                        }];
                        //                                [self dismissViewControllerAnimated:true completion:nil];
                    }else{
                        NewBindUserVC *vc = [[NewBindUserVC alloc] init];
                        BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
                        vc2.modalPresentationStyle = 0;

                        vc.uninoId = [NSString stringWithFormat:@"%@",result.message];

                        [self presentViewController:vc2 animated:true completion:nil];


                    }

                }];
                
//                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//                [dic setValue:@"wx13518cb074bdb381" forKey:@"appid"];
//                [dic setValue:@"7407f9aa0731075a56de65369c4c0e7e" forKey:@"secret"];
//                [dic setValue:authResp.code forKey:@"code"];
//                [dic setValue:@"authorization_code" forKey:@"grant_type"];
//                [self.accountApi getWechatUniod:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
//
//                }];
//
                
            } failBlock:^(BOOL fail) {
                
            }];
        }else if (type == 1001){
            NewOtherLoginVC *vc = [NewOtherLoginVC new];
            BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
            vc2.modalPresentationStyle = 0;
            vc.type = 1;
            [self presentViewController:vc2 animated:true completion:nil];
            
        }else if (type == 1002){
            NewOtherLoginVC *vc = [NewOtherLoginVC new];
            BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
            vc2.modalPresentationStyle = 0;
            vc.type = 2;
            [self presentViewController:vc2 animated:true completion:nil];
            
        }
    } inVC:self];
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}



@end
