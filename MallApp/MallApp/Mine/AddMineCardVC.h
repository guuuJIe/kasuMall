//
//  AddMineCardVC.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddMineCardVC : BaseViewController
- (IBAction)submit:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameLbl;
@property (weak, nonatomic) IBOutlet UITextField *bankNum;
@property (weak, nonatomic) IBOutlet UITextField *banktype;

@end

NS_ASSUME_NONNULL_END
