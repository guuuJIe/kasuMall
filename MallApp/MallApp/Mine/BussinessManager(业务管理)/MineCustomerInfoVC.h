//
//  MineCustomerInfoVC.h
//  MallApp
//
//  Created by Mac on 2020/2/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineCustomerInfoVC : BaseViewController
@property (nonatomic,assign) NSInteger customerId;
@end

NS_ASSUME_NONNULL_END
