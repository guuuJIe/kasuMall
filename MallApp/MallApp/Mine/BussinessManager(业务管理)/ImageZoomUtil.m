//
//  ImageZoomUtil.m
//  MallApp
//
//  Created by Mac on 2020/2/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ImageZoomUtil.h"
static CGRect oldframe;
@implementation ImageZoomUtil
+(void)ImageZoomWithImageView:(UIImageView *)contentImageview{
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    oldframe=[contentImageview convertRect:contentImageview.bounds toView:window];
    [self scanBigImageWithImage:contentImageview frame:[contentImageview convertRect:contentImageview.bounds toView:window] superview:window];
    
}

+ (void)scanBigImageWithImage:(UIImageView *)scalimage frame:(CGRect)frame superview:(UIWindow *)window{
   
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:oldframe];
    imageView.image = scalimage.image;
    imageView.tag = 1024;
    //背景
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    backgroundView.backgroundColor = [UIColor blackColor];
    [window addSubview:backgroundView];
    [backgroundView addSubview:imageView];
    
    //添加点击事件同样是类方法 -> 作用是再次点击回到初始大小
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImageView:)];
    [backgroundView addGestureRecognizer:tapGestureRecognizer];
//    UIImageView *imageView = [tap.view viewWithTag:1024];
    //动画放大所展示的ImageView
    UIImage *image = imageView.image;
    [UIView animateWithDuration:0.4 animations:^{
        CGFloat y,height;
        y = (Screen_Height - image.size.height * Screen_Width / image.size.width) * 0.5;
        //宽度为屏幕宽度
//        width = [UIScreen mainScreen].bounds.size.width;
        //高度 根据图片宽高比设置
        height = image.size.height * Screen_Width/ image.size.width;
        [imageView setFrame:CGRectMake(0, y, Screen_Width, height)];
        //重要！ 将视图显示出来
//        [backgroundView setAlpha:1];
    } completion:^(BOOL finished) {
        
    }];

}

/**
 *  恢复imageView原始尺寸
 */
+(void)hideImageView:(UITapGestureRecognizer *)tap{
    UIView *backgroundView = tap.view;
    //原始imageview
    UIImageView *imageView = [tap.view viewWithTag:1024];
    //恢复
    [UIView animateWithDuration:0.4 animations:^{
        [imageView setFrame:oldframe];
        [backgroundView setAlpha:0];
    } completion:^(BOOL finished) {
        [backgroundView removeFromSuperview];
    }];
}
@end
