//
//  AppleWholesaleItemDetailVC.m
//  MallApp
//
//  Created by Mac on 2020/2/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AppleWholesaleItemDetailVC.h"
#import "PurchaseView.h"
#import "BussinessMangeApi.h"
#import "ImageZoomUtil.h"
@interface AppleWholesaleItemDetailVC ()
@property (nonatomic, strong)PurchaseView *payBtn;
@property (nonatomic, strong)BussinessMangeApi *bussinessApi;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *connactLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *shopPic;
@property (nonatomic, strong) NSString *imgUrl;
@end

@implementation AppleWholesaleItemDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"批发审核";
    
    [self.view addSubview:self.payBtn];
    [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    self.shopPic.userInteractionEnabled = true;
    [self.shopPic addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self getdata];
}

- (void)getdata{
    [self.bussinessApi getApplyWholesaleDetailWithparameters:@{@"userid":@(self.userId)} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            [self configData:datas.firstObject];
        }
    }];
}

- (void)applyPassedWholesale{
    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认该用户申请通过？" andHandler:^(NSInteger index) {
        if (index == 1) {
            [self.bussinessApi ApplyPassedWholesaleWithparameters:@{@"userid":@(self.userId),@"Verify":@(true)} withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    if (self.backBlock) {
                        self.backBlock();
                        [self.navigationController popViewControllerAnimated:true];
                    }
                    
                }
            }];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    NSString *type = dic[@"mjType"];
    if ([type isEqualToString:@"4"]) {
        self.typeLbl.text = @"三类维修";
    }else if ([type isEqualToString:@"1"]){
        self.typeLbl.text = @"汽车4S店";
    }else if ([type isEqualToString:@"2"]){
        self.typeLbl.text = @"一类综合维修";
    }else if ([type isEqualToString:@"3"]){
        self.typeLbl.text = @"二类维修";
    }else if ([type isEqualToString:@"5"]){
        self.typeLbl.text = @"汽车专项维修";
    }else if ([type isEqualToString:@"8"]){
        self.typeLbl.text = @"配件零售商";
    }else if ([type isEqualToString:@"9"]){
        self.typeLbl.text = @"精品零售商";
    }else if ([type isEqualToString:@"7"]){
        self.typeLbl.text = @"汽车改装服务";
    }else if ([type isEqualToString:@"6"]){
        self.typeLbl.text = @"汽车装潢服务";
    }
    self.nameLbl.text = dic[@"qyname"];
    self.emailLbl.text = dic[@"email"];
    self.connactLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contact"] andDefailtInfo:@""];
    self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"phone"] andDefailtInfo:@""];
    self.telLbl.userInteractionEnabled = true;
    [self.telLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callMan)]];
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"file0"]];
    [self.shopPic yy_setImageWithURL:URL(url) options:0];
    self.imgUrl = url;
//    self.shopPic.tag = 1000;
    self.timeLbl.text = dic[@"applyDate"];
}

- (void)click:(UITapGestureRecognizer *)tap{
     UIImageView *clickedImageView = (UIImageView *)tap.view;
     [ImageZoomUtil ImageZoomWithImageView:clickedImageView];
}
- (void)callMan{
    if (self.telLbl.text.length !=0) {
        [XJUtil callTelNoCommAlert:self.telLbl.text];
    }
    
}

- (PurchaseView *)payBtn{
    if (!_payBtn) {
        _payBtn = [PurchaseView new];
        [_payBtn.purchaseButton setTitle:@"同意申请" forState:0];
        WeakSelf(self);
        _payBtn.addAddressBlock = ^{
            [weakself applyPassedWholesale];
        };
        
    }
    return _payBtn;
}

- (BussinessMangeApi *)bussinessApi{
    if (!_bussinessApi) {
        _bussinessApi = [BussinessMangeApi new];
    }
    return _bussinessApi;
}
@end
