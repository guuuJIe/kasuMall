//
//  MineDelegateRecommandVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineCustomerVC.h"
#import "DelegateRecommandCell.h"
#import "MineApi.h"
#import "MineCustomerInfoVC.h"
@interface MineCustomerVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,strong) UITextField *contentText;
@end

@implementation MineCustomerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initProperty];
    [self setupUI];
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupData:RefreshTypeNormal];
}
- (void)setupUI{
    self.title = @"我的客户";
    
    UIView *topView = [UIView new];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
        make.top.mas_equalTo(10);
    }];
    
    UITextField *text = [UITextField new];
    text.placeholder = @"请输入账号";
    text.textColor = UIColor333;
    text.font = LabelFont14;
    [topView addSubview:text];
    self.contentText = text;
    [text mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(12);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(200);
        make.centerY.mas_equalTo(topView);
        
    }];
    
    UIButton *btn = [UIButton new];
    [btn setTitle:@"搜索" forState:0];
    [btn setTitleColor:APPColor forState:0];
    [btn.titleLabel setFont:LabelFont14];
    [topView addSubview:btn];
    [btn addTarget:self action:@selector(searchAct) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(topView);
    }];
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(topView.mas_bottom).offset(10);
    }];
    
    
}

- (void)initProperty{
    
    self.num = 1;
    self.size = 20;
    
}

- (void)searchAct{
    if (!self.contentText.text) {
        [JMBManager showBriefAlert:@"请输入检索内容"];
        return;
    }
    
    [self setupData:RefreshTypeNormal];
}



- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
 
    
    [self.api getBusinessClerkListWithparameters:self.contentText.text withPageNum:self.num andPageSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;

            self.dataArray = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.dataArray withNum:self.num];
            [self.listTableView reloadData];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"DelegateRecommandCell";
    DelegateRecommandCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[DelegateRecommandCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArray[indexPath.row]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MineCustomerInfoVC *vc = [MineCustomerInfoVC new];
    NSDictionary *dic = self.dataArray[indexPath.row];
    vc.customerId = [dic[@"id"] integerValue];
    [self.navigationController pushViewController:vc animated:true];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        [_listTableView registerNib:[UINib nibWithNibName:@"DelegateRecommandCell" bundle:nil] forCellReuseIdentifier:@"DelegateRecommandCell"];
      
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}
@end
