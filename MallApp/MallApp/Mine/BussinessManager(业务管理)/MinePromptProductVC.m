//
//  MinePromptProductVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MinePromptProductVC.h"
#import "MinePromptView.h"
#import "PromptProductTableCell.h"
#import "MineApi.h"
#import "MinePromptProductInfoVC.h"
@interface MinePromptProductVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) MinePromptView *promptView;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, strong) MineApi *api;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation MinePromptProductVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setupProp];
}

- (void)setupUI{
    
    self.title = @"推广产品";
//    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.right.mas_equalTo(self.view);
//        make.height.mas_equalTo(45);
//    }];
//
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
    
    
}

- (void)setupProp{
//    self.promptView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = 0;
    [self.listTableView.mj_header beginRefreshing];
    
}




- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    [self.api getBussinessClerkProjectListnWithparameters:@"" withpageSize:self.size pageNum:self.num withCompletionHandler:^(NSError *error, MessageBody *result) {
        NSArray *datas = result.result;
        
        self.dataArray = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.dataArray withNum:self.num];
        [self.listTableView reloadData];
        
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"PromptProductTableCell";
    PromptProductTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[PromptProductTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    NSDictionary *dic = self.dataArray[indexPath.row];
    [cell setupdata:dic[@"productInfo"]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MinePromptProductInfoVC *vc = [MinePromptProductInfoVC new];
    NSDictionary *dic = self.dataArray[indexPath.row];
    vc.pids = [NSString stringWithFormat:@"%@",dic[@"id"]];
    [self.navigationController pushViewController:vc animated:true];
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        [_listTableView registerClass:[PromptProductTableCell class] forCellReuseIdentifier:@"PromptProductTableCell"];
      
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}


- (MinePromptView *)promptView
{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        WeakSelf(self)
        _promptView.dataSource = @[@"进行中",@"已过期"];
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
//                    weakself.statues = 0;
                    break;
                case 1:
//                    weakself.statues = 1;
                    break;
                
                default:
                    break;
            }
             [weakself.listTableView.mj_header beginRefreshing];
        };
//        [self.view addSubview:_promptView];
    }
    return _promptView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
