//
//  MinePromptOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MinePromptFixOrderVC.h"
#import "MinePromptView.h"
#import "MineApi.h"
#import "PromptOrderCell.h"
@interface MinePromptFixOrderVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) MinePromptView *orderView;
@property (nonatomic,assign) NSInteger statues;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) NSMutableArray *dataArr;
@end

@implementation MinePromptFixOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupProp];
    [self setupUI];
    
    
    
//    [self.listTableView.mj_header beginRefreshing];
    
}

- (void)setupProp{
    self.orderView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = 0;
}

- (void)setupUI{
    self.title = @"推广维修订单";
    [self.orderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
   
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.orderView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    [self.api getRecommendFixOrderListWithparameters:@{} withPageNum:self.num andPageSize:self.size andStatues:self.statues withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
//            [self configData:datas withRefreshType:type];
            self.dataArr = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.dataArr withNum:self.num];
            [self.listTableView reloadData];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"PromptOrderCell";
    PromptOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[PromptOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
        [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}



- (MinePromptView *)orderView
{
    if (!_orderView) {
        _orderView = [MinePromptView new];
        WeakSelf(self);
        //@param status  状态0=全部，1=进行中，2=已支付，4=完成订单
        _orderView.dataSource = @[@"全部",@"进行中",@"已支付",@"已完成"];
        _orderView.clickBlock = ^(NSInteger tag) {
            switch (tag - 200) {
                case 0:
                    weakself.statues = 0;
                    break;
                case 1:
                    weakself.statues = 1;
                    break;
                case 2:
                    weakself.statues = 2;
                    break;
                    
                case 3:
                    weakself.statues = 4;
                    break;
                case 4:
//                    weakself.statues = 4;
                    break;
                default:
                    break;
            }
             [weakself.listTableView.mj_header beginRefreshing];
        };
        [self.view addSubview:_orderView];
    }
    return _orderView;
}

- (MineApi *)api{
    if (!_api) {
        _api =[MineApi new];
    }
    return _api;
}
-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerNib:[UINib nibWithNibName:@"PromptOrderCell" bundle:nil] forCellReuseIdentifier:@"PromptOrderCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}



@end
