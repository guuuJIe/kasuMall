//
//  AppleWholesaleItemDetailVC.h
//  MallApp
//
//  Created by Mac on 2020/2/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppleWholesaleItemDetailVC : BaseViewController
@property (nonatomic,assign) NSInteger userId;
@property (nonatomic,copy) void(^backBlock)(void);
@end

NS_ASSUME_NONNULL_END
