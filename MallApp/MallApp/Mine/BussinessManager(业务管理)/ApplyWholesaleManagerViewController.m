//
//  ApplyWholesaleManagerViewController.m
//  MallApp
//
//  Created by Mac on 2020/2/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplyWholesaleManagerViewController.h"
#import "AppleWholesaleItemCell.h"
#import "BussinessMangeApi.h"
#import "MineCustomerInfoVC.h"
#import "AppleWholesaleItemDetailVC.h"
@interface ApplyWholesaleManagerViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) NSMutableArray *dataArray;
@property (nonatomic) BussinessMangeApi *api;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;

@end

@implementation ApplyWholesaleManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"批发审核";
    [self initProperty];
    [self setupUI];
    [self setupData:RefreshTypeNormal];
}


- (void)setupUI{
  
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(0);
    }];
}

- (void)initProperty{
    self.num = 1;
    self.size = 20;
    
}



- (void)setupData:(RefreshType)type{
//    if (type == RefreshTypeDown) {
//        self.num = 1;
//    }else if (type == RefreshTypeUP){
//        self.num = self.num+1;
//    }
    
    [self.api getApplyWholesaleListWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dataArray = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.dataArray withNum:self.num];
            [self.listTableView reloadData];
        }
    }];
//    
//    [self.api getBusinessClerkListWithparameters:@"0" withPageNum:self.num andPageSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//
////            [self configData:datas withRefreshType:type];
//
//            [self.listTableView reloadData];
//        }
//    }];
}

#pragma mark ---UITableViewDelegate---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"AppleWholesaleItemCell";
    AppleWholesaleItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[AppleWholesaleItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArray[indexPath.row]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AppleWholesaleItemDetailVC *vc = [[AppleWholesaleItemDetailVC alloc] initWithNibName:@"AppleWholesaleItemDetailVC" bundle:nil];
    vc.backBlock = ^{
        [self setupData:RefreshTypeNormal];
    };
    NSDictionary *dic = self.dataArray[indexPath.row];
    vc.userId = [dic[@"userId"] integerValue];
    [self.navigationController pushViewController:vc animated:true];
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        [_listTableView registerNib:[UINib nibWithNibName:@"AppleWholesaleItemCell" bundle:nil] forCellReuseIdentifier:@"AppleWholesaleItemCell"];
      
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
//        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            StrongSelf(self);
//            [self setupData:RefreshTypeUP];
//        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (BussinessMangeApi *)api{
    if (!_api) {
        _api = [BussinessMangeApi new];
    }
    return _api;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
