//
//  MineDelegateInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineDelegateInfoVC.h"
#import "MineDelegateInfoOneCell.h"
#import "MineDelegateInfoTwoCell.h"
#import "MineDelegateInfoThreeCell.h"
#import "MineDelegateInfoFourCell.h"
#import "MineApi.h"
@interface MineDelegateInfoVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,strong) NSArray *dataArray;
@end

@implementation MineDelegateInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"客户资料";
    [self setupUI];
    [self getData];
}

- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)getData{
    [JMBManager showLoading];
    if (!self.customerId) {
        self.customerId = 0;
    }
    [self.api getDelegateInfoithparameters:@(self.customerId) withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArray = result.result;
            [self.listTableView reloadData];
            
        }
        [JMBManager hideAlert];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"MineDelegateInfoOneCell";
        MineDelegateInfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[MineDelegateInfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataArray.firstObject];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"MineDelegateInfoTwoCell";
        MineDelegateInfoTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[MineDelegateInfoTwoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataArray.firstObject];
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"MineDelegateInfoThreeCell";
        MineDelegateInfoThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[MineDelegateInfoThreeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataArray.firstObject];
        return cell;
    }else if (indexPath.section == 3){
        static NSString *Identifier = @"MineDelegateInfoFourCell";
        MineDelegateInfoFourCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[MineDelegateInfoFourCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataArray.firstObject];
        return cell;
    }
    return nil;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        [_listTableView registerNib:[UINib nibWithNibName:@"MineDelegateInfoOneCell" bundle:nil] forCellReuseIdentifier:@"MineDelegateInfoOneCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MineDelegateInfoTwoCell" bundle:nil] forCellReuseIdentifier:@"MineDelegateInfoTwoCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MineDelegateInfoThreeCell" bundle:nil] forCellReuseIdentifier:@"MineDelegateInfoThreeCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MineDelegateInfoFourCell" bundle:nil] forCellReuseIdentifier:@"MineDelegateInfoFourCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;
        
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}
@end
