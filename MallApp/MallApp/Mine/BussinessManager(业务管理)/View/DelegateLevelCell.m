//
//  DelegateLevelCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/31.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "DelegateLevelCell.h"
@interface DelegateLevelCell()
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation DelegateLevelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    return self;
    
}

- (void)setupUI{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.top.mas_equalTo(14);
        make.bottom.mas_equalTo(-14);
    }];
    
    [self.selBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-14);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    self.selBtn.selected = NO;
    self.titleLabel.text = dic[@"name"];
//    [self.payIcon setImage:[UIImage imageNamed:dic[@"image"]] forState:0];
}


-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"分类";
        _titleLabel.font = LabelFont14;
        _titleLabel.textColor = UIColor333;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UIButton *)selBtn{
    if (!_selBtn) {
        _selBtn = [UIButton new];
        [_selBtn setImage:[UIImage imageNamed:@"unsel"] forState:UIControlStateNormal];
        [_selBtn setImage:[UIImage imageNamed:@"select"] forState:UIControlStateSelected];
        _selBtn.userInteractionEnabled = false;
        [self addSubview:_selBtn];
    }
    
    return _selBtn;
}


@end
