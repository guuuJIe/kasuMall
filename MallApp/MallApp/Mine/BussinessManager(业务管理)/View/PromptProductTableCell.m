//
//  PromptProductTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PromptProductTableCell.h"
@interface PromptProductTableCell()
@property (nonatomic, strong) UIImageView *goodsImage;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *priceLbl;

@end

@implementation PromptProductTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = UIColor.whiteColor;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    UIView *topView = [UIView new];
    topView.backgroundColor = [UIColor whiteColor];
    topView.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
    [self.goodsImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(topView.mas_bottom).offset(12);
        make.bottom.mas_equalTo(-12);
        make.size.mas_equalTo(CGSizeMake(64, 64));
    }];
    
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsImage);
        make.left.mas_equalTo(self.goodsImage.mas_right).offset(8);
        make.right.mas_equalTo(-12);
    }];


    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.goodsImage);
        make.left.mas_equalTo(self.goodsImage.mas_right).offset(8);
        make.right.mas_equalTo(-12);
    }];

    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.size.mas_equalTo(CGSizeMake(70, 24));
        make.bottom.mas_equalTo(self.goodsImage);
    }];
    
}


- (void)setupdata:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"proPic"]];
        [self.goodsImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = dic[@"title"];
        self.priceLbl.text = @"";

    }
}


- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.textColor = UIColor333;
        _titleLbl.font = LabelFont13;
        _titleLbl.text = @"Mobil美孚一号全合成机油 5W-30 SN级 4L";
        [self.contentView addSubview:_titleLbl];
    }
    
    return _titleLbl;
}


- (UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.textColor = UIColorFF3E;
        _priceLbl.font = LabelFont12;
        _priceLbl.text = @"¥10";
        [self.contentView addSubview:_priceLbl];
    }
    
    return _priceLbl;
}

- (UIImageView *)goodsImage{
    if (!_goodsImage) {
        _goodsImage = [UIImageView new];
        _goodsImage.image = [UIImage imageNamed:@""];
//        _goodsImage.backgroundColor = APPColor;
        [self.contentView addSubview:_goodsImage];
    }
    
    return _goodsImage;
}


- (UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [UIButton new];
        [_shareBtn setTitle:@"去分享" forState:0];
        _shareBtn.layer.cornerRadius = 12;
        _shareBtn.backgroundColor = APPColor;
        [_shareBtn.titleLabel setFont:LabelFont13];
        [_shareBtn setTitleColor:UIColor.whiteColor forState:0];
        _shareBtn.userInteractionEnabled = false;
        [self.contentView addSubview:_shareBtn];
    }
    
    return _shareBtn;
}

@end
