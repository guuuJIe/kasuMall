//
//  SelectDelegateLevelView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectDelegateLevelView : UIView
+(instancetype)initViewClass;
@property (nonatomic,copy) void(^clickBlock)(NSInteger index);
@end

NS_ASSUME_NONNULL_END
