//
//  QRProductTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "QRProductTableCell.h"
@interface QRProductTableCell()
@property (nonatomic, strong) UIImageView *codeImage;
@end

@implementation QRProductTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
        
    }
    
    return self;
}

- (void)setupUI{
    
    UIView *topView = [UIView new];
    topView.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
    UILabel *title = [UILabel new];
    title.text = @"项目二维码";
    title.textColor = UIColor333;
    title.font = LabelFont14;
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView.mas_bottom).offset(12);
        make.left.mas_equalTo(16);
    }];
    
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title.mas_bottom).offset(8);
//        make.bottom.mas_equalTo(self.contentView).offset(0);
    }];
    
    
    UIImageView *img = [UIImageView new];
//    img.backgroundColor = UIColor.redColor;
    [self.contentView addSubview:img];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line.mas_bottom).offset(32*AdapterScal);

       
        make.size.mas_equalTo(CGSizeMake(200*AdapterScal, 200*AdapterScal));
        make.bottom.mas_equalTo(self.contentView).offset(-37);
        make.centerX.mas_equalTo(self.contentView);
        
    }];

    self.codeImage = img;
    
   
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        [filter setDefaults];
        NSString *string = [NSString stringWithFormat:@"%@",dic[@"wapdata"]];
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        [filter setValue:data forKeyPath:@"inputMessage"];
        CIImage *image = [filter outputImage];// 此时的 image 是模糊的
        
        self.codeImage.image = [XJUtil createNonInterpolatedUIImageFormCIImage:image withSize:200];
    }
}

@end
