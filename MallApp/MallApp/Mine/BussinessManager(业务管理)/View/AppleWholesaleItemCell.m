//
//  AppleWholesaleItemCell.m
//  MallApp
//
//  Created by Mac on 2020/2/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AppleWholesaleItemCell.h"
@interface AppleWholesaleItemCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *accountLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation AppleWholesaleItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.bgView.layer.cornerRadius = 4;
    self.backgroundColor = UIColorF5F7;
    self.bgView.layer.shadowColor = shawdowColor.CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.bgView.layer.shadowOpacity = 0.3;
    self.bgView.layer.shadowRadius = 2.0;
    self.bgView.layer.cornerRadius = 4;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = dic[@"name"];
        self.accountLbl.text = dic[@"userName"];
        self.timeLbl.text = dic[@"applyDate"];
    }
}
@end
