//
//  DelegateLevelView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <MMPopupView/MMPopupView.h>

NS_ASSUME_NONNULL_BEGIN

@interface DelegateLevelView : MMPopupView
@property (nonatomic,copy) void(^clickBlock)(NSInteger index);
@property (nonatomic,strong)NSMutableArray *arr;
@end

NS_ASSUME_NONNULL_END
