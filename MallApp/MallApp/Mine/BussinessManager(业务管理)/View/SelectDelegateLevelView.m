//
//  SelectDelegateLevelView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SelectDelegateLevelView.h"
@interface SelectDelegateLevelView()
@property (weak, nonatomic) IBOutlet UIView *oneView;
@property (weak, nonatomic) IBOutlet UIView *twoView;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;

@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@end

@implementation SelectDelegateLevelView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self.oneView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.twoView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

+(instancetype)initViewClass{
    return [[[NSBundle mainBundle] loadNibNamed:@"SelectDelegateLevelView" owner:self options:nil] lastObject];
}

- (IBAction)cancelAct:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock(1);
    }
}

- (IBAction)confirmAct:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock(2);
    }
}


- (void)click:(UITapGestureRecognizer *)ges{
    if (self.clickBlock) {
        self.clickBlock(ges.view.tag);
    }
    
    switch (ges.view.tag) {
        case 100:
        {
            self.oneBtn.selected = true;
            self.twoBtn.selected = false;
        }
            break;
        case 101:
        {
            self.oneBtn.selected = false;
            self.twoBtn.selected = true;
        }
            break;
        default:
            break;
    }
}

@end
