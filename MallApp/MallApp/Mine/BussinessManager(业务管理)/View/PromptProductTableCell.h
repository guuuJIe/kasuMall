//
//  PromptProductTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PromptProductTableCell : UITableViewCell

@property (nonatomic, strong) UIButton *shareBtn;
- (void)setupdata:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
