//
//  DelegateLevelView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "DelegateLevelView.h"
#import "SelectDelegateLevelView.h"
#import "DelegateLevelCell.h"
@interface DelegateLevelView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)SelectDelegateLevelView *selectView;
@property (nonatomic)UITableView *TabelView;
@property (nonatomic) NSIndexPath *selectIndexPath;
@end

@implementation DelegateLevelView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        
        
//        self.selectView = [SelectDelegateLevelView initViewClass];
//        [self addSubview:self.selectView];
//        self.selectView.layer.cornerRadius = 8;
//        [self.selectView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.center.mas_equalTo(self);
//            make.size.mas_equalTo(CGSizeMake(Screen_Width - 92, 190));
//        }];
//        WeakSelf(self)
//        self.selectView.clickBlock = ^(NSInteger index) {
//            if (index == 1) {
//                [weakself hide];
//            }else if (index == 2){
//                if (weakself.clickBlock) {
//                    weakself.clickBlock(3);
//                }
//            }else if (index == 100){
//                if (weakself.clickBlock) {
//                    weakself.clickBlock(1);
//                }
//            }else if (index == 101){
//                if (weakself.clickBlock) {
//                    weakself.clickBlock(2);
//                }
//            }
//        };
        
        
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        view.layer.cornerRadius = 8;
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(Screen_Width - 92, 190));
        }];
        
        UILabel *titleLbl = [UILabel new];
        titleLbl.text = @"选择代理级别";
        titleLbl.textColor = UIColor333;
        [view addSubview:titleLbl];
        [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view);
            make.top.mas_equalTo(16);
        }];
        
        [view addSubview:self.TabelView];
        [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(view);
            make.top.mas_equalTo(titleLbl.mas_bottom).offset(0);
            make.height.mas_equalTo(100);
        }];
        
        
        UIButton *btnOK = [[UIButton alloc] init];
        [btnOK setTitle:@"确定" forState:UIControlStateNormal];
        [btnOK setTitleColor:APPColor forState:UIControlStateNormal];
        [btnOK addTarget:self action:@selector(BtnClickOK:) forControlEvents:UIControlEventTouchUpInside];
        btnOK.tag = 100;
        [btnOK.titleLabel setFont:LabelFont15];
        [view addSubview:btnOK];
        
        UIButton *btnCancel = [[UIButton alloc] init];
        [btnCancel setTitle:@"取消" forState:UIControlStateNormal];
        [btnCancel setTitleColor:UIColor333 forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(BtnClickOK:) forControlEvents:UIControlEventTouchUpInside];
        [btnCancel.titleLabel setFont:LabelFont15];
         btnCancel.tag = 101;
        [view addSubview:btnCancel];
        
        [btnCancel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(view);
            make.size.mas_equalTo(CGSizeMake((Screen_Width - 92)/2 - 1, 40));
            make.bottom.mas_equalTo(-1);
        }];
        
        [btnOK mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btnCancel.mas_right);
            make.size.mas_equalTo(CGSizeMake((Screen_Width - 92)/2- 1, 40));
            make.bottom.mas_equalTo(-1);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = UIColorEF;
        [view addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
            make.centerX.mas_equalTo(view);
            make.top.mas_equalTo(self.TabelView.mas_bottom);
            make.width.mas_equalTo(lineHeihgt);
        }];
        
    }
    return self;
}

- (void)BtnClickOK:(UIButton *)button{
    if (button.tag == 100) {
        if (self.clickBlock) {
            self.clickBlock(self.selectIndexPath.row);
        }
    }else if (button.tag == 101){
        [self hide];
    }
}

-(void)setArr:(NSMutableArray *)arr{
    _arr = arr;
    [self.TabelView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arr.count;
//    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"DelegateLevelCell";
    DelegateLevelCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[DelegateLevelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
   
//    [cell setupData:self.arr[indexPath.row] andNSIndexPath:indexPath];
    
    [cell setupData:self.arr[indexPath.row]];
    if (_selectIndexPath.row == indexPath.row) {
           cell.selBtn.selected = true;
       }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectIndexPath = indexPath;
    
    [_TabelView reloadData];
}


-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        [_TabelView setRowHeight:50];
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//        [_TabelView registerNib:[UINib nibWithNibName:@"ArguTableCell" bundle:nil] forCellReuseIdentifier:@"ArguTableCell"];
        _TabelView.showsVerticalScrollIndicator = false;
//        _TabelView.scrollEnabled = false;
        [_TabelView registerClass:[DelegateLevelCell class] forCellReuseIdentifier:@"DelegateLevelCell"];
        //        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}
@end
