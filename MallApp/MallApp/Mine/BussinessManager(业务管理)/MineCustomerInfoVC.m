//
//  MineDelegateInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineCustomerInfoVC.h"
#import "MineDelegateInfoOneCell.h"
#import "MineDelegateInfoTwoCell.h"
#import "MineDelegateInfoThreeCell.h"
#import "AuthorationTableViewCell.h"
#import "MineApi.h"
#import "DelegateLevelView.h"
#import "ChangeInfoVC.h"
@interface MineCustomerInfoVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,strong) NSArray *dataArray;
@property (nonatomic,strong) NSArray *delegateLevelArray;
@property (nonatomic,assign) NSInteger type;
@property (nonatomic, strong) DelegateLevelView *delegateView;
@end

@implementation MineCustomerInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"客户资料";
    
    [self setupUI];
    [self getData];
    [self getDelegateData];
}

- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)getData{
    [JMBManager showLoading];
    if (!self.customerId) {
        self.customerId = 0;
    }
    [self.api getDelegateInfoithparameters:@(self.customerId) withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArray = result.result;
            [self.listTableView reloadData];
            [JMBManager hideAlert];
        }
    }];
}

- (void)getDelegateData{
    [JMBManager showLoading];

    [self.api getDelegateScopeWithparameters:@"" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
           JLog(@"%@",result.result);
            self.delegateLevelArray = result.result;
            NSDictionary *dic = self.delegateLevelArray.firstObject;
            self.type = [dic[@"id"] integerValue];
        }
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"MineDelegateInfoOneCell";
        MineDelegateInfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[MineDelegateInfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataArray.firstObject];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"MineDelegateInfoTwoCell";
        MineDelegateInfoTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[MineDelegateInfoTwoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataArray.firstObject];
        WeakSelf(self)
        cell.editBlock = ^{
            StrongSelf(self)
            NSDictionary *dic = self.dataArray.firstObject;
            ChangeInfoVC *vc = [[ChangeInfoVC alloc] initWithNibName:@"ChangeInfoVC" bundle:nil];
            vc.tag = [dic[@"userId"] integerValue];
            vc.refreshBlock = ^{
                [self getData];
            };
            [self.navigationController pushViewController:vc animated:true];
            
        };
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"AuthorationTableViewCell";
        AuthorationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[AuthorationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }

        WeakSelf(self);
        cell.clickBlock = ^{
            StrongSelf(self)
            if (self.delegateLevelArray.count != 0) {
                DelegateLevelView *levelView = [DelegateLevelView new];
                levelView.arr = self.delegateLevelArray;
                [levelView show];
                weakself.delegateView = levelView;
                levelView.clickBlock = ^(NSInteger index) {
                      
                    NSDictionary *dic = self.delegateLevelArray[index];
                    self.type = [dic[@"id"] integerValue];
                    
                    [self authorizeAct];
                };
            }
            
          

        };
        return cell;
    }
    return nil;
}

-(void)authorizeAct{
    WeakSelf(self)
    [self.api AuthorizeWithparameters:@{@"UserId":@(self.customerId),@"IsClerk":@(self.type)} withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
         if (result.code == 200) {
             [self.delegateView hide];
             [JMBManager showBriefAlert:@"授权成功"];
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  [self.navigationController popViewControllerAnimated:true];
             });
            
         }
     }];
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        [_listTableView registerNib:[UINib nibWithNibName:@"MineDelegateInfoOneCell" bundle:nil] forCellReuseIdentifier:@"MineDelegateInfoOneCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"MineDelegateInfoTwoCell" bundle:nil] forCellReuseIdentifier:@"MineDelegateInfoTwoCell"];
         [_listTableView registerNib:[UINib nibWithNibName:@"MineDelegateInfoThreeCell" bundle:nil] forCellReuseIdentifier:@"MineDelegateInfoThreeCell"];
          [_listTableView registerNib:[UINib nibWithNibName:@"AuthorationTableViewCell" bundle:nil] forCellReuseIdentifier:@"AuthorationTableViewCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;
        
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}
@end
