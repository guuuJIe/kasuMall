//
//  MineDelegateRecommandVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineDelegateRecommandVC.h"
#import "DelegateRecommandCell.h"
#import "MineApi.h"
#import "MineDelegateInfoVC.h"
@interface MineDelegateRecommandVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) NSMutableArray *dataArray;
@property (nonatomic) MineApi *api;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@end

@implementation MineDelegateRecommandVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initProperty];
    [self setupUI];
    
    [self setupData:RefreshTypeNormal];
}

- (void)setupUI{
    self.title = @"我的团队";
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)initProperty{
    self.num = 1;
    self.size = 20;
    
}



- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    [self.api getMyGroupWithparameters:@"0" withPageNum:self.num andPageSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
//            [self configData:datas withRefreshType:type];
            self.dataArray = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.dataArray withNum:self.num];
            [self.listTableView reloadData];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"DelegateRecommandCell";
    DelegateRecommandCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[DelegateRecommandCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArray[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MineDelegateInfoVC *vc = [MineDelegateInfoVC new];
    NSDictionary *dic = self.dataArray[indexPath.row];
    vc.customerId = [dic[@"id"] integerValue];
    [self.navigationController pushViewController:vc animated:true];
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        [_listTableView registerNib:[UINib nibWithNibName:@"DelegateRecommandCell" bundle:nil] forCellReuseIdentifier:@"DelegateRecommandCell"];
      
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}
@end
