//
//  ImageZoomUtil.h
//  MallApp
//
//  Created by Mac on 2020/2/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageZoomUtil : NSObject
/**
 *  @param contentImageview 图片所在的imageView
 */
+(void)ImageZoomWithImageView:(UIImageView *)contentImageview;
@end

NS_ASSUME_NONNULL_END
