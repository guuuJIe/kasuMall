//
//  MineBusinessMangeVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineBusinessMangeVC.h"
#import "MineDelegateInfoVC.h"
#import "MineDelegateRecommandVC.h"
#import "MinePromptOrderVC.h"
#import "MinePromptGroupOrderVC.h"
#import "MinePromptFixOrderVC.h"
#import "MineCustomerVC.h"
#import "ApplyWholesaleManagerViewController.h"
#import "MinePromptProductVC.h"
#import "MineApi.h"
@interface MineBusinessMangeVC ()
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *recommView;
@property (weak, nonatomic) IBOutlet UIView *goodsView;
@property (weak, nonatomic) IBOutlet UIView *grouporderView;
@property (weak, nonatomic) IBOutlet UIView *fixOrderView;
@property (weak, nonatomic) IBOutlet UIView *myCustomerView;
@property (weak, nonatomic) IBOutlet UIView *promptProView;
@property (weak, nonatomic) IBOutlet UIView *applyWholeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *applyWholeViewHeight;
@property (strong, nonatomic) MineApi *api;
@end

@implementation MineBusinessMangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"业务管理";
    
    [self.infoView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.recommView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.goodsView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.grouporderView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.fixOrderView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.myCustomerView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.applyWholeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.promptProView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    
//    NSInteger isclerk = [self.dataDic[@"isClerk"] integerValue];
//    if (isclerk == 2) {
//        self.promptProView.hidden = false;
//    }else{
//        self.promptProView.hidden = true;
//    }
    
    [self.api getDelegateInfoithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            NSDictionary *dic = datas.firstObject;
            [self setupInfo:dic];
        }
    }];
    
}

- (void)setupInfo:(NSDictionary *)dic{
    NSInteger isClerk = [dic[@"isClerk"] integerValue];
    NSInteger isB = [dic[@"isB"] integerValue];
    if (isClerk == 2 || isClerk == 3 || isClerk == 4) {
        self.promptProView.hidden = false;
    }else{
        self.promptProView.hidden = true;
    }
    
    
    if (isB == 1) {
        self.applyWholeView.hidden = false;
    }else{
        self.applyWholeView.hidden = true;
        self.applyWholeViewHeight.constant = 0;
    }
    
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            MineDelegateInfoVC *vc = [[MineDelegateInfoVC alloc] init];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 101:
        {
            MineDelegateRecommandVC *vc = [[MineDelegateRecommandVC alloc] init];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 102:
        {
            MinePromptOrderVC *vc = [[MinePromptOrderVC alloc] init];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 103:
        {
            MinePromptGroupOrderVC *vc = [[MinePromptGroupOrderVC alloc] init];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 104:
        {
            MinePromptFixOrderVC *vc = [[MinePromptFixOrderVC alloc] init];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 106:
        {
            MineCustomerVC *vc = [[MineCustomerVC alloc] init];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 107:
        {
//            MineCustomerVC *vc = [[MineCustomerVC alloc] init];
//            [self.navigationController pushViewController:vc animated:true];
            JLog(@"点击审核");
            ApplyWholesaleManagerViewController *vc = [ApplyWholesaleManagerViewController new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
            
        case 108:
        {
            MinePromptProductVC *vc = [MinePromptProductVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            
            break;
            
        default:
            break;
    }
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    
    return _api;
}
@end
