//
//  MinePromptProductInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MinePromptProductInfoVC.h"
#import "PromptProductTableCell.h"
#import "MineApi.h"
#import "PurchaseView.h"
#import "QRProductTableCell.h"
#import "ShareView.h"
#import "PayMannagerUtil.h"
@interface MinePromptProductInfoVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) MineApi *api;
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic, strong) PurchaseView *purchaseView;
@end

@implementation MinePromptProductInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self getData];
}

- (void)setupUI{
    self.title = @"推广产品详情";
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)getData{
    [self.api getBussinessClerkProjectInfoWithparameters:self.pids withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dic = datas.firstObject;
            [self.listTableView reloadData];
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *Identifier = @"PromptProductTableCell";
        PromptProductTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[PromptProductTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
//        NSDictionary *dic = self.dataArray[indexPath.row];
        [cell setupdata:self.dic[@"productInfo"]];
        cell.shareBtn.hidden = true;
        return cell;
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"QRProductTableCell";
        QRProductTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[QRProductTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        [cell setupData:self.dic];

        return cell;
    }
    
    return nil;
    
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        [_listTableView registerClass:[PromptProductTableCell class] forCellReuseIdentifier:@"PromptProductTableCell"];
        [_listTableView registerClass:[QRProductTableCell class] forCellReuseIdentifier:@"QRProductTableCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;
        WeakSelf(self);
//        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            StrongSelf(self);
//
//            [self setupData:RefreshTypeDown];
//
//        }];
//
//        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            StrongSelf(self);
//            [self setupData:RefreshTypeUP];
//        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    
    return _api;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"立即分享" forState:0];
         WeakSelf(self)
        _purchaseView.addAddressBlock = ^{
            ShareView *shareview = [ShareView new];
            [shareview show];
            WeakSelf(shareview)
            shareview.clickBlock = ^(NSInteger index) {
                [[PayMannagerUtil sharedManager] wxShareWithDic:weakself.dic successBlock:^(BOOL success) {
                    [weakshareview hide];
                } failBlock:^(BOOL fail) {
                    
                } InSecen:index];
                 
            };
        };
        
        [self.view addSubview:_purchaseView];
    }
    
    return _purchaseView;
}

@end
