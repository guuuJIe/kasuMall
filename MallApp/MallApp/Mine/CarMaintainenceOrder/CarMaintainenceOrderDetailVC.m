//
//  CarMaintainenceOrderDetailVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarMaintainenceOrderDetailVC.h"

#import "ServicesDetailSectionOneCell.h"
#import "ServicesOrderDetailSectionTwoCell.h"
#import "ServicesOrderDetailSectionThreeCell.h"
#import "ServicesOrderDetailSectionFourCell.h"
#import "PurchaseView.h"
#import "CarMaintainenceListVC.h"
#import "ApplyRepairVC.h"
#import "GroupApi.h"
#import "WebVC.h"
#import "SubmitMyCarInfoVC.h"
@interface CarMaintainenceOrderDetailVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) PurchaseView *purchaseView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSDictionary *dic;

@property (nonatomic, assign) BOOL isSelected;
@end

@implementation CarMaintainenceOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setupUI];
    
    [self getData];
}

- (void)setupUI{
    self.title = @"订单详情";
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).offset(-50-BottomAreaHeight);
    }];
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)getData{
    
    [self.api getEmOrderListInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dic = datas.firstObject;
            NSInteger statues = [self.dic[@"status"] integerValue];
            NSInteger isRecord = [self.dic[@"nowRecordId"] integerValue];
            if (statues == 1 && isRecord == 0) {
                self.purchaseView.hidden = false;
            }else if(statues == 0){
                self.purchaseView.hidden = false;
                [self.purchaseView.purchaseButton setTitle:@"再次购买" forState:0];
            }else{
                self.purchaseView.hidden = true;
            }
            [self.listTableView reloadData];
        }
    }];
    
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        static NSString *Identifier = @"ServicesDetailSectionOneCell";
        ServicesDetailSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        WeakSelf(self)
        cell.clickBlock = ^{
            CarMaintainenceListVC *vc = [CarMaintainenceListVC new];
            vc.orderNum = weakself.orderNum;
            [weakself.navigationController pushViewController:vc animated:true];
        };
        [cell setupData:self.dic];
        cell.stateLbl.text = @"保养记录";
        return cell;
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"ServicesOrderDetailSectionTwoCell";
        ServicesOrderDetailSectionTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dic];
        return cell;
    }else if (indexPath.row == 2){
        static NSString *Identifier = @"ServicesOrderDetailSectionThreeCell";
        ServicesOrderDetailSectionThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        WeakSelf(self)
        cell.clickBlock = ^(BOOL isSel) {

            weakself.isSelected = isSel;
            
            [weakself.listTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        };
        
        cell.checkDelegateBlock = ^{
            WebVC *vc = [WebVC new];
            vc.ids = [weakself.dic[@"productId"] integerValue];
            [weakself.navigationController pushViewController:vc animated:true];
        };
        [cell setupData:self.dic];
        return cell;
    }else if (indexPath.row == 3){
        static NSString *Identifier = @"ServicesOrderDetailSectionFourCell";
        ServicesOrderDetailSectionFourCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dic isHidden:self.isSelected];
        return cell;
    }
   

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        if (self.isSelected) {
            return 65.0f;
        }else{
            return CGFLOAT_MIN;
        }
    }
    
    return UITableViewAutomaticDimension;
}



- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.tableHeaderView = [UIView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.estimatedSectionHeaderHeight = 0;
        _listTableView.estimatedSectionFooterHeight = 0;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesDetailSectionOneCell" bundle:nil] forCellReuseIdentifier:@"ServicesDetailSectionOneCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderDetailSectionTwoCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderDetailSectionTwoCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderDetailSectionThreeCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderDetailSectionThreeCell"];
         [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderDetailSectionFourCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderDetailSectionFourCell"];
        _listTableView.showsVerticalScrollIndicator= false;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"申请保养" forState:0];
        WeakSelf(self);
        _purchaseView.addAddressBlock = ^{
            StrongSelf(self)
            
            NSInteger statues = [self.dic[@"status"] integerValue];
            if (statues == 0) {
                SubmitMyCarInfoVC *vc = [[SubmitMyCarInfoVC alloc] initWithNibName:@"SubmitMyCarInfoVC" bundle:nil];
                vc.ids = [self.dic[@"productId"] integerValue];
                [self.navigationController pushViewController:vc animated:true];
            }else{
                ApplyRepairVC *vc = [ApplyRepairVC new];
                vc.orderNum = weakself.orderNum;
                vc.type = 1;
                [weakself.navigationController pushViewController:vc animated:true];
            }
           
        };
    }
    return _purchaseView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}
@end
