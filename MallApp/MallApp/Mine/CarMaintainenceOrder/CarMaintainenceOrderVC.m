//
//  CarMaintainenceOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarMaintainenceOrderVC.h"
#import "EmptyViewForRecord.h"
#import "MyOrderHeadView.h"
#import "ServicesOrderItemCell.h"
#import "GroupApi.h"
#import "ServicesOfOrderFootView.h"
#import "CarMaintainenceOrderDetailVC.h"
#import "SubmitMyCarInfoVC.h"
@interface CarMaintainenceOrderVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>

@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, strong) GroupApi *api;

@end

@implementation CarMaintainenceOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"保养记录";
    
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"CalculateCarMaintenancePriceVC",@"SubmitMyCarInfoVC",@"SelectPayWayVC"]];
    
    [self setupProp];
    
    [self setupUI];
    
    [self setupData:RefreshTypeDown];
}


- (void)setupProp{
    
    self.num = 1;
    self.size = 20;
    self.statues = 1;
}

- (void)setupUI{
//    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.equalTo(self.view);
//        make.height.mas_equalTo(45);
//    }];
    
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"ServicesOrderItemCell";
    ServicesOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    [cell setupData:self.datas[indexPath.section]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MyOrderHeadView *HeadView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderHeadView"];
    [HeadView setupEwData:self.datas[section]];
    return HeadView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    [footView setupOpeationViewWithDic:self.datas[section] andType:UserCarMaintainceOpe andIsNeedShow:true];
    
    return [footView cellHeight];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ServicesOfOrderFootView *footView = [ServicesOfOrderFootView new];
    NSDictionary *dic = self.datas[section];
    [footView setupOpeationViewWithDic:self.datas[section] andType:UserCarMaintainceOpe andIsNeedShow:true];
    WeakSelf(self)
    footView.clickBlock = ^(NSString * _Nonnull flag) {
        StrongSelf(self)
        if ([flag isEqualToString:@"purchaseAgain"]) {
            
            SubmitMyCarInfoVC *vc = [[SubmitMyCarInfoVC alloc] initWithNibName:@"SubmitMyCarInfoVC" bundle:nil];
            vc.ids = [dic[@"productId"] integerValue];
            [self.navigationController pushViewController:vc animated:true];
            
        }else if ([flag isEqualToString:@"conact"]){
            [XJUtil callTelNoCommAlert:@"400-163-8887"];
        }
    };
    return footView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CarMaintainenceOrderDetailVC *vc = [CarMaintainenceOrderDetailVC new];
    NSDictionary *dic = self.datas[indexPath.section];
    vc.orderNum = dic[@"orderNumber"];
    
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    EmptyViewForRecord *view = [EmptyViewForRecord initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-115, 150, 150);
    view.desLbl.text = @"暂无数据";
    return view;
}



- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    

    
    [self.api getCarMaintainenceOrderListListnWithparameters:@(self.statues) withpageSize:self.size pageNum:self.num withCompletionHandler:^(NSError *error, MessageBody *result) {
         if (result.code == 200) {
        
             NSArray *array = result.result;
             
             self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
             [self.listTableView reloadData];
             [self.listTableView reloadEmptyDataSet];
         }
    }];
}


- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];

       
    }
    return _datas;
}




- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        [_listTableView setEmptyDataSetSource:self];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderItemCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderItemCell"];
        [_listTableView registerClass:[MyOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"MyOrderHeadView"];
//        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);

            [self setupData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (GroupApi *)api{
    if (!_api) {
        _api =[GroupApi new];
    }
    return _api;
}


@end
