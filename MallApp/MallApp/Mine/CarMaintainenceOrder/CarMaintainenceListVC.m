//
//  CarMaintainenceListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarMaintainenceListVC.h"
#import "LiPeiRecordItemCell.h"
#import "EmptyViewForRecord.h"
#import "LiPeiJiLuDetailVC.h"
#import "GroupApi.h"
@interface CarMaintainenceListVC ()<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) GroupApi *api;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;

@property (nonatomic, strong) NSMutableArray *datas;

@end

@implementation CarMaintainenceListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self setupProp];
    
    [self setupUI];
    

    [self setupData:RefreshTypeDown];
    
   
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = UIColorEF;
}



- (void)setupUI{
    
    [self.view addSubview:self.TabelView];
     self.title = @"保养记录";
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {

        make.edges.mas_equalTo(self.view);
    }];
    
   
}


- (void)setupProp{

    self.num = 1;
    self.size = 20;
   
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    [self.api getEmRecordListWithparameters:self.orderNum withpageSize:self.size pageNum:self.num withCompletionHandler:^(NSError *error, MessageBody *result) {
         if (result.code == 200) {
        
             NSArray *array = result.result;
             
             self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.TabelView AndCurDataArray:self.datas withNum:self.num];
             [self.TabelView reloadData];
             [self.TabelView reloadEmptyDataSet];
         }
    }];
}



#pragma mark ---UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *Identifier = @"LiPeiRecordItemCell";
    LiPeiRecordItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
   
    [cell setupData:self.datas[indexPath.row]];

    return cell;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LiPeiJiLuDetailVC *vc = [LiPeiJiLuDetailVC new];
    NSDictionary *dic = self.datas[indexPath.row];
    vc.ids = [NSString stringWithFormat:@"%@",dic[@"id"]];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:true];
}


#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    EmptyViewForRecord *view = [EmptyViewForRecord initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-115, 150, 150);
    view.desLbl.text = @"暂无保养记录";
    return view;
}






-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setEmptyDataSetSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
      
        [_TabelView registerNib:[UINib nibWithNibName:@"LiPeiRecordItemCell" bundle:nil] forCellReuseIdentifier:@"LiPeiRecordItemCell"];
        WeakSelf(self);
        _TabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        //
        _TabelView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        _TabelView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}


@end
