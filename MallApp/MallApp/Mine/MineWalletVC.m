//
//  MineWalletVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineWalletVC.h"
#import "MineApi.h"
#import "RechargeVC.h"
#import "WithdrawVC.h"
#import "MinExchangeRecodeVC.h"
#import "MineBackRecordVC.h"
#import "WithdrawDetailVC.h"
@interface MineWalletVC ()
@property (nonatomic) MineApi *mineApi;
@property (weak, nonatomic) IBOutlet UILabel *BalaceLbl;
@end

@implementation MineWalletVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的钱包";
    
    
    [self initClick];
    
}

- (void)initClick{
    [self.rechargeLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
     [self.submitView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
     [self.rechargeRecordView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
     [self.backRecordView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupData];
}

- (void)setupData{
    [self.mineApi getAmountWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *data = result.resultDic[@"list"];
            CGFloat price = [data.firstObject doubleValue];
            self.BalaceLbl.text = [NSString stringWithFormat:@"剩余:(%.3f元)",price];
            //            self.BalaceLbl.text = [NSString stringWithFormat:@"%@",data.firstObject];
        }
    }];
   
}

- (void)click:(UITapGestureRecognizer *)ges{
    NSLog(@"%ld",(long)ges.view.tag);
    NSInteger tag = ges.view.tag;
    if (tag == 100) {
        RechargeVC *vc = [[RechargeVC alloc] initWithNibName:@"RechargeVC" bundle:nil];
        [self.navigationController pushViewController:vc animated:true];
        
    }else if (tag == 101){
        [self.mineApi withDrawWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *datas = result.result;
                if (![datas isEqual:[NSNull null]]) {
                    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"当前有未处理的提现订单,是否前往查看" andHandler:^(NSInteger index) {
                        if (index == 1) {
                            WithdrawDetailVC *vc = [[WithdrawDetailVC alloc] initWithNibName:@"WithdrawDetailVC" bundle:nil];
                            vc.data = datas.firstObject;
                            [self.navigationController pushViewController:vc animated:true];
                        }
                    }];
                }else{
                    WithdrawVC *vc = [[WithdrawVC alloc] initWithNibName:@"WithdrawVC" bundle:nil];
                    vc.moneyStr = self.BalaceLbl.text;
                    [self.navigationController pushViewController:vc animated:true];
                    
                }
            }
        }];
       
    }else if (tag == 102){
        MinExchangeRecodeVC *vc = [MinExchangeRecodeVC new];
        [self.navigationController pushViewController:vc animated:true];
    }else if (tag == 103){
        MineBackRecordVC *vc = [[MineBackRecordVC alloc] init];
        [self.navigationController pushViewController:vc animated:true];
       
    }

}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    
    return _mineApi;
 
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
