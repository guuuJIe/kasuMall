//
//  AdresslistTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdresslistTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
- (void)setupData:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *defaultBtn;
@property (weak, nonatomic) IBOutlet UIButton *Edit;
- (IBAction)deleteAct:(UIButton *)sender;
- (IBAction)setDefault:(UIButton *)sender;
@property (nonatomic,copy) void(^deleteBlock)(void);
@property (nonatomic,copy) void(^setDefalutBlock)(void);
@end

NS_ASSUME_NONNULL_END
