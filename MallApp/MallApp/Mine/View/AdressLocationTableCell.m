//
//  AdressLocationTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/22.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AdressLocationTableCell.h"
@interface AdressLocationTableCell()
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *infoLabel;
@property(nonatomic,strong)UIView *line;

@end
@implementation AdressLocationTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    self.backgroundColor=[UIColor whiteColor];
    self.titleLabel=[[UILabel alloc] init];
    self.titleLabel.textColor=APPFourColor;
    self.titleLabel.font=LabelFont16;
    self.titleLabel.numberOfLines = 0;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        
    }];
    
    self.infoLabel=[[UILabel alloc] init];
    self.infoLabel.textColor=UIColor999;
    self.infoLabel.font=LabelFont14;
    [self.contentView addSubview:self.infoLabel];
    [self.infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(12);
        make.right.mas_equalTo(-12);
        
    }];
    
    self.line = [UIView new];
    self.line.backgroundColor = UIColorEF;
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(self.infoLabel.mas_bottom).offset(Default_Space);
    }];
}

-(void)showData:(DDSearchPoi *)poi index:(int)index{
    self.titleLabel.text = poi.name;
    self.infoLabel.text = poi.address;
}
@end
