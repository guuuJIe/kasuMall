//
//  MyGoodsOrderFootView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyGoodsOrderFootView : UITableViewHeaderFooterView
@property (nonatomic,copy) void(^clickBlock)(NSString *flag);
- (CGFloat)cellHeight;
- (void)setupData:(NSDictionary *)dic withGoodsNum:(NSInteger)num withType:(OrderOperationType)type;
@end

NS_ASSUME_NONNULL_END
