#import "MinePromptView.h"
#import "UnderLineButton.h"
@interface MinePromptView()

@property (nonatomic , strong) UnderLineButton *selectedBtn;

@end
@implementation MinePromptView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.userInteractionEnabled = YES;
//        [self setupLayout];
    }
    return self;
}
- (void)setupLayout
{
//    UIView *lastView;
//    NSArray *arr = @[@"全部",@"未支付",@"进行中",@"退货中",@"已完成"];
//    for (int i = 0; i<arr.count; i++) {
//        UnderLineButton *button = [UnderLineButton new];
//        button.lineView.backgroundColor = UIColor08;
//        button.l_width = 50;
//        [button setTitle:arr[i] forState:0];
//        [button setTitleColor:UIColor333 forState:0];
//        [button setBackgroundColor:[UIColor whiteColor]];
//        [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
//        [button setTitleColor:UIColor08 forState:UIControlStateSelected];
//        button.tag = i+200;
//        [self addSubview:button];
//        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
//        [button mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (i == 0) {
//                make.left.equalTo(self).offset(1);
//            }else{
//                make.left.equalTo(lastView.mas_right);
//            }
//            make.top.equalTo(self).offset(0);
//            make.width.mas_equalTo(Screen_Width/5);
//            make.bottom.equalTo(self).offset(0*AdapterScal);
//        }];
//        lastView = button;
//    }

}

- (void)setDataSource:(NSArray *)dataSource{
    UIView *lastView;
    CGFloat singWidhth = Screen_Width/dataSource.count;
    for (int i = 0; i<dataSource.count; i++) {
        UnderLineButton *button = [UnderLineButton new];
        button.lineView.backgroundColor = UIColor08;
        button.l_width = 50;
        [button setTitle:dataSource[i] forState:0];
        [button setTitleColor:UIColor333 forState:0];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [button setTitleColor:UIColor08 forState:UIControlStateSelected];
//        [button setTitleColor:UIColor333 forState:UIControlStateFocused];
//        button.highlighted = false;
        button.tag = i+200;
        [self addSubview:button];
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.equalTo(self).offset(1);
            }else{
                make.left.equalTo(lastView.mas_right);
            }
            make.top.equalTo(self).offset(0);
            make.width.mas_equalTo(singWidhth);
            make.bottom.equalTo(self).offset(0*AdapterScal);
        }];
        lastView = button;
    }
}

-(void)setOrderStatues:(NSInteger)orderStatues
{
    _orderStatues = orderStatues;
    UnderLineButton *button = [self viewWithTag:self.orderStatues];
    [self click:button];
}


- (void)click:(UnderLineButton *)btn
{
    if (btn!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
//        self.selectedBtn = btn;
        self.selectedBtn.selected = YES;
    }
    if (self.clickBlock) {
        self.clickBlock(btn.tag);
    }
}

@end
