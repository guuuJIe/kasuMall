//
//  DelegateRecommandCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "DelegateRecommandCell.h"

@implementation DelegateRecommandCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"userName"] andDefailtInfo:@""];
        self.idLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"clerkStatus"] andDefailtInfo:@""];
        self.timeLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"createTime"] andDefailtInfo:@""];
    }
}

@end
