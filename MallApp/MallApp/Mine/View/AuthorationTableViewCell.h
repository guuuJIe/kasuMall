//
//  AuthorationTableViewCell.h
//  MallApp
//
//  Created by Mac on 2020/2/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AuthorationTableViewCell : UITableViewCell
- (IBAction)authorizateAct:(id)sender;
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
