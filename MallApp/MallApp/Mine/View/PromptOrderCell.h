//
//  PromptOrderCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PromptOrderCell : UITableViewCell
- (void)setupData:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UILabel *orderNumText;
@property (weak, nonatomic) IBOutlet UILabel *orderPriceText;
@property (weak, nonatomic) IBOutlet UILabel *createTimeText;
@property (weak, nonatomic) IBOutlet UILabel *statuesText;
@end

NS_ASSUME_NONNULL_END
