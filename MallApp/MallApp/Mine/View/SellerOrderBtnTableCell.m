//
//  SellerOrderBtnTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerOrderBtnTableCell.h"
#import "MineFuncItemCell.h"
@interface SellerOrderBtnTableCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) UICollectionView *colletionView;
@property (nonatomic) NSMutableArray *dataArr;
@end
@implementation SellerOrderBtnTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(12,12,Screen_Width,20);
    label.numberOfLines = 0;
    label.text = @"我的订单";
    label.font = LabelFont14;
    [self.contentView addSubview:label];
    
    [self.contentView addSubview:self.colletionView];
    [self.colletionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.top.equalTo(label.mas_bottom).offset(0);
    }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MineFuncItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MineFuncItemCell" forIndexPath:indexPath];
//    cell.model = self.old_wine[indexPath.row];
//    cell.backgroundColor = RandomColor;
    [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.dataArr[indexPath.row];
    if (self.clickBlock) {
        self.clickBlock(indexPath.row,[NSString stringWithFormat:@"%@",dic[@"name"]]);
    }
}


- (UICollectionView *)colletionView
{
    if (!_colletionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(Screen_Width/4,70);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _colletionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _colletionView.showsVerticalScrollIndicator = NO;
        _colletionView.showsHorizontalScrollIndicator = NO;
        _colletionView.backgroundColor = [UIColor clearColor];
        _colletionView.delegate = self;
        _colletionView.dataSource = self;
        [_colletionView registerClass:[MineFuncItemCell class] forCellWithReuseIdentifier:@"MineFuncItemCell"];
    }
    return _colletionView;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        
        _dataArr = [NSMutableArray array];
        NSArray *arr = [NSArray arrayWithObjects:
  @{@"name":@"商品订单",@"image":@"S_1"},@{@"name":@"维修订单",@"image":@"S_2"},@{@"name":@"保养订单",@"image":@"S_3"},@{@"name":@"保修订单",@"image":@"S_4"},@{@"name":@"退款订单",@"image":@"S_5"},@{@"name":@"抢修订单",@"image":@"S_6"},@{@"name":@"施救订单",@"image":@"S_7"},@{@"name":@"维修套餐订单",@"image":@"S_8"}, nil];
        [_dataArr addObjectsFromArray:arr];
    }
    return _dataArr;
}
@end
