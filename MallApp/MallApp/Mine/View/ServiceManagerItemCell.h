//
//  ServiceManagerItemCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceManagerItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *levelLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
- (IBAction)bindservice:(id)sender;
- (void)setupData:(NSDictionary *)dic;
@property (nonatomic,copy) void(^bindBlock)(void);
@end

NS_ASSUME_NONNULL_END
