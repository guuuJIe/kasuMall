//
//  PromptOrderCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PromptOrderCell.h"

@implementation PromptOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorF5F7;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSInteger stautues = [dic[@"orderStatus"] integerValue];
        if (stautues == 1) {
            self.statuesText.text = @"未支付";
            self.statuesText.textColor = UIColor999;
        }else if (stautues == 2){
            self.statuesText.text = @"进行中";
            
        }else if (stautues == 3){
            self.statuesText.text = @"退货中";
            self.statuesText.textColor = UIColor999;
            
        }else if (stautues == 4){
            self.statuesText.text = @"已完成";
            self.statuesText.textColor = APPColor;
            
        }
        self.orderNumText.text = [NSString stringWithFormat:@"订单编号：%@",dic[@"orderNumber"]];
       self.orderPriceText.text = [NSString stringWithFormat:@"%@",dic[@"realPrice"]];
        self.createTimeText.text = [XJUtil insertStringWithNotNullObject:dic[@"createTime"] andDefailtInfo:@""];
        
    }
}

@end
