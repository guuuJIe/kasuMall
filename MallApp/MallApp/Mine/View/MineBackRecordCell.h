//
//  MineBackRecordCell.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineBackRecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *OrderNum;
@property (weak, nonatomic) IBOutlet UILabel *totalNum;
@property (weak, nonatomic) IBOutlet UILabel *backNum;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *perTimeLbl;

@property (weak, nonatomic) IBOutlet UILabel *curTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *statuesLbl;

- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
