//
//  BankCardTableViewCell.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankCardTableViewCell : UITableViewCell
- (void)setupData:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UILabel *bankTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLbl;
- (IBAction)BtnClick:(id)sender;
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
