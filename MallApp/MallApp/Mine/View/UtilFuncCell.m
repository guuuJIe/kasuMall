//
//  UtilFuncCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UtilFuncCell.h"
#import "MineFuncItemCell.h"
@interface UtilFuncCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) UICollectionView *colletionView;
@property (nonatomic) NSMutableArray *dataArr;
@end

@implementation UtilFuncCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    [self.contentView addSubview:view];
    view.backgroundColor = UIColorEF;
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(12,CGRectGetMaxY(view.frame)+12,Screen_Width,20);
    label.numberOfLines = 0;
    label.text = @"常用功能";
    label.font = LabelFont14;
    [self.contentView addSubview:label];
    
    [self.contentView addSubview:self.colletionView];
    [self.colletionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.top.equalTo(label.mas_bottom).offset(0);
    }];

}

- (void)setMenuArr:(NSArray *)menuArr{
    if (menuArr.count == 0) {
        return;
    }
    _menuArr = menuArr;
    [self.colletionView reloadData];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSInteger isClerk = [dic[@"isClerk"] integerValue];
        if (isClerk == 0) {
//            self.menuArr = [self.dataArr objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 8)]];
//            [self.colletionView reloadData];
//            self.menuArr = @[@{@"name":@"地址管理",@"image":@"M_item_1"},@{@"name":@"发票管理",@"image":@"M_item_2"},@{@"name":@"用户反馈",@"image":@"M_item_3"},@{@"name":@"我的钱包",@"image":@"M_item_4"},@{@"name":@"我的卡包",@"image":@"M_item_6"},@{@"name":@"服务经理",@"image":@"M_item_8"},@{@"name":@"申请批发",@"image":@"M_item_5"},@{@"name":@"保修服务",@"image":@"M_item_9"}];,@{@"name":@"服务经理",@"image":@"M_item_8"}
            self.menuArr = @[@{@"name":@"我的钱包",@"image":@"M_item_5"},@{@"name":@"我的卡包",@"image":@"M_item_6"},@{@"name":@"地址管理",@"image":@"M_item_1"},@{@"name":@"发票管理",@"image":@"M_item_2"},@{@"name":@"服务经理",@"image":@"M_item_8"},@{@"name":@"全车保修",@"image":@"M_item_9"},@{@"name":@"一年保养",@"image":@"M_item_11"},@{@"name":@"用户反馈",@"image":@"M_item_8"},@{@"name":@"客服电话",@"image":@"M_item_10"},@{@"name":@"申请批发",@"image":@"M_item_4"},@{@"name":@"申请开店",@"image":@"M_item_12"},@{@"name":@"成为团长",@"image":@"M_group_1"}];
        }else if (isClerk == 1){
//            self.menuArr = @[@{@"name":@"地址管理",@"image":@"M_item_1"},@{@"name":@"发票管理",@"image":@"M_item_2"},@{@"name":@"用户反馈",@"image":@"M_item_3"},@{@"name":@"我的钱包",@"image":@"M_item_4"},@{@"name":@"我的卡包",@"image":@"M_item_6"},@{@"name":@"业务管理",@"image":@"M_item_7"},@{@"name":@"服务经理",@"image":@"M_item_8"},@{@"name":@"申请批发",@"image":@"M_item_5"},@{@"name":@"保修服务",@"image":@"M_item_9"}];@{@"name":@"服务经理",@"image":@"M_item_8"},
//                       [self.colletionView reloadData];
               self.menuArr = @[@{@"name":@"我的钱包",@"image":@"M_item_5"},@{@"name":@"我的卡包",@"image":@"M_item_6"},@{@"name":@"地址管理",@"image":@"M_item_1"},@{@"name":@"发票管理",@"image":@"M_item_2"},@{@"name":@"业务管理",@"image":@"M_item_7"},@{@"name":@"服务经理",@"image":@"M_item_8"},@{@"name":@"全车保修",@"image":@"M_item_9"},@{@"name":@"一年保养",@"image":@"M_item_11"},@{@"name":@"用户反馈",@"image":@"M_item_8"},@{@"name":@"客服电话",@"image":@"M_item_10"},@{@"name":@"申请批发",@"image":@"M_item_4"},@{@"name":@"申请开店",@"image":@"M_item_12"},];
//            @{@"name":@"成为团长",@"image":@"M_item_12"}
        }else if (isClerk == 6){
//            self.menuArr = [self.dataArr objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 7)]];
//             self.menuArr = @[@{@"name":@"地址管理",@"image":@"M_item_1"},@{@"name":@"发票管理",@"image":@"M_item_2"},@{@"name":@"用户反馈",@"image":@"M_item_3"},@{@"name":@"我的钱包",@"image":@"M_item_4"},@{@"name":@"我的卡包",@"image":@"M_item_6"},@{@"name":@"服务经理",@"image":@"M_item_8"},@{@"name":@"申请批发",@"image":@"M_item_5"},@{@"name":@"保修服务",@"image":@"M_item_9"}];@{@"name":@"服务经理",@"image":@"M_item_8"},
            self.menuArr = @[@{@"name":@"我的钱包",@"image":@"M_item_5"},@{@"name":@"我的卡包",@"image":@"M_item_6"},@{@"name":@"地址管理",@"image":@"M_item_1"},@{@"name":@"发票管理",@"image":@"M_item_2"},@{@"name":@"服务经理",@"image":@"M_item_8"},@{@"name":@"全车保修",@"image":@"M_item_9"},@{@"name":@"一年保养",@"image":@"M_item_11"},@{@"name":@"用户反馈",@"image":@"M_item_8"},@{@"name":@"客服电话",@"image":@"M_item_10"},@{@"name":@"申请批发",@"image":@"M_item_4"},@{@"name":@"申请开店",@"image":@"M_item_12"},];
//            @{@"name":@"成为团长",@"image":@"M_item_12"}
        }
        [self.colletionView reloadData];
    }
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.menuArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MineFuncItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MineFuncItemCell" forIndexPath:indexPath];
//    cell.model = self.old_wine[indexPath.row];
//    cell.backgroundColor = RandomColor;
    [cell setupData:self.menuArr[indexPath.row]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.menuArr[indexPath.row];
    if (self.clickBlock) {
        self.clickBlock(indexPath.row,[NSString stringWithFormat:@"%@",dic[@"name"]]);
    }
}


- (UICollectionView *)colletionView
{
    if (!_colletionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(Screen_Width/4,70);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _colletionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _colletionView.showsVerticalScrollIndicator = NO;
        _colletionView.showsHorizontalScrollIndicator = NO;
        _colletionView.backgroundColor = [UIColor clearColor];
        _colletionView.delegate = self;
        _colletionView.dataSource = self;
        [_colletionView registerClass:[MineFuncItemCell class] forCellWithReuseIdentifier:@"MineFuncItemCell"];
    }
    return _colletionView;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        
        _dataArr = [NSMutableArray array];
        NSArray *arr = [NSArray arrayWithObjects:
  @{@"name":@"地址管理",@"image":@"M_item_1"},@{@"name":@"发票管理",@"image":@"M_item_2"},@{@"name":@"用户反馈",@"image":@"M_item_3"},@{@"name":@"我的钱包",@"image":@"M_item_4"},@{@"name":@"我的卡包",@"image":@"M_item_6"},@{@"name":@"申请批发",@"image":@"M_item_5"},@{@"name":@"服务经理",@"image":@"M_item_8"},@{@"name":@"业务管理",@"image":@"M_item_7"}, nil];
        [_dataArr addObjectsFromArray:arr];
    }
    return _dataArr;
}
@end
