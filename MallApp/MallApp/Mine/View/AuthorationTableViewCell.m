//
//  AuthorationTableViewCell.m
//  MallApp
//
//  Created by Mac on 2020/2/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AuthorationTableViewCell.h"

@implementation AuthorationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)authorizateAct:(id)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}
@end
