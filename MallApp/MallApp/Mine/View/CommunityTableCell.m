//
//  CommunityTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CommunityTableCell.h"
#import "MineFuncItemCell.h"
@interface CommunityTableCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) UICollectionView *colletionView;
@property (nonatomic) NSMutableArray *dataArr;
@end

@implementation CommunityTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    [self.contentView addSubview:view];
    view.backgroundColor = UIColorEF;
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(12,CGRectGetMaxY(view.frame)+12,Screen_Width,20);
    label.numberOfLines = 0;
    label.text = @"社区团长";
    label.font = LabelFont14;
    [self.contentView addSubview:label];
    
    [self.contentView addSubview:self.colletionView];
    [self.colletionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.top.equalTo(label.mas_bottom).offset(0);
    }];
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MineFuncItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MineFuncItemCell" forIndexPath:indexPath];
//    cell.model = self.old_wine[indexPath.row];
//    cell.backgroundColor = RandomColor;
    [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.dataArr[indexPath.row];
    if (self.clickBlock) {
        self.clickBlock(indexPath.row,[NSString stringWithFormat:@"%@",dic[@"name"]]);
    }
}


- (UICollectionView *)colletionView
{
    if (!_colletionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(Screen_Width/4,70);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _colletionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _colletionView.showsVerticalScrollIndicator = NO;
        _colletionView.showsHorizontalScrollIndicator = NO;
        _colletionView.backgroundColor = [UIColor clearColor];
        _colletionView.delegate = self;
        _colletionView.dataSource = self;
        [_colletionView registerClass:[MineFuncItemCell class] forCellWithReuseIdentifier:@"MineFuncItemCell"];
    }
    return _colletionView;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        
        _dataArr = [NSMutableArray array];
        NSArray *arr = [NSArray arrayWithObjects:
  @{@"name":@"我是团长",@"image":@"M_group_1"},@{@"name":@"我的项目",@"image":@"M_group_3"},
  @{@"name":@"我的业绩",@"image":@"M_group_4"},@{@"name":@"邀请好友",@"image":@"M_group_5"}, nil];
        [_dataArr addObjectsFromArray:arr];
    }
    return _dataArr;
}
@end
