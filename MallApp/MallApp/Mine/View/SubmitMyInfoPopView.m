//
//  SubmitMyInfoPopView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitMyInfoPopView.h"
@interface SubmitMyInfoPopView()
@property (nonatomic, strong) UITextField *nameText;
@property (nonatomic, strong) UITextField *telText;
@end

@implementation SubmitMyInfoPopView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.cornerRadius = 5;
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(27);
            make.right.mas_equalTo(-27);
            make.height.mas_equalTo(244);
            make.center.mas_equalTo(self);
        }];
        UILabel *titleLbl = [UILabel new];
        titleLbl.text = @"填写资料";
        titleLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
        titleLbl.textColor = UIColor333;
        [bgView addSubview:titleLbl];
        [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.top.mas_equalTo(10);
        }];
        
        UIView *view = [UIView new];
        view.backgroundColor = UIColorEF;
        [bgView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(bgView);
            make.top.mas_equalTo(titleLbl.mas_bottom).offset(9);
            make.height.mas_equalTo(lineHeihgt);
        }];
        
        UILabel *title = [UILabel new];
        title.text = @"姓名";
        title.textColor = UIColor66;
        title.font = LabelFont14;
        
        UITextField *textF = [UITextField new];
        textF.placeholder = @"请输入姓名";
        textF.borderStyle = UITextBorderStyleRoundedRect;
        textF.textColor = UIColor333;
        self.nameText = textF;
        [bgView addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.top.mas_equalTo(view.mas_bottom).offset(36*AdapterScal);
        }];
        
        [bgView addSubview:textF];
        [textF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(60);
            make.size.mas_equalTo(CGSizeMake(220*AdapterScal, 40*AdapterScal));
            make.centerY.mas_equalTo(title);
        }];
        
        
        UILabel *title2 = [UILabel new];
        title2.text = @"性别";
        title2.textColor = UIColor66;
        title2.font = LabelFont14;
        
        UITextField *textF2 = [UITextField new];
        textF2.placeholder = @"男/女";
        textF2.borderStyle = UITextBorderStyleRoundedRect;
        textF2.textColor = UIColor333;
        self.telText = textF2;
        [bgView addSubview:title2];
        [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.top.mas_equalTo(title.mas_bottom).offset(37*AdapterScal);
        }];
        
        [bgView addSubview:textF2];
        [textF2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(60);
            make.size.mas_equalTo(CGSizeMake(220*AdapterScal, 40*AdapterScal));
            make.centerY.mas_equalTo(title2);
        }];
        
        UIButton *submitBtn = [UIButton new];
        [submitBtn setTitle:@"提交" forState:0];
        [submitBtn setTitleColor:UIColor.whiteColor forState:0];
        [submitBtn.titleLabel setFont:LabelFont14];
        [submitBtn setBackgroundColor:APPColor];
        submitBtn.layer.cornerRadius = 6;
        [submitBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:submitBtn];
        
        [submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-25);
            make.height.mas_equalTo(40);
            make.left.mas_equalTo(19);
            make.right.mas_equalTo(-19);
        }];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideBg)]];
        
               
    }
    
    return self;
}

- (void)hideBg{
    [self hide];
}

- (void)click{
    
    if (self.nameText.text.length == 0 || self.telText.text.length == 0) {
        
        [JMBManager showBriefAlert:@"信息填写不完整"];
        return;
    }
    
    if (self.clickBlock) {
        self.clickBlock(self.nameText.text,self.telText.text);
    }
}

@end
