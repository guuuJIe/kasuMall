//
//  FixcardCell.h
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FixcardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *sellerLbl;
@property (weak, nonatomic) IBOutlet UILabel *accountLbl;
@property (weak, nonatomic) IBOutlet UIImageView *isUsedIcon;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
