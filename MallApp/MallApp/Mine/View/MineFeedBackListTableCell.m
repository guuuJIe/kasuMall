//
//  MineFeedBackListTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineFeedBackListTableCell.h"

@implementation MineFeedBackListTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@""];
        self.contentLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"content"] andDefailtInfo:@""];
        self.replyLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"回复:%@",dic[@"reply"]] andDefailtInfo:@"回复:"];
        self.timeLbl.text = dic[@"updated"];
        NSInteger reply = [dic[@"isReply"] integerValue];
        if (reply == 0) {
            self.statuesLbl.text = @"待回复";
        }
    }
}

@end
