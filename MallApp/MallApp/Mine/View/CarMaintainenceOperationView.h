//
//  CarMaintainenceOperationView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CarMaintainenceOperationView : UIView
@property (nonatomic , strong) NSMutableArray *btnModelArray;
@property (nonatomic,copy) void(^orderHandleBlock)(NSString *flag);
- (void)setOrderOption:(NSDictionary *)dic;
- (void)setWXTaocanOrderOption:(NSDictionary *)dic;

- (void)setupOperationViewWithDic:(NSDictionary *)dic andType:(OperationType)type;
@end

NS_ASSUME_NONNULL_END
