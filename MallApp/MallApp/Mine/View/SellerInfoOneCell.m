//
//  SellerInfoOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SellerInfoOneCell.h"
@interface SellerInfoOneCell()
@property (nonatomic, strong) UIImageView *bgImage;
@property (nonatomic, strong) UIImageView *headImage;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *idLbl;
@property (nonatomic, strong) UIButton *changeBtn;
@property (nonatomic, strong) UIButton *setBtn;
@end

@implementation SellerInfoOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    [self.contentView addSubview:self.bgImage];
    [self.bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
        make.height.mas_equalTo(140);
    }];
    
    [self.contentView addSubview:self.headImage];
    [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [self.contentView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImage).offset(10);
        make.left.mas_equalTo(self.headImage.mas_right).offset(12);
    }];
    
    [self.contentView addSubview:self.idLbl];
    [self.idLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLbl.mas_bottom).offset(5);
        make.left.mas_equalTo(self.nameLbl);
        make.size.mas_equalTo(CGSizeMake(52, 18));
    }];
    
    
    
    [self.setBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(StatusBarHeight+10);
        make.right.mas_equalTo(-15);
    }];
    
    [self.changeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.setBtn.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(60, 20));
        make.right.mas_equalTo(0);
    }];
    
    [self.contentView layoutIfNeeded];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.changeBtn.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.changeBtn.bounds;
    maskLayer.path = maskPath.CGPath;
    self.changeBtn.layer.mask = maskLayer;
    [self.changeBtn setImage:[UIImage imageNamed:@"switch"] forState:0];
   
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = dic[@"userName"];
        NSInteger isClerk = [dic[@"userLevel"] integerValue];
        if (isClerk == 0) {
            self.idLbl.text = @"普通买家";
            self.idLbl.textColor = APPColor;
            self.changeBtn.hidden = true;
        }else if (isClerk == 4){
            self.idLbl.text = @"专业卖家";
            self.idLbl.textColor = APPColor;
//             self.bgImage.image = [UIImage imageNamed:@"M_headBg"];
             self.changeBtn.hidden = false;
        }else if (isClerk == 1){
            self.idLbl.text = @"普通买家";
            self.idLbl.textColor = APPColor;
            self.changeBtn.hidden = true;
        }else if (isClerk == 3){
            self.idLbl.text = @"普通卖家";
            self.idLbl.textColor = APPColor;
            self.changeBtn.hidden = false;
        }else if (isClerk == 2){
            self.idLbl.text = @"专业买家";
            self.idLbl.textColor = APPColor;
            self.changeBtn.hidden = true;
        }else if (isClerk == 5){
            self.idLbl.text = @"专业卖家";
            self.idLbl.textColor = APPColor;
            
            self.changeBtn.hidden = false;
        }
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"userLogo"]];
        [self.headImage yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"defaultHead"]];
        self.idLbl.backgroundColor = UIColor.whiteColor;
        self.idLbl.layer.cornerRadius = 9;
        self.idLbl.layer.masksToBounds = true;
        
    }
}

- (void)click{
    if (self.clickBlock) {
        self.clickBlock(1);
    }
}

- (void)clickSet{
    if (self.clickBlock) {
        self.clickBlock(2);
    }
}
- (UIImageView *)bgImage{
    if (!_bgImage) {
        _bgImage = [UIImageView new];
        _bgImage.backgroundColor = APPColor;
    }
    
    return _bgImage;
}

- (UIImageView *)headImage{
    if (!_headImage) {
        _headImage = [UIImageView new];
        _headImage.image = [UIImage imageNamed:@"defaultHead"];
        _headImage.layer.cornerRadius = 30;

    }
    
    return _headImage;
}

- (UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [UILabel new];
        _nameLbl.textColor = UIColor.whiteColor;
        _nameLbl.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    }
    
    return _nameLbl;
}

- (UILabel *)idLbl{
    if (!_idLbl) {
        _idLbl = [UILabel new];
        _idLbl.textColor = UIColor.whiteColor;
        _idLbl.font = LabelFont10;
        _idLbl.backgroundColor = UIColor.whiteColor;
        _idLbl.textAlignment = NSTextAlignmentCenter;
    }
    
    return _idLbl;
}

- (UIButton *)changeBtn{
    if (!_changeBtn) {
        _changeBtn = [UIButton new];
        [_changeBtn setTitle:@"切换卖家" forState:0];
        [_changeBtn setTitleColor:UIColor.whiteColor forState:0];
        [_changeBtn.titleLabel setFont:LabelFont11];
        [_changeBtn setBackgroundColor:[UIColor colorWithHexString:@"6C9DFF"]];
        [_changeBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_changeBtn];
    }
    
    return _changeBtn;
}

- (UIButton *)setBtn{
    if (!_setBtn) {
        _setBtn = [UIButton new];
        [_setBtn setImage:[UIImage imageNamed:@"shezhi"] forState:0];
        [_setBtn addTarget:self action:@selector(clickSet) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_setBtn];
    }
    
    return _setBtn;
}

@end
