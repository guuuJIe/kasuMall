//
//  AddCardFootView.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddCardFootView.h"

@implementation AddCardFootView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorEF;
        AddCardFootView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"AddCardFootView" owner:self options:nil] lastObject];
        lastView.backgroundColor = UIColorEF;
        lastView.addAct.backgroundColor = UIColorEF;
        lastView.addAct.userInteractionEnabled = true;
        [lastView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
        [self addSubview:lastView];
//         lastView.addAct.layer.borderWidth = 1;
//         lastView.addAct.layer.borderColor = UIColorB6.CGColor;
//        lastView.addAct.layer.cornerRadius = 3;
        
        [lastView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self);
            make.height.equalTo(@45);
        }];
        lastView.addAct.frame = CGRectMake(0, 0, Screen_Width- 24, 35);
        [self addBorderToLayer:lastView.addAct];
    }
    return self;
}

- (void)click{
    if (self.addBlock) {
        self.addBlock();
    }
    
}

- (void)addBorderToLayer:(UIView *)view
{
    CAShapeLayer *border = [CAShapeLayer layer];
    //  线条颜色
    border.strokeColor = UIColorB6.CGColor;
    
    border.fillColor = nil;
    
    border.path = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
    
    border.frame = view.bounds;
    
    // 不要设太大 不然看不出效果
    border.lineWidth = 1;
    
    border.lineCap = @"square";
    
    //  第一个是 线条长度   第二个是间距    nil时为实线
    border.lineDashPattern = @[@9, @4];
    
    [view.layer addSublayer:border];
}


@end
