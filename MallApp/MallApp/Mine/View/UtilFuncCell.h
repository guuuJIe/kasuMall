//
//  UtilFuncCell.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UtilFuncCell : UITableViewCell
@property (nonatomic,copy) void(^clickBlock)(NSInteger index,NSString *title);
@property (nonatomic,strong) NSArray *menuArr;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
