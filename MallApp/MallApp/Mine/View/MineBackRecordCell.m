//
//  MineBackRecordCell.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineBackRecordCell.h"

@implementation MineBackRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.OrderNum.text = [NSString stringWithFormat:@"订单编号:%@",dic[@"groupOrderNum"]];
        self.totalNum.text = [NSString stringWithFormat:@"%@",dic[@"totalPrice"]];
        self.backNum.text = [NSString stringWithFormat:@"%@",dic[@"stagePrice"]];
        NSString *times = [NSString stringWithFormat:@"%@",dic[@"nextDate"]];
        self.timeLbl.text = [NSString stringWithFormat:@"下个返还日%@",[times substringToIndex:10]];
        self.curTimeLbl.text = [NSString stringWithFormat:@"总%@期 已返回%@期",dic[@"stagesNum"],dic[@"returnNum"]];
        self.perTimeLbl.text = [NSString stringWithFormat:@"返还周期 %@天",dic[@"dateSize"]];
        NSInteger statues = [dic[@"status"] integerValue];
        if (statues == 1) {
            self.statuesLbl.text = @"未开始";
            self.statuesLbl.textColor = APPColor;
            self.timeLbl.hidden = true;
        }else if (statues == 2){
            self.statuesLbl.text = @"生效中";
            self.statuesLbl.textColor = UIColorFF9F;
        }else if (statues == 3){
            self.statuesLbl.text = @"已完成";
            self.statuesLbl.textColor = [UIColor colorWithHexString:@"#00751F"];
             self.timeLbl.hidden = true;
        }else if (statues == 4){
            self.statuesLbl.text = @"未生效";
             self.statuesLbl.textColor = [UIColor colorWithHexString:@"#B2B2B2"];
        }
    }
}
@end
