//
//  AddCardFootView.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddCardFootView : UIView
@property (weak, nonatomic) IBOutlet UIView *addAct;
@property (weak, nonatomic) IBOutlet UILabel *carTyepLbl;
@property (nonatomic, copy) void (^addBlock)(void);
@end

NS_ASSUME_NONNULL_END
