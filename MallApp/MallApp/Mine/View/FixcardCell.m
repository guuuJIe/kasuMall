//
//  FixcardCell.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixcardCell.h"
@interface FixcardCell()
@property (weak, nonatomic) IBOutlet UIImageView *cardBg;


@end

@implementation FixcardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@"暂无标题"];
        self.sellerLbl.text = [NSString stringWithFormat:@"发布商家：%@",dic[@"userShopName"]];
        self.accountLbl.text = [NSString stringWithFormat:@"账号：%@",dic[@"account"]];
        NSInteger isUsed = [dic[@"isUse"] integerValue];
        if (isUsed == 0) {
            self.cardBg.image = [UIImage imageNamed:@"fixcardBg"];
            self.isUsedIcon.hidden = true;
        }else{
            self.cardBg.image = [UIImage imageNamed:@"fixcardUsedBg"];
            self.isUsedIcon.hidden = false;
        }
    }
}

@end
