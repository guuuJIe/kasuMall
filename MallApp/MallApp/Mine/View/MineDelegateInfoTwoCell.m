//
//  MineDelegateInfoTwoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineDelegateInfoTwoCell.h"
@interface MineDelegateInfoTwoCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *genderLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIView *editNameView;

@end

@implementation MineDelegateInfoTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.editNameView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(edit)]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"realName"] andDefailtInfo:@""];
        self.genderLbl.text =  [XJUtil insertStringWithNotNullObject:dic[@"sex"] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contact"] andDefailtInfo:@""];
        self.timeLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"createTime"] andDefailtInfo:@""];
    }
}

- (void)edit{
    if (self.editBlock) {
        self.editBlock();
    }
}

@end
