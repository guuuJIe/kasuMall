//
//  GasCarTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GasCarTableCell.h"

@implementation GasCarTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorEF;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *str = [NSString stringWithFormat:@"%@",dic[@"bank"]];
        if ([str isEqualToString:@"中国石化"]) {
            self.cardBgImage.image = [UIImage imageNamed:@"shihua"];
            self.cardIconImage.image = [UIImage imageNamed:@"shihua_icon"];
        }else if ([str isEqualToString:@"中国石油"]){
            self.cardBgImage.image = [UIImage imageNamed:@"shiyouka"];
            self.cardIconImage.image = [UIImage imageNamed:@"shiyouka_icon"];
        }
        self.nameLbl.text = str;
        self.numLbl.text = [NSString stringWithFormat:@"%@",dic[@"bankNumber"]];

    }
}

- (IBAction)delAct:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}
@end
