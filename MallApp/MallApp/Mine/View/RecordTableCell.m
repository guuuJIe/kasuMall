//
//  RecordTableCell.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "RecordTableCell.h"

@implementation RecordTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = dic[@"flowType"];
        self.timeLbl.text = dic[@"createDate"];
        self.moneyLbl.text = [NSString stringWithFormat:@"%@",dic[@"flowAmount"]];
    }
}

@end
