//
//  MyInfoOneCell.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyInfoOneCell.h"
@interface MyInfoOneCell()
@property (nonatomic, strong) UIButton *setBtn;
@end

@implementation MyInfoOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.cornerView.frame = CGRectMake(0, 0, Screen_Width, 10);
    
    
//    self.cornerView.layer.mask = maskLayer;
//    self.contentView.backgroundColor = APPColor;
 
    [self.setBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(StatusBarHeight + 10);
        make.right.mas_equalTo(-15);
    }];
    
    UIButton *btn = [UIButton new];
    [btn setTitle:@"切换买家" forState:0];
    [btn setTitleColor:UIColor.whiteColor forState:0];
    [btn.titleLabel setFont:LabelFont11];
    [btn setBackgroundColor:[UIColor colorWithHexString:@"FF795B"]];
    [self.contentView addSubview:btn];
    [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(self.setBtn.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(60, 20));
    }];
    [self.contentView layoutIfNeeded];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:btn.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = btn.bounds;
    maskLayer.path = maskPath.CGPath;
    btn.layer.mask = maskLayer;
    self.changebtn = btn;
    [self.changebtn setImage:[UIImage imageNamed:@"switch"] forState:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = dic[@"userName"];
        NSInteger isClerk = [dic[@"userLevel"] integerValue];
        if (isClerk == 0) {
            self.idLbl.text = @"普通买家";
            self.idLbl.textColor = APPColor;
            self.changebtn.hidden = true;
        }else if (isClerk == 4){
            self.idLbl.text = @"专业卖家";
            self.idLbl.textColor = UIColorFF65;
             self.bgImage.image = [UIImage imageNamed:@"M_headBg"];
             self.changebtn.hidden = false;
        }else if (isClerk == 1){
            self.idLbl.text = @"普通买家";
            self.idLbl.textColor = APPColor;
            self.changebtn.hidden = true;
        }else if (isClerk == 3){
            self.idLbl.text = @"普通卖家";
            self.idLbl.textColor = UIColorFF65;
            self.bgImage.image = [UIImage imageNamed:@"M_headBg"];
             self.changebtn.hidden = false;
        }else if (isClerk == 2){
            self.idLbl.text = @"专业买家";
            self.changebtn.hidden = true;
        }
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"userLogo"]];
        [self.userHeadImg yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"defaultHead"]];
        self.idLbl.backgroundColor = UIColor.whiteColor;
        self.idLbl.layer.cornerRadius = 9;
        self.idLbl.layer.masksToBounds = true;
        
    }
}



- (void)click{
    if (self.clickBlock) {
        self.clickBlock(1);
    }
}

- (void)clickSet{
    if (self.clickBlock) {
        self.clickBlock(2);
    }
}

- (UIButton *)setBtn{
    if (!_setBtn) {
        _setBtn = [UIButton new];
        [_setBtn setImage:[UIImage imageNamed:@"shezhi"] forState:0];
        [_setBtn addTarget:self action:@selector(clickSet) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_setBtn];
    }
    
    return _setBtn;
}

@end
