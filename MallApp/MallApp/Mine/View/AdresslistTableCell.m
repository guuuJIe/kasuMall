//
//  AdresslistTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AdresslistTableCell.h"

@implementation AdresslistTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorEF;
    self.bgView.layer.cornerRadius = 8;
    self.selectionStyle = UITableViewCellSeparatorStyleNone;
//    self.defaultBtn.userInteractionEnabled = false;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (!dic) {
        return;
    }
    
    self.nameLbl.text = dic[@"name"];
    self.telLbl.text = dic[@"mobile"];
    self.addressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",dic[@"strProvince"],dic[@"strCity"],dic[@"strArea"],dic[@"street"]];
    NSInteger defaultAddress = [dic[@"tuiJian"] integerValue];
    if (defaultAddress == 1) {
        self.defaultBtn.selected = true;
    }else{
        
    }
}

- (IBAction)deleteAct:(UIButton *)sender {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

- (IBAction)setDefault:(UIButton *)sender {
    if (self.setDefalutBlock) {
        self.setDefalutBlock();
    }
}
@end
