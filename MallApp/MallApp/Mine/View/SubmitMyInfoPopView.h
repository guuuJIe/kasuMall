//
//  SubmitMyInfoPopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <MMPopupView/MMPopupView.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubmitMyInfoPopView : MMPopupView
@property (nonatomic,copy) void(^clickBlock)(NSString *name,NSString *sex);
@end

NS_ASSUME_NONNULL_END
