//
//  MyInfoOneCell.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyInfoOneCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cornerView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *idLbl;
@property (weak, nonatomic) IBOutlet UIImageView *userHeadImg;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (nonatomic, strong) UIButton *changebtn;
- (void)setupData:(NSDictionary *)dic;
@property (nonatomic,copy) void(^clickBlock)(NSInteger type);

@end

NS_ASSUME_NONNULL_END
