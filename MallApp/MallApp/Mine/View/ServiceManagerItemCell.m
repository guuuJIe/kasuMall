//
//  ServiceManagerItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServiceManagerItemCell.h"

@implementation ServiceManagerItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorF5F7;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)bindservice:(id)sender {
    if (self.bindBlock) {
        self.bindBlock();
    }
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"realName"] andDefailtInfo:@"暂无该用户"];
        self.levelLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"级别：%@",dic[@"level"]] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"电话：%@",dic[@"contact"]] andDefailtInfo:@""];
    }
}
@end
