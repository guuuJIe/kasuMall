//
//  BankCardTableViewCell.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BankCardTableViewCell.h"

@implementation BankCardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = UIColorEF;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.bankNameLbl.text = [NSString stringWithFormat:@"%@",dic[@"bank"]];
        self.bankTypeLbl.text = @"";
        NSString *bankNum = [NSString stringWithFormat:@"%@",dic[@"bankNumber"]];
        self.bankNumLbl.text = [NSString stringWithFormat:@"****  ****  ****  %@",[bankNum substringFromIndex:bankNum.length - 4]];
    }
}

- (IBAction)BtnClick:(id)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}
@end
