//
//  CarMaintainenceOperationView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CarMaintainenceOperationView.h"

@implementation CarMaintainenceOperationView
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UIView * line = [UIView new];
        line.backgroundColor = UIColorEF;
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(self);
            make.height.mas_equalTo(lineHeihgt);
        }];
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    self.userInteractionEnabled = YES;
}


- (NSDictionary *)createPurchasingButton:(NSDictionary *)data
{
    NSDictionary *dic;
    
  
    NSInteger type = [data[@"status"] integerValue];
    if (type == 0) {
        dic = @{
            @"title":@"再次购买",
            @"flag":@"purchaseAgain"
        };
        [self.btnModelArray addObject:dic];
     
        
    }
    
   
    
    return dic;
}


/// 保养订单view
/// @param dic2 dic2 description
- (void)setOrderOption:(NSDictionary *)dic2
{
    
    _btnModelArray = [NSMutableArray new];
    NSDictionary *dic;
    dic = [self createPurchasingButton:dic2];
    UIView *lastView;
    
    
    for (UIView *view in self.subviews) {
        if ([view isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < self.btnModelArray.count; i++) {
        NSDictionary *dic = self.btnModelArray[i];
        UIButton *button = [UIButton new];
        
        [button setTitleColor:APPColor forState:UIControlStateNormal];
        button.layer.borderColor = APPColor.CGColor;

        [button setTitle:dic[@"title"] forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont14;
        button.layer.borderWidth = 1;
        button.layer.cornerRadius = 15;
        [button setTitle:dic[@"flag"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(160/2, 60/2));
            make.centerY.equalTo(self);
            if(i == 0){
                make.right.equalTo(self).offset(-12);
            }else{
                make.right.equalTo(lastView.mas_left).offset(-15);
            }
        }];
        lastView = button;
    }
}

- (NSDictionary *)createActButton:(NSDictionary *)data
{
    NSDictionary *dic;
    
   
    NSInteger type = [data[@"status"] integerValue];
    if (type == 2) {
        dic = @{
            @"title":@"去付款",
            @"flag":@"purchaseAgain"
        };
        [self.btnModelArray addObject:dic];
     
        dic = @{
            @"title":@"取消订单",
            @"flag":@"cancelOrder"
        };
        [self.btnModelArray addObject:dic];
    }
    
    if (type == 4) {
//        dic = @{
//            @"title":@"确认收货",
//            @"flag":@"confirmOrder"
//        };
//        [self.btnModelArray addObject:dic];
        
        dic = @{
            @"title":@"查看服务卡",
            @"flag":@"checkServiceCard"
        };
        [self.btnModelArray addObject:dic];
    }
    
    
    if (type == 6) {
        dic = @{
            @"title":@"查看服务卡",
            @"flag":@"checkServiceCard"
        };
        [self.btnModelArray addObject:dic];
        
      
    }
    
   
    
    return dic;
}


/// 维修套餐底部view
/// @param dic dic description
- (void)setWXTaocanOrderOption:(NSDictionary *)dic
{
    
    _btnModelArray = [NSMutableArray new];
    NSDictionary *datadic;
    datadic = [self createActButton:dic];
    UIView *lastView;
    
    for (UIView *view in self.subviews) {
        if ([view isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < self.btnModelArray.count; i++) {
        NSDictionary *dic = self.btnModelArray[i];
        UIButton *button = [UIButton new];
        [button setTitleColor:APPColor forState:UIControlStateNormal];
        button.layer.borderColor = APPColor.CGColor;
        [button setTitle:dic[@"title"] forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont14;
        button.layer.borderWidth = 1;
        button.layer.cornerRadius = 15;
        [button setTitle:dic[@"flag"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(170/2, 60/2));
            make.top.equalTo(self).offset(10);
            if(i == 0){
                make.right.equalTo(self).offset(-12);
            }else{
                make.right.equalTo(lastView.mas_left).offset(-15);
            }
        }];
        lastView = button;
    }
}





- (NSDictionary *)createUserCarMaintaingButton:(NSDictionary *)data
{
    NSDictionary *dic;
    
  
    NSInteger type = [data[@"status"] integerValue];
    //订单状态：0=失效，1=生效，2=审核中，3=审核失败
    if (type == 0) {
        dic = @{
            @"title":@"再次购买",
            @"flag":@"purchaseAgain"
        };
        [self.btnModelArray addObject:dic];

    }
    
    
    if (type == 3) {
        dic = @{
            @"title":@"联系客服",
            @"flag":@"conact"
        };
        [self.btnModelArray addObject:dic];

    }
    
   
    
    return dic;
}

- (NSDictionary *)createSellerCarMaintaingButton:(NSDictionary *)data
{
    NSDictionary *dic;
    
  
    NSInteger type = [data[@"status"] integerValue];
    //订单状态：0=失效，1=生效，2=审核中，3=审核失败
    if (type == 2) {
        dic = @{
            @"title":@"开始维修",
            @"flag":@"startRepair"
        };
        [self.btnModelArray addObject:dic];

    }
    
    
    if (type == 3) {
        dic = @{
            @"title":@"结束维修",
            @"flag":@"achieveRepair"
        };
        [self.btnModelArray addObject:dic];

    }
    
   
    
    return dic;
}

- (NSDictionary *)createRepairOpButton:(NSDictionary *)data
{
    NSDictionary *dic;
    NSInteger type = [data[@"status"] integerValue];
    if (type == 2) {
        dic = @{
            @"title":@"付款",
            @"flag":@"orderPay"
            
        };[self.btnModelArray addObject:dic];
        
    }
    if (type == 21) {
//        dic =
//        @{@"title":@"取消订单",
//          @"flag":@"orderCancel"
//
//        };
//        [self.btnModelArray addObject:dic];
    }
    if (type == 22) {
//        dic = @{
//            @"title":@"联系对方",
//            @"flag":@"connact"
//            
//        };
//        [self.btnModelArray addObject:dic];
        dic = @{
            @"title":@"取消订单",
            @"flag":@"orderCancel"
            
        };
        [self.btnModelArray addObject:dic];
        
    }
    
    return dic;
    
}

- (NSDictionary *)createSellerRepairOrderOpButton:(NSDictionary *)data
{
    NSDictionary *dic;
    NSInteger type = [data[@"status"] integerValue];
   
    if (type == 24) {
        dic = @{@"title":@"确认完成",
                @"flag":@"confirm"
                
        };
        [self.btnModelArray addObject:dic];
        dic = @{@"title":@"确认取消",
                @"flag":@"orderCancel"};
        [self.btnModelArray addObject:dic];
    }
    if (type == 22) {
        dic = @{@"title":@"确认完成",
                @"flag":@"confirm"
                
        };
//        [self.btnModelArray addObject:dic];
//        dic = @{@"title":@"确认取消",
//                @"flag":@"orderCancel"};
        [self.btnModelArray addObject:dic];
        
    }
    
    return dic;
    
}


- (void)setupOperationViewWithDic:(NSDictionary *)dic andType:(OperationType)type{
    
    _btnModelArray = [NSMutableArray new];
    NSDictionary *datadic;
    if (type == SellerCarMaintainceOpe) {
        datadic = [self createSellerCarMaintaingButton:dic];
    }else if (type == UserCarMaintainceOpe){
        datadic = [self createUserCarMaintaingButton:dic];
    }else if (type == UserEmergencyOrderOpe){
        datadic = [self createRepairOpButton:dic];
    }else if (type == SellerEmergencyOrderOpe){
        datadic = [self createSellerRepairOrderOpButton:dic];
    }

    UIView *lastView;
    
    for (UIView *view in self.subviews) {
        if ([view isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < self.btnModelArray.count; i++) {
        NSDictionary *dic = self.btnModelArray[i];
        UIButton *button = [UIButton new];
        [button setTitleColor:APPColor forState:UIControlStateNormal];
        button.layer.borderColor = APPColor.CGColor;
        [button setTitle:dic[@"title"] forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont14;
        button.layer.borderWidth = 1;
        button.layer.cornerRadius = 15;
        [button setTitle:dic[@"flag"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(160/2, 60/2));
            make.top.equalTo(self).offset(10);
            if(i == 0){
                make.right.equalTo(self).offset(-12);
            }else{
                make.right.equalTo(lastView.mas_left).offset(-15);
            }
        }];
        lastView = button;
    }
    
}

-(void)buttonClick:(UIButton *)button{
    self.orderHandleBlock([button titleForState:UIControlStateSelected]);
}

@end
