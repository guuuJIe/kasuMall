//
//  MineDelegateInfoFourCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineDelegateInfoFourCell.h"

@implementation MineDelegateInfoFourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        [filter setDefaults];
        NSString *string = [NSString stringWithFormat:@"%@",dic[@"qrCode"]];
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        [filter setValue:data forKeyPath:@"inputMessage"];
         CIImage *image = [filter outputImage];// 此时的 image 是模糊的
        self.codeImage.image =[XJUtil createNonInterpolatedUIImageFormCIImage:image withSize:120];// 
        
    }
}



@end
