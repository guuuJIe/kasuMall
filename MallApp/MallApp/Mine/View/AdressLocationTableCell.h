//
//  AdressLocationTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/22.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDSearchObj.h"
NS_ASSUME_NONNULL_BEGIN

@interface AdressLocationTableCell : UITableViewCell
-(void)showData:(DDSearchPoi *)poi index:(int)index;
@end

NS_ASSUME_NONNULL_END
