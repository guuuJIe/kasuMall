//
//  MineDelegateInfoThreeCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineDelegateInfoThreeCell.h"
@interface MineDelegateInfoThreeCell()
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *codeLbl;

@end

@implementation MineDelegateInfoThreeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"bindLevelName"] andDefailtInfo:@""];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"brokerage"]];
        self.codeLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"tgCode"] andDefailtInfo:@""];
        
    }
}

@end
