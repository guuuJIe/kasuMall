//
//  GasCarTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GasCarTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (nonatomic,copy) void(^clickBlock)(void);
- (IBAction)delAct:(UIButton *)sender;
- (void)setupData:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UILabel *subLbl;
@property (weak, nonatomic) IBOutlet UIImageView *cardBgImage;
@property (weak, nonatomic) IBOutlet UIImageView *cardIconImage;
@end

NS_ASSUME_NONNULL_END
