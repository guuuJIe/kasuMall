//
//  MallServiceInfoVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MallServiceInfoVC : BaseViewController
@property (nonatomic, assign) NSInteger type;//1商城 3维修
@property (nonatomic, strong) NSDictionary *dic;
@end

NS_ASSUME_NONNULL_END
