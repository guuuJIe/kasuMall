//
//  ShopSetVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopSetVC.h"
#import "MineApi.h"
#import "MallServiceInfoVC.h"
#import "ApplyforShopVC.h"
@interface ShopSetVC ()
@property (weak, nonatomic) IBOutlet UIView *mallView;
@property (weak, nonatomic) IBOutlet UIView *fixView;
@property (weak, nonatomic) IBOutlet UIView *basicInfoView;
@property (strong, nonatomic) MineApi *mineApi;

@end

@implementation ShopSetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"设置";
    [self.mallView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.fixView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.basicInfoView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            MallServiceInfoVC *vc = [[MallServiceInfoVC alloc] initWithNibName:@"MallServiceInfoVC" bundle:nil];
            vc.type = 1;
//            if ([[self.dic allKeys] containsObject:@"productService"]) {
//                vc.dic = self.dic;
//                [self.navigationController pushViewController:vc animated:true];
//            }else{
//                [JMBManager showBriefAlert:@"当前暂无商城信息"];
//            }
            if (![self.dic[@"productService"] isEqual:[NSNull null]]) {
                vc.dic = self.dic;
                [self.navigationController pushViewController:vc animated:true];
            }else{
                [JMBManager showBriefAlert:@"当前暂无商城信息"];
            }
            
        }
            break;
        case 101:
        {
            MallServiceInfoVC *vc = [[MallServiceInfoVC alloc] initWithNibName:@"MallServiceInfoVC" bundle:nil];
            vc.type = 3;
//            if ([[self.dic allKeys] containsObject:@"repairService"]) {
//                vc.dic = self.dic;
//                [self.navigationController pushViewController:vc animated:true];
//            }else{
//                [JMBManager showBriefAlert:@"当前暂无维修信息"];
//            }
            
            if (![self.dic[@"repairService"] isEqual:[NSNull null]]) {
                vc.dic = self.dic;
                [self.navigationController pushViewController:vc animated:true];
            }else{
                [JMBManager showBriefAlert:@"当前暂无商城信息"];
            }
        }
            break;
        case 102:
        {
            ApplyforShopVC *vc = [ApplyforShopVC new];
            vc.totalDic = self.dic;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        default:
            break;
    }
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    
    return _mineApi;
}

@end
