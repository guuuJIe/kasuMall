//
//  MallServiceInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MallServiceInfoVC.h"

@interface MallServiceInfoVC ()
@property (weak, nonatomic) IBOutlet UILabel *connactLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *areaLbl;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet UILabel *introLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end

@implementation MallServiceInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self fillData];
    
    
}

- (void)fillData {
    if (self.type == 1) {
        self.title = @"商城服务";
        self.connactLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"contact"] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"tel"] andDefailtInfo:@""];
        self.areaLbl.text = [NSString stringWithFormat:@"%@%@%@",self.dic[@"productService"][@"province"],self.dic[@"productService"][@"city"],self.dic[@"productService"][@"area"]];
        self.detailLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"productService"][@"address"] andDefailtInfo:@""];
        self.introLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"productService"][@"remark"] andDefailtInfo:@""];
        self.timeLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"productService"][@"openTime"] andDefailtInfo:@""];
    }else if(self.type == 3){
        self.title = @"维修服务";
        self.connactLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"contact"] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"phone"] andDefailtInfo:@""];
        self.areaLbl.text = [NSString stringWithFormat:@"%@%@%@",self.dic[@"repairService"][@"province"],self.dic[@"repairService"][@"city"],self.dic[@"repairService"][@"area"]];
        self.detailLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"repairService"][@"address"] andDefailtInfo:@""];
        self.introLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"repairService"][@"remark"] andDefailtInfo:@""];
        self.timeLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"repairService"][@"openTime"] andDefailtInfo:@""];
    }
   
}

@end
