//
//  PersonInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PersonInfoVC.h"
#import "ChangeInfoVC.h"
#import "MineApi.h"
#import "UIViewController+XHPhoto.h"
@interface PersonInfoVC ()
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,strong) NSDictionary *dic;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *telText;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UILabel *genderLbl;
@property (weak, nonatomic) IBOutlet UIView *uploadView;
@property (weak, nonatomic) IBOutlet UIImageView *userHead;
@end

@implementation PersonInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.uploadView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.nameView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.telLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.emailLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.genderview addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    self.title = @"个人信息";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getUserData];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            ChangeInfoVC *vc = [[ChangeInfoVC alloc] initWithNibName:@"ChangeInfoVC" bundle:nil];
            vc.tag = 100;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 101:
        {
            ChangeInfoVC *vc = [[ChangeInfoVC alloc] initWithNibName:@"ChangeInfoVC" bundle:nil];
            vc.tag = 101;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 102:
        {
            ChangeInfoVC *vc = [[ChangeInfoVC alloc] initWithNibName:@"ChangeInfoVC" bundle:nil];
            vc.tag = 102;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 103:
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            
            //添加常规类型按钮
            [alert addAction:[UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSLog(@"男");
                 
                [self.api memberInfoEditWithparameters:@{@"sex":@"男"} withCompletionHandler:^(NSError *error, MessageBody *result) {
                    self.genderLbl.text = @"男";
                }];
            }]];
            //添加常规类型按钮
            [alert addAction:[UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSLog(@"女");
                
                [self.api memberInfoEditWithparameters:@{@"sex":@"女"} withCompletionHandler:^(NSError *error, MessageBody *result) {
                    self.genderLbl.text = @"女";
                }];
            }]];
            //添加销毁类型按钮
            [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                NSLog(@"点击警告");
            }]];
            
            
            
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case 104:
        {
            
            [self showCanEdit:true photo:^(UIImage *photo) {
                self.userHead.image = photo;
                NSArray *arr =@[@{@"name":@"files",@"image":photo}];
                [JMBManager showLoading];
                [self.api uploadPicWithparameters:@"10" VisitImagesArr:arr withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        NSArray *datas = result.result;
                        [self.api memberInfoEditWithparameters:@{@"UserLogo":datas.firstObject} withCompletionHandler:^(NSError *error, MessageBody *result) {
                            if (result.code == 200) {
                                
                            }else{
                                [JMBManager showBriefAlert:@"上传失败"];
                            }
                             [JMBManager hideAlert];
                        }];
//                        self.imagesArr = result.result;
//                        NSLog(@"%@",result.result);
                    }
                   
                }];
            }];
        
        }
            break;
        default:
            break;
    }
}


- (void)getUserData{
    [self.api getUserInfoWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *arr = result.result;
            self.dic = arr.firstObject;
            [self configData:self.dic];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    self.nameText.text = [XJUtil insertStringWithNotNullObject:dic[@"realName"] andDefailtInfo:@""];
    self.genderLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"sex"] andDefailtInfo:@""];
    self.telText.text = [XJUtil insertStringWithNotNullObject:dic[@"contact"] andDefailtInfo:@""];
    self.emailText.text = [XJUtil insertStringWithNotNullObject:dic[@"email"] andDefailtInfo:@""];
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"userLogo"]];
             [self.userHead yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"defaultHead"]];
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    
    return _api;
}

@end
