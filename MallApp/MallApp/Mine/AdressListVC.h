//
//  AdressListVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AdressListVC : BaseViewController
@property (nonatomic,copy) void(^setAdressBlock)(NSDictionary *dic);
@end

NS_ASSUME_NONNULL_END
