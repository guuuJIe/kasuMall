//
//  MineCardPackageVC.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineCardPackageVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *bankAct;

@property (weak, nonatomic) IBOutlet UIView *gasAct;
@end

NS_ASSUME_NONNULL_END
