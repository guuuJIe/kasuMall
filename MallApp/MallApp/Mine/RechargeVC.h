//
//  RechargeVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RechargeVC : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *moneyNumText;
- (IBAction)SubmitAct:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
