//
//  BindMyServiceManagerVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BindMyServiceManagerVC.h"
#import "ServiceManagerItemCell.h"
#import "MineApi.h"
#import "MineSettingVC.h"
@interface BindMyServiceManagerVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *contentText;
@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,strong) NSArray *dataArray;
@end

@implementation BindMyServiceManagerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.title = @"绑定服务经理";
    [self.listTableView registerNib:[UINib nibWithNibName:@"ServiceManagerItemCell" bundle:nil] forCellReuseIdentifier:@"ServiceManagerItemCell"];
    self.listTableView.backgroundColor = UIColorF5F7;
}
- (IBAction)search:(id)sender {
    
    [self.api searchServiceManagerWithparameters:self.contentText.text withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArray = result.result;
            [self.listTableView reloadData];
        }
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"ServiceManagerItemCell";
    ServiceManagerItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[ServiceManagerItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArray[indexPath.row]];
    cell.bindBlock = ^{
        NSDictionary *dic = self.dataArray[indexPath.row];
        [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定绑定该用户？" andHandler:^(NSInteger index) {
            if (index == 1) {
                [self.api bindServiceManagerWithparameters:[NSString stringWithFormat:@"%@",dic[@"code"]] withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        
//                        [XJUtil popToVC:@"MineSettingVC" inViewControllers:self.navigationController.viewControllers];
//                        [self.navigationController popToRootViewControllerAnimated:true];
//                        [self popCurVC];
                        
//                        [XJUtil popToVC:@"MineServiceManagerVC" inViewControllers:self.navigationController.viewControllers InCurNav:self.navigationController];
                        [JMBManager showBriefAlert:@"绑定成功"];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.navigationController popToRootViewControllerAnimated:true];
                        });
                       
                    }
                }];
            }
        }];
    };
    return cell;
}

- (void)popCurVC{
    for (UIViewController *temp in self.navigationController.viewControllers) {
        if ([temp isKindOfClass:[MineSettingVC class]]) {
           [self.navigationController popToViewController:temp animated:YES];
        }
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
