//
//  BankCardVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineGasCardVC.h"
#import "GasCarTableCell.h"
#import "AddCardFootView.h"
#import "AddMineCardVC.h"
#import "MineApi.h"
#import "AddGasCardVC.h"
@interface MineGasCardVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *listTableView;

@property (nonatomic) MineApi *api;
@property (nonatomic) NSMutableArray *dataArray;
@end

@implementation MineGasCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"加油卡";
    [self setupUI];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getBankListData];
}
- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"GasCarTableCell";
    GasCarTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[GasCarTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    cell.clickBlock = ^{
        NSDictionary *dic = self.dataArray[indexPath.row];
        [self.api delMyCardWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                [self getBankListData];
            }
        }];
    };
    [cell setupData:self.dataArray[indexPath.row]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    AddCardFootView *FootView = [[AddCardFootView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 24, 45)];
    FootView.addBlock = ^{
        AddGasCardVC *vc = [[AddGasCardVC alloc] initWithNibName:@"AddGasCardVC" bundle:nil];
        [self.navigationController pushViewController:vc animated:true];
    };
    FootView.carTyepLbl.text = @"添加加油卡";
//    FootView.backgroundColor = [UIColor whiteColor];
    return FootView;
}

- (void)getBankListData{
    [self.api getBankListWithparameters:@"2" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArray = result.result;
            [self.listTableView reloadData];
        }
    }];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;

        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
     
        [_listTableView registerNib:[UINib nibWithNibName:@"GasCarTableCell" bundle:nil] forCellReuseIdentifier:@"GasCarTableCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorEF;

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
