//
//  ChangeInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ChangeInfoVC.h"
#import "MineApi.h"
@interface ChangeInfoVC ()
- (IBAction)submit:(id)sender;
@property (nonatomic,strong) MineApi *api;
@property (weak, nonatomic) IBOutlet UITextField *content;
@end

@implementation ChangeInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"修改信息";
}


- (IBAction)submit:(id)sender {
    if (self.tag == 101) {
        [self.api memberInfoEditWithparameters:@{@"contact":self.content.text} withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                [self.navigationController popViewControllerAnimated:true];
            }
        }];
    }else if (self.tag == 102){
        [self.api memberInfoEditWithparameters:@{@"email":self.content.text} withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [self.navigationController popViewControllerAnimated:true];
                        }
                    }];
    }else if (self.tag == 100){
        [self.api memberInfoEditWithparameters:@{@"realName":self.content.text} withCompletionHandler:^(NSError *error, MessageBody *result) {
                 if (result.code == 200) {
                     [self.navigationController popViewControllerAnimated:true];
                 }
             }];
    }else{
        [self.api clerkInfoEditWithparameters:@{@"RealName":self.content.text,@"UserId":@(self.tag)} withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                if (self.refreshBlock) {
                    self.refreshBlock();
                }
                [self.navigationController popViewControllerAnimated:true];
            }
        }];
    }
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}
@end
