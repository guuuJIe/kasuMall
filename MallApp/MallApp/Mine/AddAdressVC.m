//
//  AddAdressVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddAdressVC.h"
#import "DQAreasView.h"
#import "DQAreasModel.h"
#import "MineApi.h"
#import "AddressLocationVC.h"
@interface AddAdressVC ()<DQAreasViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *telText;
@property (weak, nonatomic) IBOutlet UILabel *areaText;
@property (weak, nonatomic) IBOutlet UITextField *detailText;
@property (nonatomic, strong) DQAreasView *areasView;//所在地
@property (nonatomic, strong) NSString *strProvince;
@property (nonatomic, strong) NSString *strCity;
@property (nonatomic, strong) NSString *strArea;
@property (nonatomic, assign) BOOL isDefault;
@property (nonatomic, strong) MineApi *mineApi;
@end

@implementation AddAdressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"新增地址";
    self.view.backgroundColor = [UIColor whiteColor];
    self.areaText.userInteractionEnabled = true;
    [self.areaText addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
    self.areasView = [DQAreasView new];
    self.areasView.delegate = self;
    self.isDefault = true;
}
- (IBAction)setDefaultAddress:(UISwitch *)sender {
    
    self.isDefault = sender.isOn;
    if (self.isDefault) {
        NSLog(@"11");
        self.isDefault = sender.isOn;
        NSLog(@"22");
        self.isDefault = sender.isOn;
    }
}

- (IBAction)saveAdress:(id)sender {
//    NSMutableDictionary *dic = [NSMutableDictionary new];
//    [dic setValue:self.nameText.text forKey:@"name"];
//    [dic setValue:self.telText.text forKey:@"mobile"];
//    [dic setValue:self.strProvince forKey:@"strProvince"];
//    [dic setValue:self.strCity forKey:@"strCity"];
//    [dic setValue:self.strArea forKey:@"strArea"];
//    [dic setValue:self.detailText forKey:@"street"];
//    [dic setValue:@(self.isDefault) forKey:@"tuiJian"];
//    [dic setValue:userId forKey:@"userId"];
    if (self.strArea.length < 3) {
        self.strArea = [NSString stringWithFormat:@"%@区",self.strArea];
    }
    NSDictionary *dic = @{
        @"name":self.nameText.text,
        @"mobile":self.telText.text,
        @"strProvince":[NSString stringWithFormat:@"%@省",self.strProvince],
        @"strCity":[NSString stringWithFormat:@"%@市",self.strCity],
        @"strArea":[NSString stringWithFormat:@"%@",self.strArea],
        @"userId":usersID,
        @"street":self.detailText.text,
        @"tuiJian":@(self.isDefault),
        @"cityId":@(0),
        @"areaId":@0,
        @"provinceId":@0,
        @"cityId":@0,
        @"zipcode": @"",
    };
    WeakSelf(self)
    [self.mineApi addAdressWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [weakself.navigationController popViewControllerAnimated:true];
        }
    }];
}


- (void)click{
    [self.view endEditing:true];
    [self.areasView startAnimationFunction];
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi  new];
    }
    return _mineApi;
}

- (void)clickAreasViewEnsureBtnActionAreasDate:(DQAreasModel *)model{
    NSLog(@"%@",model.city);
    self.areaText.text = [NSString stringWithFormat:@"%@ %@ %@",model.Province,model.city,model.county];
    self.strProvince = model.Province;
    self.strCity = model.city;
    self.strArea = model.county;

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
