//
//  AddressLocationVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/22.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"
#import "DDSearchManager.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^LocationBlock)(DDSearchPoi *poi);
@interface AddressLocationVC : BaseViewController
@property (nonatomic,copy) LocationBlock block;
@end

NS_ASSUME_NONNULL_END
