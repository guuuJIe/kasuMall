//
//  MineCardPackageVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineCardPackageVC.h"
#import "BankCardVC.h"
#import "MineGasCardVC.h"
#import "MineFixcardViewController.h"
#import "MineServicesCardVC.h"
@interface MineCardPackageVC ()
@property (weak, nonatomic) IBOutlet UIView *fixCardView;
@property (weak, nonatomic) IBOutlet UIView *myserviceView;

@end

@implementation MineCardPackageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的卡包";
    [self.bankAct addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.gasAct addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.fixCardView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.myserviceView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

- (void)click:(UITapGestureRecognizer *)ges{
    NSInteger tag = ges.view.tag;
    if (tag == 200) {
        BankCardVC *vc = [BankCardVC new];
        [self.navigationController pushViewController:vc animated:true];
    }else if (tag == 201){
        MineGasCardVC *vc = [MineGasCardVC new];
        [self.navigationController pushViewController:vc animated:true];
    }else if (tag == 202){
        MineFixcardViewController *vc = [MineFixcardViewController new];
        [self.navigationController pushViewController:vc animated:true];
    }else if (tag == 203){
        MineServicesCardVC *vc = [MineServicesCardVC new];
        [self.navigationController pushViewController:vc animated:true];
    }//
}

@end
