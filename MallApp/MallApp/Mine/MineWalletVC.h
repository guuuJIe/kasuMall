//
//  MineWalletVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineWalletVC : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIView *rechargeLbl;
@property (weak, nonatomic) IBOutlet UIView *submitView;
@property (weak, nonatomic) IBOutlet UIView *rechargeRecordView;
@property (weak, nonatomic) IBOutlet UIView *backRecordView;
@end

NS_ASSUME_NONNULL_END
