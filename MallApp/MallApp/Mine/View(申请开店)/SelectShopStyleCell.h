//
//  SelectShopStyleCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectShopStyleCell : UITableViewCell
@property (nonatomic,copy) void(^refreshBlock)(NSInteger pType,NSInteger rType);

@end

NS_ASSUME_NONNULL_END
