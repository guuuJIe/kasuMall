//
//  ShopBasicInfoTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopBasicInfoTableCell.h"
@interface ShopBasicInfoTableCell()
@property (nonatomic, strong) UITextField *nameText;
@property (nonatomic, strong) UITextField *connactText;
@property (nonatomic, strong) UITextField *telText;
@property (nonatomic, strong) UITextField *emailText;
@property (nonatomic, strong) UITextField *companyNameText;
@property (nonatomic, strong) UITextField *shopnameText;
@property (nonatomic, strong) UITextField *areaText;
@property (nonatomic, strong) UITextField *stateText;
@end

@implementation ShopBasicInfoTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postData) name:@"postShopData" object:nil];
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UILabel *title1 = [UILabel new];
    title1.text = @"*联系人";
    title1.textColor = UIColor333;
    title1.font = LabelFont14;
    [self.contentView addSubview:title1];
    [title1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(12);
    }];
    title1.attributedText = [self buildStringWithText:title1.text];
    
    [self.contentView addSubview:self.nameText];
    [self.nameText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title1);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title1.mas_bottom).offset(12);
//        make.bottom.mas_equalTo(self.contentView);
    }];
    
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"*联系号码";
    title2.textColor = UIColor333;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(12);
    }];
    title2.attributedText = [self buildStringWithText:title2.text];
    
    [self.contentView addSubview:self.connactText];
    [self.connactText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title2);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorEF;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title2.mas_bottom).offset(12);
        
    }];
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"*电话号码";
    title3.textColor = UIColor333;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line2.mas_bottom).offset(12);
    }];
    title3.attributedText = [self buildStringWithText:title3.text];
    
    [self.contentView addSubview:self.telText];
    [self.telText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title3);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = UIColorEF;
    [self.contentView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title3.mas_bottom).offset(12);
    }];
    
    
    
    UILabel *title4 = [UILabel new];
    title4.text = @" 邮箱";
    title4.textColor = UIColor333;
    title4.font = LabelFont14;
    [self.contentView addSubview:title4];
    [title4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line3.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.emailText];
    [self.emailText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title4);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    
    UIView *line4 = [UIView new];
    line4.backgroundColor = UIColorEF;
    [self.contentView addSubview:line4];
    [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title4.mas_bottom).offset(12);
        
    }];
       
    
    UILabel *title5 = [UILabel new];
    title5.text = @"*公司名称";
    title5.textColor = UIColor333;
    title5.font = LabelFont14;
    [self.contentView addSubview:title5];
    [title5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line4.mas_bottom).offset(12);
    }];
    title5.attributedText = [self buildStringWithText:title5.text];
    
    [self.contentView addSubview:self.companyNameText];
    [self.companyNameText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title5);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    
    UIView *line5 = [UIView new];
    line5.backgroundColor = UIColorEF;
    [self.contentView addSubview:line5];
    [line5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title5.mas_bottom).offset(12);
       
    }];
    
    
    UILabel *title6 = [UILabel new];
    title6.text = @"*店铺名称";
    title6.textColor = UIColor333;
    title6.font = LabelFont14;
    [self.contentView addSubview:title6];
    [title6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line5.mas_bottom).offset(12);
    }];
     title6.attributedText = [self buildStringWithText:title6.text];
    
    [self.contentView addSubview:self.shopnameText];
    [self.shopnameText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title6);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    
    UIView *line6 = [UIView new];
    line6.backgroundColor = UIColorEF;
    [self.contentView addSubview:line6];
    [line6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title6.mas_bottom).offset(12);
        
    }];
    
    
//    UILabel *title7 = [UILabel new];
//    title7.text = @"省市区";
//    title7.textColor = UIColor333;
//    title7.font = LabelFont14;
//    [self.contentView addSubview:title7];
//    [title7 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(12);
//        make.top.mas_equalTo(line6.mas_bottom).offset(12);
//    }];
//
//    [self.contentView addSubview:self.areaText];
//    [self.areaText mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(title7);
//        make.left.mas_equalTo(84);
//        make.right.mas_equalTo(-10);
//    }];
//
//    UIView *line7 = [UIView new];
//    line7.backgroundColor = UIColorEF;
//    [self.contentView addSubview:line7];
//    [line7 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.mas_equalTo(self.contentView);
//        make.height.mas_equalTo(lineHeihgt);
//        make.top.mas_equalTo(title7.mas_bottom).offset(12);
//
//    }];
    
    
    
    UILabel *title8 = [UILabel new];
    title8.text = @" 店铺描述";
    title8.textColor = UIColor333;
    title8.font = LabelFont14;
    [self.contentView addSubview:title8];
    [title8 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line6.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.stateText];
    [self.stateText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title8);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    
    UIView *line8 = [UIView new];
    line8.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line8];
    [line8 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
        make.top.mas_equalTo(title8.mas_bottom).offset(12);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    
}

- (void)postData{
    
    if (self.nameText.text.length == 0 || self.connactText.text.length == 0 || self.telText.text.length == 0 || self.companyNameText.text.length == 0 || self.shopnameText.text.length == 0) {
        [JMBManager showBriefAlert:@"请检查必填信息是否已填写"];
        return;
    }
    
    if (self.shopnameText.text.length<4) {
        [JMBManager showBriefAlert:@"店铺名称长度必须在4——100个字之间"];
        return;
    }
    
    if (self.companyNameText.text.length<4) {
        [JMBManager showBriefAlert:@"公司名称在4~50个字之间"];
        return;
    }
    
    NSDictionary *dic = @{@"shopType":@"企业",@"userId":usersID,
                          @"Contact":[XJUtil insertStringWithNotNullObject:self.nameText.text andDefailtInfo:@""],
                          @"Phone":[XJUtil insertStringWithNotNullObject:self.connactText.text andDefailtInfo:@""],
                          @"Tel":[XJUtil insertStringWithNotNullObject:self.telText.text andDefailtInfo:@""],
                          @"Email":[XJUtil insertStringWithNotNullObject:self.emailText.text andDefailtInfo:@""],
                          @"CompanyName":[XJUtil insertStringWithNotNullObject:self.companyNameText.text andDefailtInfo:@""],
                          @"ShopName":[XJUtil insertStringWithNotNullObject:self.shopnameText.text andDefailtInfo:@""],
                          @"ShopMs":[XJUtil insertStringWithNotNullObject:self.stateText.text andDefailtInfo:@""],
                          
    };
    JLog(@"信息接受了");
    if (self.clickBlock) {
        self.clickBlock(dic);
    }
    
}


- (NSMutableAttributedString *)buildStringWithText:(NSString *)text{
    NSMutableAttributedString *abs = [[NSMutableAttributedString alloc] initWithString:text];
    [abs addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 1)];
    
    return abs;
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameText.text = dic[@"contact"];
        self.connactText.text = dic[@"phone"];
        self.telText.text = dic[@"tel"];
        self.emailText.text = dic[@"email"];
        self.companyNameText.text = dic[@"companyName"];
        self.shopnameText.text = dic[@"shopName"];
        self.stateText.text = dic[@"shopMs"];
        self.nameText.userInteractionEnabled = false;
        self.connactText.userInteractionEnabled = false;
        self.telText.userInteractionEnabled = false;
        self.emailText.userInteractionEnabled = false;
        self.companyNameText.userInteractionEnabled = false;
        self.shopnameText.userInteractionEnabled = false;
        self.stateText.userInteractionEnabled = false;
    }
}


- (UITextField *)nameText{
    if (!_nameText) {
        _nameText = [UITextField new];
        _nameText.placeholder = @"请输入联系人姓名(必填)";
        _nameText.textColor = UIColor333;
        _nameText.font = LabelFont14;
#if DEBUG
        _nameText.text = @"张三";
#endif
        [self.contentView addSubview:_nameText];
    }
    
    return _nameText;
}

- (UITextField *)connactText{
    if (!_connactText) {
        _connactText = [UITextField new];
        _connactText.placeholder = @"请输入联系人手机号码(必填)";
        _connactText.textColor = UIColor333;
        _connactText.font = LabelFont14;
        _connactText.keyboardType = UIKeyboardTypePhonePad;
        #if DEBUG
                _connactText.text = @"13577889945";
        #endif
        [self.contentView addSubview:_connactText];
    }
    
    return _connactText;
}

- (UITextField *)telText{
    if (!_telText) {
        _telText = [UITextField new];
        _telText.placeholder = @"请输入电话号码(必填)";
        _telText.textColor = UIColor333;
        _telText.font = LabelFont14;
        _telText.keyboardType = UIKeyboardTypePhonePad;
        #if DEBUG
                _telText.text = @"13777889945";
        #endif
        [self.contentView addSubview:_telText];
    }
    
    return _telText;
}

- (UITextField *)emailText{
    if (!_emailText) {
        _emailText = [UITextField new];
        _emailText.placeholder = @"请输入邮箱";
        _emailText.textColor = UIColor333;
        _emailText.font = LabelFont14;
        _emailText.keyboardType = UIKeyboardTypeEmailAddress;
        #if DEBUG
                _emailText.text = @"141632222@qq.com";
        #endif
        [self.contentView addSubview:_emailText];
    }
    
    return _emailText;
}

- (UITextField *)companyNameText{
    if (!_companyNameText) {
        _companyNameText = [UITextField new];
        _companyNameText.placeholder = @"请输入公司名称(必填)";
        _companyNameText.textColor = UIColor333;
        _companyNameText.font = LabelFont14;
#if DEBUG
        _companyNameText.text = @"华盛汽修";
#endif
        [self.contentView addSubview:_companyNameText];
    }
    
    return _companyNameText;
}

- (UITextField *)shopnameText{
    if (!_shopnameText) {
        _shopnameText = [UITextField new];
        _shopnameText.placeholder = @"请输入店铺名称(必填)";
        _shopnameText.textColor = UIColor333;
        _shopnameText.font = LabelFont14;
        #if DEBUG
                _shopnameText.text = @"华盛汽修2";
        #endif
        [self.contentView addSubview:_shopnameText];
    }
    
    return _shopnameText;
}

- (UITextField *)areaText{
    if (!_areaText) {
        _areaText = [UITextField new];
        _areaText.placeholder = @"请选择省市区";
        _areaText.textColor = UIColor333;
        _areaText.font = LabelFont14;
#if DEBUG
        _shopnameText.text = @"华盛汽修2";
#endif
        [self.contentView addSubview:_areaText];
    }
    
    return _areaText;
}

- (UITextField *)stateText{
    if (!_stateText) {
        _stateText = [UITextField new];
        _stateText.placeholder = @"请输入相关介绍";
        _stateText.textColor = UIColor333;
        _stateText.font = LabelFont14;
        #if DEBUG
                _stateText.text = @"相关介绍相关介绍相关介绍相关介绍相关介绍相关介绍相关介绍";
        #endif
        [self.contentView addSubview:_stateText];
    }
    
    return _stateText;
}
@end
