//
//  ApplyShopSatuesVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplyShopSatuesVC.h"
#import "ApplyforShopVC.h"
@interface ApplyShopSatuesVC ()
@property (nonatomic, strong) UIImageView *stautImage;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *subTitleLbl;
@property (nonatomic, strong) UIButton *applyBtn;
@end

@implementation ApplyShopSatuesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"审核状态";
    [self setupUI];
    
    [self setupData];
}

- (void)setupUI{
    
    [self.view addSubview:self.stautImage];
    [self.stautImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
//        make.size.mas_equalTo(CGSizeMake(68, 68));
        make.top.mas_equalTo(46);
    }];
    
    [self.view addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.stautImage.mas_bottom).offset(27);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.subTitleLbl];
    [self.subTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLbl.mas_bottom).offset(8);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.applyBtn];
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-20-BottomAreaHeight);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(44);
    }];
    
}

- (void)setupData{
    NSInteger statues = [self.dic[@"sellPassed"] integerValue];
    if (statues == 1) {
        self.titleLbl.text = @"申请中";
        self.subTitleLbl.text = @"资料已提交，等待客服审核";
        [self.applyBtn setBackgroundColor:[UIColor colorWithHexString:@"C7D9FB"]];
        [self.applyBtn setTitle:@"审核中" forState:0];
        self.stautImage.image = [UIImage imageNamed:@"M_applyshop_0"];
    }else if (statues == 2){
        self.titleLbl.text = @"审核通过";
        self.subTitleLbl.text = @"恭喜！您提交的审核已通过";
        [self.applyBtn setTitle:@"返回首页" forState:0];
        self.stautImage.image = [UIImage imageNamed:@"M_applyshop_2"];
    }else if (statues == 3){
        self.titleLbl.text = @"审核未通过";
        self.subTitleLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"verifySm"] andDefailtInfo:@""];
         [self.applyBtn setTitle:@"再次申请" forState:0];
        self.stautImage.image = [UIImage imageNamed:@"M_applyshop_1"];
    }
}

- (void)clickAct{
    if ([self.applyBtn.currentTitle isEqualToString:@"审核中"] || [self.applyBtn.currentTitle isEqualToString:@"返回首页"]) {
        [self.navigationController popViewControllerAnimated:true];
    }else if ([self.applyBtn.currentTitle isEqualToString:@"再次申请"]){
        ApplyforShopVC *vc = [ApplyforShopVC new];
        [self.navigationController pushViewController:vc animated:true];
    }
}



- (UIImageView *)stautImage{
    if (!_stautImage) {
        _stautImage = [UIImageView new];
//        _stautImage.backgroundColor = APPColor;
    }
    
    return _stautImage;
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.text = @"申请中";
        _titleLbl.textColor = UIColor333;
        _titleLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
    }
    
    return _titleLbl;
}

- (UILabel *)subTitleLbl{
    if (!_subTitleLbl) {
        _subTitleLbl = [UILabel new];
        _subTitleLbl.text = @"申请中";
        _subTitleLbl.textColor = UIColor90;
        _subTitleLbl.font = LabelFont14;
    }
    
    return _subTitleLbl;
}


- (UIButton *)applyBtn{
    if (!_applyBtn) {
        _applyBtn = [UIButton new];
        [_applyBtn setTitle:@"取消申请" forState:0];
        [_applyBtn setTitleColor:UIColor.whiteColor forState:0];
        [_applyBtn.titleLabel setFont:LabelFont16];
        _applyBtn.layer.cornerRadius = 4;
        [_applyBtn setBackgroundColor:APPColor];
        [_applyBtn addTarget:self action:@selector(clickAct) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _applyBtn;
}
@end
