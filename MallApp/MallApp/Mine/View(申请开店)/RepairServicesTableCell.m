//
//  RepairServicesTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/14.
//  Copyright © 2020 Mac. All rights reserved.
//


#import "RepairServicesTableCell.h"
#import "DQAreasView.h"
#import "DQAreasModel.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "MutipleTypePopView.h"
#import "TypeModel.h"
@interface RepairServicesTableCell()<DQAreasViewDelegate,AMapSearchDelegate,UITextFieldDelegate>
@property (nonatomic, strong) UITextField *serviceTypeText;
@property (nonatomic, strong) UITextField *nameText;
@property (nonatomic, strong) UITextField *detailText;
@property (nonatomic, strong) UITextField *serviceIntroText;
@property (nonatomic, strong) UITextField *shopTimeText;
@property (nonatomic, strong) UITextField *areaText;
@property (nonatomic, strong) DQAreasView *areasView;//所在地
@property (nonatomic, strong) DQAreasModel *daModel;
@property (nonatomic, strong) AMapSearchAPI *searchApi;
@property (nonatomic, strong) AMapGeoPoint *mapGeo;
@property (nonatomic, strong) MutipleTypePopView *popView;
@property (nonatomic, strong) NSMutableArray *datasArr;
@end

@implementation RepairServicesTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reviceRepairInfo) name:@"postRepairInfo" object:nil];
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    UIView *titleView = [UIView new];
    [self.contentView addSubview:titleView];
    [titleView setBackgroundColor:bgColor];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(Screen_Width);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
    }];
    
    UILabel *title1 = [UILabel new];
    title1.text = @"维修服务";
    title1.textColor = UIColorB6;
    title1.font = LabelFont14;
    
    title1.textAlignment = NSTextAlignmentLeft;
    [titleView addSubview:title1];
    [title1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(0);
       
    }];
    UILabel *title = [UILabel new];
    title.text = @"服务类型";
    title.textColor = UIColor333;
    title.font = LabelFont14;
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(titleView.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.serviceTypeText];
    [self.serviceTypeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    UIButton *btn2 = [UIButton new];
    [btn2 setImage:[UIImage imageNamed:@"more"] forState:0];
    [self.contentView addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.serviceTypeText);
        make.right.mas_equalTo(-12);
    }];
    
    UIButton *btn3 = [UIButton new];
    [self.contentView addSubview:btn3];
    [btn3 addTarget:self action:@selector(selServiceType) forControlEvents:UIControlEventTouchUpInside];
    [btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.serviceTypeText);
        make.height.mas_equalTo(22);
    }];
    UIView *sline = [UIView new];
    sline.backgroundColor = UIColorEF;
    [self.contentView addSubview:sline];
    [sline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title.mas_bottom).offset(12);
    }];
   
    
    UILabel *title2 = [UILabel new];
    title2.text = @"联系号码";
    title2.textColor = UIColor333;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(sline.mas_bottom).offset(12);
    }];
    [self.contentView addSubview:self.nameText];
    [self.nameText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title2);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title2.mas_bottom).offset(12);
    }];
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"省市区";
    title3.textColor = UIColor333;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(12);
    }];
    [self.contentView addSubview:self.areaText];
    [self.areaText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title3);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-32);
    }];
//
    UIButton *btn = [UIButton new];
    [btn setImage:[UIImage imageNamed:@"more"] forState:0];
    [self.contentView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.areaText);
        make.right.mas_equalTo(-12);
    }];
    
    UIButton *btn1 = [UIButton new];
    [self.contentView addSubview:btn1];
    [btn1 addTarget:self action:@selector(selAdress) forControlEvents:UIControlEventTouchUpInside];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.areaText);
        make.height.mas_equalTo(22);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorEF;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title3.mas_bottom).offset(12);
//        make.bottom.mas_equalTo(self.contentView).offset(-12);
    }];
    
    
    UILabel *title4 = [UILabel new];
    title4.text = @"详细地址";
    title4.textColor = UIColor333;
    title4.font = LabelFont14;
    [self.contentView addSubview:title4];
    [title4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line2.mas_bottom).offset(12);
    }];
    [self.contentView addSubview:self.detailText];
    [self.detailText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title4);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    UIButton *adbtn = [UIButton new];
    [self.contentView addSubview:adbtn];
    [adbtn addTarget:self action:@selector(clickAct) forControlEvents:UIControlEventTouchUpInside];
    [adbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.detailText);
    }];
    UIView *line3 = [UIView new];
    line3.backgroundColor = UIColorEF;
    [self.contentView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title4.mas_bottom).offset(12);
        //        make.bottom.mas_equalTo(self.contentView).offset(-12);
    }];
    
    
    UILabel *title5 = [UILabel new];
    title5.text = @"服务介绍";
    title5.textColor = UIColor333;
    title5.font = LabelFont14;
    [self.contentView addSubview:title5];
    [title5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line3.mas_bottom).offset(12);
    }];
    [self.contentView addSubview:self.serviceIntroText];
    [self.serviceIntroText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title5);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
    }];
    UIView *line4 = [UIView new];
    line4.backgroundColor = UIColorEF;
    [self.contentView addSubview:line4];
    [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(title5.mas_bottom).offset(12);
        //        make.bottom.mas_equalTo(self.contentView).offset(-12);
    }];
    
    UILabel *title6 = [UILabel new];
    title6.text = @"营业时间";
    title6.textColor = UIColor333;
    title6.font = LabelFont14;
    [self.contentView addSubview:title6];
    [title6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line4.mas_bottom).offset(12);
    }];
    [self.contentView addSubview:self.shopTimeText];
    [self.shopTimeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title6);
        make.left.mas_equalTo(84);
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(-12);
    }];
    
}

- (void)reviceRepairInfo{
    if (!self.mapGeo) {
        [JMBManager showBriefAlert:@"请输入详细地址"];
        return;
    }
    if (!self.daModel) {
        [JMBManager showBriefAlert:@"请选择省市区"];
        return;
    }
    if (self.nameText.text.length == 0) {
        [JMBManager showBriefAlert:@"请输入联系号码"];
        return;
    }
    NSNumber *longtitudeNumber = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",self.mapGeo.longitude]];
    bool bool_true = true;
    NSNumber *latitudeNumber = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",self.mapGeo.latitude]];
    NSDictionary *dic = @{@"IsService":@(bool_true),@"Type":@3,@"sType":[XJUtil insertStringWithNotNullObject:self.serviceTypeText.text andDefailtInfo:@""],
                          @"gpsLongitude":longtitudeNumber,
                          @"gpsLatitude":latitudeNumber,
                         @"province":[XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"%@省",self.daModel.Province] andDefailtInfo:@""],
                          @"city":[XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"%@市",self.daModel.city] andDefailtInfo:@""],
                          @"area":[XJUtil insertStringWithNotNullObject:self.daModel.county andDefailtInfo:@""],
                          @"address":[XJUtil insertStringWithNotNullObject:self.detailText.text andDefailtInfo:@""],
                          @"openTime":[XJUtil insertStringWithNotNullObject:self.shopTimeText.text andDefailtInfo:@""],
                          @"phone":[XJUtil insertStringWithNotNullObject:self.nameText.text andDefailtInfo:@""],
                          @"remark":[XJUtil insertStringWithNotNullObject:self.serviceIntroText.text andDefailtInfo:@""],
                          
                          
    };
//    NSMutableDictionary *mutableDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
    self.clickBlock(dic);
}

- (void)setReapirList:(NSArray *)dataList{
    if (dataList) {
        if (self.datasArr.count == 0) {
            for (int i = 0; i<dataList.count; i++) {
                TypeModel *model = [TypeModel new];
                [model setValuesForKeysWithDictionary:dataList[i]];
                [self.datasArr addObject:model];
            }
            self.popView.datasArr = self.datasArr;
        }
        
    }
}

- (void)clickAct{
    if (self.addresskBlock) {
        self.addresskBlock();
    }
}

- (void)selAdress{
    [self endEditing:true];
    [self.areasView startAnimationFunction];
}

- (void)setupData:(DDSearchPoi *)poi{
    self.detailText.text = poi.address;
    self.mapGeo= [AMapGeoPoint locationWithLatitude:poi.coordinate.latitude longitude:poi.coordinate.longitude];
//    [[AMapGeocode a]
}

- (void)selServiceType{
    [self endEditing:true];
    [self.popView show];
    NSMutableArray *typeArr = [NSMutableArray array];
    WeakSelf(self)
    self.popView.clickBlock = ^(NSArray * _Nonnull datas) {
        [datas enumerateObjectsUsingBlock:^(TypeModel  *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.isSel) {
                [typeArr addObject:obj.name];
            }
        }];
        NSString *typeStr = [typeArr componentsJoinedByString:@","];
        weakself.serviceTypeText.text = typeStr;
    };
    
    
}



- (void)clickAreasViewEnsureBtnActionAreasDate:(DQAreasModel *)model{
    NSLog(@"%@",model.city);
    self.areaText.text = [NSString stringWithFormat:@"%@ %@ %@",model.Province,model.city,model.county];
    self.daModel = model;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //    self.searchApi = [[AMapSearchAPI alloc] init];
    //    self.searchApi.delegate = self;
    if (!self.daModel) {
        return;
    }
    AMapGeocodeSearchRequest *geo = [[AMapGeocodeSearchRequest alloc] init];
    geo.address = [NSString stringWithFormat:@"%@省%@市%@%@",self.daModel.Province,self.daModel.city,self.daModel.county,textField.text];
    JLog(@"%@",[NSString stringWithFormat:@"%@%@%@%@",self.daModel.Province,self.daModel.city,self.daModel.county,textField.text]);
    [self.searchApi AMapGeocodeSearch:geo];
}

/**
 * @brief 地理编码查询回调函数
 * @param request  发起的请求，具体字段参考 AMapGeocodeSearchRequest 。
 * @param response 响应结果，具体字段参考 AMapGeocodeSearchResponse 。
 */
- (void)onGeocodeSearchDone:(AMapGeocodeSearchRequest *)request response:(AMapGeocodeSearchResponse *)response{
    if (response.geocodes.count == 0)
    {
        return;
    }
    
    NSArray<AMapGeocode *> *datas = response.geocodes;
//    self.mapGeo = datas.firstObject;
//    JLog(@"当前经纬度 %@",self.mapGeo.location);
}

- (UITextField *)serviceTypeText{
    if (!_serviceTypeText) {
        _serviceTypeText = [UITextField new];
        _serviceTypeText.placeholder = @"请选择服务类型";
        _serviceTypeText.textColor = UIColor333;
        _serviceTypeText.font = LabelFont14;
//        _serviceTypeText.keyboardType = UIKeyboardTypePhonePad;
        _serviceTypeText.userInteractionEnabled = false;
        #if DEBUG
//                _serviceTypeText.text = @"13777889945";
        #endif
        [self.contentView addSubview:_serviceTypeText];
    }
    
    return _serviceTypeText;
}

- (UITextField *)nameText{
    if (!_nameText) {
        _nameText = [UITextField new];
        _nameText.placeholder = @"请输入联系人手机号码";
        _nameText.textColor = UIColor333;
        _nameText.font = LabelFont14;
        _nameText.keyboardType = UIKeyboardTypePhonePad;
        #if DEBUG
                _nameText.text = @"13777889945";
        #endif
        [self.contentView addSubview:_nameText];
    }
    
    return _nameText;
}

- (UITextField *)detailText{
    if (!_detailText) {
        _detailText = [UITextField new];
        _detailText.placeholder = @"如道路、门牌号等(必填)";
        _detailText.textColor = UIColor333;
        _detailText.font = LabelFont14;
        _detailText.delegate = self;
       #if DEBUG
//        _detailText.text = @"牛山北路";
             #endif
        [self.contentView addSubview:_detailText];
    }
    
    return _detailText;
}


- (UITextField *)areaText{
    if (!_areaText) {
        _areaText = [UITextField new];
        _areaText.placeholder = @"请选择省市区";
        _areaText.textColor = UIColor333;
        _areaText.font = LabelFont14;
//        _areaText.userInteractionEnabled = false;
        [self.contentView addSubview:_areaText];
    }
    
    return _areaText;
}

- (UITextField *)serviceIntroText{
    if (!_serviceIntroText) {
        _serviceIntroText = [UITextField new];
        _serviceIntroText.placeholder = @"请输入相关介绍";
        _serviceIntroText.textColor = UIColor333;
        _serviceIntroText.font = LabelFont14;
#if DEBUG
        _serviceIntroText.text = @"相关介绍相关介绍相关介绍相关介绍相关介绍相关介绍相关介绍";
#endif
        [self.contentView addSubview:_serviceIntroText];
    }
    
    return _serviceIntroText;
}

- (UITextField *)shopTimeText{
    if (!_shopTimeText) {
        _shopTimeText = [UITextField new];
        _shopTimeText.placeholder = @"请输入营业时间(例08:00-18:00)";
        _shopTimeText.textColor = UIColor333;
        _shopTimeText.font = LabelFont14;
//        _shopTimeText.keyboardType = UIKeyboardTypeNamePhonePad;
        #if DEBUG
                _shopTimeText.text = @"08:00-18:00";
        #endif
        [self.contentView addSubview:_shopTimeText];
    }
    
    return _shopTimeText;
}

- (DQAreasView *)areasView{
    if (!_areasView) {
        _areasView = [DQAreasView new];
        _areasView.delegate = self;
    }
    
    return _areasView;
}

- (AMapSearchAPI *)searchApi{
    if (!_searchApi) {
        _searchApi = [AMapSearchAPI new];
        _searchApi.delegate = self;
    }
    
    return _searchApi;
}
- (MutipleTypePopView *)popView{
    if (!_popView) {
        _popView = [MutipleTypePopView new];
        
        _popView.titleLabel.text = @"请选择服务类型（可多选）";
//        WeakSelf(self)
//        _popView.clickBlock = ^(NSInteger pType, NSInteger rType) {
//
//
//        };
    }
    
    return _popView;
}

- (NSMutableArray *)datasArr{
    if (!_datasArr) {
        _datasArr = [NSMutableArray array];
//        NSArray *datas = @[@{@"name":@"产品销售",@"isSel":@0},@{@"name":@"维修服务",@"isSel":@0}];
//        [_datasArr addObject:@{@"title":@"产品销售",@"isSel":@0}];
//        [_datasArr addObject:@{@"title":@"维修服务",@"isSel":@0}];
//        [_datasArr addObject:@{@"title":@"产品销售"}];
//        [_datasArr addObject:@{@"title":@"维修服务"}];
//        [_datasArr addObject:@{@"title":@"产品销售"}];
//        [_datasArr addObject:@{@"title":@"维修服务"}];
       
    }

    return _datasArr;
}

@end
