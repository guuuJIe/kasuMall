//
//  RepairServicesTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDSearchObj.h"
NS_ASSUME_NONNULL_BEGIN

@interface RepairServicesTableCell : UITableViewCell
@property (nonatomic, copy) void(^clickBlock)(NSDictionary *dic);
@property (nonatomic, copy) void(^addresskBlock)(void);
- (void)setReapirList:(NSArray *)dataList;
- (void)setupData:(DDSearchPoi *)poi;
@end

NS_ASSUME_NONNULL_END
