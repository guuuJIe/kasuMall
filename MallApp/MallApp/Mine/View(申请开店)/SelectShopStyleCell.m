//
//  SelectShopStyleCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SelectShopStyleCell.h"
#import "MutipleTypePopView.h"
#import "TypeModel.h"
@interface SelectShopStyleCell()
@property (nonatomic, strong) UIButton *typeBtn;
@property (nonatomic, strong) MutipleTypePopView *popView;
@property (nonatomic, strong) NSMutableArray *datasArr;
@end
@implementation SelectShopStyleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
        
    }
    
    return self;
}

- (void)setupUI{
    UILabel *title1 = [UILabel new];
    title1.text = @"店铺类型";
    title1.textColor = UIColor333;
    title1.font = LabelFont14;
    [self.contentView addSubview:title1];
    [title1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(12);
        make.bottom.mas_equalTo(-12);
    }];
    
    
    
    
    UIButton *btn = [UIButton new];
    [btn setImage:[UIImage imageNamed:@"more"] forState:0];
    [self.contentView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title1);
//        make.size.mas_equalTo(CGSizeMake(22, 22));
        make.bottom.mas_equalTo(-12);
        make.right.mas_equalTo(-8);
    }];
    
    [self.typeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(title1);
        make.left.mas_equalTo(title1.mas_right).offset(15);
//        make.right.mas_equalTo(-13);
//        make.width.mas_equalTo(Screen_Width - 100);
    }];
    UIButton *button = [UIButton new];
    [self.contentView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
    [button addTarget:self action:@selector(typeClick) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)typeClick{
    [self.popView show];
}




- (UIButton *)typeBtn{
    if (!_typeBtn) {
        _typeBtn = [UIButton new];
        [_typeBtn setTitle:@"请选择你的店铺类型" forState:0];
        [_typeBtn setTitleColor:UIColorBF forState:0];
        [_typeBtn.titleLabel setFont:LabelFont14];
//        [_typeBtn.titleLabel setTextAlignment:NSTextAlignmentLeft];

        
        [self.contentView addSubview:_typeBtn];
    }
    
    return _typeBtn;
}

- (MutipleTypePopView *)popView{
    if (!_popView) {
        _popView = [MutipleTypePopView new];
        _popView.datasArr = self.datasArr;
        _popView.titleLabel.text = @"请选择店铺类型（可多选）";
        WeakSelf(self)
        _popView.clickBlock = ^(NSArray * _Nonnull datas) {
            TypeModel *pModel = datas[0];
            TypeModel *rModel = datas[1];
            NSInteger pType = 0,rType = 0;
            if (pModel.isSel) {
                pType = 1;
            }
            if (rModel.isSel) {
                rType = 1;
            }
            
            if (pType == 1 && rType == 1) {
                [weakself.typeBtn setTitle:@"商城服务、维修服务" forState:0];
            }
            
            if (pType == 1 && rType != 1) {
                [weakself.typeBtn setTitle:@"商城服务" forState:0];
            }
            
            if (rType == 1 && pType != 1) {
               [weakself.typeBtn setTitle:@"维修服务" forState:0];
            }
            
            [weakself.typeBtn setTitleColor:UIColor333 forState:0];
            
            if (weakself.refreshBlock) {
                weakself.refreshBlock(pType, rType);
            }
        };
       
    }
    
    return _popView;
}

- (NSMutableArray *)datasArr{
    if (!_datasArr) {
        _datasArr = [NSMutableArray array];
        NSArray *datas = @[
  @{@"name":@"商城服务",@"isSel":@0},
  @{@"name":@"维修服务",@"isSel":@0}];

        for (int i = 0; i<datas.count; i++) {
            TypeModel *model = [TypeModel new];
            [model setValuesForKeysWithDictionary:datas[i]];
            [_datasArr addObject:model];
        }
    }

    return _datasArr;
}



@end
