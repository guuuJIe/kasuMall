//
//  MutipleTypePopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <MMPopupView/MMPopupView.h>

NS_ASSUME_NONNULL_BEGIN

@interface MutipleTypePopView : MMPopupView
@property (nonatomic, strong) NSArray *datasArr;
//@property (nonatomic, copy) void(^clickBlock)(NSInteger pType,NSInteger rType);

@property (nonatomic, copy) void(^clickBlock)(NSArray *datas);
@property (nonatomic, strong) UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
