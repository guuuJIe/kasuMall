//
//  TypeModel.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TypeModel : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger ids;
@property (nonatomic, assign) BOOL isSel;
@end

NS_ASSUME_NONNULL_END
