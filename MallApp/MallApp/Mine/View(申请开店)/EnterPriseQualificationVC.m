//
//  EnterPriseQualificationVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "EnterPriseQualificationVC.h"
#import "ShowPicTableViewCell.h"
#import <YBImageBrowser/YBImageBrowser.h>
@interface EnterPriseQualificationVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSMutableArray *datas;
@end

@implementation EnterPriseQualificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    
    [self setupData];
}

- (void)setupUI{
    self.title = @"企业资质";
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)setupData{
    NSString *file0 = self.dataDic[@"file0"];
    if (file0.length > 0) {
        NSDictionary *dic = @{@"title":@"营业执照",@"file":file0};
        [self.datas addObject:dic];
    }
  
    NSString *file1 = self.dataDic[@"file3"];
    if (file1.length > 0) {
        NSDictionary *dic = @{@"title":@"道路许可证",@"file":file1};
        [self.datas addObject:dic];
    }
    
    NSString *file2 = self.dataDic[@"file1"];
    if (file2.length > 0) {
        NSDictionary *dic = @{@"title":@"门头照",@"file":file2};
        [self.datas addObject:dic];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"ShowPicTableViewCell";
    ShowPicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[ShowPicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datas[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    YBIBImageData *data = [YBIBImageData new];
    
    NSDictionary *dic = self.datas[indexPath.row];
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"file"]];
    data.imageURL = URL(url);
    YBImageBrowser *browser = [YBImageBrowser new];
    NSArray *dataArray = @[data];
    browser.dataSourceArray = dataArray;
    browser.currentPage = indexPath.row;
    [browser show];
}

- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];
    }
    
    return _datas;
}

-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerClass:[ShowPicTableViewCell class] forCellReuseIdentifier:@"ShowPicTableViewCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}
@end
