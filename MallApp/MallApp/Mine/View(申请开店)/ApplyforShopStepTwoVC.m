//
//  ApplyforShopStepTwoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/14.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplyforShopStepTwoVC.h"
#import "SelectShopStyleCell.h"
#import "ProductSalesTableCell.h"
#import "RepairServicesTableCell.h"
#import "ReasonPopView.h"
#import "MineApi.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "AddressLocationVC.h"
@interface ApplyforShopStepTwoVC ()<UITableViewDelegate,UITableViewDataSource,AMapSearchDelegate>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger Ptype;
@property (nonatomic, assign) NSInteger Rtype;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) AMapSearchAPI *searchApi;
@property (nonatomic, strong) NSArray *datas;
@end

@implementation ApplyforShopStepTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self fillData];
    [self initProp];
    
    [self setupUI];
    
    
    
}

- (void)initProp{
    self.Ptype = 0;
    self.Rtype = 0;
//    self.isExecute = true;
    
//    self.searchApi = [[AMapSearchAPI alloc] init];
//    self.searchApi.delegate = self;
//    AMapGeocodeSearchRequest *geo = [[AMapGeocodeSearchRequest alloc] init];
//    geo.address = @"浙江省温州市鹿城区牛山商务大厦";
//    [self.searchApi AMapGeocodeSearch:geo];
}



- (void)fillData{
    JLog(@"%@",self.dic);
    [self.mineApi getRepairTypeListWithparameters:nil withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.datas = result.result;
        }
    }];
    
    
//    [self.mineApi getServiceTypeListWithparameters:nil withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//
//        }
//    }];
    
}



- (void)setupUI{
    self.title = @"申请开店";
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-BottomAreaHeight - 70);
    }];
    
    
    UIButton *next = [UIButton new];
    [next setTitle:@"提交申请" forState:0];
    [next setBackgroundColor:APPColor];
    [next setTitleColor:UIColor.whiteColor forState:0];
    [next.titleLabel setFont:LabelFont14];
    [next addTarget:self action:@selector(submitInfo) forControlEvents:UIControlEventTouchUpInside];
    next.layer.cornerRadius = 4;
    [self.view addSubview:next];
    [next mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(-17-BottomAreaHeight);
        make.height.mas_equalTo(44);
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.Ptype == 1 && self.Rtype == 1) {
        return 3;
    }
    
    if (self.Ptype == 1) {
        return 2;
    }
    
    if (self.Rtype == 1) {
        return 3;
    }
    
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *Identifier = @"SelectShopStyleCell";
        SelectShopStyleCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[SelectShopStyleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        WeakSelf(self)
        cell.refreshBlock = ^(NSInteger pType, NSInteger rType) {
            weakself.Ptype = pType;
            weakself.Rtype = rType;
            [weakself.listTableView reloadData];
        };
        return cell;
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"ProductSalesTableCell";
        ProductSalesTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[ProductSalesTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        if (self.Ptype == 0) {
            cell.hidden = true;
        }else{
            cell.hidden = false;
        }
        WeakSelf(self)
        cell.clickBlock = ^(NSDictionary * _Nonnull dic) {
            StrongSelf(self)
            if (self.Ptype == 1) {
                if (dic) {

                    [self.dic setValue:dic forKey:@"ProductService"];
                }
            }
            

        };
        WeakSelf(cell);
        cell.addresskBlock = ^{
            StrongSelf(self)
            
            AddressLocationVC *vc = [AddressLocationVC new];
            vc.block = ^(DDSearchPoi * _Nonnull poi) {
                [weakcell setupData:poi];
            };
            [self.navigationController pushViewController:vc animated:true];
            
        };
        return cell;
    }else if (indexPath.row == 2){
        static NSString *Identifier = @"RepairServicesTableCell";
        RepairServicesTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[RepairServicesTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        if (self.Rtype == 0) {
            cell.hidden = true;
        }else{
            cell.hidden = false;
        }
        WeakSelf(self)
        cell.clickBlock = ^(NSDictionary * _Nonnull dic) {
            StrongSelf(self)
            if (self.Rtype == 1) {
                if (dic) {
                    [self.dic setValue:dic forKey:@"repairService"];
                }
            }
            
         
        };
        WeakSelf(cell);
        cell.addresskBlock = ^{
            StrongSelf(self)
            
            AddressLocationVC *vc = [AddressLocationVC new];
            vc.block = ^(DDSearchPoi * _Nonnull poi) {
                [weakcell setupData:poi];
            };
            [self.navigationController pushViewController:vc animated:true];
            
        };
        [cell setReapirList:self.datas];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        if (self.Rtype == 1 && self.Ptype != 1) {
            return CGFLOAT_MIN;
        }
    }
    
    return UITableViewAutomaticDimension;
}

- (void)uploadShopInfo{
//    JLog(@"111");
//    if (self.isExecute) {
//        self.isExecute = !self.isExecute;
//    }
    JLog(@"%@",self.dic);
    [JMBManager showLoading];
    [self.mineApi applyShopActWithparameters:self.dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [JMBManager showBriefAlert:@"申请成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:true];
            });
        }
        [JMBManager hideAlert];
    }];
}

- (void)submitInfo{
    

    if (self.Ptype == 0 && self.Rtype == 0) {
        [JMBManager showBriefAlert:@"请选择店铺类型"];
        return;
    }
    
    
    if (self.Ptype == 1 && self.Rtype == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"postProudctInfo" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"postRepairInfo" object:nil];
    }
    
    
    if (self.Ptype == 1 && self.Rtype != 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"postProudctInfo" object:nil];
    }
    
    if (self.Rtype == 1 && self.Ptype != 1 ) {
         [[NSNotificationCenter defaultCenter] postNotificationName:@"postRepairInfo" object:nil];
    }
    
    [self uploadShopInfo];
    
    
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    
    return _mineApi;
}

-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerClass:[SelectShopStyleCell class] forCellReuseIdentifier:@"SelectShopStyleCell"];
        [_listTableView registerClass:[ProductSalesTableCell class] forCellReuseIdentifier:@"ProductSalesTableCell"];
        [_listTableView registerClass:[RepairServicesTableCell class] forCellReuseIdentifier:@"RepairServicesTableCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}



@end
