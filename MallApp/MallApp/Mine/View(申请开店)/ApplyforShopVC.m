//
//  ApplyforShopVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplyforShopVC.h"
#import "ShopBasicInfoTableCell.h"
#import "UploadPicTableCell.h"
#import "TZImagePickerController.h"
#import "ApplyforShopStepTwoVC.h"
#import "MineApi.h"
#import <YBImageBrowser/YBImageBrowser.h>

#import "UploadPicTableCell.h"

@interface ApplyforShopVC ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, strong) NSArray *titleData;
@property (nonatomic, assign) NSInteger maxPicNum;//挑选图片的数量
@property (nonatomic, strong) NSIndexPath *curIndexPath;//挑选图片的数量
@property (nonatomic, strong) NSMutableArray *picDatas;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) NSMutableDictionary *picDataDic;
@property (nonatomic, strong) NSMutableDictionary *tempDic;
@property (nonatomic, strong) NSString *picStr;
@property (nonatomic, strong) UIButton *nextBtn;
@end

@implementation ApplyforShopVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self setupdata];
}

- (void)setupUI{
    self.title = @"申请开店";
    self.maxPicNum = 3;
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-BottomAreaHeight-61);
    }];
    
    
    UIButton *next = [UIButton new];
    [next setTitle:@"下一步" forState:0];
    [next setBackgroundColor:APPColor];
    [next setTitleColor:UIColor.whiteColor forState:0];
    [next.titleLabel setFont:LabelFont14];
    [next addTarget:self action:@selector(nextAct) forControlEvents:UIControlEventTouchUpInside];
    next.layer.cornerRadius = 4;
    [self.view addSubview:next];
    self.nextBtn = next;
    [next mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(-17-BottomAreaHeight);
        make.height.mas_equalTo(44);
    }];
    
}




- (void)setupdata{
    if (self.totalDic) {
        [self.picDatas removeAllObjects];
        self.nextBtn.hidden = true;
        for (int i = 0; i<5; i++) {
            NSMutableDictionary *dic = [NSMutableDictionary new];
            if (i == 0) {
                [dic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"shopLogo"] andDefailtInfo:@""]  forKey:[NSString stringWithFormat:@"0"]];
            }else if (i == 1){
                [dic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file1"] andDefailtInfo:@""]  forKey:[NSString stringWithFormat:@"0"]];
            }else if (i == 2){
                [dic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file2"] andDefailtInfo:@""]  forKey:[NSString stringWithFormat:@"0"]];
            }else if (i == 3){
                [dic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file0"] andDefailtInfo:@""]  forKey:[NSString stringWithFormat:@"0"]];
            }else if (i == 4){
                [dic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file3"] andDefailtInfo:@""]  forKey:[NSString stringWithFormat:@"0"]];
            }
            
            
            [self.picDatas addObject:dic];
        }
//        [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"shopLogo"] andDefailtInfo:@""] forKey:@"0"];
//        [self.picDatas addObject:self.picDataDic];
//        [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file1"] andDefailtInfo:@""] forKey:@"1"];
//        [self.picDatas addObject:self.picDataDic];
//        [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file2"] andDefailtInfo:@""] forKey:@"2"];
//        [self.picDatas addObject:self.picDataDic];
//        [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file0"] andDefailtInfo:@""] forKey:@"3"];
//        [self.picDatas addObject:self.picDataDic];
//        [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.totalDic[@"file3"] andDefailtInfo:@""] forKey:@"4"];
//        [self.picDatas addObject:self.picDataDic];
        [self.listTableView reloadData];
    }
}

#pragma mark ----UITableViewDataSource----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return self.titleData.count;
    }
    
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"ShopBasicInfoTableCell";
        ShopBasicInfoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[ShopBasicInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        WeakSelf(self)
        cell.clickBlock = ^(NSDictionary * _Nonnull dic) {
            StrongSelf(self)
            NSString *file0 = [self.picDataDic objectForKey:@"file0"];
            NSString *file1 = [self.picDataDic objectForKey:@"file1"];
            
            if (file1.length <= 0) {
                [JMBManager showBriefAlert:@"请上传门头照"];
                return ;
            }
            if (file0.length <= 0) {
                [JMBManager showBriefAlert:@"请上传营业执照"];
                return ;
            }
//           
            [self.picDataDic addEntriesFromDictionary:dic];
            ApplyforShopStepTwoVC *vc = [ApplyforShopStepTwoVC new];
            vc.dic = self.picDataDic;
            [self.navigationController pushViewController:vc animated:true];
        };
        [cell setupData:self.totalDic];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"UploadPicTableCell";
        UploadPicTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        if (self.totalDic) {
            [cell setupCollectionData:self.picDatas[indexPath.row] withIndexpath:indexPath];
        }else{
            [cell setupCollectionDatawith:self.picDatas[indexPath.row] withIndexPath:indexPath];
        }
        WeakSelf(self)
        cell.clickBlock = ^(NSInteger index) {
            StrongSelf(self)
            NSLog(@"%ld",(long)indexPath.row);
            if (self.totalDic) {
                [self checkUploadInfo:indexPath];
            }else{
               [self clickActWithNSIndexPath:indexPath];
            }
           
        };
        
        [cell setupTitleData:self.titleData[indexPath.row]];
        return cell;
    }
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.totalDic) {
        if (indexPath.section == 1) {
            
           
            

        }
       
        
    }else{
        if (indexPath.section == 1) {
           
        }
    }
    
    
}

-(void)checkUploadInfo:(NSIndexPath *)indexPath{
    
    YBIBImageData *data = [YBIBImageData new];
    NSDictionary *dic = self.picDatas[indexPath.row];
    NSMutableArray *array = [NSMutableArray array];
    if (indexPath.row == 2) {
        NSString *str = dic[@"0"];
        NSArray *arrays;
        if (str.length > 0) {
            arrays = [str componentsSeparatedByString:@","];
            for (int i = 0; i<arrays.count; i++) {
                
                NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,arrays[i]];
                data.imageURL = URL(url);
                [array addObject:data];
            }
        }
       
    }else{
//        YBIBImageData *data = [YBIBImageData new];
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"0"]];
        data.imageURL = URL(url);
        [array addObject:data];
    }
   
    YBImageBrowser *browser = [YBImageBrowser new];
//    NSArray *dataArray = @[data];
    browser.dataSourceArray = array;
    browser.currentPage = 0;
    [browser show];
}
- (void)clickActWithNSIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        self.maxPicNum = 4;

    }else{
        self.maxPicNum = 1;
    }
    self.curIndexPath = indexPath;
    [self selectPhoto];
}

- (void)uploadPic{
    NSMutableArray *imagesObj = [NSMutableArray new];
    for (int i = 0; i<self.selectedPhotos.count; i++) {
        NSDictionary *dic = @{@"name":@"files",@"image":self.selectedPhotos[i]};
        [imagesObj addObject:dic];
    }
    WeakSelf(self)
    [JMBManager showLoading];
    [self.mineApi uploadPicWithparameters:@"7" VisitImagesArr:imagesObj withCompletionHandler:^(NSError *error, MessageBody *result) {
        
        JLog(@"%@",result.result);
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.picStr = [datas componentsJoinedByString:@","];
            [weakself.picDatas replaceObjectAtIndex:self.curIndexPath.row withObject:weakself.tempDic];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.listTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.curIndexPath.row inSection:self.curIndexPath.section]] withRowAnimation:UITableViewRowAnimationNone];
            });
            [weakself matchPicKeyAndValues];
           
        }else{
            [JMBManager showBriefAlert:@"图片上传失败，请重新上传"];
        }
       
        [JMBManager hideAlert];
    }];
}

- (void)matchPicKeyAndValues{
    
    switch (self.curIndexPath.row) {
        case 0:
        {
            [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"shopLogo"];
        }
            break;
        case 1:
        {
            [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"file1"];
        }
            break;
        case 2:
        {
            [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"file2"];
        }
            break;
        case 3:
        {
            [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"file0"];
        }
            break;
        case 4:
        {
            [self.picDataDic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"file3"];
        }
            break;
        default:
            break;
    }
    
    
}

- (void)selectPhoto{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:6 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    imagePickerVc.maxImagesCount = self.maxPicNum;
    imagePickerVc.allowTakePicture = true; // 在内部显示拍照按钮
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    WeakSelf(self)
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        JLog(@"%@ --- %@",assets,photos);
        weakself.selectedAssets = [NSMutableArray arrayWithArray:assets];
        weakself.selectedPhotos = [NSMutableArray arrayWithArray:photos];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        NSString *indexAsset = [NSString stringWithFormat:@"%ld-assets",(long)self.curIndexPath.row];
        NSString *indexPhoto = [NSString stringWithFormat:@"%ld-photo",(long)self.curIndexPath.row];
        [dic setValue:weakself.selectedAssets forKey:indexAsset];
        [dic setValue:weakself.selectedPhotos forKey:indexPhoto];
        weakself.tempDic = dic;
       
        [weakself uploadPic];
        
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)checkPhoto:(NSInteger)index{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:_selectedAssets selectedPhotos:_selectedPhotos index:index];
    
    
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)nextAct{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postShopData" object:nil];

}


- (NSArray *)titleData{
    if (!_titleData) {
        _titleData = [NSArray arrayWithObjects:
     @{@"title":@"店铺LOGO",@"subTitle":@"请上传店铺LOGO",@"maxNumPic":@1},
     @{@"title":@"门脸照",@"subTitle":@"需拍出完整门匾、门框(必填)",@"maxNumPic":@1},
     @{@"title":@"店内照",@"subTitle":@"需真实反映店内环境(多图)",@"maxNumPic":@3},
     @{@"title":@"营业执照",@"subTitle":@"请上传营业执照(必填)",@"maxNumPic":@1},
     @{@"title":@"道路许可",@"subTitle":@"请上传道路许可证",@"maxNumPic":@1},
                      nil];
    }
    
    return _titleData;
}

-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerClass:[ShopBasicInfoTableCell class] forCellReuseIdentifier:@"ShopBasicInfoTableCell"];
        _listTableView.estimatedSectionHeaderHeight = 0;
        _listTableView.estimatedSectionFooterHeight = 0;
        [_listTableView registerClass:[UploadPicTableCell class] forCellReuseIdentifier:@"UploadPicTableCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)picDatas{
    if (!_picDatas) {
        _picDatas = [NSMutableArray array];
       
//        [dic setValue:@"" forKey:@"0"];
        for (int i = 0; i<5; i++) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            NSString *indexAsset = [NSString stringWithFormat:@"%d-assets",i];
            NSString *indexPhoto = [NSString stringWithFormat:@"%d-photo",i];
            [dic setValue:@[] forKey:indexAsset];
            [dic setValue:@[] forKey:indexPhoto];
            [_picDatas addObject:dic];
        }
    }
    
    return _picDatas;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    
    return _mineApi;
}

- (NSMutableDictionary *)picDataDic{//匹配图片字段的key
    if (!_picDataDic) {
        _picDataDic = [NSMutableDictionary dictionary];
    }
    
    return _picDataDic;
}
@end
