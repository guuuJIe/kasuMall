//
//  ReasonPopView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MutipleTypePopView.h"
#import "PurchaseView.h"
#import "ReasonTableCell.h"
@interface MutipleTypePopView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic) UIButton *chaButton;
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) NSIndexPath *selectIndexPath;
@property (nonatomic) PurchaseView *purchaseView;
@end

@implementation MutipleTypePopView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(300);
        }];
        
        [bgView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.top.mas_equalTo(16);
        }];
        
        [bgView addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(12));
            make.top.equalTo(@16);
        }];
        
        UIView *line = [UIView new];
        [bgView addSubview:line];
        line.backgroundColor = UIColorEF;
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(16);
            make.height.mas_equalTo(lineHeihgt);
        }];
        
        [bgView addSubview:self.TabelView];
        [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom).offset(10);
            make.left.right.equalTo(bgView);
            make.bottom.mas_equalTo(-50);
        }];
        
        [bgView addSubview:self.purchaseView];
        [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(bgView);
            make.height.mas_equalTo(50);
        }];
        
    }
    return self;
}

- (void)dismissView{
    [self hide];
}


- (void)setDatasArr:(NSArray *)datasArr{
    _datasArr = datasArr;
  
    [self.TabelView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datasArr.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"ReasonTableCell";
    ReasonTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[ReasonTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
    cell.typeModel = self.datasArr[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectIndexPath = indexPath;
    
    TypeModel *model = self.datasArr[indexPath.row];
    model.isSel = !model.isSel;
    
   
    [_TabelView reloadData];
    
//    [self hide];
}




-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"请选择质保原因";
        _titleLabel.font = LabelFont16;
        _titleLabel.textColor = UIColor333;
        
    }
    return _titleLabel;
}

-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
      
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}

#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.estimatedRowHeight = 50;
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerClass:[ReasonTableCell class] forCellReuseIdentifier:@"ReasonTableCell"];
        _TabelView.showsVerticalScrollIndicator = false;

    }
    return _TabelView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"确认" forState:0];
        WeakSelf(self)
        _purchaseView.addAddressBlock = ^{

            if (weakself.clickBlock) {
                weakself.clickBlock(weakself.datasArr);
            }
            [weakself hide];
        };
    }
    
    return _purchaseView;
}
@end

