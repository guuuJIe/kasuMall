//
//  UploadPicTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UploadPicTableCell.h"
#import "TZTestCell.h"
#import "TZImagePickerController.h"
#import "ImageZoomUtil.h"
@interface UploadPicTableCell()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate>
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *subTitleLbl;
@property (nonatomic, strong) UICollectionView *picCollectionView;
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, strong) NSMutableDictionary *dataDic;
@property (nonatomic, strong) NSArray *datas;
@end

@implementation UploadPicTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.mas_equalTo(12);
    }];
    
    [self.subTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(84);
        make.centerY.mas_equalTo(self.titleLbl);
    }];
    
    
    [self.picCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(84);
        make.top.mas_equalTo(self.titleLbl.mas_bottom).offset(12);
        make.height.mas_equalTo(60);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-22);
    }];

    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
}




- (void)setupCollectionDatawith:(NSMutableDictionary *)dataDic withIndexPath:(NSIndexPath *)indexPath{
    if (dataDic) {
        NSString *indexAsset = [NSString stringWithFormat:@"%ld-assets",(long)indexPath.row];
        NSString *indexPhoto = [NSString stringWithFormat:@"%ld-photo",(long)indexPath.row];
        _selectedAssets = dataDic[indexAsset];
        _selectedPhotos = dataDic[indexPhoto];
        [self.picCollectionView reloadData];
    }
   
}

- (void)setupCollectionData:(NSMutableDictionary *)dataDic withIndexpath:(NSIndexPath *)indexPath{
    _dataDic = dataDic;
    NSString *str = self.dataDic[@"0"];
    if (str.length > 0) {
        self.datas = [str componentsSeparatedByString:@","];
    }
    [self.picCollectionView reloadData];
}


#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.dataDic) {
        return self.datas.count;
    }else{
        if (_selectedPhotos.count >= 6) {
            return _selectedPhotos.count;
        }
        
        return _selectedPhotos.count + 1;
    }
   
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TZTestCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TZTestCell" forIndexPath:indexPath];
    cell.videoImageView.hidden = YES;

    if (self.selectedPhotos) {
        if (indexPath.item == _selectedPhotos.count) {
            cell.imageView.image = [UIImage imageNamed:@"R_addPic"];
            cell.deleteBtn.hidden = YES;
            cell.gifLable.hidden = YES;
        } else {
            cell.imageView.image = _selectedPhotos[indexPath.item];
            cell.asset = _selectedAssets[indexPath.item];
            cell.deleteBtn.hidden = false;
        }
        
        cell.deleteBtn.tag = indexPath.item;
        [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        cell.deleteBtn.hidden = YES;
        cell.gifLable.hidden = YES;

//        NSString *keys = [NSString stringWithFormat:@"%ld",(long)indexPath.item];
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,self.datas[indexPath.item]];
        [cell.imageView yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"R_addPic"]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataDic) {
//        self.picCollectionView.userInteractionEnabled = true;
//        TZTestCell *cell = (TZTestCell *)[collectionView cellForItemAtIndexPath:indexPath];
//        [ImageZoomUtil ImageZoomWithImageView:cell.imageView];
        if (self.clickBlock) {
            self.clickBlock(0);
        }
    }else{
//        if (indexPath.item == _selectedPhotos.count) {
//            
//            if (self.clickBlock) {
//                self.clickBlock(_selectedPhotos.count);
//            }
//            
//        }else{
//            if (self.clickBlock) {
//                self.clickBlock(indexPath.item);
//            }
//        }
        if (self.clickBlock) {
            self.clickBlock(0);
        }
    }
   
}

#pragma mark - Click Event

- (void)deleteBtnClik:(UIButton *)sender {
    if ([self collectionView:self.picCollectionView numberOfItemsInSection:0] <= _selectedPhotos.count) {
        [_selectedPhotos removeObjectAtIndex:sender.tag];
        [_selectedAssets removeObjectAtIndex:sender.tag];
        [self.picCollectionView reloadData];
        return;
    }
    
    [_selectedPhotos removeObjectAtIndex:sender.tag];
    [_selectedAssets removeObjectAtIndex:sender.tag];
    [_picCollectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:0];
        [self->_picCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [self->_picCollectionView reloadData];
    }];
}

- (void)setupTitleData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = dic[@"title"];
        self.subTitleLbl.text = dic[@"subTitle"];
    }
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.text = @"店铺LOGO";
        _titleLbl.textColor = UIColor333;
        _titleLbl.font = LabelFont14;
        [self.contentView addSubview:_titleLbl];
    }
    
    return _titleLbl;
}

- (UILabel *)subTitleLbl{
    if (!_subTitleLbl) {
        _subTitleLbl = [UILabel new];
        _subTitleLbl.text = @"请上传店铺LOGO";
        _subTitleLbl.textColor = UIColorBF;
        _subTitleLbl.font = LabelFont14;
        [self.contentView addSubview:_subTitleLbl];
    }
    
    return _subTitleLbl;
}


- (UICollectionView *)picCollectionView{
    if (!_picCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = CGSizeMake(56, 56);
        _picCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        
        _picCollectionView.backgroundColor = [UIColor whiteColor];
        _picCollectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4);
        _picCollectionView.dataSource = self;
        _picCollectionView.delegate = self;
        _picCollectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _picCollectionView.showsHorizontalScrollIndicator = false;
        [_picCollectionView registerClass:[TZTestCell class] forCellWithReuseIdentifier:@"TZTestCell"];
//        _picCollectionView.userInteractionEnabled = false;
        [self.contentView addSubview:_picCollectionView];
    }
    
    return _picCollectionView;
}

@end
