//
//  WithdrawDetailVC.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawDetailVC : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *identifyLbl;
@property (weak, nonatomic) IBOutlet UILabel *bankLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (nonatomic,strong) NSDictionary *data;
@end

NS_ASSUME_NONNULL_END
