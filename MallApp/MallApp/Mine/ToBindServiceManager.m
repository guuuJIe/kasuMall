//
//  ToBindServiceManager.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ToBindServiceManager.h"
#import "BindMyServiceManagerVC.h"
@interface ToBindServiceManager ()

@end

@implementation ToBindServiceManager

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"服务经理";
}
- (IBAction)bindservice:(id)sender {
    BindMyServiceManagerVC *vc = [[BindMyServiceManagerVC alloc] initWithNibName:@"BindMyServiceManagerVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
