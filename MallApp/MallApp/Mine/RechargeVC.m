//
//  RechargeVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "RechargeVC.h"
#import "MineApi.h"
#import "MinePayVC.h"
@interface RechargeVC ()
@property (nonatomic) MineApi *mineApi;
@end

@implementation RechargeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"充值";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SubmitAct:(UIButton *)sender {
    
    if (self.moneyNumText.text.length == 0) {
        [JMBManager showBriefAlert:@"请填写金额"];
        return;
    }
    NSDictionary *dic = @{@"amount":self.moneyNumText.text};
    [self.mineApi rechargeBalanceWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            MinePayVC *payVc =[MinePayVC new];
            payVc.money = self.moneyNumText.text;
            payVc.orderData = result.resultDic[@"list"];
            [self.navigationController pushViewController:payVc animated:true];
        }
    }];
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    return _mineApi;
}
@end
