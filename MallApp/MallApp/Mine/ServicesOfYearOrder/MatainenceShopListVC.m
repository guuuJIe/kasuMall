//
//  MatainenceShopListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MatainenceShopListVC.h"
#import "NoDataView.h"
#import "GroupApi.h"
#import "FixShopItemCell.h"
@interface MatainenceShopListVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>
@property (nonatomic, strong) UITableView *TabelView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, strong) NSMutableArray *datas;
@end

@implementation MatainenceShopListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    
    [self setupData:RefreshTypeNormal];
}

- (void)setupUI{
    self.title = @"门店列表";
    [self.view addSubview:self.TabelView];
    
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}


- (void)setupData:(RefreshType)type{
    [self.api getShopListDataWithparameters:@(self.type) withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.datas = [XJUtil dealData:datas withRefreshType:type withListView:self.TabelView AndCurDataArray:self.datas withNum:1];
            [self.TabelView reloadData];
            [self.TabelView reloadEmptyDataSet];
        }
    }];
}

#pragma mark ---UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *Identifier = @"FixShopItemCell";
    FixShopItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];

    [cell setupData:self.datas[indexPath.row]];

    return cell;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.clickBlock) {
        self.clickBlock(self.datas[indexPath.row]);
        [self.navigationController popViewControllerAnimated:true];
    }
}


#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NoDataView *view = [NoDataView initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-115, 150, 150);

    return view;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{

//    [self.collectionView reloadNetworkDataSet];
//    [self getHomeData];
}

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setEmptyDataSetSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
      
        [_TabelView registerClass:[FixShopItemCell class] forCellReuseIdentifier:@"FixShopItemCell"];
        WeakSelf(self);
        _TabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
     
//        _TabelView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            StrongSelf(self);
//            [self setupData:RefreshTypeUP];
//        }];
        _TabelView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    
    return _api;
}
@end
