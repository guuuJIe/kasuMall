//
//  LiPeiJiLuDetailVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "LiPeiJiLuDetailVC.h"
#import "LiPeiJiluDetailSectionOneCell.h"
#import "LiPeiJiluDetailSectionTwoCell.h"
#import "FixShopItemCell.h"
#import "QRCodeTableCell.h"
#import "GroupApi.h"
#import "PurchaseView.h"
@interface LiPeiJiLuDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, strong) PurchaseView *purchaseView;
@end

@implementation LiPeiJiLuDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"详情";
    
    [self setupUI];
    
    [self setupData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.view.backgroundColor = [UIColor whiteColor];
}
- (void)setupUI{
    
   
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-50-BottomAreaHeight);
    }];
    
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setupData{
    WeakSelf(self)
    if (self.type == 1) {
        //保养
        [self.api getEmRecordInfoWithparameters:self.ids withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *datas = result.result;
                [weakself configData:datas];
            }
        }];
    }else{
        //报修
        [self.api getEwRecordInfoWithparameters:self.ids withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *datas = result.result;
                [weakself configData:datas];
            }
        }];
    }
    
}

- (void)configData:(NSArray *)datas{
    
    self.dataDic = datas.firstObject;
    [self.listTableView reloadData];
    
    
    NSInteger statues = [self.dataDic[@"status"] integerValue];
    
    if (statues == 4) {
        self.purchaseView.hidden = false;
    }else{
        self.purchaseView.hidden = true;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *Identifier = @"FixShopItemCell";
        FixShopItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dataDic[@"shopInfo"]];
        return cell;
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"QRCodeTableCell";
        QRCodeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.row == 2){
        static NSString *Identifier = @"LiPeiJiluDetailSectionOneCell";
        LiPeiJiluDetailSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.row == 3){
        static NSString *Identifier = @"LiPeiJiluDetailSectionTwoCell";
        LiPeiJiluDetailSectionTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.dataDic];
        return cell;
    }
    
    return nil;
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.showsVerticalScrollIndicator = false;

        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"LiPeiJiluDetailSectionOneCell" bundle:nil] forCellReuseIdentifier:@"LiPeiJiluDetailSectionOneCell"];
         [_listTableView registerNib:[UINib nibWithNibName:@"LiPeiJiluDetailSectionTwoCell" bundle:nil] forCellReuseIdentifier:@"LiPeiJiluDetailSectionTwoCell"];
        [_listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        [_listTableView registerClass:[FixShopItemCell class] forCellReuseIdentifier:@"FixShopItemCell"];
        [_listTableView registerClass:[QRCodeTableCell class] forCellReuseIdentifier:@"QRCodeTableCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [self.purchaseView.purchaseButton setTitle:@"确定完成" forState:0];
        _purchaseView.hidden = true;
        WeakSelf(self)
        _purchaseView.addAddressBlock = ^{
            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否确认完成" andHandler:^(NSInteger index) {
                if (index == 1) {
                    [weakself.api emRepairOrderCompleteWithparameters:weakself.dataDic[@"orderNumber"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                        if (result.code == 200) {
                            [weakself setupData];
                        }
                    }];
                }
            }];
        };
    }
    
    
    return _purchaseView;
}

- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

@end
