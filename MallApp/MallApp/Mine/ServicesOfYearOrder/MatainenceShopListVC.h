//
//  MatainenceShopListVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MatainenceShopListVC : BaseViewController
@property (nonatomic, assign) NSInteger type;//1保养 2保修
@property (nonatomic,copy) void(^clickBlock)(NSDictionary *dic);
@end

NS_ASSUME_NONNULL_END
