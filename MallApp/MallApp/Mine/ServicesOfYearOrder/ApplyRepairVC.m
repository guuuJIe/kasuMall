//
//  ApplyRepairVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplyRepairVC.h"
#import "ServicesRepairSectionOneCell.h"
#import "ServicesRepairSectionTwoCell.h"
#import "ServicesRepairSectionThreeCell.h"
#import "TZImagePickerController.h"
#import "PurchaseView.h"
#import "LiPeiJiLuDetailVC.h"
#import "GroupApi.h"
#import "MineApi.h"
#import "MatainenceShopListVC.h"
#import "ReasonStateCell.h"
@interface ApplyRepairVC ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate>
@property (nonatomic, strong) UITableView *listTableView;

@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, strong) PurchaseView *purchaseView;
@property (nonatomic, strong) UITextField *nameText;
@property (nonatomic, strong) UITextField *telText;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) GroupApi *api;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) dispatch_semaphore_t lock;
@property (nonatomic, strong) NSString *picStr;
@property (nonatomic, strong) NSDictionary *shopInfo;
@end

@implementation ApplyRepairVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)setupUI{
    if (self.type == 1) {
        self.title = @"申请保养";
    }else{
       self.title = @"申请维修";
    }
   
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-50-BottomAreaHeight);
    }];
    
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)submitInfoAct{
   

    [self uploadInfo];
    
    
}

- (void)uploadInfo{
    
    if (self.telText.text.length == 0 || self.nameText.text.length == 0 || self.textView.text.length == 0 || !self.shopInfo) {
        [JMBManager showBriefAlert:@"请输入完整信息"];
        return;
    }
    
  
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        if (self.selectedPhotos.count >0) {
        NSMutableArray *imagesObj = [NSMutableArray new];
        if (self.selectedPhotos.count != 0) {
            for (int i = 0; i<self.selectedPhotos.count; i++) {
                NSDictionary *dic = @{@"name":@"files",@"image":self.selectedPhotos[i]};
                [imagesObj addObject:dic];
            }
            
            [JMBManager showLoading];
            
            [self.mineApi uploadPicWithparameters:@"5" VisitImagesArr:imagesObj withCompletionHandler:^(NSError *error, MessageBody *result) {
                
                JLog(@"%@",result.result);
                NSArray *datas = result.result;
                self.picStr = [datas componentsJoinedByString:@","];
                [JMBManager hideAlert];
                
                XJ_UNLOCK(self.lock);
            }];
            
            //        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER); // 在A请求未结束前 线程阻赛在这。直接A请求结束调用semaphore_signal信号量+1。不阻塞
            XJ_LOCK(self.lock);
        }
        
        NSMutableDictionary *dic = [NSMutableDictionary new];
        dispatch_async(dispatch_get_main_queue(), ^{
            [dic setValue:self.orderNum forKey:@"orderNumber"];
            [dic setValue:self.nameText.text forKey:@"contactPerson"];
            [dic setValue:self.telText.text forKey:@"contactPhone"];
//            [dic setValue:self.textView.text forKey:@"description"];
            [dic setValue:[XJUtil getNowTime] forKey:@"declarationTime"];
            [dic setValue:[XJUtil insertStringWithNotNullObject:self.picStr andDefailtInfo:@""] forKey:@"voucherPicture"];
            [dic setValue:self.shopInfo[@"shopId"] forKey:@"shopID"];
            XJ_UNLOCK(self.lock);
        });
        XJ_LOCK(self.lock);
        if (self.type == 1) {
            //保养
            [self.api createEmRecordOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [JMBManager showBriefAlert:@"提交成功"];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                }
                
                //             dispatch_semaphore_signal(semaphore);
                XJ_UNLOCK(self.lock);
            }];
        }else{
            //报修
            [self.api createEwRecordOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [JMBManager showBriefAlert:@"提交成功"];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                }
                
               
                XJ_UNLOCK(self.lock);
            }];
        }
        
       
        
       
    });
    

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        
        static NSString *Identifier = @"ServicesRepairSectionTwoCell";
        ServicesRepairSectionTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        [cell setupData:self.shopInfo];
        return cell;
        
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"ServicesRepairSectionOneCell";
        ServicesRepairSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        self.nameText = cell.nameTex;
        self.telText = cell.telTex;
        return cell;
    }else if (indexPath.row == 2){
        static NSString *Identifier = @"ReasonStateCell";
        ReasonStateCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        self.textView = cell.textView;
        return cell;
    }else if (indexPath.row == 3){
        static NSString *Identifier = @"ServicesRepairSectionThreeCell";
        ServicesRepairSectionThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        WeakSelf(self)
        cell.clickBlock = ^(NSInteger index) {
            if (index == weakself.selectedPhotos.count) {
                [weakself selectPhoto];
            }else{
                [weakself checkPhoto:index];
            }
        };
        [cell setupCollectionDatawith:_selectedPhotos andAssets:_selectedAssets];
        return cell;
    }
    
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        MatainenceShopListVC *vc = [MatainenceShopListVC new];
        vc.type = self.type;
        vc.clickBlock = ^(NSDictionary * _Nonnull dic) {
//            self.shopId = [dic[@"shopId"] integerValue];
            self.shopInfo = dic;
            dispatch_async(dispatch_get_main_queue(), ^{
                 [self.listTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            });
        };
        [self.navigationController pushViewController:vc animated:true];
    }
}



- (void)selectPhoto{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:6 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    imagePickerVc.maxImagesCount = 6;
    imagePickerVc.allowTakePicture = true; // 在内部显示拍照按钮
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    WeakSelf(self)
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        JLog(@"%@ --- %@",assets,photos);
        weakself.selectedAssets = [NSMutableArray arrayWithArray:assets];
        weakself.selectedPhotos = [NSMutableArray arrayWithArray:photos];
        dispatch_async(dispatch_get_main_queue(), ^{
             [self.listTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        });
       
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)checkPhoto:(NSInteger)index{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:_selectedAssets selectedPhotos:_selectedPhotos index:index];
    
    
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.showsVerticalScrollIndicator = false;

        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesRepairSectionOneCell" bundle:nil] forCellReuseIdentifier:@"ServicesRepairSectionOneCell"];
         [_listTableView registerNib:[UINib nibWithNibName:@"ServicesRepairSectionTwoCell" bundle:nil] forCellReuseIdentifier:@"ServicesRepairSectionTwoCell"];
        [_listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesRepairSectionThreeCell" bundle:nil] forCellReuseIdentifier:@"ServicesRepairSectionThreeCell"];
        [_listTableView registerClass:[ReasonStateCell class] forCellReuseIdentifier:@"ReasonStateCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"提交" forState:0];
        WeakSelf(self);
        _purchaseView.addAddressBlock = ^{
//            [weakself purchase];
            [weakself submitInfoAct];
        };
    }
    return _purchaseView;
}


- (GroupApi *)api{
    if (!_api) {
        _api = [GroupApi new];
    }
    return _api;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    return _mineApi;
}

- (dispatch_semaphore_t)lock {
    if (!_lock) {
        _lock = dispatch_semaphore_create(0);
    }
    return _lock;
}
@end
