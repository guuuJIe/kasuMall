//
//  ApplyRepairVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplyRepairVC : BaseViewController
@property (nonatomic, strong) NSString *orderNum;
@property (nonatomic, assign) NSInteger type; //1保养 2维修
@end

NS_ASSUME_NONNULL_END
