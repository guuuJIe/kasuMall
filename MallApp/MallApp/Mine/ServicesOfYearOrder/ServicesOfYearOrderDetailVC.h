//
//  ServicesOfYearOrderDetailVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServicesOfYearOrderDetailVC : BaseViewController
@property (nonatomic, strong) NSString *orderNum;
@property (nonatomic, assign) NSInteger ids;
@end

NS_ASSUME_NONNULL_END
