//
//  ServicesOfYearOrderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOfYearOrderVC.h"
#import "MinePromptView.h"
#import "MyOrderHeadView.h"
#import "ServicesOrderItemCell.h"
#import "GroupApi.h"
#import "ServicesOfOrderFootView.h"
#import "ServicesOfYearOrderDetailVC.h"
#import <TestSDKWorkspace/TestSDKWorkspace.h>
#import "EmptyViewForRecord.h"
@interface ServicesOfYearOrderVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>

@property (nonatomic, strong) MinePromptView *promptView;
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger size;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, strong) GroupApi *api;
@end

@implementation ServicesOfYearOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"保修服务";
    
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"SelectPayWayVC",@"SubmitServicesInfoVC",@"SubmitPersonCarInfoTwoVC",@"SubmitPersonCarInfoOneVC"]];
    
    [self setupProp];
    
    [self setupUI];
    
    [self setupData:RefreshTypeDown];
}


- (void)setupProp{
    self.promptView.orderStatues = 200;
    self.num = 1;
    self.size = 20;
    self.statues = 1;
}

- (void)setupUI{
//    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.equalTo(self.view);
//        make.height.mas_equalTo(45);
//    }];
    
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return self.datas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"ServicesOrderItemCell";
    ServicesOrderItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    [cell setupData:self.datas[indexPath.section]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MyOrderHeadView *HeadView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyOrderHeadView"];
    [HeadView setupEwData:self.datas[section]];
    return HeadView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ServicesOfOrderFootView *FootView = [ServicesOfOrderFootView new];
    [FootView setupEwData:self.datas[section]];
    return FootView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ServicesOfYearOrderDetailVC *vc = [ServicesOfYearOrderDetailVC new];
    NSDictionary *dic = self.datas[indexPath.section];
    vc.orderNum = dic[@"orderNumber"];
    
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    EmptyViewForRecord *view = [EmptyViewForRecord initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-115, 150, 150);
    view.desLbl.text = @"暂无数据";
    return view;
}



- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    

    
    [self.api getEwOrderListListnWithparameters:@(self.statues) withpageSize:self.size pageNum:self.num withCompletionHandler:^(NSError *error, MessageBody *result) {
         if (result.code == 200) {
        
             NSArray *array = result.result;
             
             self.datas = [XJUtil dealData:array withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datas withNum:self.num];
             [self.listTableView reloadData];
             [self.listTableView reloadEmptyDataSet];
         }
    }];
}


- (NSMutableArray *)datas{
    if (!_datas) {
        _datas = [NSMutableArray array];

       
    }
    return _datas;
}



- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        [_listTableView setEmptyDataSetSource:self];
        _listTableView.sectionHeaderHeight = 0.01f;
        _listTableView.sectionFooterHeight = 0.01f;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesOrderItemCell" bundle:nil] forCellReuseIdentifier:@"ServicesOrderItemCell"];
        [_listTableView registerClass:[MyOrderHeadView class] forHeaderFooterViewReuseIdentifier:@"MyOrderHeadView"];
//        [_listTableView registerClass:[MyOrderListFootView class] forHeaderFooterViewReuseIdentifier:@"MyOrderListFootView"];
        _listTableView.showsVerticalScrollIndicator= false;
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);

            [self setupData:RefreshTypeDown];

        }];
//
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (GroupApi *)api{
    if (!_api) {
        _api =[GroupApi new];
    }
    return _api;
}

@end
