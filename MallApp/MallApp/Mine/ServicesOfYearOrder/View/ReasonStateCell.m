//
//  ReasonStateCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ReasonStateCell.h"
@interface ReasonStateCell()<UITextViewDelegate>

@end
@implementation ReasonStateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    [self.contentView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(12);
    }];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = UIColorEF;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.nameLbl.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    [self.contentView addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(8);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(lineView.mas_bottom).offset(0);
        make.height.mas_equalTo(60);
        
    }];
    
    UIView *lineView2 = [UIView new];
    lineView2.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:lineView2];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.textView.mas_bottom).offset(12);
        make.height.mas_equalTo(10);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
    textView.text = @"";
    textView.textColor = UIColor333;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        textView.text = @"请输入申报说明";
        textView.textColor = UIColor999;
    }
}

- (UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [UILabel new];
        _nameLbl.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
        _nameLbl.textColor = UIColor333;
        _nameLbl.text = @"申报说明";
    }
    
    return _nameLbl;
}

- (UITextView *)textView{
    if (!_textView) {
        _textView = [UITextView new];
        _textView.textColor = UIColor999;
        _textView.font = LabelFont14;
        _textView.delegate = self;
        _textView.text = @"请输入申报说明";
    }
    
    return _textView;
}

@end
