//
//  ServicesDetailSectionOneCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServicesDetailSectionOneCell : UITableViewCell
@property (nonatomic,copy) void(^clickBlock)(void);
- (void)setupData:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UILabel *stateLbl;
@end

NS_ASSUME_NONNULL_END
