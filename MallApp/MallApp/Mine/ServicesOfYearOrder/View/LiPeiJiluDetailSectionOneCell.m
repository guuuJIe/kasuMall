//
//  LiPeiJiluDetailSectionOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "LiPeiJiluDetailSectionOneCell.h"
@interface LiPeiJiluDetailSectionOneCell()
@property (weak, nonatomic) IBOutlet UILabel *orderNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *stateLbl;

@end

@implementation LiPeiJiluDetailSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.orderNumLbl.text = dic[@"orderNumber"];
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contactPerson"] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contactPhone"] andDefailtInfo:@""];
        self.stateLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"description"] andDefailtInfo:@""];
    }
}

@end
