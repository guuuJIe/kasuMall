//
//  EmptyViewForRecord.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "EmptyViewForRecord.h"

@implementation EmptyViewForRecord

+(instancetype)initView{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"EmptyViewForRecord" owner:self options:nil] lastObject];
     // lastObject 可改为 firstObject，该数组只有一个元素，写哪个都行
}


@end
