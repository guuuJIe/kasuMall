//
//  ServicesOrderDetailSectionThreeCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOrderDetailSectionThreeCell.h"
@interface ServicesOrderDetailSectionThreeCell()
@property (weak, nonatomic) IBOutlet UIButton *arrowBtn;
@property (weak, nonatomic) IBOutlet UILabel *orderStatuesLbl;
@property (weak, nonatomic) IBOutlet UILabel *createTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *payWayLbl;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceLbl;
@end

@implementation ServicesOrderDetailSectionThreeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickAct:(id)sender {
    if (self.checkDelegateBlock) {
        self.checkDelegateBlock();
    }
}
- (IBAction)changeAct:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.isSelected) {
        [UIView animateWithDuration:0.3 animations:^{
            self.arrowBtn.transform = CGAffineTransformRotate(self.arrowBtn.transform,M_PI/2);
            
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            self.arrowBtn.transform = CGAffineTransformIdentity;
            
        }];
    }
    
    if (self.clickBlock) {
        self.clickBlock(sender.isSelected);
    }
    
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.orderStatuesLbl.text = dic[@"statusMessage"];
        self.createTimeLbl.text = dic[@"createdTime"];
        self.orderNoLbl.text = dic[@"orderNumber"];
        
        self.payWayLbl.text = dic[@"payWay"];
        self.totalPriceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
    }
}


@end
