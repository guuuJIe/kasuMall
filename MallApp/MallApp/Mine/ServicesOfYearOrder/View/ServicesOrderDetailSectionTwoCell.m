//
//  ServicesOrderDetailSectionTwoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOrderDetailSectionTwoCell.h"
@interface ServicesOrderDetailSectionTwoCell()
@property (weak, nonatomic) IBOutlet UILabel *mileAgeLbl;
@property (weak, nonatomic) IBOutlet UILabel *carNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *carVinLbl;
@property (weak, nonatomic) IBOutlet UILabel *carModelLbl;
@property (weak, nonatomic) IBOutlet UILabel *useNature;

@end

@implementation ServicesOrderDetailSectionTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.mileAgeLbl.text = [NSString stringWithFormat:@"%.f公里",[dic[@"mileage"] floatValue]];
        self.carNumLbl.text = dic[@"license"];
        self.carVinLbl.text = dic[@"vim"];
       self.carModelLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"model"] andDefailtInfo:@""];
        self.useNature.text = dic[@"nature"];
    }
}


@end
