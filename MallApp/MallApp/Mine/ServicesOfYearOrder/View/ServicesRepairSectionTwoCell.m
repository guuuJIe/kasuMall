//
//  ServicesRepairSectionTwoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesRepairSectionTwoCell.h"
@interface ServicesRepairSectionTwoCell()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *adressLbl;

@end

@implementation ServicesRepairSectionTwoCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopName"] andDefailtInfo:@""];
        self.adressLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopAddress"] andDefailtInfo:@""];
    }
}

@end
