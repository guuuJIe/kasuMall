//
//  LiPeiRecordItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "LiPeiRecordItemCell.h"
@interface LiPeiRecordItemCell()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *statuesLbl;

@end

@implementation LiPeiRecordItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor clearColor];
    self.bgView.layer.shadowColor = shawdowColor.CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.bgView.layer.shadowOpacity = 0.2;
    self.bgView.layer.shadowRadius = 2.0;
    self.bgView.layer.cornerRadius = 8;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contactPerson"] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contactPhone"] andDefailtInfo:@""];
        self.timeLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"declarationTime"] andDefailtInfo:@""];
        
        
//        NSInteger statues = [dic[@"status"] integerValue];
//        if (statues == 1) {
//            self.statuesLbl.text = @"提交中";
//        }else if(statues == 2){
//            self.statuesLbl.text = @"维修中";
//        }else if(statues == 3){
//            self.statuesLbl.text = @"维修完成";
//        }else if(statues == 4){
//            self.statuesLbl.text = @"确认完成";
//        }else if(statues == 4){
//            self.statuesLbl.text = @"已失效";
//        }
        
        NSInteger stautes = [dic[@"status"] integerValue];
        if (stautes == 1) {
            self.statuesLbl.text = @"审核中";
        }else if(stautes == 0){
            self.statuesLbl.text = @"已失效";
        }else if(stautes == 2){
            self.statuesLbl.text = @"待服务";
        }else if(stautes == 3){
            self.statuesLbl.text = @"服务中";
        }else if(stautes == 4){
            self.statuesLbl.text = @"维修完成";
        }else if(stautes == 5){
            self.statuesLbl.text = @"已完成";
        }else if(stautes == 6){
            //           self.titlelabel.text = @"确认完成";
        }
        
        
    }
}

@end
