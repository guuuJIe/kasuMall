//
//  FixShopItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixShopItemCell.h"
@interface FixShopItemCell()
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *adressLbl;
@end

@implementation FixShopItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    [self.contentView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-12);
    }];
    
    [self.contentView addSubview:self.adressLbl];
    [self.adressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.nameLbl.mas_bottom).offset(5);
        make.right.mas_equalTo(-12);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(self.adressLbl.mas_bottom).offset(12);
    }];
    
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = [NSString stringWithFormat:@"%@ %@",[XJUtil insertStringWithNotNullObject:dic[@"shopName"] andDefailtInfo:@""],[XJUtil insertStringWithNotNullObject:dic[@"shopPhone"] andDefailtInfo:@""]];
        self.adressLbl.text = [NSString stringWithFormat:@"%@",[XJUtil insertStringWithNotNullObject:dic[@"shopAddress"] andDefailtInfo:@""]];
    }
}

- (UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [UILabel new];
        _nameLbl.font = LabelFont16;
        _nameLbl.textColor = UIColor333;
        _nameLbl.text = @"公司名";
    }
    
    return _nameLbl;
}


- (UILabel *)adressLbl{
    if (!_adressLbl) {
        _adressLbl = [UILabel new];
        _adressLbl.font = LabelFont14;
        _adressLbl.textColor = UIColorBF;
        _adressLbl.text = @"浙江省温州市鹿城区";
        _adressLbl.numberOfLines = 0;
    }
    
    return _adressLbl;
}
@end
