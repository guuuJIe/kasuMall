//
//  ServicesOrderDetailSectionThreeCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServicesOrderDetailSectionThreeCell : UITableViewCell
@property (nonatomic,copy) void(^clickBlock)(BOOL isSel);
@property (nonatomic,copy) void(^checkDelegateBlock)(void);
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
