//
//  ServicesRepairSectionThreeCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServicesRepairSectionThreeCell : UITableViewCell
@property (nonatomic,copy) void(^clickBlock)(NSInteger index);
- (void)setupCollectionDatawith:(NSMutableArray *)photos andAssets:(NSMutableArray *)assets;
@end

NS_ASSUME_NONNULL_END
