//
//  ServicesOrderItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOrderItemCell.h"
@interface ServicesOrderItemCell()
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;

@end

@implementation ServicesOrderItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
        [self.productImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@""];
        self.subTitleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"sTitle"] andDefailtInfo:@""];
    }
}

@end
