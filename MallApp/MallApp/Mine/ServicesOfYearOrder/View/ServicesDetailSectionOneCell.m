//
//  ServicesDetailSectionOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesDetailSectionOneCell.h"
@interface ServicesDetailSectionOneCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitileLbl;
@property (weak, nonatomic) IBOutlet UILabel *kiloLbl;
@property (weak, nonatomic) IBOutlet UIImageView *statuImage;
@property (weak, nonatomic) IBOutlet UILabel *endtimeLbl;

@end

@implementation ServicesDetailSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)GoToLiPeiAct:(id)sender {
    
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@""];
        NSString *startTime = [XJUtil insertStringWithNotNullObject:dic[@"startTime"] andDefailtInfo:@""];
        NSString *endTime = [XJUtil insertStringWithNotNullObject:dic[@"endTime"] andDefailtInfo:@""];
        self.subTitileLbl.text = [NSString stringWithFormat:@"生效时间：%@年%@月%@日0时",[startTime substringWithRange:NSMakeRange(0, 4)],[startTime substringWithRange:NSMakeRange(5, 2)],[startTime substringWithRange:NSMakeRange(8, 2)]];
        self.endtimeLbl.text = [NSString stringWithFormat:@"结束时间：%@年%@月%@日24时",[endTime substringWithRange:NSMakeRange(0, 4)],[endTime substringWithRange:NSMakeRange(5, 2)],[endTime substringWithRange:NSMakeRange(8, 2)]];
        self.kiloLbl.text = [NSString stringWithFormat:@"有效公里数：%@公里至%@公里",dic[@"startMileage"],dic[@"endMileage"]];
        NSInteger statues = [dic[@"status"] integerValue];
        //1是生效 0是已失效 2审核中 3 审核失败
        if (statues == 1) {
            self.statuImage.image = [UIImage imageNamed:@"M_fix_iswork"];
//            self.endtimeLbl.hidden = false;
//            self.kiloLbl.hidden = false;
//            self.subTitileLbl.hidden = false;
        }else if(statues == 0){
            self.statuImage.image = [UIImage imageNamed:@"M_fix_unwork"];
//            self.endtimeLbl.hidden = false;
//            self.kiloLbl.hidden = false;
//            self.subTitileLbl.hidden = false;
           
        }else if (statues == 2){
            self.statuImage.image = [UIImage imageNamed:@"M_fix_isreview"];
//            self.endtimeLbl.hidden = true;
//            self.kiloLbl.hidden = true;
//            self.subTitileLbl.hidden = true;
            self.subTitileLbl.text = @"生效时间：";
            self.endtimeLbl.text = @"结束时间：";
            self.kiloLbl.text = @"有效公里数：";
        }else if (statues == 3){
            self.statuImage.image = [UIImage imageNamed:@"M_fix_isfail"];
//            self.endtimeLbl.hidden = true;
//            self.kiloLbl.hidden = true;
//            self.subTitileLbl.hidden = true;
            self.subTitileLbl.text = @"生效时间：";
            self.endtimeLbl.text = @"结束时间：";
            self.kiloLbl.text = @"有效公里数：";
        }
    }
}

@end
