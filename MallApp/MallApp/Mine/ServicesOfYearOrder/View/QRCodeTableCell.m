//
//  QRCodeTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "QRCodeTableCell.h"
@interface QRCodeTableCell()
@property (nonatomic, strong) UIImageView *codeImage;
@end

@implementation QRCodeTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    
    UIView *lineView2 = [UIView new];
    lineView2.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:lineView2];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(10);
        
    }];
    
    
    UIImageView *img = [UIImageView new];
   
    [self.contentView addSubview:img];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView2.mas_bottom).offset(24);
        make.centerX.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(120*AdapterScal, 120*AdapterScal));
    }];
    self.codeImage = img;
        
   
    UILabel *title = [UILabel new];
    title.text = @"到达维修门店后出示此二维码";
    title.textColor = UIColor333;
    title.font = LabelFont14;
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(img);
        make.top.mas_equalTo(img.mas_bottom).offset(20);
        make.bottom.mas_equalTo(self.contentView).offset(-24);
    }];
    
    

}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        [filter setDefaults];
        NSString *string = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        [filter setValue:data forKeyPath:@"inputMessage"];
        CIImage *image = [filter outputImage];// 此时的 image 是模糊的
        
        self.codeImage.image = [XJUtil createNonInterpolatedUIImageFormCIImage:image withSize:120];
    }
}

@end
