//
//  ServicesOrderDetailSectionFourCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesOrderDetailSectionFourCell.h"
@interface ServicesOrderDetailSectionFourCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;

@property (weak, nonatomic) IBOutlet UILabel *adressLbl;
@end

@implementation ServicesOrderDetailSectionFourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic isHidden:(BOOL)isHidden{
    if (dic) {
        
        self.nameLbl.text = dic[@"address"][@"name"];
        self.telLbl.text = dic[@"address"][@"mobile"];
        self.adressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",dic[@"address"][@"strProvince"],dic[@"address"][@"strCity"],dic[@"address"][@"strArea"],dic[@"address"][@"street"]];
        self.nameLbl.hidden = !isHidden;
        self.telLbl.hidden = !isHidden;
        self.adressLbl.hidden = !isHidden;
    }
}
@end
