//
//  EmptyViewForRecord.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmptyViewForRecord : UIView
+(instancetype)initView;
@property (weak, nonatomic) IBOutlet UILabel *desLbl;
@end

NS_ASSUME_NONNULL_END
