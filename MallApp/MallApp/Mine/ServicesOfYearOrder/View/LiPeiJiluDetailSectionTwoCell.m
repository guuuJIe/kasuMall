//
//  LiPeiJiluDetailSectionTwoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "LiPeiJiluDetailSectionTwoCell.h"
#import "FixOrderListImageCollectionCell.h"
#import <YBImageBrowser/YBImageBrowser.h>
@interface LiPeiJiluDetailSectionTwoCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *statuesImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *statuesLbl;
@property (strong, nonatomic) NSMutableArray *dataArray;
@end

@implementation LiPeiJiluDetailSectionTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self configCollectionView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCollectionView {
    // 如不需要长按排序效果，将LxGridViewFlowLayout类改成UICollectionViewFlowLayout即可
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    layout.itemSize = CGSizeMake(60, 60);
    layout.minimumLineSpacing = 0.0;
    layout.minimumInteritemSpacing = 10.0;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView.collectionViewLayout = layout;
    _collectionView.backgroundColor = [UIColor whiteColor];
//    _collectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4);
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _collectionView.showsHorizontalScrollIndicator = false;
    [_collectionView registerNib:[UINib nibWithNibName:@"FixOrderListImageCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"FixOrderListImageCollectionCell"];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *str = [NSString stringWithFormat:@"%@",dic[@"voucherPicture"]];
        NSArray *datas;
        if (str.length != 0) {
            datas = [str componentsSeparatedByString:@","];
        }
        [self.dataArray removeAllObjects];
       
//        NSInteger statues = [dic[@"status"] integerValue];
//        if (statues == 1) {
//            self.statuesLbl.text = @"提交中";
//        }else if(statues == 2){
//            self.statuesLbl.text = @"维修中";
//        }else if(statues == 3){
//            self.statuesLbl.text = @"维修完成";
//        }else if(statues == 4){
//            self.statuesLbl.text = @"确认完成";
//        }else if(statues == 5){
//            self.statuesLbl.text = @"已失效";
//        }
        
        NSInteger stautes = [dic[@"status"] integerValue];
        if (stautes == 1) {
            self.statuesLbl.text = @"审核中";
        }else if(stautes == 0){
            self.statuesLbl.text = @"已失效";
        }else if(stautes == 2){
            self.statuesLbl.text = @"待服务";
        }else if(stautes == 3){
            self.statuesLbl.text = @"服务中";
        }else if(stautes == 4){
            self.statuesLbl.text = @"维修完成";
        }else if(stautes == 5){
            self.statuesLbl.text = @"已完成";
        }else if(stautes == 6){
            self.statuesLbl.text = @"已失效";
        }
        
        self.timeLbl.text = dic[@"declarationTime"];
        
        
        [datas enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YBIBImageData *data = [YBIBImageData new];
             NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,datas[idx]];
            data.imageURL = URL(url);
//            data.projectiveView = [self viewAtIndex:idx];
            [self.dataArray addObject:data];
           
//            [self.dataArray addObject:url];
            
        }];
        JLog(@"%@",self.dataArray);
        [self.collectionView reloadData];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FixOrderListImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FixOrderListImageCollectionCell" forIndexPath:indexPath];
    YBIBImageData *data = self.dataArray[indexPath.row];
    

    [cell.dataImageView yy_setImageWithURL:data.imageURL placeholder:[UIImage imageNamed:@"placeholder_square"]];
    return cell;
    

}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section

{
     return 10.f;

}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section

{
 return 10.f;

}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 10, 0, 0);//分别为上、左、下、右
}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = self.dataArray;
    browser.currentPage = indexPath.item;
    //       browser.delegate = self;
    [browser show];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    
    return _dataArray;
}

@end
