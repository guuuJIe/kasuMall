//
//  ServicesOfOrderFootView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServicesOfOrderFootView : UIView


- (void)setupEwData:(NSDictionary *)dic;
- (void)setupTaocanOrderData:(NSDictionary *)dic;
- (void)setupSellerTaocanOrderData:(NSDictionary *)dic;

@property (nonatomic,copy) void(^clickBlock)(NSString *flag);
- (CGFloat)cellHeight;
- (void)setupOpeationViewWithDic:(NSDictionary *)dic andType:(OperationType)type andIsNeedShow:(BOOL)isShow;
@end

NS_ASSUME_NONNULL_END
