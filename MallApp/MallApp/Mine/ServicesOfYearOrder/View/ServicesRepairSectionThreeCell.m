//
//  ServicesRepairSectionThreeCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServicesRepairSectionThreeCell.h"
#import "TZTestCell.h"
#import "TZImagePickerController.h"
@interface ServicesRepairSectionThreeCell()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *picCollectionView;
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@end

@implementation ServicesRepairSectionThreeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self configCollectionView];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCollectionView {
    // 如不需要长按排序效果，将LxGridViewFlowLayout类改成UICollectionViewFlowLayout即可
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(56, 56);
    _picCollectionView.collectionViewLayout = layout;
    _picCollectionView.backgroundColor = [UIColor whiteColor];
    _picCollectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4);
    _picCollectionView.dataSource = self;
    _picCollectionView.delegate = self;
    _picCollectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _picCollectionView.showsHorizontalScrollIndicator = false;
    [_picCollectionView registerClass:[TZTestCell class] forCellWithReuseIdentifier:@"TZTestCell"];
}

- (void)setupCollectionDatawith:(NSMutableArray *)photos andAssets:(NSMutableArray *)assets{
    _selectedAssets = assets;
    _selectedPhotos = photos;
    [self.picCollectionView reloadData];
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_selectedPhotos.count >= 6) {
        return _selectedPhotos.count;
    }
    
    return _selectedPhotos.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TZTestCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TZTestCell" forIndexPath:indexPath];
    cell.videoImageView.hidden = YES;
    if (indexPath.item == _selectedPhotos.count) {
        cell.imageView.image = [UIImage imageNamed:@"addPic"];
        cell.deleteBtn.hidden = YES;
        cell.gifLable.hidden = YES;
    } else {
        cell.imageView.image = _selectedPhotos[indexPath.item];
        cell.asset = _selectedAssets[indexPath.item];
        cell.deleteBtn.hidden = NO;
    }

    cell.deleteBtn.tag = indexPath.item;
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == _selectedPhotos.count) {
        
        if (self.clickBlock) {
            self.clickBlock(_selectedPhotos.count);
        }
        
    }else{
        if (self.clickBlock) {
            self.clickBlock(indexPath.item);
        }
    }
}

#pragma mark - Click Event

- (void)deleteBtnClik:(UIButton *)sender {
    if ([self collectionView:self.picCollectionView numberOfItemsInSection:0] <= _selectedPhotos.count) {
        [_selectedPhotos removeObjectAtIndex:sender.tag];
        [_selectedAssets removeObjectAtIndex:sender.tag];
        [self.picCollectionView reloadData];
        return;
    }
    
    [_selectedPhotos removeObjectAtIndex:sender.tag];
    [_selectedAssets removeObjectAtIndex:sender.tag];
    [_picCollectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:0];
        [self->_picCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [self->_picCollectionView reloadData];
    }];
}

- (void)pushTZImagePickerController {
 
  
}

@end
