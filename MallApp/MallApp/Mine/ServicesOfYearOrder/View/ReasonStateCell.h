//
//  ReasonStateCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReasonStateCell : UITableViewCell
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UITextView *textView;
@end

NS_ASSUME_NONNULL_END
