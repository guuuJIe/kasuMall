//
//  ChangePwdVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ChangePwdVC.h"
#import "MineApi.h"
#import "LoginVC.h"
@interface ChangePwdVC ()
@property (nonatomic, strong)MineApi *api;
@end

@implementation ChangePwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"设置新密码";
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (IBAction)submit:(id)sender {
//    if (![XJUtil isValidPassword:self.pwdText.text]) {
//        [JMBManager showBriefAlert:@"请输入正确格式的密码"];
//        return;
//    }
    
    if (![self.pwdText.text isEqualToString:self.confirmPwdText.text]) {
        [JMBManager showBriefAlert:@"密码不一致"];
        return;
    }
    NSDictionary *dic = @{@"userId":usersID,@"password":self.confirmPwdText.text};
    [self.api resetPasswordWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [JMBManager showBriefAlert:@"修改成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:true];
            });
            
//            LoginVC *vc = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//            BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//            vc2.modalPresentationStyle = 0;
//            [self presentViewController:vc2 animated:true completion:nil];
//            [self.navigationController pushViewController:vc animated:true];
        }
    }];
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}
@end
