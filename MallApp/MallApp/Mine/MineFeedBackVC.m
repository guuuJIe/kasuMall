//
//  MineFeedBackVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineFeedBackVC.h"
#import "MineApi.h"
#import "MineFeedBackListTableCell.h"
@interface MineFeedBackVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) MineApi *api;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@end

@implementation MineFeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的反馈";
    [self setupUI];
    [self getData];
}

- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)getData{
    self.num = 1;
    self.size = 20;
    [self.api getFeedBackListWithparameters:@{} withPageNum:self.num andPageSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArray = result.result;
            [self.listTableView reloadData];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return self.dataArray.count;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"MineFeedBackListTableCell";
    MineFeedBackListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[MineFeedBackListTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
    [cell setupData:self.dataArray[indexPath.section]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *head = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    head.backgroundColor = UIColorEF;
    return head;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}



- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;

        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
     
        [_listTableView registerNib:[UINib nibWithNibName:@"MineFeedBackListTableCell" bundle:nil] forCellReuseIdentifier:@"MineFeedBackListTableCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorEF;

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
