//
//  AddMineCardVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddMineCardVC.h"
#import "MineApi.h"
@interface AddMineCardVC ()
@property (nonatomic) MineApi *api;
@end

@implementation AddMineCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"添加银行卡";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit:(id)sender {
    NSDictionary *dic = @{
        @"CardType" :@1,
        @"Yname" :self.nameLbl.text,
        @"Bank" :self.banktype.text,
        @"BankNumber" :self.bankNum.text,
    };
    WeakSelf(self)
    [self.api addMyCardWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self);
        if (result.code == 200) {
            [self.navigationController popViewControllerAnimated:true];
        }
    }];
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}
@end
