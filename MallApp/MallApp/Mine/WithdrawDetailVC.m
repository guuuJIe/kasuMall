//
//  WithdrawDetailVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WithdrawDetailVC.h"

@interface WithdrawDetailVC ()

@end

@implementation WithdrawDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"提现详情";
    [self setUpData];
}

- (void)setUpData{
    self.identifyLbl.text = self.data[@"userName"];
    NSString *bank = [NSString stringWithFormat:@"%@",self.data[@"bankInfo"]];
    self.bankLbl.text = [NSString stringWithFormat:@"尾号%@",[bank substringFromIndex:bank.length - 4]];
    self.moneyLbl.text = [NSString stringWithFormat:@"¥%@",self.data[@"amount"]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
