//
//  MinePayVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MinePayVC : BaseViewController
@property (nonatomic) NSString *money;
@property (nonatomic) NSArray *orderData;
@end

NS_ASSUME_NONNULL_END
