//
//  MyProjectVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyProjectVC.h"
#import "MinePromptView.h"
#import "MineProjectItemCell.h"
#import "CommunityProjectListVC.h"
#import "MinePromptProductInfoVC.h"
#import "BussinessMangeApi.h"
#import "NoDataView.h"
@interface MyProjectVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>
@property (nonatomic, strong) MinePromptView *promptView;
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSMutableArray *datasArr;
@property (nonatomic, strong) BussinessMangeApi *managers;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, strong) NSString *size;
@end

@implementation MyProjectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupProp];
    [self setupUI];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    UIButton *btn = [UIButton new];
    [btn setTitleColor:APPColor forState:0];
    [btn.titleLabel setFont:LabelFont14];
    [btn setTitle:@"添加项目" forState:0];
    [btn addTarget:self action:@selector(clickAct) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self setupData:RefreshTypeDown];
}

- (void)setupProp{
 
    self.num = 1;
    self.size = @"20";
   
}

- (void)setupUI{
    self.title = @"我的项目";
//    [self.view addSubview:self.promptView];
//    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(1);
//        make.left.right.mas_equalTo(self.view);
//        make.height.mas_equalTo(44);
//    }];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    [self.managers userProductListWithparameters:nil andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
           
            self.datasArr = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datasArr withNum:self.num];
            [self.listTableView reloadData];
            [self.listTableView reloadEmptyDataSet];
        }
    }];
}

- (void)clickAct{
    CommunityProjectListVC *vc = [CommunityProjectListVC new];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark ---UITableViewDelegate----
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datasArr.count;
//    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"MineProjectItemCell";
    MineProjectItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[MineProjectItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datasArr[indexPath.row]];
    WeakSelf(self)
    cell.clickBlock = ^{
        StrongSelf(self)
        [self delPro:self.datasArr[indexPath.row]];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.datasArr[indexPath.row];
    MinePromptProductInfoVC *vc = [MinePromptProductInfoVC new];
    vc.pids = [NSString stringWithFormat:@"%@",dic[@"id"]];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)delPro:(NSDictionary *)dic{
    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否删除该项目" andHandler:^(NSInteger index) {
        if (index == 1) {
            [self.managers delProductWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [self setupData:RefreshTypeDown];
                }
            }];
        }
    }];
}


#pragma mark ---EmptyDelegate----
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NoDataView *view = [NoDataView initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-75, 150, 150);

    return view;
}

- (MinePromptView *)promptView{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        _promptView.dataSource = @[@"进行中",@"已过期"];
    }
    
    return _promptView;
}


-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        [_listTableView setEmptyDataSetSource:self];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[MineProjectItemCell class] forCellReuseIdentifier:@"MineProjectItemCell"];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)datasArr{
    if (!_datasArr) {
        _datasArr = [NSMutableArray array];
    }
    return _datasArr;
}

- (BussinessMangeApi *)managers{
    if (!_managers) {
        _managers = [BussinessMangeApi new];
    }
    
    return _managers;
}
@end
