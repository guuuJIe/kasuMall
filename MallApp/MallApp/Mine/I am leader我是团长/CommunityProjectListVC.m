//
//  CommunityProjectListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CommunityProjectListVC.h"
#import "CommunityGoodsItemCell.h"
#import "BussinessMangeApi.h"
@interface CommunityProjectListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) BussinessMangeApi *managers;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) NSMutableArray *datasArr;
@end

@implementation CommunityProjectListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupProp];
    [self setupUI];
    
    [self setupData:RefreshTypeDown];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)setupUI{
    self.title = @"项目列表";
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)setupProp{
 
    self.num = 1;
    self.size = @"20";
   
}


- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    [self.managers systemProductListWithparameters:nil andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
           
            self.datasArr = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datasArr withNum:self.num];
            [self.listTableView reloadData];
//            [self.listTableView reloadEmptyDataSet];
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    return self.datasArr.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"CommunityGoodsItemCell";
    CommunityGoodsItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[CommunityGoodsItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datasArr[indexPath.row]];
    WeakSelf(self)
    cell.clickBlock = ^{
        StrongSelf(self)
        NSDictionary *dic = self.datasArr[indexPath.row];
        
        [self addPro:dic];
    };
    return cell;
}

- (void)addPro:(NSDictionary *)dic{
    NSDictionary *params = @{@"Title":[XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@""],@"ProType":[XJUtil insertStringWithNotNullObject:dic[@"proType"] andDefailtInfo:@""],@"Pid":dic[@"pid"]};
    [self.managers addProductWithparameters:params withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [JMBManager showBriefAlert:@"添加成功"];
        }
    }];
}


-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[CommunityGoodsItemCell class] forCellReuseIdentifier:@"CommunityGoodsItemCell"];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (BussinessMangeApi *)managers{
    if (!_managers) {
        _managers = [BussinessMangeApi new];
    }
    
    return _managers;
}
@end
