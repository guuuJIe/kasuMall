//
//  InviteFriendVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InviteFriendVC.h"
#import "InviteFriendSectionOneCell.h"
#import "InviteFriendSectionTwoCell.h"
#import "InviteHeadView.h"
#import "InviteFootView.h"
#import "InviteRecordItemCell.h"
#import "BussinessMangeApi.h"
#import "ShareToFriendVC.h"
@interface InviteFriendVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSMutableArray *datasArr;
@property (nonatomic, strong) BussinessMangeApi *managers;
@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, strong) NSArray *inviters;
@end

@implementation InviteFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"ScanVC"]];
    [self setupUI];
    [self setupData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:UIColorFFDA] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)setupUI{
    self.title = @"邀请好友";
    self.view.backgroundColor = UIColorFFDA;
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)setupData{
    [self.managers getGrouperInviteListWithparameters:nil withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dataDic = datas.firstObject;
            self.inviters = self.dataDic[@"inviters"];
            [self.listTableView reloadData];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 2) {
        return self.inviters.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"InviteFriendSectionOneCell";
        InviteFriendSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InviteFriendSectionOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        WeakSelf(self)
        cell.clickBlock = ^{
            StrongSelf(self)
            ShareToFriendVC *vc = [ShareToFriendVC new];
            [self.navigationController pushViewController:vc animated:true];
        };
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"InviteFriendSectionTwoCell";
        InviteFriendSectionTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InviteFriendSectionTwoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"InviteRecordItemCell";
        InviteRecordItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InviteRecordItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        return cell;
    }
    
    return nil;
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 2) {
        InviteHeadView *headView = [InviteHeadView new];
        return headView;
    }
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 2) {
        return 60;
    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 2) {
        InviteFootView *headView = [InviteFootView new];
        return headView;
    }
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return 20;
    }
    return CGFLOAT_MIN;
}



-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[InviteFriendSectionOneCell class] forCellReuseIdentifier:@"InviteFriendSectionOneCell"];
        [_listTableView registerClass:[InviteFriendSectionTwoCell class] forCellReuseIdentifier:@"InviteFriendSectionTwoCell"];
        [_listTableView registerClass:[InviteRecordItemCell class] forCellReuseIdentifier:@"InviteRecordItemCell"];
         _listTableView.backgroundColor = UIColorFFDA;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)datasArr{
    if (!_datasArr) {
        _datasArr = [NSMutableArray array];
    }
    return _datasArr;
}

- (BussinessMangeApi *)managers{
    if (!_managers) {
        _managers = [BussinessMangeApi new];
    }
    
    return _managers;
}

@end
