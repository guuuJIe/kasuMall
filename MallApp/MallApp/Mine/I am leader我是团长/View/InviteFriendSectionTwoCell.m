//
//  InviteFriendSectionTwoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InviteFriendSectionTwoCell.h"
@interface InviteFriendSectionTwoCell()
@property (nonatomic, strong) UILabel *numLbl;
@property (nonatomic, strong) UILabel *priceLbl;
@end

@implementation InviteFriendSectionTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    self.contentView.backgroundColor = UIColorFFDA;
    
    UIImageView *headImage = [UIImageView new];
    headImage.image = [UIImage imageNamed:@"M_Invite_1"];
    [self.contentView addSubview:headImage];
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.centerX.mas_equalTo(self.contentView);
    }];
    
    
    UIView *bgview = [UIView new];
    [self.contentView addSubview:bgview];
    bgview.backgroundColor = [UIColor colorWithHexString:@"FFFEE3"];
    bgview.layer.cornerRadius = 10;
    [bgview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(headImage.mas_bottom).offset(14);
        make.height.mas_equalTo(132);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    [bgview addSubview:self.numLbl];
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(54);
        make.top.mas_equalTo(30);
    }];
    
    UILabel *title = [UILabel new];
    title.text = @"邀请团长";
    title.textColor = [UIColor colorWithHexString:@"5C5C5C"];
    title.font = LabelFont12;
    [bgview addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.numLbl);
        make.top.mas_equalTo(self.numLbl.mas_bottom).offset(12);
    }];
    
    [bgview addSubview:self.priceLbl];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-54);
        make.top.mas_equalTo(30);
    }];
    
    UILabel *title2 = [UILabel new];
    title2.text = @"获得奖励(元)";
    title2.textColor = [UIColor colorWithHexString:@"5C5C5C"];
    title2.font = LabelFont12;
    [bgview addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.priceLbl);
        make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(12);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.numLbl.text = [NSString stringWithFormat:@"%@",dic[@"counts"]];
        self.priceLbl.text = [NSString stringWithFormat:@"%@",dic[@"price"]];
    }
}

- (UILabel *)numLbl{
    if (!_numLbl) {
        _numLbl = [UILabel new];
        _numLbl.font = [UIFont systemFontOfSize:45 weight:UIFontWeightBold];
        _numLbl.textColor = UIColor333;
        _numLbl.text = @"0";
//        [self.contentView addSubview:_numLbl];
    }
    
    return _numLbl;
}

- (UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont systemFontOfSize:45 weight:UIFontWeightBold];
        _priceLbl.textColor = [UIColor colorWithHexString:@"F83500"];
        _priceLbl.text = @"0";
    }
    
    return _priceLbl;
}
@end
