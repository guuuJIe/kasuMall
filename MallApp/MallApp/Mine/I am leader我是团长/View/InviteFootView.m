//
//  InviteFootView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InviteFootView.h"

@implementation InviteFootView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = UIColorFFDA;
        
        UIView *headview = [[UIView alloc] initWithFrame:CGRectMake(15, 0, Screen_Width - 30, 20)];
        headview.backgroundColor = UIColor.whiteColor;
        [self addSubview:headview];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headview.bounds byRoundingCorners:UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii:CGSizeMake(10, 10)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = headview.bounds;
        maskLayer.path = maskPath.CGPath;
        headview.layer.mask = maskLayer;
        
    }
    
    return self;
}


@end
