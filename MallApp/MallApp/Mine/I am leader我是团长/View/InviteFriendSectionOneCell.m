//
//  InviteFriendSectionOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InviteFriendSectionOneCell.h"
@interface InviteFriendSectionOneCell()
@property (nonatomic, strong) UIImageView *btnBg;
@property (nonatomic, strong) UILabel *titleLbl;
@end

@implementation InviteFriendSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"communityBg"];
    [self.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
    
    [self.btnBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(- 20);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
    }];
    
    [self.btnBg addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.btnBg);
    }];
}

- (void)shareAct{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (UIImageView *)btnBg{
    if (!_btnBg) {
        _btnBg = [UIImageView new];
        _btnBg.image = [UIImage imageNamed:@"btnBg"];
        _btnBg.userInteractionEnabled = true;
        [_btnBg addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shareAct)]];
        [self.contentView addSubview:_btnBg];
    }
    
    return _btnBg;
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont systemFontOfSize:17 weight:UIFontWeightBold];
        _titleLbl.textColor = [UIColor colorWithHexString:@"FFFBE5"];
        _titleLbl.text = @"立即分享";
    }
    
    return _titleLbl;
}
@end
