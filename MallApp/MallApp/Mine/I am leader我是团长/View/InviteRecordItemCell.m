//
//  InviteRecordItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InviteRecordItemCell.h"
@interface InviteRecordItemCell()
@property (nonatomic, strong) UILabel *timeLbl;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *moneyeLbl;
@end

@implementation InviteRecordItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    
    return self;
}

- (void)setUpUI{
    self.contentView.backgroundColor = UIColorFFDA;
    
    UIView *view = [UIView new];
    view.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    [view addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
//        make.centerY.mas_equalTo(self.contentView);
//        make.top.mas_equalTo(25);
        make.bottom.mas_equalTo(view);
    }];
    
    [self.contentView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(view);
        make.centerY.mas_equalTo(self.timeLbl);
    }];
    
    [self.contentView addSubview:self.moneyeLbl];
    [self.moneyeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(view).offset(-20);
        make.centerY.mas_equalTo(self.timeLbl);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *timeStr =  [NSString stringWithFormat:@"%@",dic[@"createdTime"]];
        self.timeLbl.text = [NSString stringWithFormat:@"%@",[timeStr substringToIndex:10]];
        self.nameLbl.text = dic[@"userName"];
        self.moneyeLbl.text = [NSString stringWithFormat:@"获得%@元",dic[@"price"]];
    }
}

- (UILabel *)timeLbl{
    if (!_timeLbl) {
        _timeLbl = [UILabel new];
        _timeLbl.font = LabelFont12;
        _timeLbl.text = @"2019-05";
        _timeLbl.textColor = [UIColor colorWithHexString:@"7C7C7C"];
    }
    
    return _timeLbl;
}

- (UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [UILabel new];
        _nameLbl.font = LabelFont12;
        _nameLbl.text = @"王大宝";
        _nameLbl.textColor = [UIColor colorWithHexString:@"7C7C7C"];
    }
    
    return _nameLbl;
}

- (UILabel *)moneyeLbl{
    if (!_moneyeLbl) {
        _moneyeLbl = [UILabel new];
        _moneyeLbl.font = LabelFont14;
        _moneyeLbl.text = @"获得5元";
        _moneyeLbl.textColor = [UIColor colorWithHexString:@"7A4901"];
    }
    
    return _moneyeLbl;
}
@end
