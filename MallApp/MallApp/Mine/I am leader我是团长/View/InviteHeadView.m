//
//  InviteHeadView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InviteHeadView.h"

@implementation InviteHeadView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = UIColorFFDA;
        
        UIView *headview = [[UIView alloc] initWithFrame:CGRectMake(15, 12, Screen_Width - 30, 48)];
        headview.backgroundColor = [UIColor colorWithHexString:@"FEEBC3"];
        [self addSubview:headview];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headview.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerTopLeft cornerRadii:CGSizeMake(10, 10)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = headview.bounds;
        maskLayer.path = maskPath.CGPath;
        headview.layer.mask = maskLayer;
        UILabel *title = [UILabel new];
        title.text = @"邀请记录";
        title.textColor = [UIColor colorWithHexString:@"E02A21"];
        title.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
        [headview addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(25);
            make.centerY.mas_equalTo(headview);
        }];
    }
    
    return self;
}

@end
