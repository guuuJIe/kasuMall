//
//  LeaderDelegateInfoCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/28.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "LeaderDelegateInfoCell.h"
@interface LeaderDelegateInfoCell()

@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@end

@implementation LeaderDelegateInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
       
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"brokerage"]];
       
        
    }
}
@end
