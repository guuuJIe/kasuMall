//
//  PerformaceItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PerformaceItemCell.h"
@interface PerformaceItemCell()
@property (nonatomic, strong) UIImageView *goodImage;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *orderNumLbl;
@property (nonatomic, strong) UILabel *statuesLbl;
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) UILabel *comissionLbl;
@property (nonatomic, strong) UILabel *timeLbl;
@end

@implementation PerformaceItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    self.contentView.backgroundColor = UIColorF5F7;
    UIView *bgView = [UIView new];
    bgView.backgroundColor = UIColor.whiteColor;
    bgView.layer.cornerRadius = 10;
    [self.contentView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(162);
    }];
    
    [bgView addSubview:self.goodImage];
    [self.goodImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(16);
        make.size.mas_equalTo(CGSizeMake(64, 64));
    }];
    
    [bgView addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.goodImage.mas_right).offset(8);
        make.right.mas_equalTo(-8);
        make.top.mas_equalTo(self.goodImage);
    }];
    
    [bgView addSubview:self.orderNumLbl];
    [self.orderNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLbl);
        make.right.mas_equalTo(self.titleLbl);
        make.top.mas_equalTo(self.titleLbl.mas_bottom).offset(6);
    }];
    
    [bgView addSubview:self.statuesLbl];
    [self.statuesLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLbl);
        make.size.mas_equalTo(CGSizeMake(60, 20));
        make.top.mas_equalTo(self.orderNumLbl.mas_bottom).offset(6);
    }];
    
    [bgView addSubview:self.priceLbl];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.goodImage);
        
        make.top.mas_equalTo(self.goodImage.mas_bottom).offset(15);
    }];
    
    [bgView addSubview:self.comissionLbl];
    [self.comissionLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        
        make.top.mas_equalTo(self.priceLbl);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [bgView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(bgView);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(16);
    }];
    
    [bgView addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceLbl);
        make.top.mas_equalTo(line.mas_bottom).offset(8);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"productPic"]];
        [self.goodImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = dic[@"productTitle"];
        self.orderNumLbl.text = [NSString stringWithFormat:@"订单编号%@",dic[@"orderNumber"]];
        self.statuesLbl.text = dic[@"orderStatusString"];
        self.priceLbl.text = [NSString stringWithFormat:@"订单金额 ¥%@",dic[@"orderPrice"]];
        self.comissionLbl.text = [NSString stringWithFormat:@"所得佣金 ¥%@",dic[@"orderAmountPrice"]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:self.comissionLbl.text];
        [attrString addAttribute:NSForegroundColorAttributeName value:UIColor90 range:[self.comissionLbl.text rangeOfString:@"所得佣金"]];
        self.comissionLbl.attributedText = attrString;
        NSMutableAttributedString *priceAttrString = [[NSMutableAttributedString alloc] initWithString:self.priceLbl.text];
        [priceAttrString addAttribute:NSForegroundColorAttributeName value:UIColor90 range:[self.priceLbl.text rangeOfString:@"订单金额"]];
        self.priceLbl.attributedText = priceAttrString;
        NSString *timeStr = [NSString stringWithFormat:@"%@",dic[@"orderCreated"]];
        self.timeLbl.text = [NSString stringWithFormat:@"%@创建",[timeStr substringToIndex:10]];
        NSInteger staues = [dic[@"orderStatus"] integerValue];
        if (staues == 3) {
            self.statuesLbl.backgroundColor = UIColorFFA1;
        }else if (staues == 33){
             self.statuesLbl.backgroundColor = UIColorBF;
        }else{
            self.statuesLbl.backgroundColor = APPColor;
        }
    }
}

- (UIImageView *)goodImage{
    if (!_goodImage) {
        _goodImage = [UIImageView new];
    }
    return _goodImage;
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
        _titleLbl.textColor = UIColor333;
        _titleLbl.text = @"标题";
    }
    
    return _titleLbl;
}

- (UILabel *)orderNumLbl{
    if (!_orderNumLbl) {
        _orderNumLbl = [UILabel new];
        _orderNumLbl.font = LabelFont12;
        _orderNumLbl.textColor = UIColor90;
        _orderNumLbl.text = @"订单编号:57584368119471";
    }
    
    return _orderNumLbl;
}

- (UILabel *)statuesLbl{
    if (!_statuesLbl) {
        _statuesLbl = [UILabel new];
        _statuesLbl.font = LabelFont12;
        _statuesLbl.textColor = UIColor.whiteColor;
        _statuesLbl.text = @"未支付";
        _statuesLbl.textAlignment = NSTextAlignmentCenter;
        _statuesLbl.layer.cornerRadius = 10;
        _statuesLbl.layer.masksToBounds = true;
        _statuesLbl.backgroundColor = [UIColor colorWithHexString:@"#BFC2CC"];
    }
    
    return _statuesLbl;
}

- (UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = LabelFont14;
        _priceLbl.textColor = UIColor333;
        _priceLbl.text = @"订单金额¥5000";
    }
    
    return _priceLbl;
}

- (UILabel *)comissionLbl{
    if (!_comissionLbl) {
        _comissionLbl = [UILabel new];
        _comissionLbl.font = LabelFont14;
        _comissionLbl.textColor = UIColorFF3E;
        _comissionLbl.text = @"所得佣金¥20";
    }
    
    return _comissionLbl;
}

- (UILabel *)timeLbl{
    if (!_timeLbl) {
        _timeLbl = [UILabel new];
        _timeLbl.font = LabelFont12;
        _timeLbl.textColor = UIColor90;
        _timeLbl.text = @"2019-08-01";
    }
    
    return _timeLbl;
}

@end
