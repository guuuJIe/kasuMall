//
//  MineProjectItemCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineProjectItemCell.h"
@interface MineProjectItemCell()
@property (nonatomic, strong) UIImageView *goodImage;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UILabel *priceLbl;
@property (nonatomic, strong) UILabel *comissionLbl;
@property (nonatomic, strong) UIButton *addBtn;
@property (nonatomic, strong) UIButton *delBtn;
@end

@implementation MineProjectItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    self.contentView.backgroundColor = UIColorF5F7;
    UIView *view = [UIView new];
    view.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(88);
        make.top.mas_equalTo(12);
        make.bottom.mas_equalTo(0);
    }];
    
    [view addSubview:self.goodImage];
    [self.goodImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(view);
        make.size.mas_equalTo(CGSizeMake(64, 64));
    }];
    
    [view addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.goodImage.mas_right).offset(8);
        make.top.mas_equalTo(self.goodImage);
        make.right.mas_equalTo(-12);
    }];
    
    [view addSubview:self.priceLbl];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLbl);
        make.bottom.mas_equalTo(self.goodImage).offset(-3);
    }];
    
    [view addSubview:self.comissionLbl];
    [self.comissionLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceLbl.mas_right).offset(4);
        make.bottom.mas_equalTo(self.goodImage).offset(-3);
    }];
    
//    [view addSubview:self.addBtn];
//    [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-12);
//        make.centerY.mas_equalTo(self.comissionLbl);
//        make.size.mas_equalTo(CGSizeMake(66, 24));
//    }];
    
    [view addSubview:self.delBtn];
    [self.delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(self.comissionLbl);
//        make.size.mas_equalTo(CGSizeMake(66, 24));
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"proPic"]];
        [self.goodImage yy_setImageWithURL:URL(url) options:0];
        self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"title"] andDefailtInfo:@""];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",[XJUtil insertStringWithNotNullObject:dic[@"price"] andDefailtInfo:@""]];
        NSInteger discountWay = [dic[@"discountway"] integerValue];
        if (discountWay == 1) {//比例佣金
            NSString *str = @"%";
            self.comissionLbl.text = [NSString stringWithFormat:@"佣金方式:比例%@%@",dic[@"comdiscount"],str];
            self.comissionLbl.hidden = false;
        }else if (discountWay == 0){
            NSString *str = @"%";
            self.comissionLbl.text = [NSString stringWithFormat:@"佣金方式:比例%@%@",dic[@"comdiscount"],str];
            self.comissionLbl.hidden = false;
        }else if(discountWay == 2){//固定佣金
            self.comissionLbl.text = [NSString stringWithFormat:@"佣金方式:固定¥%@",dic[@"comdiscount"]];
            self.comissionLbl.hidden = false;
        }else{
            self.comissionLbl.hidden = true;
        }
        
        
        //        self.comissionLbl.text = [NSString stringWithFormat:@"佣金¥%@",dic[@"comdiscount"]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:self.comissionLbl.text];
        //        [attrString addAttribute:NSFontAttributeName value:LabelFont9 range:[_comissionLbl.text rangeOfString:@"佣金方式"]];
        [attrString addAttribute:NSForegroundColorAttributeName value:UIColor66 range:NSMakeRange(0, 7)];
        self.comissionLbl.attributedText = attrString;
    }
}

- (void)delAct{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (UIImageView *)goodImage{
    if (!_goodImage) {
        _goodImage = [UIImageView new];
    }
    
    return _goodImage;
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.textColor = UIColor333;
        _titleLbl.font = LabelFont14;
        
    }
    
    return _titleLbl;
}

- (UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.textColor = UIColor333;
        _priceLbl.font = LabelFont14;
        _priceLbl.text = @"¥111";
    }
    
    return _priceLbl;
}

- (UILabel *)comissionLbl{
    if (!_comissionLbl) {
        _comissionLbl = [UILabel new];
        _comissionLbl.textColor = UIColorFF3E;
        _comissionLbl.font = LabelFont9;
        _comissionLbl.backgroundColor = [UIColor colorWithHexString:@"FFF6D8"];
        _comissionLbl.text = @"佣金¥13.33";
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:_comissionLbl.text];
        [attrString addAttribute:NSFontAttributeName value:LabelFont9 range:[_comissionLbl.text rangeOfString:@"佣金"]];
        [attrString addAttribute:NSForegroundColorAttributeName value:UIColor66 range:[_comissionLbl.text rangeOfString:@"佣金"]];
        _comissionLbl.attributedText = attrString;
    }
    
    return _comissionLbl;
}

- (UIButton *)addBtn{
    if (!_addBtn) {
        _addBtn = [UIButton new];
        [_addBtn setTitle:@"去分享" forState:0];
        _addBtn.layer.cornerRadius = 12;
        _addBtn.layer.borderColor = APPColor.CGColor;
        _addBtn.layer.borderWidth = 1;
        [_addBtn.titleLabel setFont:LabelFont12];
        _addBtn.backgroundColor = APPColor;
        [_addBtn setTitleColor:UIColor.whiteColor forState:0];
    }
    
    return _addBtn;
}

- (UIButton *)delBtn{
    if (!_delBtn) {
        _delBtn = [UIButton new];
        [_delBtn setTitleColor:UIColor66 forState:0];
        [_delBtn.titleLabel setFont:LabelFont12];
        [_delBtn setTitle:@"删除" forState:0];
        [_delBtn addTarget:self action:@selector(delAct) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _delBtn;
}
@end
