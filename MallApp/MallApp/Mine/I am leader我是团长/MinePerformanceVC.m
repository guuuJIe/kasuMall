//
//  MinePerformanceVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MinePerformanceVC.h"
#import "MinePromptView.h"
#import "PerformaceItemCell.h"
#import "BussinessMangeApi.h"
@interface MinePerformanceVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) MinePromptView *promptView;
@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) NSInteger statues;
@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) BussinessMangeApi *manager;
@property (nonatomic, strong) NSMutableArray *datasArr;
@end

@implementation MinePerformanceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupProp];
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)setupUI{
    self.title = @"我的业绩";
    [self.view addSubview:self.promptView];
    [self.promptView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(44);
        make.left.right.top.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.promptView.mas_bottom).offset(0);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)setupProp{
    self.promptView.orderStatues = 200;
    self.num = 1;
    self.size = @"20";
    self.statues = 0;
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    [self.manager getProjectOrderListWithparameters:[NSString stringWithFormat:@"%ld",(long)self.statues] andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
           
            self.datasArr = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.datasArr withNum:self.num];
            [self.listTableView reloadData];
            [self.listTableView reloadEmptyDataSet];
        }
    }];
}

#pragma mark ---UITableViewDelegate----

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datasArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"PerformaceItemCell";
    PerformaceItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[PerformaceItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.datasArr[indexPath.row]];
    return cell;
}


- (MinePromptView *)promptView{
    if (!_promptView) {
        _promptView = [MinePromptView new];
        _promptView.dataSource = @[@"全部",@"进行中",@"已完成"];
        WeakSelf(self)
        _promptView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            
            self.statues = tag - 200;
            [self.listTableView.mj_header beginRefreshing];
            
        };
    }
    
    return _promptView;
}

-(UITableView *)listTableView{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
       
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerClass:[PerformaceItemCell class] forCellReuseIdentifier:@"PerformaceItemCell"];
        WeakSelf(self);
        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (BussinessMangeApi *)manager{
    if (!_manager) {
        _manager = [BussinessMangeApi new];
    }
    
    return _manager;
}
@end
