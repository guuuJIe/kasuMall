//
//  ShareToFriendVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/27.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShareToFriendVC.h"
#import "BussinessMangeApi.h"
#import "PurchaseView.h"
#import "ShareView.h"
#import "PayMannagerUtil.h"
@interface ShareToFriendVC ()
@property (nonatomic, strong) BussinessMangeApi *managers;
@property (nonatomic, strong) PurchaseView *purchaseView;
@property (nonatomic, strong) UIImageView *codeImage;
@property (nonatomic, strong) NSDictionary *dic;
@end

@implementation ShareToFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self setupData];
}

- (void)setupUI{
    self.title = @"邀请好友";
    
    UIView *view = [UIView new];
    view.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(350);
    }];
    
    UIImageView *img = [UIImageView new];
    [view addSubview:img];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(211*AdapterScal, 211*AdapterScal));
        make.center.mas_equalTo(view);
    }];
    self.codeImage = img;
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setupData{
    [self.managers getInviteInfoWithparameters:nil withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dic = datas.firstObject;
            [self configData:datas.firstObject];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    NSString *string = [NSString stringWithFormat:@"%@",dic[@"wapdata"]];
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [filter setValue:data forKeyPath:@"inputMessage"];
    CIImage *image = [filter outputImage];// 此时的 image 是模糊的
    
    self.codeImage.image = [XJUtil createNonInterpolatedUIImageFormCIImage:image withSize:200];
}

- (BussinessMangeApi *)managers{
    if (!_managers) {
        _managers = [BussinessMangeApi new];
    }
    
    return _managers;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"立即分享" forState:0];
        WeakSelf(self)
        _purchaseView.addAddressBlock = ^{
            StrongSelf(self)
            
            ShareView *shareview = [ShareView new];
            [shareview show];
            WeakSelf(shareview)
            shareview.clickBlock = ^(NSInteger index) {
                [[PayMannagerUtil sharedManager] wxShareForInviterWithDic:weakself.dic successBlock:^(BOOL success) {
                    [weakshareview hide];
                } failBlock:^(BOOL fail) {
                    
                } InSecen:index];
                
            };
            
        };
    }
    
    return _purchaseView;
}
@end
