//
//  ToBeLeaderVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ToBeLeaderVC.h"
#import "SubmitMyInfoPopView.h"
#import "BussinessMangeApi.h"
@interface ToBeLeaderVC ()
@property (nonatomic, strong) UIImageView *btnBg;
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) SubmitMyInfoPopView *popView;
@property (nonatomic, strong) BussinessMangeApi *manager;
@end

@implementation ToBeLeaderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:UIColorFFDA] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

- (void)setupUI{
    self.title = @"申请团长";
    
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"communityBg"];
//    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.btnBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-BottomAreaHeight - 20);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
    }];
    
    [self.btnBg addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.btnBg);
    }];
    
}

- (void)applyAct{
    [self.popView show];

    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSInteger ids = [clerkid integerValue];
    if (ids) {
        [dic setValue:@(ids) forKey:@"ClerkId"];//代理id
    }
    WeakSelf(self)
    self.popView.clickBlock = ^(NSString * _Nonnull name, NSString * _Nonnull sex) {
      StrongSelf(self)
        [dic setValue:name forKey:@"RealName"];
        [dic setValue:sex forKey:@"Sex"];
        [self.manager ApplyGrouperWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                [JMBManager showBriefAlert:@"申请成功"];
                if (self.clickBlock) {
                    self.clickBlock();
                }
                 [self.popView hide];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:true];
                });
                
               
            }
        }];
    };
}
- (UIImageView *)btnBg{
    if (!_btnBg) {
        _btnBg = [UIImageView new];
        _btnBg.image = [UIImage imageNamed:@"btnBg"];
        _btnBg.userInteractionEnabled = true;
        [_btnBg addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(applyAct)]];
        [self.view addSubview:_btnBg];
    }
    
    return _btnBg;
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont systemFontOfSize:17 weight:UIFontWeightBold];
        _titleLbl.textColor = [UIColor colorWithHexString:@"FFFBE5"];
        _titleLbl.text = @"申请团长";
    }
    
    return _titleLbl;
}

- (SubmitMyInfoPopView *)popView{
    if (!_popView) {
        _popView = [SubmitMyInfoPopView new];
    }
    return _popView;
}

- (BussinessMangeApi *)manager{
    if (!_manager) {
        _manager = [BussinessMangeApi new];
    }
    
    return _manager;
}
@end
