//
//  MinePayOrderDetailVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MinePayOrderDetailVC.h"

@interface MinePayOrderDetailVC ()

@end

@implementation MinePayOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"账单详情";
    [self setData];
}

- (void)setData{
    self.titleLbl.text = self.dic[@"flowType"];
    self.moneyLbl.text = [NSString stringWithFormat:@"%@",self.dic[@"flowAmount"]];
    self.sumitMoneyLbl.text = self.dic[@"orderType"];
    self.orderNumLbl.text = self.dic[@"tradeNo"];
    self.exchangeNumLbl.text = self.dic[@"orderNumber"];
    self.timeLbl.text = self.dic[@"createDate"];
    self.remarkLbl.text = self.dic[@"remark"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
