//
//  BankCardVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BankCardVC.h"
#import "BankCardTableViewCell.h"
#import "AddCardFootView.h"
#import "AddMineCardVC.h"
#import "MineApi.h"
@interface BankCardVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *listTableView;

@property (nonatomic) MineApi *api;
@property (nonatomic) NSMutableArray *dataArray;
@end

@implementation BankCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"银行卡";
    [self setupUI];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getBankListData];
}
- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"BankCardTableViewCell";
    BankCardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[BankCardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    cell.clickBlock = ^{
        NSDictionary *dic = self.dataArray[indexPath.row];
        [self.api delMyCardWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                [self getBankListData];
            }
        }];
    };
    [cell setupData:self.dataArray[indexPath.row]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    AddCardFootView *FootView = [[AddCardFootView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 24, 45)];
    FootView.addBlock = ^{
        AddMineCardVC *vc = [[AddMineCardVC alloc] initWithNibName:@"AddMineCardVC" bundle:nil];
        [self.navigationController pushViewController:vc animated:true];
    };
//    FootView.backgroundColor = [UIColor whiteColor];
    return FootView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.itemBlock) {
        self.itemBlock(self.dataArray[indexPath.row]);
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (void)getBankListData{
    [self.api getBankListWithparameters:@"1" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArray = result.result;
            [self.listTableView reloadData];
        }
    }];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;

        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
     
        [_listTableView registerNib:[UINib nibWithNibName:@"BankCardTableViewCell" bundle:nil] forCellReuseIdentifier:@"BankCardTableViewCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorEF;

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
