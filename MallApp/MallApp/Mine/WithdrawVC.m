//
//  WithdrawVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WithdrawVC.h"
#import "BankCardVC.h"
#import "WithdrawDetailVC.h"
#import "MineApi.h"
@interface WithdrawVC ()
@property (nonatomic,strong) MineApi *mineApi;
@property (weak, nonatomic) IBOutlet UILabel *moneText;
@property (nonatomic,strong) NSDictionary *bankInfoDic;
@end

@implementation WithdrawVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.title = @"提现";
    [self.selectBankView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
    self.moneText.text = [NSString stringWithFormat:@"%@",self.moneyStr];
}

- (void)click{
    BankCardVC *vc = [BankCardVC new];
    vc.itemBlock = ^(NSDictionary * _Nonnull dic) {
        self.bankInfoDic = dic;
        self.banktype.text = dic[@"bank"];
        NSString *bankNum = [NSString stringWithFormat:@"%@",dic[@"bankNumber"]];
        self.bankNum.text = [NSString stringWithFormat:@"尾号:%@",[bankNum substringFromIndex:bankNum.length - 4]];
    };
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)submit:(UIButton *)sender {
    if (!self.bankInfoDic) {
        return;
    }
    NSDictionary *dic = @{@"Amount":self.moneyText.text,@"BankInfo":[NSString stringWithFormat:@"%@,%@,%@", self.bankInfoDic[@"yname"], self.bankInfoDic[@"bank"], self.bankInfoDic[@"bankNumber"]]};
    [self.mineApi submitWithdrawInfoWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [self.navigationController popViewControllerAnimated:true];
        }
    }];
//    [self.mineApi withDrawWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//            NSArray *datas = result.result;
//            if (![datas isEqual:[NSNull null]]) {
//                //                       [JSCMMPopupViewTool showMMPopAlertWithMessage:@"当前有未处理的提现订单,是否前往查看" andHandler:^(NSInteger index) {
//                //                           if (index == 1) {
//                WithdrawDetailVC *vc = [[WithdrawDetailVC alloc] initWithNibName:@"WithdrawDetailVC" bundle:nil];
//                vc.data = datas.firstObject;
//                [self.navigationController pushViewController:vc animated:true];
//                //                           }
//                //                       }];
//            }else{
//                WithdrawVC *vc = [[WithdrawVC alloc] initWithNibName:@"WithdrawVC" bundle:nil];
//                [self.navigationController pushViewController:vc animated:true];
//
//            }
//        }
//    }];
    //    WithdrawDetailVC *vc = [[WithdrawDetailVC alloc] initWithNibName:@"WithdrawDetailVC" bundle:nil];
    //    [self.navigationController pushViewController:vc animated:true];
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    return _mineApi;
}


@end
