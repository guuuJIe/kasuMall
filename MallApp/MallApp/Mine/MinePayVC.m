//
//  MinePayVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MinePayVC.h"
#import "PayWayCell.h"
#import "PurchaseView.h"
#import "OrderApi.h"
#import "PayMannagerUtil.h"
#import "MineWalletVC.h"
#import "SetPwdPopView.h"
#import "SetPayVC.h"
#import "IQKeyboardManager.h"
#import "CustomePwdView.h"
@interface MinePayVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) UILabel *TitleLabel;
@property (nonatomic) PurchaseView *payBtn;
@property (nonatomic) NSMutableArray *dataArr;
@property (nonatomic) NSIndexPath *selectIndexPath;
@property (nonatomic) NSInteger payway;
@property (nonatomic) OrderApi *api;
@property (nonatomic) SetPwdPopView *setPwdView;
@property (nonatomic) CustomePwdView *pwdView;
@end

@implementation MinePayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"选择支付方式";
    [self setupUI];
    self.payway = 3;
    self.TitleLabel.text = [NSString stringWithFormat:@"¥%@",self.money];
    // 添加通知监听见键盘弹出/退出
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    //TODO: 页面Disappear 启用
   [[IQKeyboardManager sharedManager] setEnable:YES];
   [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark UITextFieldDelegate
// 点击非TextField区域取消第一响应者
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
     [self.pwdView.passView.textF resignFirstResponder];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    //方式一
    [self.view endEditing:YES];
   
}

- (void)setupUI{
    [self.view addSubview:self.TitleLabel];
    [self.TitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(44);
        make.centerX.equalTo(self.view);
    }];
    
    [self.view addSubview:self.TabelView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.TitleLabel.mas_bottom).offset(24);
        make.left.equalTo(@12);
        make.right.equalTo(@(-12));
        make.height.mas_equalTo(119);
    }];
    
    [self.view addSubview:self.payBtn];
    [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"PayWayCell";
    PayWayCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[PayWayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
   
    [cell setupData:self.dataArr[indexPath.row]];
    if (_selectIndexPath.row == indexPath.row) {
        cell.selectBtn.selected = true;
    }
    cell.leftAmountLbl.hidden = true;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectIndexPath = indexPath;
    if (indexPath.row == 0) {//支付宝支付
        self.payway = 3;
    }else if (indexPath.row == 1){
        self.payway = 2;

    }
    [_TabelView reloadData];
}

- (void)payOrder{
    
    
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:@"APP" forKey:@"clientType"];
    [dic setValue:@(self.payway) forKey:@"payment"];
    [dic setValue:self.orderData[1] forKey:@"outTradeNo"];
    //普通买家
    [self.api payOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arguData = dic[@"list"];
            if (self.payway == 3) {//支付宝
                [[PayMannagerUtil sharedManager] payWithZhifubaoMethod:arguData.firstObject successBlock:^(BOOL success) {
                    if (success) {
                        [self popWalletVC];
                    }
                } failBlock:^(BOOL fail) {
                   
                    [JMBManager showBriefAlert:@"支付失败"];
                }];
            }else if(self.payway == 2){//微信
                [[PayMannagerUtil sharedManager] payWithWeChatMethod:arguData.firstObject successBlock:^(BaseResp * _Nonnull res) {
                    switch (res.errCode) {
                        case WXSuccess:{
                            [self popWalletVC];
                        }
                            
                            break;
                            
                        default:{
                            
                            [JMBManager showBriefAlert:@"支付失败"];
                        }
                            break;
                    }
                } failBlock:^(BOOL fail) {
                   
                }];
            }else if (self.payway == 5){
               
            }
            
        }
       
    }];
}

- (void)popWalletVC{
    for (UIViewController *temp in self.navigationController.viewControllers) {
        if ([temp isKindOfClass:[MineWalletVC class]]) {
           [self.navigationController popToViewController:temp animated:YES];
        }
    }
}


// 键盘监听事件
- (void)keyboardAction:(NSNotification*)sender{
    // 通过通知对象获取键盘frame: [value CGRectValue]
    NSDictionary *useInfo = [sender userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    //取出键盘弹出需要花费的时间
    double duration = [useInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // <注意>具有约束的控件通过改变约束值进行frame的改变处理
    if([sender.name isEqualToString:UIKeyboardWillShowNotification]){
//        self.toBottom.constant = [value CGRectValue].size.height;
         [UIView animateWithDuration:duration animations:^{
             self.pwdView.frame = CGRectMake(0, self.view.frame.size.height - 200 - [value CGRectValue].size.height, Screen_Width, 200);
         }];
    }else{
        [UIView animateWithDuration:1 animations:^{
            self.pwdView.frame = CGRectMake(0, self.view.frame.size.height, Screen_Width, 200);
        }];
    }
}


#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"PayWayCell" bundle:nil] forCellReuseIdentifier:@"PayWayCell"];
        _TabelView.scrollEnabled = false;
        _TabelView.backgroundColor = [UIColor whiteColor];
        _TabelView.layer.cornerRadius = 8;
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}
-(UILabel *)TitleLabel
{
    if(!_TitleLabel)
    {
        _TitleLabel=[UILabel  new];
        _TitleLabel.font = [UIFont boldSystemFontOfSize:32];
        _TitleLabel.textColor = [UIColor colorWithHexString:@"666666"];
        _TitleLabel.numberOfLines = 1;
        _TitleLabel.text = @"0.00元";
//        _goodsTitleLabel.alpha = 0.2;
    }
    return _TitleLabel;
}

- (PurchaseView *)payBtn{
    if (!_payBtn) {
        _payBtn = [PurchaseView new];
        [_payBtn.purchaseButton setTitle:@"确认支付" forState:0];
        WeakSelf(self);
        _payBtn.addAddressBlock = ^{
           
            [weakself payOrder];
            NSLog(@"支付");
        };
        
    }
    return _payBtn;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
        NSArray *arr = @[@{@"name":@"支付宝支付",@"image":@"pay_2"},@{@"name":@"微信支付",@"image":@"pay_3"}];
        [_dataArr addObjectsFromArray:arr];
    }
    
    return _dataArr;
}
- (SetPwdPopView *)setPwdView{
    if (!_setPwdView) {
        _setPwdView = [SetPwdPopView new];
    }
    return _setPwdView;
}

//- (CustomePwdView *)pwdView{
//    if (!_pwdView) {
//        _pwdView = [[CustomePwdView alloc] initWithFrame:CGRectMake(0, ViewHeight, Screen_Width, 200)];
//        WeakSelf(self)
//        _pwdView.finishBlock = ^(NSString * _Nonnull str) {
//            StrongSelf(self)
//            NSMutableDictionary *dic = [NSMutableDictionary new];
//            [dic setValue:@"APP" forKey:@"clientType"];
//            [dic setValue:@(self.payway) forKey:@"payment"];
//            [dic setValue:str forKey:@"Password"];
//            [dic setValue:self.priceArr[@"orderNumber"] forKey:@"outTradeNo"];
//            
//            //普通买家
//            [self.api payOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
//                if (result.code == 200) {
//                    NSDictionary *dic = result.resultDic;
//                    
//                    if (self.payway == 5){
//                        [self paySuccess];
//                    }
//                    
//                }else{
//                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付密码不正确" preferredStyle:UIAlertControllerStyleAlert];
//                    UIAlertAction *act1 = [UIAlertAction actionWithTitle:@"忘记密码" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                        
//                    }];
//                    UIAlertAction *act2 = [UIAlertAction actionWithTitle:@"重新输入" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                        [self.pwdView.passView cleanPassword];
//                    }];
//                    
//                    [alert addAction:act1];
//                    [alert addAction:act2];
//                    [self presentViewController:alert animated:true completion:nil];
//                }
//                
//            }];
//        };
//        [self.view addSubview:_pwdView];
//    }
//    return _pwdView;
//}
//

- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}
@end
