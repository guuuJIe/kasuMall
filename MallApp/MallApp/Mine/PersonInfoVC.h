//
//  PersonInfoVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonInfoVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UIView *telLbl;
@property (weak, nonatomic) IBOutlet UIView *emailLbl;
@property (weak, nonatomic) IBOutlet UIView *genderview;

@end

NS_ASSUME_NONNULL_END
