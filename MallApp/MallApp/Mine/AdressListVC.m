//
//  AdressListVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AdressListVC.h"
#import "AdresslistTableCell.h"
#import "PurchaseView.h"
#import "AddAdressVC.h"
#import "OrderApi.h"
#import "JSCMMPopupViewTool.h"
@interface AdressListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *TabelView;
@property (nonatomic) PurchaseView *addNewAddress;
@property (nonatomic) OrderApi *api;
@property (nonatomic) NSArray *adressArr;
@end

@implementation AdressListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"收货地址";
    [self setupUI];
   
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setData];
}

- (void)setupUI{
     self.view.backgroundColor = UIColorEF;
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.mas_equalTo(-(50+BottomAreaHeight));
    }];
    
    [self.view addSubview:self.addNewAddress];
    [self.addNewAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)setData{
    [self.api getAddressWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        NSDictionary *dic = result.resultDic;
        NSArray *arr = dic[@"list"];
        self.adressArr = arr;
        [self.TabelView reloadData];
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.adressArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"AdresslistTableCell";
    AdresslistTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[AdresslistTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
    [cell setupData:self.adressArr[indexPath.row]];
    WeakSelf(self)
    cell.deleteBlock = ^{
        NSDictionary *dic = self.adressArr[indexPath.row];
        [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否删除该地址" andHandler:^(NSInteger index) {
            if (index == 1) {
                [self.api delAddressWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                    [weakself setData];
                }];
            }
        }];
    };
    
    cell.setDefalutBlock = ^{
        NSDictionary *dic = self.adressArr[indexPath.row];
        
        
        [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否默认地址" andHandler:^(NSInteger index) {
            if (index == 1) {
                [self.api setDefaultAdressWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
                    [weakself setData];
                }];
            }
              }];
       
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    WeakSelf(self);
    if (self.setAdressBlock) {
        NSDictionary *dic = self.adressArr[indexPath.row];
        self.setAdressBlock(self.adressArr[indexPath.row]);
        [self.api setDefaultAdressWithparameters:dic[@"id"] withCompletionHandler:^(NSError *error, MessageBody *result) {
//            [weakself setData];
             [weakself.navigationController popViewControllerAnimated:true];
        }];
       
        
    }
    
}

#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"AdresslistTableCell" bundle:nil] forCellReuseIdentifier:@"AdresslistTableCell"];
        _TabelView.backgroundColor = UIColorEF;
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (PurchaseView *)addNewAddress{
    if (!_addNewAddress) {
        _addNewAddress = [PurchaseView new];
        [_addNewAddress.purchaseButton setTitle:@"新增地址" forState:0];
        WeakSelf(self);
        _addNewAddress.addAddressBlock = ^{
              AddAdressVC *vc = [[AddAdressVC alloc] initWithNibName:@"AddAdressVC" bundle:nil];
            [weakself.navigationController pushViewController:vc animated:true];
        };
        
    }
    return _addNewAddress;
}

- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}

@end
