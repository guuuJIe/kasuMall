//
//  SuggestionFeedBackVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/18.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SuggestionFeedBackVC.h"
#import "MineApi.h"
#import "MineFeedBackVC.h"
@interface SuggestionFeedBackVC ()<UITextViewDelegate>
@property (nonatomic,strong) UIButton *rightBtn;
@property (nonatomic,strong) MineApi *api;

@end

@implementation SuggestionFeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"意见反馈";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    self.contentText.delegate = self;
}
- (IBAction)submit:(id)sender {
    NSDictionary *dic = @{@"title":self.titleText.text,@"content":self.contentText.text};
    [self.api submitSuggestWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [JMBManager showBriefAlert:@"提交成功"];
//            [self.navigationController popViewControllerAnimated:true];
        }
    }];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    textView.text = @"";
    textView.textColor = UIColor333;
    return true;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
       textView.text = @"请填写您的意见";
        textView.textColor = UIColorBF;
    }
   
    return true;
}

- (void)click{
    [self.navigationController pushViewController:[MineFeedBackVC new] animated:true];
}

-(UIButton *)rightBtn
{
    if(!_rightBtn)
    {
        _rightBtn=[[UIButton  alloc] initWithFrame:CGRectMake(0, 0, 48, 18)];
        [_rightBtn.titleLabel setFont:LabelFont12];
//        [_rightBtn.titleLabel setText:@"意见反馈"];
        [_rightBtn setTitle:@"我的反馈" forState:0];
        [_rightBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [_rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        
    }
    return _rightBtn;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
