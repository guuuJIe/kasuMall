//
//  ApplyWholesaleVC.m
//  MallApp
//
//  Created by Mac on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ApplyWholesaleVC.h"
#import "PurchaseView.h"
#import "CusPickerView.h"
#import "MineApi.h"
#import "UIViewController+XHPhoto.h"
@interface ApplyWholesaleVC ()
@property (nonatomic,strong) PurchaseView *addNewAddress;
@property (nonatomic,strong) CusPickerView *pickerView;
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,strong) NSDictionary *dataDic;
@property (nonatomic,strong) UIImage *imageData;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UIView *picView;
@property (weak, nonatomic) IBOutlet UIImageView *shopImage;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *adressText;
@property (weak, nonatomic) IBOutlet UITextField *conactText;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *telText;
@property (nonatomic,strong) NSArray *imagesArr;
@property (nonatomic,strong) NSString *buyid;
@end

@implementation ApplyWholesaleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    [self setupUI];
    
    [self getWholesaleData];
    
    [self.typeLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
    [self.picView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(uploadPic)]];
}

- (void)setupUI{
    self.title = @"申请批发";
    
    
    [self.view addSubview:self.addNewAddress];
    [self.addNewAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)getWholesaleData{
    [self.api applyWholesaleWithparameters:@"" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            if ([datas isEqual:[NSNull null]]) {
                 [self getBuyType];
            }else{
                self.dataDic = datas.firstObject;
                [self configureData:self.dataDic];
            }
            
        }
    }];
}

- (void)configureData:(NSDictionary *)dic{
    NSInteger value = [dic[@"mjPassed"] integerValue];
    self.typeLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"mjType"] andDefailtInfo:@"类型"];
    self.nameText.text = [XJUtil insertStringWithNotNullObject:dic[@"qyname"] andDefailtInfo:@""];
    self.adressText.text = [XJUtil insertStringWithNotNullObject:dic[@"address"] andDefailtInfo:@""];
    self.telText.text = [XJUtil insertStringWithNotNullObject:dic[@"tel"] andDefailtInfo:@""];
    self.emailText.text = [XJUtil insertStringWithNotNullObject:dic[@"email"] andDefailtInfo:@""];
    self.conactText.text = [XJUtil insertStringWithNotNullObject:dic[@"contact"] andDefailtInfo:@""];
    //0-未申请，1-申请中，2-通过申请
    if (value == 0) {
        [self getBuyType];
        
    }else if (value == 1){
        [self setUsetInterface];
//        [self.addNewAddress setBackgroundColor:UIColorEF];
        [self.addNewAddress.purchaseButton setBackgroundColor:UIColorEF];
        self.addNewAddress.purchaseButton.enabled = false;
        [self.addNewAddress.purchaseButton setTitleColor:UIColorB6 forState:0];
         [_addNewAddress.purchaseButton setTitle:@"申请中" forState:0];
       NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"file0"]];
       [self.shopImage yy_setImageWithURL:URL(url) options:0];
    }else if (value == 2){
        [self setUsetInterface];
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"file0"]];
        [self.shopImage yy_setImageWithURL:URL(url) options:0];
        
        self.addNewAddress.purchaseButton.enabled = false;
        [self.addNewAddress.purchaseButton setTitle:@"通过申请" forState:0];
    }
    
}

- (void)setUsetInterface{
    self.typeLbl.userInteractionEnabled = false;
    self.nameText.userInteractionEnabled = false;
    self.adressText.userInteractionEnabled = false;
    self.adressText.userInteractionEnabled = false;
    self.telText.userInteractionEnabled = false;
    self.emailText.userInteractionEnabled = false;
    self.conactText.userInteractionEnabled = false;
    self.picView.userInteractionEnabled = false;
}

- (void)click{
    [self.pickerView popPickerView];
}

- (void)uploadPic{
    [self showCanEdit:true photo:^(UIImage *photo) {
        NSLog(@"%@",photo);
//        self.imageData = photo;
        self.shopImage.image = photo;
         NSArray *arr =@[@{@"name":@"files",@"image":photo}];
        [JMBManager showLoading];
        [self.api uploadPicWithparameters:@"9" VisitImagesArr:arr withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                self.imagesArr = result.result;
                NSLog(@"%@",result.result);
            }
            [JMBManager hideAlert];
        }];
    }];
}

- (void)getBuyType{
    [self.api getMemberBuyTypeWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.pickerView.dataArr = datas;
        }
    }];
}

- (PurchaseView *)addNewAddress{
    if (!_addNewAddress) {
        _addNewAddress = [PurchaseView new];
        [_addNewAddress.purchaseButton setTitle:@"提交" forState:0];
//        [_addNewAddress.purchaseButton setTitle:@"申请中" forState:UIControlStateDisabled];
//        [_addNewAddress.purchaseButton setTitle:@"申请中" forState:UIControlStateDisabled];
        WeakSelf(self);
        _addNewAddress.addAddressBlock = ^{
           StrongSelf(self)
            NSDictionary *dic =
            @{
                @"mjType":[XJUtil insertStringWithNotNullObject:self.buyid andDefailtInfo:@""],
                @"qyname":[XJUtil insertStringWithNotNullObject:self.nameText.text andDefailtInfo:@""],
                @"email":[XJUtil insertStringWithNotNullObject:self.emailText.text andDefailtInfo:@""],
                @"contact":[XJUtil insertStringWithNotNullObject:self.conactText.text andDefailtInfo:@""],
                @"mobile":[XJUtil insertStringWithNotNullObject:self.telText.text andDefailtInfo:@""],
                @"Address":[XJUtil insertStringWithNotNullObject:self.adressText.text andDefailtInfo:@""],
                @"tel":[XJUtil insertStringWithNotNullObject:self.telText.text andDefailtInfo:@""],
                @"file0":[XJUtil insertStringWithNotNullObject:self.imagesArr.firstObject andDefailtInfo:@""],
                @"mjPassed":@0,
                @"verify":@(false)
            };
            [self.api applyWholesaleShopWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [self.navigationController popViewControllerAnimated:true];
                }
            }];

        };
        
    }
    return _addNewAddress;
}

- (CusPickerView *)pickerView{
    if (!_pickerView) {
        _pickerView = [[CusPickerView alloc] initWithFrame:CGRectMake(0, Screen_Height, Screen_Width, Screen_Height)];
        WeakSelf(self);
        _pickerView.selectBlock = ^(id  _Nonnull result, id  _Nonnull result2, NSString * _Nonnull name) {
            StrongSelf(self);
            self.buyid = result;
            self.typeLbl.text = result2;
        };
        [self.navigationController.view addSubview:_pickerView];
    }
    return _pickerView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}
@end
