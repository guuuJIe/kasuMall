//
//  ChangeInfoVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangeInfoVC : BaseViewController
@property (nonatomic,copy) void(^refreshBlock)(void);
@property (assign,nonatomic) NSInteger tag;
@end

NS_ASSUME_NONNULL_END
