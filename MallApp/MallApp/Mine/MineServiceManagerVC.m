//
//  MineServiceManagerVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineServiceManagerVC.h"
#import "BindMyServiceManagerVC.h"
@interface MineServiceManagerVC ()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *subnameLbl;
@property (weak, nonatomic) IBOutlet UIView *changeView;

@property (weak, nonatomic) IBOutlet UIView *callView;
@end

@implementation MineServiceManagerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"服务经理";
    [self.changeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.callView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self setupData];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            [XJUtil callTelNoCommAlert:self.dic[@"contact"]];
        }
            break;
        case 101:
        {
            BindMyServiceManagerVC *vc = [[BindMyServiceManagerVC alloc] initWithNibName:@"BindMyServiceManagerVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        default:
            break;
    }
}

- (void)setupData{
    self.nameLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"realName"] andDefailtInfo:@"暂无姓名"];
    self.subnameLbl.text = self.dic[@"level"];
}

@end
