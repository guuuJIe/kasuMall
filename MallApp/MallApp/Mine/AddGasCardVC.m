//
//  AddGasCardVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddGasCardVC.h"
#import "MineApi.h"
@interface AddGasCardVC ()
@property (weak, nonatomic) IBOutlet UIView *cardTypeView;
@property (weak, nonatomic) IBOutlet UITextField *nameLbl;
@property (weak, nonatomic) IBOutlet UITextField *gascardnumLbl;
@property (weak, nonatomic) IBOutlet UITextField *gasTypeText;
@property (nonatomic, strong) MineApi *api;
@end

@implementation AddGasCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"添加加油卡";
    [self.cardTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
    
}

- (void)click{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    //添加常规类型按钮
    [alert addAction:[UIAlertAction actionWithTitle:@"中国石化" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.gasTypeText.text = @"中国石化";
    }]];
    //添加常规类型按钮
    [alert addAction:[UIAlertAction actionWithTitle:@"中国石油" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.gasTypeText.text = @"中国石油";
    }]];
    //添加销毁类型按钮
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        NSLog(@"点击警告");
    }]];
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)submitAct:(id)sender {
    NSDictionary *dic = @{
        @"CardType" :@2,
        @"Yname" :self.nameLbl.text,
        @"Bank" :self.gasTypeText.text,
        @"BankNumber" :self.gascardnumLbl.text,
    };
    WeakSelf(self)
    [self.api addMyCardWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self);
        if (result.code == 200) {
            [self.navigationController popViewControllerAnimated:true];
        }
    }];
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
