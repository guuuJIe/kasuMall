//
//  MineFixcardViewController.h
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineFixcardViewController : BaseViewController
@property (nonatomic, strong) NSString *orderNum;
@property (nonatomic, strong) NSString *ProId;
@end

NS_ASSUME_NONNULL_END
