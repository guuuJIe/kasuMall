//
//  ReturnServiceCardVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReturnServiceCardVC : BaseViewController
@property (nonatomic, strong) NSDictionary *dic;//产品信息
@property (nonatomic, strong) NSDictionary *serviceDic;//服务卡信息
@end

NS_ASSUME_NONNULL_END
