//
//  MyServiceCarQRCodeVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MyServiceCarQRCodeVC.h"

@interface MyServiceCarQRCodeVC ()

@end

@implementation MyServiceCarQRCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"服务卡二维码";
    
    
    UIImageView *img = [UIImageView new];
//    img.backgroundColor = [UIColor redColor];
    [self.view addSubview:img];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.view).offset(-100*AdapterScal);
        make.centerX.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(200*AdapterScal, 200*AdapterScal));
    }];
    
    
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    NSString *string = [NSString stringWithFormat:@"Kasu-%@",self.dic[@"account"]];
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [filter setValue:data forKeyPath:@"inputMessage"];
    CIImage *image = [filter outputImage];// 此时的 image 是模糊的
    
    img.image = [XJUtil createNonInterpolatedUIImageFormCIImage:image withSize:200];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
