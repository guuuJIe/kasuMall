//
//  MineFixcardViewController.m
//  MallApp
//
//  Created by Mac on 2020/2/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineServicesCardVC.h"
#import "MinePromptView.h"
#import "MineApi.h"
#import "PromptOrderCell.h"
#import "FixcardCell.h"
#import "MineServicesCardDetailVC.h"
@interface MineServicesCardVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) MinePromptView *topView;
@property (nonatomic,assign) NSInteger statues;
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,strong) MineApi *api;
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSMutableArray *dataSelArr;
@end

@implementation MineServicesCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupProp];
    
    [self setupUI];
    
    [self setupData:RefreshTypeNormal];
}

- (void)setupProp{
    
    self.num = 1;
    self.size = 20;
    self.statues = 1;
    
    self.topView.orderStatues = 200;
}

- (void)setupUI{
    self.title = @"服务卡";
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
   
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@(self.statues) forKey:@"use"];
    
    
    if ([userLevelGrade integerValue] > 3) {
        self.type = 2;
    }else{
        self.type = 3;
    }
    
    
    [self.api getServiceCardListnWithparameters:dic andType:self.type withpageSize:self.size pageNum:self.num withCompletionHandler:^(NSError *error, MessageBody *result) {
        NSArray *datas = result.result;
        
        self.dataArr = [XJUtil dealData:datas withRefreshType:type withListView:self.listTableView AndCurDataArray:self.dataArr withNum:self.num];
        [self configData:self.dataArr withType:0];
    }];
}

- (void)configData:(NSArray *)datas withType:(NSInteger)type{
    [self.dataSelArr removeAllObjects];
    for (int i = 0; i<datas.count; i++) {
        NSDictionary *dic = datas[i];
        NSInteger isused = [dic[@"isUse"] integerValue];
        if (type == 0) {
            if (isused == 0) {
                [self.dataSelArr addObject:dic];
            }
        }else{
           if (isused == 1) {
                [self.dataSelArr addObject:dic];
            }
        }
        
    }
    [self.listTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSelArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"FixcardCell";
    FixcardCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[FixcardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
//    [cell setupData:self.dataSelArr[indexPath.row] withType:self.statues];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MineServicesCardDetailVC *vc = [[MineServicesCardDetailVC alloc] initWithNibName:@"MineServicesCardDetailVC" bundle:nil];
//    vc.dic = self.dataSelArr[indexPath.row];
    [self.navigationController pushViewController:vc animated:true];
}

- (MinePromptView *)topView
{
    if (!_topView) {
        _topView = [MinePromptView new];
        WeakSelf(self);
        //@param status  状态0=全部，1=进行中，2=已支付，4=完成订单
        _topView.dataSource = @[@"未使用",@"已使用"];
        _topView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self)
            switch (tag - 200) {
                case 0:
                    self.statues = 1;
                    [self configData:self.dataArr withType:0];
                    
                    break;
                case 1:
                    self.statues = 2;
                    [self configData:self.dataArr withType:1];
                    break;
                default:
                    break;
            }
//             [weakself.listTableView.mj_header beginRefreshing];
//            [weakself setupData:RefreshTypeNormal];
        };
        [self.view addSubview:_topView];
    }
    return _topView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

-(UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerNib:[UINib nibWithNibName:@"FixcardCell" bundle:nil] forCellReuseIdentifier:@"FixcardCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor clearColor];
//        WeakSelf(self);
//        _listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            StrongSelf(self);
//
//            [self setupData:RefreshTypeDown];
//
//        }];
//
//        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            StrongSelf(self);
//            [self setupData:RefreshTypeUP];
//        }];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}
- (NSMutableArray *)dataSelArr{
    if (!_dataSelArr) {
        _dataSelArr = [NSMutableArray array];
    }
    return _dataSelArr;
}
@end
