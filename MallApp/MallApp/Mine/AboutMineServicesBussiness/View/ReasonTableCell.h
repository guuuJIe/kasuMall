//
//  ReasonTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypeModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReasonTableCell : UITableViewCell
@property (nonatomic, strong) UIButton *selBtn;
@property (nonatomic, strong) TypeModel *typeModel;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
