//
//  ReasonTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ReasonTableCell.h"
@interface ReasonTableCell()
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation ReasonTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    
    return self;
}

- (void)setupUI{
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(24);
        make.centerY.mas_equalTo(self.contentView);
        
    }];
    
    [self.contentView addSubview:self.selBtn];
    [self.selBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(-12);
        make.size.mas_equalTo(CGSizeMake(24, 24));
        
    }];
    
    
    UIView *line = [UIView new];
    [self.contentView addSubview:line];
    line.backgroundColor = UIColorEF;
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    self.selBtn.selected = NO;
    self.titleLabel.text = dic[@"title"];
}

- (void)setTypeModel:(TypeModel *)typeModel{
    _typeModel = typeModel;
    self.titleLabel.text = typeModel.name;
    self.selBtn.selected = typeModel.isSel;
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"请选择质保原因";
        _titleLabel.font = LabelFont16;
        _titleLabel.textColor = UIColor333;
        
    }
    return _titleLabel;
}

- (UIButton *)selBtn{
    if (!_selBtn) {
        _selBtn = [UIButton new];
        [_selBtn setImage:[UIImage imageNamed:@"unsel"] forState:UIControlStateNormal];
        [_selBtn setImage:[UIImage imageNamed:@"select"] forState:UIControlStateSelected];
        
        
    }
    
    return _selBtn;
}

@end
