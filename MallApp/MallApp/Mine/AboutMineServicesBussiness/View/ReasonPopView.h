//
//  ReasonPopView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <MMPopupView/MMPopupView.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReasonPopView : MMPopupView
@property (nonatomic, strong) NSArray *datasArr;
@property (nonatomic, copy) void(^clickBlock)(NSString *text);
@property (nonatomic, strong) UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
