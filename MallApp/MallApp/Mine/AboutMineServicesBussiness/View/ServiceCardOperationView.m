//
//  ServiceCardOperationView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ServiceCardOperationView.h"

@implementation ServiceCardOperationView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.userInteractionEnabled = true;
    }
    
    return self;
}


- (void)setType:(NSInteger)Type{
    _Type = Type;
    
    if (Type == 1) {
        
    }else if (Type == 0){
        UIView *lastView;
        NSArray *btnMoelArray = @[@{@"title":@"申请退卡",@"flag":@"apply"},@{@"title":@"确定完成",@"flag":@"achieve"}];
        for (int i = 0; i < btnMoelArray.count; i++) {
            NSDictionary *dic = btnMoelArray[i];
            UIButton *button = [UIButton new];
            if (i == 0) {
                [button setTitleColor:APPColor forState:UIControlStateNormal];
            }else{
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [button setBackgroundColor:APPColor];
            }
            
        
            [button setTitle:dic[@"title"] forState:UIControlStateNormal];
            button.titleLabel.font = LabelFont14;
            [button setTitle:dic[@"flag"] forState:UIControlStateSelected];
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(Screen_Width/2, 50));
                make.top.mas_equalTo(0);
                if(i == 0){
                    make.left.equalTo(self).offset(0);
                }else{
                    make.left.equalTo(lastView.mas_right).offset(0);
                }
            }];
            lastView = button;
        }
    }
}


- (void)buttonClick:(UIButton *)sender{
    if (self.clickBlock) {
        self.clickBlock([sender titleForState:UIControlStateSelected]);
    }
}


@end
