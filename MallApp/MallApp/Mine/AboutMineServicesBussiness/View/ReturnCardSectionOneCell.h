//
//  ReturnCardSectionOneCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReturnCardSectionOneCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *reasonText;
@property (weak, nonatomic) IBOutlet UILabel *reasonLbl;
@property (nonatomic, copy) void(^clickBlock)(void);
@property (nonatomic,strong) NSDictionary *dic;
@end

NS_ASSUME_NONNULL_END
