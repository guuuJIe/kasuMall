//
//  ReturnCardSectionOneCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ReturnCardSectionOneCell.h"
#import "ReasonPopView.h"
@interface ReturnCardSectionOneCell()
@property (nonatomic, strong) ReasonPopView *popView;
@property (nonatomic, strong) NSMutableArray *datasArr;
@end

@implementation ReturnCardSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnClick:(UIButton *)sender {
//    if (self.clickBlock) {
//        self.clickBlock();
//    }
    NSInteger type = [self.dic[@"pass"] integerValue];
    if (type == 0) {
        [self.popView show];
    }
    
}

- (ReasonPopView *)popView{
    if (!_popView) {
        _popView = [ReasonPopView new];
        _popView.datasArr = self.datasArr;
        WeakSelf(self)
        _popView.clickBlock = ^(NSString * _Nonnull text) {
            weakself.reasonText.text = [NSString stringWithFormat:@"%@",text];
        };
    }
    
    return _popView;
}

- (NSMutableArray *)datasArr{
    if (!_datasArr) {
        _datasArr = [NSMutableArray array];
        [_datasArr addObject:@{@"title":@"没有完成延期付款"}];
        [_datasArr addObject:@{@"title":@"质量问题"}];
        [_datasArr addObject:@{@"title":@"做工瑕疵破损"}];
        [_datasArr addObject:@{@"title":@"配件破损"}];
        [_datasArr addObject:@{@"title":@"很不喜欢"}];
    }

    return _datasArr;
}

- (void)setDic:(NSDictionary *)dic{
    if (dic) {
        _dic = dic;
        self.reasonText.text = [XJUtil insertStringWithNotNullObject:dic[@"reason"] andDefailtInfo:@""];
        NSInteger type = [self.dic[@"pass"] integerValue];
        if (type != 0) {
            self.reasonText.userInteractionEnabled = false;
        }
    }
    
    
}

@end
