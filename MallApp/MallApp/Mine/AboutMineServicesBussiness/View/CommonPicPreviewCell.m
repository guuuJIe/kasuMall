//
//  CommonPicPreviewCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/23.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CommonPicPreviewCell.h"
#import "FixOrderListImageCollectionCell.h"
#import <YBImageBrowser/YBImageBrowser.h>
@interface CommonPicPreviewCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) UICollectionView *collectionView;

@end

@implementation CommonPicPreviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UILabel *title = [UILabel new];
    title.text = @"凭证资料";
    title.textColor = UIColor333;
    title.font = LabelFont14;
    self.titleLbl = title;
    
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@12);
        make.top.mas_equalTo(@12);
        
    }];
    
    [self.contentView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title.mas_bottom).offset(10);
        make.height.mas_equalTo(80);
        make.bottom.mas_equalTo(self.contentView);
    }];
}


- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *str = [NSString stringWithFormat:@"%@",dic[@"pic"]];
        NSArray *datas;
        if (str.length != 0) {
            datas = [str componentsSeparatedByString:@","];
        }
        [self.dataArray removeAllObjects];
        [datas enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YBIBImageData *data = [YBIBImageData new];
            NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,datas[idx]];
            data.imageURL = URL(url);
            
            [self.dataArray addObject:data];

        }];
        JLog(@"%@",self.dataArray);
        [self.collectionView reloadData];
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FixOrderListImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FixOrderListImageCollectionCell" forIndexPath:indexPath];
    YBIBImageData *data = self.dataArray[indexPath.row];
    

    [cell.dataImageView yy_setImageWithURL:data.imageURL placeholder:[UIImage imageNamed:@"placeholder_square"]];
    return cell;
    

}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section

{
     return 10.f;

}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section

{
 return 10.f;

}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 10, 0, 0);//分别为上、左、下、右
}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = self.dataArray;
    browser.currentPage = indexPath.item;
    //       browser.delegate = self;
    [browser show];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    
    return _dataArray;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        
        layout.itemSize = CGSizeMake(60, 60);
        layout.minimumLineSpacing = 0.0;
        layout.minimumInteritemSpacing = 10.0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        
        _collectionView.backgroundColor = [UIColor whiteColor];
        //    _collectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _collectionView.showsHorizontalScrollIndicator = false;
        [_collectionView registerNib:[UINib nibWithNibName:@"FixOrderListImageCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"FixOrderListImageCollectionCell"];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    }
    
    return _collectionView;
}

@end
