//
//  ServiceCardOperationView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceCardOperationView : UIView
@property (assign, nonatomic) NSInteger Type;
@property (nonatomic,copy) void(^clickBlock)(NSString *prop);
@end

NS_ASSUME_NONNULL_END
