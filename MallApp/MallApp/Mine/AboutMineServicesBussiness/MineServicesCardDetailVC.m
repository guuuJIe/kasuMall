//
//  MineServicesCardDetailVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineServicesCardDetailVC.h"
#import "PurchaseView.h"
#import "MyServiceCarQRCodeVC.h"
#import "ServiceCardOperationView.h"
#import "ReturnServiceCardVC.h"
@interface MineServicesCardDetailVC ()
@property (weak, nonatomic) IBOutlet UIView *checkView;
@property (nonatomic,strong) PurchaseView *purchaseView;
@property (nonatomic,strong) ServiceCardOperationView *operationView;
@property (nonatomic,strong) UITableView *listTableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *accountLbl;
@property (weak, nonatomic) IBOutlet UILabel *statesLbl;
@property (weak, nonatomic) IBOutlet UILabel *moneLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *belongsLBl;
@property (weak, nonatomic) IBOutlet UILabel *isUsedLbl;
@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet UIView *useTimeView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *usedTimeLbl;
@end

@implementation MineServicesCardDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"服务卡详情";
    

    [self setupData];
}

- (void)setupUI{
    
    [self.checkView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkAct)]];
    
//    [self.view addSubview:self.purchaseView];
//    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.bottom.right.equalTo(self.view);
//        make.height.mas_equalTo(50+BottomAreaHeight);
//    }];
    
    
//    [self.view addSubview:self.operationView];
//    [self.operationView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.bottom.right.equalTo(self.view);
//        make.height.mas_equalTo(50+BottomAreaHeight);
//    }];
    
    
}


- (void)setupData{
    self.titleLbl.text = [XJUtil insertStringWithNotNullObject:self.dic[@"title"] andDefailtInfo:@""];
    self.accountLbl.text = self.dic[@"account"];
//    if ([userLevelGrade integerValue] >= 3) {
//        self.moneLbl.text = [NSString stringWithFormat:@"%@",self.dic[@"amount"]];
//        self.statesLbl.text = @"金额";
//    }else{
        self.moneLbl.text = [NSString stringWithFormat:@"%@",self.dic[@"password"]];
        self.statesLbl.text = @"密码";
//    }
   
    self.timeLbl.text = self.dic[@"created"];
    self.shopNameLbl.text = self.dic[@"userShopName"];
    self.belongsLBl.text = self.dic[@"userShopUserName"];
    NSInteger isuesd = [self.dic[@"isUse"] integerValue];
    if (isuesd == 0) {
//        self.operationView.Type = 0;
        self.isUsedLbl.text = @"未使用";
        self.userView.hidden = true;
        self.useTimeView.hidden = true;
        self.isUsedLbl.textColor = [UIColor colorWithHexString:@"23CC0C"];
    }else{
//        self.operationView.Type = 1;
        self.isUsedLbl.text = @"已使用";
        self.userView.hidden = false;
        self.useTimeView.hidden = false;
        self.userNameLbl.text = self.dic[@"userUseShopUserName"];
        self.usedTimeLbl.text = self.dic[@"useTime"];
        self.isUsedLbl.textColor = [UIColor colorWithHexString:@"FFA127"];
    }
}

- (void)checkAct{
    MyServiceCarQRCodeVC *vc = [MyServiceCarQRCodeVC new];
    [self.navigationController pushViewController:vc animated:true];
}



- (ServiceCardOperationView *)operationView{
    if (!_operationView) {
        _operationView = [ServiceCardOperationView new];
//        _operationView.Type = 0;
        WeakSelf(self)
        _operationView.clickBlock = ^(NSString * _Nonnull prop) {
            ReturnServiceCardVC *vc = [ReturnServiceCardVC new];
            [weakself.navigationController pushViewController:vc animated:true];
        };
    }
    
    return _operationView;
}


@end
