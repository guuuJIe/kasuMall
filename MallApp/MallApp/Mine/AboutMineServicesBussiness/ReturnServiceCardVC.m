//
//  ReturnServiceCardVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/21.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ReturnServiceCardVC.h"
#import "ReturnCardSectionOneCell.h"
#import "ServicesRepairSectionTwoCell.h"
#import "ServicesRepairSectionThreeCell.h"
#import "TZImagePickerController.h"
#import "PurchaseView.h"
#import "ReturnServiceCardVC.m"
#import "OrdetItemCell.h"
#import "MineApi.h"
#import "MemberApi.h"
#import "CommonPicPreviewCell.h"
@interface ReturnServiceCardVC ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, strong) UITextField *reasonText;
@property (nonatomic, strong) UILabel *reasonLbl;
@property (nonatomic, strong) PurchaseView *purchaseView;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) dispatch_semaphore_t lock;
@property (nonatomic, strong) MineApi *mineApi;
@property (nonatomic, strong) MemberApi *memberApi;
@property (nonatomic, strong) NSString *picStr;
@property (nonatomic, assign) NSInteger type;
@end

@implementation ReturnServiceCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"申请退卡";
    
    [self setupData];
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    
    [self.view addSubview:self.purchaseView];
    [self.purchaseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *Identifier = @"OrdetItemCell";
        OrdetItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        [cell setupTaocanData:self.dic];
        return cell;
    }else if (indexPath.row == 1){
        static NSString *Identifier = @"ReturnCardSectionOneCell";
        ReturnCardSectionOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
        self.reasonText = cell.reasonText;

        cell.dic = self.dic;
        return cell;
    }else if (indexPath.row == 2){
        
        if (self.type == 0) {
            static NSString *Identifier = @"ServicesRepairSectionThreeCell";
            ServicesRepairSectionThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
            WeakSelf(self)
            cell.clickBlock = ^(NSInteger index) {
                if (index == weakself.selectedPhotos.count) {
                    [weakself selectPhoto];
                }else{
                    [weakself checkPhoto:index];
                }
            };
            [cell setupCollectionDatawith:_selectedPhotos andAssets:_selectedAssets];
            return cell;
        }else{
            static NSString *Identifier = @"CommonPicPreviewCell";
            CommonPicPreviewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
            [cell setupData:self.dic];
            return cell;
        }
       
    }
    
    
    return nil;
}


- (void)setupData{
    self.type = [self.dic[@"pass"] integerValue];
    if (self.type != 0) {
        self.reasonText.userInteractionEnabled = false;
        [self.purchaseView.purchaseButton setBackgroundColor:UIColorEF];
        self.purchaseView.purchaseButton.enabled = false;
    }
    
    if (self.type == 1) {
        [self.purchaseView.purchaseButton setTitle:@"待审核" forState:0];
        
    }else if (self.type == 2){
        [self.purchaseView.purchaseButton setTitle:@"未通过" forState:0];
    }else if (self.type == 3){
        [self.purchaseView.purchaseButton setTitle:@"已通过" forState:0];
    }
}

- (void)submitInfoVC{
//    ReturnServiceCardVC *vc = [ReturnServiceCardVC new];
//    [self.navigationController pushViewController:vc animated:true];
    
    if (!self.reasonText.text) {
        [JMBManager showBriefAlert:@"请填写原因"];
        return;
    }
    
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //        if (self.selectedPhotos.count >0) {
        NSMutableArray *imagesObj = [NSMutableArray new];
        for (int i = 0; i<self.selectedPhotos.count; i++) {
            NSDictionary *dic = @{@"name":@"files",@"image":self.selectedPhotos[i]};
            [imagesObj addObject:dic];
        }
        
        [JMBManager showLoading];
        //            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        [self.mineApi uploadPicWithparameters:@"17" VisitImagesArr:imagesObj withCompletionHandler:^(NSError *error, MessageBody *result) {
            
            JLog(@"%@",result.result);
            NSArray *datas = result.result;
            self.picStr = [datas componentsJoinedByString:@","];
            
            //                dispatch_semaphore_signal(semaphore);
            XJ_UNLOCK(self.lock);
        }];
        //        }
        //        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        XJ_LOCK(self.lock);
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.dic];
        dispatch_async(dispatch_get_main_queue(), ^{
            [dic setValue:self.reasonText.text forKey:@"reason"];
            [dic setValue:self.picStr forKey:@"pic"];
            [dic setValue:self.serviceDic[@"account"] forKey:@"account"];
        });
        
        [self.memberApi applyReturnCardWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:true];
                });
            }
            [JMBManager hideAlert];
        }];
        
    });
    
}


- (void)selectPhoto{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:6 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    imagePickerVc.maxImagesCount = 6;
    imagePickerVc.allowTakePicture = true; // 在内部显示拍照按钮
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    WeakSelf(self)
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        JLog(@"%@ --- %@",assets,photos);
        weakself.selectedAssets = [NSMutableArray arrayWithArray:assets];
        weakself.selectedPhotos = [NSMutableArray arrayWithArray:photos];
        dispatch_async(dispatch_get_main_queue(), ^{
             [self.listTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        });
       
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)checkPhoto:(NSInteger)index{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:_selectedAssets selectedPhotos:_selectedPhotos index:index];
    
    
    imagePickerVc.showSelectedIndex = true;
    imagePickerVc.isSelectOriginalPhoto = true;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.showsVerticalScrollIndicator = false;
        _listTableView.estimatedRowHeight = 40;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.backgroundColor = [UIColor clearColor];
        [_listTableView registerNib:[UINib nibWithNibName:@"ReturnCardSectionOneCell" bundle:nil] forCellReuseIdentifier:@"ReturnCardSectionOneCell"];
        [_listTableView registerClass:[OrdetItemCell class] forCellReuseIdentifier:@"OrdetItemCell"];
        [_listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"ServicesRepairSectionThreeCell" bundle:nil] forCellReuseIdentifier:@"ServicesRepairSectionThreeCell"];
        [_listTableView registerClass:[CommonPicPreviewCell class] forCellReuseIdentifier:@"CommonPicPreviewCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (PurchaseView *)purchaseView{
    if (!_purchaseView) {
        _purchaseView = [PurchaseView new];
        [_purchaseView.purchaseButton setTitle:@"提交" forState:0];
        WeakSelf(self);
        _purchaseView.addAddressBlock = ^{
//            [weakself purchase];
//            [weakself submitInfoAct];
            [weakself submitInfoVC];
        };
    }
    return _purchaseView;
}
                   

- (dispatch_semaphore_t)lock {
    if (!_lock) {
        _lock = dispatch_semaphore_create(0);
    }
    return _lock;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    return _mineApi;
}

- (MemberApi *)memberApi{
    if (!_memberApi) {
        _memberApi = [MemberApi new];
    }
    
    return _memberApi;
}
@end
