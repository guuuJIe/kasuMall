//
//  SetPwdPopView.m
//  MallApp
//
//  Created by Mac on 2020/2/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SetPwdPopView.h"
#import "GoSetPwdView.h"
@interface SetPwdPopView()
@property (nonatomic, strong)UIButton *chaButton;
@property (nonatomic, strong) GoSetPwdView *goView;
@end

@implementation SetPwdPopView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(431*AdapterScal);
        }];
        
        [bgView addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(16));
            make.top.equalTo(@16);
        }];
        
        
        UILabel *type = [UILabel new];
        type.text = @"请输入支付密码";
        type.textColor = UIColor333;
        type.font = LabelFont16;
        [bgView addSubview:type];
        [type mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(bgView);
            make.top.equalTo(bgView).offset(16);
        }];
        
//        UILabel *type2 = [UILabel new];
//        type2.text = @"您还未设置支付密码,请先设置支付密码后在完成支付";
//        type2.textColor = UIColor66;
//        type2.font = LabelFont13;
////        type2.textAlignment = nstex;
////        type2.
//        type2.backgroundColor = UIColorEF;
//
//        [bgView addSubview:type2];
//        [type2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(bgView);
//            make.top.equalTo(type.mas_bottom).offset(16);
//            make.height.mas_equalTo(42);
//        }];
        
        [bgView addSubview:self.goView];
        [self.goView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(bgView);
            make.top.mas_equalTo(type.mas_bottom).offset(16);
            make.height.mas_equalTo(100);
        }];
        
    }
    return self;
}

- (void)dismissView{
    [self hide];
}

- (void)click{
    [self hide];
    if (self.clickBlock) {
        self.clickBlock();
    }
}
-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
      
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}


- (GoSetPwdView *)goView{
    if (!_goView) {
        _goView = [[GoSetPwdView alloc] init];
        [_goView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
    }
    return _goView;
}
@end
