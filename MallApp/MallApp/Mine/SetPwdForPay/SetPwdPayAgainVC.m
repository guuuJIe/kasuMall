//
//  SetPwdPayAgainVC.m
//  MallApp
//
//  Created by Mac on 2020/2/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SetPwdPayAgainVC.h"
#import "YN_PassWordView.h"
#import "AccountApi.h"
@interface SetPwdPayAgainVC ()
@property (nonatomic, strong) YN_PassWordView *passView;
@property (nonatomic, strong) AccountApi *api;
@end

@implementation SetPwdPayAgainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置支付密码";
    
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"SetPayVC",@"SetPwdPayVC",@"VerifyTelVC",@"ChangePayPwdVC"]];
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请再次输入支付密码";
    label.textColor = UIColor999;
    label.font = LabelFont16;
    label.numberOfLines = 0;
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(20);
    }];
    self.passView = [[YN_PassWordView alloc] init];
    self.passView.frame = CGRectMake(25, 60, Screen_Width-25*2, 50);
    
    self.passView.showType = 1;//五种样式
    self.passView.textF.keyboardType = UIKeyboardTypeNumberPad;
    self.passView.num = 6;//框框个数
    self.passView.tintColor = UIColorE9;
    [self.passView show];
    self.passView.backgroundColor= [UIColor clearColor];
    [self.view addSubview:self.passView];
    [self.passView.textF becomeFirstResponder];
    WeakSelf(self)
    self.passView.textBlock = ^(NSString *str) {
        StrongSelf(self)
        
        if (![self.password isEqualToString:str]) {
            [JMBManager showBriefAlert:@"密码不一致,请重新输入"];
            [self.passView cleanPassword];
            return ;
        }
        if (self.curPwd) {
             NSDictionary *dic = @{@"UserName": myname,@"OldPassword":self.curPwd,@"PassWord":str};
            [self.api changePayPasswordWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                if (result.code == 200) {
                    [JMBManager showBriefAlert:@"设置成功"];
                    //                [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"userpayPassword"];
                    //                [[NSUserDefaults standardUserDefaults] synchronize];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                }
            }];
        }else{
            
            
            NSDictionary *dic = @{@"Phone":self.tel,@"VerifyCode":self.verifyCode,@"PassWord":str};
            
            [self.api setPayPasswordWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                StrongSelf(self)
                if (result.code == 200) {
                    
                    [JMBManager showBriefAlert:@"设置成功"];
                    //                [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"userpayPassword"];
                    //                [[NSUserDefaults standardUserDefaults] synchronize];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });
                }
            }];
        }
        
        
       
//        [XJUtil popToVC:@"MineSettingVC" inViewControllers:self.navigationController.viewControllers InCurNav:self.navigationController];
    };
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}


- (AccountApi *)api{
    if (!_api) {
        _api = [AccountApi new];
    }
    return _api;
}

@end
