//
//  SetPwdPayVC.h
//  MallApp
//
//  Created by Mac on 2020/2/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetPwdPayVC : BaseViewController
@property (nonatomic, strong)NSString *verifyCode;
@property (nonatomic, strong)NSString *tel;
@property (nonatomic, strong)NSString *curPwd;
@end

NS_ASSUME_NONNULL_END
