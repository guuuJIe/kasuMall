//
//  GoSetPwdView.m
//  MallApp
//
//  Created by Mac on 2020/2/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoSetPwdView.h"
@interface GoSetPwdView()

@end

@implementation GoSetPwdView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadNibFile];
    }
    return self;
}
- (void)layoutSubviews{
    _viewSelf.frame = self.bounds;
}

- (void)loadNibFile{
    _viewSelf = [[[NSBundle mainBundle] loadNibNamed:@"GoSetPwdView" owner:self options:nil] lastObject];
    [self addSubview:_viewSelf];
}
@end
