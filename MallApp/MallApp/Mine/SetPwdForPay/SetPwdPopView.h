//
//  SetPwdPopView.h
//  MallApp
//
//  Created by Mac on 2020/2/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetPwdPopView : MMPopupView
@property (nonatomic,copy) void(^clickBlock)(void);
@end

NS_ASSUME_NONNULL_END
