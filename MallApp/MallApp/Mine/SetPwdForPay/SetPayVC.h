//
//  SetPayVC.h
//  MallApp
//
//  Created by Mac on 2020/2/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetPayVC : BaseViewController
@property (assign,nonatomic) NSInteger type;
@property (strong,nonatomic) NSString  *tel;
@end

NS_ASSUME_NONNULL_END
