//
//  SetPayVC.m
//  MallApp
//
//  Created by Mac on 2020/2/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SetPayVC.h"
#import "SetPwdPayVC.h"
#import "VerifyTelVC.h"
#import "OrderApi.h"
#import "ChangePayPwdVC.h"
@interface SetPayVC ()
@property (weak, nonatomic) IBOutlet UIView *setPayView;
@property (weak, nonatomic) IBOutlet UIView *forgetPwdView;
@property (weak, nonatomic) IBOutlet UILabel *statuesLbl;
@property (nonatomic, strong)OrderApi *api;
@end

@implementation SetPayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"支付管理";
    [self.setPayView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.forgetPwdView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [JMBManager showLoading];
    [self.api getCurAccountStatuesWithparameters:@"" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            NSDictionary *dic = datas.firstObject;
            NSInteger type = [dic[@"payStatus"] integerValue];
            if (type == 1) {
                self.statuesLbl.text = @"修改支付密码";
                self.forgetPwdView.hidden = false;
            }else if (type == 0){
                self.forgetPwdView.hidden = true;
            }
            self.type = type;
            self.tel = dic[@"regPhone"];
        }
        //        self.te
        [JMBManager hideAlert];
    }];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            if(self.type == 1){
                ChangePayPwdVC *vc = [ChangePayPwdVC new];
                vc.tel = self.tel;
                [self.navigationController pushViewController:vc animated:true];
            }else{
                VerifyTelVC *vc = [[VerifyTelVC alloc] init];
                vc.tel = self.tel;
                [self.navigationController pushViewController:vc animated:true];
            }
           
        }
            break;
        case 101:
        {
            VerifyTelVC *vc = [VerifyTelVC new];
            vc.tel = self.tel;
            [self.navigationController pushViewController:vc animated:true];

        }
            break;
        default:
            break;
    }
}

- (OrderApi *)api{
    if (!_api) {
        _api = [OrderApi new];
    }
    return _api;
}

@end
