//
//  ChangePayPwdVC.m
//  MallApp
//
//  Created by Mac on 2020/2/26.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ChangePayPwdVC.h"
#import "YN_PassWordView.h"
#import "SetPwdPayVC.h"
@interface ChangePayPwdVC ()
@property (nonatomic, strong) YN_PassWordView *passView;
@end

@implementation ChangePayPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"修改支付密码";
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请输入原始密码";
    label.textColor = UIColor999;
    label.font = LabelFont16;
    label.numberOfLines = 0;
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(20);
    }];
    self.passView = [[YN_PassWordView alloc] init];
    self.passView.frame = CGRectMake(25, 60, Screen_Width-25*2, 50);
    
    self.passView.showType = 1;//五种样式
    self.passView.textF.keyboardType = UIKeyboardTypeNumberPad;
    self.passView.num = 6;//框框个数
    self.passView.tintColor = UIColorE9;
    [self.passView show];
    self.passView.backgroundColor= [UIColor clearColor];
    [self.view addSubview:self.passView];
    [self.passView.textF becomeFirstResponder];
    WeakSelf(self)
    self.passView.textBlock = ^(NSString *str) {
        StrongSelf(self)
        SetPwdPayVC *vc = [SetPwdPayVC new];
        vc.curPwd = str;
        vc.tel = self.tel;
        [self.navigationController pushViewController:vc animated:true];
    };
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

@end
