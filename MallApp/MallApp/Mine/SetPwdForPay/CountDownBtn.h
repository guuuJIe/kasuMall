//
//  CountDownBtn.h
//  MallApp
//
//  Created by Mac on 2020/2/25.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CountDownBtn : UIButton
-(void)startClick;
@end

NS_ASSUME_NONNULL_END
