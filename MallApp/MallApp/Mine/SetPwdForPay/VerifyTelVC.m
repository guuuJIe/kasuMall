//
//  VerifyTelVC.m
//  MallApp
//
//  Created by Mac on 2020/2/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "VerifyTelVC.h"
#import "CountDownBtn.h"
#import "SetPwdPayVC.h"
#import "AccountApi.h"
@interface VerifyTelVC ()
@property (nonatomic,strong) CountDownBtn *countBtn;
@property (nonatomic,strong) UITextField *text;
@property (nonatomic,strong) AccountApi *Api;
@end

@implementation VerifyTelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"验证手机";
    
    [self setupUI];
    
    [self getVerifyCode];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setupUI{
//    self.title = @"";
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@"验证码已发送至 %@",self.tel];
    label.textColor = UIColor333 ;
    label.font = LabelFont16;
    label.numberOfLines = 0;
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(16);
        make.left.mas_equalTo(40);
    }];
    
    UITextField *text = [UITextField new];
    text.placeholder = @"请输入验证码";
    text.textColor = UIColor333;
    text.font = LabelFont14;
    [self.view addSubview:text];
    self.text = text;
    [text mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(label);
        make.top.mas_equalTo(label.mas_bottom).offset(25);
        make.size.mas_equalTo(CGSizeMake(175*AdapterScal, 32));
    }];
    
    self.countBtn = [CountDownBtn new];
    [self.view addSubview:self.countBtn];
//    [self.countBtn startClick];
    [self.countBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).offset(-40);
        make.centerY.mas_equalTo(text);
        make.size.mas_equalTo(CGSizeMake(100, 32));
    }];
    
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:label.text];
    [aStr addAttribute:NSForegroundColorAttributeName value:APPColor range:[label.text rangeOfString:self.tel]];
    label.attributedText = aStr;
    
    UIView *line = [UIView new];
    [self.view addSubview:line];
    line.backgroundColor = UIColorEF;
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.text);
        make.right.equalTo(self.countBtn);
        make.top.mas_equalTo(self.countBtn.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitle:@"下一步" forState:0];
    [button.titleLabel setFont:LabelFont14];
    button.backgroundColor = APPColor;
    [button setTitleColor:[UIColor whiteColor] forState:0];
    button.tag = 100;
    [button addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = true;
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(line);
        make.height.mas_equalTo(44);
        make.top.mas_equalTo(line.mas_bottom).offset(30);
    }];
}

- (void)getVerifyCode{
    NSDictionary *dic = @{@"phone":self.tel};
    [self.Api getverifyCodeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            [self.countBtn startClick];
        }
    }];
}

- (void)click{
    NSDictionary *dic = @{@"phone":self.tel,@"verifyCode":self.text.text};
    [self.Api verifyCodeLoginWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            SetPwdPayVC *vc = [SetPwdPayVC new];
            vc.tel = self.tel;
            vc.verifyCode = self.text.text;
            [self.navigationController pushViewController:vc animated:true];
        }
    }];
   
}

- (AccountApi *)Api{
    if (!_Api) {
        _Api = [AccountApi new];
    }
    return _Api;
}

- (void)dealloc{
    JLog(@"dealloc");
}
@end
