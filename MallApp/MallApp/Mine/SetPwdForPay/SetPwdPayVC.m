//
//  SetPwdPayVC.m
//  MallApp
//
//  Created by Mac on 2020/2/24.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SetPwdPayVC.h"
#import "YN_PassWordView.h"
#import "SetPwdPayAgainVC.h"
@interface SetPwdPayVC ()
@property (nonatomic, strong) YN_PassWordView *passView;
@end

@implementation SetPwdPayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"设置支付密码";
    
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请设置支付密码,用于余额付款";
    label.textColor = UIColor999;
    label.font = LabelFont16;
    label.numberOfLines = 0;
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(20);
    }];
    self.passView = [[YN_PassWordView alloc] init];
    self.passView.frame = CGRectMake(25, 60, Screen_Width-25*2, 50);
    
    self.passView.showType = 1;//五种样式
    self.passView.textF.keyboardType = UIKeyboardTypeNumberPad;
    self.passView.num = 6;//框框个数
    self.passView.tintColor = UIColorE9;
    [self.passView show];
    self.passView.backgroundColor= [UIColor clearColor];
    [self.view addSubview:self.passView];
    [self.passView.textF becomeFirstResponder];
    WeakSelf(self)
    self.passView.textBlock = ^(NSString *str) {
        StrongSelf(self)
//        if (self.finishBlock) {
//            self.finishBlock(str);
//        }
        SetPwdPayAgainVC *vc = [[SetPwdPayAgainVC alloc] init];
        vc.tel = self.tel;
        vc.verifyCode = self.verifyCode;
        vc.password = str;
        vc.curPwd = self.curPwd;
        [weakself.navigationController pushViewController:vc animated:true];
    };
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}


- (void)dealloc{
   
    NSLog(@"111");
    
}
@end
