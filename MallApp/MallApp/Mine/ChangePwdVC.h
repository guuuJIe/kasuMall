//
//  ChangePwdVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangePwdVC : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *pwdText;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdText;
- (IBAction)submit:(id)sender;

@end

NS_ASSUME_NONNULL_END
