//
//  MineVC.m
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineVC.h"
#import "MyInfoOneCell.h"
#import "OrderFuncCell.h"
#import "UtilFuncCell.h"
#import "LoginVC.h"
#import "AdressListVC.h"
#import "OrderListVC.h"
#import "FapiaoVC.h"
#import "MineFixOrderVC.h"
#import "MyGoodOrderListVC.h"
#import "MineApi.h"
#import "MineWalletVC.h"
#import "ApplyWholesaleVC.h"
#import "MineCardPackageVC.h"
#import "MineBusinessMangeVC.h"
#import "MineSettingVC.h"
#import "MineInfoSectionFourCell.h"
#import "SellerCenterGradOrderVC.h"
#import "SellerOrderManagerVC.h"
#import "SellerGoodManagerViewController.h"
#import "RefundOrderListVC.h"
#import "SuggestionFeedBackVC.h"
#import "MerchantSettledVC.h"
#import "SellerFixManagerVC.h"
#import "ToBindServiceManager.h"
#import "MineServiceManagerVC.h"
#import "ServicesOfYearOrderVC.h"
#import "CarMaintainenceOrderVC.h"
#import "FixCardSearchVC.h"
#import "MyWeixiuTaocanOrderVC.h"
#import "FixOrderManageVC.h"
#import "ApplyforShopVC.h"
#import "ApplyShopSatuesVC.h"
#import "ShopSetVC.h"
#import "CommunityTableCell.h"
#import "SellerOrderBtnTableCell.h"
#import "SellerUtilitiesTableCell.h"
#import "ToBeLeaderVC.h"
#import "SellerInfoOneCell.h"
#import "SellerCenterGoodsOrderVC.h"
#import "SellerFixOrderListVC.h"
#import "SellerOrderRefundVC.h"
#import "SellerCenteWeixiuTaocanOrderVC.h"
#import "SellerCarMaintainenceOrderVC.h"
#import "SellerCarMaintainenceOrderVC.h"
#import "SellerRepairOrderVC.h"
#import "MyProjectVC.h"
#import "InviteFriendVC.h"
#import "MineDelegateInfoVC.h"
#import "MinePerformanceVC.h"
#import "LeaderInfoVC.h"
@interface MineVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic) UITableView *TabelView;
@property (nonatomic) MineApi *mineApi;
@property (nonatomic) NSDictionary *dataDic;
@property (nonatomic) NSArray *menuArray;
@property (nonatomic) UITableView *sellerTabelView;
@end

@implementation MineVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.title = @"我的";
//    self.view.backgroundColor = APPColor;
    [self setupUI];
    [self getUserData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置navbar
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = nil;

//    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [btn setImage:[UIImage imageNamed:@"shezhi"] forState:0];
//    [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:false animated:true];
}


- (void)setupUI{
    
//    UIView *view = [UIView new];
//    [self.view addSubview:view];
//    view.backgroundColor = APPColor;
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.mas_equalTo(self.view);
//        make.height.mas_equalTo(200);
//    }];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.sellerTabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
   AdjustsScrollViewInsetNever(self, self.TabelView);
   AdjustsScrollViewInsetNever(self, self.sellerTabelView);
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserData) name:refreshUserVC object:nil];
}

- (void)click{
    MineSettingVC *vc = [[MineSettingVC alloc] initWithNibName:@"MineSettingVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark --- getdata ---


- (void)getUserData{
    [JMBManager showLoading];
    WeakSelf(self)
    [self.mineApi getUserInfoWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSArray *arr = result.resultDic[@"list"];
            self.dataDic = arr.firstObject;
            if ([userLevelGrade integerValue] >= 3) {
                self.TabelView.hidden = false;
                self.sellerTabelView.hidden = true;
                [self.TabelView reloadData];
            }else{
                self.sellerTabelView.hidden = true;
                self.TabelView.hidden = false;
                [self.TabelView reloadData];
            }
            
            
        }
        [self.TabelView.mj_header endRefreshing];
        [JMBManager hideAlert];
    }];
}

//- (void)getMeunData{
//    [self.mineApi getMyUtilitiesWithparameters:@"1001" withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//            NSInteger isclerk = [self.dataDic[@"isClerk"] integerValue];
//
//            if (isclerk == 0) {
//                self.menuArray = result.result;
////                [self.menuArray objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 6)]]
//            }
//            [self.TabelView reloadData];
//        }
//    }];
//}

#pragma mark --- UItableDelegate ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger isClerk = [self.dataDic[@"isClerk"] integerValue];
    if (tableView.tag == 3000) {
        return isClerk > 0 ? 4 :3;
    }else{
        return 3;
    }
//    return [userLevelGrade integerValue] >= 3 ? 3 : 4;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        if (tableView.tag == 3000) {
            static NSString *Identifier = @"SellerInfoOneCell";
            SellerInfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
            if (cell == nil) {
                cell = [[SellerInfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
            } 
             [cell setupData:self.dataDic];
           
         
            WeakSelf(self)
            cell.clickBlock = ^(NSInteger type) {
                StrongSelf(self)
                if (type == 1) {
                    self.sellerTabelView.hidden = false;
                    self.TabelView.hidden = true;
                    [self.sellerTabelView reloadData];
                }else if (type == 2){
                    [self click];
                }
            };
            return cell;
        }else{
            static NSString *Identifier = @"MyInfoOneCell";
            MyInfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
            if (cell == nil) {
                cell = [[MyInfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
            }
            [cell setupData:self.dataDic];
         
            WeakSelf(self)
            cell.clickBlock = ^(NSInteger type) {
                StrongSelf(self)
                if (type == 1) {
                    self.sellerTabelView.hidden = true;
                    self.TabelView.hidden = false;
                    [self.TabelView reloadData];
                }else if (type == 2){
                    [self click];
                }
            };
            return cell;
        }
        
       
    }else if(indexPath.section == 1){
        if (tableView.tag == 3001) {
            static NSString *Identifier = @"SellerOrderBtnTableCell";
            SellerOrderBtnTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
            if (cell == nil) {
                cell = [[SellerOrderBtnTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
            }
            WeakSelf(self)
            cell.clickBlock = ^(NSInteger index, NSString * _Nonnull title) {
                StrongSelf(self)
                [self click:index+100];
            };
            return cell;
        }else{
            static NSString *Identifier = @"OrderFuncCell";
                 OrderFuncCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
                 if (cell == nil) {
                     cell = [[OrderFuncCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
                 }
                 
                 cell.clickBlock = ^(NSInteger index) {
                     if (index == 0) {
                         
                         MyGoodOrderListVC *vc = [MyGoodOrderListVC new];
                         [self.navigationController pushViewController:vc animated:true];
                         
                     }else if(index == 1){
                         
                         MyWeixiuTaocanOrderVC *vc = [MyWeixiuTaocanOrderVC new];
                         [self.navigationController pushViewController:vc animated:true];
                         
                     }else if (index == 2){
                         
                         OrderListVC *vc = [OrderListVC new];
                         [self.navigationController pushViewController:vc animated:true];
                        
                     }else if(index == 3){
                                         
                         FixOrderManageVC *vc = [[FixOrderManageVC alloc] initWithNibName:@"FixOrderManageVC" bundle:nil];
                         [self.navigationController pushViewController:vc animated:true];
                        
                     }else{
                         RefundOrderListVC *vc = [RefundOrderListVC new];
                         [self.navigationController pushViewController:vc animated:true];
                     }

                    
                 };
                 
                 return cell;
        }
     
    }

      else if(indexPath.section == 2){
          if (tableView.tag == 3001) {
              static NSString *Identifier = @"SellerUtilitiesTableCell";
              SellerUtilitiesTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
              if (cell == nil) {
                  cell = [[SellerUtilitiesTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
              }
              [self sellCentenrClick:cell];
              return cell;
          }else{
              static NSString *Identifier = @"UtilFuncCell";
              UtilFuncCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
              if (cell == nil) {
                  cell = [[UtilFuncCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
              }
              
              [cell setupData:self.dataDic];
              [self cllClickEvent:cell];
              
              return cell;
          }
        
       
      }else if (indexPath.section == 3){
          static NSString *Identifier = @"CommunityTableCell";
          CommunityTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
          if (cell == nil) {
              cell = [[CommunityTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
          }
          [self communityCellClick:cell];
          return cell;
      }
    
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     NSInteger isClerk = [self.dataDic[@"isClerk"] integerValue];
    if (indexPath.section == 0) {
        return 140.0f;
    }else if(indexPath.section == 1){
        if (tableView.tag == 3001) {
             return 180.0f;
        }else{
            return 80.0f;
        }
       
    }else if(indexPath.section == 2){
        if (tableView.tag == 3001) {
            return 125.0f;
        }else{
//            return isClerk == 1 ? 330 : 270.0f;
            return 270.0f;
        }

        
    }else if(indexPath.section == 3){
        
          return 125.0f;
        
    }else{
        return CGFLOAT_MIN;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [self.navigationController pushViewController:[TuanGouGoodInfoVC new] animated:true];
}
#pragma mark ClcikEvent

- (void)cllClickEvent:(UtilFuncCell *)cell{
    cell.clickBlock = ^(NSInteger index,NSString *title) {
        if ([title isEqualToString:@"地址管理"]) {
             [self.navigationController pushViewController:[AdressListVC new] animated:true];
        }else if ([title isEqualToString:@"发票管理"]){
            [self.navigationController pushViewController:[FapiaoVC new] animated:true];
        }else if ([title isEqualToString:@"用户反馈"]){
            SuggestionFeedBackVC *vc = [[SuggestionFeedBackVC alloc] initWithNibName:@"SuggestionFeedBackVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"我的钱包"]){
            MineWalletVC *vc = [[MineWalletVC alloc] initWithNibName:@"MineWalletVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"申请批发"]){
            ApplyWholesaleVC *vc = [[ApplyWholesaleVC alloc] initWithNibName:@"ApplyWholesaleVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
//            [self.mineApi selectShopWithparameters:@{@"ShopId":@99,@"OrderNumber":@"F954118021441"} withCompletionHandler:^(NSError *error, MessageBody *result) {
//                if (result.code == 200) {
//                    [JMBManager showBriefAlert:@"派单成功"];
//                }
//            }];
        }else if ([title isEqualToString:@"我的卡包"]){
            MineCardPackageVC *vc = [[MineCardPackageVC alloc] initWithNibName:@"MineCardPackageVC" bundle:nil];
                       [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"业务管理"]){
            MineBusinessMangeVC *vc = [[MineBusinessMangeVC alloc] initWithNibName:@"MineBusinessMangeVC" bundle:nil];
            vc.dataDic = self.dataDic;
                       [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"商家入驻"]){
            MerchantSettledVC *vc = [[MerchantSettledVC alloc] init];
                       [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"服务经理"]){
            [self checkMyManager];
        }else if ([title isEqualToString:@"全车保修"]){
            ServicesOfYearOrderVC *vc = [ServicesOfYearOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"客服电话"]){
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"请确认您要拨打的联系电话号码" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"400-163-8887" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                 [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",@"400-163-8887"]]];
            }];
            [alert addAction:action1];
            [alert addAction:action2];
            [self presentViewController:alert animated:YES completion:nil];
        }else if ([title isEqualToString:@"一年保养"]){
            CarMaintainenceOrderVC *vc = [CarMaintainenceOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"申请开店"]){
            [JMBManager showLoading];
            [self.mineApi getApplyShopStatuesWithparameters:nil withCompletionHandler:^(NSError *error, MessageBody *result) {
                [JMBManager hideAlert];
                if (result.code == 200) {
                    NSArray *datas = result.result;
                    NSDictionary *dic = datas.firstObject;
                    NSInteger stautues = [dic[@"sellPassed"] integerValue];
                    if (stautues == 0) {
                        
                        ApplyforShopVC *vc = [ApplyforShopVC new];
                        [self.navigationController pushViewController:vc animated:true];
                        
                    }else{
                        ApplyShopSatuesVC *vc = [ApplyShopSatuesVC new];
                        vc.dic = dic;
                        [self.navigationController pushViewController:vc animated:true];
                    }
                }
                
            }];

        }else if ([title isEqualToString:@"成为团长"]){
            ToBeLeaderVC *vc =[ToBeLeaderVC new];
            vc.clickBlock = ^{
                [self getUserData];
            };
            [self.navigationController pushViewController:vc animated:true];
        }
       
    };
}

- (void)checkMyManager{
    [self.mineApi checkMyServiceManagerWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *data = result.result;
            if ([data.firstObject isEqual:[NSNull null]]) {
//                [JMBManager showBriefAlert:@"没绑定"];
                ToBindServiceManager *vc = [[ToBindServiceManager alloc] initWithNibName:@"ToBindServiceManager" bundle:nil];
                [self.navigationController pushViewController:vc animated:true];
            }else{
                MineServiceManagerVC *vc = [[MineServiceManagerVC alloc] initWithNibName:@"MineServiceManagerVC" bundle:nil];
                vc.dic = data.firstObject;
                [self.navigationController pushViewController:vc animated:true];
//                [JMBManager showBriefAlert:@"已绑定"];
            }
        }
    }];
}

- (void)sellCentenrClick:(SellerUtilitiesTableCell *)cell{

    
    cell.clickBlock = ^(NSInteger index, NSString * _Nonnull title) {
        if ([title isEqualToString:@"订单管理"]) {
            SellerOrderManagerVC *vc = [[SellerOrderManagerVC alloc] initWithNibName:@"SellerOrderManagerVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"维修套餐管理"]){
            
        }else if ([title isEqualToString:@"商品管理"]){
            SellerGoodManagerViewController *vc = [SellerGoodManagerViewController new];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"服务卡查询"]){
//            SellerFixManagerVC *vc = [[SellerFixManagerVC alloc] initWithNibName:@"SellerFixManagerVC" bundle:nil];
//            [self.navigationController pushViewController:vc animated:true];
            FixCardSearchVC *vc = [[FixCardSearchVC alloc] initWithNibName:@"FixCardSearchVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"抢单中心"]){
            SellerCenterGradOrderVC *vc = [SellerCenterGradOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"店铺设置"]){
            [JMBManager showLoading];
            [self.mineApi getApplyShopStatuesWithparameters:nil withCompletionHandler:^(NSError *error, MessageBody *result) {
                [JMBManager hideAlert];
                if (result.code == 200) {
                    NSArray *datas = result.result;
                    NSDictionary *dic = datas.firstObject;
                    NSInteger stautues = [dic[@"sellPassed"] integerValue];
                    if (stautues == 2) {
                        ShopSetVC *vc = [[ShopSetVC alloc] initWithNibName:@"ShopSetVC" bundle:nil];
                        vc.dic = dic;
                        [self.navigationController pushViewController:vc animated:true];
                        
                    }else{
                        [JMBManager showBriefAlert:@"暂无权限"];
                    }
                }
                
            }];
        }
    };
}

- (void)communityCellClick:(CommunityTableCell *)cell{
    cell.clickBlock = ^(NSInteger index, NSString * _Nonnull title) {
        if ([title isEqualToString:@"我是团长"]) {
            LeaderInfoVC *vc = [LeaderInfoVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"我的项目"]){
            MyProjectVC *vc = [MyProjectVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"我的业绩"]){
            MinePerformanceVC *vc = [MinePerformanceVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if ([title isEqualToString:@"邀请好友"]){
            InviteFriendVC *vc = [InviteFriendVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
    };
}

- (void)click:(NSInteger)curTag{
    switch (curTag) {
        case 100:
        {
            SellerCenterGoodsOrderVC *vc = [SellerCenterGoodsOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 101:
        {
            SellerFixOrderListVC *vc = [SellerFixOrderListVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 102:
        {

            SellerCarMaintainenceOrderVC *vc = [SellerCarMaintainenceOrderVC new];
            vc.type = 2;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
            
        case 103:
        {

            SellerCarMaintainenceOrderVC *vc = [SellerCarMaintainenceOrderVC new];
            vc.type = 1;
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 104:{

              SellerOrderRefundVC *vc = [SellerOrderRefundVC new];
             [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 105:{

            SellerRepairOrderVC *vc = [SellerRepairOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 106:{
            
            SellerRepairOrderVC *vc = [SellerRepairOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 107:{
            SellerCenteWeixiuTaocanOrderVC *vc = [SellerCenteWeixiuTaocanOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        default:
            break;
    }
}

#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
//        _TabelView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        _TabelView.estimatedSectionFooterHeight = 0;
        _TabelView.estimatedSectionHeaderHeight = 0;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"MyInfoOneCell" bundle:nil] forCellReuseIdentifier:@"MyInfoOneCell"];
        [_TabelView registerClass:[OrderFuncCell class] forCellReuseIdentifier:@"OrderFuncCell"];
        [_TabelView registerClass:[UtilFuncCell class] forCellReuseIdentifier:@"UtilFuncCell"];
        _TabelView.backgroundColor = [UIColor clearColor];
        [_TabelView registerClass:[CommunityTableCell class] forCellReuseIdentifier:@"CommunityTableCell"];
        WeakSelf(self)
        _TabelView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            StrongSelf(self)
            
            [self getUserData];
        }];
//        _TabelView.hidden = true;
        _TabelView.tag = 3000;
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

#pragma mark -get
-(UITableView *)sellerTabelView
{
    if(_sellerTabelView==nil)
    {
        _sellerTabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_sellerTabelView setDelegate:self];
        [_sellerTabelView setDataSource:self];
        [_sellerTabelView setTableFooterView:[UIView new]];
        _sellerTabelView.rowHeight = UITableViewAutomaticDimension;
        _sellerTabelView.estimatedRowHeight = 44;
        _sellerTabelView.showsVerticalScrollIndicator = false;
        [_sellerTabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_sellerTabelView registerNib:[UINib nibWithNibName:@"MyInfoOneCell" bundle:nil] forCellReuseIdentifier:@"MyInfoOneCell"];
        [_sellerTabelView registerClass:[SellerOrderBtnTableCell class] forCellReuseIdentifier:@"SellerOrderBtnTableCell"];
        [_sellerTabelView registerClass:[SellerUtilitiesTableCell class] forCellReuseIdentifier:@"SellerUtilitiesTableCell"];
        _sellerTabelView.backgroundColor = UIColor.clearColor;
        _sellerTabelView.hidden = true;
        _sellerTabelView.tag = 3001;
        [self.view addSubview:_sellerTabelView];
    }
    return _sellerTabelView;
}

- (MineApi *)mineApi{
    if (!_mineApi) {
        _mineApi = [MineApi new];
    }
    return _mineApi;
}


@end
