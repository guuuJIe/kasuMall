//
//  BankCardVC.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BankCardVC : BaseViewController
@property (nonatomic,copy) void(^itemBlock)(NSDictionary *dic);
@end

NS_ASSUME_NONNULL_END
