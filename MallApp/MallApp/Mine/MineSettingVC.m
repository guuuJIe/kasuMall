//
//  MineSettingVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineSettingVC.h"
//#import "LoginVC.h"
#import "MineApi.h"
#import "PersonInfoVC.h"
#import "MineServiceManagerVC.h"
#import "ToBindServiceManager.h"
#import "ChangePwdVC.h"
#import "SetPayVC.h"

#import "NewLoginVC.h"
#import "AccountApi.h"
#import "PayMannagerUtil.h"
#import "NewBindUserVC.h"
#import "NewOtherLoginVC.h"
#import "AccountApi.h"
#import <UMVerify/UMVerify.h>

//#import "ProgressHUD.h"
#import "UMModelCreate.h"
#import <CloudPushSDK/CloudPushSDK.h>
@interface MineSettingVC ()
@property (weak, nonatomic) IBOutlet UIView *logout;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *subNameLbl;
@property (nonatomic,strong) MineApi *api;
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (nonatomic,strong) NSDictionary *dic;
@property (nonatomic) AccountApi *accountApi;
@property (weak, nonatomic) IBOutlet UIView *ServiceView;
@property (weak, nonatomic) IBOutlet UIView *changePwdView;
@property (weak, nonatomic) IBOutlet UIImageView *userHeadImg;
@property (weak, nonatomic) IBOutlet UILabel *versionLbl;
@property (weak, nonatomic) IBOutlet UIView *resetCarInfoView;
@end

@implementation MineSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"设置";
    [self.logout addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(logoutAct)]];
    [self.headView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpMyInfo)]];
    [self.ServiceView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(setPayPwd)]];
    [self.changePwdView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changePwd)]];
    [self.resetCarInfoView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resetCarInfo)]];
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];  
    // app版本
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    self.versionLbl.text = [NSString stringWithFormat:@"V %@",app_Version];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getUserData];
}

- (void)logoutAct{
//    [self resetDefaults];

    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"是否退出当前账号" andHandler:^(NSInteger index) {
        if (index == 1) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"accessToken"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"refreshToken"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"refreshToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
//            [XJUtil resetDefaults];
//            [JPUSHService deleteAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
//
//            } seq:0];
//            LoginVC *vc = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//            BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//            vc2.modalPresentationStyle = 0;
//            [self presentViewController:vc2 animated:true completion:nil];
//            [self.navigationController popViewControllerAnimated:true];
            self.tabBarController.selectedIndex = 0;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:false];
            });
           
            
            [CloudPushSDK removeAlias:[NSString stringWithFormat:@"%@",usersID] withCallback:^(CloudPushCallbackResult *res) {
                
            }];
        }
    }];
}

- (void)setPayPwd{
    NSInteger ispay = [self.dic[@"isPayPassword"] integerValue];
//    if (ispay == 0) {//未设置支付密码
    SetPayVC *vc = [[SetPayVC alloc] initWithNibName:@"SetPayVC" bundle:nil];
    vc.type = ispay;
    vc.tel = self.dic[@"mobile"];
    [self.navigationController pushViewController:vc animated:true];
//    }else{
//
//    }
    
}

- (void)jumpMyInfo{
    PersonInfoVC *vc = [[PersonInfoVC alloc] initWithNibName:@"PersonInfoVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)changePwd{
    ChangePwdVC *vc = [[ChangePwdVC alloc] initWithNibName:@"ChangePwdVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)resetCarInfo {
    [JMBManager showBriefAlert:@"清除成功"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mycarName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mycarmodelName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshHomeData" object:nil];
}


- (void)getUserData{
    [self.api getUserInfoWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *arr = result.result;
            self.dic = arr.firstObject;
            [self configData:self.dic];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"userLogo"]];
          [self.userHeadImg yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"defaultHead"]];
    self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"userName"] andDefailtInfo:@""];
    self.subNameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"userName"] andDefailtInfo:@""];
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    
    return _api;
}


- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}

@end
