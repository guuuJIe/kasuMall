//
//  AddressLocationVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/22.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddressLocationVC.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "AdressLocationTableCell.h"

@interface AddressLocationVC ()<UITextFieldDelegate,MAMapViewDelegate,AMapSearchDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITextField *selectText;
@property(nonatomic,strong)UIButton *selectBtn;
@property(nonatomic,strong)MAMapView *mapView;
@property(nonatomic,strong)UIImageView *iconImage;
@property(nonatomic,strong)UITableView *listTableView;
@property(nonatomic,strong)NSArray<DDSearchPoi *> *poiArray;
@property(nonatomic,strong)AMapLocationManager *locManager;
@end

@implementation AddressLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
//    [self searchLoc];
    [self getSiteByUserloc];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initBar];
}



-(void)initBar{
    
    self.navigationItem.leftBarButtonItem = nil;
    UIView *selectView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-80, 30)];
    selectView.backgroundColor=[UIColor whiteColor];
    selectView.layer.cornerRadius=4;
    selectView.layer.masksToBounds=YES;
    
    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 5, selectView.frame.size.height-5*2, selectView.frame.size.height-5*2)];
    selectIcon.image=[UIImage imageNamed:@"select_black_icon"];
    
    self.selectText=[[UITextField alloc]initWithFrame:CGRectMake(Default_Space, 0, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
    self.selectText.textColor=APPFourColor;
    self.selectText.placeholder=@"输入关键字";
    self.selectText.returnKeyType = UIReturnKeySearch;
    self.selectText.delegate = self;
    self.selectText.font=LabelFont14;
    if (@available(iOS 13.0, *)) {
        
        self.selectText.attributedPlaceholder=[[NSAttributedString alloc]initWithString:self.selectText.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];

      
    }else{
        [self.selectText setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
    }
    [selectView addSubview:selectIcon];
    [selectView addSubview:self.selectText];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:selectView];
    
    _selectBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,0,44,40)];
    [_selectBtn setTitle:@"取消" forState:(UIControlStateNormal)];
    [_selectBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    _selectBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_selectBtn addTarget:self action:@selector(popVC) forControlEvents:(UIControlEventTouchUpInside)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_selectBtn];

}

- (void)setupUI{
    [self.view addSubview:self.mapView];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.height.mas_equalTo(Screen_Width *0.5);
    }];
    
    [self.mapView addSubview:self.iconImage];
    [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.mapView);
        make.size.mas_equalTo(CGSizeMake(20, 40));
    }];
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mapView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.poiArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    AdressLocationTableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AdressLocationTableCell" forIndexPath:indexPath];
    if(cell==nil){
        cell=[[AdressLocationTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AdressLocationTableCell"];
    }
    DDSearchPoi *poi=[self.poiArray objectAtIndex:indexPath.row];
    [cell showData:poi index:(int)indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.block) {
        self.block(self.poiArray[indexPath.row]);
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (void)searchLoc:(NSString *)keyword{
    [[DDSearchManager sharedManager] keyWordsSearch:keyword city:@"温州" returnBlock:^(NSArray<__kindof DDSearchPoi *> *pois) {
        
        self.poiArray = pois;
        [self.listTableView reloadData];
    }];
    
}
- (void)getSiteByUserloc{
    WeakSelf(self)
    [self.locManager requestLocationWithReGeocode:true completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        CLLocationCoordinate2D cordin = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);

        [[DDSearchManager sharedManager] keyWordsAroundSearch:@"汽修厂" city:@"温州" withReGeocode:cordin returnBlock:^(NSArray<__kindof DDSearchPoi *> *pois) {
//            for (DDSearchPoi *annotation in pois) {
//                JLog(@"%@ %@ (%f,%f)",annotation.name,annotation.address,annotation.coordinate.latitude,annotation.coordinate.longitude);
//            }
            weakself.poiArray = pois;
            [weakself.listTableView reloadData];
//            [self.mapView setCenterCoordinate:cll]
        }];
    }];
   
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length < 0) {
        return false;
    }
    [self searchLoc:textField.text];
    return YES;
}

- (void)popVC{
    [self.navigationController popViewControllerAnimated:true];
}

- (MAMapView *)mapView{
    if (!_mapView) {
        _mapView = [MAMapView new];
        _mapView.delegate = self;
        self.mapView.rotateEnabled=NO;
        self.mapView.zoomLevel=18;
        self.mapView.mapType=MAMapTypeStandard;
        self.mapView.userTrackingMode=MAUserTrackingModeFollow;
    }
    
    return _mapView;
}

- (UIImageView *)iconImage{
    if (!_iconImage) {
        _iconImage = [UIImageView new];
        _iconImage.image = [UIImage imageNamed:@"location_red"];
    }
    
    return _iconImage;
}

- (UITableView *)listTableView{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        [_listTableView registerClass:[AdressLocationTableCell class] forCellReuseIdentifier:@"AdressLocationTableCell"];
        [self.view addSubview:_listTableView];
    }
    
    return _listTableView;
}

- (AMapLocationManager *)locManager{
    if (!_locManager) {
        _locManager = [AMapLocationManager new];
//        [_locManager setDelegate:self];
        //设置期望定位精度
        [self.locManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
        
        //设置不允许系统暂停定位
        [self.locManager setPausesLocationUpdatesAutomatically:NO];
        
        //设置允许在后台定位
        [self.locManager setAllowsBackgroundLocationUpdates:false];
        
//        //设置定位超时时间
//        [self.locManager setLocationTimeout:DefaultLocationTimeout];
//
//        //设置逆地理超时时间
//        [self.locManager setReGeocodeTimeout:DefaultReGeocodeTimeout];
        
        //设置开启虚拟定位风险监测，可以根据需要开启
        [self.locManager setDetectRiskOfFakeLocation:NO];
//        [self.mapView removeAnnotations:self.mapView.annotations];

    }
    
    return _locManager;
}
@end
