//
//  MinExchangeRecodeVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MineBackRecordVC.h"
#import "MineBackRecordCell.h"
#import "MinePayOrderDetailVC.h"
#import "MineApi.h"
@interface MineBackRecordVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UITableView *listTableView;
@property (nonatomic) MineApi *api;
@property (nonatomic) NSMutableArray *dataArr;
@end

@implementation MineBackRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    [self getData];
}


- (void)setupUI{
    self.title = @"返还记录";
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)getData{
    [self.api getBackRecordWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArr = result.resultDic[@"list"];
            [self.listTableView reloadData];
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"MineBackRecordCell";
    MineBackRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[MineBackRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;

        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
     
        [_listTableView registerNib:[UINib nibWithNibName:@"MineBackRecordCell" bundle:nil] forCellReuseIdentifier:@"MineBackRecordCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = UIColorF5F7;

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (MineApi *)api{
    if (!_api) {
        _api = [MineApi new];
    }
    return _api;
}

@end
