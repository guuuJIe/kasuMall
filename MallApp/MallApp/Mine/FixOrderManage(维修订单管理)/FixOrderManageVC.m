//
//  FixOrderManageVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "FixOrderManageVC.h"
#import "MineFixOrderVC.h"
#import "MineEmergencyOrderVC.h"
@interface FixOrderManageVC ()

@property (weak, nonatomic) IBOutlet UIView *qiangxiuView;
@property (weak, nonatomic) IBOutlet UIView *shijiuView;
@property (weak, nonatomic) IBOutlet UIView *weixiuView;

@end

@implementation FixOrderManageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"订单管理";
    [self.qiangxiuView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.shijiuView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.weixiuView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

- (void)click:(UITapGestureRecognizer *)ges{
    switch (ges.view.tag) {
        case 100:
        {
            MineEmergencyOrderVC *vc = [MineEmergencyOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 101:
        {
            MineEmergencyOrderVC *vc = [MineEmergencyOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        case 102:
        {
            MineFixOrderVC *vc = [MineFixOrderVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
            break;
        default:
            break;
    }
}


@end
