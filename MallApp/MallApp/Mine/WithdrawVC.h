//
//  WithdrawVC.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *selectBankView;
@property (weak, nonatomic) IBOutlet UILabel *banktype;
@property (weak, nonatomic) IBOutlet UILabel *bankNum;
- (IBAction)submit:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *moneyText;
@property (nonatomic, strong) NSString *moneyStr;
@end

NS_ASSUME_NONNULL_END
