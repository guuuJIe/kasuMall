//
//  NumView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NumView : UIView
- (IBAction)add:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
- (IBAction)reduce:(UIButton *)sender;
/**
 加
 */
@property (nonatomic, copy) void (^AddBlock)(UILabel *countLabel);

/**
 减
 */
@property (nonatomic, copy) void (^CutBlock)(UILabel *countLabel);
@end

NS_ASSUME_NONNULL_END
