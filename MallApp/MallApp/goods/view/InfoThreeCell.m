//
//  InfoThreeCell.m
//  RebateApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InfoThreeCell.h"
#import "ArgumentView.h"
#import "GoodsTypeView.h"
@interface InfoThreeCell()
@property (nonatomic) ArgumentView *arugView;
@property (nonatomic) GoodsTypeView *typeView;
@end


@implementation InfoThreeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)selectAction:(id)sender {
//    [self.typeView show];
    if (self.itemBlock) {
        self.itemBlock();
    }
    
}
- (IBAction)checkArgu:(id)sender {
    [self.arugView show];
    
}

- (ArgumentView *)arugView{
    if (!_arugView) {
        _arugView = [ArgumentView new];
       
       
//        [_arugView addSubview:effectView];
//        [self.arugView addSubview:_arugView];
    }
    return _arugView;
}

- (void)setupData:(NSMutableArray *)data{
    if (data.count == 0) {
        return;
    }
    self.arugView.arr = data;
    
}

- (GoodsTypeView *)typeView{
    if (!_typeView) {
        _typeView = [GoodsTypeView new];
//        [self.arugView addSubview:_arugView];
    }
    return _typeView;
}

@end
