//
//  InfoFourCell.m
//  RebateApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InfoFourCell.h"

@interface InfoFourCell()

@end

@implementation InfoFourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.backgroundColor = [UIColor purpleColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    
    [self.contentView addSubview:self.wkwebView];
    [self.wkwebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
//
//    NSString *webString = @"<p><img src=
//    "https://gd2.alicdn.com/imgextra/i2/827576580/TB2lBaOXJMnyKJjSZPhXXaeZVXa_!!827576580.jpg"/></p>
//    ";
//
//     [self.wkwebView loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",webString] baseURL:nil];
   
    
//    [self.wkwebView loadRequest:[NSURL URLWithString:@"https://blog.csdn.net/weixin_33949359/article/details/92646186"]];
//     [self.wkwebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.jianshu.com/p/8cdad2282d24"]]];
    [self setdata];
}


- (void)setdata{
//     [self.wkwebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.jianshu.com/p/8cdad2282d24"]]];
}


- (WKWebView *)wkwebView {
    if (!_wkwebView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 偏好设置
        config.preferences = [[WKPreferences alloc] init];
        config.preferences.minimumFontSize = 10;
        config.preferences.javaScriptEnabled = YES;
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;


        WKUserContentController *userContentController = [[WKUserContentController alloc] init];
//        WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:cookie injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
//        [userContentController addUserScript:cookieScript];

//        [userContentController addScriptMessageHandler:self name:@"share"];
//        [userContentController addScriptMessageHandler:self name:@"login"];

        config.userContentController = userContentController;
        config.selectionGranularity = WKSelectionGranularityDynamic;
        config.allowsInlineMediaPlayback = YES;
//        config.mediaTypesRequiringUserActionForPlayback = false;
        
      
        _wkwebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 1)];
        
//        _wkwebView.navigationDelegate = self;//导航代理
//        _wkwebView.UIDelegate = self;//UI代理
        _wkwebView.backgroundColor = [UIColor whiteColor];
        _wkwebView.allowsBackForwardNavigationGestures = YES;
        _wkwebView.scrollView.scrollEnabled = false;
       
    }
    return _wkwebView;
}


@end
