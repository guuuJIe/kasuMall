//
//  InfoOneCell.m
//  RebateApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InfoOneCell.h"
#import "SDCycleScrollView.h"
@interface InfoOneCell()
@property (nonatomic) SDCycleScrollView *focusHeadView;
@end

@implementation InfoOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupLayout];
        
    }
    return self;
}

- (void)setupLayout{
    [self.focusHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
    
    
}

-(void)setDic:(NSDictionary *)dic{
    _dic = dic;
//    if (self.dic) {
//        NSString *pic = [NSString stringWithFormat:@"%@%@",URLAddress,self.dic[@"bigPic"]];
//        [self.focusHeadView setImageURLStringsGroup:@[pic]];
//    }
}

- (void)setDataArr:(NSMutableArray *)dataArr{
//    if (dataArr.count == 0) {
////        if (!self.dic) {
////            NSString *pic = [NSString stringWithFormat:@"%@%@",URLAddress,self.dic[@"bigPic"]];
////            [self.focusHeadView setImageURLStringsGroup:@[pic]];
////        }
////
//    }else{
        [self.focusHeadView setImageURLStringsGroup:dataArr];
//    }
   
}


- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *images = [NSString stringWithFormat:@"%@",dic[@"morePic"]];
        NSString *smallPic = [NSString stringWithFormat:@"%@",dic[@"smallPic"]];
        NSArray * array = [images componentsSeparatedByString:@","];
        JLog(@"%@",array);
        NSMutableArray *datasArray = [NSMutableArray array];
        if (![array.firstObject isEqual:@""]) {
            for (int i = 0; i<array.count; i++) {
                [datasArray addObject:[NSString stringWithFormat:@"%@Uploadfiles/MorePic/%@",URLAddress,array[i]]];
            }
        }
       
        
        [datasArray addObject:[NSString stringWithFormat:@"%@%@",URLAddress,smallPic]];
        [self.focusHeadView setImageURLStringsGroup:datasArray];
    }
}

#pragma mark ----UI---
-(SDCycleScrollView *)focusHeadView{
    if (!_focusHeadView) {
        _focusHeadView = [SDCycleScrollView new];
        _focusHeadView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
        //        _focusHeadView.currentPageDotColor = ThemeColor;
        _focusHeadView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
//        _focusHeadView.placeholderImage = PlaceholderImage;
        _focusHeadView.tag = 200;
//        _focusHeadView.delegate = self;
        _focusHeadView.backgroundColor = [UIColor whiteColor];
#ifdef DEBUG
        
        [_focusHeadView setImageURLStringsGroup:@[@"http://cdnimg.jsojs.com/goods_item1/300085/icon.jpg@!414",@"http://cdnimg.jsojs.com/goods_item1/300085/icon06.jpg@!414"]];
#else
        
#endif
        [self.contentView addSubview:_focusHeadView];
        
    }
    return _focusHeadView;
}

@end
