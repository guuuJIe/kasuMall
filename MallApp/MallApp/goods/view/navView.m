//
//  navView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "navView.h"
@interface navView()

@end

@implementation navView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    [self addSubview:self.backButton];
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.bottom.equalTo(@(-10));
//        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
}


- (void)back{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

-(UIButton *)backButton
{
    if(!_backButton)
    {
        _backButton=[UIButton  new];
      
        [_backButton setImage:[UIImage imageNamed:@"返回"] forState:0];
        [_backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _backButton;
}

@end
