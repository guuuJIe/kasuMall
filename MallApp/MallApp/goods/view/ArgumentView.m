//
//  ArgumentView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ArgumentView.h"
#import "ArguTableCell.h"
@interface ArgumentView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIButton *chaButton;
@property (nonatomic)UITableView *TabelView;
@end

@implementation ArgumentView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
//        //实现模糊效果
//        UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//        //毛玻璃视图
//        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
//        effectView.alpha = 0.7f;
//        effectView.frame = CGRectMake(0, 0, Screen_Width, Screen_Height);
//        
//        [self addSubview:effectView];
        
        
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(356);
        }];
        
        [bgView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@12);
            make.top.mas_equalTo(16);
        }];
        
        [bgView addSubview:self.chaButton];
        [self.chaButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-12));
            make.top.equalTo(@16);
        }];
        
        UIView *line = [UIView new];
        [bgView addSubview:line];
        line.backgroundColor = UIColorEF;
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(16);
            make.height.mas_equalTo(lineHeihgt);
        }];
        
        [bgView addSubview:self.TabelView];
        [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom).offset(10);
            make.left.right.equalTo(bgView);
            make.bottom.mas_equalTo(-10);
        }];
               
    }
    return self;
}

- (void)dismissView{
    [self hide];
}

-(void)setArr:(NSMutableArray *)arr{
    _arr = arr;
    [self.TabelView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *Identifier = @"ArguTableCell";
    ArguTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[ArguTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    
    [cell setupData:self.arr[indexPath.row] andNSIndexPath:indexPath];
    
    return cell;
}


-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"参数";
        _titleLabel.font = LabelFont14;
        _titleLabel.textColor = UIColor333;
        
    }
    return _titleLabel;
}

-(UIButton *)chaButton
{
    if(!_chaButton)
    {
        _chaButton=[UIButton  new];
      
        [_chaButton setImage:[UIImage imageNamed:@"关闭"] forState:0];
        [_chaButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _chaButton;
}

#pragma mark -get
-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
//        [_TabelView setRowHeight:50];
        _TabelView.estimatedRowHeight = 50;
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerNib:[UINib nibWithNibName:@"ArguTableCell" bundle:nil] forCellReuseIdentifier:@"ArguTableCell"];
        _TabelView.showsVerticalScrollIndicator = false;
//        _TabelView.scrollEnabled = false;
        //        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

@end
