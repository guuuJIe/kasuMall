//
//  ArgumentView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArgumentView : MMPopupView
@property (nonatomic,strong)NSMutableArray *arr;
@end

NS_ASSUME_NONNULL_END
