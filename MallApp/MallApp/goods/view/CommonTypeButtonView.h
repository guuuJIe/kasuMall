//
//  CommonTypeButtonView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonTypeButtonView : UIView
@property (nonatomic,copy) void(^itemClickBlock)(NSInteger index,NSString *title,NSDictionary *dic);
@property (nonatomic,strong)UIButton *selectedBtn;
-(void)showTagarray:(NSArray *)array;


@end

NS_ASSUME_NONNULL_END
