//
//  CommonTypeButtonView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CommonTypeButtonView.h"
#import "NSString+Extend.h"
@interface CommonTypeButtonView()
@property (nonatomic, strong) NSArray *datas;
@end
@implementation CommonTypeButtonView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.userInteractionEnabled = true;
    }
    return self;
}

-(void)showTagarray:(NSArray *)array{
    if (!array) {
        return;
    }
    _datas = array;
    
    for (UIView *view in self.subviews){
        [view removeFromSuperview];
    }
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=36;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        NSDictionary *dic = array[i];
        UIFont *titleFont=LabelFont13;
        NSString *titleStr=dic[@"name"];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(tagNowX, tagNowLine*tagItemHeight, itemWidth, tagItemHeight)];
        view.backgroundColor=[UIColor clearColor];
        view.userInteractionEnabled = true;//要设置 要不然响应时间传递不了
        UIButton *title=[[UIButton alloc]initWithFrame:CGRectMake(Default_Space, 5, itemWidth-Default_Space*2, tagItemHeight-Default_Space)];
        [title setTitleColor:UIColor333 forState:0];
//        title.textAlignment=NSTextAlignmentCenter;
        [title setTitle:titleStr forState:0];
        [title.titleLabel setFont:titleFont];
        title.layer.cornerRadius=12;
//        title.layer.borderColor=APPFourColor.CGColor;
//        title.layer.borderWidth=Default_Line;
        title.layer.masksToBounds=YES;
        title.backgroundColor=UIColorEF;
        [title setTitleColor:APPColor forState:UIControlStateSelected];
        [view addSubview:title];
        [title addTarget:self action:@selector(hisItemClick:) forControlEvents:UIControlEventTouchUpInside];
        title.tag = [dic[@"id"] integerValue];
//        view.tag=i;
//        UITapGestureRecognizer *itemTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hisItemClick:)];
//
//        [view addGestureRecognizer:itemTap];
//
        [self addSubview:view];
        
        tagNowX+=itemWidth;
    }
}


- (void)hisItemClick:(UIButton *)btn
{
    if (btn!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
        self.selectedBtn.selected = YES;
    }
    if (self.itemClickBlock) {
        self.itemClickBlock(btn.tag,btn.titleLabel.text,self.datas[btn.tag - 1]);
    }
}

@end
