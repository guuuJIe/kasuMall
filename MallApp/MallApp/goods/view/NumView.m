//
//  NumView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NumView.h"
@interface NumView()
@property (nonatomic,assign) NSInteger num;
@property (nonatomic , strong) UIButton *addBtn;
@property (nonatomic , strong) UILabel *numLabel;
@property (nonatomic , strong) UIButton *cutBtn;
@end

@implementation NumView

- (instancetype)init{
    self = [super init];
    if (self) {
      
        //数量加按钮
        UIButton *addBtn = [UIButton new];
        [addBtn setBackgroundColor:[UIColor whiteColor]];
        [addBtn setImage:[UIImage imageNamed:@"加_高亮"] forState:UIControlStateNormal];
        [addBtn setImage:[UIImage imageNamed:@"加_高亮"] forState:UIControlStateHighlighted];
        [addBtn addTarget:self action:@selector(addBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        self.addBtn = addBtn;
        //    [self.addBtn setEnlargeEdgeWithTop:10 left:20 bottom:10 right:10];
        [self addSubview:addBtn];
        
        //数量显示
        UILabel* numberLabel = [[UILabel alloc]init];
        numberLabel.textAlignment = NSTextAlignmentCenter;
        numberLabel.text = @"1";
        numberLabel.font = [UIFont systemFontOfSize:13];
        numberLabel.textColor = UIColor333;
        [numberLabel setBackgroundColor:UIColorEF];
        numberLabel.layer.cornerRadius = 1;
        numberLabel.layer.masksToBounds = YES;
        self.numLabel = numberLabel;
        [self addSubview:numberLabel];
        
        //数量减按钮
        UIButton *cutBtn = [UIButton new];
        [cutBtn setImage:[UIImage imageNamed:@"减_高亮"] forState:UIControlStateNormal];
        [cutBtn setImage:[UIImage imageNamed:@"减_高亮"] forState:UIControlStateHighlighted];
        [cutBtn setBackgroundColor:[UIColor whiteColor]];
        [cutBtn addTarget:self action:@selector(cutBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        self.cutBtn = cutBtn;
        //    [self.cutBtn setEnlargeEdgeWithTop:10 left:20 bottom:10 right:10];
        [self addSubview:cutBtn];
        
        [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-1);
            make.top.equalTo(self).offset(1);
            make.bottom.equalTo(self).offset(-1);
            make.width.mas_equalTo(25);
        }];
        [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-1);
            make.top.equalTo(self).offset(1);
            make.right.equalTo(addBtn.mas_left).offset(0);
            make.width.mas_equalTo((50-4));
        }];
        [cutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self).offset(1);
            make.bottom.equalTo(self).offset(-1);
            make.right.equalTo(numberLabel.mas_left).offset(0);
        }];
    }
    return self;
}

- (void)addBtnClick:(UIButton *)sender
{
    NSInteger count = [self.numLabel.text integerValue];
    count++;
    self.numLabel.text = [NSString stringWithFormat:@"%ld", (long)count];
    if (self.AddBlock) {
        self.AddBlock(self.numLabel);
    }
}

- (void)cutBtnClick:(UIButton *)sender
{
    NSInteger count = [self.numLabel.text integerValue];
    count--;
    if (count <= 0) {
        return;
    }
    self.numLabel.text = [NSString stringWithFormat:@"%ld", (long)count];
    if (self.CutBlock) {
        self.CutBlock(self.numLabel);
    }
    
}

- (IBAction)add:(UIButton *)sender {
    self.num++;
    self.numLbl.text = [NSString stringWithFormat:@"%ld",(long)self.num];
    
}
- (IBAction)reduce:(UIButton *)sender {
}
@end
