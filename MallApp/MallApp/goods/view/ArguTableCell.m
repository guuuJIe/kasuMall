//
//  ArguTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ArguTableCell.h"

@implementation ArguTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic andNSIndexPath:(NSIndexPath *)indexpath{
    if (indexpath.row ==4) {
        self.bottomview.hidden = false;
    }else{
        self.bottomview.hidden = true;
    }
    self.Lbl.text = dic[@"name"];
    self.valueLbl.text =dic[@"value"];
}

@end
