//
//  InfoOneCell.h
//  RebateApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfoOneCell : UITableViewCell
- (void)setupData:(NSDictionary *)dic;
@property (nonatomic)NSMutableArray *dataArr;
@property (nonatomic)NSDictionary *dic;
@end

NS_ASSUME_NONNULL_END
