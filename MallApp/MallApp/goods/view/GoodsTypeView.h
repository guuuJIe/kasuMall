//
//  GoodsTypeView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MMPopupView.h"
@protocol buttonClickDelegate<NSObject>

-(void)buttonClick:(NSInteger)index;

@end
NS_ASSUME_NONNULL_BEGIN

@interface GoodsTypeView : MMPopupView
@property (nonatomic,copy) void(^itemBlock)(NSInteger index);
@property (nonatomic,copy) void(^purchaseBlock)(NSInteger tag,NSInteger num);
@property (nonatomic,weak) id<buttonClickDelegate> clickDelegate;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
