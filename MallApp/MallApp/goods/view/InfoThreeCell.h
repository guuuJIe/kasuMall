//
//  InfoThreeCell.h
//  RebateApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfoThreeCell : UITableViewCell
@property (nonatomic,copy) void(^itemBlock)(void);
- (void)setupData:(NSMutableArray *)data;
@end

NS_ASSUME_NONNULL_END
