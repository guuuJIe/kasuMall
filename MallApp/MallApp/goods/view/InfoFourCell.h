//
//  InfoFourCell.h
//  RebateApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface InfoFourCell : UITableViewCell
@property(nonatomic,strong)WKWebView * wkwebView;
- (void)setdata;
@end

NS_ASSUME_NONNULL_END
