//
//  InfoTwoCell.m
//  RebateApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "InfoTwoCell.h"

@implementation InfoTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic
{
    
    if (!dic) {
        return;
    }
//    self.stateLbl.text = @"自营玲珑轮胎 玲珑轮胎 LMA1";
   NSString *titleString = dic[@"title"];
    //创建  NSMutableAttributedString 富文本对象
    NSMutableAttributedString *maTitleString = [[NSMutableAttributedString alloc] initWithString:titleString];
    //创建一个小标签的Label
    NSString *aa = @"自营";
    CGFloat aaW = 12*aa.length +6;
    UILabel *aaL = [UILabel new];
    aaL.frame = CGRectMake(0, 0, aaW*3, 16*3);
    aaL.text = aa;
    aaL.font = [UIFont systemFontOfSize:12*3];
    aaL.textColor = [UIColor whiteColor];
    aaL.backgroundColor = APPColor;
    aaL.clipsToBounds = YES;
    aaL.layer.cornerRadius = 3*3;
    aaL.textAlignment = NSTextAlignmentCenter;
    //调用方法，转化成Image
    UIImage *image = [self imageWithUIView:aaL];
    //创建Image的富文本格式
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.bounds = CGRectMake(5, -3.5, aaW, 16); //这个-2.5是为了调整下标签跟文字的位置
    attach.image = image;
    //添加到富文本对象里
    NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
    [maTitleString insertAttributedString:imageStr atIndex:0];//加入文字前面
    //[maTitleString appendAttributedString:imageStr];//加入文字后面
    //[maTitleString insertAttributedString:imageStr atIndex:4];//加入文字第4的位置
    
    
    self.stateLbl.attributedText = maTitleString;
    
//    self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    self.saleCountLbl.text = [NSString stringWithFormat:@"销量%@",dic[@"sell"]];
    NSString *sumStr = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
    [aStr addAttribute:NSFontAttributeName value:LabelFont15 range:[sumStr rangeOfString:@"¥"]];
    
    self.priceLbl.attributedText = aStr;
    self.sTitleLbl.text = [NSString stringWithFormat:@"%@",dic[@"sTitle"]];
}

//view转成image
- (UIImage*) imageWithUIView:(UIView*) view{
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage* tImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tImage;
}
@end
