//
//  OperationView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OperationView.h"
@interface OperationView()
@property (nonatomic) UIButton *addButton;
@property (nonatomic) UIButton *purchaseButton;
@end

@implementation OperationView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    [self addSubview:self.addButton];
    [self addSubview:self.purchaseButton];
    
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        make.height.equalTo(@50);
        make.width.mas_equalTo(Screen_Width/2);
    }];
    
    [self.purchaseButton mas_makeConstraints:^(MASConstraintMaker *make) {
          make.right.top.equalTo(self);
          make.height.equalTo(@50);
        make.left.mas_equalTo(self.addButton.mas_right);
          make.width.mas_equalTo(Screen_Width/2);
      }];
}


- (void)join{
    if (self.addBlock) {
        self.addBlock();
    }
}
- (void)purchase:(UIButton *)button{
    if (self.purchaseBlock) {
        self.purchaseBlock(button.tag);
    }
}

-(UIButton *)addButton
{
    if(!_addButton)
    {
        _addButton=[UIButton  new];
        [_addButton setTitle:@"加入购物车" forState:UIControlStateNormal];
        _addButton.titleLabel.font = LabelFont14;
        [_addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_addButton addTarget:self action:@selector(purchase:) forControlEvents:UIControlEventTouchUpInside];
        _addButton.tag = 101;
        _addButton.backgroundColor = UIColor448;
        
    }
    return _addButton;
}

-(UIButton *)purchaseButton
{
    if(!_purchaseButton)
    {
        _purchaseButton=[UIButton  new];
        [_purchaseButton setTitle:@"立即购买" forState:UIControlStateNormal];
        _purchaseButton.titleLabel.font = LabelFont14;
        [_purchaseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_purchaseButton addTarget:self action:@selector(purchase:) forControlEvents:UIControlEventTouchUpInside];
        _purchaseButton.tag = 102;
        _purchaseButton.backgroundColor = UIColor276;
        
    }
    return _purchaseButton;
}

@end
