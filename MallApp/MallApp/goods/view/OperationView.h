//
//  OperationView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OperationView : UIView
/**
 加
 */
@property (nonatomic, copy) void (^purchaseBlock)(NSInteger tag);

/**
 减
 */
@property (nonatomic, copy) void (^addBlock)(void);
@end

NS_ASSUME_NONNULL_END
