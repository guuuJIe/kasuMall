//
//  GoodsTypeWithSpecificalView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <MMPopupView/MMPopupView.h>
@protocol specificbuttonClickDelegate<NSObject>

-(void)buttonClick:(NSInteger)index;

@end
NS_ASSUME_NONNULL_BEGIN

@interface GoodsTypeWithSpecificalView : MMPopupView
@property (nonatomic,copy) void(^itemBlock)(NSString *title);
@property (nonatomic,copy) void(^purchaseBlock)(NSInteger tag,NSInteger num);
@property (nonatomic,weak) id<specificbuttonClickDelegate> clickDelegate;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
