//
//  GoodsTypeWithSpecificalView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoodsTypeWithSpecificalView.h"
#import "OperationView.h"
#import "CommonTypeButtonView.h"
#import "NSString+Extend.h"
#import "NumView.h"
@interface GoodsTypeWithSpecificalView()
@property (nonatomic , strong) UIImageView *goodsImageView;
@property (nonatomic , strong) UILabel *priceLbl;
@property (nonatomic , strong) UILabel *goodsTitleLabel;
@property (nonatomic) OperationView *OpView;
@property (nonatomic , strong) UILabel *stockLabel;
@property (nonatomic) CommonTypeButtonView *typeView;
@property (nonatomic) NumView *numView;
@property (nonatomic) UIScrollView *scrollview;
@property (nonatomic) NSInteger num;
@end

@implementation GoodsTypeWithSpecificalView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        self.num = 1;
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimiss)]];
        
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(450);
        }];
        
        
        [bgView addSubview:self.goodsImageView];
        [self.goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(16);
            make.size.mas_equalTo(CGSizeMake(76, 76));
        }];
        
        [bgView addSubview:self.priceLbl];
        [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.goodsImageView.mas_right).offset(22);
            make.top.equalTo(@25);
        }];
        
        [bgView addSubview:self.stockLabel];
        [self.stockLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.priceLbl);
            make.top.equalTo(self.priceLbl.mas_bottom).offset(4);
        }];
        
        [bgView addSubview:self.goodsTitleLabel];
        [self.goodsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.priceLbl);
            make.top.equalTo(self.stockLabel.mas_bottom).offset(7);
            make.right.mas_equalTo(-12);
        }];
        
        
        UIView *line = [UIView new];
        [bgView addSubview:line];
        line.backgroundColor = UIColorEF;
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.mas_equalTo(self.goodsImageView.mas_bottom).offset(16);
            make.height.mas_equalTo(lineHeihgt);
        }];
        
        UILabel *type = [UILabel new];
        type.text = @"规格";
        type.textColor = UIColor333;
        type.font = LabelFont16;
        [bgView addSubview:type];
        [type mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@12);
            make.top.equalTo(line.mas_bottom).offset(16);
        }];
//
        [bgView addSubview:self.OpView];
        [self.OpView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(bgView);
            make.height.mas_equalTo(50+BottomAreaHeight);
        }];
        
        
        
        

//
        self.scrollview = [[UIScrollView alloc] init];
        self.scrollview.showsVerticalScrollIndicator = false;
        [bgView addSubview:self.scrollview];
        [self.scrollview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(bgView);
            make.top.equalTo(type.mas_bottom).offset(16);
//            make.height.mas_equalTo(130);
            make.bottom.mas_equalTo(-50-BottomAreaHeight);
        }];
        
        UIView *contentView = [UIView new];
        [self.scrollview addSubview:contentView];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.scrollview);
            make.width.equalTo(self.scrollview);
        }];


        [contentView addSubview:self.typeView];
        self.typeView.backgroundColor = [UIColor whiteColor];
        [self.typeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(contentView);
            make.height.mas_equalTo(100);
        }];
        

//
         
        
       
        
        
        
        UILabel *num = [UILabel new];
        num.text = @"购买数量";
        num.textColor = UIColor333;
        num.font = LabelFont16;
        [contentView addSubview:num];
        [num mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@12);
            make.top.equalTo(self.typeView.mas_bottom).offset(0);
        }];
//
        [contentView addSubview:self.numView];
        [self.numView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(num);
            make.right.equalTo(@-16);
            make.size.mas_equalTo(CGSizeMake(100, 26));
            make.bottom.mas_equalTo(contentView);
        }];
//
//
//        UIView *line2 = [UIView new];
//        [bgView addSubview:line2];
//        line2.backgroundColor = UIColorEF;
//        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(self);
//            make.bottom.mas_equalTo(num.mas_top).offset(-16);
//            make.height.mas_equalTo(lineHeihgt);
//        }];
    }
    return self;
}
//TOFix
- (void)setupData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"smallPic"]];
    [self.goodsImageView yy_setImageWithURL:URL(url) options:0];
    NSString *sumStr = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
    [aStr addAttribute:NSFontAttributeName value:LabelFont12 range:[sumStr rangeOfString:@"¥"]];
    
    self.priceLbl.attributedText = aStr;
//    self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    self.stockLabel.text = [NSString stringWithFormat:@"库存%@件",dic[@"stock"]];
    
    
//    ∫®
    if ([[dic allKeys] containsObject:@"pPriceList"]) {
        if ([dic[@"pPriceList"] isEqual:[NSNull null]]) {
            NSArray *data = @[@{@"id":@0,@"name":@"通用件",@"value":@"通用件"}];
            [self.typeView showTagarray:data];
            CGFloat height = [self getTagViewHeight:data];
            
            
            
            
            [self.typeView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(height);
            }];
            
            self.scrollview.contentSize = CGSizeMake(Screen_Width, height+50);
        }else{
            [self.typeView showTagarray:dic[@"pPriceList"]];
            CGFloat height = [self getTagViewHeight:dic[@"pPriceList"]];
            
            
            
            
            [self.typeView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(height);
            }];
            
            self.scrollview.contentSize = CGSizeMake(Screen_Width, height+50);
        }
        
    }else{
    }
    
    
    
}

-(CGFloat)getTagViewHeight:(NSArray *)array{
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=36;
    int tagNowLine=0;
    
    for(int i=0;i<array.count;i++){
        NSDictionary *dic = array[i];
        UIFont *titleFont=LabelFont14;
        NSString *titleStr=dic[@"name"];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        tagNowX+=itemWidth;
    }
    
    return (tagNowLine+1)*tagItemHeight+Default_Space;
}

- (void)dimiss{
    [self hide];
}

-(UIImageView *)goodsImageView
{
    if(!_goodsImageView)
    {
        _goodsImageView=[UIImageView  new];
        _goodsImageView.image = [UIImage imageNamed:@"DefaultImage"];
        _goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _goodsImageView;
}

-(UILabel *)goodsTitleLabel
{
    if(!_goodsTitleLabel)
    {
        _goodsTitleLabel=[UILabel  new];
        _goodsTitleLabel.font = LabelFont12;
        _goodsTitleLabel.textColor = [UIColor colorWithHexString:@"333333"];
        _goodsTitleLabel.numberOfLines = 1;
         _goodsTitleLabel.text = @"请选择商品规格";

    }
    return _goodsTitleLabel;
}

-(UILabel *)priceLbl
{
    if(!_priceLbl)
    {
        _priceLbl=[UILabel  new];
        _priceLbl.font = LabelFont20;
        _priceLbl.textColor = FF3E3E;
        _priceLbl.text = @"¥122";
    }
    return _priceLbl;
}
-(UILabel *)stockLabel
{
    if(!_stockLabel)
    {
        _stockLabel=[UILabel  new];
        _stockLabel.font = LabelFont12;
        _stockLabel.textColor = UIColor999;
        _stockLabel.text = @"库存293件";
    }
    return _stockLabel;
}


- (OperationView *)OpView{
    if (!_OpView) {
        _OpView = [OperationView new];
        WeakSelf(self)
        _OpView.purchaseBlock = ^(NSInteger tag) {
            if (weakself.purchaseBlock) {
                weakself.purchaseBlock(tag,weakself.num);
            }
        };
   
    }
    return _OpView;
}

- (CommonTypeButtonView *)typeView{
    if (!_typeView) {
        _typeView = [CommonTypeButtonView new];
        WeakSelf(self)
        _typeView.itemClickBlock = ^(NSInteger index,NSString *title,NSDictionary *dic) {

            weakself.goodsTitleLabel.text = title;
            NSString *sumStr = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
            NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:sumStr];
            [aStr addAttribute:NSFontAttributeName value:LabelFont12 range:[sumStr rangeOfString:@"¥"]];
            
            weakself.priceLbl.attributedText = aStr;
            
            if (weakself.itemBlock) {
                weakself.itemBlock(title);
            }
//
        };
        _typeView.backgroundColor = [UIColor whiteColor];
    }
    return _typeView;
}

- (NumView *)numView{
    if (!_numView) {
        _numView = [NumView new];
        WeakSelf(self);
        _numView.AddBlock = ^(UILabel * _Nonnull countLabel) {
//            NSInteger value = [countLabel.text integerValue];
//            NSLog(@"%ld",(long)value);
            weakself.num = [countLabel.text integerValue];
        };
        _numView.CutBlock = ^(UILabel * _Nonnull countLabel) {
            weakself.num = [countLabel.text integerValue];
        };
//        _numView.backgroundColor = [UIColor redColor];
    }
    return _numView;
}
@end

