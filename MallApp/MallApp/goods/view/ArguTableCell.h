//
//  ArguTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ArguTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bottomview;

@property (weak, nonatomic) IBOutlet UILabel *Lbl;
@property (weak, nonatomic) IBOutlet UILabel *valueLbl;

- (void)setupData:(NSDictionary *)dic andNSIndexPath:(NSIndexPath *)indexpath;
@end

NS_ASSUME_NONNULL_END
