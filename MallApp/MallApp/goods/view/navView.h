//
//  navView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/4.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface navView : UIView
@property (nonatomic,copy) void(^clickBlock)(void);
@property (nonatomic) UIButton *backButton;
@end

NS_ASSUME_NONNULL_END
