//
//  VerfiyLoginView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "VerfiyLoginView.h"
#import "AccountApi.h"

@interface VerfiyLoginView()<UITextFieldDelegate>
@property(nonatomic,strong) UIButton *dxdlSendSMSBtn;
@property(nonatomic,strong) AccountApi *accountApi;
@property(nonatomic,assign) int sendSMSTimeCount;
@property(nonatomic,strong) NSTimer *sendSMSTime;
@property(nonatomic,strong) UIButton *loginBtn;
@end
@implementation VerfiyLoginView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    UILabel *title = [UILabel new];
    title.text = @"手机号";
    title.textColor = UIColor60;
    title.font = LabelFont14;
    [self addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(32);
    }];
    UITextField  *teltext = [UITextField new];
    teltext.placeholder = @"请输入手机号";
    teltext.textColor = UIColor333;
    teltext.font = LabelFont16;
//    teltext.delegate = self;
    teltext.keyboardType = UIKeyboardTypeNumberPad;
    [self addSubview:teltext];
    self.acctextField = teltext;
    [teltext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(title.mas_bottom).offset(6*AdapterScal);
    }];
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(teltext.mas_bottom).offset(8*AdapterScal);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"验证码";
    title2.textColor = UIColor60;
    title2.font = LabelFont14;
    [self addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line.mas_bottom).offset(8*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *codetext = [UITextField new];
    codetext.placeholder = @"请输入验证码";
    codetext.textColor = UIColor333;
    codetext.font = LabelFont16;
    codetext.keyboardType = UIKeyboardTypeNumberPad;
    self.pwdtextField = codetext;
//    self.pwdtextField.secureTextEntry = true;
    self.pwdtextField.delegate = self;
    [self addSubview:codetext];
    [codetext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(title2.mas_bottom).offset(6*AdapterScal);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(codetext.mas_bottom).offset(8*AdapterScal);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    
    self.dxdlSendSMSBtn=[UIButton new];
    [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:APPColor forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:UIColorB6 forState:UIControlStateDisabled];
    self.dxdlSendSMSBtn.titleLabel.font=LabelFont16;
    [self.dxdlSendSMSBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.dxdlSendSMSBtn];
    
    
    [self.dxdlSendSMSBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-32);
        make.size.mas_equalTo(CGSizeMake(100, 22));
        make.centerY.mas_equalTo(codetext);
    }];
    
    
    
    
    UIButton *button = [UIButton new];
    [button setTitle:@"登录" forState:0];
    [button.titleLabel setFont:LabelFont14];
    [button setTitleColor:[UIColor whiteColor] forState:0];
    button.layer.cornerRadius = 20;
    button.backgroundColor = APPColor;
    button.layer.shadowColor = [UIColor colorWithHexString:@"085BC7"].CGColor;
    button.layer.shadowOffset = CGSizeMake(0, 2);
    button.layer.shadowOpacity = 0.4;
    button.layer.shadowRadius = 2.0;
    button.alpha = 0.5;
    button.tag = 100;
    self.loginBtn = button;
    [self addSubview:button];
    [button addTarget:self action:@selector(clickType:) forControlEvents:UIControlEventTouchUpInside];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line2.mas_bottom).offset(32*AdapterScal);
        make.left.mas_equalTo(32);
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(40);
    }];
    
    
    UIButton *btn = [UIButton new];
    [btn setTitle:@"账号登录" forState:0];
    [btn setImage:[UIImage imageNamed:@"more"] forState:0];
    [btn.titleLabel setFont:LabelFont14];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 70, 0, 0)];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -70, 0, 0)];
    [btn setTitleColor:UIColor90 forState:0];
    btn.tag = 101;
    [btn addTarget:self action:@selector(clickType:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(button);
        make.top.mas_equalTo(button.mas_bottom).offset(16*AdapterScal);
        make.width.mas_equalTo(120);
    }];
    
    
    UIButton *btn2 = [UIButton new];
    [btn2 setTitle:@"立即注册" forState:0];
 
    [btn2.titleLabel setFont:LabelFont14];
    [btn2 setTitleColor:UIColor90 forState:0];
    btn2.tag = 102;
    [btn2 addTarget:self action:@selector(clickType:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(button);
        make.top.mas_equalTo(button.mas_bottom).offset(16*AdapterScal);
        make.bottom.mas_equalTo(self);
    }];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField.text.length == 6) {
        self.loginBtn.alpha = 1;
           
    }
  return true;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.loginBtn.alpha = 0.5;
}

- (void)click:(UIButton *)sender{
    sender.selected = !sender.selected;
    
    self.pwdtextField.secureTextEntry = !sender.selected;
    
}

- (void)sendSMS{
    
    if (self.acctextField.text.length != 11) {
        [JMBManager showBriefAlert:@"请输入电话号码"];
        return;
    }//15888017959
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.acctextField.text forKey:@"Phone"];
    [JMBManager showLoading];
    WeakSelf(self)
    [self.accountApi getverifyCodeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        //           VerifyCodeLogin *login = [VerifyCodeLogin new];
        //           login.tel = self.telText.text;
        //           login.scanId = self.scanId;
        //           [self.navigationController pushViewController:login animated:true];
        //           [JMBManager hideAlert];
        
        if(self.sendSMSTimeCount!=0){
            return;
        }
        self.sendSMSTimeCount=60;
        self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
        [JMBManager hideAlert];
    }];
    
   
}



-(void)changeSMSTime{
    
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.dxdlSendSMSBtn.enabled = true;
    }else{
        
        self.dxdlSendSMSBtn.enabled = false;
        [self.dxdlSendSMSBtn setTitle:[NSString stringWithFormat:@"重新发送(%is)",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
    
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}


- (void)clickType:(UIButton *)sender{
    if (self.clickBlock) {
        self.clickBlock(sender.tag);
    }
}
@end
