//
//  AuthoritaionLoginVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AuthoritaionLoginVC.h"
#import "AccountApi.h"

@interface AuthoritaionLoginVC ()
@property(nonatomic,assign) int sendSMSTimeCount;
@property(nonatomic,strong) NSTimer *sendSMSTime;
@property(nonatomic,strong) AccountApi *accountApi;
@end

@implementation AuthoritaionLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"验证登录";
//    [self.smsBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
//    [self.smsBtn set]
    
    if (self.uninoId) {
        NSLog(@"接收到的%@",self.uninoId);
      self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"LoginVC"]];
    }
    
    if (self.scanId) {
        NSLog(@"接收到的%@",self.scanId);
       self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"ScanVC"]];
    }
}


- (IBAction)SendCodeAct:(UIButton *)sender {
    
    if(self.sendSMSTimeCount!=0){
        return;
    }
    
    if (self.telText.text.length < 0) {
        return;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.telText.text forKey:@"Phone"];
    [self.accountApi getverifyCodeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
      
    }];
    self.sendSMSTimeCount=60;
    self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
}

-(void)changeSMSTime{
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.smsBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.smsBtn.enabled = true;
    }else{
      
        self.smsBtn.enabled = false;
        [self.smsBtn setTitle:[NSString stringWithFormat:@"%is后重新获取",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
}


- (IBAction)ConfirmAct:(UIButton *)sender {
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.telText.text forKey:@"phone"];
    [dic setValue:self.codeText.text forKey:@"verifyCode"];
    if (self.uninoId) {
        [dic setValue:self.uninoId forKey:@"unionID"];
        if (!self.scanId) {
            self.scanId = @"0";
        }
        [self.accountApi verifyUserCodeLoginWithparameters:dic withId:self.scanId withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code != 200) {
                
            }else{
                NSDictionary *dic = result.resultDic;
                NSArray *arr = dic[@"list"];
                NSDictionary *userData = arr.firstObject;
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Bid"];
                [self dismissViewControllerAnimated:true completion:nil];
            }
            NSLog(@"%@",result.resultDic);
        }];
    }
    
    if (self.scanId) {
        [self.accountApi verifyUserCodeLoginWithparameters:dic withId:self.scanId withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSDictionary *dic = result.resultDic;
                NSArray *arr = dic[@"list"];
                NSDictionary *userData = arr.firstObject;
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Bid"];
                [self.navigationController popViewControllerAnimated:true];
            }
        }];
    }
    
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}

@end
