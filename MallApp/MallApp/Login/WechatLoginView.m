//
//  WechatLoginView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WechatLoginView.h"
@interface WechatLoginView()

@property (weak, nonatomic) IBOutlet UIView *wechatActView;
@end

@implementation WechatLoginView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self.wechatActView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
//    [self.verifyLogin addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
//    [self.accountView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

+(instancetype)initViewClass{
    return [[[NSBundle mainBundle] loadNibNamed:@"WechatLoginView" owner:self options:nil] lastObject];
}


- (void)click:(UITapGestureRecognizer *)ges{
    if (self.clickBlock) {
        self.clickBlock(ges.view.tag);
    }
}
@end
