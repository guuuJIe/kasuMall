//
//  NewBindUserVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewBindUserVC : BaseViewController
@property (nonatomic,strong) NSString *uninoId;
@property (nonatomic,strong) NSString *scanId;
@end

NS_ASSUME_NONNULL_END
