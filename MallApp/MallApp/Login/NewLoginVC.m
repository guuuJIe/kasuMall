//
//  NewLoginVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NewLoginVC.h"
#import "NewLoginWayView.h"
#import "NewBindUserVC.h"
#import "NewOtherLoginVC.h"

@interface NewLoginVC ()
@property (nonatomic, strong) UILabel *telLbl;
@property (nonatomic, strong) NewLoginWayView *loginView;

@end

@implementation NewLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self setupUM];
    
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    [self.backBtn setImage:[UIImage imageNamed:@"关闭"] forState:0];
    self.backBtn.tintColor = [UIColor blackColor];
     self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setupUM{
//    //设置秘钥
//    NSString *info = @"oRDIZGskUdOi9onqLA2kj9EgoO08WWsGXd9nVvX07jLwRWRTK+/P2CMQN3Jal3bWn5wcYdi8WW2lMa2JE91XULvk4kb4+D3NLp7guqrzSP0Ff4haezpj+CaX4tyMF1JpoPin7WeEUKy64tQ+gKtlRqoU42ydKC04Ls1uJ77beOywVcZ5skh2aPae00ny7bChb3TkHIXx21rbdjpomzAI7UoRozRi90G1PZxMU0OeIep4K5e4Bg3lcA==";
//    __weak typeof(self) weakSelf = self;
//    [UMCommonHandler setVerifySDKInfo:info complete:^(NSDictionary * _Nonnull resultDic) {
//        
//    }];
}

- (void)setupUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageview = [UIImageView new];
    imageview.image = [UIImage imageNamed:@"L_icon1"];
    
    [self.view addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(90*AdapterScal);
    }];
    
    [self.view addSubview:self.telLbl];
    [self.telLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(imageview);
        make.top.mas_equalTo(imageview.mas_bottom).offset(15*AdapterScal);
    }];
    
    UIButton *button = [UIButton new];
    [button setTitle:@"本机号码一键登录" forState:0];
    [button.titleLabel setFont:LabelFont14];
    [button setTitleColor:[UIColor whiteColor] forState:0];
    button.layer.cornerRadius = 20;
    button.backgroundColor = APPColor;
    button.layer.shadowColor = [UIColor colorWithHexString:@"085BC7"].CGColor;
    button.layer.shadowOffset = CGSizeMake(0, 2);
    button.layer.shadowOpacity = 0.4;
    button.layer.shadowRadius = 2.0;
    
    [self.view addSubview:button];
    [button addTarget:self action:@selector(loginAct) forControlEvents:UIControlEventTouchUpInside];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.telLbl.mas_bottom).offset(32*AdapterScal);
        make.left.mas_equalTo(32);
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(40);
    }];
    
    self.loginView = [NewLoginWayView initViewClass];
    [self.view addSubview:self.loginView];
    [self.loginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40*AdapterScal);
        make.right.mas_equalTo(-40*AdapterScal);
        make.height.mas_equalTo(150);
        make.bottom.mas_equalTo(-20);
    }];
    
    WeakSelf(self)
    self.loginView.clickBlock = ^(NSInteger type) {
        StrongSelf(self)
        if (type == 1000) {
            NewBindUserVC *vc = [NewBindUserVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if (type == 1001){
            NewOtherLoginVC *vc = [NewOtherLoginVC new];
            [self.navigationController pushViewController:vc animated:true];
        }
    };

}

- (void)showResult:(id __nullable)result {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *desc = nil;
        if ([result isKindOfClass:NSString.class]) {
            desc = (NSString *)result;
        } else {
            desc = [result description];
            if (desc != nil) {
                desc = [NSString stringWithCString:[desc cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
            }
        }
        self.telLbl.text = desc;
    });
}


- (void)loginAct{
    
//    [UMCommonHandler checkEnvAvailableWithComplete:^(NSDictionary * _Nullable resultDic) {
//        if ([PNSCodeSuccess isEqualToString:[resultDic objectForKey:@"resultCode"]] == NO) {
//            [JMBManager showBriefAlert:@"接口检查失败，环境不满足"];
//            return;
//        }
//        
//        [UMCommonHandler accelerateLoginPageWithTimeout:3 complete:^(NSDictionary * _Nonnull resultDic) {
//            
//        }];
//        
//       
//    }];
    
}

- (UILabel *)telLbl{
    if (!_telLbl) {
        _telLbl = [UILabel new];
        _telLbl.text = @"135****666";
        _telLbl.textColor = [UIColor colorWithHexString:@"303133"];
        _telLbl.font = LabelFont21;
        
    }
    
    return _telLbl;
}

@end
