//
//  NewOtherLoginVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewOtherLoginVC : BaseViewController

@property (nonatomic,copy) void(^clickBlock)(NSInteger type);
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,strong) NSString *scanId;
@end

NS_ASSUME_NONNULL_END
