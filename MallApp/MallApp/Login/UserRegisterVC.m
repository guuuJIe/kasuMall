//
//  UserRegisterVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/10.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "UserRegisterVC.h"
#import "AccountApi.h"
@interface UserRegisterVC ()<UITextFieldDelegate>
@property (nonatomic,strong) UITextField *accountText;
@property (nonatomic,strong) UITextField *pwdText;
@property (nonatomic,strong) UITextField *pwdText2;
@property (nonatomic,strong) UITextField *telText;
@property (nonatomic,strong) UITextField *codeText;
@property (nonatomic,strong) UIButton *dxdlSendSMSBtn;
@property (nonatomic,assign) int sendSMSTimeCount;
@property (nonatomic,strong) NSTimer *sendSMSTime;
@property (nonatomic,strong) AccountApi *accountApi;
@property (nonatomic,strong) UIButton *loginBtn;
@end

@implementation UserRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setupUI{
    self.title = @"注册";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *title = [UILabel new];
    title.text = @"帐号";
    title.textColor = UIColor60;
    title.font = LabelFont14;
    [self.view addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(51*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *teltext = [UITextField new];
    teltext.placeholder = @"请输入注册账号名";
    teltext.textColor = UIColor333;
    teltext.font = LabelFont16;
    [self.view addSubview:teltext];
//    teltext.keyboardType = UIKeyboardTypeNumberPad;
    self.accountText = teltext;
    [teltext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(title.mas_bottom).offset(6);
    }];
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self.view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(teltext.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"密码";
    title2.textColor = UIColor60;
    title2.font = LabelFont14;
    [self.view addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line.mas_bottom).offset(8*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *codetext = [UITextField new];
    codetext.placeholder = @"请输入登录密码";
    codetext.textColor = UIColor333;
    codetext.font = LabelFont16;
//    codetext.keyboardType = UIKeyboardTypeNumberPad;
    self.pwdText = codetext;
    self.pwdText.secureTextEntry = true;
    [self.view addSubview:codetext];
    [codetext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(title2.mas_bottom).offset(6);
    }];
    UIView *line2 = [UIView new];
    line2.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self.view addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(codetext.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    UIButton *btn1=[UIButton new];
    
    [btn1 setImage:[UIImage imageNamed:@"L_icon7"] forState:0];
    [btn1 setImage:[UIImage imageNamed:@"L_icon6"] forState:UIControlStateSelected];
    [btn1 addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    btn1.tag = 200;
    [self.view addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-32);
        make.size.mas_equalTo(CGSizeMake(22 , 22));
        make.centerY.mas_equalTo(codetext);
    }];
       
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"确认密码";
    title3.textColor = UIColor60;
    title3.font = LabelFont14;
    [self.view addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line2.mas_bottom).offset(8*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *codetext2 = [UITextField new];
    codetext2.placeholder = @"请再次确认登录密码";
    codetext2.textColor = UIColor333;
    codetext2.font = LabelFont16;
//    codetext2.keyboardType = UIKeyboardTypeNumberPad;
    self.pwdText2 = codetext2;
    self.pwdText2.secureTextEntry = true;
    [self.view addSubview:codetext2];
    [codetext2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(title3.mas_bottom).offset(6);
    }];
    UIView *line3 = [UIView new];
    line3.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self.view addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(codetext2.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    UIButton *btn2=[UIButton new];
    
    [btn2 setImage:[UIImage imageNamed:@"L_icon7"] forState:0];
    [btn2 setImage:[UIImage imageNamed:@"L_icon6"] forState:UIControlStateSelected];
    [btn2 addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    btn2.tag = 201;
    [self.view addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-32);
        make.size.mas_equalTo(CGSizeMake(22 , 22));
        make.centerY.mas_equalTo(codetext2);
    }];
    
    
    UILabel *tel = [UILabel new];
    tel.text = @"手机";
    tel.textColor = UIColor60;
    tel.font = LabelFont14;
    [self.view addSubview:tel];
    [tel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line3.mas_bottom).offset(8*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *codetext3 = [UITextField new];
    codetext3.placeholder = @"请输入手机号";
    codetext3.textColor = UIColor333;
    codetext3.font = LabelFont16;
    codetext3.keyboardType = UIKeyboardTypeNumberPad;
    self.telText = codetext3;
    [self.view addSubview:codetext3];
    [codetext3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(tel.mas_bottom).offset(6);
    }];
    UIView *line4 = [UIView new];
    line4.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self.view addSubview:line4];
    [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(codetext3.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    
    UILabel *verifyCode = [UILabel new];
    verifyCode.text = @"验证码";
    verifyCode.textColor = UIColor60;
    verifyCode.font = LabelFont14;
    [self.view addSubview:verifyCode];
    [verifyCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line4.mas_bottom).offset(8*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *codetext4 = [UITextField new];
    codetext4.placeholder = @"请输入验证码";
    codetext4.textColor = UIColor333;
    codetext4.font = LabelFont16;
    codetext4.keyboardType = UIKeyboardTypeNumberPad;
   
    codetext4.delegate = self;
     self.codeText = codetext4;
    [self.view addSubview:codetext4];
    [codetext4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(verifyCode.mas_bottom).offset(6);
    }];
    UIView *line5 = [UIView new];
    line5.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self.view addSubview:line5];
    [line5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(codetext4.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    self.dxdlSendSMSBtn=[UIButton new];
    [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:APPColor forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:UIColorB6 forState:UIControlStateDisabled];
    self.dxdlSendSMSBtn.titleLabel.font=LabelFont16;
    [self.dxdlSendSMSBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.dxdlSendSMSBtn];
    
    
    [self.dxdlSendSMSBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-32);
        make.size.mas_equalTo(CGSizeMake(100, 22));
        make.centerY.mas_equalTo(codetext4);
    }];
    
    
    
    UIButton *button = [UIButton new];
    [button setTitle:@"注册" forState:0];
    [button.titleLabel setFont:LabelFont14];
    [button setTitleColor:[UIColor whiteColor] forState:0];
    button.layer.cornerRadius = 20;
    button.backgroundColor = APPColor;
    button.layer.shadowColor = [UIColor colorWithHexString:@"085BC7"].CGColor;
    button.layer.shadowOffset = CGSizeMake(0, 2);
    button.layer.shadowOpacity = 0.4;
    button.layer.shadowRadius = 2.0;
    button.alpha = 0.5;
    [self.view addSubview:button];
    self.loginBtn = button;
    [button addTarget:self action:@selector(registerAct) forControlEvents:UIControlEventTouchUpInside];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line5.mas_bottom).offset(32*AdapterScal);
        make.left.mas_equalTo(32);
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(40);
    }];
       
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text.length == 6) {
        self.loginBtn.alpha = 1;
    }
    
   
}

- (void)click:(UIButton *)sender{
    sender.selected = !sender.selected;
    
    if (sender.tag == 200) {
         self.pwdText.secureTextEntry = !sender.selected;
    }else if (sender.tag == 201){
         self.pwdText2.secureTextEntry = !sender.selected;
    }
   
    
}


- (void)sendSMS{
    
    if (self.telText.text.length != 11) {
        [JMBManager showBriefAlert:@"请输入电话号码"];
        return;
    }//15888017959
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.telText.text forKey:@"Phone"];
    [JMBManager showLoading];
    WeakSelf(self)
    [self.accountApi getverifyCodeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
     
        
        if(self.sendSMSTimeCount!=0){
            return;
        }
        self.sendSMSTimeCount=60;
        self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
        [JMBManager hideAlert];
    }];
    
   
}



-(void)changeSMSTime{
    
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.dxdlSendSMSBtn.enabled = true;
    }else{
        
        self.dxdlSendSMSBtn.enabled = false;
        [self.dxdlSendSMSBtn setTitle:[NSString stringWithFormat:@"重新发送(%is)",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
    
}

- (void)registerAct{
    if (self.accountText.text.length == 0 || self.pwdText.text.length == 0 || self.pwdText2.text.length == 0 || self.telText.text.length == 0 || self.codeText.text.length == 0) {
        
        [JMBManager showBriefAlert:@"信息不完善，请补充完整"];
        return;
    }
    
    if (![self.pwdText.text isEqualToString:self.pwdText2.text]) {
        [JMBManager showBriefAlert:@"密码不一致"];
        return;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:self.accountText.text forKey:@"UserName"];
    [dic setValue:self.pwdText2.text forKey:@"PassWord"];
    [dic setValue:self.telText.text forKey:@"Phone"];
    [dic setValue:self.codeText.text forKey:@"VerifyCode"];
    NSString *delegateId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Bid"];
    if (!delegateId) {
        delegateId = @"0";
    }
    JLog(@"%@",delegateId);
    
    WeakSelf(self)
    [self.accountApi userRegisterWithparameters:dic withId:delegateId withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            [self.navigationController popViewControllerAnimated:true];
        }
    }];
}


- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}

@end
