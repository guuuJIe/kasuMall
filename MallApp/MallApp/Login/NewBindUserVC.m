//
//  NewBindUserVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NewBindUserVC.h"
#import "AccountApi.h"
#import <CloudPushSDK/CloudPushSDK.h>
@interface NewBindUserVC ()
@property(nonatomic,strong) UIButton *dxdlSendSMSBtn;
@property(nonatomic,assign) int sendSMSTimeCount;
@property(nonatomic,strong) NSTimer *sendSMSTime;
@property(nonatomic,strong) AccountApi *accountApi;
@property(nonatomic,strong) UITextField *acctextField;
@property(nonatomic,strong) UITextField *pwdtextField;
@end

@implementation NewBindUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"NewOtherLoginVC",@"ScanVC"]];
    [self setUpUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    [self.backBtn setImage:[UIImage imageNamed:@"关闭"] forState:0];
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setUpUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageview = [UIImageView new];
    imageview.image = [UIImage imageNamed:@"L_icon5"];
    
    [self.view addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(50*AdapterScal);
    }];
    
    
    
    UILabel *title = [UILabel new];
    title.text = @"手机号";
    title.textColor = UIColor60;
    title.font = LabelFont14;
    [self.view addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imageview.mas_bottom).offset(63*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *teltext = [UITextField new];
    teltext.placeholder = @"请输入手机号";
    teltext.textColor = UIColor333;
    teltext.font = LabelFont16;
    [self.view addSubview:teltext];
    teltext.keyboardType = UIKeyboardTypeNumberPad;
    self.acctextField = teltext;
    [teltext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(title.mas_bottom).offset(6);
    }];
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self.view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(teltext.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"验证码";
    title2.textColor = UIColor60;
    title2.font = LabelFont14;
    [self.view addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line.mas_bottom).offset(8*AdapterScal);
        make.left.mas_equalTo(32);
    }];
    UITextField  *codetext = [UITextField new];
    codetext.placeholder = @"请输入验证码";
    codetext.textColor = UIColor333;
    codetext.font = LabelFont16;
    codetext.keyboardType = UIKeyboardTypeNumberPad;
    self.pwdtextField = codetext;
    [self.view addSubview:codetext];
    [codetext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(title2.mas_bottom).offset(6);
    }];
    UIView *line2 = [UIView new];
    line2.backgroundColor = [UIColor colorWithHexString:@"EDEFF2"];
    [self.view addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(teltext);
        make.top.mas_equalTo(codetext.mas_bottom).offset(8);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    
    self.dxdlSendSMSBtn=[UIButton new];
    [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:APPColor forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:UIColorB6 forState:UIControlStateDisabled];
    self.dxdlSendSMSBtn.titleLabel.font=LabelFont16;
    [self.dxdlSendSMSBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.dxdlSendSMSBtn];
    
    
    [self.dxdlSendSMSBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-32);
        make.size.mas_equalTo(CGSizeMake(100, 22));
        make.centerY.mas_equalTo(codetext);
    }];
    
    
    UIButton *button = [UIButton new];
    [button setTitle:@"登录" forState:0];
    [button.titleLabel setFont:LabelFont14];
    [button setTitleColor:[UIColor whiteColor] forState:0];
    button.layer.cornerRadius = 20;
    button.backgroundColor = APPColor;
    button.layer.shadowColor = [UIColor colorWithHexString:@"085BC7"].CGColor;
    button.layer.shadowOffset = CGSizeMake(0, 2);
    button.layer.shadowOpacity = 0.4;
    button.layer.shadowRadius = 2.0;
    
    [self.view addSubview:button];
    [button addTarget:self action:@selector(verLogin) forControlEvents:UIControlEventTouchUpInside];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line2.mas_bottom).offset(32*AdapterScal);
        make.left.mas_equalTo(32);
        make.right.mas_equalTo(-32);
        make.height.mas_equalTo(40);
    }];
    
    
}


- (void)sendSMS{
    if (self.acctextField.text.length != 11) {
        [JMBManager showBriefAlert:@"请输入电话号码"];
        return;
    }//15888017959
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.acctextField.text forKey:@"Phone"];
    [JMBManager showLoading];
    WeakSelf(self)
    [self.accountApi getverifyCodeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        //           VerifyCodeLogin *login = [VerifyCodeLogin new];
        //           login.tel = self.telText.text;
        //           login.scanId = self.scanId;
        //           [self.navigationController pushViewController:login animated:true];
        //           [JMBManager hideAlert];
        
        if(self.sendSMSTimeCount!=0){
            return;
        }
        self.sendSMSTimeCount=60;
        self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
        [JMBManager hideAlert];
    }];
   
}


-(void)changeSMSTime{
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.dxdlSendSMSBtn.enabled = true;
    }else{
      
        self.dxdlSendSMSBtn.enabled = false;
        [self.dxdlSendSMSBtn setTitle:[NSString stringWithFormat:@"重新发送(%is)",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
}

- (void)verLogin{
   if (self.acctextField.text.length == 0 && self.pwdtextField.text.length == 0) {
        
        [JMBManager showBriefAlert:@"请填写登录信息"];
        return;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.acctextField.text forKey:@"phone"];
    [dic setValue:self.pwdtextField.text forKey:@"verifyCode"];
    if (self.uninoId) {
        [dic setValue:self.uninoId forKey:@"unionID"];
    }
    
    if (!self.scanId) {
        self.scanId = @"0";
    }
    self.scanId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Bid"] ? [[NSUserDefaults standardUserDefaults] objectForKey:@"Bid"] : @"0";
    WeakSelf(self)
    [self.accountApi verifyUserCodeLoginWithparameters:dic withId:self.scanId withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            NSDictionary *userData = arr.firstObject;
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"mobile"] forKey:@"mobile"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshUserVC object:nil];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Bid"];
            [CloudPushSDK addAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] withCallback:^(CloudPushCallbackResult *res) {
                if (res.success) {
                    JLog(@"设置成功");
                }
                
            }];
            [self dismissViewControllerAnimated:true completion:nil];
        }
    }];
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}

@end
