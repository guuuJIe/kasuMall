//
//  VerfiyLoginView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerfiyLoginView : UIView
@property (nonatomic,copy) void(^clickBlock)(NSInteger type);
@property(nonatomic,strong) UITextField *acctextField;
@property(nonatomic,strong) UITextField *pwdtextField;
@end

NS_ASSUME_NONNULL_END
