//
//  NewOtherLoginVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/9.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NewOtherLoginVC.h"
#import "WechatLoginView.h"
#import "AccLoginView.h"
#import "VerfiyLoginView.h"
#import "AccountApi.h"
#import <UMCommon/UMCommon.h>
#import <UMVerify/UMVerify.h>
#import "PayMannagerUtil.h"
#import "NewBindUserVC.h"
#import "UserRegisterVC.h"
#import <CloudPushSDK/CloudPushSDK.h>
#import <AuthenticationServices/AuthenticationServices.h>
#import "SignInApple.h"
@interface NewOtherLoginVC ()
@property(nonatomic,strong) AccLoginView *accLoginView;
@property(nonatomic,strong) WechatLoginView *loginView;
@property(nonatomic,strong) VerfiyLoginView *verLoginView;
@property(nonatomic,strong) AccountApi *accountApi;
@property(nonatomic,strong) SignInApple *signInApple;
@end

@implementation NewOtherLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpUI];
    
    if (![WXApi isWXAppInstalled]) {
        self.loginView.hidden = true;
    }
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"ScanVC"]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    NSDictionary *dic =@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:17 weight:UIFontWeightBold]};
    self.navigationController.navigationBar.titleTextAttributes = dic;
    [self.backBtn setImage:[UIImage imageNamed:@"关闭"] forState:0];
    self.backBtn.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setUpUI{//普通买家 delafqm
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageview = [UIImageView new];
    imageview.image = [UIImage imageNamed:@"L_icon5"];
    
    [self.view addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(40*AdapterScal);
    }];
    
    self.accLoginView = [AccLoginView new];
    [self.view addSubview:self.accLoginView];
    [self.accLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(imageview.mas_bottom).offset(58*AdapterScal);
        make.right.mas_equalTo(0);
        
    }];
    
    
    
    self.verLoginView = [VerfiyLoginView new];
    self.verLoginView.hidden = true;
    [self.view addSubview:self.verLoginView];
    [self.verLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(imageview.mas_bottom).offset(63*AdapterScal);
        make.right.mas_equalTo(0);
        
    }];
    
//    if (@available(ios 13.0, *)) {
//        ASAuthorizationAppleIDButton *appleIDBtn = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeDefault style:ASAuthorizationAppleIDButtonStyleWhite];
//        appleIDBtn.layer.borderWidth = 1;
//        appleIDBtn.layer.borderColor = UIColor333.CGColor;
//        appleIDBtn.layer.cornerRadius = 15;
//        [appleIDBtn addTarget:self action:@selector(didAppleIDBtnClicked) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:appleIDBtn];
//        [appleIDBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.mas_equalTo(self.view);
//            make.top.mas_equalTo(self.verLoginView.mas_bottom).offset(15);
//            make.size.mas_equalTo(CGSizeMake(180, 48));
//        }];
//    }
   
    
    self.loginView = [WechatLoginView initViewClass];
    [self.view addSubview:self.loginView];
    [self.loginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(40*AdapterScal);
        make.right.mas_equalTo(-40*AdapterScal);
        make.height.mas_equalTo(150);
        make.bottom.mas_equalTo(-20-BottomAreaHeight);
    }];
    
    if (self.type == 1) {
        self.verLoginView.hidden = false;
        self.accLoginView.hidden = true;
    }
    
    if (self.type == 2) {
        self.verLoginView.hidden = true;
        self.accLoginView.hidden = false;
    }
    
    WeakSelf(self)
    self.accLoginView.clickBlock = ^(NSInteger type) {
        StrongSelf(self)
        if (type == 101) {
            self.verLoginView.hidden = false;
            self.accLoginView.hidden = true;
        }else if (type == 102){
            UserRegisterVC *vc = [UserRegisterVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if (type == 100){
            
            [self accLogin];
        }
       
    };
    
    
 
    self.verLoginView.clickBlock = ^(NSInteger type) {
        StrongSelf(self)
        
        
        if (type == 101) {
            self.accLoginView.hidden = false;
            self.verLoginView.hidden = true;
        }else if (type == 102){
            UserRegisterVC *vc = [UserRegisterVC new];
            [self.navigationController pushViewController:vc animated:true];
        }else if (type == 100){
            
            [self verLogin];
            
        }
    };
    
    self.loginView.clickBlock = ^(NSInteger type) {
        StrongSelf(self)
        [[PayMannagerUtil sharedManager] loginWithWeChatMethodsuccessBlock:^(BaseResp * _Nonnull res) {
            SendAuthResp *authResp = (SendAuthResp *)res;
            NSLog(@"微信登录的code%@",authResp.code);
            WeakSelf(self)
            [self.accountApi AppWxLoginWithparameters:@{@"code":authResp.code} withCompletionHandler:^(NSError *error, MessageBody *result) {
                StrongSelf(self)
                NSLog(@"接口返回code%@",result.message);
                if (result.code == 200) {
                    NSDictionary *dic = result.resultDic;
                    NSArray *arr = dic[@"list"];
                    NSDictionary *userData = arr.firstObject;
                    [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
                    [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
                    [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
                    [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
                    [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [CloudPushSDK addAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] withCallback:^(CloudPushCallbackResult *res) {
                        if (res.success) {
                            JLog(@"设置成功");
                        }
                        
                    }];
                    self.tabBarController.selectedIndex = 0;
                    [self dismissViewControllerAnimated:true completion:nil];
                }else{
                    NewBindUserVC *vc = [[NewBindUserVC alloc] init];
                    vc.uninoId = [NSString stringWithFormat:@"%@",result.message];
//                                                   vc.scanId = self.scanId;
                    [self.navigationController pushViewController:vc animated:true];
                }

            }];


        } failBlock:^(BOOL fail) {

        }];
    };
}

- (void)didAppleIDBtnClicked{
    [self.signInApple handleAuthorizationAppleIDButtonPress];
//    [self.signInApple perfomExistingAccountSetupFlows];
}

- (void)accLogin{
    if (self.accLoginView.acctextField.text.length == 0 && self.accLoginView.pwdtextField.text.length == 0) {
        
        [JMBManager showBriefAlert:@"请填写登录信息"];
        return;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.accLoginView.acctextField.text forKey:@"userName"];
    [dic setValue:self.accLoginView.pwdtextField.text forKey:@"passWord"];
    self.scanId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Bid"];
    if (!self.scanId) {
        self.scanId = @"0";
    }
    [JMBManager showLoading];
    WeakSelf(self)
    [self.accountApi accountLoginWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            NSDictionary *userData = arr.firstObject;
            //            [JPUSHService setAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
            //
            //            } seq:0];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:refreshUserVC object:nil];
//            [self setupUM];
            [CloudPushSDK addAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] withCallback:^(CloudPushCallbackResult *res) {
                if (res.success) {
                    JLog(@"设置成功");
                }
                
            }];
            self.tabBarController.selectedIndex = 0;
            [self dismissViewControllerAnimated:true completion:nil];
            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [UMCommonHandler cancelLoginVCAnimated:YES complete:nil];
//            });
        }
        [JMBManager hideAlert];
    }];
}

- (void)verLogin{
    if (self.verLoginView.acctextField.text.length == 0 && self.verLoginView.pwdtextField.text.length == 0) {
        
        [JMBManager showBriefAlert:@"请填写登录信息"];
        return;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.verLoginView.acctextField.text forKey:@"phone"];
    [dic setValue:self.verLoginView.pwdtextField.text forKey:@"verifyCode"];
    self.scanId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Bid"];
    if (!self.scanId) {
        self.scanId = @"0";
    }
    WeakSelf(self)
    [self.accountApi verifyUserCodeLoginWithparameters:dic withId:self.scanId withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            NSDictionary *userData = arr.firstObject;
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"mobile"] forKey:@"mobile"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshUserVC object:nil];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Bid"];
            [CloudPushSDK addAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] withCallback:^(CloudPushCallbackResult *res) {
                if (res.success) {
                    JLog(@"设置成功");
                }
                
            }];
            
            [self dismissViewControllerAnimated:true completion:nil];
        }
    }];
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}

- (SignInApple *)signInApple{
    if (!_signInApple) {
        _signInApple = [SignInApple new];
    }
    return _signInApple;
}

@end
