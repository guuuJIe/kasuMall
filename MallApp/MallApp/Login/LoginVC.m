//
//  LoginVC.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "LoginVC.h"
#import "VerifyCodeLogin.h"
#import "GoodInfoVC.h"
#import "AccountApi.h"
#import "PayMannagerUtil.h"
#import "AuthoritaionLoginVC.h"

@interface LoginVC ()
@property (nonatomic) AccountApi *accountApi;
@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    if (![WXApi isWXAppInstalled]) {
        self.weChatLoginView.hidden = true;
    }
    
    if (self.scanId) {
        [[NSUserDefaults standardUserDefaults] setObject:self.scanId forKey:@"Bid"];
    }
    
}


//点击用户名登录
- (IBAction)userLoginView:(id)sender {
    self.telLoginView.hidden = true;
    self.nameLoginView.hidden = false;
}
//点击手机登录
- (IBAction)telLogin:(id)sender {
    self.telLoginView.hidden = false;
    self.nameLoginView.hidden = true;
}

- (IBAction)userPwdLogin:(id)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.nameText.text forKey:@"userName"];
    [dic setValue:self.passwordText.text forKey:@"passWord"];
    [JMBManager showLoading];
    WeakSelf(self)
    [self.accountApi accountLoginWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code != 200) {
//                        [JMBManager showBriefAlert:result.message];
        }else{
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            NSDictionary *userData = arr.firstObject;
//            [JPUSHService setAlias:[NSString stringWithFormat:@"%@",userData[@"userId"]] completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
//                
//            } seq:0];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
            [self dismissViewControllerAnimated:true completion:nil];
            
        }
//        NSLog(@"%@",result.resultDic);
        [JMBManager hideAlert];
    }];
    
    
}
- (IBAction)sendCode:(id)sender {
//
    if (self.telText.text.length != 11) {
        [JMBManager showBriefAlert:@"请输入电话号码"];
        return;
    }//15888017959
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.telText.text forKey:@"Phone"];
    [JMBManager showLoading];
    WeakSelf(self)
    [self.accountApi getverifyCodeWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        VerifyCodeLogin *login = [VerifyCodeLogin new];
        login.tel = self.telText.text;
        login.scanId = self.scanId;
        [self.navigationController pushViewController:login animated:true];
        [JMBManager hideAlert];
    }];
   
    
}
- (IBAction)backAction:(id)sender {
//    if (self.navigationController.viewControllers.count >0) {
//        [self.navigationController popViewControllerAnimated:true];
//    }else{
        
        [self dismissViewControllerAnimated:true completion:nil];
//    }
    
}
- (IBAction)weChatLogin:(UIButton *)sender {
    [[PayMannagerUtil sharedManager] loginWithWeChatMethodsuccessBlock:^(BaseResp * _Nonnull res) {
         SendAuthResp *authResp = (SendAuthResp *)res;
         NSLog(@"微信登录的code%@",authResp.code);
        WeakSelf(self)
        [self.accountApi AppWxLoginWithparameters:@{@"code":authResp.code} withCompletionHandler:^(NSError *error, MessageBody *result) {
            StrongSelf(self)
             NSLog(@"接口返回code%@",result.message);
            if (result.code == 200) {
                NSDictionary *dic = result.resultDic;
                NSArray *arr = dic[@"list"];
                NSDictionary *userData = arr.firstObject;
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self dismissViewControllerAnimated:true completion:nil];
            }else{
                AuthoritaionLoginVC *vc = [[AuthoritaionLoginVC alloc] initWithNibName:@"AuthoritaionLoginVC" bundle:nil];
                vc.uninoId = [NSString stringWithFormat:@"%@",result.message];
                vc.scanId = self.scanId;
                [self.navigationController pushViewController:vc animated:true];
            }
           
        }];
        
     
    } failBlock:^(BOOL fail) {
        
    }];
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}
- (IBAction)wechatLogin:(UIButton *)sender {
}
@end
