//
//  NewLoginWayView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewLoginWayView : UIView
+(instancetype)initViewClass;
@property (nonatomic,copy) void(^clickBlock)(NSInteger type);
@end

NS_ASSUME_NONNULL_END
