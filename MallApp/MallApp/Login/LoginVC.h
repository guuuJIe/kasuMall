//
//  LoginVC.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseHideNavViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginVC : BaseHideNavViewController
@property (weak, nonatomic) IBOutlet UIView *nameLoginView;
@property (weak, nonatomic) IBOutlet UIView *telLoginView;
@property (weak, nonatomic) IBOutlet UIView *weChatLoginView;
- (IBAction)userLoginView:(id)sender;
- (IBAction)telLogin:(id)sender;
- (IBAction)userPwdLogin:(id)sender;
//账户和密码登录
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;

//验证码操作
- (IBAction)sendCode:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *telText;
- (IBAction)wechatLogin:(UIButton *)sender;

@property (nonatomic,strong) NSString *scanId;
@end

NS_ASSUME_NONNULL_END
