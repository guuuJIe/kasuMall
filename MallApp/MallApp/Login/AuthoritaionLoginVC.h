//
//  AuthoritaionLoginVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthoritaionLoginVC : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *telText;
@property (weak, nonatomic) IBOutlet UIButton *smsBtn;
- (IBAction)SendCodeAct:(UIButton *)sender;
- (IBAction)ConfirmAct:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *codeText;

@property (nonatomic,strong) NSString *uninoId;
@property (nonatomic,strong) NSString *scanId;
@end

NS_ASSUME_NONNULL_END
