//
//  VerifyCodeLogin.h
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifyCodeLogin : BaseViewController
@property (nonatomic) NSString *tel;
@property (nonatomic,strong) NSString *scanId;
@end

NS_ASSUME_NONNULL_END
