//
//  NewLoginWayView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NewLoginWayView.h"
@interface NewLoginWayView()
@property (weak, nonatomic) IBOutlet UIView *wechatView;
@property (weak, nonatomic) IBOutlet UIView *verifyLogin;
@property (weak, nonatomic) IBOutlet UIView *accountView;

@end

@implementation NewLoginWayView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self.wechatView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.verifyLogin addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
    [self.accountView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)]];
}

+(instancetype)initViewClass{
    return [[[NSBundle mainBundle] loadNibNamed:@"NewLoginWayView" owner:self options:nil] lastObject];
}


- (void)click:(UITapGestureRecognizer *)ges{
    if (self.clickBlock) {
        self.clickBlock(ges.view.tag);
    }
}

@end
