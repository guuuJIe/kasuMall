//
//  VerifyCodeLogin.m
//  MallApp
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "VerifyCodeLogin.h"
#import "YN_PassWordView.h"
#import "AccountApi.h"
@interface VerifyCodeLogin ()
@property(nonatomic,strong) UILabel *telLbl;
@property(nonatomic,strong) YN_PassWordView *passView;
@property(nonatomic,strong) UIButton *dxdlSendSMSBtn;
@property(nonatomic,assign) int sendSMSTimeCount;
@property(nonatomic,strong) NSTimer *sendSMSTime;
@property(nonatomic,strong) AccountApi *accountApi;
@end

@implementation VerifyCodeLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"获取验证码";
    [self setupUI];
   
    [self sendSMS];
    
    NSLog(@"%@",self.navigationController.viewControllers);
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"LoginVC"]];
}

- (void)setupUI{
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(25,90,184,32);
    label.numberOfLines = 0;
    [self.view addSubview:label];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"请输入短信验证码" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 23], NSForegroundColorAttributeName: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]}];
    
    label.attributedText = string;
    label.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    label.alpha = 1.0;
    
    self.telLbl = [[UILabel alloc] init];
    self.telLbl.frame = CGRectMake(25,CGRectGetMaxY(label.frame)+20,Screen_Width-50,20);
    self.telLbl.numberOfLines = 0;
    self.telLbl.text = [NSString stringWithFormat:@"短信验证码将发送至+86 %@",self.tel];
    self.telLbl.font = LabelFont14;
    self.telLbl.textColor = [UIColor colorWithRed:182/255.0 green:182/255.0 blue:182/255.0 alpha:1.0];
    [self.view addSubview:self.telLbl];
    
   
    self.passView = [[YN_PassWordView alloc] init];
    self.passView.frame = CGRectMake(25, CGRectGetMaxY(self.telLbl.frame)+64, Screen_Width-25*2, 200/6);
        self.passView.backgroundColor= UIColorF5F7;
    self.passView.showType = 5;//五种样式
    self.passView.num = 6;//框框个数
    [self.passView show];
    [self.view addSubview:self.passView];
    [self.passView.textF becomeFirstResponder];
    
    WeakSelf(self)
    self.passView.textBlock = ^(NSString *str) {
        StrongSelf(self)
        NSLog(@"str----%@",str);
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setValue:weakself.tel forKey:@"phone"];
        [dic setValue:str forKey:@"verifyCode"];
        
        if (!self.scanId) {
            self.scanId = @"0";
        }
        [weakself.accountApi verifyUserCodeLoginWithparameters:dic withId:self.scanId withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code != 200) {
//                [JMBManager showBriefAlert:result.message];
            }else{
                NSDictionary *dic = result.resultDic;
                NSArray *arr = dic[@"list"];
                NSDictionary *userData = arr.firstObject;
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"accessToken"] forKey:@"accessToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"refreshToken"] forKey:@"refreshToken"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userId"] forKey:@"userId"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userName"] forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"userLevel"] forKey:@"userLevel"];
                [[NSUserDefaults standardUserDefaults] setObject:userData[@"mobile"] forKey:@"mobile"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:refreshAppHome object:nil];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Bid"];
                [self dismissViewControllerAnimated:true completion:nil];
            }
            NSLog(@"%@",result.resultDic);
        }];
        
    };
    
    
    self.dxdlSendSMSBtn=[UIButton new];
    [self.dxdlSendSMSBtn setTitle:@"重新获取" forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:APPColor forState:UIControlStateNormal];
    [self.dxdlSendSMSBtn setTitleColor:UIColorB6 forState:UIControlStateDisabled];
    self.dxdlSendSMSBtn.titleLabel.font=LabelFont14;
    [self.dxdlSendSMSBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.dxdlSendSMSBtn];
    
    
    [self.dxdlSendSMSBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passView.mas_bottom).offset(38);
        make.centerX.equalTo(self.view);
    }];
}

- (void)sendSMS{
    if(self.sendSMSTimeCount!=0){
        return;
    }
    self.sendSMSTimeCount=60;
    self.sendSMSTime=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
   
}

-(void)changeSMSTime{
    self.sendSMSTimeCount-=1;
    if(self.sendSMSTimeCount<=0){
        [self.sendSMSTime invalidate];
        [self.dxdlSendSMSBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.dxdlSendSMSBtn.enabled = true;
    }else{
      
        self.dxdlSendSMSBtn.enabled = false;
        [self.dxdlSendSMSBtn setTitle:[NSString stringWithFormat:@"%is后重新获取",self.sendSMSTimeCount] forState:UIControlStateNormal];
    }
}

- (AccountApi *)accountApi{
    if (!_accountApi) {
        _accountApi = [AccountApi new];
    }
    return _accountApi;
}

- (void)dealloc{
    NSLog(@"销毁了");
}

@end
