//
//  SubmitHelpInfoVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "SubmitHelpInfoVC.h"
#import "HelpStartPlaceView.h"
#import "HelpPlaceVC.h"
@interface SubmitHelpInfoVC ()
@property (nonatomic) HelpStartPlaceView *startView;
@end

@implementation SubmitHelpInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"救援信息";
    [self setupLayout];
}

- (void)setupLayout{
    [self.view addSubview:self.startView];
    [self.startView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(12);
        make.left.equalTo(self.view).offset(12);
        make.right.equalTo(self.view).offset(-12);
    }];
}

- (void)Click{
    HelpPlaceVC *vc = [HelpPlaceVC new];
    [self.navigationController pushViewController:vc animated:true];
}

- (HelpStartPlaceView *)startView{
    if (!_startView) {
        _startView = [[HelpStartPlaceView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 20, 100)];
        _startView.userInteractionEnabled = true;
        [_startView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Click)]];
        
    }
    return _startView;
}

@end
