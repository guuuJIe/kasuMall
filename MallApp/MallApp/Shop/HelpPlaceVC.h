//
//  HelpPlaceVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"
#import <AMapSearchKit/AMapSearchKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface HelpPlaceVC : BaseViewController
@property (nonatomic,copy) void(^clickBlock)(AMapPOI *poi);
@end

NS_ASSUME_NONNULL_END
