//
//  ShopInfoVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import  <AMapFoundationKit/AMapFoundationKit.h>
#import  <AMapLocationKit/AMapLocationKit.h>

#import "ShopInfoVC.h"
#import "ShopHeadView.h"
#import "FSScrollContentView.h"
#import "ShopHomeVC.h"
#import "ContentTableCell.h"
#import "ShopInfoBottomView.h"
#import "ShopOtherContentVC.h"
#import "ShopApi.h"
#import "GoodInfoVC.h"
#import "HelpDetailVC.h"
#import "HelpVC.h"
#import "SearchVC.h"
#import "BaseTableView.h"
#import "ShopBannerTableCell.h"
#import "ShopIntroTableCell.h"
#import "navView.h"
#import "PurchaseView.h"
#import "ShopInfoDetailVC.h"
@interface ShopInfoVC ()<UITableViewDelegate,UITableViewDataSource,FSPageContentViewDelegate,FSPageContentViewDelegate,FSSegmentTitleViewDelegate,UIScrollViewDelegate,AMapLocationManagerDelegate>
@property (nonatomic, strong) BaseTableView *TabelView;
@property (nonatomic, strong) FSSegmentTitleView *titleView;
@property (nonatomic, strong) ShopHeadView *headView;
@property (nonatomic, strong) ContentTableCell *contentCell;
@property (nonatomic, strong) ShopInfoBottomView *actView;
@property (nonatomic, strong) ShopApi *api;
@property (nonatomic, strong) NSMutableDictionary *totalDic;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *productArr;
@property (nonatomic, strong) UIButton * searchBtn;
@property (nonatomic, assign) BOOL canScroll;
@property (nonatomic, assign) BOOL cellScroll;
@property (nonatomic, assign) CGFloat curContentoffset;
@property (nonatomic, strong) navView *navbar;
@property (nonatomic, strong) PurchaseView *connactBtn;
@property (nonatomic, strong) NSArray *checkHasOwnApp;
@property (nonatomic, strong) AMapLocationManager *locationManager;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@end

@implementation ShopInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTopAct:) name:@"changeTop" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeScrollStatus) name:@"leaveTop" object:nil];
    [self setupUI];
    [self setupData];
    
}

- (void)viewWillAppear:(BOOL)animated{
    self.view.backgroundColor = [UIColor whiteColor];
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
//    [self initBar];
}

- (void)initBar{
    

    UIView * searview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width,20)];
    searview.backgroundColor = [UIColor clearColor];

    self.navigationItem.titleView = searview;
  
    
    
    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, searview.frame.size.width - 90, 25)];
    [self.searchBtn setTitle:@"搜索店内商品" forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    [self.searchBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.searchBtn setBackgroundColor:[UIColor whiteColor]];
    [self.searchBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 0, 5, 0)];
    self.searchBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.searchBtn setImage:[UIImage imageNamed:@"搜索"] forState:(UIControlStateNormal)];
    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    self.searchBtn.layer.cornerRadius = 13;
    self.searchBtn.layer.masksToBounds = YES;
    [searview addSubview:self.searchBtn];
    
    
}

- (void)onClickSearchBtn{
    [self.navigationController pushViewController:[SearchVC new] animated:true];
}


- (void)setupUI{
    
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-50-BottomAreaHeight);
    }];
    
    
//    [self.view addSubview:self.actView];
//    [self.actView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.left.right.equalTo(self.view);
//        make.height.mas_equalTo(50+BottomAreaHeight);
//    }];
    
    
    [self.view addSubview:self.navbar];
    [self.navbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(44+StatusBarHeight);
    }];
    

    [self.view addSubview:self.connactBtn];
    [self.connactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    AdjustsScrollViewInsetNever(self, self.TabelView);
}

- (void)setupData{
    self.cellScroll = true;
    WeakSelf(self)
    [self.api shopInfoWithparameters:[XJUtil insertStringWithNotNullObject:self.shopsId andDefailtInfo:@""] withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSArray *data = result.resultDic[@"list"];
            NSDictionary *dic = data.firstObject;
            self.totalDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
            [self configData:dic];
            [self.headView setupData:self.totalDic];
        }
    }];
}

- (void)configData:(NSDictionary *)dic{
    self.dataArr = [NSMutableArray array];
    NSDictionary *defaultDic = @{@"serviceName":@"首页"};
    [self.dataArr addObject:defaultDic];
    NSArray *data = dic[@"service"];
    NSMutableArray *titleArr = [NSMutableArray new];
    for (int i = 0; i<data.count; i++) {
        NSDictionary *dic2 = data[i];
        [self.dataArr addObject:dic2];
        
    }
    for (int i = 0; i<self.dataArr.count; i++) {
        NSDictionary *dic2 = self.dataArr[i];
        
        [titleArr addObject:dic2[@"serviceName"]];
    }
    
    self.productArr = [NSMutableArray array];
    [self.productArr addObject:dic[@"productList"]];
    [self.productArr addObject:dic[@"repairList"]];
    self.titleView = [[FSSegmentTitleView alloc] initWithFrame:CGRectMake(85, 105, Screen_Width - 70, 40) titles:titleArr delegate:self indicatorType:FSIndicatorTypeCustom];
    self.titleView.titleSelectFont = LabelFont16;
    self.titleView.titleSelectColor = APPColor;
    self.titleView.indicatorColor = APPColor;
    self.titleView.titleNormalColor = UIColor333;
    [self.headView addSubview:self.titleView];
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.headView);
        make.size.mas_equalTo(CGSizeMake(Screen_Width - 70, 40));
        make.left.mas_equalTo(70);
    }];
    [self.locationManager startUpdatingLocation];
    [self.TabelView reloadData];
    
}






#pragma mark ---UITableViewDelegate---

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return self.dataArr.count > 0 ? 1 : 0;
    return 1;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
             return CGRectGetHeight(self.view.bounds);
        }
      
    }
    return UITableViewAutomaticDimension;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        static NSString *Identifier = @"ShopBannerTableCell";
        ShopBannerTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (!cell) {
            cell = [[ShopBannerTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.totalDic];
        return cell;
    }else if (indexPath.section == 1){
        
        static NSString *Identifier = @"ShopIntroTableCell";
        ShopIntroTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (!cell) {
            cell = [[ShopIntroTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.totalDic];
        WeakSelf(self)
        cell.clickBlock = ^(NSInteger type) {
            StrongSelf(self)
            if (type == 1) {
                ShopInfoDetailVC *vc = [ShopInfoDetailVC new];
                vc.dic = self.totalDic;
                [self.navigationController pushViewController:vc animated:true];
            }else if (type == 2){
                [XJUtil checkHasOwnApp:self withData:self.totalDic];
            }
        };
        return cell;
        
        
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"ContentTableCell";
        ContentTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        
        if (!cell) {
            cell = [[ContentTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
            
        }else{
            NSMutableArray *contentVCs = [NSMutableArray array];
            for (NSDictionary *dic in self.dataArr) {
                NSString *title = dic[@"serviceName"];
                if (![title isEqualToString:@"首页"]) {
                    ShopOtherContentVC *vc = [[ShopOtherContentVC alloc]init];
                    
                    [contentVCs addObject:vc];
                }else{
                    ShopHomeVC *vc = [[ShopHomeVC alloc]init];
                    vc.dataDic = self.totalDic;
                    vc.title = title;
                    vc.str = title;
                    [contentVCs addObject:vc];
                    
                    WeakSelf(self)
                    vc.checkMoreBlock = ^{
                        
                        ShopOtherContentVC *vc = (ShopOtherContentVC*)weakself.contentCell.viewControllers[1];
                        vc.type = 1;
                        vc.shopsId = weakself.shopsId;
                        weakself.titleView.selectIndex = 1;
                        weakself.contentCell.pageContentView.contentViewCurrentIndex = 1;
                    };
                    
                    vc.itemclickBlock = ^(NSDictionary * _Nonnull dic) {
                        GoodInfoVC *vc = [GoodInfoVC new];
                        vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
                        [weakself.navigationController pushViewController:vc animated:true];
                    };
                }
                
            }
            cell.viewControllers = contentVCs;
            cell.pageContentView = [[FSPageContentView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, CGRectGetHeight(self.view.bounds)) childVCs:contentVCs parentVC:self delegate:self];
            [cell.contentView addSubview:cell.pageContentView];
        }
        
        _contentCell = cell;
        
        return cell;
    }
    

    return nil;

    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 2) {
         return self.headView;
    }
    
    return [UIView new];
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 2) {
        return 50.0f;
    }
    return CGFLOAT_MIN;
}


#pragma mark FSSegmentTitleViewDelegate
- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex
{
    if (endIndex == 1) {
        ShopOtherContentVC *vc = (ShopOtherContentVC*)self.contentCell.viewControllers[1];
        vc.type = 1;
        vc.shopsId = self.shopsId;
    }else if (endIndex == 2){
        ShopOtherContentVC *vc = (ShopOtherContentVC*)self.contentCell.viewControllers[2];
        vc.type = 3;
        vc.shopsId = self.shopsId;
    }
    self.contentCell.pageContentView.contentViewCurrentIndex = endIndex;
}

- (void)FSContentViewDidScroll:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex progress:(CGFloat)progress
{
//    _listTable.scrollEnabled = NO;//pageView开始滚动主tableview禁止滑动
    if (endIndex == 1) {
         ShopOtherContentVC *vc = (ShopOtherContentVC*)self.contentCell.viewControllers[1];
         vc.type = 1;
         vc.shopsId = self.shopsId;
     }else if (endIndex == 2){
         ShopOtherContentVC *vc = (ShopOtherContentVC*)self.contentCell.viewControllers[2];
         vc.type = 3;
         vc.shopsId = self.shopsId;
     }
    

    
    [self.titleView setPageTitleViewWithProgress:progress originalIndex:startIndex targetIndex:endIndex];
   
//     _TabelView.scrollEnabled = YES;//此处其实是监测scrollview滚动，pageView滚动结束主tableview可以滑动，或者通过手势监听或者kvo，这里只是提供一种实现方式
}

- (void)changeScrollStatus//改变主视图的状态
{
    self.canScroll = YES;

}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGRect rectsize = [self.TabelView rectForSection:2];
    JLog(@"%f   ---   %f ---   %f",scrollView.contentOffset.y, rectsize.size.height,rectsize.origin.y);
    CGFloat value = scrollView.contentOffset.y;
    CGFloat limit = rectsize.origin.y - StatusBarAndNavigationBarHeight;

    
    if (value >= limit) {
        
        scrollView.contentOffset = CGPointMake(0, limit);
        if (self.cellScroll) {
            _curContentoffset = limit;
            _canScroll = false;
            _cellScroll = false;
        }
        
        
    }else{
        
        if (value<=0) {
            scrollView.contentOffset = CGPointMake(0, 0);
            _cellScroll = true;
        }
        
        if (!self.canScroll && _curContentoffset) {
            scrollView.contentOffset = CGPointMake(0, limit);
            
            JLog(@"执行了");
        }
    }

    if (scrollView.contentOffset.y >StatusBarAndNavigationBarHeight) {
        
        [UIView animateWithDuration:0.5 animations:^{
            self.navbar.backgroundColor = APPColor;
            [self.navbar.backButton setImage:[UIImage imageNamed:@"back"] forState:0];
        }];
        
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            self.navbar.backgroundColor = [UIColor clearColor];
            [self.navbar.backButton setImage:[UIImage imageNamed:@"返回"] forState:0];
        }];
    }
    
}

#pragma mark - AMapLocationManagerDelegate 协议

#pragma mark - 定位失败

- (void)amapLocationManager:(AMapLocationManager *)manager didFailWithError:(NSError *)error

{
    [_locationManager stopUpdatingLocation];

    [JMBManager showBriefAlert:@"定位失败"];

}

#pragma mark - 定位成功

- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location

{
    
    [_locationManager stopUpdatingLocation];
    
   
    self.coordinate = location.coordinate;
    NSArray *datas = self.totalDic[@"service"];
    NSDictionary *dic =  datas.firstObject;
    
    
    double distance =  [self distanceBetweenOrderBy:location.coordinate.latitude :[dic[@"gpsLatitude"] doubleValue] :[dic[@"gpsLongitude"] doubleValue] :location.coordinate.longitude];
    JLog(@"当前距离:%f",distance);
    NSDecimalNumber *distance2 = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",distance]];
    [self.totalDic setValue:distance2 forKey:@"distance"];
    [self.TabelView reloadData];
}

-(double)distanceBetweenOrderBy:(double) latitude1 :(double) latitude2 :(double) longitude1 :(double) longitude2{
    
    CLLocation *curLocation = [[CLLocation alloc] initWithLatitude:latitude1 longitude:longitude1];
    
    CLLocation *otherLocation = [[CLLocation alloc] initWithLatitude:latitude2 longitude:longitude2];
    
    
    double  distance  = [curLocation distanceFromLocation:otherLocation];
    
    return  distance/1000;
    
}


- (void)dealloc{
    JLog(@"销毁");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(BaseTableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[BaseTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellID"];
        _TabelView.backgroundColor = [UIColor whiteColor];
        [_TabelView registerClass:[ShopBannerTableCell class] forCellReuseIdentifier:@"ShopBannerTableCell"];
        [_TabelView registerClass:[ShopIntroTableCell class] forCellReuseIdentifier:@"ShopIntroTableCell"];
        [_TabelView registerClass:[ContentTableCell class] forCellReuseIdentifier:@"ContentTableCell"];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (ShopHeadView *)headView{
    if (!_headView) {
        _headView = [[ShopHeadView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
//        [_headView addSubview:self.titleView];
        
        WeakSelf(self);
        _headView.backBlock = ^{
            [weakself.navigationController popViewControllerAnimated:true];
        };
//        [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.right.equalTo(self->_headView);
//        }];
        
        _headView.searchBlock = ^{
            [weakself.navigationController pushViewController:[SearchVC new] animated:true];
        };
    }
    
    return _headView;
}



- (ShopApi *)api{
    if (!_api) {
        _api = [ShopApi new];
    }
    return _api;
}

- (ShopInfoBottomView *)actView{
    if (!_actView) {
        _actView = [[ShopInfoBottomView alloc] init];
        WeakSelf(self);
        _actView.commonBlock = ^(NSInteger tag) {
            StrongSelf(self);
            if (tag == 2) {
                JLog(@"点击了道路救援");
                if (!accessToken) {
                    [JMBManager showBriefAlert:@"请先登录"];
                    return ;
                }
                [self.api repairOpentWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        NSArray *datas = result.resultDic[@"list"];
                        if ([datas.firstObject isEqual:[NSNull null]]) {
                            HelpVC *vc = [HelpVC new];
                            [self.navigationController pushViewController:vc animated:true];
                        }else{
                            [JSCMMPopupViewTool showMMPopAlertWithMessage:@"你有订单未完成,是否前往查看?" andHandler:^(NSInteger index) {
                                if (index == 1) {
                                    HelpDetailVC *vc = [HelpDetailVC new];
                                    [self.navigationController pushViewController:vc animated:true];
                                }
                            }];
                        }
                        
                    }
                }];
            }else if (tag == 3){
                [XJUtil callTelNoCommAlert:weakself.totalDic[@"tel"]];
            }

        };
        
    }
    return _actView;
}

- (navView *)navbar{
    if (!_navbar) {
        _navbar = [navView new];
        
        WeakSelf(self);
        _navbar.clickBlock = ^{
            [weakself.navigationController popViewControllerAnimated:true];
        };
    }
    return _navbar;
}



- (PurchaseView *)connactBtn{
    if (!_connactBtn) {
        _connactBtn = [PurchaseView new];
        [_connactBtn.purchaseButton setTitle:@"联系商家" forState:0];
        [_connactBtn.purchaseButton setBackgroundColor:APPColor];
        [_connactBtn.purchaseButton setImage:[UIImage imageNamed:@"R_connact"] forState:0];
        [_connactBtn.purchaseButton setTitleColor:UIColor.whiteColor forState:0];
        WeakSelf(self)
        _connactBtn.addAddressBlock = ^{
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"请确认您要拨打的联系电话号码" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            UIAlertAction * action2 = [UIAlertAction actionWithTitle:weakself.totalDic[@"tel"] style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                [XJUtil callTelNoCommAlert:weakself.totalDic[@"tel"]];
            }];
            UIAlertAction * action3 = [UIAlertAction actionWithTitle:weakself.totalDic[@"phone"] style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                [XJUtil callTelNoCommAlert:weakself.totalDic[@"phone"]];
            }];
            [alert addAction:action1];
            [alert addAction:action2];
            [alert addAction:action3];
            [weakself presentViewController:alert animated:YES completion:nil];
            
        };
        [self.view addSubview:_connactBtn];
    }
    
    return _connactBtn;
}


-(AMapLocationManager *)locationManager{
    
    if (!_locationManager) {
        
        _locationManager = [[AMapLocationManager alloc]init];
        _locationManager.delegate = self;
        
    }
    
    return _locationManager;
    
}



@end
