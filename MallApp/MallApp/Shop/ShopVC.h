//
//  ShopVC.h
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShopVC : BaseViewController
@property(nonatomic,strong) NSString *shopId;
@end

NS_ASSUME_NONNULL_END
