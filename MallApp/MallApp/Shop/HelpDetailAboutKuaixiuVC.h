//
//  HelpDetailAboutKuaixiuVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HelpDetailAboutKuaixiuVC : BaseViewController
@property (nonatomic,strong) NSString *orderNum;
@property (nonatomic,assign) OperationType opType;
@end

NS_ASSUME_NONNULL_END
