//
//  ShopHomeVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopHomeVC : UIViewController
@property (nonatomic, assign) BOOL vcCanScroll;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) BOOL isRefresh;
@property (nonatomic, strong) NSString *str;
@property (nonatomic,copy) void(^checkMoreBlock)(void);
@property (nonatomic,copy) void(^itemclickBlock)(NSDictionary *dic);
@property (nonatomic) NSDictionary *dataDic;
@end

NS_ASSUME_NONNULL_END
