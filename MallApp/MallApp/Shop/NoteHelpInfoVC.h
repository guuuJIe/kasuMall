//
//  NoteHelpInfoVC.h
//  MallApp
//
//  Created by Mac on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NoteHelpInfoVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *adressLbl;

@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UITextField *carNumLbl;
@property (weak, nonatomic) IBOutlet UITextField *usernameLbl;
@property (weak, nonatomic) IBOutlet UITextField *telLbl;

@property (weak, nonatomic) IBOutlet UITextField *quesLbl;
@property (nonatomic,assign) NSInteger protype;
@property (nonatomic,strong) NSString *reason;
@end

NS_ASSUME_NONNULL_END
