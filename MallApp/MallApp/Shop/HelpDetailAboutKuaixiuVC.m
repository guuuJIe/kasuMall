//
//  HelpDetailVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpDetailAboutKuaixiuVC.h"
#import "HelpDetailAboutKuaiXiuProgressCell.h"
#import "HelpDetailSectionTwoCell.h"
#import "HelpDetailSectionThreeCell.h"
#import "HelpOrderOperationView.h"
#import "ShopApi.h"
#import "PayWayCell.h"
#import "OrderApi.h"
#import "PayMannagerUtil.h"
#import "MineFixOrderVC.h"
#import "CustomePwdView.h"
#import "IQKeyboardManager.h"
#import "HelpServiceCompanyTableCell.h"
#import "KuaiXiuOrderPriceDetailCell.h"
@interface HelpDetailAboutKuaixiuVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *TabelView;
@property (nonatomic, strong) HelpOrderOperationView *oprationView;
@property (nonatomic, strong) ShopApi *api;
@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, assign) NSInteger value;
@property (nonatomic, strong) OrderApi *orderapi;
@property (nonatomic, strong) NSArray *amountArr;
@property (nonatomic, strong) NSIndexPath *selectIndexPath;
@property (nonatomic, assign) NSInteger payway;
@property (nonatomic, strong) CustomePwdView *pwdView;
@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, strong) NSString *pwdStr;
@end

@implementation HelpDetailAboutKuaixiuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self getInfoData];
    [self getBalance];
    self.navigationController.viewControllers = [XJUtil removeNavViewController:self.navigationController.viewControllers withArrayClassName:@[@"HelpPlaceVC",@"AnotherNoteHelpInfoVC",@"HelpVC",@"MineFixOrderVC"]];
    self.payway = 5;
    if (!self.opType) {
        self.opType = UserEmergencyOrderOpe;
    }
    // 添加通知监听见键盘弹出/退出
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //TODO: 页面appear 禁用
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [self.pwdView.passView.textF resignFirstResponder];
   
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    //TODO: 页面Disappear 启用
   [[IQKeyboardManager sharedManager] setEnable:YES];

}

- (void)setupUI{
    self.title = @"订单详情";
    [self.view addSubview:self.oprationView];
    [self.oprationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
   
    
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        
        make.bottom.mas_equalTo(-50-BottomAreaHeight);
    }];
    
    
}

- (void)getData{
    [self.api repairOpentWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *arr = result.resultDic[@"list"];
            self.dataDic = arr.firstObject;
            [self.oprationView setOrderOptionWithDic:self.dataDic withType:self.opType];
             self.value = [self.dataDic[@"status"] integerValue];
            [self.TabelView reloadData];
        }
    }];
}

- (void)getInfoData{
    [self.api getrepairOrderInfoWithparameters:self.orderNum withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *arr = result.resultDic[@"list"];
            self.dataDic = arr.firstObject;
            [self.oprationView setOrderOptionWithDic:self.dataDic withType:self.opType];
            self.value = [self.dataDic[@"status"] integerValue];
            [self.TabelView reloadData];
            
        }
        [self.TabelView.mj_header endRefreshing];
    }];
}

- (void)getBalance{
   
    
    [self.orderapi getAmountWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            self.amountArr = dic[@"list"];
            [self.TabelView reloadData];
        }
    }];
}


- (void)payOrder{
    
    if (self.payway == 5) {
        
        [self.pwdView.passView.textF becomeFirstResponder];
        return;
    }else{
        self.pwdStr = @"";
    }
    
    
    [self payAct];
   
}

- (void)payAct{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    
    [dic setValue:@"APP" forKey:@"clientType"];
    [dic setValue:@(self.payway) forKey:@"payment"];
    [dic setValue:self.dataDic[@"orderNumber"] forKey:@"outTradeNo"];
    if (self.pwdStr) {
        [dic setValue:self.pwdStr forKey:@"Password"];
    }
    //普通买家
    [self.orderapi payOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSDictionary *dic = result.resultDic;
            NSArray *arguData = dic[@"list"];
            if (self.payway == 3) {//支付宝
                [[PayMannagerUtil sharedManager] payWithZhifubaoMethod:arguData.firstObject successBlock:^(BOOL success) {
                    if (success) {
                        [self paySuccess];
                    }
                } failBlock:^(BOOL fail) {
                    //                    self.isCallPay = 1;
                    [JMBManager showBriefAlert:@"支付失败"];
                }];
            }else if(self.payway == 2){//微信
                [[PayMannagerUtil sharedManager] payWithWeChatMethod:arguData.firstObject successBlock:^(BaseResp * _Nonnull res) {
                    switch (res.errCode) {
                        case WXSuccess:{
                            [self paySuccess];
                        }
                            
                            break;
                            
                        default:{
                            
                            [JMBManager showBriefAlert:@"支付失败"];
                        }
                            break;
                    }
                } failBlock:^(BOOL fail) {
                    
                }];
            }else if (self.payway == 5){
                [self paySuccess];
            }
            
        }
        
    }];
}

- (void)paySuccess{
//    [self.navigationController pushViewController:[MineFixOrderVC new] animated:true];
    [self.pwdView.passView.textF resignFirstResponder];
    [self getInfoData];
}


// 键盘监听事件
- (void)keyboardAction:(NSNotification*)sender{
    // 通过通知对象获取键盘frame: [value CGRectValue]
    NSDictionary *useInfo = [sender userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    //取出键盘弹出需要花费的时间
    double duration = [useInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // <注意>具有约束的控件通过改变约束值进行frame的改变处理
    if([sender.name isEqualToString:UIKeyboardWillShowNotification]){
//        self.toBottom.constant = [value CGRectValue].size.height;
         [UIView animateWithDuration:duration animations:^{
             self.pwdView.frame = CGRectMake(0, self.view.frame.size.height - 200 - [value CGRectValue].size.height, Screen_Width, 200);
         }];
    }else{
        [UIView animateWithDuration:1 animations:^{
            self.pwdView.frame = CGRectMake(0, self.view.frame.size.height, Screen_Width, 200);
        }];
    }
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.value == 2) {
        return 4;
    }else{
        return 3;
    }
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 3) {
        return 3;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"HelpDetailAboutKuaiXiuProgressCell";
        HelpDetailAboutKuaiXiuProgressCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[HelpDetailAboutKuaiXiuProgressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataDic];
        return cell;
    }else if (indexPath.section ==1){
        static NSString *Identifier = @"HelpServiceCompanyTableCell";
        HelpServiceCompanyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[HelpServiceCompanyTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }

        [cell setupData:self.dataDic];
        if (self.value == Unpay || self.value == ApplyCancel) {
            cell.hidden = true;
        }else{
            cell.hidden = false;
        }
        return cell;
        
    }else if (indexPath.section ==2){
        static NSString *Identifier = @"KuaiXiuOrderPriceDetailCell";
        KuaiXiuOrderPriceDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[KuaiXiuOrderPriceDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setUpData:self.dataDic];
        return cell;
        
    }else if (indexPath.section == 3){
        static NSString *Identifier = @"PayWayCell";
        PayWayCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[PayWayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setAmount:self.amountArr];
        [cell setupData:self.dataArr[indexPath.row]];
        if (_selectIndexPath.row == indexPath.row) {
            cell.selectBtn.selected = true;
        }
        if (indexPath.row != 0) {
            cell.leftAmountLbl.hidden = true;
        }else{
            cell.leftAmountLbl.hidden = false;
        }
       
        return cell;
    }
    
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 110;
    }else if (indexPath.section == 2){
        return UITableViewAutomaticDimension;
    }else if(indexPath.section == 1){
        return self.value == Unpay || self.value == ApplyCancel ? CGFLOAT_MIN : UITableViewAutomaticDimension;
    }else{
        return UITableViewAutomaticDimension;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3) {
        _selectIndexPath = indexPath;
        if (indexPath.row == 0) {
            self.payway = 5;
        }else if (indexPath.row == 1){//支付宝支付
            self.payway = 3;
            
        }else if(indexPath.row == 2){//微信支付
            self.payway = 2;
        }
        [_TabelView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
    }
   
}

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        _TabelView.estimatedSectionHeaderHeight = 0;
        _TabelView.estimatedSectionFooterHeight = 0;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerClass:[HelpDetailAboutKuaiXiuProgressCell class] forCellReuseIdentifier:@"HelpDetailAboutKuaiXiuProgressCell"];
//        [_TabelView registerNib:[UINib nibWithNibName:@"HelpDetailSectionTwoCell" bundle:nil] forCellReuseIdentifier:@"HelpDetailSectionTwoCell"];
        [_TabelView registerClass:[KuaiXiuOrderPriceDetailCell class] forCellReuseIdentifier:@"KuaiXiuOrderPriceDetailCell"];
        [_TabelView registerClass:[HelpDetailSectionThreeCell class] forCellReuseIdentifier:@"HelpDetailSectionThreeCell"];
        [_TabelView registerNib:[UINib nibWithNibName:@"PayWayCell" bundle:nil] forCellReuseIdentifier:@"PayWayCell"];
        [_TabelView registerClass:[HelpServiceCompanyTableCell class] forCellReuseIdentifier:@"HelpServiceCompanyTableCell"];
        _TabelView.backgroundColor = [UIColor whiteColor];
        WeakSelf(self)
        _TabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakself getInfoData];
        }];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (HelpOrderOperationView *)oprationView{
    if (!_oprationView) {
        _oprationView = [HelpOrderOperationView new];
        WeakSelf(self)
        _oprationView.orderHandleBlock = ^(NSString * _Nonnull flag) {
            StrongSelf(self)
            if ([flag isEqualToString:@"orderCancel"]) {
                [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定取消当前订单？" andHandler:^(NSInteger index) {
                    if (index == 1) {
                        [self.orderapi cancelUserFastSetviceOrderWithparameters:@{@"OrderNumber":self.orderNum} withCompletionHandler:^(NSError *error, MessageBody *result) {
                            if (result.code == 200) {
                                [self getInfoData];
                            }
                        }];
                    }
                }];
            }else if ([flag isEqualToString:@"connact"]){
                [JSCMMPopupViewTool showMMPopAlertWithTitle:@"请通知对方" message:@"在个人中心/维修订单里取消订单" withCancelButtonTitle:@"取消" withSureButton:@"立即联系" andHandler:^(NSInteger index) {
                    if (index == 1) {
                        [XJUtil callTelNoCommAlert:self.dataDic[@"shopTel"]];
                    }
                }];
            }else if ([flag isEqualToString:@"orderConfirm"]){
                [JSCMMPopupViewTool showMMPopAlertWithMessage:@"确定当前服务商？" andHandler:^(NSInteger index) {
                    if (index == 1) {
                        [self.api confirmfixOrderServicerWithparameters:@{} withCompletionHandler:^(NSError *error, MessageBody *result) {
                            if (result.code == 200) {
                                 if (self.orderNum) {
                                    [self getInfoData];
                                   }else{
                                      [self getData];
                                   }
                            }
                        }];
                    }
                }];
            }else if ([flag isEqualToString:@"orderPay"]){
                [self payOrder];
            }
        };
    }
    return _oprationView;
}


- (CustomePwdView *)pwdView{
    if (!_pwdView) {
        _pwdView = [[CustomePwdView alloc] initWithFrame:CGRectMake(0, ViewHeight, Screen_Width, 200)];
        _pwdView.passView.textF.keyboardType = UIKeyboardTypeNumberPad;
        WeakSelf(self)
        _pwdView.finishBlock = ^(NSString * _Nonnull str) {
            StrongSelf(self)
            self.pwdStr = str;
            
//            [self.params setValue:str forKey:@"Password"];
//
            [self payAct];
            
//            //普通买家
//            [self.api payOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
//                if (result.code == 200) {
//                    NSDictionary *dic = result.resultDic;
//                    [self.pwdView.passView.textF resignFirstResponder];
//                    if (self.payway == 5){
//                        [self paySuccess];
//                    }
//
//                }else{
//                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付密码不正确" preferredStyle:UIAlertControllerStyleAlert];
//                    UIAlertAction *act1 = [UIAlertAction actionWithTitle:@"忘记密码" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
//                        [self.pwdView.passView.textF resignFirstResponder];
//                        [self.pwdView.passView cleanPassword];
//                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                            SetPayVC *vc = [[SetPayVC alloc] initWithNibName:@"SetPayVC" bundle:nil];
//                                                   [self.navigationController pushViewController:vc animated:true];
//                        });
//
//
//                    }];
//                    UIAlertAction *act2 = [UIAlertAction actionWithTitle:@"重新输入" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                        [self.pwdView.passView cleanPassword];
////                        [self.pwdView.passView.textF becomeFirstResponder];
//                    }];
//
//                    [alert addAction:act1];
//                    [alert addAction:act2];
//                    [self presentViewController:alert animated:true completion:nil];
//                }
//
//            }];
        };
        [self.view addSubview:_pwdView];
    }
    return _pwdView;
}


- (NSMutableDictionary *)params{
    if (!_params) {
        _params = [NSMutableDictionary dictionary];
        [_params setValue:@"APP" forKey:@"clientType"];
        [_params setValue:@(self.payway) forKey:@"payment"];
        [_params setValue:self.dataDic[@"orderNumber"] forKey:@"outTradeNo"];
    }
    
    return _params;
}

-(ShopApi *)api{
    if (!_api) {
        _api = [ShopApi new];
    }
    return _api;
}

- (OrderApi *)orderapi{
    if (!_orderapi) {
        _orderapi = [OrderApi new];
    }
    return _orderapi;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
        NSArray *arr = @[@{@"name":@"余额支付",@"image":@"pay_1"},@{@"name":@"支付宝支付",@"image":@"pay_2"},@{@"name":@"微信支付",@"image":@"pay_3"}];
        [_dataArr addObjectsFromArray:arr];
    }
    
    return _dataArr;
}
@end


