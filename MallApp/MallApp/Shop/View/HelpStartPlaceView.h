//
//  HelpStartPlaceView.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface HelpStartPlaceView : UIView
@property (weak, nonatomic) IBOutlet UIView *contenView;
@property (nonatomic) UILabel *telLabel;
- (void)setupAdress:(AMapPOI *)poi;
@end

NS_ASSUME_NONNULL_END
