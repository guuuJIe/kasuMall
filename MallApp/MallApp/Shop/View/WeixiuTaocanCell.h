//
//  WeixiuTaocanCell.h
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WeixiuTaocanCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *goodImage;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *shopnameLbl;
@property (nonatomic,copy) void(^itemClickBlock)(void);
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
