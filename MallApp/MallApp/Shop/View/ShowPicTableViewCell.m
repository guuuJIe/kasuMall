//
//  ShowPicTableViewCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShowPicTableViewCell.h"
@interface ShowPicTableViewCell()
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIImageView *headImage;
@end

@implementation ShowPicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = UIColor.whiteColor;
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    UIView *view = [UIView new];
    view.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(45);
    }];
    [view addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(view);
    }];
    
    
    [self.contentView addSubview:self.headImage];
    [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_bottom).offset(32);
        make.centerX.mas_equalTo(self.contentView);
//        make.size.mas_equalTo(CGSizeMake(249, 170));
        make.bottom.mas_equalTo(-32);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.titleLbl.text = dic[@"title"];
        
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"file"]];
//        [self.headImage yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"placeholder_square"]];
        [self.headImage yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"placeholder_square"] options:YYWebImageOptionProgressive completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(image.size.width*AdapterScal, image.size.height*AdapterScal));
            }];
            
            [self.contentView layoutIfNeeded];
        }];
    }
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.textColor = UIColor333;
        _titleLbl.font = LabelFont16;
    }
    
    return _titleLbl;
}

- (UIImageView *)headImage{
    if (!_headImage) {
        _headImage = [UIImageView new];
      
    }
    
    return _headImage;
}

@end
