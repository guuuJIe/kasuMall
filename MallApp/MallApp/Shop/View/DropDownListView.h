//
//  DropDownListView.h
//  MallApp
//
//  Created by Mac on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DropDownListView : UIView
@property (nonatomic , strong) NSArray *datasource;
@property (nonatomic , copy) void(^clickBlock)(NSInteger row);
@property (nonatomic , copy) void(^bgViewHideBlock)(void);
-(void)showSelView;
-(void)hidenSelView;
@end

NS_ASSUME_NONNULL_END
