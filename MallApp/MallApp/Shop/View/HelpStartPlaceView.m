//
//  HelpStartPlaceView.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpStartPlaceView.h"
@interface HelpStartPlaceView()
@property (nonatomic) UIImageView *headImage;
@property (nonatomic) UIImageView *arrow;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *adressLabel;

@end

@implementation HelpStartPlaceView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
//        HelpStartPlaceView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"HelpStartPlaceView" owner:self options:nil] lastObject];
//
//        [self addSubview:lastView];
//
////        self = lastView;
//        lastView.contenView.backgroundColor = [UIColor redColor];
//        lastView.frame = frame;
        [self setupLayout];
        
    }
    return self;
}

- (void)setupLayout{
    
    [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.equalTo(self).offset(-5);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headImage.mas_right).offset(15);
        make.top.mas_equalTo(self).offset(5);
    }];
    
    [self.adressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(8);
        make.right.mas_equalTo(self).offset(-45);
    }];
    
    [self.telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.adressLabel);
        make.top.mas_equalTo(self.adressLabel.mas_bottom).offset(8);
//        make.height.mas_equalTo(18);
//        make.bottom.equalTo(self).offset(-15);
    }];
    
    [self.arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.telLabel.mas_bottom).offset(15);
        make.bottom.equalTo(self).offset(0);
        make.left.equalTo(@10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(line);
    }];
    
}

- (void)setupAdress:(AMapPOI *)poi{
    self.titleLabel.text = poi.name;
    self.adressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",poi.province,poi.city,poi.district,poi.name];
}

-(UIImageView *)headImage
{
    if(!_headImage)
    {
        _headImage=[UIImageView  new];
        _headImage.image = [UIImage imageNamed:@"adress"];
//        _headImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_headImage];
    }
    return _headImage;
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"救援地址";
        _titleLabel.font = LabelFont14;
        _titleLabel.textColor = UIColor333;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(UILabel *)adressLabel
{
    if(!_adressLabel)
    {
        _adressLabel=[UILabel  new];
        _adressLabel.text = @"填写救援地址";
        _adressLabel.font = LabelFont14;
        _adressLabel.textColor = UIColor999;
        _adressLabel.numberOfLines = 1;
        [self addSubview:_adressLabel];
    }
    return _adressLabel;
}

-(UILabel *)telLabel
{
    if(!_telLabel)
    {
        _telLabel=[UILabel  new];
        _telLabel.text = @"";
        _telLabel.font = LabelFont14;
        _telLabel.textColor = UIColor333;
        _telLabel.numberOfLines = 1;
        [self addSubview:_telLabel];
    }
    return _telLabel;
}

-(UIImageView *)arrow
{
    if(!_arrow)
    {
        _arrow=[UIImageView  new];
        _arrow.image = [UIImage imageNamed:@"more"];
//        _headImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_arrow];
    }
    return _arrow;
}

@end
