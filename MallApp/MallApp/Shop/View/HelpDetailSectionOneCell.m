//
//  HelpDetailSectionOneCell.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpDetailSectionOneCell.h"
#import "XFStepView.h"

@interface HelpDetailSectionOneCell()

@property (strong, nonatomic)XFStepView *stepView;
@end

@implementation HelpDetailSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    _stepView = [[XFStepView alloc]initWithFrame:CGRectMake(0, 24, Screen_Width, 60) Titles:[NSArray arrayWithObjects:@"派单中", @"选择维修厂", @"服务中", @"待付款", @"已完成", nil]];
    self.stepView.stepIndex = 0;
    [self.contentView addSubview:self.stepView];
    
    UIView *view = [UIView new];
    view.backgroundColor = bgColor;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.stepView.mas_bottom).offset(15);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    NSInteger value = [dic[@"status"] integerValue] ;
    self.stepView.stepIndex = value - 1;
}
@end
