//
//  ShopInfoSectionCell.h
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopInfoSectionCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *HeadLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)checkMore:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *checkMoreBtn;
@property (nonatomic,copy) void(^checkMoreBlock)(void);
@property (nonatomic,copy) void(^itemClickBlock)(NSDictionary *dic);
@property (weak, nonatomic) IBOutlet UIImageView *typeTitleImage;
@property (nonatomic) NSArray *dataArr;
@end

NS_ASSUME_NONNULL_END
