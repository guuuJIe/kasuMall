//
//  PicCollectionCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "PicCollectionCell.h"
@interface PicCollectionCell()

@end
@implementation PicCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    [self.contentView addSubview:self.picImageView];
    [self.picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

- (UIImageView *)picImageView{
    if (!_picImageView) {
        _picImageView = [UIImageView new];
        _picImageView.layer.cornerRadius = 8;
//        _picImageView.backgroundColor = APPColor;
    }
    
    return _picImageView;
}
@end
