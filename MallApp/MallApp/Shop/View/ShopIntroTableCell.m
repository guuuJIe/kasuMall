//
//  ShopIntroTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopIntroTableCell.h"
#import "ShopIntroView.h"
@interface ShopIntroTableCell()
@property (nonatomic, strong)ShopIntroView *introView;
@property (nonatomic, strong)UILabel *adressLbl;
@property (nonatomic, strong)UIButton *navBtn;
@end

@implementation ShopIntroTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    [self.contentView addSubview:self.introView];
    [self.introView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.mas_equalTo(12);
        make.width.mas_equalTo(Screen_Width);
//        make.bottom.mas_equalTo(self.contentView).offset(-12);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorED;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.introView.mas_bottom).offset(14);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UIImageView *adressBg = [UIImageView new];
    adressBg.image = [UIImage imageNamed:@"white_address"];
    [self.contentView addSubview:adressBg];
    [adressBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(14);
        make.size.mas_equalTo(CGSizeMake(17, 17));
        make.top.mas_equalTo(line.mas_bottom).offset(11);
    }];
    
    [self.contentView addSubview:self.navBtn];
    [self.navBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.mas_equalTo(adressBg);
    }];
    
    [self.contentView addSubview:self.adressLbl];
    [self.adressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(adressBg.mas_right).offset(2);
        make.centerY.mas_equalTo(adressBg);
        make.bottom.mas_equalTo(-12);
//        make.right.mas_equalTo(self.navBtn.mas_left);
        make.width.mas_lessThanOrEqualTo(Screen_Width-100);
    }];
    
  
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        [self.introView setupData:dic];
        NSArray *datas = dic[@"service"];
        if (datas.count == 1) {
            NSDictionary *dic = datas.firstObject;
            self.adressLbl.text = [NSString stringWithFormat:@"%@%@",[XJUtil insertStringWithNotNullObject:dic[@"address"] andDefailtInfo:@""],[XJUtil insertStringWithNotNullObject:dic[@"street"] andDefailtInfo:@""]];
        }else if(datas.count == 2){
            NSDictionary *dic = datas[1];
            self.adressLbl.text = [NSString stringWithFormat:@"%@%@",[XJUtil insertStringWithNotNullObject:dic[@"address"] andDefailtInfo:@""],[XJUtil insertStringWithNotNullObject:dic[@"street"] andDefailtInfo:@""]];
        }
//        NSDecimalNumber *distance = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",]];
        [self.navBtn setTitle:[NSString stringWithFormat:@"%@km",dic[@"distance"]] forState:0];
        
    }
   
}

- (void)clickAct{
    if (self.clickBlock) {
        self.clickBlock(1);
    }
}

- (void)navAct{
    if (self.clickBlock) {
           self.clickBlock(2);
       }
}

- (ShopIntroView *)introView{
    if (!_introView) {
        _introView = [ShopIntroView new];
        [_introView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAct)]];
    }
    return _introView;
}

- (UILabel *)adressLbl{
    if (!_adressLbl) {
        _adressLbl = [UILabel new];
        _adressLbl.textColor = UIColor333;
        _adressLbl.font = LabelFont14;
        _adressLbl.text = @"温州大道工业企业";
        [self.contentView addSubview:_adressLbl];
    }
    
    return _adressLbl;
}

- (UIButton *)navBtn{
    if (!_navBtn) {
        _navBtn = [UIButton new];
        [_navBtn setImage:[UIImage imageNamed:@"R_nav"] forState:0];
        [_navBtn setTitle:@"0km" forState:0];
        [_navBtn setTitleColor:UIColor333 forState:0];
        [_navBtn.titleLabel setFont:LabelFont14];
        [_navBtn addTarget:self action:@selector(navAct) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navBtn;
}
@end
