//
//  CommonLayout.m
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "CommonLayout.h"

@implementation CommonLayout

-(instancetype)init{
    self = [super init];
    if (self) {
        self.itemSize = CGSizeMake(Screen_Width/3, 150);
        self.minimumLineSpacing = 0.0;
        self.minimumInteritemSpacing = 0.0;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return self;
}
@end
