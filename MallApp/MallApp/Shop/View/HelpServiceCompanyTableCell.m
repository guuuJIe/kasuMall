//
//  HelpServiceCompanyTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/5.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpServiceCompanyTableCell.h"
@interface HelpServiceCompanyTableCell()
@property (nonatomic, strong)UILabel *nameLbl;
@property (nonatomic, strong)UILabel *stateLbl;
@property (nonatomic, strong)UILabel *telLbl;
@property (nonatomic, strong)UILabel *distanceLbl;
@end

@implementation HelpServiceCompanyTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    
    [self.contentView addSubview:self.nameLbl];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(15);
    }];
    
    [self.contentView addSubview:self.stateLbl];
    [self.stateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLbl);
        make.top.mas_equalTo(self.nameLbl.mas_bottom).offset(6);
    }];
    
    [self.contentView addSubview:self.telLbl];
    [self.telLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLbl);
        make.top.mas_equalTo(self.stateLbl.mas_bottom).offset(6);
    }];
    
    [self.contentView addSubview:self.distanceLbl];
    [self.distanceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(lineHeihgt);
        make.top.mas_equalTo(self.telLbl.mas_bottom).offset(15);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSInteger status = [dic[@"status"] integerValue];
        if (status == SendOrders) {//派单中
            self.nameLbl.hidden = true;
            self.telLbl.hidden = true;
            self.distanceLbl.hidden = true;
        }else if (status == Servicing || status == ApplyCancel || status == TradeComplete || status == TradeCancel || status == SellerConfirm){
            self.stateLbl.hidden = true;
            self.stateLbl.text = @"";
            self.nameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopName"] andDefailtInfo:@""];
            self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopTel"] andDefailtInfo:@""];

//            self.distanceLbl.hidden = true;

        }
        self.distanceLbl.text = [NSString stringWithFormat:@"%@公里",[XJUtil insertStringWithNotNullObject:dic[@"distance"] andDefailtInfo:@"0"]];
    }
}

- (UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [UILabel new];
        _nameLbl.text = @"";
        _nameLbl.textColor = UIColor333;
        _nameLbl.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    }
    
    
    return _nameLbl;
}

- (UILabel *)stateLbl{
    if (!_stateLbl) {
        _stateLbl = [UILabel new];
        _stateLbl.text = @"服务商稍后显示";
        _stateLbl.textColor = UIColor66;
        _stateLbl.font = LabelFont14;
    }

    return _stateLbl;
}


- (UILabel *)telLbl{
    if (!_telLbl) {
        _telLbl = [UILabel new];
        _telLbl.text = @"1358892235";
        _telLbl.textColor = UIColor999;
        _telLbl.font = LabelFont12;
    }

    return _telLbl;
}

- (UILabel *)distanceLbl{
    if (!_distanceLbl) {
        _distanceLbl = [UILabel new];
        _distanceLbl.text = @"";
        _distanceLbl.textColor = UIColor66;
        _distanceLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
//        _distanceLbl.hidden = true;
    }

    return _distanceLbl;
}

@end
