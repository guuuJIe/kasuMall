//
//  WeixiuItemCell.h
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WeixiuItemCell : UICollectionViewCell
- (void)setupData:(NSDictionary *)dic;
@property (weak, nonatomic) IBOutlet UIImageView *imgaeview;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;
@end

NS_ASSUME_NONNULL_END
