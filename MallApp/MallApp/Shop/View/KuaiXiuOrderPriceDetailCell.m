//
//  KuaiXiuOrderPriceDetailCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "KuaiXiuOrderPriceDetailCell.h"
#import "ShowFixRuleView.h"
@interface KuaiXiuOrderPriceDetailCell()
@property (nonatomic, strong) UILabel *statusLbl;
@property (nonatomic, strong) UILabel *timeLbl;
@property (nonatomic, strong) UILabel *typeLbl;

@property (nonatomic, strong) UILabel *codeLbl;
@property (nonatomic, strong) UILabel *confirmCodeLbl;

@property (nonatomic, strong) UILabel *addressLbl;

@property (nonatomic, strong) UILabel *destionLbl;
@property (nonatomic, strong) UILabel *desLbl;

@property (nonatomic, strong) UILabel *priceLbl;


@end

@implementation KuaiXiuOrderPriceDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout{
    UILabel *title = [UILabel new];
    title.text = @"订单状态：";
    title.textColor = UIColor999;
    title.font = LabelFont14;
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(14);
    }];
    [self.contentView addSubview:self.statusLbl];
    [self.statusLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title.mas_right).offset(0);
        make.centerY.mas_equalTo(title);
    }];
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"服务时间：";
    title2.textColor = UIColor999;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(title.mas_bottom).offset(14);
    }];
    [self.contentView addSubview:self.timeLbl];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title2.mas_right).offset(0);
        make.centerY.mas_equalTo(title2);
    }];
    

    UILabel *title3 = [UILabel new];
    title3.text = @"服务类型：";
    title3.textColor = UIColor999;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(title2.mas_bottom).offset(14);
        
    }];
    [self.contentView addSubview:self.typeLbl];
    [self.typeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title3.mas_right).offset(0);
        make.centerY.mas_equalTo(title3);
    }];
    
    UILabel *title4 = [UILabel new];
    title4.text = @"确 认 码：";
    title4.textColor = UIColor999;
    title4.font = LabelFont14;
    [self.contentView addSubview:title4];
    [title4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(title3.mas_bottom).offset(14);
        
    }];
    self.confirmCodeLbl = title4;
    [self.contentView addSubview:self.codeLbl];
    [self.codeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title4.mas_right).offset(4);
        make.centerY.mas_equalTo(title4);
    }];
    
    UILabel *title5 = [UILabel new];
    title5.text = @"救 援 地：";
    title5.textColor = UIColor999;
    title5.font = LabelFont14;
    [self.contentView addSubview:title5];
    [title5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(title4.mas_bottom).offset(14);
        
    }];
    
    [self.contentView addSubview:self.addressLbl];
    [self.addressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title5.mas_right).offset(4);
        make.top.mas_equalTo(title5).offset(0);
        make.right.mas_equalTo(-10);
    }];
    
    UILabel *title6 = [UILabel new];
    title6.text = @"目 的 地：";
    title6.textColor = UIColor999;
    title6.font = LabelFont14;
    [self.contentView addSubview:title6];
    [title6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.addressLbl.mas_bottom).offset(14);
        
    }];
    self.desLbl = title6;
    [self.contentView addSubview:self.destionLbl];
    [self.destionLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title6.mas_right).offset(4);
        make.top.mas_equalTo(title6);
        make.right.mas_equalTo(-10);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(lineHeihgt);
//        make.bottom.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.destionLbl.mas_bottom).offset(14);
    }];
    
    
    UILabel *title7 = [UILabel new];
    title7.text = @"订单总价";
    title7.textColor = UIColor999;
    title7.font = LabelFont14;
    [self.contentView addSubview:title7];
    [title7 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(14);
        
    }];
    UIButton *button = [UIButton new];
    [button setImage:[UIImage imageNamed:@"ques"] forState:0];
    [self.contentView addSubview:button];
    [button addTarget:self action:@selector(showRules) forControlEvents:UIControlEventTouchUpInside];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title7.mas_right).offset(0);
        make.centerY.mas_equalTo(title7);
        make.size.mas_equalTo(CGSizeMake(22, 20));
    }];
    
    [self.contentView addSubview:self.priceLbl];
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(title7);
    }];
    
    UIView *lin2 = [UIView new];
    lin2.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:lin2];
    [lin2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(10);
        make.bottom.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.priceLbl.mas_bottom).offset(14);
    }];
}


- (void)setUpData:(NSDictionary *)dic{
    if (dic) {
        NSInteger value = [dic[@"status"] integerValue];
        if (value == 24) {
            self.statusLbl.text = @"申请取消";
        }else if (value == 2){

            self.statusLbl.text = @"待付款";
        }else if (value == 21){

            self.statusLbl.text = @"派单中";
        }else if (value == 22){

            self.statusLbl.text = @"服务中";
        }else if (value == 6){
            self.statusLbl.text = @"已完成";
        }else if (value == 7){
            self.statusLbl.text = @"已取消";
        }else if (value == 23){
            self.statusLbl.text = @"商家已确认";
        }
        
        self.timeLbl.text = [NSString stringWithFormat:@"%@",dic[@"orderTime"]];
        self.typeLbl.text = [NSString stringWithFormat:@"%@",[XJUtil insertStringWithNotNullObject:dic[@"proTypeString"] andDefailtInfo:@""]];
        self.addressLbl.text = [NSString stringWithFormat:@"%@",dic[@"locationString"]];
        self.destionLbl.text = [NSString stringWithFormat:@"%@",[XJUtil insertStringWithNotNullObject:dic[@"locationToString"] andDefailtInfo:@""]];
        self.desLbl.hidden = [dic[@"locationToString"] isEqual:[NSNull null]] ? true : false;
        self.destionLbl.hidden = [dic[@"locationToString"] isEqual:[NSNull null]] ? true : false;
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
        self.codeLbl.text = [NSString stringWithFormat:@"%@",[XJUtil insertStringWithNotNullObject:dic[@"code"] andDefailtInfo:@""]];
        self.codeLbl.hidden = [dic[@"code"] isEqual:[NSNull null]] ? true : false;
        self.confirmCodeLbl.hidden = [dic[@"code"] isEqual:[NSNull null]] ? true : false;
        
        if (self.destionLbl.hidden) {
            self.desLbl.text = @"";
        }
    }
}

- (void)showRules{
    ShowFixRuleView *ruleView = [ShowFixRuleView new];
    [ruleView show];
}

- (UILabel *)statusLbl{
    if (!_statusLbl) {
        _statusLbl = [UILabel new];
        _statusLbl.textColor = UIColor333;
        _statusLbl.font = LabelFont14;
    }
    
    return _statusLbl;
}

- (UILabel *)timeLbl{
    if (!_timeLbl) {
        _timeLbl = [UILabel new];
        _timeLbl.textColor = UIColor333;
        _timeLbl.font = LabelFont14;
    }
    
    return _timeLbl;
}

- (UILabel *)typeLbl{
    if (!_typeLbl) {
        _typeLbl = [UILabel new];
        _typeLbl.textColor = UIColor333;
        _typeLbl.font = LabelFont14;
    }
    
    return _typeLbl;
}

- (UILabel *)codeLbl{
    if (!_codeLbl) {
        _codeLbl = [UILabel new];
        _codeLbl.textColor = APPColor;
        _codeLbl.font = LabelFont14;
    }
    
    return _codeLbl;
}

- (UILabel *)addressLbl{
    if (!_addressLbl) {
        _addressLbl = [UILabel new];
        _addressLbl.textColor = UIColor333;
        _addressLbl.font = LabelFont14;
        _addressLbl.numberOfLines = 0;
    }
    
    return _addressLbl;
}

- (UILabel *)destionLbl{
    if (!_destionLbl) {
        _destionLbl = [UILabel new];
        _destionLbl.textColor = UIColor333;
        _destionLbl.font = LabelFont14;
        _destionLbl.textAlignment = NSTextAlignmentLeft;
        _destionLbl.numberOfLines = 0;
    }
    
    return _destionLbl;
}

- (UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.textColor = UIColorFF3E;
        _priceLbl.font = LabelFont14;
        
    }
    
    return _priceLbl;
}
@end
