//
//  ShowFixRuleView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShowFixRuleView.h"
@interface ShowFixRuleView()

@end

@implementation ShowFixRuleView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.type = MMPopupTypeCustom;
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(Screen_Width, Screen_Height));
        }];
        UIView *bgView = [UIView new];
        [self addSubview:bgView];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.cornerRadius = 8;
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(46*AdapterScal);
            make.right.mas_equalTo(-46*AdapterScal);
            make.center.mas_equalTo(self);
            make.height.mas_equalTo(285*AdapterScal);
        }];
        
        UILabel *title = [UILabel new];
        title.text = @"价格计算须知";
        title.textColor = UIColor333;
        title.font = LabelFont16;
        [bgView addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(12*AdapterScal);
            make.centerX.mas_equalTo(bgView);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = UIColorEF;
        [bgView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(bgView);
            make.top.mas_equalTo(title.mas_bottom).offset(13*AdapterScal);
            make.height.mas_equalTo(lineHeihgt);
        }];
        
        
        UILabel *title2 = [UILabel new];
        title2.text = @"1.订单价格是通过计算服务商于您所在的位置的距离得出的。\n\n 2.出单以后不得取消退单，如果特殊情况，请电话联系服务商沟通";
        title2.textColor = UIColor999;
        title2.font = LabelFont14;
        title2.numberOfLines = 0;
        [bgView addSubview:title2];
        [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(line.mas_bottom).offset(20*AdapterScal);
            make.centerX.mas_equalTo(bgView);
            make.width.mas_equalTo(236*AdapterScal);
        }];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"我知道了" forState:0];
        [button addTarget:self action:@selector(hidebg) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundColor:APPColor];
        button.layer.cornerRadius = 4;
        [button.titleLabel setFont:LabelFont14];
        [bgView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(-15);
            make.height.mas_equalTo(40);
            make.bottom.mas_equalTo(bgView).offset(-18*AdapterScal);
        }];
        
    }
    return self;
}


- (void)hidebg{
    [self hide];
}
@end
