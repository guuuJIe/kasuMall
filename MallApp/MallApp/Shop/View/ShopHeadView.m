//
//  ShopHeadView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopHeadView.h"
#import "FSScrollContentView.h"

@interface ShopHeadView()
@property (nonatomic)UIImageView *headImage;
@property (nonatomic)UIImageView *backImage;
@property (nonatomic)UIButton *searchBtn;

@end

@implementation ShopHeadView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
       
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    
    self.backgroundColor = UIColorF5F7;

    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(14, 10, 60, 30)];
    [self.searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];

//    [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:0.2 alpha:0.3]];
    [self.searchBtn setBackgroundColor:[UIColor colorWithRed:168/255.0 green:168/255.0 blue:168/255.0 alpha:0.3]];
    [self.searchBtn setImageEdgeInsets:UIEdgeInsetsMake(5, -5, 5, 0)];
    self.searchBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.searchBtn setImage:[UIImage imageNamed:@"search"] forState:(UIControlStateNormal)];
    [self.searchBtn setTitleColor:UIColor333 forState:0];
    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
//    self.searchBtn.alpha = 0.27;
    self.searchBtn.layer.cornerRadius = 14;
    [self addSubview:self.searchBtn];
//    [self.searchBtn setTintColor:[UIColor whiteColor]];
//    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
//
//    self.searchBtn.layer.cornerRadius = 20;
//    self.searchBtn.layer.masksToBounds = YES;
//    [searview addSubview:self.searchBtn];
//    [self addSubview:searview];
//    [searview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.shopImage);
//        make.top.equalTo(self.shopImage.mas_bottom).offset(17);
//    }];
    
    
}
- (void)onClickSearchBtn{
    if (self.searchBlock) {
        self.searchBlock();
    }
}

- (void)click{
    if (self.backBlock) {
        self.backBlock();
    }
}

- (void)setupData:(NSDictionary *)dic{
   
    [self.introView setupData:dic];
}

-(UIImageView *)headImage
{
    if(!_headImage)
    {
        _headImage=[UIImageView  new];
        _headImage.image = [UIImage imageNamed:@"bghead"];
        _headImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _headImage;
}

-(UIImageView *)backImage
{
    if(!_backImage)
    {
        _backImage=[UIImageView  new];
        _backImage.image = [UIImage imageNamed:@"back"];
        _backImage.userInteractionEnabled = true;
        [_backImage addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
        _backImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _backImage;
}

- (ShopIntroView *)introView{
    if (!_introView) {
        _introView = [ShopIntroView new];
    }
    return _introView;
}


@end
