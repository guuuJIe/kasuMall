//
//  AddressCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AMapSearchKit/AMapSearchKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface AddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *buildLbl;

@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
- (void)setData:(AMapPOI *)poi;
@end

NS_ASSUME_NONNULL_END
