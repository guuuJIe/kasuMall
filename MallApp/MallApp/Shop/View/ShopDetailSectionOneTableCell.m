//
//  ShopDetailSectionOneTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopDetailSectionOneTableCell.h"
#import "ShopIntroView.h"
@interface ShopDetailSectionOneTableCell()
@property (nonatomic, strong)ShopIntroView *introView;
@end

@implementation ShopDetailSectionOneTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = UIColor.whiteColor;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    [self.contentView addSubview:self.introView];
    [self.introView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.left.mas_equalTo(12);
        make.width.mas_equalTo(Screen_Width);
        make.bottom.mas_equalTo(self.contentView).offset(-12);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        [self.introView setupData:dic];
        self.introView.arrBtn.hidden = true;
    }
}


- (ShopIntroView *)introView{
    if (!_introView) {
        _introView = [ShopIntroView new];
    }
    return _introView;
}

@end
