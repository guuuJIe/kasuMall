//
//  HelpStartPlaceView.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpEndPlaceView.h"
@interface HelpEndPlaceView()
@property (nonatomic) UIImageView *headImage;
@property (nonatomic) UIImageView *arrow;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *adressLabel;
@property (nonatomic) UILabel *telLabel;
@end

@implementation HelpEndPlaceView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];

        [self setupLayout];
        
    }
    return self;
}

- (void)setupLayout{
    
    [self.headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headImage.mas_right).offset(15);
        make.centerY.mas_equalTo(self).offset(-12);
    }];
    
    [self.adressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(8);
    }];
    
    
    
    [self.arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.adressLabel.mas_bottom).offset(15);
        make.bottom.equalTo(self).offset(0);
        make.left.equalTo(@10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
//    [self mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(line);
//    }];
    
}

-(UIImageView *)headImage
{
    if(!_headImage)
    {
        _headImage=[UIImageView  new];
        _headImage.image = [UIImage imageNamed:@"adress"];
//        _headImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_headImage];
    }
    return _headImage;
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"拖车目的地";
        _titleLabel.font = LabelFont14;
        _titleLabel.textColor = UIColor333;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

-(UILabel *)adressLabel
{
    if(!_adressLabel)
    {
        _adressLabel=[UILabel  new];
        _adressLabel.text = @"填写拖车目的地";
        _adressLabel.font = LabelFont14;
        _adressLabel.textColor = UIColor999;
        _adressLabel.numberOfLines = 1;
        [self addSubview:_adressLabel];
    }
    return _adressLabel;
}



-(UIImageView *)arrow
{
    if(!_arrow)
    {
        _arrow=[UIImageView  new];
        _arrow.image = [UIImage imageNamed:@"more"];
//        _headImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_arrow];
    }
    return _arrow;
}

@end
