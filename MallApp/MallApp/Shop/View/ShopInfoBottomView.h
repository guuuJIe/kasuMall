//
//  ShopInfoBottomView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopInfoBottomView : UIView

@property (nonatomic, copy) void(^commonBlock)(NSInteger tag);

- (IBAction)helpAct:(UIButton *)sender;
- (IBAction)connactAct:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
