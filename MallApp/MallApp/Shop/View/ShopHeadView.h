//
//  ShopHeadView.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopIntroView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ShopHeadView : UIView
@property (nonatomic, copy) void (^backBlock)(void);
@property (nonatomic, strong) ShopIntroView *introView;
@property (nonatomic, copy) void (^searchBlock)(void);

- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
