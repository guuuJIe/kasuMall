//
//  HelpDetailSectionThreeCell.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpDetailSectionThreeCell.h"
#import "FixOrderListImageCollectionCell.h"
#import <YBImageBrowser/YBImageBrowser.h>
@interface HelpDetailSectionThreeCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) UILabel *TitleLabel;
@property (nonatomic) UILabel *priceLabel;
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *dataArray;
@end

@implementation HelpDetailSectionThreeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
//    [self.contentView addSubview:self.TitleLabel];
//    [self.TitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(12);
//        make.top.mas_equalTo(12);
//    }];
//
//    [self.contentView addSubview:self.priceLabel];
//    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self).offset(-12);
//        make.top.equalTo(self.TitleLabel);
//    }];
    
//    UIView *view = [UIView new];
//    view.backgroundColor = bgColor;
//    [self.contentView addSubview:view];
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.TitleLabel.mas_bottom).offset(12);
//        make.left.right.equalTo(self.contentView);
//        make.height.mas_equalTo(lineHeihgt);
//    }];
    
    UILabel *list = [UILabel new];
    list.text = @"维修清单";
    list.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    list.textColor = [UIColor colorWithHexString:@"333333"];
    list.numberOfLines = 1;
    [self.contentView addSubview:list];
    [list mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
         make.left.mas_equalTo(12);
    }];
    
    [self.contentView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(list.mas_bottom).offset(15);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(80);
    }];
    
    UIView *bottomview = [UIView new];
    bottomview.backgroundColor = bgColor;
    [self.contentView addSubview:bottomview];
    [bottomview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom).offset(10);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
    
}
- (void)setupData:(NSDictionary *)dic{
    if (dic) {
//        self.priceLabel.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"¥%@",dic[@"realPrice"]] andDefailtInfo:@"¥0.0"];
        NSString *str = [NSString stringWithFormat:@"%@",dic[@"repairDetailed"]];
       
        NSArray *datas;
        if (str.length != 0) {
            datas = [str componentsSeparatedByString:@","];
        }
        
        [self.dataArray removeAllObjects];
        [datas enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YBIBImageData *data = [YBIBImageData new];
            NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,datas[idx]];
            data.imageURL = URL(url);
            
            [self.dataArray addObject:data];
            
        }];
        
        NSLog(@"%@",self.dataArray);
        [self.collectionView reloadData];
    }
}

- (void)setupListData:(NSDictionary *)dic{
    if (dic) {
        self.priceLabel.text = [XJUtil insertStringWithNotNullObject:[NSString stringWithFormat:@"¥%@",dic[@"realPrice"]] andDefailtInfo:@"¥0.0"];
        NSString *str = [NSString stringWithFormat:@"%@",dic[@"repairDetailed"]];
        NSArray *datas;
        if (str.length != 0) {
            datas = [str componentsSeparatedByString:@","];
        }
        
        [self.dataArray removeAllObjects];
        [datas enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YBIBImageData *data = [YBIBImageData new];
            NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,datas[idx]];
            data.imageURL = URL(url);
            
            [self.dataArray addObject:data];
            
        }];
        
        JLog(@"%@",self.dataArray);
        [self.collectionView reloadData];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FixOrderListImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FixOrderListImageCollectionCell" forIndexPath:indexPath];
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,self.dataArray[indexPath.item]];
      
    [cell.dataImageView yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"placeholder_square"]];
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section

{
     return 10.f;

}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section

{
    return 10.f;

}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 10, 0, 0);//分别为上、左、下、右
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = self.dataArray;
    browser.currentPage = indexPath.item;
    //       browser.delegate = self;
    [browser show];
}

-(UILabel *)TitleLabel
{
    if(!_TitleLabel)
    {
        _TitleLabel=[UILabel  new];
        _TitleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
        _TitleLabel.textColor = [UIColor colorWithHexString:@"333333"];
        _TitleLabel.numberOfLines = 1;
        _TitleLabel.text = @"订单总价";

    }
    return _TitleLabel;
}

-(UILabel *)priceLabel
{
    if(!_priceLabel)
    {
        _priceLabel=[UILabel  new];
        _priceLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
        _priceLabel.textColor = [UIColor colorWithHexString:@"FF3E3E"];
        _priceLabel.numberOfLines = 1;
        _priceLabel.text = @"￥0.00";

    }
    return _priceLabel;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(100, 100);
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.minimumInteritemSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
//        [_collectionView registerClass:[FliterCell class] forCellWithReuseIdentifier:@"FliterCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"FixOrderListImageCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"FixOrderListImageCollectionCell"];
   
    }
    
    return _collectionView;
    
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    
    return _dataArray;
}

@end
