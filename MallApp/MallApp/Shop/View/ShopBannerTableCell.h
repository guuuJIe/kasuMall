//
//  ShopBannerTableCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopBannerTableCell : UITableViewCell
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
