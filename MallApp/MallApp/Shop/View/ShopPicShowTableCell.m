//
//  ShopPicShowTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopPicShowTableCell.h"
#import "PicCollectionCell.h"
#import <YBImageBrowser/YBImageBrowser.h>
@interface ShopPicShowTableCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UICollectionView *picCollectionView;
@property (nonatomic, strong) NSMutableArray *datasArray;

@end
@implementation ShopPicShowTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIView *view = [UIView new];
    view.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(10);
        make.height.mas_equalTo(10);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view.mas_bottom).offset(12);
        make.left.mas_equalTo(12);
    }];
    
    [self.contentView addSubview:self.picCollectionView];
    [self.picCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.bottom.mas_equalTo(self.contentView).offset(-12);
        make.height.mas_equalTo(89);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(self.titleLbl.mas_bottom).offset(12);
    }];
}

- (void)setPicArray:(NSArray *)picArray{
    _picArray = picArray;
    
    [self.datasArray removeAllObjects];
    [picArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        YBIBImageData *data = [YBIBImageData new];
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,obj];
        data.imageURL = URL(url);
        
        [self.datasArray addObject:data];
        
    }];
    
   
    [self.picCollectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.datasArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PicCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PicCollectionCell" forIndexPath:indexPath];
    NSString *str = [NSString stringWithFormat:@"%@%@",URLAddress,self.picArray[indexPath.item]];
    [cell.picImageView yy_setImageWithURL:URL(str) placeholder:[UIImage imageNamed:@""]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
   
      YBImageBrowser *browser = [YBImageBrowser new];
     browser.dataSourceArray = self.datasArray;
      browser.currentPage = indexPath.item;
     
      [browser show];
}

- (NSMutableArray *)datasArray{
    if (!_datasArray) {
        _datasArray = [NSMutableArray array];
    }
    
    return _datasArray;
}
- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.text = @"店铺照片";
        _titleLbl.textColor = UIColor333;
        _titleLbl.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
        [self.contentView addSubview:_titleLbl];
    }
    
    return _titleLbl;
}

- (UICollectionView *)picCollectionView{
    if (!_picCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.itemSize = CGSizeMake(142, 89);
        _picCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        
        _picCollectionView.backgroundColor = [UIColor whiteColor];
        _picCollectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4);
        _picCollectionView.dataSource = self;
        _picCollectionView.delegate = self;
        _picCollectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _picCollectionView.showsHorizontalScrollIndicator = false;
        [_picCollectionView registerClass:[PicCollectionCell class] forCellWithReuseIdentifier:@"PicCollectionCell"];

        [self.contentView addSubview:_picCollectionView];
    }
    
    return _picCollectionView;
}

@end
