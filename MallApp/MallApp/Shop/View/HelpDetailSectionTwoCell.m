//
//  HelpDetailSectionTwoCell.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpDetailSectionTwoCell.h"
@interface HelpDetailSectionTwoCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *destionLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *codeLbl;
@property (weak, nonatomic) IBOutlet UILabel *desLbl;
@property (weak, nonatomic) IBOutlet UILabel *confirmCodeLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *helpPlaceCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *totalPriceTopCons;
@end

@implementation HelpDetailSectionTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setUpData:(NSDictionary *)dic{
    if (dic) {
        NSInteger value = [dic[@"status"] integerValue];
        if (value == 1) {
            self.titleLbl.text = @"等待商家接单";
            self.statusLbl.text = @"等待商家接单";
            self.subTitleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopAddress"] andDefailtInfo:@""];
        }else if (value == 2){
            self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopName"] andDefailtInfo:@""];
            self.subTitleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopAddress"] andDefailtInfo:@""];
            self.statusLbl.text = @"选择维修厂";
        }else if (value == 3){
            self.titleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopName"] andDefailtInfo:@""];
            self.subTitleLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopAddress"] andDefailtInfo:@""];
            self.statusLbl.text = @"服务中";
        }else if (value == 4){
            self.titleLbl.text = @"等待支付";
            self.statusLbl.text = @"等待支付";
        }else if (value == 5){
            self.titleLbl.text = @"支付完成";
            self.statusLbl.text = @"已完成";
        }else if (value == 6){
            
        }else if (value == 7){
            
        }
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",[XJUtil insertStringWithNotNullObject:dic[@"realPrice"] andDefailtInfo:@""]];
        self.timeLbl.text = [NSString stringWithFormat:@"%@",dic[@"orderTime"]];
        self.typeLbl.text = [NSString stringWithFormat:@"%@",[XJUtil insertStringWithNotNullObject:dic[@"proTypeString"] andDefailtInfo:@""]];
        self.addressLbl.text = [NSString stringWithFormat:@"%@",dic[@"locationString"]];
        self.destionLbl.text = [NSString stringWithFormat:@"%@",[XJUtil insertStringWithNotNullObject:dic[@"locationToString"] andDefailtInfo:@""]];
        self.desLbl.hidden = [dic[@"locationToString"] isEqual:[NSNull null]] ? true : false;
        self.destionLbl.hidden = [dic[@"locationToString"] isEqual:[NSNull null]] ? true : false;
    }
}
@end
