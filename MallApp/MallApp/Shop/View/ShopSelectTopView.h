//
//  ShopSelectTopView.h
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopSelectTopView : UIView
@property (nonatomic,copy) void(^clickBlock)(NSInteger tag);
@property (nonatomic,assign) NSInteger orderStatues;
@property (nonatomic,strong) UIButton *proviceBtn;
@property (nonatomic,strong) UIButton *cityBtn;
@property (nonatomic,strong) UIButton *areaBtn;

@end

NS_ASSUME_NONNULL_END
