//
//  ShopIntroView.m
//  MallApp
//
//  Created by Mac on 2020/3/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopIntroView.h"
@interface ShopIntroView()
@property (nonatomic)UIImageView *shopImage;
@property (nonatomic)UILabel *titleLabel;
@property (nonatomic)NSMutableArray *dataArr;
@end

@implementation ShopIntroView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
         self.backgroundColor = [UIColor clearColor];
        [self createUI];
    }
    
    return self;
}

- (void)createUI{
    [self addSubview:self.shopImage];
    [self.shopImage mas_makeConstraints:^(MASConstraintMaker *make) {

        make.size.mas_equalTo(CGSizeMake(44, 44));
        make.left.mas_equalTo(self).offset(0);
        make.bottom.mas_equalTo(0);
        make.top.mas_equalTo(0);

    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shopImage);
        make.left.equalTo(self.shopImage.mas_right).offset(12);
        make.right.mas_equalTo(-12);
    }];
    
    UIButton *moreBtn = [UIButton new];
    [moreBtn setImage:[UIImage imageNamed:@"more"] forState:0];
    [self addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18);
//        make.size.mas_equalTo(CGSizeMake(22, 22));
        make.centerY.mas_equalTo(self);
    }];
    self.arrBtn = moreBtn;
}

- (void)setupData:(NSDictionary *)dic{
    NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"logo"]];
    [self.shopImage yy_setImageWithURL:URL(url) options:0];
    
    self.titleLabel.text = dic[@"shopName"];
    NSArray *data = dic[@"service"];
    self.dataArr = [NSMutableArray array];
//    NSDictionary *defaultDic = @{@"serviceName":@"商城"};
//    [self.dataArr addObject:defaultDic];
    for (int i = 0; i<data.count; i++) {
        NSDictionary *dic = data[i];
        [self.dataArr addObject:dic];
    }
    UIView *lastView;
    for (int i = 0; i < self.dataArr.count; i++) {
        NSDictionary *dic = self.dataArr[i];
        UIButton *button = [UIButton new];
        button.backgroundColor = UIColorF0F5;
        [button setTitle:dic[@"serviceName"] forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont12;
        [button setTitleColor:APPColor forState:0];
        button.layer.cornerRadius = 3;
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(34, 19));
            make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
            if(i == 0){
                make.left.equalTo(self.titleLabel);
            }else{
                make.left.equalTo(lastView.mas_right).offset(10);
            }
        }];
        lastView = button;
    }
}

-(UIImageView *)shopImage
{
    if(!_shopImage)
    {
        _shopImage=[UIImageView  new];
//        _shopImage.image = [UIImage imageNamed:@"head"];
//        _shopImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _shopImage;
}

-(UILabel *)titleLabel
{
    if(!_titleLabel)
    {
        _titleLabel=[UILabel  new];
        _titleLabel.text = @"店铺名";
        _titleLabel.font = LabelFont18;
        _titleLabel.textColor = UIColor333;
        _titleLabel.numberOfLines = 1;
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

@end
