//
//  ShopInfoBottomView.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopInfoBottomView.h"

@implementation ShopInfoBottomView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor redColor];
        ShopInfoBottomView *lastView = [[[NSBundle mainBundle] loadNibNamed:@"ShopInfoBottomView" owner:self options:nil] lastObject];
        
        [self addSubview:lastView];
        self = lastView;
//////        self.submitBlock()
//        [lastView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.top.equalTo(self);
//            make.height.equalTo(@50);
//        }];
    }
    return self;
}


- (IBAction)helpAct:(UIButton *)sender {
    if (self.commonBlock) {
        self.commonBlock(2);
    }
}

- (IBAction)connactAct:(UIButton *)sender {
    if (self.commonBlock) {
        self.commonBlock(3);
    }
}
@end
