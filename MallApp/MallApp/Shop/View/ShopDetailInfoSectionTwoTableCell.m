//
//  ShopDetailInfoSectionTwoTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/16.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopDetailInfoSectionTwoTableCell.h"
@interface ShopDetailInfoSectionTwoTableCell()
@property (nonatomic, strong) UILabel *connactLbl;
@property (nonatomic, strong) UILabel *telLbl;
@property (nonatomic, strong) UILabel *phoneLbl;
@property (nonatomic, strong) UILabel *emailLbl;
@property (nonatomic, strong) UILabel *companyNameLbl;
@property (nonatomic, strong) UILabel *shopNameLbl;
@property (nonatomic, strong) UILabel *qualificationLbl;
@end

@implementation ShopDetailInfoSectionTwoTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    UIView *view = [UIView new];
    view.backgroundColor = UIColorF5F7;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(10);
    }];
    
    UILabel *title = [UILabel new];
    title.text = @"基础信息";
    title.textColor = UIColor333;
    title.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [self.contentView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(view.mas_bottom).offset(12);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title2 = [UILabel new];
    title2.text = @"联系人";
    title2.textColor = UIColor66;
    title2.font = LabelFont14;
    [self.contentView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.connactLbl];
    [self.connactLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(title2);
    }];
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = UIColorEF;
    [self.contentView addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title2.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title3 = [UILabel new];
    title3.text = @"联系号码";
    title3.textColor = UIColor66;
    title3.font = LabelFont14;
    [self.contentView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line2.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.telLbl];
    [self.telLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(title3);
    }];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = UIColorEF;
    [self.contentView addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title3.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    
    UILabel *title4 = [UILabel new];
    title4.text = @"电话号码";
    title4.textColor = UIColor66;
    title4.font = LabelFont14;
    [self.contentView addSubview:title4];
    [title4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line3.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.phoneLbl];
    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(title4);
    }];
    UIView *line4 = [UIView new];
    line4.backgroundColor = UIColorEF;
    [self.contentView addSubview:line4];
    [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title4.mas_bottom).offset(7);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    UILabel *title5 = [UILabel new];
    title5.text = @"邮箱";
    title5.textColor = UIColor66;
    title5.font = LabelFont14;
    [self.contentView addSubview:title5];
    [title5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line4.mas_bottom).offset(12);
    
    }];
    
    [self.contentView addSubview:self.emailLbl];
    [self.emailLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(title5);
    }];
    
    UIView *line5 = [UIView new];
    line5.backgroundColor = UIColorEF;
    [self.contentView addSubview:line5];
    [line5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title5.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
//        make.bottom.mas_equalTo(self.contentView).offset(-12);
    }];
    
    UILabel *title6 = [UILabel new];
    title6.text = @"公司名称";
    title6.textColor = UIColor66;
    title6.font = LabelFont14;
    [self.contentView addSubview:title6];
    [title6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line5.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.companyNameLbl];
    [self.companyNameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(title6);
    }];
    
    UIView *line6 = [UIView new];
    line6.backgroundColor = UIColorEF;
    [self.contentView addSubview:line6];
    [line6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title6.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
        
    }];
    
    UILabel *title7 = [UILabel new];
    title7.text = @"店铺描述";
    title7.textColor = UIColor66;
    title7.font = LabelFont14;
    [self.contentView addSubview:title7];
    [title7 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line6.mas_bottom).offset(12);
    }];
    
    [self.contentView addSubview:self.shopNameLbl];
    [self.shopNameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(title7);
        make.width.mas_equalTo(Screen_Width - 100);
    }];
    
    UIView *line7 = [UIView new];
    line7.backgroundColor = UIColorEF;
    [self.contentView addSubview:line7];
    [line7 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title7.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
        
    }];
    
    UILabel *title8 = [UILabel new];
    title8.text = @"企业资质";
    title8.textColor = UIColor66;
    title8.font = LabelFont14;
    [self.contentView addSubview:title8];
    [title8 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(line7.mas_bottom).offset(12);
    }];
    
    UIImageView *arrImage = [UIImageView new];
    [arrImage setImage:[UIImage imageNamed:@"more"]];
    [self.contentView addSubview:arrImage];
   
    [arrImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).offset(-12);
        make.centerY.mas_equalTo(title8);
    }];
    
    UIImageView *qImage = [UIImageView new];
    [qImage setImage:[UIImage imageNamed:@"R_qulicationIcon"]];
    [self.contentView addSubview:qImage];
    qImage.userInteractionEnabled = true;
       [qImage addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAct)]];
    [qImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(arrImage.mas_left).offset(-6);
        make.centerY.mas_equalTo(title8);
    }];
    
    UIView *line8 = [UIView new];
    line8.backgroundColor = UIColorEF;
    [self.contentView addSubview:line8];
    [line8 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(title8.mas_bottom).offset(12);
        make.height.mas_equalTo(lineHeihgt);
        make.bottom.mas_equalTo(self.contentView).offset(0);
    }];
}

- (void)clickAct{
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.connactLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"contact"] andDefailtInfo:@""];
        self.telLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"phone"] andDefailtInfo:@""];
        self.phoneLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"tel"] andDefailtInfo:@""];
        self.emailLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"email"] andDefailtInfo:@""];
        self.shopNameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopMs"] andDefailtInfo:@""];
        self.companyNameLbl.text = [XJUtil insertStringWithNotNullObject:dic[@"shopName"] andDefailtInfo:@""];
    }
}

- (UILabel *)connactLbl{
    if (!_connactLbl) {
        _connactLbl = [UILabel new];
        _connactLbl.textColor = UIColor66;
        _connactLbl.font = LabelFont14;
        _connactLbl.text = @"111";
    }
    
    return _connactLbl;
}


- (UILabel *)telLbl{
    if (!_telLbl) {
        _telLbl = [UILabel new];
        _telLbl.textColor = UIColor66;
        _telLbl.font = LabelFont14;
        _telLbl.text = @"136";
    }
    
    return _telLbl;
}

- (UILabel *)phoneLbl{
    if (!_phoneLbl) {
        _phoneLbl = [UILabel new];
        _phoneLbl.textColor = UIColor66;
        _phoneLbl.font = LabelFont14;
        _phoneLbl.text = @"";
    }
    
    return _phoneLbl;
}

- (UILabel *)emailLbl{
    if (!_emailLbl) {
        _emailLbl = [UILabel new];
        _emailLbl.textColor = UIColor66;
        _emailLbl.font = LabelFont14;
        _emailLbl.text = @"";
    }
    
    return _emailLbl;
}

- (UILabel *)companyNameLbl{
    if (!_companyNameLbl) {
        _companyNameLbl = [UILabel new];
        _companyNameLbl.textColor = UIColor66;
        _companyNameLbl.font = LabelFont14;
        _companyNameLbl.text = @"";
    }
    
    return _companyNameLbl;
}

- (UILabel *)shopNameLbl{
    if (!_shopNameLbl) {
        _shopNameLbl = [UILabel new];
        _shopNameLbl.textColor = UIColor66;
        _shopNameLbl.font = LabelFont14;
        _shopNameLbl.text = @"";
        _shopNameLbl.textAlignment = NSTextAlignmentRight;
    }
    
    return _shopNameLbl;
}
@end
