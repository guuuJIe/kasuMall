//
//  RepairBannerCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "RepairBannerCell.h"
#import "SDCycleScrollView.h"
@interface RepairBannerCell()
@property (nonatomic) SDCycleScrollView *focusHeadView;
@end

@implementation RepairBannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    self.contentView.backgroundColor = UIColorF5F7;
    
    [self.focusHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(12);
        make.bottom.mas_equalTo(-12);
    }];
    
}


-(SDCycleScrollView *)focusHeadView{
    if (!_focusHeadView) {
        _focusHeadView = [SDCycleScrollView new];
        _focusHeadView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
//        _focusHeadView.currentPageDotColor = JSCGlobalThemeColor;
        _focusHeadView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
        _focusHeadView.layer.cornerRadius = 8;
        _focusHeadView.mainView.layer.cornerRadius = 8;
//        _focusHeadView.placeholderImage = PlaceholderImageWithSquare;
        
        _focusHeadView.tag = 200;
        [_focusHeadView setLocalizationImageNamesGroup:@[@"R_banner"]];
//        _focusHeadView.delegate = self;
//        _focusHeadView.delegate = self;
#ifdef DEBUG
        
//                [_focusHeadView setImageURLStringsGroup:@[@"http://cdnimg.jsojs.com/goods_item1/300085/icon.jpg@!414",@"http://cdnimg.jsojs.com/goods_item1/300085/icon06.jpg@!414"]];
        //#else
        
#endif
        
        [self.contentView addSubview:_focusHeadView];
    }
    return _focusHeadView;
}

@end
