//
//  ShopBannerTableCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopBannerTableCell.h"
@interface ShopBannerTableCell()
@property (nonatomic, strong) UIImageView *bgImage;
@property (nonatomic, strong) UILabel *titleLbl;
@end

@implementation ShopBannerTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    UIImageView *bgimage = [UIImageView new];
    bgimage.image = [UIImage imageNamed:@"bghead"];
    [self.contentView addSubview:bgimage];
    [bgimage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
        make.height.mas_equalTo(240*AdapterScal);
    }];
    self.bgImage = bgimage;
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.mas_equalTo(self.contentView);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(167);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"file1"]];
        [self.bgImage yy_setImageWithURL:URL(url) placeholder:[UIImage imageNamed:@"bghead"]];
        NSArray *datas = dic[@"service"];
        NSDictionary *dic = datas.firstObject;
        self.titleLbl.text = [NSString stringWithFormat:@"营业时间：%@",dic[@"openTime"]];
    }
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.text = @"营业时间：08:00-17:00";
        _titleLbl.textColor = UIColor.whiteColor;
        _titleLbl.font = LabelFont14;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        [_titleLbl setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.3]];
        _titleLbl.hidden = true;
        [self.contentView addSubview:_titleLbl];
    }
    
    return _titleLbl;
}

@end
