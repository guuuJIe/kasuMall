//
//  ShopSelectTopView.m
//  MallApp
//
//  Created by Mac on 2020/1/7.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopSelectTopView.h"

@implementation ShopSelectTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.userInteractionEnabled = YES;
        [self setupLayout];
    }
    return self;
}
- (void)setupLayout
{
    UIView *lastView;
    NSArray *arr = @[@"选择省",@"选择市",@"选择区"];
    for (int i = 0; i<arr.count; i++) {
        UIButton *button = [UIButton new];
       
        [button setTitle:arr[i] forState:0];
        [button setTitleColor:UIColor333 forState:0];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [button setTitleColor:APPColor forState:UIControlStateSelected];
        button.tag = i+200;
        [self addSubview:button];
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.equalTo(self).offset(1);
            }else{
                make.left.equalTo(lastView.mas_right);
            }
            make.top.equalTo(self).offset(0);
            make.width.mas_equalTo(Screen_Width/3);
            make.bottom.equalTo(self).offset(0*AdapterScal);
        }];
        lastView = button;
        
        if (i == 0) {
            self.proviceBtn = button;
        }else if (i == 1){
            self.cityBtn = button;
        }else{
            self.areaBtn = button;
        }
    }
    
}

- (void)click:(UIButton *)btn
{
//    if (btn!= self.selectedBtn) {
//        self.selectedBtn.selected = NO;
//        btn.selected = YES;
//        self.selectedBtn = btn;
//    }else{
//        self.selectedBtn.selected = YES;
//    }
    if (self.clickBlock) {
        self.clickBlock(btn.tag);
    }
}
@end
