//
//  HelpOrderOperationView.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpOrderOperationView.h"

@implementation HelpOrderOperationView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.priceLabel.hidden = true;
        [self addSubview:self.priceLabel];
        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(20);
            make.centerY.mas_equalTo(self);
        }];
        
    }
    return self;
}

- (NSDictionary *)createPurchasingButton:(NSDictionary *)data
{
    NSDictionary *dic;
    
    
    NSInteger type = [data[@"status"] integerValue];
    if (type == 2) {
       
        dic = @{
            @"title":@"确认",
            @"flag":@"orderConfirm"
        };
        
        [self.btnModelArray addObject:dic];
        dic = @{
            @"title":@"换一家",
            @"flag":@"orderChange"
        };
        [self.btnModelArray addObject:dic];
    }
    
    if (type == 1) {
        dic = @{
            @"title":@"取消订单",
            @"flag":@"orderCancel"
        };
        [self.btnModelArray addObject:dic];
        
        
    }
    
    if (type == 3) {
//        dic = @{
//            @"title":@"取消订单",
//            @"flag":@"orderCancel"
//        };
//        [self.btnModelArray addObject:dic];
        
        
    }
    if (type == 4) {
        dic = @{
            @"title":@"去付款",
            @"flag":@"orderPay"
        };
        [self.btnModelArray addObject:dic];
        
        
    }
    
    if (type == 5) {
//          dic = @{
//              @"title":@"去付款",
//              @"flag":@"orderPay"
//          };
//          [self.btnModelArray addObject:dic];
          
          
      }
    return dic;
}


- (NSDictionary *)createOpButton:(NSDictionary *)data
{
    NSDictionary *dic;
    NSInteger type = [data[@"status"] integerValue];
    if (type == 2) {
        dic = @{
            @"title":@"付款",
            @"flag":@"orderPay"
            
        };
        [self.btnModelArray addObject:dic];
        
    }
    if (type == 21) {
//        dic =
//        @{@"title":@"取消订单",
//          @"flag":@"orderCancel"
//        };
//        [self.btnModelArray addObject:dic];
        }
    if (type == 22) {
        dic = @{
            @"title":@"联系对方",
            @"flag":@"connact"
            
        };
        [self.btnModelArray addObject:dic];
        dic = @{
            @"title":@"取消订单",
            @"flag":@"orderCancel"
            
        };
        [self.btnModelArray addObject:dic];
    }
    
    return dic;
    
}
- (NSDictionary *)createSellerRepairOrderOpButton:(NSDictionary *)data
{
    NSDictionary *dic;
    NSInteger type = [data[@"status"] integerValue];
   
    if (type == 24) {
        dic = @{@"title":@"确认取消",
                @"flag":@"orderCancel"};
        [self.btnModelArray addObject:dic];
    }
    if (type == 22) {
        dic = @{@"title":@"确认完成",
                @"flag":@"confirm"
                
        };
        [self.btnModelArray addObject:dic];
        dic = @{@"title":@"确认取消",
                @"flag":@"orderCancel"};
        [self.btnModelArray addObject:dic];
        
    }
    
    return dic;
    
}


- (void)setOrderOptionWithDic:(NSDictionary *)dic withType:(OperationType)opType
{
    
    NSInteger type = [dic[@"status"] integerValue];
    
    _btnModelArray = [NSMutableArray new];
    NSDictionary *contentDic;
    if (opType == UserEmergencyOrderOpe) {//抢修施救
        contentDic = [self createOpButton:dic];
        
    }else if (opType == UserFixOrderOpe){//维修
        contentDic = [self createPurchasingButton:dic];
    }else if (opType == SellerEmergencyOrderOpe){
        contentDic = [self createSellerRepairOrderOpButton:dic];
    }
    
    
    UIView *lastView;
    
    
    for (UIView *view in self.subviews) {
        if ([view isMemberOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < self.btnModelArray.count; i++) {
        NSDictionary *dic = self.btnModelArray[i];
        UIButton *button = [UIButton new];
        NSString *title = [NSString stringWithFormat:@"%@",dic[@"title"]];
        button.backgroundColor = [UIColor whiteColor];
        if (![title isEqualToString:@"取消订单"]) {
            
            [button setTitleColor:APPColor forState:UIControlStateNormal];
            button.layer.borderColor = APPColor.CGColor;
            
        }else{
            [button setTitleColor:UIColorB6 forState:UIControlStateNormal];
            button.layer.borderColor = UIColorB6.CGColor;
        }
        [button setTitle:title forState:UIControlStateNormal];
        button.titleLabel.font = LabelFont14;
        button.layer.borderWidth = 1;
        button.layer.cornerRadius = 15;
        [button setTitle:dic[@"flag"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(160/2, 60/2));
            make.top.equalTo(self).offset(10);
            if(i == 0){
                make.right.equalTo(self).offset(-12);
            }else{
                make.right.equalTo(lastView.mas_left).offset(-15);
            }
        }];
        lastView = button;
    }
    if (type == 2) {
        self.priceLabel.text = [NSString stringWithFormat:@"¥%@",dic[@"realPrice"]];
        self.priceLabel.hidden = false;
    }else{
        self.priceLabel.hidden = true;
    }
}

-(void)buttonClick:(UIButton *)button{
    self.orderHandleBlock([button titleForState:UIControlStateSelected]);
}


-(UILabel *)priceLabel
{
    if(!_priceLabel)
    {
        _priceLabel=[UILabel  new];
        _priceLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
        _priceLabel.textColor = [UIColor colorWithHexString:@"FF3E3E"];
        _priceLabel.numberOfLines = 1;
        _priceLabel.text = @"￥0.00";

    }
    return _priceLabel;
}
@end
