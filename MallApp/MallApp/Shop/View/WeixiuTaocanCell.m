//
//  WeixiuTaocanCell.m
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WeixiuTaocanCell.h"

@implementation WeixiuTaocanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
        self.nameLbl.text = dic[@"title"];
        NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"productPic"]];
        [self.goodImage yy_setImageWithURL:URL(url) options:0];
        self.priceLbl.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
        self.shopnameLbl.text = dic[@"shopName"];
    }
}
- (IBAction)jumpToShop:(id)sender {
    if (self.itemClickBlock) {
        self.itemClickBlock();
    }
}

@end
