//
//  HelpDetailAboutKuaiXiuProgressCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpDetailAboutKuaiXiuProgressCell.h"
#import "XFStepView.h"

@interface HelpDetailAboutKuaiXiuProgressCell()

@property (strong, nonatomic)XFStepView *stepView;
@end

@implementation HelpDetailAboutKuaiXiuProgressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    _stepView = [[XFStepView alloc]initWithFrame:CGRectMake(0, 24, Screen_Width, 60) Titles:[NSArray arrayWithObjects:@"待付款", @"派单中", @"抢修中", @"已完成", nil]];
    self.stepView.stepIndex = 0;
    [self.contentView addSubview:self.stepView];
    
    UIView *view = [UIView new];
    view.backgroundColor = bgColor;
    [self.contentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.stepView.mas_bottom).offset(15);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(10);
    }];
}

- (void)setupData:(NSDictionary *)dic{
    NSInteger value = [dic[@"status"] integerValue];
    
    if (value == 2) {//待支付
        self.stepView.stepIndex = 0;
    }else if (value == 21){//派单中
        self.stepView.stepIndex = 1;
    }else if (value == 22){//服务中
        self.stepView.stepIndex = 2;
    }else if (value == 6 || value == 23){//已完成
        self.stepView.stepIndex = 3;
    }else if (value == ApplyCancel){
        self.stepView.titles = @[@"待付款", @"派单中", @"取消中", @"已完成"];
        self.stepView.stepIndex = 2;
    }else if (value == TradeCancel){
        self.stepView.titles = @[@"待付款", @"派单中", @"已取消", @"已完成"];
        self.stepView.stepIndex = 3;
    }
//    self.stepView.stepIndex = value - 1;
}
@end
