//
//  PicCollectionCell.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/19.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PicCollectionCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *picImageView;
@end

NS_ASSUME_NONNULL_END
