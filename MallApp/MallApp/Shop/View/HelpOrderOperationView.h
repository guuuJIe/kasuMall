//
//  HelpOrderOperationView.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpOrderOperationView : UIView
@property (nonatomic , strong) NSMutableArray *btnModelArray;
@property (nonatomic , copy) void(^orderHandleBlock)(NSString *flag);
@property (nonatomic , strong) UILabel *priceLabel;
- (void)setOrderOptionWithDic:(NSDictionary *)dic withType:(OperationType)opType;
@end

NS_ASSUME_NONNULL_END
