//
//  ShopIntroView.h
//  MallApp
//
//  Created by Mac on 2020/3/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopIntroView : UIView
@property (nonatomic, strong) UIButton *arrBtn;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
