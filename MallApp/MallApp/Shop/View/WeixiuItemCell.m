//
//  WeixiuItemCell.m
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WeixiuItemCell.h"

@implementation WeixiuItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setupData:(NSDictionary *)dic{
    if (dic) {
         NSString *url = [NSString stringWithFormat:@"%@%@",URLAddress,dic[@"productPic"]];
        [self.imgaeview yy_setImageWithURL:URL(url) options:0];
        self.title.text = dic[@"title"];
        self.price.text = [NSString stringWithFormat:@"¥%@",dic[@"price"]];
    }
}

@end
