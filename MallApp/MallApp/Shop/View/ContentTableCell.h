//
//  ContentTableCell.h
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSScrollContentView.h"
#import "ShopHomeVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface ContentTableCell : UITableViewCell
@property (nonatomic, strong) FSPageContentView *pageContentView;
@property (nonatomic, strong) NSMutableArray<UIViewController *> *viewControllers;
@property (nonatomic, assign) BOOL cellCanScroll;
@property (nonatomic, assign) BOOL isRefresh;

@property (nonatomic, strong) NSString *currentTagStr;
@end

NS_ASSUME_NONNULL_END
