//
//  FixOrderListImageCollectionCell.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FixOrderListImageCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *dataImageView;

@end

NS_ASSUME_NONNULL_END
