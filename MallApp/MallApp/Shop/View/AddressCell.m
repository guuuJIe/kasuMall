//
//  AddressCell.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setData:(AMapPOI *)poi{
    
    self.buildLbl.text = poi.name;
    self.addressLbl.text = poi.address;
    
}

@end
