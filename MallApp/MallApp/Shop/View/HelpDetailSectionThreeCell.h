//
//  HelpDetailSectionThreeCell.h
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpDetailSectionThreeCell : UITableViewCell
- (void)setupData:(NSDictionary *)dic;
- (void)setupListData:(NSDictionary *)dic;//列表跳详情
@end

NS_ASSUME_NONNULL_END
