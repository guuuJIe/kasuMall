//
//  DropDownListView.m
//  MallApp
//
//  Created by Mac on 2020/1/11.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "DropDownListView.h"
#import "DropDownViewCell.h"
@interface DropDownListView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic , strong) UIView *bgView;
@property (nonatomic , strong) UITableView *tableView;

@end

@implementation DropDownListView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    [self addSubview:self.bgView];
    [self addSubview:self.tableView];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.bgView);
        }];
    
    [self.bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidenSelView)]];
}

-(void)showSelView{
    
    
    self.hidden = NO;
   
    WeakSelf(self);
    [UIView animateWithDuration:0.2 animations:^{
        StrongSelf(self);
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(240);
        }];

    } completion:^(BOOL finished) {
        
        
    }];
    
}

-(void)hidenSelView{
    
    
    self.hidden = YES;
    
    if (self.bgViewHideBlock) {
        self.bgViewHideBlock();
    }
    WeakSelf(self);
    [UIView animateWithDuration:0.2 animations:^{
        StrongSelf(self);
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {

        
    }];
    
}

- (void)setDatasource:(NSMutableArray *)datasource{
//     = datasource;
    _datasource = datasource;
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *rid=@"DropDownViewCell";
    
    DropDownViewCell *cell=[tableView dequeueReusableCellWithIdentifier:rid];
    
    if(cell==nil)
    {
        cell=[[DropDownViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:rid];
    }
    NSDictionary *dic = self.datasource[indexPath.row];
    
    cell.nameLbl.text = [NSString stringWithFormat:@"%@",dic[@"name"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.clickBlock) {
        self.clickBlock(indexPath.row);
    }
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor colorWithRed:.255 green:.255 blue:.255 alpha:.2];
        _bgView.userInteractionEnabled = true;
    }
    return _bgView;
}

//- (NSMutableArray *)datasource{
//    if (!_datasource) {
//        _datasource = [NSMutableArray arrayWithCapacity:0];
//    }
//    return _datasource;
//}
-(UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"DropDownViewCell" bundle:nil] forCellReuseIdentifier:@"DropDownViewCell"];
        _tableView.showsVerticalScrollIndicator = NO;
       
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 44;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}
@end
