//
//  ShopInfoSectionCell.m
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopInfoSectionCell.h"
#import "WeixiuItemCell.h"
#import "NoDataView.h"
@interface ShopInfoSectionCell()<DZNEmptyDataSetSource,UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation ShopInfoSectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
//    _collectionView = [[UICollectionView alloc] in]
//    [_collectionView re:[WeixiuItemCell clasrs] forCellWithReuseIdentifier:@"WeixiuItemCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"WeixiuItemCell" bundle:nil] forCellWithReuseIdentifier:@"WeixiuItemCell"];
    
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView setEmptyDataSetSource:self];
    _checkMoreBtn.layer.cornerRadius = 14;
    _checkMoreBtn.layer.borderWidth = 1;
    _checkMoreBtn.layer.borderColor = UIColorBF.CGColor;
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)checkMore:(UIButton *)sender {
    if (self.checkMoreBlock) {
        self.checkMoreBlock();
    }
}

-(void)setDataArr:(NSArray *)dataArr{
    if (dataArr) {
        _dataArr = dataArr;
        [self.collectionView reloadData];
       
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         [self.collectionView reloadEmptyDataSet];
    });
   
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WeixiuItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WeixiuItemCell" forIndexPath:indexPath];
//    cell.model = self.old_wine[indexPath.row];
//    cell.backgroundColor = RandomColor;
//    [cell setupData:self.dataArr[indexPath.row]];
    [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(Screen_Width/3 - 13, 170);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.itemClickBlock) {
        self.itemClickBlock(self.dataArr[indexPath.item]);
    }
}

#pragma mark --EmptyDataSource--
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NoDataView *view = [NoDataView initView];
    view.frame = CGRectMake((Screen_Width-13)/2 - 30, 40, 60, 60);
//    view.backgroundColor = [UIColor redColor];
//    [scrollView addSubview:view];
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.center.mas_equalTo(scrollView);
//
////        make.size.mas_equalTo(CGSizeMake(50, 50));
//    }];
//    view.backgroundColor = [UIColor redColor];
    return view;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{

//    [self.collectionView reloadNetworkDataSet];
//    [self getHomeData];
}


@end
