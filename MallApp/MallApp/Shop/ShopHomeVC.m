//
//  ShopHomeVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//
/**
 * 随机数据
 */
#define RandomData [NSString stringWithFormat:@"随机数据---%d", arc4random_uniform(1000000)]
#import "ShopHomeVC.h"
#import "ShopInfoSectionCell.h"
@interface ShopHomeVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic) NSArray *serviceArr;
@property (nonatomic) NSMutableArray *cateArr;
@end

@implementation ShopHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupSubViews];
    self.view.backgroundColor = UIColorF5F7;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.view.backgroundColor = UIColorF5F7;
}


- (void)setupSubViews
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - 170*AdapterScal) style:UITableViewStyleGrouped];//reduce headviews height。bottom height was reduced outside
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 40;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.showsVerticalScrollIndicator = false;
//    _tableView.tableFooterView = [UIView new];
//    _tableView.bounces = NO;
    _tableView.backgroundColor = UIColorF5F7;
    [_tableView registerNib:[UINib nibWithNibName:@"ShopInfoSectionCell" bundle:nil] forCellReuseIdentifier:@"ShopInfoSectionCell"];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
//    [self insertRowAtTop];
}




- (void)setDataDic:(NSDictionary *)dataDic{
   
    if (dataDic) {
        self.cateArr = [NSMutableArray array];
        _dataDic = dataDic;
        self.serviceArr = dataDic[@"service"];
        [self.cateArr addObject:dataDic[@"productList"]];
        [self.cateArr addObject:dataDic[@"repairList"]];
        [self.tableView reloadData];
    }
    
}

#pragma mark UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.serviceArr.count;
//    return 200;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"%lu",(unsigned long)self.data.count);
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    view.backgroundColor = UIColorF5F7;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
    view.backgroundColor = UIColorEF;
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopInfoSectionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShopInfoSectionCell"];
    if (!cell) {
        cell = [[ShopInfoSectionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSDictionary *dic = self.serviceArr[indexPath.section];
    NSString *title = [NSString stringWithFormat:@"%@",dic[@"serviceName"]];
    if ([title isEqualToString:@"商城"]) {
        cell.typeTitleImage.image = [UIImage imageNamed:@"R_services"];
    }else if ([title isEqualToString:@"维修"]){
        cell.typeTitleImage.image = [UIImage imageNamed:@"R_fix"];
    }
//    cell.HeadLbl.text = [NSString stringWithFormat:@"%@",dic[@"serviceName"]];
//    cell.nameLbl.text = [NSString stringWithFormat:@"%@",dic[@"address"]];
    cell.dataArr = self.cateArr[indexPath.section];
    cell.itemClickBlock = ^(NSDictionary * _Nonnull dic) {
        if (self.itemclickBlock) {
            self.itemclickBlock(dic);
        }
    };
    cell.checkMoreBlock = ^{
        if (self.checkMoreBlock) {
            self.checkMoreBlock();
        }
    };

    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (!self.vcCanScroll) {

    }
    JLog(@"子视图cell的偏移量%f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y <= 0) {

        scrollView.contentOffset = CGPointMake(0, 0);
       [[NSNotificationCenter defaultCenter] postNotificationName:@"leaveTop" object:nil];//到顶通知父视图改变状态
    }else{
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTop" object:nil userInfo:@{@"yValue":@(scrollView.contentOffset.y)}];
    }

    self.tableView.showsVerticalScrollIndicator = _vcCanScroll?YES:NO;
}


@end
