//
//  ShopOtherContentVC.h
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopOtherContentVC : UIViewController
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,strong) NSString *shopsId;
@end

NS_ASSUME_NONNULL_END
