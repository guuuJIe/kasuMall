//
//  GoodsWithSpecificVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/4/8.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "GoodsWithSpecificVC.h"

#import "InfoOneCell.h"
#import "InfoTwoCell.h"
#import "InfoThreeCell.h"
#import "InfoFourCell.h"
#import "OperationView.h"
#import "navView.h"
#import "GoodsTypeWithSpecificalView.h"
#import "GoodsInfoApi.h"
#import "MemberApi.h"
#import "SubmitOrderVC.h"
#import "LoginVC.h"
#import "UIScrollView+EmptyDataSet.h"
#import "NetWorkBadView.h"
#import "GoodsTypeWithSpecificalView.h"
@interface GoodsWithSpecificVC ()<UITableViewDelegate,UITableViewDataSource,WKNavigationDelegate,WKUIDelegate,DZNEmptyDataSetSource>
@property (nonatomic) UITableView *listTableView;
@property (nonatomic,assign) CGFloat webViewCellHeight;
@property (nonatomic,strong)WKWebView * wkwebView;
@property (nonatomic) OperationView *OpView;
@property (nonatomic) navView *navbar;
@property (nonatomic) GoodsTypeWithSpecificalView *typeView;

@property (nonatomic) GoodsInfoApi *goodApi;
@property (nonatomic) MemberApi *memberApi;

@property (nonatomic) NSDictionary *totalData;
@property (nonatomic) NSMutableArray *arguData;
@property (nonatomic) NSMutableArray *bannerData;

@property (nonatomic) NSString *purchaseId;//立即购买ID
@property (nonatomic) NSString *addressId;//地址ID
@property (nonatomic) NSString *model;//立即购买ID
@property (nonatomic,assign) NSInteger num;//数量
@property (nonatomic,strong) NSMutableString *htmStr;
@property (nonatomic,assign) BOOL isFirstLoad;//只加载一次
@property (nonatomic,assign) NSInteger rows;
@end

@implementation GoodsWithSpecificVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self.view addSubview:self.wkwebView];
//    [self initWkwebView];
   
    [self setupUI];
    
    
    
    [self setupData];
}


- (void)setupUI{
    [self.view addSubview:self.listTableView];
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(-(50+BottomAreaHeight));
    }];
    AdjustsScrollViewInsetNever(self, self.listTableView);
//
    [self.view addSubview:self.OpView];
    [self.OpView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];

    [self.view addSubview:self.navbar];
    [self.navbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(44+StatusBarHeight);
    }];
    

}

- (void)dealloc{
    JLog(@"销毁了");
}

- (void)setupData{
    
//    NSLog(@"%@",self.goodsId);
    self.model = @"0";
    self.isFirstLoad = true;
    [JMBManager showLoading];
    self.rows = 0;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.goodsId forKey:@"id"];
    WeakSelf(self)
    [self.goodApi getGoodsItemWithparameters:self.goodsId withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSLog(@"%@",result.resultDic);
            NSArray *arr = result.resultDic[@"list"];
            self.totalData = arr.firstObject;
            self.rows = 4;
            [self configData:self.totalData];
            
//
            
        }
        [JMBManager hideAlert];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.listTableView reloadEmptyDataSet];
        });
    }];
    
}

- (void)configData:(NSDictionary *)dic{
    
    NSString *morePic = dic[@"morePic"];
    NSString *smallPic = dic[@"smallPic"];
    self.bannerData = [NSMutableArray array];
    if (morePic.length > 0) {
        NSArray *array = [morePic componentsSeparatedByString:@"|"];//|为分隔符
        
        
        for (int i = 0; i<array.count; i++) {
            [self.bannerData addObject:[NSString stringWithFormat:@"%@Uploadfiles/MorePic/%@",URLAddress,array[i]]];
        }
    }
    
    [self.bannerData addObject:[NSString stringWithFormat:@"%@%@",URLAddress,smallPic]];
     [self.bannerData addObject:[NSString stringWithFormat:@"%@%@",URLAddress,[XJUtil insertStringWithNotNullObject:dic[@"bigPic"] andDefailtInfo:@""]]];
    
    NSMutableString * carType = [NSMutableString new];
    if (![self.totalData[@"modelsList"] isEqual: [NSNull null]]) {
        NSArray *datas = [XJUtil insertStringWithNotNullObject:self.totalData[@"modelsList"] andDefailtInfo:@""];
        for (int i = 0; i<datas.count; i++) {
            NSDictionary *dic = datas[i];
            [carType appendString:[NSString stringWithFormat:@"%@\n",dic[@"name"]]];
        }
    }
    
    
    
    NSArray *arguArr = @[@{@"name":@"品牌",@"value":[XJUtil insertStringWithNotNullObject:self.totalData[@"strBrand"] andDefailtInfo:@"无"]},@{@"name":@"编号",@"value":[XJUtil insertStringWithNotNullObject:self.totalData[@"bh"] andDefailtInfo:@"无"]},@{@"name":@"质保",@"value":[XJUtil insertStringWithNotNullObject:self.totalData[@"pZB"] andDefailtInfo:@"无"]},@{@"name":@"原厂码",@"value":[XJUtil insertStringWithNotNullObject:self.totalData[@"ycm"] andDefailtInfo:@"无"]},@{@"name":@"适用车型",@"value":[XJUtil insertStringWithNotNullObject:carType andDefailtInfo:@"无"]}];
    [self.arguData addObjectsFromArray:arguArr];
    [self.typeView setupData:self.totalData];
    
    
    self.htmStr = [NSMutableString new];
    NSString *content = [XJUtil filterImage:[XJUtil insertStringWithNotNullObject:dic[@"content"] andDefailtInfo:@""]];
    NSString *parameter = [XJUtil filterImage:[XJUtil insertStringWithNotNullObject:dic[@"parameter"] andDefailtInfo:@""]];
    NSString *aftersale = [XJUtil filterImage:[XJUtil insertStringWithNotNullObject:dic[@"aftersale"] andDefailtInfo:@""]];
    NSString *packing = [XJUtil filterImage:[XJUtil insertStringWithNotNullObject:dic[@"packing"] andDefailtInfo:@""]];
    
    [self.htmStr appendString:content];
    [self.htmStr appendString:parameter];
    [self.htmStr appendString:aftersale];
    [self.htmStr appendString:packing];
    [self.listTableView reloadData];
    
    [self.listTableView reloadEmptyDataSet];
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        static NSString *Identifier = @"InfoOneCell";
        InfoOneCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InfoOneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
//        cell.dataArr = self.bannerData;
//        [cell setupData:self.totalData];
//        cell.dic = self.totalData;
        cell.dataArr = self.bannerData;
        return cell;
    }else if(indexPath.row == 1){
        static NSString *Identifier = @"InfoTwoCell";
        InfoTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InfoTwoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.totalData];
        return cell;
    }else if(indexPath.row == 2){
        static NSString *Identifier = @"InfoThreeCell";
        InfoThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InfoThreeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        [cell setupData:self.arguData];
        
        WeakSelf(self)
        cell.itemBlock = ^{
            StrongSelf(self)
            if (!accessToken) {
                
//                LoginVC *vc = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//                BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//                vc2.modalPresentationStyle = 0;
//                [self presentViewController:vc2 animated:true completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
                //普通买家 delafqm
                return ;
            }
            [self.typeView show];
        };
        return cell;
    }else if(indexPath.row == 3){
        static NSString *Identifier = @"InfoFourCell";
        InfoFourCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[InfoFourCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
//        cell.backgroundColor = [UIColor redColor];
        cell.wkwebView.navigationDelegate = self;//加载webview记得要开启网络
        cell.wkwebView.UIDelegate = self;
        if (self.htmStr) {
          [cell.wkwebView loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",self.htmStr] baseURL:nil];
        }
        
//        [cell setdata];
        return cell;
        
    }
    else{
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return Screen_Width;
    }else if (indexPath.row == 3){
        return self.webViewCellHeight;
    }else{
        return UITableViewAutomaticDimension;
    }
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NetWorkBadView *view = [NetWorkBadView ViewClass];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 50, CGRectGetHeight(self.view.frame)/2-50, 100, 100);
    
    return view;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{
    [self setupData];
}

#pragma mark - UIWebView Delegate Methods


- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    [webView evaluateJavaScript:@"document.body.offsetHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        // 计算webView高度
        if (!self.webViewCellHeight && self.isFirstLoad) {
            self.webViewCellHeight = [result doubleValue];
            // 刷新tableView
            self.isFirstLoad = !self.isFirstLoad;
            [UIView performWithoutAnimation:^{
                [self.listTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:3 inSection:0],nil] withRowAnimation:UITableViewRowAnimationNone];
            }];
//            [self.listTableView reloadData];
            [JMBManager hideAlert];
        }
        
    }];
    
    
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    [JMBManager hideAlert];
}


- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.emptyDataSetSource = self;
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 40;
        _listTableView.backgroundColor = [UIColor whiteColor];
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView registerClass:[InfoOneCell class] forCellReuseIdentifier:@"InfoOneCell"];
        
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_listTableView registerNib:[UINib nibWithNibName:@"InfoTwoCell" bundle:nil] forCellReuseIdentifier:@"InfoTwoCell"];
        [_listTableView registerNib:[UINib nibWithNibName:@"InfoThreeCell" bundle:nil] forCellReuseIdentifier:@"InfoThreeCell"];
        [_listTableView registerClass:[InfoFourCell class] forCellReuseIdentifier:@"InfoFourCell"];
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

- (void)addIncar{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.goodsId forKey:@"proid"];
    [dic setValue:self.totalData[@"price"] forKey:@"price"];
    [dic setValue:@1 forKey:@"minQuantity"];
    [dic setValue:@1 forKey:@"types"];
    [dic setValue:@(self.num) forKey:@"quantity"];
    [dic setValue:self.model forKey:@"xh"];
    [dic setValue:usersID forKey:@"userid"];
    [self.goodApi AddinCarWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSLog(@"%@",result.resultDic);
            NSDictionary *dic = result.resultDic;
            NSArray *arr = dic[@"list"];
            
            self.purchaseId = arr.firstObject;
            NSLog(@"%@",self.purchaseId);
            [self.typeView hide];
            [JMBManager showBriefAlert:result.message];
            [[NSNotificationCenter defaultCenter] postNotificationName:refreshShopCart object:nil];
        }
    }];
}
//立即购买
- (void)purchase{
   
    
    
   
}

- (OperationView *)OpView{
    if (!_OpView) {
        _OpView = [OperationView new];
        _OpView.addBlock = ^{
//          [self.navigationController pushViewController: animated:<#(BOOL)#>]
        };
        WeakSelf(self);
        _OpView.purchaseBlock = ^(NSInteger tag) {
            if (!accessToken) {
                //                [JMBManager showBriefAlert:@"未登入"];
//                LoginVC *vc = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
//                BaseNavigationController *vc2 = [[BaseNavigationController alloc] initWithRootViewController:vc];
//                vc2.modalPresentationStyle = 0;
//                [weakself presentViewController:vc2 animated:true completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
                //普通买家 delafqm
                return ;
            }
            
            [weakself.typeView show];
            //加入购物车
            
        };

    }
    return _OpView;
}

- (navView *)navbar{
    if (!_navbar) {
        _navbar = [navView new];
        
        WeakSelf(self);
        _navbar.clickBlock = ^{
            [weakself.navigationController popViewControllerAnimated:true];
        };
    }
    return _navbar;
}




#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 判断webView所在的cell是否可见，如果可见就layout
//    NSArray *cells = self.listTableView.visibleCells;
//    for (UITableViewCell *cell in cells) {
//        if ([cell isKindOfClass:[InfoFourCell class]]) {
//            InfoFourCell *webCell = (InfoFourCell *)cell;
//
//            [webCell.wkwebView setNeedsLayout];
//        }
//    }
    if (scrollView.contentOffset.y >64) {
        
        [UIView animateWithDuration:0.5 animations:^{
            self.navbar.backgroundColor = APPColor;
            [self.navbar.backButton setImage:[UIImage imageNamed:@"back"] forState:0];
        }];
        
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            self.navbar.backgroundColor = [UIColor clearColor];
            [self.navbar.backButton setImage:[UIImage imageNamed:@"返回"] forState:0];
        }];
    }
    
}


- (GoodsTypeWithSpecificalView *)typeView{
    if (!_typeView) {
        _typeView = [[GoodsTypeWithSpecificalView alloc] init];

         WeakSelf(self);
        _typeView.itemBlock = ^(NSString *title) {
           
            weakself.model = title;
//            NSLog(@"%ld",(long)index);
        };
        
        _typeView.purchaseBlock = ^(NSInteger tag,NSInteger num) {
            NSLog(@"%ld",(long)num);
            weakself.num = num;
            if (!accessToken) {
                [JMBManager showBriefAlert:@"请先登录"];
                return ;
            }
           //加入购物车
            if (tag == 101) {
                [weakself addIncar];
                
            }else{//立即购买
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                [dic setValue:weakself.goodsId forKey:@"proid"];
                [dic setValue:weakself.totalData[@"price"] forKey:@"price"];
                [dic setValue:@1 forKey:@"minQuantity"];
                [dic setValue:@1 forKey:@"types"];
                [dic setValue:@(weakself.num) forKey:@"quantity"];
                [dic setValue:weakself.model forKey:@"xh"];
//                [dic setValue:userId forKey:@"userid"];
                [weakself.goodApi AddinCarWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
//                        NSLog(@"%@",result.resultDic);
                        NSDictionary *dic = result.resultDic;
                        NSArray *arr = dic[@"list"];
                        
                        weakself.purchaseId = arr.firstObject;

                        SubmitOrderVC *vc = [SubmitOrderVC new];
                        vc.purchaseId = weakself.purchaseId;
                        vc.from = @"3";
                        vc.num = [NSString stringWithFormat:@"%ld",(long)weakself.num];
                        if (![weakself.typeView isHidden]) {
                            [weakself.typeView hide];
                        }
                        [weakself.navigationController pushViewController:vc animated:true];

                    }
                }];
                
            }
        };
    }
    return _typeView;
}

- (GoodsInfoApi *)goodApi{
    if (!_goodApi) {
        _goodApi = [GoodsInfoApi new];
    }
    return _goodApi;
}
- (MemberApi *)memberApi{
    if (!_memberApi) {
        _memberApi = [MemberApi new];
    }
    return _memberApi;
}

- (NSMutableArray *)arguData{
    if (!_arguData) {
        _arguData = [NSMutableArray array];
    }
    return _arguData;
}

@end
