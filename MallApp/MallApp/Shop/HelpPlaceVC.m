//
//  HelpPlaceVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpPlaceVC.h"
#import "AddressCell.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
@interface HelpPlaceVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AMapLocationManagerDelegate,AMapSearchDelegate>
@property (nonatomic,strong) UIButton * searchBtn;
@property (nonatomic,strong) UITextField * textField;
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) AMapSearchAPI *search;
@property (nonatomic,strong) CLLocation *location;
@property (nonatomic,strong) AMapLocationManager *locationManager;
@property (nonatomic,strong) NSMutableArray<AMapPOI *> *dataArray;
@end

@implementation HelpPlaceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    
    [self startLocation];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置navbar
//    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    [self initBar];
    
}
- (void)initBar{

    self.navigationItem.backBarButtonItem = nil;
    UIView *selectView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-80*AdapterScal, 30)];
    selectView.backgroundColor=[UIColor whiteColor];
    selectView.layer.cornerRadius=16;
    selectView.layer.masksToBounds=YES;
    
    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 5, selectView.frame.size.height-5*2, selectView.frame.size.height-5*2)];
    selectIcon.image=[UIImage imageNamed:@"搜索"];
    
    self.textField=[[UITextField alloc]initWithFrame:CGRectMake(selectIcon.frame.size.width+selectIcon.frame.origin.x+Default_Space, 0, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
         self.textField.textColor=UIColor333;
    self.textField.placeholder=@"输入关键字";
    self.textField.font=LabelFont14;
    self.textField.returnKeyType = UIReturnKeySearch;
    if (@available(iOS 13.0, *)) {
        
        self.textField.attributedPlaceholder=[[NSAttributedString alloc]initWithString:self.textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];
        
        
    }else{
        [self.textField setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
    }
    
    self.textField.delegate = self;
    [selectView addSubview:selectIcon];
    [selectView addSubview:self.textField];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:selectView];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStylePlain) target:self action:@selector(rightBtnAction)];
    // 字体颜色
    [rightBtn setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)setupUI{
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)rightBtnAction{
    [self.navigationController popViewControllerAnimated:true];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    // 发起周边搜索
   [self searchAroundwithType:textField.text];
    return YES;
}

- (void)startLocation
{
    if([CLLocationManager locationServicesEnabled]){
        AMapLocationManager *locationManager = [[AMapLocationManager alloc]init];
        locationManager.delegate = self;
        
        // 设定定位的最小更新距离。单位米，默认为 kCLDistanceFilterNone，表示只要检测到设备位置发生变化就会更新位置信息
        locationManager.distanceFilter = 5;
        
        // 设定期望的定位精度。单位米，默认为 kCLLocationAccuracyBest
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
        // 开始定位服务
        
        _locationManager = locationManager;
        
        
        [self.locationManager startUpdatingLocation];
    }
}

#pragma mark ---AMapLocationManagerDelegate---


- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location{
    NSLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
    self.location = location;

    // 停止定位
    [self.locationManager stopUpdatingLocation];
}

/** 根据定位坐标进行周边搜索 */
- (void)searchAroundwithType:(NSString *)type{
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;

    AMapPOIAroundSearchRequest *req = [[AMapPOIAroundSearchRequest alloc] init];
    req.keywords = type;
    req.sortrule = 1;
    req.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
    //发起周边搜索
    [self.search AMapPOIAroundSearch:req];
    
    [JMBManager showLoading];

    
}

// 实现POI搜索对应的回调函数
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response{
    NSLog(@"周边搜索回调");
    [JMBManager hideAlert];
    if(response.pois.count == 0)
    {
        return;
    }
    
    NSLog(@"%@",response.pois.firstObject);
    self.dataArray = [[NSMutableArray alloc] initWithArray:response.pois];
    [self.listTableView reloadData];
   
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"AddressCell";
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[AddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddressCell"];
    }
    [cell setData:self.dataArray[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.clickBlock) {
        self.clickBlock(self.dataArray[indexPath.row]);
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [UITableView new];
        _listTableView.tableFooterView = [UIView new];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;

        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 45.0f;
        _listTableView.showsVerticalScrollIndicator = false;
     
        [_listTableView registerNib:[UINib nibWithNibName:@"AddressCell" bundle:nil] forCellReuseIdentifier:@"AddressCell"];
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor = [UIColor whiteColor];

        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

//- (NSMutableArray *)dataArray{
//    if (!_dataArray) {
//        _dataArray = [NSMutableArray new];
//    }
//    return _dataArray;
//}

@end
