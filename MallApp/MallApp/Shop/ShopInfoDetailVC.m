//
//  ShopInfoDetailVC.m
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/5/15.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopInfoDetailVC.h"
#import "ShopDetailSectionOneTableCell.h"
#import "ShopDetailInfoSectionTwoTableCell.h"
#import "ShopDetailInfoSectionThreeTableCell.h"
#import "ShopPicShowTableCell.h"
#import "EnterPriseQualificationVC.h"
@interface ShopInfoDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) NSArray *serviceArr;
@property (nonatomic, strong) NSArray *picArr;
@end

@implementation ShopInfoDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

- (void)setupUI{
    self.title = @"店铺介绍";
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 3) {
        self.serviceArr = self.dic[@"service"];
        return self.serviceArr.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        static NSString *Identifier = @"ShopDetailSectionOneTableCell";
        ShopDetailSectionOneTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (!cell) {
            cell = [[ShopDetailSectionOneTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dic];
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }else if (indexPath.section == 1){
        static NSString *Identifier = @"ShopPicShowTableCell";
        ShopPicShowTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (!cell) {
            cell = [[ShopPicShowTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        NSString *shopPic = self.dic[@"file2"];
        if (shopPic.length >0) {
            cell.hidden = false;
             NSArray *datas = [shopPic componentsSeparatedByString:@","];
            cell.picArray = datas;
        }else{
             cell.hidden = true;
        }
       
//        if (datas.count>0) {
//            cell.hidden = false;
//        }else{
//            cell.hidden = true;
//        }
        
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"ShopDetailInfoSectionTwoTableCell";
        ShopDetailInfoSectionTwoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (!cell) {
            cell = [[ShopDetailInfoSectionTwoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        WeakSelf(self)
        cell.clickBlock = ^{
            StrongSelf(self)
            
            EnterPriseQualificationVC *vc = [EnterPriseQualificationVC new];
            vc.dataDic = self.dic;
            [self.navigationController pushViewController:vc animated:true];
        };
        [cell setupData:self.dic];
        return cell;
    }else{
        static NSString *Identifier = @"ShopDetailInfoSectionThreeTableCell";
        ShopDetailInfoSectionThreeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (!cell) {
            cell = [[ShopDetailInfoSectionThreeTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.serviceArr[indexPath.row]];
       
        return cell;
    }
    
    return nil;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        NSString *shopPic = self.dic[@"file2"];
        if (shopPic.length > 0) {
//            NSArray *datas = [shopPic componentsSeparatedByString:@","];
            return UITableViewAutomaticDimension;
        }else{
            return CGFLOAT_MIN;
        }
        
       
    }
    
    return UITableViewAutomaticDimension;
}


- (UITableView *)listTableView{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectZero style:0];
        [_listTableView setDelegate:self];
        [_listTableView setDataSource:self];
        [_listTableView setTableFooterView:[UIView new]];
        _listTableView.rowHeight = UITableViewAutomaticDimension;
        _listTableView.estimatedRowHeight = 44;
        _listTableView.showsVerticalScrollIndicator = false;
        [_listTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellID"];
        _listTableView.backgroundColor = [UIColor whiteColor];
        [_listTableView registerClass:[ShopDetailSectionOneTableCell class] forCellReuseIdentifier:@"ShopDetailSectionOneTableCell"];
        [_listTableView registerClass:[ShopPicShowTableCell class] forCellReuseIdentifier:@"ShopPicShowTableCell"];
        [self.view addSubview:_listTableView];
    }
    
    return _listTableView;
}


@end
