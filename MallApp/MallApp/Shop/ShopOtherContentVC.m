//
//  ShopOtherContentVC.m
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ShopOtherContentVC.h"
#import "SelectVCCell.h"
#import "ShopApi.h"
#import "GoodInfoVC.h"
#import "NoDataView.h"
@interface ShopOtherContentVC ()<UICollectionViewDelegate,UICollectionViewDataSource,DZNEmptyDataSetSource>
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *dataArr;
@property (nonatomic) ShopApi *api;
@end

@implementation ShopOtherContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initLayout];
    NSLog(@"%ld",self.type);
    [self getData];
    
}



- (void)initLayout{
    self.title = @"商城配件";
    

    self.view.backgroundColor = UIColorEF;
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.mas_equalTo(-170*AdapterScal);
    }];

}

- (void)getData{
    [self.api getServiceProductListWithparameters:@(self.type) andWithShopId:self.shopsId andWithNum:@"1" andSize:@"50" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            self.dataArr = result.resultDic[@"list"];
            [self.collectionView reloadData];
            [self.collectionView reloadEmptyDataSet];
        }
    }];
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((Screen_Width-10*3)/2,255);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SelectVCCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SelectVCCell" forIndexPath:indexPath];
    [cell setupData:self.dataArr[indexPath.item]];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GoodInfoVC *vc = [GoodInfoVC new];
    NSDictionary *dic = self.dataArr[indexPath.item];
    vc.goodsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
    [self.navigationController pushViewController:vc animated:true];
}


- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NoDataView *view = [NoDataView initView];


    [scrollView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(scrollView);
        make.centerY.mas_equalTo(-30);

    }];
    return view;
}




- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake((Screen_Width-10*3)/2, 255);
        flowLayout.minimumInteritemSpacing =0;
        flowLayout.minimumLineSpacing = 10;
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        
        _collectionView.backgroundColor = UIColorEF;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView setEmptyDataSetSource:self];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        WeakSelf(self)
//        _collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
////            [weakself getProductData];
//        }];
        
        [_collectionView registerNib:[UINib nibWithNibName:@"SelectVCCell" bundle:nil] forCellWithReuseIdentifier:@"SelectVCCell"];
    }
    
    return _collectionView;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
  
    JLog(@"子视图cell的偏移量%f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y <= 0) {

        scrollView.contentOffset = CGPointMake(0, 0);
       [[NSNotificationCenter defaultCenter] postNotificationName:@"leaveTop" object:nil];//到顶通知父视图改变状态
    }else{
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTop" object:nil userInfo:@{@"yValue":@(scrollView.contentOffset.y)}];
    }

   
}


- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}

- (ShopApi *)api{
    if (!_api) {
        _api = [ShopApi new];
    }
    return _api;
}
@end
