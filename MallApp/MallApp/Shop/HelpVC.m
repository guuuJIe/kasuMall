//
//  HelpVC.m
//  MallApp
//
//  Created by Mac on 2020/1/12.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "HelpVC.h"
#import "UnderLineButton.h"
#import "HelpStartPlaceView.h"
#import "HelpEndPlaceView.h"
#import "PurchaseView.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "NoteHelpInfoVC.h"
#import "AnotherNoteHelpInfoVC.h"
#import "ShopApi.h"
#import "HelpDetailVC.h"
#import "HelpDetailAboutKuaixiuVC.h"
#import "ReasonPopView.h"
#import "HelpPlaceVC.h"
@interface HelpVC ()<AMapLocationManagerDelegate,AMapSearchDelegate>
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UnderLineButton *selectedBtn;
@property (nonatomic,strong) HelpStartPlaceView *startView;
@property (nonatomic,strong) HelpEndPlaceView *endView;
@property (nonatomic,strong) UIButton *getHelpBtn;
@property (nonatomic,strong) MAMapView *mapView;
@property (nonatomic,strong) AMapLocationManager *locationManager;
@property (nonatomic,strong) AMapSearchAPI *search;
@property (nonatomic,strong) CLLocation *location;
@property (nonatomic,assign) NSInteger clickTag;
@property (nonatomic,assign) NSInteger protype;
@property (nonatomic,strong) ShopApi *shopApi;
@property (nonatomic,strong) ReasonPopView *reasonPopView;
@end

@implementation HelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"救援服务";
    [self setupUI];
    
    [self startLocation];
}

- (void)setupUI{
    
    [AMapServices sharedServices].enableHTTPS = YES;
    
  
    ///把地图添加至view
    [self.view addSubview:self.mapView];
    
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.cornerRadius = 8;
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(-10-BottomAreaHeight);
        make.height.mas_equalTo(200);
    }];
    self.bgView = bgView;
    
    UIView *lastView;
    NSArray *arr = @[@"抢修",@"施救",@"维修"];
    for (int i = 0; i<arr.count; i++) {
        UnderLineButton *button = [UnderLineButton new];
        [button setTitle:arr[i] forState:0];
        [button setTitleColor:UIColor999 forState:0];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [button setTitleColor:APPColor forState:UIControlStateSelected];
        button.tag = i+200;
        [self.bgView addSubview:button];
        [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.equalTo(self.bgView).offset(1);
            }else{
                make.left.equalTo(lastView.mas_right);
            }
            make.top.equalTo(self.bgView).offset(5);
            make.width.mas_equalTo((Screen_Width-20)/3);
            make.height.mas_equalTo(45);
        }];
        lastView = button;
    }
    
    
    
    UIView *line = [UIView new];
    line.backgroundColor = UIColorEF;
    [self.bgView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lastView.mas_bottom);
        make.left.equalTo(@10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(lineHeihgt);
    }];
    
    [self.bgView addSubview:self.startView];
    [self.startView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lastView.mas_bottom).offset(12);
        
        make.left.right.equalTo(self.bgView);
    }];
    
    [self.bgView addSubview:self.endView];
    [self.endView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.startView.mas_bottom).offset(0);
        make.height.mas_equalTo(86);
        make.left.right.equalTo(self.bgView);
    }];
   
    
    [self.bgView addSubview:self.getHelpBtn];
    [self.getHelpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgView.mas_bottom).offset(-10);

        make.left.equalTo(@10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(45);
    }];
    
    UnderLineButton *defaultButton = [self.view viewWithTag:200];
    [self click:defaultButton];
}

- (void)click:(UnderLineButton *)btn{
    if (btn!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        btn.selected = YES;
        self.selectedBtn = btn;
    }else{
        self.selectedBtn.selected = YES;
    }
    NSInteger tag = btn.tag;
    self.clickTag = tag;
    if (tag == 200) {
        self.protype = 1;
    }else if (tag == 201){
        
    }else if (tag == 202){
        self.protype = 3;
    }
    if(tag == 201) {
        self.endView.hidden = false;

        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(290);
        }];

    }else{
        self.endView.hidden = true;

        [self.bgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(200);
        }];

    }
    
    [self.bgView layoutIfNeeded];
    [self.bgView updateConstraintsIfNeeded];
}

- (void)startLocation
{
    if([CLLocationManager locationServicesEnabled]){
        AMapLocationManager *locationManager = [[AMapLocationManager alloc]init];
        locationManager.delegate = self;
        
        // 设定定位的最小更新距离。单位米，默认为 kCLDistanceFilterNone，表示只要检测到设备位置发生变化就会更新位置信息
        locationManager.distanceFilter = 5;
        
        // 设定期望的定位精度。单位米，默认为 kCLLocationAccuracyBest
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
//        //是否允许后台定位。默认为NO。只在iOS 9.0及之后起作用。
//        [locationManager setAllowsBackgroundLocationUpdates:NO];
//        
        // 开始定位服务
        
        _locationManager = locationManager;
        

        [self.locationManager startUpdatingLocation];
    }
}

- (void)clickToSubmitVC:(UITapGestureRecognizer *)ges{
    if (self.clickTag <= 201) {
        WeakSelf(self)
        //抢修 施救初始化查询
        [self.shopApi repairOpentWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
            StrongSelf(self)
            if (result.code == 200) {
                NSArray *datas = result.resultDic[@"list"];
                if ([datas.firstObject isEqual:[NSNull null]]) {
                   
                    if (self.clickTag == 201) {//施救
                        AnotherNoteHelpInfoVC *vc = [[AnotherNoteHelpInfoVC alloc] initWithNibName:@"AnotherNoteHelpInfoVC" bundle:nil];
                        [self.navigationController pushViewController:vc animated:true];
                    }else{//抢修

                        HelpPlaceVC *vc = [HelpPlaceVC new];
                        vc.clickBlock = ^(AMapPOI * _Nonnull poi) {

                            [self.startView setupAdress:poi];
                            
                        };
                        [self.navigationController pushViewController:vc animated:true];
                    }
                    
                }else{
                    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"你有订单未完成,是否前往查看?" andHandler:^(NSInteger index) {
                        if (index == 1) {
                            NSDictionary *dic = datas.firstObject;
                            HelpDetailAboutKuaixiuVC *vc = [HelpDetailAboutKuaixiuVC new];
                            vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
                            [self.navigationController pushViewController:vc animated:true];
                        }
                    }];
                }
                
            }
        }];
        
    }else{
        [self.shopApi queryRepairOrderWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *datas = result.resultDic[@"list"];
                //                NSDictionary *dic = datas.firstObject;
                if ([datas.firstObject isEqual:[NSNull null]]) {//维修
                    NoteHelpInfoVC *vc = [[NoteHelpInfoVC alloc] initWithNibName:@"NoteHelpInfoVC" bundle:nil];
                    vc.protype = self.protype;
                    [self.navigationController pushViewController:vc animated:true];
                }else{
                    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"你有订单未完成,是否前往查看?" andHandler:^(NSInteger index) {
                        if (index == 1) {
                            NSDictionary *dic = datas.firstObject;
                            HelpDetailVC *vc = [HelpDetailVC new];
                            if([dic[@"orderNumber"] isEqual:[NSNull null]]){
                                vc.orderNum = nil;
                            }else{
                                vc.orderNum = dic[@"orderNumber"];
                            }
                            
                            [self.navigationController pushViewController:vc animated:true];
                        }
                    }];
                    
                }
                
            }
        }];
    }
}

#pragma mark ---AMapLocationManagerDelegate---


- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location{
    NSLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
    self.location = location;
    // 发起周边搜索
    [self searchAround];
    // 停止定位
    [self.locationManager stopUpdatingLocation];
}
- (void)amapLocationManager:(AMapLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
}

/** 根据定位坐标进行周边搜索 */
- (void)searchAround{
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    
    //构造AMapPOIAroundSearchRequest对象，设置周边请求参数
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
    // types属性表示限定搜索POI的类别，默认为：餐饮服务|商务住宅|生活服务
    // POI的类型共分为20种大类别，分别为：
    // 汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|
    // 医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|
    // 交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施
//    request.types = @"汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施";
//    request.types = @"维修厂";
    request.sortrule = 0;
    request.requireExtension = YES;
    
    NSLog(@"周边搜索");
    
    //发起周边搜索
    [self.search AMapPOIAroundSearch: request];
}

// 实现POI搜索对应的回调函数
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response{
    NSLog(@"周边搜索回调");
    if(response.pois.count == 0)
    {
        return;
    }
    
    NSLog(@"%@",response.pois.firstObject);
    
    [self.startView setupAdress:response.pois.firstObject];
}


- (HelpStartPlaceView *)startView{
    if (!_startView) {
        _startView = [[HelpStartPlaceView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 20, 70)];
        _startView.tag = 100;
        [_startView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickToSubmitVC:)]];
//        [self.view addSubview:_startView];
    }
    return _startView;
}
- (HelpEndPlaceView *)endView{
    if (!_endView) {
        _endView.tag = 101;
        _endView = [[HelpEndPlaceView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 20, 100)];
        [_endView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickToSubmitVC:)]];
    }
    return _endView;
}

- (void)click{
    if (self.clickTag <= 201) {
        
        WeakSelf(self)
        //抢修 施救初始化查询
        [self.shopApi repairOpentWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *datas = result.resultDic[@"list"];
                if ([datas.firstObject isEqual:[NSNull null]]) {
                    if (self.clickTag == 201) {//施救
                        AnotherNoteHelpInfoVC *vc = [[AnotherNoteHelpInfoVC alloc] initWithNibName:@"AnotherNoteHelpInfoVC" bundle:nil];
                        [self.navigationController pushViewController:vc animated:true];
                    }else{//抢修
                        [self.reasonPopView show];
                        self.reasonPopView.clickBlock = ^(NSString * _Nonnull text) {
                            
                            StrongSelf(self)
                            NoteHelpInfoVC *vc = [[NoteHelpInfoVC alloc] initWithNibName:@"NoteHelpInfoVC" bundle:nil];
                            vc.protype = self.protype;
                            vc.reason = text;
                            [self.navigationController pushViewController:vc animated:true];
                            
                            
                        };
                    }
                    
                }else{
                    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"你有订单未完成,是否前往查看?" andHandler:^(NSInteger index) {
                        if (index == 1) {
                            NSDictionary *dic = datas.firstObject;
                            HelpDetailAboutKuaixiuVC *vc = [HelpDetailAboutKuaixiuVC new];
                            vc.orderNum = [NSString stringWithFormat:@"%@",dic[@"orderNumber"]];
                            [self.navigationController pushViewController:vc animated:true];
                        }
                    }];
                }
                
            }
        }];
      
    }else{
        [self.shopApi queryRepairOrderWithparameters:@"0" withCompletionHandler:^(NSError *error, MessageBody *result) {
            if (result.code == 200) {
                NSArray *datas = result.resultDic[@"list"];
//                NSDictionary *dic = datas.firstObject;
                if ([datas.firstObject isEqual:[NSNull null]]) {//维修
                    NoteHelpInfoVC *vc = [[NoteHelpInfoVC alloc] initWithNibName:@"NoteHelpInfoVC" bundle:nil];
                    vc.protype = self.protype;
                    [self.navigationController pushViewController:vc animated:true];
                }else{
                    [JSCMMPopupViewTool showMMPopAlertWithMessage:@"你有订单未完成,是否前往查看?" andHandler:^(NSInteger index) {
                        if (index == 1) {
                            NSDictionary *dic = datas.firstObject;
                            HelpDetailVC *vc = [HelpDetailVC new];
                            if([dic[@"orderNumber"] isEqual:[NSNull null]]){
                                vc.orderNum = nil;
                            }else{
                                vc.orderNum = dic[@"orderNumber"];
                            }
                            
                            [self.navigationController pushViewController:vc animated:true];
                        }
                    }];
                    
                }
                
            }
        }];
    }
    
}

-(UIButton *)getHelpBtn
{
    if(!_getHelpBtn)
    {
        _getHelpBtn=[UIButton  new];
        [_getHelpBtn setTitle:@"获取救援" forState:UIControlStateNormal];
        _getHelpBtn.titleLabel.font = LabelFont14;
        [_getHelpBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getHelpBtn.backgroundColor = UIColor276;
        _getHelpBtn.layer.cornerRadius = 5;
        [_getHelpBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _getHelpBtn;
}

-(MAMapView *)mapView{
    if (!_mapView) {
        _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        _mapView.showsUserLocation = YES;
        _mapView.userTrackingMode = MAUserTrackingModeFollow;
        // 设置缩放级别
        _mapView.zoomLevel = 18;
    }
    return _mapView;
}

- (ShopApi *)shopApi{
    if (!_shopApi) {
        _shopApi = [ShopApi new];
    }
    
    return _shopApi;
}

- (ReasonPopView *)reasonPopView{
    if (!_reasonPopView) {
        _reasonPopView = [ReasonPopView new];
        _reasonPopView.datasArr = @[@{@"title":@"电瓶没电"},@{@"title":@"轮胎破损"},@{@"title":@"其他"}];
        _reasonPopView.titleLabel.text = @"请选择故障原因";
    }
    
    return _reasonPopView;
}
@end
