//
//  NoteHelpInfoVC.m
//  MallApp
//
//  Created by Mac on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NoteHelpInfoVC.h"
#import "PurchaseView.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "HelpPlaceVC.h"
#import "ShopApi.h"
#import "CarListVC.h"
#import "HelpDetailVC.h"
#import "ReasonPopView.h"
#import "HelpDetailAboutKuaixiuVC.h"
@interface NoteHelpInfoVC ()<AMapLocationManagerDelegate,AMapSearchDelegate>
@property (nonatomic,strong) PurchaseView *addNewAddress;
@property (nonatomic,strong) AMapLocationManager *locationManager;
@property (nonatomic,strong) AMapSearchAPI *search;
@property (nonatomic,strong) CLLocation *location;
@property (nonatomic,strong) ShopApi *shopApi;
@property (nonatomic,strong) AMapGeoPoint *placelocation;
@property (nonatomic,strong) AMapPOI *poi;
@property (nonatomic,strong) ReasonPopView *reasonPopView;
@property (nonatomic,strong) NSMutableArray *datasArr;
@property (nonatomic,strong) NSString *carModel;
@end

@implementation NoteHelpInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    [self setupUI];
    
  
    [self initProp];
}

- (void)setupUI{
    self.title = @"救援信息";
    [self.view addSubview:self.addNewAddress];
    [self.addNewAddress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
}

- (void)initProp{
    [self.addressView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
    [self startLocation];
    [self.typeLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectCartype)]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCarType:) name:@"getCarModel" object:nil];
    self.quesLbl.text = self.reason;
}

- (void)startLocation
{
    if([CLLocationManager locationServicesEnabled]){
        AMapLocationManager *locationManager = [[AMapLocationManager alloc]init];
        locationManager.delegate = self;
        
        // 设定定位的最小更新距离。单位米，默认为 kCLDistanceFilterNone，表示只要检测到设备位置发生变化就会更新位置信息
        locationManager.distanceFilter = 5;
        
        // 设定期望的定位精度。单位米，默认为 kCLLocationAccuracyBest
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
        // 开始定位服务
        
        _locationManager = locationManager;
        
        [JMBManager showLoading];
        [self.locationManager startUpdatingLocation];
    }
}

- (void)selectCartype{
    CarListVC *vc = [CarListVC new];
//    vc.clickBlock = ^(NSString * _Nonnull name) {
//        self.typeLbl.text = name;
//    };
    vc.type = 1;
    vc.vcname = @"NoteHelpInfoVC";
    [self.navigationController pushViewController:vc animated:true];
}

- (void)getCarType:(NSNotification *)noti{
    JLog(@"%@",noti.userInfo);
    NSDictionary *dic = noti.userInfo;
    NSString *carModel = dic[@"model"];
    NSArray * array = [carModel componentsSeparatedByString:@"-"];

    self.carModel = [array componentsJoinedByString:@","];
    self.typeLbl.text = dic[@"carname"];
}


- (void)click{
    HelpPlaceVC *vc = [HelpPlaceVC new];
    vc.clickBlock = ^(AMapPOI * _Nonnull poi) {
        self.nameLbl.text = poi.name;
        self.adressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",poi.province,poi.city,poi.district,poi.name];
        self.placelocation = poi.location;
        self.poi = poi;
    };
    [self.navigationController pushViewController:vc animated:true];
}
#pragma mark ---AMapLocationManagerDelegate---
- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location{
    NSLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
    self.location = location;
    // 发起周边搜索
    [self searchAround];
    // 停止定位
    [self.locationManager stopUpdatingLocation];
}
- (void)amapLocationManager:(AMapLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
}

/** 根据定位坐标进行周边搜索 */
- (void)searchAround{
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    
    //构造AMapPOIAroundSearchRequest对象，设置周边请求参数
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
    self.placelocation = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
    // types属性表示限定搜索POI的类别，默认为：餐饮服务|商务住宅|生活服务
    // POI的类型共分为20种大类别，分别为：
    // 汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|
    // 医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|
    // 交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施
//    request.types = @"汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施";
//    request.types = @"维修厂";
    request.sortrule = 0;
    request.requireExtension = YES;
    
    NSLog(@"周边搜索");
    
    //发起周边搜索
    [self.search AMapPOIAroundSearch: request];
}

// 实现POI搜索对应的回调函数
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response{
    NSLog(@"周边搜索回调");
    if(response.pois.count == 0)
    {
        return;
    }
    
//    NSLog(@"%@",response.pois.firstObject);
    AMapPOI *poi = response.pois.firstObject;
    self.nameLbl.text = poi.name;
    self.adressLbl.text = [NSString stringWithFormat:@"%@%@%@%@",poi.province,poi.city,poi.district,poi.name];
    self.poi = poi;
    [JMBManager hideAlert];
   
}
- (IBAction)selectTroubleType:(id)sender {
    
    [self.reasonPopView show];
    WeakSelf(self)
    self.reasonPopView.clickBlock = ^(NSString * _Nonnull text) {
        weakself.quesLbl.text = text;
    };
}

- (PurchaseView *)addNewAddress{
    if (!_addNewAddress) {
        _addNewAddress = [PurchaseView new];
        [_addNewAddress.purchaseButton setTitle:@"提交" forState:0];
        WeakSelf(self);
        _addNewAddress.addAddressBlock = ^{
            StrongSelf(self);
//            NSDictionary *dic  =
//             @{
//                 @"CarModel":[self.carModel substringToIndex:self.carModel.length - 1],
//                 @"CarContact":self.usernameLbl.text,
//                 @"CarLicense":self.carNumLbl.text,
//                 @"CarPhone":self.telLbl.text,
//                 @"Remark":self.quesLbl.text,
//                 @"LocationGPS":[NSString stringWithFormat:@"%f,%f",self.placelocation.latitude,self.placelocation.longitude],
//                 @"LocationString":[NSString stringWithFormat:@"%@%@%@%@%@",self.poi.province,self.poi.city,self.poi.district,self.poi.name,self.poi.address],
//                 @"ProType":@(self.protype)
//             };
            if (self.protype == 3) {//维修
                NSDictionary *dic  =
                            @{
                                @"CarModel":self.typeLbl.text,
                                @"CarContact":self.usernameLbl.text,
                                @"CarLicense":self.carNumLbl.text,
                                @"CarPhone":self.telLbl.text,
                                @"Remark":self.quesLbl.text,
                                @"LocationGPS":[NSString stringWithFormat:@"%f,%f",self.placelocation.longitude,self.placelocation.latitude],
                                @"LocationString":[NSString stringWithFormat:@"%@%@%@%@%@",self.poi.province,self.poi.city,self.poi.district,self.poi.name,self.poi.address],
                                @"ProType":@(self.protype)
                            };
                [self.shopApi submitFixOrderDataWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        [JMBManager showBriefAlert:@"提交成功"];
                        [self.navigationController popViewControllerAnimated:true];
//                        NSArray *datas = result.result;
//                        HelpDetailVC *vc = [HelpDetailVC new];
//                        vc.orderNum = datas.firstObject;
//                        [self.navigationController pushViewController:vc animated:true];
                    }
                }];
            }else{
                NSDictionary *dic  =
                            @{
                                @"CarModel":[self.carModel substringToIndex:self.carModel.length - 1],
                                @"CarContact":self.usernameLbl.text,
                                @"CarLicense":self.carNumLbl.text,
                                @"CarPhone":self.telLbl.text,
                                @"Remark":self.quesLbl.text,
                                @"LocationGPS":[NSString stringWithFormat:@"%f,%f",self.placelocation.longitude,self.placelocation.latitude],
                                @"LocationString":[NSString stringWithFormat:@"%@%@%@%@%@",self.poi.province,self.poi.city,self.poi.district,self.poi.name,self.poi.address],
                                @"ProType":@(self.protype)
                            };
                [self.shopApi sumitHelpOrderWithparameters:dic withCompletionHandler:^(NSError *error, MessageBody *result) {
                    if (result.code == 200) {
                        NSArray *datas = result.result;
                        
                        HelpDetailAboutKuaixiuVC *vc = [HelpDetailAboutKuaixiuVC new];
                        vc.orderNum = datas.firstObject;
                        [self.navigationController pushViewController:vc animated:true];
                    }
                }];
            }
           
           
        };
        
    };
    return _addNewAddress;
}

- (ShopApi *)shopApi{
    if (!_shopApi) {
        _shopApi = [ShopApi new];
    }
    return _shopApi;
}

- (ReasonPopView *)reasonPopView{
    if (!_reasonPopView) {
        _reasonPopView = [ReasonPopView new];
        _reasonPopView.datasArr = self.datasArr;
        _reasonPopView.titleLabel.text = @"故障原因";
    }
    
    return _reasonPopView;
}

- (NSMutableArray *)datasArr{
    if (!_datasArr) {
        _datasArr = [NSMutableArray array];
        [_datasArr addObject:@{@"title":@"电瓶没电"}];
        [_datasArr addObject:@{@"title":@"轮胎破损"}];
        [_datasArr addObject:@{@"title":@"其他"}];

    }
    
    return _datasArr;
}

@end
