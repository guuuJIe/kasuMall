//
//  WeiXiuShop.m
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WeiXiuShop.h"
#import "ShopItemCell.h"
#import "ShopSelectTopView.h"
#import "DQAreasView.h"
#import "DQAreasModel.h"
#import "DropDownListView.h"
#import "ShopInfoVC.h"
#import "ShopApi.h"
@interface WeiXiuShop ()<UITableViewDelegate,UITableViewDataSource,DQAreasViewDelegate>
@property (nonatomic,strong) UITableView *TabelView;
@property (nonatomic,strong) ShopSelectTopView *topView;
@property (nonatomic,strong) DropDownListView *selView;
@property (nonatomic,assign) NSInteger proviceId;
@property (nonatomic,assign) NSInteger cityId;
@property (nonatomic,assign) NSInteger areaId;
@property (nonatomic,strong) NSArray *proviceArr;
@property (nonatomic,strong) NSArray *cityArr;
@property (nonatomic,strong) NSArray *areaArr;
@property (nonatomic,strong) ShopApi *shopApi;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSMutableArray *regionArr;
@property (nonatomic,assign) NSInteger RegionID;
@property (nonatomic,assign) NSInteger clickTag;

@property (nonatomic,strong) NSString *p;
@property (nonatomic,strong) NSString *c;
@property (nonatomic,strong) NSString *a;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,strong) NSString *size;
@end

@implementation WeiXiuShop

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initProp];
    [self setupUI];
    [self setupData:RefreshTypeDown];
    [self getRegion];
    
    
}

- (void)initProp{
    self.RegionID = 0;
    self.clickTag = 200;
    self.num = 1;
    self.size = @"20";
}

- (void)setupUI{
    self.title = @"维修门店";
    [self.view addSubview:self.topView];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    [self.view addSubview:self.TabelView];
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(40);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    [self.selView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom).offset(1);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
//    self.areasView = [DQAreasView new];
//    self.areasView.delegate = self;

}



- (void)setupData:(RefreshType)type{
    if (type == RefreshTypeDown) {
        self.num = 1;
    }else if (type == RefreshTypeUP){
        self.num = self.num+1;
    }
    
    NSDictionary *dic = @{@"p":[XJUtil insertStringWithNotNullObject:self.p andDefailtInfo:@""],@"c":[XJUtil insertStringWithNotNullObject:self.c andDefailtInfo:@""],@"a":[XJUtil insertStringWithNotNullObject:self.a andDefailtInfo:@""]};
    [self.shopApi shopListWithparameters:dic andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:@"20" withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dataArr = [XJUtil dealData:datas withRefreshType:type withListView:self.TabelView AndCurDataArray:self.dataArr withNum:self.num];
            [self.TabelView reloadData];
        }
    }];
}

- (void)getRegion{
    [self.shopApi getRegionWithparameters:@(self.RegionID) withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
//             = result.resultDic[@"list"];
            if (self.clickTag == 200 && self.proviceId == 0) {
                self.proviceArr = result.resultDic[@"list"];
            }else if (self.clickTag == 201 && self.proviceId != 0){
                self.cityArr = result.resultDic[@"list"];
            }else if (self.clickTag == 202 && self.cityId != 0){
                self.areaArr = result.resultDic[@"list"];
            }
//            if (self.proviceId == 0) {
//
//            }else if (self.cityId == 0){
//
//            }else if (self.areaId == 0){
//
//            }
            self.selView.datasource = result.resultDic[@"list"];
        }
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"ShopItemCell";
    ShopItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[ShopItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    cell.contentView.backgroundColor = UIColorF5F7;
    [cell setupData:self.dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ShopInfoVC *vc = [ShopInfoVC new];
    NSDictionary *dic = self.dataArr[indexPath.row];
    vc.shopsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark
- (void)clickAreasViewEnsureBtnActionAreasDate:(DQAreasModel *)model{
    NSLog(@"%@",model.city);
    [self.topView.proviceBtn setTitle:model.Province forState:0];
    [self.topView.cityBtn setTitle:model.city forState:0];
    [self.topView.areaBtn setTitle:model.county forState:0];

}

-(void)showSel{
    
    if (self.selView.hidden) {
        [self.selView showSelView];
    }else{
        [self.selView hidenSelView];
    }
}

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

        [_TabelView registerNib:[UINib nibWithNibName:@"ShopItemCell" bundle:nil] forCellReuseIdentifier:@"ShopItemCell"];
        WeakSelf(self);
        _TabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            StrongSelf(self);
            
            [self setupData:RefreshTypeDown];
            
        }];
        
        _TabelView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self setupData:RefreshTypeUP];
        }];
        _TabelView.backgroundColor = [UIColor clearColor];
        
    }
    return _TabelView;
}


    

- (ShopSelectTopView *)topView{
    if (!_topView) {
        _topView = [ShopSelectTopView new];
        WeakSelf(self);
        _topView.clickBlock = ^(NSInteger tag) {
            StrongSelf(self);
            if (tag == 200) {
                if (self.proviceArr.count != 0) {
                    self.selView.datasource = self.proviceArr;
                }
                
            }else if (tag == 201){
                if (weakself.proviceId == 0) {
                    [JMBManager showBriefAlert:@"请先选择省"];
                    return ;
                }
                if (self.cityArr.count != 0) {
                    self.selView.datasource = self.cityArr;
                }
               
            }else if (tag == 202){
                if (weakself.proviceId == 0) {
                   [JMBManager showBriefAlert:@"请先选择省"];
                    return ;
                }
                
                if (weakself.cityId == 0) {
                   [JMBManager showBriefAlert:@"请先选择市"];
                    return ;
                }
                self.selView.datasource = self.areaArr;
            }
            self.clickTag = tag;
            [weakself showSel];
        };
    }
    return _topView;
}

- (DropDownListView *)selView
{
    if (!_selView) {
        _selView = [[DropDownListView alloc] init];
        [_selView hidenSelView];
//        _selView.datasource = [NSMutableArray arrayWithObjects:@"北京",@"天津",@"上海",@"四川",@"成都", nil];
        WeakSelf(self);
        _selView.clickBlock = ^(NSInteger row) {
            
            StrongSelf(self);
            if (self.clickTag == 200 && self.proviceArr.count != 0) {
                NSDictionary *dic = weakself.proviceArr[row];
                self.proviceId = [dic[@"id"] integerValue];
                [self.topView.proviceBtn setTitle:[NSString stringWithFormat:@"%@",dic[@"name"]] forState:0];
                self.RegionID = [dic[@"id"] integerValue];
                self.p = [NSString stringWithFormat:@"%@",dic[@"name"]];
                self.clickTag = 201;
                [self getRegion];
            }else if (self.clickTag == 201 && self.cityArr.count != 0){
                NSDictionary *dic = weakself.cityArr[row];
                self.cityId = [dic[@"id"] integerValue];
                [self.topView.cityBtn setTitle:[NSString stringWithFormat:@"%@",dic[@"name"]] forState:0];
                self.RegionID = [dic[@"id"] integerValue];
                self.c = [NSString stringWithFormat:@"%@",dic[@"name"]];
                self.clickTag = 202;
                [self getRegion];
            }else if (self.clickTag == 202 && self.areaArr.count != 0){
                NSDictionary *dic = weakself.areaArr[row];
                self.areaId = [dic[@"id"] integerValue];
                [self.topView.areaBtn setTitle:[NSString stringWithFormat:@"%@",dic[@"name"]] forState:0];
                self.RegionID = [dic[@"id"] integerValue];
                 self.a = [NSString stringWithFormat:@"%@",dic[@"name"]];
            }
          
            NSLog(@"%ld - %ld  - %ld",self.proviceId,self.areaId,self.cityId);
            NSLog(@"%@ %@ %@",self.p,self.c,self.a);
            
            [self setupData:RefreshTypeDown];
            
            
            
            [self.selView hidenSelView];
            
            
        };
        [self.view addSubview:_selView];
    }
    return _selView;
}

- (ShopApi *)shopApi{
    if (!_shopApi) {
        _shopApi = [ShopApi new];
    }
    return _shopApi;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}

- (NSMutableArray *)regionArr{
    if (!_regionArr) {
        _regionArr = [NSMutableArray new];
    }
    return _regionArr;
}
@end
