//
//  ShopInfoVC.h
//  MallApp
//
//  Created by 温州轩捷贸易有限公司 on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseHideNavViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShopInfoVC : BaseHideNavViewController
@property (nonatomic,strong) NSString *shopsId;
@end

NS_ASSUME_NONNULL_END
