//
//  WeixiuTaoCanVC.m
//  MallApp
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "WeixiuTaoCanVC.h"
#import "WeixiuTaocanCell.h"
#import "GoodsWithSpecificVC.h"
#import "ShopApi.h"
#import "ShopInfoVC.h"
#import "SearchGoodsApi.h"
@interface WeixiuTaoCanVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic,strong)UITableView *TabelView;
@property (nonatomic,strong)ShopApi *shopApi;
@property (nonatomic,assign)NSInteger num;
@property (nonatomic,strong)NSString *size;
@property (nonatomic,strong)NSString *type;
@property (nonatomic,strong)NSMutableArray *dataArr;
@property (nonatomic,strong)UIView *titleView;
@property (nonatomic,strong)SearchGoodsApi *goodsApi;
@end

@implementation WeixiuTaoCanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"维修套餐";
    [self initProp];
    [self setupUI];
    [self setupData];
    
}

 -(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationItem.hidesBackButton = true;
     
     self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.titleView];

     
}


- (void)setupUI{
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)initProp{
    self.num = 1;
    self.size = @"20";
}

- (void)setupData{
   
   
    NSDictionary *dic = @{@"type":@"8",@"order":@"0"};
    WeakSelf(self)
    [self.shopApi fixgoodsListWithparameters:dic andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSArray *array = result.resultDic[@"list"];
            if (self.num == 1) {
                self.dataArr = [NSMutableArray arrayWithArray:array];
            }else{
                if (array.count == 0) {
                    [self.TabelView.mj_footer endRefreshingWithNoMoreData];
                }else{
                    if (array.count <20) {
                        [self.TabelView.mj_footer endRefreshingWithNoMoreData];
                    }
                    [self.dataArr addObjectsFromArray:array];
                }
                
            }
           
            [self.TabelView reloadData];
        }
    }];
}

- (void)loadMoreData{
    self.num++;
    [self setupData];
}

- (void)searchProWithKeywords:(NSString *)keys{
    NSDictionary *dic = @{@"key":keys,@"type":@"8"};
    [JMBManager showLoading];
    [self.goodsApi searchProductListWithparameters:dic andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dataArr = [XJUtil dealData:datas withRefreshType:RefreshTypeDown withListView:self.TabelView AndCurDataArray:self.dataArr withNum:self.num];
            [self.TabelView reloadData];
        }
        [JMBManager hideAlert];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *Identifier = @"WeixiuTaocanCell";
    WeixiuTaocanCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[WeixiuTaocanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    [cell setupData:self.dataArr[indexPath.row]];
    WeakSelf(self)
    cell.itemClickBlock = ^{
        StrongSelf(self)
        NSDictionary *dic = self.dataArr[indexPath.row];
        ShopInfoVC *vc = [ShopInfoVC new];
        vc.shopsId = [NSString stringWithFormat:@"%@",dic[@"shopId"]];
        [self.navigationController pushViewController:vc animated:true];
    };
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GoodsWithSpecificVC *vc = [GoodsWithSpecificVC new];
    NSDictionary *dic = self.dataArr[indexPath.row];
    vc.goodsId = dic[@"id"];
    [self.navigationController pushViewController:vc animated:true];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length == 0) {
        return false;
    }
    [self searchProWithKeywords:textField.text];
    return true;
}

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

        [_TabelView registerNib:[UINib nibWithNibName:@"WeixiuTaocanCell" bundle:nil] forCellReuseIdentifier:@"WeixiuTaocanCell"];
        WeakSelf(self)
        _TabelView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakself loadMoreData];
        }];
        _TabelView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (ShopApi *)shopApi{
    if (!_shopApi) {
        _shopApi = [ShopApi new];
    }
    return _shopApi;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}

- (UIView *)titleView{
    if (!_titleView) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 30)];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [button setImage:[UIImage imageNamed:@"back"] forState:0];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        [button addTarget:self action:@selector(clickPop) forControlEvents:UIControlEventTouchUpInside];
        [_titleView addSubview:button];

        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(button.frame), 0, _titleView.frame.size.width - 70, 30)];
        titleView.backgroundColor = [UIColor whiteColor];
        titleView.layer.cornerRadius = 15;
//        [titleView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onClickSearchBtn)]];
        
        UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 20, 20)];
        selectIcon.image=[UIImage imageNamed:@"搜索"];
        
        UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(selectIcon.frame), 2, _titleView.frame.size.width-130-selectIcon.frame.size.width, _titleView.frame.size.height - 3)];
        textField.textColor=UIColor333;
        textField.placeholder=@"请输入套餐名称";
        
        if (@available(iOS 13.0, *)) {
            
            textField.attributedPlaceholder=[[NSAttributedString alloc]initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];

        }else{
            [textField setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
        }
        textField.font=LabelFont14;
        textField.returnKeyType = UIReturnKeySearch;
        textField.delegate = self;
//        textField.userInteractionEnabled = false;

        [titleView addSubview:selectIcon];
        [titleView addSubview:textField];
        [_titleView addSubview:titleView];
        
    }
    
    return _titleView;
}
- (SearchGoodsApi *)goodsApi{
    if (!_goodsApi) {
        _goodsApi = [SearchGoodsApi new];
    }
    return _goodsApi;
}

- (void)clickPop{
    [self.navigationController popViewControllerAnimated:true];
}
@end
