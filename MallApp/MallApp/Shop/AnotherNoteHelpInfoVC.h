//
//  AnotherNoteHelpInfoVC.h
//  MallApp
//
//  Created by Mac on 2020/1/13.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AnotherNoteHelpInfoVC : BaseViewController
- (IBAction)AddAddressOne:(id)sender;
- (IBAction)addAdressTwo:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *oneLbl;
@property (weak, nonatomic) IBOutlet UILabel *oneSubLbl;
@property (weak, nonatomic) IBOutlet UILabel *twoSubLbl;
@property (weak, nonatomic) IBOutlet UILabel *twoLbl;
@property (weak, nonatomic) IBOutlet UILabel *cartypeLbl;
@property (weak, nonatomic) IBOutlet UITextField *carNumLbl;
@property (weak, nonatomic) IBOutlet UITextField *nameLbl;
@property (weak, nonatomic) IBOutlet UITextField *telLbl;
@property (weak, nonatomic) IBOutlet UITextField *quesLbl;

@end

NS_ASSUME_NONNULL_END
