//
//  ShopVC.m
//  MallApp
//
//  Created by Mac on 2020/1/3.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>

#import "ShopVC.h"
#import "RepairFuncCell.h"
#import "RepairHeaderCell.h"
#import "WeixiuTaocanCell.h"
#import "CarPayView.h"
#import "ShopInfoVC.h"
#import "WeixiuTaoCanVC.h"
#import "WeiXiuShop.h"
#import "ShopApi.h"
#import "HelpVC.h"
#import "HelpDetailVC.h"
#import "LoginVC.h"
#import "NoDataView.h"
#import "CarMaintainceList.h"
#import "MyServicesOfYear.h"
#import "RepairBannerCell.h"
#import "SearchVC.h"
#import "GoodsWithSpecificVC.h"
@interface ShopVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>
@property (nonatomic,strong) UITableView *TabelView;
@property (nonatomic,strong) CarPayView *payView;
@property (nonatomic,strong) ShopApi *shopApi;
@property (nonatomic,assign) NSInteger num;
@property (nonatomic,strong) NSString *size;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) AMapLocationManager *locationManager;
@property (nonatomic,strong) UIButton * searchBtn;
@property (nonatomic,strong) UIView *titleView;

@end

@implementation ShopVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.title = @"维修";
    [self initProp];
    
    [self setupUI];
   
//    [self getLocation];
    [self getWeixiuData:RefreshTypeNormal];
}

- (void)initProp{
    self.num = 1;
    self.size = @"20";
}

 -(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationItem.hidesBackButton = true;
     
     self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.titleView];

     
}

- (void)setupUI{
    self.title = @"汽车维修";
    [self.TabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
    
}

- (void)getWeixiuData:(RefreshType)type{
    if (type == RefreshTypeDown || type == RefreshTypeNormal) {
         self.num = 1;
     }else if (type == RefreshTypeUP){
         self.num = self.num+1;
     }
    NSDictionary *dic = @{@"type":@"8",@"order":@"0"};
    WeakSelf(self)
    [self.shopApi fixgoodsListWithparameters:dic andWithNum:[NSString stringWithFormat:@"%ld",(long)self.num] andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
        StrongSelf(self)
        if (result.code == 200) {
            NSArray *datas = result.result;
            self.dataArray = [XJUtil dealData:datas withRefreshType:type withListView:self.TabelView AndCurDataArray:self.dataArray withNum:self.num];
            [self.TabelView reloadData];
        }
    }];
}

//- (void)setupDatawithPro:(NSString *)pro city:(NSString *)city country:(NSString *)country{
//    NSDictionary *dic = @{@"p":pro,@"c":city,@"a":country};
//    [JMBManager showLoading];
//    [self.shopApi shopListWithparameters:dic andWithNum:self.num andSize:self.size withCompletionHandler:^(NSError *error, MessageBody *result) {
//        if (result.code == 200) {
//            self.dataArray = result.resultDic[@"list"];
//            [self.TabelView reloadData];
//            [self.TabelView reloadEmptyDataSetInSection:2];
//        }
//        [JMBManager hideAlert];
//    }];
//}

- (void)getLocation{
    
//    if([CLLocationManager locationServicesEnabled]){
//        //查询天气
////        AMapWeatherSearchRequest *request = [[AMapWeatherSearchRequest alloc] init];
//        AMapLocationManager *locationManager = [[AMapLocationManager alloc]init];
////        locationManager.delegate = self;
//
//        // 设定定位的最小更新距离。单位米，默认为 kCLDistanceFilterNone，表示只要检测到设备位置发生变化就会更新位置信息
//        locationManager.distanceFilter = 5;
//
//        // 设定期望的定位精度。单位米，默认为 kCLLocationAccuracyBest
//        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
//
//        //        //是否允许后台定位。默认为NO。只在iOS 9.0及之后起作用。
//        //        [locationManager setAllowsBackgroundLocationUpdates:NO];
//        //
//        // 开始定位服务
//
//        _locationManager = locationManager;
//        [JMBManager showLoading];
//        [_locationManager requestLocationWithReGeocode:true completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
//            JLog(@"%@%@%@",regeocode.province,regeocode.district,regeocode.city);
//            if (error) {
//                [self setupDatawithPro:@"浙江省" city:@"温州市" country:@"鹿城区"];
//            }else{
//                [self setupDatawithPro:regeocode.province city:regeocode.city country:regeocode.district];
//            }
//
////            self.titleLabel.text = regeocode.city;
//
//        }];
//    ;
//    }
}

-(void)initBar{
    UIView * searview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width,30)];
    searview.backgroundColor = [UIColor clearColor];
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searview];
    self.navigationItem.titleView = searview;
//    self.titleLabel.frame = CGRectMake(10, 0, 50, 30);
//    [searview addSubview:self.titleLabel];
    
    
    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, searview.frame.size.width-100, 30)];
    [self.searchBtn setTitle:@"请输入商品名称" forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.searchBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.searchBtn setBackgroundColor:[UIColor whiteColor]];
    [self.searchBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 0, 5, 0)];
    self.searchBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.searchBtn setImage:[UIImage imageNamed:@"搜索"] forState:(UIControlStateNormal)];
//    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    self.searchBtn.layer.cornerRadius = 15;
    self.searchBtn.layer.masksToBounds = YES;
    [searview addSubview:self.searchBtn];
    
    
  
        
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 3) {
        return self.dataArray.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        static NSString *Identifier = @"RepairFuncCell";
        RepairFuncCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[RepairFuncCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
//        WeakSelf(self)
//        cell.clickBlock = ^(NSInteger index) {
//            StrongSelf(self)
//            if (index == 0) {
////
//                CarMaintainceList *vc = [CarMaintainceList new];
//                [self.navigationController pushViewController:vc animated:true];
//            }else if(index == 2){
//
//            }else if (index == 1){
//
//                 WeixiuTaoCanVC *vc = [WeixiuTaoCanVC new];
//                [self.navigationController pushViewController:vc animated:true];
////
//
//            }else if (index == 3){
//                MyServicesOfYear *vc = [MyServicesOfYear new];
//                [self.navigationController pushViewController:vc animated:true];
//            }else if (index == 4){
//                WeiXiuShop *vc = [WeiXiuShop new];
//                [self.navigationController pushViewController:vc animated:true];
//            }
//
//        };
        
        WeakSelf(self)
        cell.clickBlock = ^(NSString * _Nonnull name) {
            StrongSelf(self)
            if ([name isEqualToString:@"维修套餐"]) {
                WeixiuTaoCanVC *vc = [WeixiuTaoCanVC new];
                [self.navigationController pushViewController:vc animated:true];
            }else if ([name isEqualToString:@"一年保养"]){
                CarMaintainceList *vc = [CarMaintainceList new];
                [self.navigationController pushViewController:vc animated:true];
            }else if ([name isEqualToString:@"全车保修"]){
                MyServicesOfYear *vc = [MyServicesOfYear new];
                [self.navigationController pushViewController:vc animated:true];
            }else if ([name isEqualToString:@"上门服务"]){
                  if (!accessToken) {
                      [[NSNotificationCenter defaultCenter] postNotificationName:@"quickLogin" object:nil];
                      
//                      普通买家 delafqm
                      return ;
                  }
                
                HelpVC *vc = [HelpVC new];
                [self.navigationController pushViewController:vc animated:true];
                
            }else if ([name isEqualToString:@"维修门店"]){
                 WeiXiuShop *vc = [WeiXiuShop new];
                [self.navigationController pushViewController:vc animated:true];
            }
            
        };
        return cell;
    }else if (indexPath.section == 2){
        static NSString *Identifier = @"RepairHeaderCell";
        RepairHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[RepairHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        
        return cell;
    }else if(indexPath.section == 0){
        static NSString *Identifier = @"RepairBannerCell";
        RepairBannerCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[RepairBannerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        //               [cell setupData:self.dataArray[indexPath.row]];
        return cell;
        
        
        
    }else{
//        static NSString *Identifier = @"ShopItemCell";
//        ShopItemCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
//        if (cell == nil) {
//            cell = [[ShopItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
//        }
//        [cell setupData:self.dataArray[indexPath.row]];
//        return cell;
        static NSString *Identifier = @"WeixiuTaocanCell";
        WeixiuTaocanCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if (cell == nil) {
            cell = [[WeixiuTaocanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        }
        [cell setupData:self.dataArray[indexPath.row]];
        WeakSelf(self)
        cell.itemClickBlock = ^{
            StrongSelf(self)
            NSDictionary *dic = self.dataArray[indexPath.row];
            ShopInfoVC *vc = [ShopInfoVC new];
            vc.shopsId = [NSString stringWithFormat:@"%@",dic[@"shopId"]];
            [self.navigationController pushViewController:vc animated:true];
        };
        return cell;
    }
}
   


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return Screen_Width/4 * 2;
    }else if (indexPath.section == 0){
        return 150.f;
    }else{
        return UITableViewAutomaticDimension;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3) {
        //        ShopInfoVC *vc = [ShopInfoVC new];
        //        NSDictionary *dic = self.dataArray[indexPath.row];
        //        vc.shopsId = [NSString stringWithFormat:@"%@",dic[@"id"]];
        //        [self.navigationController pushViewController:vc animated:true];
        GoodsWithSpecificVC *vc = [GoodsWithSpecificVC new];
        NSDictionary *dic = self.dataArray[indexPath.row];
        vc.goodsId = dic[@"id"];
        [self.navigationController pushViewController:vc animated:true];
    }
}

#pragma mark ---EmptyDelegate----
- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView{
    NoDataView *view = [NoDataView initView];
    view.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2 - 75, CGRectGetHeight(self.view.frame)/2-75, 150, 150);

    return view;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{

//    [self.collectionView reloadNetworkDataSet];
//    [self getHomeData];
}

-(UITableView *)TabelView
{
    if(_TabelView==nil)
    {
        _TabelView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_TabelView setDelegate:self];
        [_TabelView setDataSource:self];
        [_TabelView setEmptyDataSetSource:self];
        [_TabelView setTableFooterView:[UIView new]];
        _TabelView.rowHeight = UITableViewAutomaticDimension;
        _TabelView.estimatedRowHeight = 44;
        _TabelView.showsVerticalScrollIndicator = false;
        [_TabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_TabelView registerClass:[RepairFuncCell class] forCellReuseIdentifier:@"RepairFuncCell"];
        [_TabelView registerNib:[UINib nibWithNibName:@"RepairHeaderCell" bundle:nil] forCellReuseIdentifier:@"RepairHeaderCell"];
        [_TabelView registerNib:[UINib nibWithNibName:@"WeixiuTaocanCell" bundle:nil] forCellReuseIdentifier:@"WeixiuTaocanCell"];
        _TabelView.backgroundColor = [UIColor whiteColor];
        WeakSelf(self)
        _TabelView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            StrongSelf(self);
            [self getWeixiuData:RefreshTypeUP];
        }];
        [self.view addSubview:_TabelView];
    }
    return _TabelView;
}

- (ShopApi *)shopApi{
    if (!_shopApi) {
        _shopApi = [ShopApi new];
    }
    return _shopApi;
}

- (UIView *)titleView{
    if (!_titleView) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 30)];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [button setImage:[UIImage imageNamed:@"back"] forState:0];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
        [button addTarget:self action:@selector(clickPop) forControlEvents:UIControlEventTouchUpInside];
        [_titleView addSubview:button];

        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(button.frame), 0, _titleView.frame.size.width - 70, 30)];
        titleView.backgroundColor = [UIColor whiteColor];
        titleView.layer.cornerRadius = 15;
        [titleView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onClickSearchBtn)]];
        
        UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 20, 20)];
        selectIcon.image=[UIImage imageNamed:@"搜索"];
        
        UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(selectIcon.frame), 2, _titleView.frame.size.width-130-selectIcon.frame.size.width, _titleView.frame.size.height - 3)];
        textField.textColor=UIColor333;
        textField.placeholder=@"请输入套餐名称";
        
        if (@available(iOS 13.0, *)) {
            
            textField.attributedPlaceholder=[[NSAttributedString alloc]initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"A1A1A1"]}];

        }else{
            [textField setValue:[UIColor colorWithHexString:@"A1A1A1"] forKeyPath:@"_placeholderLabel.textColor"];
        }
        textField.font=LabelFont14;
        textField.returnKeyType = UIReturnKeySearch;
        textField.userInteractionEnabled = false;

        [titleView addSubview:selectIcon];
        [titleView addSubview:textField];
        [_titleView addSubview:titleView];
        
    }
    
    return _titleView;
}

- (void)clickPop{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)onClickSearchBtn{
    SearchVC *vc = [SearchVC new];
    [self.navigationController pushViewController:vc animated:true];
}

@end
